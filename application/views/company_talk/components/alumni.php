<div class="card-body">
    <?php if(count($registered) == 1):?>
        <b>Halaman Detail Pendaftaran Alumni</b>
        <?php
        if ($registered->is_valid == '0') {
            echo '<div class="alert alert-warning"><b>Mohon Ditunggu!</b><p class="text-white">Pendaftaran anda sedang dalam proses review dan menunggu verifikasi penyelenggara</p></div>';
        } else if ($registered->is_valid == '1') {
            echo '<div class="alert alert-success"><b>Pendaftaran Disetujui!</b><p class="text-white">Selamat, pendaftaran ada telah lolos verifikasi. Jangan lupa datang tepat waktu pada ruang yang ditentukan.</p></div>';
        }else{
            echo '<div class="alert alert-danger"><b>Mohon Maaf!</b><p class="text-white">Pendaftaran anda tidak lolos. Silahkan tunggu pada periode selanjutnya atau pilih company talk yang tersedia.</p></div>';

        }
        ?>
        <?php else:?>
            <b>Form Pendaftaran Company Talk</b>
            <div class="d-flex justify-content-center my-3">
                <button class="btn btn-success btn-lg btn-block" id="btn-daftar"><i class="fas fa-check"></i> Daftar Sekarang</button>
            </div>
    <?php endif;?>
</div>