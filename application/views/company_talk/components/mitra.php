<div class="card-body">
    <b>Daftar Peserta Company Talk</b>
    <div class="my-3 form-inline">
        <select name="filter_status" id="filter_status" class="form-control">
            <option value="">Semua Status</option>
            <option value="0">Belum diverikasi</option>
            <option value="1">Valid</option>
            <option value="2">Tidak Valid</option>
        </select>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered table-sm" id="tabel-peserta">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama lengkap</th>
                    <th>Email/Nomor HP</th>
                    <th>Jenis kelamin</th>
                    <th>Asal Kampus</th>
                    <th>Prodi</th>
                    <th>Alamat</th>
                    <th>Status</th>
                    <th>Kelola</th>
                </tr>
            </thead>
        </table>
    </div>
</div>