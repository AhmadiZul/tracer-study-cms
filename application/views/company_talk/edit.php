<link href="<?php echo base_url() ?>public/vendor/summernote/summernote-bs4.min.css" rel="stylesheet">
<div class="card">
    <form id="form_company_edit">
        <div class="card-body">
        <div class="form-group row">
                <label for="judul_presentasi" class="col-md-3 col-form-label">Judul Presentasi <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="hidden" name="id_mitra" id="id_mitra" class="form-control">
                    <input type="text" name="judul_presentasi" id="judul_presentasi" class="form-control" maxlength="100" value="<?php echo $data['data']->judul_presentasi ?>">
                    <div class="invalid-feedback" for="judul_presentasi"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <textarea name="deskripsi" id="deskripsi" class="form-control"><?php echo $data['data']->deskripsi ?></textarea>
                    <div class="invalid-feedback" for="deskripsi"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="waktu_awal_daftar" class="col-md-3 col-form-label">Waktu Mulai & Selesai <b class="text-danger">*</b></label>
                <div class="col-md-4">
                    <div class="input group">
                        <input type="text" class="form-control fulldate" name="waktu_awal_daftar" id="waktu_awal_daftar" value="<?php echo $data['data']->waktu_awal_daftar ?>">
                        <div class="invalid-feedback" for="waktu_awal_daftar"></div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="input-group-text font-weight-bold">to</div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <input type="text" class="form-control fulldate" name="waktu_akhir_daftar" id="waktu_akhir_daftar" value="<?php echo $data['data']->waktu_akhir_daftar ?>">
                        <div class="invalid-feedback" for="waktu_akhir_daftar"></div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="tanggal_pelaksanaan" class="col-md-3 col-form-label">Tanggal Pelaksanaan <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" class="form-control date-only" autocomplete="off" name="tanggal_pelaksanaan" placeholder="tanggal pelaksanaan" value="<?php echo $data['data']->tanggal_pelaksanaan ?>">
                    <div class="invalid-feedback" for="tanggal_pelaksanaan"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="id_perguruan_tinggi_lokasi" class="col-md-3 col-form-label">Perguruan Tinggi <b class="text-danger">*</b></label>
                <div class="col-md-9">
                <select name="id_perguruan_tinggi_lokasi" id="id_perguruan_tinggi_lokasi" class="form-control select2" value="<?php echo $data['data']->id_perguruan_tinggi_lokasi ?>">
                        <option value="">Pilih Perguruan Tinggi</option>
                        <?php foreach ($perguruan_tinggi as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value ?></option>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback" for="id_perguruan_tinggi_lokasi"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="di_ruang" class="col-md-3 col-form-label">Ruang <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="di_ruang" id="di_ruang" placeholder="masukan ruang" value="<?php echo $data['data']->di_ruang ?>">
                    <div class="invalid-feedback" for="di_ruang"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="kapasitas" class="col-md-3 col-form-label">Kapasitas <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="kapasitas" id="kapasitas" placeholder="masukan kapasitas" value="<?php echo $data['data']->kapasitas ?>">
                    <div class="invalid-feedback" for="kapasitas"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="fasilitas" class="col-md-3 col-form-label">Fasilitas <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="fasilitas" id="fasilitas" placeholder="masukan fasilitas" value="<?php echo $data['data']->fasilitas ?>">
                    <div class="invalid-feedback" for="fasilitas"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="fasilitas_tambahan" class="col-md-3 col-form-label">Fasilitas Tambahan <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <textarea name="fasilitas_tambahan" id="fasilitas_tambahan" class="form-control"><?php echo $data['data']->fasilitas_tambahan ?></textarea>
                    <div class="invalid-feedback" for="fasilitas_tambahan"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="<?php echo base_url('company_talk/index/mitra') ?>" class="btn btn-warning">Kembali</a>
            <button type="submit" class="btn btn-success">Simpan</button>
        </div>
    </form>
</div>
<script src="<?php echo base_url() ?>public/vendor/summernote/summernote-bs4.min.js"></script>
<script>
    var site_url = '<?php echo site_url() ?>';
    var toolbar = [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link']],
        ['misc', ['fullscreen']]
    ];
    $('#deskripsi').summernote({
        tabsize: 2,
        height: 200,
        toolbar: toolbar
    });
    $('#fasilitas_tambahan').summernote({
        tabsize: 2,
        height: 200,
        toolbar: toolbar
    });
    $('#form_company_edit').on('submit', function(e) {
        e.preventDefault();

        $('#form_company_edit').find('input, select, textarea').removeClass('is-invalid');
        $('input, textarea').parent().removeClass('is-invalid');
        $(`.invalid-feedback`).removeClass('d-block');
        $.ajax({
            url: site_url + '/company_talk/index/edit',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                swal({
                    title: data.message.title,
                    text: data.message.body,
                    type: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = site_url + 'company_talk/index/mitra';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                dismiss_loading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);

                                if (field == 'start_publish') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'end_publish') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'gaji_rate_atas') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'gaji_rate_bawah') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal({
                            title: response.message.title,
                            html: response.message.body,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    });
</script>