var site_url = '<?php echo site_url() ?>';
    $(document).ready(function() {
        window.addEventListener('gcrud.datagrid.ready', () => {
            $(".gc-header-tools").append(
                `<div class="floatL">
            <button id="btn-unduh" class="btn btn-default btn-outline-dark t5">
              <i class="fa fa-plus floatL t3"></i>
              <span class="hidden-xs floatL l5">Tambah Company Talk</span>
              <div class="clear">
              </div>
            </button>
          </div>`
            );

            $('#btn-unduh').click(function(e) {
                e.preventDefault();
                window.location.href = site_url + 'company_talk/index/halamanTambah';
            })
        });
    })
    $.getJSON(
        `${site_url}/company_talk/index/universitas`,
        null,
        function (data, textStatus, jqXHR) {
            if (data.status != 500) {
                $('#form-company-talk').find('select[name=id_perguruan_tinggi_lokasi]').html('<option value="">Pilih Polbangtan/Pepi</option>');
                data.data.forEach((element) => {
                    $('#form-company-talk').find('select[name=id_perguruan_tinggi_lokasi]').append(`<option value="${element.id_perguruan_tinggi}">${element.nama_pendek}</option>`);
                });
            }
        });

function hapusCompanyTalk(id) {
    swal({
        type: 'warning',
        title: 'Mohon diperhatikan !',
        text: 'Data yang dihapus tidak bisa dikembalikan, apakah anda yakin ?',
        showCancelButton: true,
        confirmButtonText: 'Ya, Hapus !',
        confirmButtonColor: '#007AFF',
        cancelButtonText: 'Tidak',
        closeOnConfirm: false
    }).then(function (isvalid) {
        if (typeof isvalid.value != "undefined" && isvalid.value) {
            $.ajax({
                type: 'ajax',
                method: 'POST',
                url: site_url + '/company_talk/index/delete',
                data: {
                    id: id
                },
                async: false,
                cache: false,
                dataType: 'json',
                success: function (response) {
                    if (response.status == 201) {
                        swal('SUKSES !', response.message, 'success').then(function () {
                            $('.fa-refresh').trigger('click');
                        })
                    }
                },
                error: function (xmlhttprequest) {
                    swal('Gagal!', xmlhttprequest.responseJSON.message, 'error');
                    console.log(xmlresponse);
                }
            })
        }
    })
}

function edit(id) {

    $.ajax({
        type: 'ajax',
        method: 'POST',
        url: site_url + '/company_talk/index/edit',
        data: {
            id: id
        },
        dataType: 'json',
        beforeSend: function () {},
        success: function (response) {
            if ((typeof response === 'string' || response instanceof String)) {
                swal('Gagal!', 'Aplikasi gagal terhubung dengan server. Silahkan hubungi admin.', 'error');
            }

            if (response.status == 201) {
                const referensi = response.data;
                $('#form-company-talk').find('input[name=id]').val(referensi.id);
                $('#form-company-talk').find('input[name=judul_presentasi]').val(referensi.judul_presentasi);
                $('#form-company-talk').find('textarea[name=deskripsi]').val(referensi.deskripsi);
                $('#form-company-talk').find('input[name=waktu_awal_daftar]').val(referensi.waktu_awal_daftar).trigger('input');
                $('#form-company-talk').find('input[name=waktu_akhir_daftar]').val(referensi.waktu_akhir_daftar).trigger('input');
                $('#form-company-talk').find('input[name=tanggal_pelaksanaan]').val(referensi.tanggal_pelaksanaan).trigger('input');
                $('#form-company-talk').find('input[name=di_ruang]').val(referensi.di_ruang);
                $('#form-company-talk').find('input[name=kapasitas]').val(referensi.kapasitas);
                $('#form-company-talk').find('textarea[name=fasilitas_tambahan]').val(referensi.fasilitas_tambahan);

                $.getJSON(
                    `${site_url}/company_talk/index/fasilitas`,
                    null,
                    function (data, textStatus, jqXHR) {
                        if (data.status != 500) {
                            $('#container-fasilitas').html('');
                            data.data.forEach((element) => {
                                $('#container-fasilitas').append(`<div class="custom-control custom-checkbox">
                                                <input type = "checkbox"
                                                name = "fasilitas[]"
                                                class = "custom-control-input"
                                                value = "${element.id}"
                                                id = "customCheck${element.id}" `+(referensi.fasilitas.includes(element.id) ?'checked': '')+`>
                                                <label class = "custom-control-label"
                                                for = "customCheck${element.id}" >${element.nama}</label>
                                                </div>`);
                            });
                        }
                    });
                $.getJSON(
                    `${site_url}/company_talk/index/universitas`,
                    null,
                    function (data, textStatus, jqXHR) {
                        if (data.status != 500) {
                            $('#form-company-talk').find('select[name=id_perguruan_tinggi_lokasi]').html('<option value="">Pilih Polbangtan/Pepi</option>');
                            data.data.forEach((element) => {
                                $('#form-company-talk').find('select[name=id_perguruan_tinggi_lokasi]').append(`<option value="${element.id_perguruan_tinggi}" ` + (referensi.id_perguruan_tinggi_lokasi == element.id_perguruan_tinggi ? 'selected' : '') + `>${element.nama_pendek}</option>`);
                            });
                        }
                    });
                $('#modal-company-talk').find('.modal-title').text('Ubah pendaftaran company talk');
                $('#form-company-talk').attr('action', `${site_url}/company_talk/index/update`);
                $('#modal-company-talk').modal('show');

            }
        },
        error: function (xmlhttprequest, textstatus, message) {
            // text status value : abort, error, parseerror, timeout
            // default xmlhttprequest = xmlhttprequest.responseJSON.message
            console.log(xmlhttprequest.responseJSON);
        },
    });

}