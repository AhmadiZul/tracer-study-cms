$(document).ready(function () {
    getCompanyTalk();
});

function getCompanyTalk() {
    $.ajax({
        type: 'ajax',
        method: 'POST',
        url: site_url + '/company_talk/index/getCompanyTalkList',
        // data : {kode : kode},
        dataType: 'json',
        // beforeSend: function () {},
        success: function (response) {
            if ((typeof response === 'string' || response instanceof String)) {
                swal('Gagal!', 'Aplikasi gagal terhubung dengan server. Silahkan hubungi admin.', 'error');
            }
            if (response.status == 201) {
                let html;
                const result = response.data;
                if (result.length != 0) {
                    $('#container-company-talk').html('');
                    result.forEach(element => {

                        $('#container-company-talk').append(`<div class="col-md-4">
                                    <div class="pricing pricing-highlight">
                                        <div class="pricing-title">
                                            ${element.sektor}
                                        </div>
                                        <div class="pricing-padding">
                                            <div class="pricing-price">
                                                <h3>${element.judul_presentasi}</h3>
                                                <div>${element.mitra}</div>
                                            </div>
                                            <div class="pricing-details">
                                                <div class="pricing-item">
                                                    <div class="pricing-item-icon"><i class="fas fa-calendar-alt"></i></div>
                                                    <div class="pricing-item-label">ww</div>
                                                </div>
                                                <div class="pricing-item">
                                                    <div class="pricing-item-icon"><i class="fas fa-users"></i></div>
                                                    <div class="pricing-item-label">wwOrang</div>
                                                </div>
                                                <div class="pricing-item">
                                                    <div class="pricing-item-icon"><i class="fas fa-building"></i></div>
                                                    <div class="pricing-item-label">${element.di_ruang}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pricing-cta">
                                            <a href = "${site_url}company_talk/index/detail?key=${element.id}"> ${element.is_available.length != 0? 'Detail' : 'Daftar Sekarang'} <i class = "fas fa-arrow-right"> </i></a>
                                        </div>
                                    </div>
                                </div>`);
                    });
                }else{
                    $('#container-company-talk').html('<div class="alert alert-info"><b>Mohon Maaf!</b><p>Tidak ada company talk aktif.</p></div>');
                }
            }
        },
        error: function (xmlhttprequest, textstatus, message) {
            // text status value : abort, error, parseerror, timeout
            // default xmlhttprequest = xmlhttprequest.responseJSON.message
            $('#container-company-talk').html('<div class="col-md-12"><div class="alert alert-info"><b>Mohon Maaf!</b><p>Tidak ada company talk aktif.</p></div></div>');
            console.log(xmlhttprequest.responseJSON);
        },
    });
}