var tabelPendaftar;
$(document).ready(function () {
    $('#filter_status').change(function () {
        tabelPendaftar.draw();
    })
    tabelPendaftar = $("#tabel-peserta").DataTable({
        sDom: "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        sServerMethod: "POST",
        autoWidth: false,
        bSort: false,
        pageLength: 15,
        bProcessing: false,
        bServerSide: true,
        fnServerParams: function (aoData) {
            var search = "";
            aoData.push({
                name: "sSearch",
                value: search,
            });
            aoData.push({
                name: "idCT",
                value: idCT,
            });
            aoData.push({
                name: "status",
                value: $('#filter_status').val(),
            });
        },
        fnStateSaveParams: function (oSetings, sValue) {
            // body...
        },
        fnStateLoadParams: function (oSetings, oData) {
            // body...
        },
        sAjaxSource: site_url + "company_talk/index/peserta",
        aoColumns: [{
                mDataProp: "no"
            },
            {
                mDataProp: "nama"
            },
            {
                mDataProp: "email"
            },
            {
                mDataProp: "jenis_kelamin"
            },
            {
                mDataProp: "asal_kampus"
            },
            {
                mDataProp: "prodi"
            },
            {
                mDataProp: "alamat"
            },
            {
                mDataProp: "status"
            },
            {
                mDataProp: "kelola"
            },
        ],
    });

    $('#tabel-peserta tbody').on('click', '#btn-validasi', function (e) {
        e.preventDefault();
        let data = $(this).attr('data');
        $.ajax({
            type: 'ajax',
            method: 'POST',
            url: site_url + '/company_talk/index/checkData',
            data: {
                kode: data
            },
            dataType: 'json',
            beforeSend: function () {},
            success: function (response) {
                if ((typeof response === 'string' || response instanceof String)) {
                    Swal.fire('Gagal!', 'Aplikasi gagal terhubung dengan server. Silahkan hubungi admin.', 'error');
                }
                if (response.status == 201) {
                    const result = response.data;
                    $('#form-verifikasi').find('.label-nama').text(result.nama);
                    $('#form-verifikasi').find('.label-email').text(result.email);
                    $('#form-verifikasi').find('.label-no-hp').text(result.no_hp);
                    $('#form-verifikasi').find('.label-jenis-kelamin').text((result.jenis_kelamin == 'L' ? 'Laki-laki' : 'Perempuan'));
                    $('#form-verifikasi').find('.label-kampus').text(result.nama_pendek);
                    $('#form-verifikasi').find('.label-prodi').text(result.nama_prodi);
                    $('#form-verifikasi').find('input[name=id]').val(result.id);
                }
                $('#modal-verifikasi').modal('show');
            },
            error: function (xmlhttprequest, textstatus, message) {
                // text status value : abort, error, parseerror, timeout
                // default xmlhttprequest = xmlhttprequest.responseJSON.message
                console.log(xmlhttprequest.responseJSON);
            },
        });
    });
    $('#form-verifikasi').submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'ajax',
            method: 'POST',
            url: site_url + '/company_talk/index/verifikasi',
            data: $('#form-verifikasi').serialize(),
            dataType: 'json',
            beforeSend: function () {},
            success: function (response) {
                if ((typeof response === 'string' || response instanceof String)) {
                    swal('Gagal!', 'Aplikasi gagal terhubung dengan server. Silahkan hubungi admin.', 'error');
                }
                if (response.status == 201) {
                    swal('Berhasil!', response.message, 'success').then(function () {
                        $('#form-verifikasi')[0].reset();
                        $('#modal-verifikasi').modal('hide');
                        tabelPendaftar.draw();
                    });
                }
            },
            error: function (xmlhttprequest, textstatus, message) {
                // text status value : abort, error, parseerror, timeout
                // default xmlhttprequest = xmlhttprequest.responseJSON.message
                console.log(xmlhttprequest.responseJSON);
            },
        });
    })
});