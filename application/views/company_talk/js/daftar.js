$(document).ready(function (e) {
    $('#btn-daftar').click(function (e) {
        e.preventDefault();
        swal({
            type: 'warning',
            title: 'Mohon diperhatikan !',
            text: 'Pendaftaran hanya bisa dilakukan satu. Pastikan data anda sudah sesuai, apakah anda yakin ?',
            showCancelButton: true,
            confirmButtonText: 'Ya, Daftar Sekarang !',
            confirmButtonColor: '#007AFF',
            cancelButtonText: 'Tidak',
            closeOnConfirm: false
        }).then(function (isvalid) {
            if (typeof isvalid.value != "undefined" && isvalid.value) {
                $.ajax({
                    type: 'ajax',
                    method: 'POST',
                    url: site_url + '/company_talk/index/registered',
                    data: {
                        id_company_talk: idCT
                    },
                    async: false,
                    cache: false,
                    dataType: 'json',
                    success: function (response) {
                        if (response.status == 201) {
                            swal('SUKSES !', response.message, 'success').then(function () {
                                location.reload();
                            })
                        }
                    },
                    error: function (xmlhttprequest) {
                        swal('Gagal!', xmlhttprequest.responseJSON.message, 'error');
                        console.log(xmlresponse);
                    }
                })
            }
        })
    })
})