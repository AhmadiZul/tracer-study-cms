<link rel="stylesheet" href="<?= base_url('public/vendor/datatables/jquery.dataTables.min.css') ?>">
<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <b>Data Detail Company Talk <?php echo $data['data']->judul_presentasi ?></b>
                <table class="table table-sm borderless">
                    <tr>
                        <th>Judul Presentasi</th>
                        <th>:</th>
                        <td><?php echo $data['data']->judul_presentasi ?></td>
                    </tr>
                    <tr>
                        <th>Deskripsi</th>
                        <th>:</th>
                        <td><?php echo $data['data']->deskripsi ?></td>
                    </tr>
                    <tr>
                        <th>Perguruan Tinggi Tempat Pelaksanaan</th>
                        <th>:</th>
                        <td><?php echo $data['data']->nama_pendek ?></td>
                    </tr>
                    <tr>
                        <th>Ruangan</th>
                        <th>:</th>
                        <td><?php echo $data['data']->di_ruang ?></td>
                    </tr>
                    <tr>
                        <th>Kapasitas</th>
                        <th>:</th>
                        <td><?php echo $data['data']->kapasitas ?></td>
                    </tr>
                    <tr>
                        <th>Fasilitas</th>
                        <th>:</th>
                        <td>
                            <?php if (!empty($data['data']->fasilitas)) : ?>
                                <ol>
                                    <?php foreach ($data['data']->fasilitas['data'] as $rows) : ?>
                                        <li><?php echo $rows->nama ?></li>
                                    <?php endforeach; ?>
                                </ol>
                            <?php else : ?>
                                -
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Fasilitas Tambahan</th>
                        <th>:</th>
                        <td><?php echo $data['data']->fasilitas_tambahan ?></td>
                    </tr>
                    <tr>
                        <th>Tanggal Pendaftaran</th>
                        <th>:</th>
                        <td>
                            <?php echo $this->tanggalindo->konversi_tgl_jam($data['data']->waktu_awal_daftar) . ' - ' . $this->tanggalindo->konversi_tgl_jam($data['data']->waktu_akhir_daftar) ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Pelaksanaan</th>
                        <th>:</th>
                        <td>
                            <?php echo $this->tanggalindo->konversi($data['data']->tanggal_pelaksanaan) ?>
                        </td>
                    </tr>
                </table>
            </div>
            <?php if(getSessionRoleAccess() == '4'):?>
                <?php $this->load->view('company_talk/components/alumni')?>
                <?php else:?>
                    <?php $this->load->view('company_talk/components/mitra')?>
            <?php endif;?>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-verifikasi">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <form action="" id="form-verifikasi">
                <div class="modal-header">
                    <b class="modal-title">Verifikasi pendaftar company talk</b>
                    <button class="close" data-dismiss="times">&times;</button>
                </div>
                <div class="modal-body">
                    <table class="table table-sm borderless" id="tabel-verifikasi">
                        <tr>
                            <th width="150px">Nama</th>
                            <th width="10px">:</th>
                            <td class="label-nama"></td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <th>:</th>
                            <td class="label-email"></td>
                        </tr>
                        <tr>
                            <th>Nomor HP</th>
                            <th>:</th>
                            <td class="label-no-hp"></td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <th>:</th>
                            <td class="label-jenis-kelamin"></td>
                        </tr>
                        <tr>
                            <th>Asal Kampus</th>
                            <th>:</th>
                            <td class="label-kampus"></td>
                        </tr>
                        <tr>
                            <th>Prodi</th>
                            <th>:</th>
                            <td class="label-prodi"></td>
                        </tr>
                    </table>
                    <div class="form-group">
                        <label>Status Verifikasi</label><br>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="is_valid1" name="is_valid" value="1" required class="custom-control-input">
                            <label class="custom-control-label" for="is_valid1">Valid dan Diterima</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="is_valid2" name="is_valid" value="2" required class="custom-control-input">
                            <label class="custom-control-label" for="is_valid2">Tidak Valid</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="hidden" name="id">
                    <button class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
                    <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?= base_url('public/vendor/datatables/datatables.min.js'); ?>"></script>
<script>
    let idCT = "<?php echo $data['data']->id ?>";
    const site_url = '<?php echo site_url() ?>';
</script>