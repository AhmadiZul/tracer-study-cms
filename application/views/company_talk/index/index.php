<?php
if (isset($output->css_files)) {
    foreach ($output->css_files as $file) {
        echo '<link type="text/css" rel="stylesheet" href="' . $file . '"/>';
    }
}
?>
<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <small>
                    <?php if (isset($output)) : ?>
                        <?php echo $output->output; ?>
                    <?php endif; ?>
                </small>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-company-talk">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" id="form-company-talk" autocomplete="off">
                <div class="modal-header">
                    <b class="modal-title">Form Company</b>
                    <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Judul Presentasi <b class="text-danger">*</b></label>
                        <input type="text" class="form-control" name="judul_presentasi" maxlength="100" required placeholder="Judul Presentasi company talk">
                        <small class="text-muted">Panjang karakter maksimal 100 karakter</small>
                    </div>
                    <div class="form-group">
                        <label>Deskripsi Presentasi <b class="text-danger">*</b></label>
                        <textarea name="deskripsi" id="deskripsi" class="form-control" required style="height: 200px;" maxlength="200"></textarea>
                        <small class="text-muted">Panjang karakter maksimal 200 karakter</small>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Mulai Daftar <b class="text-danger">*</b></label>
                                <input type="text" class="form-control fulldate" autocomplete="off" name="waktu_awal_daftar" required placeholder="tanggal mulai pendaftaran">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Akhir Daftar <b class="text-danger">*</b></label>
                                <input type="text" class="form-control fulldate" autocomplete="off" name="waktu_akhir_daftar" required placeholder="tanggal akhir pendaftaran">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pelaksanaan <b class="text-danger">*</b></label>
                        <input type="text" class="form-control date-only" autocomplete="off" name="tanggal_pelaksanaan" required placeholder="tanggal pelaksanaan">
                    </div>
                    <div class="form-group">
                        <label>Perguruan Tinggi Tempat Pelaksanaan Company Talk <b class="text-danger">*</b></label>
                        <select name="id_perguruan_tinggi_lokasi" id="id_perguruan_tinggi_lokasi" class="form-control" required>
                            <option value="">Pilih Perguruan Tinggi</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Ruang <b class="text-danger">*</b></label>
                        <input type="text" name="di_ruang" class="form-control" required onkeypress="return hanyaAngka(event)">
                    </div>
                    <div class="form-group">
                        <label>Kapasitas <b class="text-danger">*</b></label>
                        <input type="text" name="kapasitas" class="form-control" required onkeypress="return hanyaAngka(event)">
                    </div>
                    <div class="form-group">
                        <label>Fasilitas</label>
                        <div id="container-fasilitas"></div>
                    </div>
                    <div class="form-group">
                        <label>Fasilitas Tambahan</label>
                        <textarea name="fasilitas_tambahan" id="fasilitas_tambahan" class="form-control" style="height: 200px;"></textarea>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="hidden" name="id">
                    <button class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
                    <button class="btn btn-success" type="submit"><i class="fas fa-save"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>
<script>
    var site_url = '<?php echo site_url() ?>';
</script>