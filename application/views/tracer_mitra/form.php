<div class="wrapper"></div>
<main id="main">
    <section class="kuesioner">
        <div class="container" data-aos="fade-up">
            <div class="text-center">
                <?php
                $urlLogo = base_url() . 'public/assets/img/logo-besar.png';

                if (!empty($jadwal['path_logo'])) {
                    $urlLogo = URL_LOGO_UNIV . $jadwal['path_logo'];
                }
                ?>
                <img src="<?php echo $urlLogo ?>" style="width:200px" class="img-fluid">
                <div class="row mt-5">
                    <h2 class="text-center"><?= (!empty($survey['deskripsi']) ? $survey['deskripsi'] : '') ?></h2>
                </div>
            </div>
            <div class="row mt-2">
                <p align="left" class="lead">Infokan link ini kepada sesama mitra yang mungkin belum mengisi tracer untuk mitra. <br>Silahkan memilih asal kampus/sekolah dan prodi/jurusan dari lulusan yang bekerja pada instansi anda.</p>
            </div>
            <form id="form_kuesioner" name="form_kuesioner" method="post" role="form">
                <input type="hidden" name="id_survey" id="id_survey" value="<?= $id_survey ?>" class="form-control">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="perguruan_tinggi">Asal Kampus/Sekolah <b class="text-danger">*</b></label>
                        <select name="perguruan_tinggi" id="perguruan_tinggi" class="form-control select2">
                            <option value="">Pilih asal kampus/sekolah</option>
                            <?php foreach ($perguruan_tinggi as $key => $value) { ?>
                                <option value="<?= $key ?>"><?= $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback" for="perguruan_tinggi">Wajib diisi</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="kode_prodi">Prodi/Jurusan <b class="text-danger">*</b></label>
                        <select name="kode_prodi" id="kode_prodi" class="form-control select2">
                            <option value="">Pilih Prodi/Jurusan</option>
                        </select>
                        <div class="invalid-feedback" for="kode_prodi">Wajib diisi</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="jenis">Jenis <b class="text-danger">*</b></label>
                        <select name="jenis" id="jenis" class="form-control select2">
                            <option value="">Pilih Jenis Mitra</option>
                            <option value="Perusahaan swasta/Industri Swasta">Perusahaan swasta/Industri Swasta</option>
                            <option value="BUMN/Perusahaan Milik Pemerintah">BUMN/Perusahaan Milik Pemerintah</option>
                            <option value="Pemerintah Daerah/Pusat">Pemerintah Daerah/Pusat</option>
                            <option value="Lembaga Pendidikan Negeri">Lembaga Pendidikan Negeri</option>
                            <option value="Lembaga Pendidikan Swasta">Lembaga Pendidikan Swasta</option>
                            <option value="Lainnya">Lainnya</option>
                        </select>
                        <div class="invalid-feedback" for="jenis">Wajib diisi</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="sektor">Sektor <b class="text-danger">*</b></label>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="sektor" id="pertanian" value="Pertanian">
                            <label class="custom-control-label" for="pertanian">Pertanian</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="sektor" id="non_pertanian" value="Non Pertanian">
                            <label class="custom-control-label" for="non_pertanian">Non Pertanian</label>
                        </div>
                        <div class="invalid-feedback" for="sektor">Wajib diisi</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email">Email <b class="text-danger">*</b></label>
                        <input type="email" name="email" id="email" class="form-control" maxlength="100">
                        <div class="invalid-feedback" for="email">Wajib diisi</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="telepon">Telepon <b class="text-danger">*</b></label>
                        <input type="telepon" name="telepon" id="telepon" maxlength="13" class="form-control number-only">
                        <div class="invalid-feedback" for="telepon">Wajib diisi</div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="alamat">Alamat <b class="text-danger">*</b></label>
                        <textarea name="alamat" id="alamat" class="form-control" maxlength="255" style="height: 100%;"></textarea>
                        <div class="invalid-feedback" for="alamat">Wajib diisi</div>
                    </div>
                </div>
                <div class="col-md-12 pt-5">
                    <button type="submit" class="btn btn-secondary float-end">Mulai Kuesioner</button>
                </div>
            </form>

        </div>
    </section>
</main>

<script type="text/javascript">
    var site_url = '<?php echo site_url() ?>tracer_mitra/index/';

    /**
     * select prodi
     */
    $('[name="perguruan_tinggi"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'getProdi',
            type: "GET",
            data: {
                kode_prodi: val
            },
            dataType: "JSON",
            success: function(data) {
                buatProdi(data);
                $('[name="kode_prodi"]').select2({
                    placeholder: '- Pilih Prodi/Jurusan -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatProdi(data) {
        let optionLoop = '<option value>Pilih Prodi/Jurusan</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        if ($('[name="kode_prodi"]')) {
            $('[name="kode_prodi"]').children().remove();
        }
        $('[name="kode_prodi"]').append(optionLoop);
    }

    /**
     * submit registrasi alumni
     */
    $('#form_kuesioner').on('submit', function(e) {
        e.preventDefault();

        $('#form_kuesioner').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + "simpan_mitra",
            dataType: 'json',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            processData: false,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                swal({
                    title: data.message.title,
                    text: data.message.body,
                    type: 'success',
                    confirmButtonColor: '#396113',
                }, ).then(function() {
                    window.location.href = site_url + 'isiKuesioner?a_id=' + data.data;
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                dismiss_loading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal({
                            title: data.message.title,
                            text: data.message.body,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        });
                        break;
                    default:
                        break;
                };
            }
        });
    });
</script>