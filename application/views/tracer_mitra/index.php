<div class="wrapper"></div>

<section id="jadwal-tracer" class="jadwal pt-5">
    <div class="col-md-12 col-sm-12">
        <h2 class="text-center">Survey Mitra</h2>
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
                <li class="page-item"><button id="btn_prev" class="page-link">Sebelumnya</button></li>
                <li class="page-item"><span id="page_span" class="page-link disabled"></span></li>
                <li class="page-item"><button id="btn_next" class="page-link">Selanjutnya</button></li>
            </ul>
        </nav>
    </div>
    <div class="row" id="listingTable">
        
    </div>
</section>
<!-- FOR PAGINATION AND FILTERING -->
<script>
    let current_page = 1;
    let records_per_page = 9;

    let objJson = JSON.parse(`<?php echo isset($survei) ? json_encode($survei) : NULL; ?>`);
    console.log(objJson);

    function prevPage() {
        if (current_page > 1) {
            current_page--;
            changePage(current_page);
        }
    }

    function nextPage() {
        if (current_page < numPages()) {
            current_page++;
            changePage(current_page);
        }
    }

    function changePage(page) {
        // array objJson baru
        let tempObjJson = objJson.map(function(obj) {
            return obj;
        });

        // Validate page
        if (page < 1) page = 1;
        if (page > numPages(tempObjJson)) page = numPages(tempObjJson);

        $('#listingTable').html('');

        if (tempObjJson.length <= 0) {
            $('#btn_prev, #btn_next').css('visibility', 'hidden');
            $('#page_span').html('0/0');
            return;
        }

        // MEMBUAT CARD HTML
        let tempAllCard = '';
        for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < tempObjJson.length; i++) {
            tempAllCard += renderCard(tempObjJson[i]);
        };

        $('#listingTable').html(tempAllCard);
        $('#page_span').html(page + '/' + numPages(tempObjJson));

        if (page == 1) {
            $('#btn_prev').css('visibility', 'hidden');
        } else {
            $('#btn_prev').css('visibility', 'visible');
        };

        if (page == numPages(tempObjJson)) {
            $('#btn_next').css('visibility', 'hidden');
        } else {
            $('#btn_next').css('visibility', 'visible');
        };
    };

    function numPages(obj = null) {
        if (obj === null) {
            return Math.ceil(objJson.length / records_per_page);
        };
        return Math.ceil(obj.length / records_per_page);
    }

    $(document).ready(function() {
        if (objJson) {
            changePage(1);
            $('#btn_prev').on('click', prevPage);
            $('#btn_next').on('click', nextPage);
        };
    });

    function renderCard(obj) {
        let untuk_survei_text = obj['untuk_survei_text'] ? obj['untuk_survei_text'] : 'Survei Alumni';
        let survei_mulai = obj['survei_mulai'] ? obj['survei_mulai'] : '';
        let survei_selesai = obj['survei_selesai'] ? obj['survei_selesai'] : '';
        let status = obj['status'] ? obj['status'] : '';
        let url = obj['url'] ? obj['url'] : '';
        if (status != '') {
            switch (status) {
                case 'delayed':
                    status = '<span class="btn btn-secondary disabled float-end"> Belum Dimulai </span>';
                    break;
                case 'ongoing':
                    status = `<a href="${url}" type="button" class="btn btn-secondary float-end"> Isi Survey </a>`;
                    break;
                case 'expired':
                    status = '<span class="btn btn-danger disabled float-end"> Telah Selesai </span>';
                    break;
                default:
                    status = '';
                    break;
            };
        };

        let template = `
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title text-center">
                            <h3>${untuk_survei_text}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <label><strong>Periode Pengisian Survey : </strong>${survei_mulai} - ${survei_selesai}</label><br>
                        ${status}
                    </div>
                </div>
            </div>
        `;
        return template;
    }
</script>