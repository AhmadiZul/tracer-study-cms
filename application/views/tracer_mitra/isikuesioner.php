<link href="<?php echo base_url() . 'public/vendor/smartwizard/smart_wizard_theme_arrows.min.css' ?>" rel="stylesheet">
<div class="wrapper mt-5"></div>

<section class="inner-page">
    <div class="container" data-aos="fade-up">

        <form id="form_kuisioner">
            <input type="hidden" name="id_answer" value="<?php echo $id_answer; ?>">
            <div class="content">
                <h3>Survey Kepuasan Pengguna Mitra</h3>
                <p class="font-italic">
                    Silahkan mengisi survey kepuasan pengguna mitra
                </p>
            </div>
            <div id="smartwizard">
                <ul class="nav">
                    <?php for ($i = 0; $i < count($question_stakeholder) + 1; $i++) { ?>
                        <li class="nav-item"><a href="#step-<?php echo ($i + 1) ?>" class="nav-link"><?php echo ($i + 1) ?></a></li>
                    <?php } ?>
                </ul>
                <div class="tab-content mt-4">
                    <?php $hit = 0; ?>
                    <script>
                        let data = [];
                    </script>
                    <?php foreach ($question_stakeholder as $per) { ?>
                        <div id="step-<?php echo ++$hit; ?>" class="tab-pane" role="tabpanel" aria-labelledby="step-<?php echo $hit; ?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <b style="font-size: 20px;"><?php echo $per['question']; ?> <span class="tandawajib-<?php echo $hit; ?> text-danger"></span></b>
                                </div><br><br>
                                <input type="hidden" name="type[]" value="<?php echo $per['question_type']; ?>">
                                <input type="hidden" name="idquestion[]" value="<?php echo $per['id_kuesioner']; ?>">
                                <script>
                                    data["<?php echo $hit ?>"] = [];
                                    data["<?php echo $hit ?>"]['step'] = <?php echo $hit; ?>;
                                    data["<?php echo $hit ?>"]['inpt'] = [];
                                </script>
                                <div class="col-md-12">
                                    <?php if ($per['question_type'] == '1') { ?>
                                        <?php $answer_rule = ''; ?>
                                        <?php $maxlength = '11'; ?>
                                        <?php if ($per['answer_rules'] == 'integer') : ?>
                                            <?php $answer_rule = 'number-only'; ?>
                                        <?php endif ?>
                                        <input type="text" name="inpt<?php echo $hit; ?>" class="form-control <?php echo $answer_rule; ?>" maxlength="<?php echo $maxlength; ?>">
                                        <script>
                                            data["<?php echo $hit ?>"]['inpt']["inpt<?php echo $hit; ?>"] = '';
                                        </script>
                                        <span class="d-none" id="wajib-<?php echo $hit; ?>"></span>
                                    <?php } else if ($per['question_type'] == '2') { ?>
                                        <textarea class="form-control" name="inpt<?php echo $hit; ?>"></textarea>
                                        <script>
                                            data["<?php echo $hit ?>"]['inpt']["inpt<?php echo $hit; ?>"] = '';
                                        </script>
                                        <span class="d-none" id="wajib-<?php echo $hit; ?>"></span>
                                    <?php } else if ($per['question_type'] == '3') { ?>
                                        <?php
                                        $radio = $this->db->select('*')->from('kuesioner_mitra_question_option')->where('id_kuesioner', $per['id_kuesioner'])->get()->result_array();
                                        foreach ($radio as $per_r) { ?>
                                            <?php if (isset($per_r['isian']) && ($per_r['isian'] == 'isian[on]')) {
                                                $inputType = 'text';
                                                $rule = '';
                                                $maxlength = 255;
                                                if (isset($per_r['answer_rules']) && !empty(trim($per_r['answer_rules']))) {
                                                    switch ($per_r['answer_rules']) {
                                                        case 'integer':
                                                            $rule = 'number-only';
                                                            $maxlength = 11;
                                                            break;
                                                        case 'valid_email':
                                                            $inputType = 'email';
                                                            break;
                                                        case 'is_date':
                                                            $inputType = 'date';
                                                            break;
                                                        default:
                                                            break;
                                                    };
                                                };
                                            ?>
                                                <input type="radio" name="inpt<?php echo $hit; ?>" value="<?php echo $per_r['id_opsi']; ?>" data-ke="<?php echo $i ?>" data-isian="true" data-hit="<?php echo $hit; ?>" required> <?php echo $per_r['opsi']; ?> <input type="<?php echo $inputType ?>" class="form-control ke-<?php echo $i ?> <?= $rule ?>" data-idkuesioner="<?php echo $per['id_kuesioner'] ?>_input_pilgan" name="<?php echo $per_r['id_opsi'] ?>_isian<?php echo $hit; ?>" maxlength="<?php echo $maxlength; ?>" readonly="readonly">
                                                <sup class="text-danger">
                                                    <?php echo isset($per_r['answer_code']) ? $per_r['answer_code'] : '' ?>
                                                </sup>
                                                <br>
                                            <?php } else {
                                            ?>
                                                <input type="radio" name="inpt<?php echo $hit; ?>" value="<?php echo $per_r['id_opsi']; ?>" data-ke="<?php echo $i ?>" required> <?php echo $per_r['opsi']; ?><br>
                                                <!-- <input type="radio" name="inpt<?php echo $hit; ?>" value="<?php echo $per_r['id_opsi']; ?>" >&nbsp;<?php echo $per_r['opsi']; ?><br> -->
                                        <?php
                                            }
                                        }
                                        ?>
                                        <script>
                                            data["<?php echo $hit ?>"]['inpt']["inpt<?php echo $hit; ?>"] = '';

                                            $('input[type=radio][name=inpt<?php echo $hit; ?>]').change(function() {
                                                let ke = $(this).data('ke');
                                                let hit = $(this).data("hit");
                                                if ($(this).data('isian') == true) {
                                                    $('[name$="_isian<?php echo $hit; ?>"]').val('')
                                                    $('[name$="_isian<?php echo $hit; ?>"]').prop('required', false)
                                                    $('[name$="_isian<?php echo $hit; ?>"]').attr('readonly', 'readonly')
                                                    $('[name="' + $(this).val() + '_isian' + hit + '"]').prop('required', true)
                                                    $('[name="' + $(this).val() + '_isian' + hit + '"]').removeAttr('readonly')
                                                } else {
                                                    $('[name$="_isian<?php echo $hit; ?>"]').val('')
                                                    $('[name$="_isian<?php echo $hit; ?>"]').prop('required', false)
                                                    $('[name$="_isian<?php echo $hit; ?>"]').attr('readonly', 'readonly')
                                                }
                                            });
                                        </script>
                                        <span class="d-none" id="wajib-<?php echo $hit; ?>"></span>
                                    <?php } else if ($per['question_type'] == '4') { ?>
                                        <?php
                                        $checkbox = $this->db->select('*')->from('kuesioner_mitra_question_option')->where('id_kuesioner', $per['id_kuesioner'])->get()->result_array();
                                        foreach ($checkbox as $per_c) { ?>
                                            <input type="checkbox" name="inpt<?php echo $hit; ?>[]" value="<?php echo $per_c['id_opsi']; ?>">&nbsp;<?php echo $per_c['opsi']; ?><br>
                                        <?php }
                                        ?>

                                        <?php if (isset($per['others']) && !empty(trim($per['others']))) : ?>
                                            <?php if ($per['others'] == 'lainnya[on]') : ?>
                                                <input type="checkbox" name="inpt<?php echo $hit; ?>[]" id="<?php echo $per['id_kuesioner'] ?>_lainnya">&nbsp;Lainnya : <input type="text" id="<?php echo $per['id_kuesioner'] ?>_lainnya_input" class="form-control" readonly="readonly"><br>
                                                <script>
                                                    /** agar jika "lainnya" selected, "input lainnya" menjadi required */
                                                    $('input[type=checkbox][name="inpt<?php echo $hit; ?>[]"]').change(function() {
                                                        if ($(this).attr('id') == "<?php echo $per['id_kuesioner'] ?>_lainnya") {
                                                            if ($(this).prop('checked') == true) {
                                                                $("#<?php echo $per['id_kuesioner'] ?>_lainnya_input").removeAttr('readonly');
                                                            } else {
                                                                $("#<?php echo $per['id_kuesioner'] ?>_lainnya_input").val('');
                                                                $("#<?php echo $per['id_kuesioner'] ?>_lainnya_input").attr('readonly', 'readonly');
                                                            }
                                                        } else {
                                                            if ($("#<?php echo $per['id_kuesioner'] ?>_lainnya").prop('checked') == true) {
                                                                $("#<?php echo $per['id_kuesioner'] ?>_lainnya_input").removeAttr('readonly');
                                                            } else {
                                                                $("#<?php echo $per['id_kuesioner'] ?>_lainnya_input").val('');
                                                                $("#<?php echo $per['id_kuesioner'] ?>_lainnya_input").attr('readonly', 'readonly');
                                                            }
                                                        }
                                                    });

                                                    /** agar jika value input lainnya sedang berubah, value pilihan lainnya juga ikut berubah sama */
                                                    $("#<?php echo $per['id_kuesioner'] ?>_lainnya_input").keyup(function() {
                                                        $("#<?php echo $per['id_kuesioner'] ?>_lainnya").val(this.value);
                                                    });
                                                </script>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <span class="d-none" id="wajib-<?php echo $hit; ?>"></span>
                                    <?php } else if ($per['question_type'] == '5') {
                                        $likert = $this->db->select('*')->from('kuesioner_mitra_question_likert')->where('id_kuesioner', $per['id_kuesioner'])->get()->result_array();
                                    ?>
                                        <input type="hidden" name="length<?php echo $hit; ?>" value="<?php echo count($likert); ?>">
                                        <br>
                                        <table class="table">
                                            <?php
                                            $pil = 'A';
                                            foreach ($likert as $per_l) {
                                                // echo ++$pil.'. '.$per_l['pertanyaan'].'<br>';
                                            ?>
                                                <tr>
                                                    <td style="vertical-align: top;"><?php echo $pil . '. ' . $per_l['pertanyaan']; ?></td>
                                                    <td><input type="radio" name="inpt<?php echo $hit; ?>_<?php echo $pil; ?>" value="1"><br>1</td>
                                                    <td><input type="radio" name="inpt<?php echo $hit; ?>_<?php echo $pil; ?>" value="2"><br>2</td>
                                                    <td><input type="radio" name="inpt<?php echo $hit; ?>_<?php echo $pil; ?>" value="3"><br>3</td>
                                                    <td><input type="radio" name="inpt<?php echo $hit; ?>_<?php echo $pil; ?>" value="4"><br>4</td>
                                                    <td><input type="radio" name="inpt<?php echo $hit; ?>_<?php echo $pil; ?>" value="5"><br>5</td>
                                                </tr>
                                                <script>
                                                    data["<?php echo $hit ?>"]['inpt']["inpt<?php echo $hit; ?>_<?php echo $pil; ?>"] = '';
                                                </script>
                                                <?php $pil++ ?>
                                            <?php
                                            }
                                            ?>
                                        </table>
                                        <span class="d-none" id="wajib-<?php echo $hit; ?>"></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ++$hit; ?>
                    <script>
                        let lastHit = <?php echo $hit; ?>;
                    </script>
                    <div id="step-<?php echo $hit; ?>" class="">
                        <div class="row">
                            <div class="col-md-12"> <span>Terima Kasih Telah Mengisi Kuesioner</span><br><br>
                                <button type="submit" class="btn btn-block btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row container">
                <div class="col mt-2">
                    <b class="text-danger"> * </b> Pertanyaan wajib dijawab.
                </div>
            </div>
        </form>

    </div>
</section>
</main>
</div>
</header>
<script src="<?php echo base_url() . 'public/vendor/smartwizard/jquery.smartWizard.min.js' ?>"></script>
<script type='text/javascript'>
    $(document).ready(function() {

        $('#smartwizard').smartWizard({
            // selected: 0,
            theme: 'arrows',
            transitionEffect: 'fade',
            showStepURLhash: false,
        });

        $('#smartwizard').on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
            var elmForm = $("#step-" + (stepNumber + 1));
            if (stepDirection === 'forward' && elmForm) {
                var type = $('[name="type[]"]');
                var id_kuesioner = $('[name="idquestion[]"]');
                var isFilled = $('[name="inpt' + (stepNumber + 1) + '"]').val();
                var pesan = 'Harus diisi';

                if (type[stepNumber].value == 3) {
                    var checked = $('[name="inpt' + (stepNumber + 1) + '"]:checked').val();
                    isFilled = $('[name="inpt' + (stepNumber + 1) + '"]').is(':checked')

                    if ($('[name="' + checked + '_isian' + (stepNumber + 1) + '"]').length != 0 && !$('[name="' + checked + '_isian' + (stepNumber + 1) + '"]').val()) {
                        isFilled = false;
                        pesan = 'Isian harus diisi';
                    }
                }
                if (type[stepNumber].value == 4) {
                    var check = $('[name="inpt' + (stepNumber + 1) + '[]"]');
                    isFilled = ($('[name="inpt' + (stepNumber + 1) + '[]"]:checked').length > 0 ? true : false)

                    console.log($('#' + id_kuesioner[stepNumber].value + '_lainnya').is(':checked'));

                    if ($('#' + id_kuesioner[stepNumber].value + '_lainnya').is(':checked') && !$('#' + id_kuesioner[stepNumber].value + '_lainnya_input').val()) {
                        isFilled = false;
                        pesan = 'Lainnya harus diisi';
                    }

                }
                if (type[stepNumber].value == 5) {
                    var length = $('[name="length' + (stepNumber + 1) + '"]').val();
                    var huruf = 'A';
                    isFilled = true;

                    for (let index = 0; index < length; index++) {
                        console.log(huruf);
                        if (!$('[name="inpt' + (stepNumber + 1) + '_' + huruf + '"]').is(':checked')) {
                            isFilled = false;
                        }
                        huruf = String.fromCharCode(huruf.charCodeAt(0) + 1);
                    }
                }

                if (isFilled) {
                    return true
                } else {
                    swal("Gagal", pesan, "warning");
                    return false
                }
            }
            return true;
        })
    });
</script>
<!-- End Header -->
<script type="text/javascript">
    for (let index = 1; index <= lastHit; index++) {
        if ($('span#wajib-' + index).length) {
            $('span.tandawajib-' + index).text('*');
        }
    }
    var site_url = '<?php echo site_url() . 'tracer_mitra/index/' ?>';
    $('#form_kuisioner').on('submit', function(e) {
        e.preventDefault()
        for (let d in data) {
            for (let value in data[d]) {
                if (value == 'inpt') {
                    for (let inpt in data[d]['inpt']) {
                        let tempAnswer;
                        if ($(`[name="${inpt}"]`).attr('type') == 'radio') {
                            tempAnswer = $(`[name="${inpt}"]:checked`).val();
                        } else {
                            tempAnswer = $(`[name="${inpt}"]`).val();
                        }
                        data[d]['inpt'][inpt] = tempAnswer;
                    }
                }
            }
        }

        let breakNow = false;
        data.forEach(d => {
            for (let key in d['inpt']) {
                if ((d['inpt'][key] == '' || d['inpt'][key] == undefined) && breakNow == false) {
                    alert(`Question ${d['step']} masih belum terjawab, periksa kembali dan jawab pertanyaan tersebut!`);
                    breakNow = true;
                }
            }
        });
        if (breakNow == true) {
            return;
        }

        $.ajax({
            url: site_url + "inputKuesioner",
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            success: function(data) {
                if (data.success) {
                    swal("Success", "Terimakasih jawaban kuesioner anda telah tersimpan", "success").then(function() {
                        window.location.href = '<?php echo base_url() . 'mitra'; ?>'
                    });
                } else {
                    if (typeof data.text !== "undefined") {
                        swal("Gagal", data.text, "warning");
                    } else {
                        swal("Gagal", "Gagal menyimpan jawaban kuesioner", "error");
                    }
                }
            }
        });
    });
</script>