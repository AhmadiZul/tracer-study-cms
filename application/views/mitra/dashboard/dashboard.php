<div class="row">
	<div class="col-xs-12 col-lg-4">
		<div class="card">
			<div class="card-header">
				<h3>Lowongan Dilihat</h3>
			</div>
			<div class="card-body">
				<h2>0</h2>
			</div>
			<div class="card-footer">
				<table>
					<tr>
						<td style="width:25%;"><label>Minggu ini</label></td>
						<td style="width:65%;"></td>
						<td style="width:10%;"><label>100%</label></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h3>Total Lowongan</h3>
			</div>
			<div class="card-body">
				<h2>0</h2>
			</div>
			<div class="card-footer">
				<table>
					<tr>
						<td style="width:25%;"><label>Minggu ini</label></td>
						<td style="width:65%;"></td>
						<td style="width:10%;"><label>100%</label></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-lg-4">
		<div class="card">
			<div class="card-header">
				<h3>Pekerjaan Dilamar</h3>
			</div>
			<div class="card-body">
				<h2>0</h2>
			</div>
			<div class="card-footer">
				<table>
					<tr>
						<td style="width:25%;"><label>Minggu ini</label></td>
						<td style="width:65%;"></td>
						<td style="width:10%;"><label>100%</label></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="card">
			<div class="card-header">
				<h3>Total Kandidat</h3>
			</div>
			<div class="card-body">
				<h2>0</h2>
			</div>
			<div class="card-footer">
				<table>
					<tr>
						<td style="width:25%;"><label>Minggu ini</label></td>
						<td style="width:65%;"></td>
						<td style="width:10%;"><label>100%</label></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-lg-4">
		<div class="card">
			<div class="card-header">
				<h3>Aktivitas</h3>
			</div>
			<div class="card-body do-nicescrol">
				<div class="item-activity d-inline-flex">
					<i class="fas fa-user"></i> <p>Profil telah berhasil diubah</p>
				</div>
			</div>
		</div>
	</div>
</div>