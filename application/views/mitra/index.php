<!-- ======= Hero Section ======= -->
<section id="hero2">
  <div class="row justify-content-center align-items-center">
    <div class="col-md-6 col-sm-12 align-self-center deskripsi">
      <h2 class="animate__animated animate__fadeInDown my-0">Tracer Study
        <br><span>Mitra Karir</span>
        <br>
        <br>
        <img src="<?php echo base_url() ?>public/assets/img/alumni/highlight-title.png" class="img img-fluid img-hero" alt="">
      </h2>
      <p class="animate__animated animate__fadeInUp">Pemberian umpan balik tentang kinerja para alumni, sekaligus sebagai data untuk kepentingan evaluasi hasil pendidikan. Dan dapatkan kemudahan perekrutan talent secara langsung.</p>
      <a href="<?php echo base_url() . 'tracer_mitra/index' ?>" class="btn btn-warning text-white animate__animated animate__fadeInUp scrollto btn-isi">Isi Kuesioner <i class="fas fa-arrow-alt-circle-right"></i></a>
    </div>
    <div class="col-md-6 d-none d-sm-none d-md-block d-lg-block d-xl-block">
      <img src="<?php echo base_url() ?>public/assets/img/mitra/img.png" class="img-leading" alt="">
    </div>
  </div>
</section><!-- End Hero -->

<main id="main">
  <!-- ======= Program Section ======= -->
  <section id="program" class="program">
    <div class="container" data-aos="fade-up">
      <div class="row content">
        <div class="col-lg-3  col-md-6 col-sm-12">
          <div>
            <h2>Program </h2>
            <h2 class="linkan-title">Mitra Karir</h2>
            <span class="mb-2">
              <b>
                Pengguna lulusan sekolah binaan Pusat Pendidikan Pertanian
              </b>
            </span>
          </div>
          <div>
            <img src="<?php echo base_url() ?>public/assets/img/mitra/bag.png" class="img img-fluid mt-2" alt="">
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 mt-2">
          <div class="card program-box">
            <div class="card-body">
              <img src="<?php echo base_url() ?>public/assets/img/mitra/messages.png" class="img img-fluid" style="width: 20%;" alt="">
              <h5 class="card-title"><b>Company Talk</b></h5>
              <!-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> -->
              <p class="card-text">Company Talk merupakan rangkaian acara dengan penyampaian materi yang kekinian dan menarik untuk diikuti karena relatable dengan situasi saat ini.</p>
            </div>
            <div class="card-footer" style="background-color: transparent; border-top: none; padding-bottom: 20px;">
              <a href="#" class="card-link text-warning">Info Lebih lanjut</a>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12 mt-2">
          <div class="card program-box">
            <div class="card-body">
              <img src="<?php echo base_url() ?>public/assets/img/mitra/directbox.png" class="img img-fluid" style="width: 20%;" alt="">
              <h5 class="card-title"><b>Career Fair</b></h5>
              <!-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> -->
              <p class="card-text">Layanan yang mempertemukan antara pencari pekerjaan dan pemberi kerja. Untuk cari kerja tanpa melamar bagi pencari kerja, dan cari SDM tanpa iklan bagi perusahaan.</p>
            </div>
            <div class="card-footer" style="background-color: transparent; border-top: none; padding-bottom: 20px;">
              <a href="#" class="card-link text-warning">Info Lebih lanjut</a>
            </div>
          </div>
        </div>
        <div class="col-lg-3  col-md-6 col-sm-12 mt-2">
          <div class="card program-box">
            <div class="card-body">
              <img src="<?php echo base_url() ?>public/assets/img/mitra/teacher.png" class="img img-fluid" style="width: 20%;" alt="">
              <h5 class="card-title"><b>Campus Hiring</b></h5>
              <!-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> -->
              <p class="card-text">Perusahaan memiliki kesempatan untuk memilih talenta terbaik yang ideal untuk mengisi talent pool ataupun langsung bekerja di perusahaan mereka dengan lebih efektif.</p>
            </div>
            <div class="card-footer" style="background-color: transparent; border-top: none; padding-bottom: 20px;">
              <a href="#" class="card-link text-warning">Info Lebih lanjut</a>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section><!-- End Program Section -->

  <!-- ======= Services Section ======= -->

  <!-- ======= Keterangan Section ======= -->
  <section id="keterangan">
    <div class="container">
      <div class="row">
        <div class="col-md-6" style="padding: 45px 82px;">
          <h4 class="tagline text-white">Aplikasi Tracer Study
            <br><span>untuk Mitra Karir</span>
          </h4>
          <p class="sub-tagline">Hadir sebagai fasilitas kepada pengguna lulusan sekolah binaan Pusdiktan. Dapatkan layanan khusus Mitra hanya dengan mengakses Aplikasi Tracer Study Mitra.</p>
          <div class="row my-4">
            <div class="col">
              <ul type="none">
                <li><b><i class="fas fa-briefcase"></i> Posting Lowongan Kerja</b></li>
                <li><b><i class="fas fa-briefcase"></i> Posting Lowongan Magang</b></li>
                <li><b><i class="fas fa-comments"></i> Penjadwalan Company Talk</b></li>
                <li><b><i class="fas fa-building"></i> Pendaftaran Career Fair</b></li>
                <li><b><i class="fas fa-graduation-cap"></i> Penjadwalan Campus Hiring</b></li>
              </ul>
            </div>
          </div>

        </div>
        <div class="col-md-6">
          <div class="d-flex justify-content-center align-items-center">
            <img src="<?php echo base_url() ?>public/assets/img/mitra/detail.PNG" class="img img-fluid" alt="">
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- End Keterangan Section -->
  <section id="langkah2" class="icon-boxes">
    <div class="container">
      <div class="row icon-box">
        <div class="img-langkah2 col-md-6">
          <img src="<?php echo base_url() ?>public/assets/img/mitra/image.png" alt="" class="img img-fluid">
        </div>
        <div class="text-langkah2 col-md-6">
          <h4 class="text-white">Dapatkan Kemudahan Rekruitmen Talent Potensial</h4>
          <p>melalui Aplikasi Tracer Study Mitra Karir</p>
          <a href="<?php echo base_url() ?>Login/mitra" class="btn btn-warning text-white">Login Aplikasi Tracer Study Alumni</a>
        </div>

      </div>
    </div>
  </section>
</main><!-- End #main -->