<div class="card">
    <div class="card-body row">
        <div class="col-lg-3">
            <ul class="list-group" role="tablist" id="profil-tab">
                <li class="list-group-item"><a id="profilmitra-tab" data-toggle="tab" data-target="#profilmitra" type="button" role="tab" aria-controls="profilmitra" aria-selected="true" class="active"><i class="fas fa-user-pen"></i> Profil Mitra</a></li>
                <li class="list-group-item"><a id="akun-tab" data-toggle="tab" data-target="#akun" type="button" role="tab" aria-controls="akun" aria-selected="false"><i class="fas fa-key"></i> Akun</a></li>
            </ul>
        </div>
        <div class="tab-content col-lg-9">
            <div class="tab-pane fade active show" class="" id="profilmitra" role="tabpanel" aria-labelledby="profilmitra-tab">
                <div class="row justify-content-between">
                    <div class="col-lg-3 title">
                        <h3><i class="fa fa-user-pen"></i> Profil</h3>
                    </div>
                </div>
                <form id="form_profil" class="needs-validation" enctype="multipart/form-data">
                    <span class="badge bg-lime text-success mb-3">PROFIL MITRA</span>
                    <input type="hidden" name="id_instansi" class="form-control" value="<?php echo $mitra->id_instansi ?>">
                    <div id="ganti-berkas-mitra" class="form-group <?php echo (isset($mitra->url_logo) ? '' : 'd-none') ?>">
                        <img src="<?php echo (isset($mitra->url_logo)  ? $mitra->url_logo : '') ?>" alt="your image" style="width: 150px;" /><br>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" name="new_upload_mitra" id="new_upload_mitra" class="custom-control-input" onclick="uploadFoto('mitra','url_photo')" value="1">
                            <label class="custom-control-label" for="new_upload_mitra">Ganti Berkas</label>
                        </div>
                    </div>
                    <div id="new-upload-mitra" class="form-group <?php echo (!empty($mitra->url_logo) ? 'd-none' : null) ?>">
                        <label for="url_photo">Foto <b class="text-danger">*</b></label><br>
                        <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                        <input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'mitra')">
                        <span class="file-info-mitra text-muted">Tidak ada berkas yang dipilih</span>
                        <div id="preview-mitra">

                        </div>
                        <div class="hint-block text-muted mt-3">
                            <small>
                                Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                Ukuran file maksimal: <strong>2 MB</strong>
                            </small>
                        </div>
                        <div class="invalid-feedback" for="url_photo">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Nama Perusahaan <b class="text-danger">*</b></label>
                        <input type="text" name="nama" class="form-control alpha-only" value="<?php echo $mitra->nama ?>" placeholder="Masukkan nama perusahaan anda">
                        <div class="invalid-feedback" for="nama">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Jenis <b class="text-danger">*</b></label>
                        <select name="jenis" id="jenis" class="select2 form-control">
                            <option value="">- Pilih Jenis -</option>
                            <option value="Perusahaan swasta/Industri Swasta" <?php echo ($mitra->jenis == 'Perusahaan swasta/Industri Swasta' ? 'selected' : '') ?>>Perusahaan swasta/Industri Swasta</option>
                            <option value="BUMN/Perusahaan Milik Pemerintah" <?php echo ($mitra->jenis == 'BUMN/Perusahaan Milik Pemerintah' ? 'selected' : '') ?>>BUMN/Perusahaan Milik Pemerintah</option>
                            <option value="Pemerintah Daerah/Pusat" <?php echo ($mitra->jenis == 'Pemerintah Daerah/Pusat' ? 'selected' : '') ?>>Pemerintah Daerah/Pusat</option>
                            <option value="Lembaga Pendidikan Negeri" <?php echo ($mitra->jenis == 'Lembaga Pendidikan Negeri' ? 'selected' : '') ?>>Lembaga Pendidikan Negeri</option>
                            <option value="Lembaga Pendidikan Swasta" <?php echo ($mitra->jenis == 'Lembaga Pendidikan Swasta' ? 'selected' : '') ?>>Lembaga Pendidikan Swasta</option>
                            <option value="Lainnya" <?php echo ($mitra->jenis == 'Lainnya' ? 'selected' : '') ?>>Lainnya</option>
                        </select>
                        <div class="invalid-feedback" for="jenis">Wajib diisi</div>
                    </div>
                    <div id="sektor" class="form-group">
                        <label>Sektor <b class="text-danger">*</b></label>
                        <div class="custom-control custom-radio my-2">
                            <input type="radio" class="custom-control-input" id="pertanian" name="sektor" value="Pertanian" <?php echo (isset($mitra->sektor) ? ($mitra->sektor == "Pertanian" ? "checked" : "") : "") ?>>
                            <label class="custom-control-label" for="pertanian">Pertanian</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="non_pertanian" name="sektor" value="Non Pertanian" <?php echo (isset($mitra->sektor) ? ($mitra->sektor == "Non Pertanian" ? "checked" : "") : "") ?>>
                            <label class="custom-control-label" for="non_pertanian">Non Pertanian</label>
                            <div class="invalid-feedback my-2" for="sektor">Wajib dipilih</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Telephone <b class="text-danger">*</b></label>
                        <input type="text" name="telephone" value="<?php echo $mitra->telephone ?>" minlength="11" maxlength="13" class="form-control number-only" placeholder="Masukkan telephone anda">
                        <div class="invalid-feedback" for="telephone">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Website <b class="text-danger">*</b></label>
                        <input type="text" name="website" value="<?php echo $mitra->url_web ?>" class="form-control" placeholder="Masukkan website anda">
                        <div class="invalid-feedback" for="website">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Alamat <b class="text-danger">*</b></label>
                        <textarea name="alamat" id="alamat" class="form-control" style="height: 100px;"><?php echo $mitra->alamat ?></textarea>
                        <div class="invalid-feedback" for="alamat">Wajib diisi</div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary px-5">Simpan</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="akun" role="tabpanel" aria-labelledby="akun-tab">
                <div class="row justify-content-between">
                    <div class="col-lg-3 title">
                        <h3><i class="fa fa-key"></i> Akun</h3>
                    </div>
                </div>
                <form id="form_akun" class="needs-validation">
                    <input type="hidden" name="id_instansi" class="form-control" value="<?php echo $mitra->id_instansi ?>">
                    <div class="form-group">
                        <label>Email <b class="text-danger">*</b></label>
                        <input type="email" name="username" value="<?php echo $mitra->username ?>" class="form-control" placeholder="Masukkan username anda">
                        <div class="invalid-feedback" for="username"></div>
                    </div>

                    <div class="form-group">
                        <label>Password Lama<b class="text-danger">*</b></label>
                        <div class="input-group mb-3">
                            <input type="password" name="password_lama" class="form-control" placeholder="Masukkan password">
                            <a class="input-group-text show-old-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                            <div class="invalid-feedback" for="password_lama">Wajib diisi</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Password Baru<b class="text-danger">*</b></label>
                        <div class="input-group mb-3">
                            <input type="password" name="password" class="form-control" placeholder="Masukkan password">
                            <a class="input-group-text show-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                            <div class="invalid-feedback" for="password">Wajib diisi</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Konfirmasi Password <b class="text-danger">*</b></label>
                        <div class="input-group mb-3">
                            <input type="password" name="konfirmasi_password" class="form-control" placeholder="Masukkan konfirmasi password anda">
                            <a class="input-group-text show-confirm-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                            <div class="invalid-feedback" for="konfirmasi_password">Wajib diisi</div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary px-5">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var site_url = '<?php echo site_url() ?>profil_mitra/index/';

    /**
     * submit profil mitra
     */
    $('#form_profil').on('submit', function(e) {
        e.preventDefault();

        $('#form_profil').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'editMitra',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                swal({
                    title: data.message.title,
                    text: data.message.body,
                    type: 'success',
                    confirmButtonColor: '#396113',
                }, ).then(function() {
                    window.location.href = site_url;
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                dismiss_loading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal({
                            title: response.message.title,
                            html: response.message.body,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    });

    /**
     * submit akun mitra
     */
    $('#form_akun').on('submit', function(e) {
        e.preventDefault();

        $('#form_akun').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'editAkun',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                swal({
                    title: data.message.title,
                    text: data.message.body,
                    type: 'success',
                    confirmButtonColor: '#396113',
                }, ).then(function() {
                    window.location.href = site_url;
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                dismiss_loading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal({
                            title: response.message.title,
                            html: response.message.body,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    });
</script>