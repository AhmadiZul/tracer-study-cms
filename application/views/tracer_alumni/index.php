<link rel="stylesheet" href="<?= base_url('public/vendor/datatables/jquery.dataTables.min.css') ?>">

<div class="card">
    <div class="card-body">
        <table id="example" class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Jenis Survey</th>
                    <th>Link</th>
                </tr>
            </thead>
            <tbody id="resultBody">
            </tbody>
        </table>
    </div>
</div>

<script src="<?= base_url('public/vendor/datatables/datatables.min.js'); ?>"></script>
<script type="text/javascript">
    var site_url = '<?php echo site_url() ?>tracer_alumni/index/';
    var kuesioner_url = '<?php echo site_url() ?>tracer/index/';
    var id_alumni = '<?php echo $this->session->userdata("t_alumniId") ?>';

    function loadTabelALumni() {
        $('.loader').removeClass('d-none');
        $('#result').addClass('d-none');
        $.ajax({
            url: site_url + "getDaftarKuesioner",
            dataType: 'json',
            type: 'POST',
            success: function(data) {
                let tbody = '';
                console.log(data)
                if (data['status']) {
                    let iteration = 0;
                    data['isi'].forEach(function(isi) {
                        iteration += 1;
                        link = '<a href="'+ kuesioner_url + 'kuesioner?id_alumni=' + id_alumni + '&s_id='+isi['id_survey']+'">Link kuesioner</a>';
                        if (data['status_data'] != 1) {
                            link = '<span class="text-danger">Belum tervalidasi</span>';
                        }
                        if (isi['id_answer'] != null) {
                            link = '<a class="text-info" href="'+ kuesioner_url + 'cetakBukti?kode=' + id_alumni + '&s_id='+isi['id_survey']+'">Cetak bukti</a>';
                        }
                        tbody += availableData(iteration, isi['jenis_survey'], link);
                    });

                    $('#totalRow').text(data['jumlah']);
                } else if (!data['status']) {
                    tbody = `
                            <tr class="odd">
                            <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                            </tr>`;
                } else {
                    tbody = `
                            <tr class="odd">
                                <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                                </tr>`;
                }

                $('.loader').addClass('d-none');
                $('#result').removeClass('d-none');

                $('#resultBody').html(null);
                $('#resultBody').append(tbody);

                $('#example').DataTable({
                    pageLength: 30,
                    lengthMenu: [
                        30,
                        60,
                        90,
                    ],
                });
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    };

    function availableData(no, jenis_survey, link) {
        return `
            <tr>
                <td>${no}</td>
                <td>${jenis_survey}</td>
                <td>${link}</td>
            </tr>`;
    }
    $(document).ready(function(e) {
        loadTabelALumni();
    });
</script>