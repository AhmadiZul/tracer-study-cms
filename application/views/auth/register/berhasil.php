<div class="alumni">
<section id="logo" class="logo">
    <div class="card">
        <div class="card-body">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 align-self-center">
            <div class="text-center">
                <a href="<?php echo base_url() ?>">
                    <img class="rounded mb-3" src="<?php echo base_url() ?>public/assets/img/login/Logo.png" width="145" height="80">
                </a>
            </div>
        </div>
        </div>
    </div>
</section>

<section id="login">
    <div class="row vw-100 vh-100 px-3">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 align-self-center">
            <div class="top-border"></div>
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">Registrasi Alumni Telah Berhasil</h2>
                    <hr>
                    <p class="card-text">Terima kasih telah melakukan Registrasi Alumni. Mohon untuk melakukan pengecekan email secara berkala. Administrator Universitas akan mengirimkan notifikasi verifikasi di email Anda.</p>
                    <div class="form-group text-center mb-4">
						<div class="row">
							<div class="col-lg-6">
								<a href="<?php echo base_url() ?>" class="btn btn-light-danger form-control">Beranda</a>
							</div>
							<div class="col-lg-6">
								<a href="<?php echo base_url() ?>Login" class="btn btn-secondary form-control">LOGIN</a>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>