<style>
    .btn-light-back {
        background-color: #FFE8DE;
        margin-right: 10px;
    }

    .btn-light-back:hover {
        background-color: #FF844B;
    }

    .bg-lime {
        background-color: #E8F0E0;
    }
</style>
<section id="login">
    <div class="row justify-content-center vw-100">
        <div class="col-sm-12 col-lg-8 col-md-8 mx-0 px-0">
            <div class="text-center">
                <img class="rounded mb-3" src="<?php echo base_url() ?>public/assets/img/login/logo-login.png">
            </div>
            <div class="bg-light-green top-border"></div>
            <div class="card">
                <div class="card-body">
                    <a href="<?php echo base_url() ?>Login" role="button" class="d-inline btn btn-light-danger btn-light-back mr-md-3 form-control"><i class="fas fa-chevron-left"></i> Kembali</a>
                    <h2 class="d-inline card-title mx-3"><?php echo $title ?></h2>
                    <hr>
                    <p class="card-text">Silahkan isi formulir berikut menggunakan data yang sebenar-benarnya</p>
                    <form id="form_registrasi_mitra">
                        <div class="badge bg-lime text-success my-3">PROFIL MITRA</div>
                        <div class="form-group">
                            <label>Nama Mitra<b class="text-danger">*</b></label>
                            <input type="text" name="nama" class="form-control" maxlength="100" placeholder="Masukkan nama lengkap anda" >
                            <div class="invalid-feedback" for="nama"></div>
                        </div>
                        <div class="form-group">
                            <label>Jenis <b class="text-danger">*</b></label>
                            <select name="jenis" id="jenis" class="select2 form-control" >
                                <option value="">- Pilih Jenis -</option>
                                <option value="Perusahaan swasta/Industri Swasta">Perusahaan swasta/Industri Swasta</option>
                                <option value="BUMN/Perusahaan Milik Pemerintah">BUMN/Perusahaan Milik Pemerintah</option>
                                <option value="Pemerintah Daerah/Pusat">Pemerintah Daerah/Pusat</option>
                                <option value="Lembaga Pendidikan Negeri">Lembaga Pendidikan Negeri</option>
                                <option value="Lembaga Pendidikan Swasta">Lembaga Pendidikan Swasta</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                            <div class="invalid-feedback" for="jenis"></div>
                        </div>
                        <div class="form-group">
                            <label>Sektor <b class="text-danger">*</b></label>
                            <div class="custom-control custom-radio my-2">
                                <input type="radio" class="custom-control-input" id="pertanian" name="sektor" value="Pertanian" >
                                <label class="custom-control-label" for="pertanian">Pertanian</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="non_pertanian" name="sektor" value="Non Pertanian" >
                                <label class="custom-control-label" for="non_pertanian">Non Pertanian</label>
                                <div class="invalid-feedback my-2" for="sektor">Wajib dipilih</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Telephone <b class="text-danger">*</b></label>
                            <input type="text" name="telephone" minlength="11" maxlength="13" class="form-control number-only" placeholder="Masukkan telephone anda" >
                            <div class="invalid-feedback" for="telephone"></div>
                        </div>
                        <div class="form-group">
                            <label>Website <b class="text-danger">*</b></label>
                            <input type="text" name="website" class="form-control" placeholder="Masukkan website anda" >
                            <div class="invalid-feedback" for="website"></div>
                        </div>
                        <div class="form-group">
                            <label>Detail Alamat <b class="text-danger">*</b></label>
                            <textarea name="alamat" id="alamat" class="form-control"  style="height: 100px;"></textarea>
                            <div class="invalid-feedback" for="alamat"></div>
                        </div>
                        <div class="form-group">
                            <label for="url_photo">Upload Logo <b class="text-danger">*</b></label>
                            <div id="new-upload-logo">
                                <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                                <input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo')">
                                <span class="file-info-logo text-muted">Tidak ada berkas yang dipilih</span>
                                <div id="preview-logo">

                                </div>
                                <div class="hint-block text-muted mt-3">
                                    <small>
                                        Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                        Ukuran file maksimal: <strong>2 MB</strong>
                                    </small>
                                </div>
                                <div class="invalid-feedback" for="url_photo"></div>
                            </div>
                        </div>
                        <div class="badge bg-lime text-success my-3">BUAT AKUN</div>
                        <div class="form-group">
                            <label>Email <b class="text-danger">*</b></label>
                            <input type="email" name="email" class="form-control" placeholder="Masukkan email anda" >
                            <div class="invalid-feedback" for="email"></div>
                        </div>

                        <div class="form-group">
                            <label>Password <b class="text-danger">*</b></label>
                            <div class="input-group mb-3">
                                <input type="password" name="password" class="form-control" placeholder="Masukkan password" >
                                <a class="input-group-text show-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                                <div class="invalid-feedback" for="password"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Konfirmasi Password <b class="text-danger">*</b></label>
                            <div class="input-group mb-3">
                                <input type="password" name="konfirmasi_password" class="form-control" placeholder="Masukkan konfirmasi password anda" >
                                <a class="input-group-text show-confirm-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                                <div class="invalid-feedback" for="konfirmasi_password"></div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary btn-lg">SUBMIT</button>
                        </div>
                        <div class="d-flex justify-content-center">
                            <small class="mt-3">Sudah punya akun? <a href="<?php echo site_url('Login') ?>" class="buat-akun"><b>Klik Disini</b></a></small>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var site_url = '<?php echo site_url() ?>auth/register/mitra/';
</script>