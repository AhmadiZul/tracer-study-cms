<style>
	.btn-light-back {
		background-color: #FFFF;
		margin-right: 10px;
	}

	.btn-light-back:hover {
		background-color: #FF844B;
	}

	.bg-lime {
		background-color: #BD1F28;
	}
</style>
<section id="logo" class="logo">
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <a href="<?php echo base_url() ?>">
                    <img class="rounded mb-3" src="<?php echo URL_FILE . $logo_utama->url_file ?>" style="margin-top:15px;" width="relative" height="80">
                </a>
            </div>
        </div>
    </div>
</section>
<section id="login" class="vh-100">
	<div class="row justify-content-center vh-100">
		<div class="col-xs-12 col-lg-8 col-md-8 mx-0 px-0">
				<div class="card">
					<div class="card-body">
						<a href="<?php echo base_url() ?>Login/alumni" role="button" class="d-inline btn btn-light-tema btn-light-back mr-md-3"><i class="fas fa-chevron-left"></i> Kembali</a>
						<h2 class="d-inline card-title"><?php echo $title ?></h2>
					</div>
				</div>
			<div class="top-border"></div>
			<div class="card">
				<div class="card-body">
					<div class="alert alert-tema">
						<i class="fas fa-info-circle"></i> Isikan “Profil Alumni” jika anda adalah alumni .
					</div>
					<form id="form_registrasi" class="needs-validation" novalidate>
						<div class="badge badge-tema text-light">IDENTITAS DIRI</div>
						<div class="form-group">
							<label for="nama">Nama Lengkap <b class="text-danger">*</b></label>
							<input type="text" id="nama" name="nama" class="form-control alpha-only" placeholder="Masukkan nama lengkap anda" maxlength="60" required>
							<div class="invalid-feedback" for="nama">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="nim">Nomor Induk Mahasiswa/Siswa (NIM/NIRM) <b class="text-danger">*</b></label>
							<input type="text" id="nim" name="nim" class="form-control number-only" maxlength="20" placeholder="Masukkan NIM/NIRM anda" required>
							<div class="invalid-feedback" for="nim">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="nik">Nomor Induk Kependudukan (NIK) <b class="text-danger">*</b></label>
							<input type="text" id="nik" name="nik" maxlength="16" minlength="16" class="form-control number-only" placeholder="Masukkan NIK anda" required>
							<div class="invalid-feedback" for="nik">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="no_hp">Nomor HP <b class="text-danger">*</b></label>
							<input type="text" id="no_hp" name="no_hp" minlength="11" maxlength="13" class="form-control number-only" placeholder="Masukkan nomor hp anda" required>
							<div class="invalid-feedback" for="no_hp">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="tempat_lahir">Tempat Lahir <b class="text-danger">*</b></label>
							<input type="text" id="tempat_lahir" name="tempat_lahir" class="form-control alpha-only" placeholder="Masukkan Tempat Lahir Anda" maxlength="50" required>
							<div class="invalid-feedback" for="tempat_lahir">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="tgl_lahir">Tanggal Lahir <b class="text-danger">*</b></label>
							<input type="text" id="tgl_lahir" name="tgl_lahir" class="form-control datepicker-lahir" placeholder="Masukkan Tanggal Lahir Anda" required>
							<div class="invalid-feedback" for="tgl_lahir">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="id_agama">Agama <b class="text-danger">*</b></label>
							<select name="id_agama" id="id_agama" class="form-control select2" required>
								<option value="">- Pilih Agama -</option>
								<?php foreach ($agama as $key => $value) { ?>
									<option value="<?php echo $key ?>"><?php echo $value ?></option>
								<?php } ?>
							</select>
							<div class="invalid-feedback" for="id_agama">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="jenis_kelamin">Jenis Kelamin <b class="text-danger">*</b></label>
							<div class="custom-control custom-radio my-2">
								<input type="radio" class="custom-control-input" id="laki_laki" name="jenis_kelamin" value="LAKI-LAKI" required>
								<label class="custom-control-label" for="laki_laki">Laki - Laki</label>
							</div>
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input" id="perempuan" name="jenis_kelamin" value="PEREMPUAN" required>
								<label class="custom-control-label" for="perempuan">Perempuan</label>
								<div class="invalid-feedback my-2" for="jenis_kelamin">Wajib dipilih</div>
							</div>
						</div>
						<div class="form-group">
							<label for="jalur_masuk">Jalur Masuk <b class="text-danger">*</b></label>
							<div class="custom-control custom-radio my-2">
								<input type="radio" class="custom-control-input" id="reguler" name="jalur_masuk" value="Reguler" required>
								<label class="custom-control-label" for="reguler">Reguler</label>
							</div>
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input" id="rpl" name="jalur_masuk" value="RPL" required>
								<label class="custom-control-label" for="rpl">RPL</label>
								<div class="invalid-feedback my-2" for="jalur_masuk">Wajib dipilih</div>
							</div>
						</div>
						<div class="form-group">
							<label for="kode_provinsi">Provinsi <b class="text-danger">*</b></label>
							<select name="kode_provinsi" id="kode_provinsi" class="form-control select2" required>
								<option value="">- Pilih Provinsi -</option>
								<?php foreach ($provinsi as $key => $value) { ?>
									<option value="<?php echo $key ?>"><?php echo $value ?></option>
								<?php } ?>
							</select>
							<div class="invalid-feedback" for="kode_provinsi">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="kode_kabupaten">Kabupaten/Kota <b class="text-danger">*</b></label>
							<select name="kode_kabupaten" id="kode_kabupaten" class="select2 form-control" required>
								<option value="">- Pilih Kabupaten/Kota -</option>
								<?php foreach ($kab_kota as $key => $value) { ?>
									<option value="<?php echo $key ?>"><?php echo $value ?></option>
								<?php } ?>
							</select>
							<div class="invalid-feedback" for="kode_kabupaten">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="kode_kecamatan">Kecamatan <b class="text-danger">*</b></label>
							<select name="kode_kecamatan" id="kode_kecamatan" class="select2 form-control" required>
								<option value="">- Pilih Kecamatan -</option>
							</select>
							<div class="invalid-feedback" for="kode_kecamatan">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="alamat">Detail Alamat <b class="text-danger">*</b></label>
							<textarea name="alamat" id="alamat" class="form-control" required style="height: 100px;" maxlength="255"></textarea>
							<div class="invalid-feedback" for="alamat">Wajib diisi</div>
						</div>
						<hr>
						<div class="badge badge-tema text-light">PROFIL ALUMNI</div>
						<div class="form-group">
							<label for="tahun_lulus">Tahun Kelulusan <b class="text-danger">*</b></label>
							<select name="tahun_lulus" id="tahun_lulus" class="form-control select2" required>
								<option value="">- Pilih Tahun Kelulusan -</option>
								<?php foreach ($tahun as $key => $value) { ?>
									<option value="<?php echo $key ?>"><?php echo $value ?></option>
								<?php } ?>
							</select>
							<div class="invalid-feedback" for="tahun_lulus">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="perguruan_tinggi">Jenjang Pendidikan <b class="text-danger">*</b></label>
							<select name="perguruan_tinggi" id="perguruan_tinggi" class="form-select select2" required>
								<option value="">- Pilih Jenjang Pendidikan -</option>
								<?php foreach ($jenjang as $key => $value) { ?>
									<option value="<?php echo $key ?>"><?php echo $value ?></option>
								<?php } ?>
							</select>
							<div class="invalid-feedback" for="perguruan_tinggi">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="kode_prodi">Prodi/Jurusan <b class="text-danger">*</b></label>
							<select name="kode_prodi" id="kode_prodi" class="select2 form-control" required>
								<option value="">- Pilih Prodi/Jurusan -</option>
							</select>
							<div class="invalid-feedback" for="kode_prodi">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="ipk_terakhir">IPK Terakhir <b class="text-danger">*</b></label>
							<input type="text" id="ipk_terakhir" name="ipk_terakhir" class="form-control ipk" placeholder="Masukkan ipk terakhir anda" required>
							<div class="invalid-feedback" for="ipk_terakhir">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="judul_skripsi">Judul Skripsi <b class="text-danger">*</b></label>
							<textarea name="judul_skripsi" id="judul_skripsi" class="form-control" required style="height: 100px;" maxlength="1000"></textarea>
							<div class="invalid-feedback" for="judul_skripsi">Wajib diisi</div>
						</div>
						<div class="form-group">
							<label for="status_alumni">Status Alumni <b class="text-danger">*</b></label>
							<select name="status_alumni" id="status_alumni" class="form-select select2" required>
								<option value="">- Pilih Status Alumni -</option>
								<?php foreach ($status_alumni as $key => $value) { ?>
									<option value="<?php echo $key ?>"><?php echo $value ?></option>
								<?php } ?>
							</select>
							<div class="invalid-feedback" for="status_alumni">Wajib diisi</div>
						</div>
						<div id="div-instansi" class="form-group d-none">
							<label id="instansi" for="nama_instansi">Nama Instansi <b class="text-danger">*</b></label>
							<input type="text" name="nama_instansi" class="form-control" placeholder="Masukkan nama instansi anda" maxlength="255">
							<div class="invalid-feedback" for="nama_instansi">Wajib diisi</div>
						</div>
						<div id="div-tgl-mulai" class="form-group d-none">
							<label for="tgl_mulai">Tanggal Masuk <b class="text-danger">*</b></label>
							<input type="text" id="tgl_mulai" name="tgl_mulai" class="form-control datepicker2">
							<div class="invalid-feedback" for="tgl_mulai">Wajib diisi</div>
						</div>
						<div id="div-jabatan" class="form-group d-none">
							<label for="jabatan">Jabatan <b class="text-danger">*</b></label>
							<input type="text" id="jabatan" name="jabatan" class="form-control" placeholder="Masukkan jabatan anda" maxlength="255">
							<div class="invalid-feedback" for="jabatan">Wajib diisi</div>
						</div>
						<div id="div-prodi" class="form-group d-none">
							<label for="prodi">Prodi <b class="text-danger">*</b></label>
							<input type="text" id="prodi" name="prodi" class="form-control" placeholder="Masukkan prodi anda" maxlength="50">
							<div class="invalid-feedback" for="prodi">Wajib diisi</div>
						</div>
						<div id="div-sektor" class="form-group d-none">
							<label for="sektor">Sektor <b class="text-danger">*</b></label>
							<div class="custom-control custom-radio my-2">
								<input type="radio" class="custom-control-input" id="pertanian" name="sektor" value="Pertanian">
								<label class="custom-control-label" for="pertanian">Pertanian</label>
							</div>
							<div class="custom-control custom-radio">
								<input type="radio" class="custom-control-input" id="non_pertanian" name="sektor" value="Non Pertanian">
								<label class="custom-control-label" for="non_pertanian">Non Pertanian</label>
								<div class="invalid-feedback my-2" for="sektor">Wajib dipilih</div>
							</div>
						</div>
						<div id="div-instansi-provinsi" class="form-group d-none">
							<label for="instansi_kode_prov">Provinsi <b class="text-danger">*</b></label>
							<select name="instansi_kode_prov" id="instansi_kode_prov" class="form-control select2">
								<option value="">- Pilih Provinsi -</option>
								<?php foreach ($provinsi as $key => $value) { ?>
									<option value="<?php echo $key ?>"><?php echo $value ?></option>
								<?php } ?>
							</select>
							<div class="invalid-feedback" for="instansi_kode_prov">Wajib diisi</div>
						</div>
						<div id="div-instansi-kab-kota" class="form-group d-none">
							<label for="instansi_kode_kab">Kabupaten/Kota <b class="text-danger">*</b></label>
							<select name="instansi_kode_kab" id="instansi_kode_kab" class="select2 form-control">
								<option value="">- Pilih Kabupaten/Kota -</option>
								<?php foreach ($kab_kota as $key => $value) { ?>
									<option value="<?php echo $key ?>"><?php echo $value ?></option>
								<?php } ?>
							</select>
							<div class="invalid-feedback" for="instansi_kode_kab">Wajib diisi</div>
						</div>
						<div id="div-instansi-kecamatan" class="form-group d-none">
							<label for="instansi_kode_kec">Kecamatan <b class="text-danger">*</b></label>
							<select name="instansi_kode_kec" id="instansi_kode_kec" class="select2 form-control">
								<option value="">- Pilih Kecamatan -</option>
							</select>
							<div class="invalid-feedback" for="instansi_kode_kec">Wajib diisi</div>
						</div>
						<div id="div-alamat" class="form-group d-none">
							<label for="instansi_alamat">Detail Alamat <b class="text-danger">*</b></label>
							<textarea name="instansi_alamat" id="instansi_alamat" class="form-control" style="height: 100px;" maxlength="255"></textarea>
							<div class="invalid-feedback" for="instansi_alamat">Wajib diisi</div>
						</div>
						<hr>
						<div class="badge badge-tema text-light">BUAT AKUN</div>
						<div class="form-group">
							<label for="email">Email <b class="text-danger">*</b></label>
							<input type="email" id="email" name="email" class="form-control" placeholder="Masukkan email anda" maxlength="50" required>
							<div class="invalid-feedback" for="email">Email tidak sesuai</div>
						</div>

						<div class="form-group">
							<label for="password">Password <b class="text-danger">*</b></label>
							<div class="input-group mb-3">
								<input type="password" id="password" name="password" class="form-control" placeholder="Masukkan password" required>
								<a class="input-group-text show-password"><i class="fas fa-eye-slash text-secondary"></i></a>
								<div class="invalid-feedback" for="password">Wajib diisi</div>
							</div>
						</div>
						<div class="form-group">
							<label for="konfirmasi_password">Konfirmasi Password <b class="text-danger">*</b></label>
							<div class="input-group mb-3">
								<input type="password" id="konfirmasi_password" name="konfirmasi_password" class="form-control" placeholder="Masukkan konfirmasi password anda" required>
								<a class="input-group-text show-confirm-password"><i class="fas fa-eye-slash text-secondary"></i></a>
								<div class="invalid-feedback" for="konfirmasi_password">Wajib diisi</div>
							</div>
						</div>
						<div class="d-flex justify-content-center">
							<button type="submit" class="btn btn-warning btn-lg">SUBMIT</button>
						</div>
						<div class="d-flex justify-content-center">
							<small class="mt-3">Sudah punya akun? <a href="<?php echo site_url('Login/alumni') ?>" class="buat-akun"><b>Klik Disini</b></a></small>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	var site_url = '<?php echo site_url() ?>register/index/';
</script>

