<section id="login">
	<div class="row" style="margin-top: 20px;margin-bottom:20px;">
		<div class="col-md-6" style="margin-top: 40px;margin-bottom:40px;">
			<div class="text-center">
				<a href="<?php echo base_url() ?>">
					<img class="rounded mb-3" src="<?php echo URL_FILE. $logo_utama->url_file ?>" style="margin-top: 15px;" width="relative" height="80">
				</a>
			</div>
			<div class="card" style="margin-left: 90px;margin-right:90px;margin-top:30px;">
				<div class="card-body">
				<?php if ($this->session->flashdata('errorMessage')) : ?>
								<div class="alert alert-danger alert-has-icon">
									<div class="alert-icon"><i class="far fa-lightbulb"></i></div>
									<div class="alert-body">
										<div class="alert-title">Mohon Maaf !</div>
										<p class="text-white"><?php echo $this->session->flashdata('errorMessage') ?></p>
									</div>
								</div>
					<?php endif; ?>
					<?php echo form_open('/', array('id' => 'login-form')); ?>
					<h3 class="card-title">Login<strong class="text-tema"> TRACER STUDY</strong></h3>
					<p class="card-text">Silahkan Login menggunakan Username dan Password</p>
					<input type="hidden" name="role" class="form-control" value="1">
					<?php
						if ($message) {
							echo '<div class="alert alert-danger" role="alert">'.$message.'</div>';
						}
					?>
					<div class="form-group">
						<label>Username</label>
                        <div class="input-group mb-03">
                            <a class="input-group-text"><i class="fas fa-user text-tema"></i></a>
                            <input type="text" name="username" class="form-control" placeholder="Masukkan username" required>
                        </div>
					</div>
					<div class="form-group">
						<label>Password</label>
						<div class="input-group mb-3">
                            <a class="input-group-text"><i class="fas fa-lock text-tema"></i></a>
							<input type="password" name="password" class="form-control" placeholder="Masukkan password" required>
							<a class="input-group-text show-password"><i class="fas fa-eye-slash text-secondary"></i></a>
						</div>
						<div class="form-group text-end my-2 right">
							<small>Lupa kata sandi? <a href="<?php echo base_url() ?>index.php/forgot/emailverif" class="buat-akun">Klik disini</a></small>
						</div>
					</div>
					<div class="form-group">
                    <div class="d-flex justify-content-around align-items-center">
                        <div id="container-captcha"><?php echo $captcha?></div>
                        <button class="btn bg-light-red rounded-sm py-1 px-3 text-tema" type="button" onclick="loadCaptcha()"><i class="fas fa-sync"></i></button>
                    </div>
                </div>
                <div class="form-group">
                    <input class="form-control" type="number" name="captcha" placeholder="Type the Captcha" required>
                </div>
					<div class="form-group text-center mb-2">
						<div class="row">
							<div class="col-lg-6">
								<a href="<?php echo base_url() ?>Login" class="btn btn-light-tema form-control">KEMBALI</a>
							</div>
							<div class="col-lg-6">
								<button name="login-button" value="login-button" type="submit" class="btn btn-tema form-control">
									<span>MASUK</span>
								</button>
							</div>
						</div>
					</div>
					<div class="form-group text-center mb-2">
						<small>Belum mempunyai akun? <a href="<?php echo base_url() ?>register/index" class="buat-akun">Buat akun</a></small>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<img src="<?php echo base_url() ?>public/assets/img/login/frame_login.png" style="width:97%;">
		</div>
	</div>
</section>
<script>
    var site_url = '<?php echo site_url()?>';
    // $(function() {
    //     loadCaptcha();
    // })
    function loadCaptcha() {
        $.ajax({
            type: 'ajax',
            url: site_url + '/Login/fetchCaptha',
            dataType: 'json',
            success: function(response) {
                console.log(response);
                $('#container-captcha').html(response.data);
            },
            error: function(xmlresponse) {
                console.log(xmlresponse);
            },
        });
    }
</script>