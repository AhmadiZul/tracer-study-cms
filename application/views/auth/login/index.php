<div class="alumni">
<section id="logo" class="logo">
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <a href="<?php echo base_url() ?>">
                    <img class="rounded mb-3" src="<?php echo base_url() ?>public/assets/img/login/Logo.png" width="145" height="80">
                </a>
            </div>
        </div>
    </div>
</section>

<section id="login" class="vh-100">
	<div class="container vh-100" >
		<div class="row justify-content-center vh-100">
			<div class="col-lg-6 col-xs-12 align-self-center">
				<div class="card">
					<div class="card-body">
						<h2 class="card-title">Mulai Login Sebagai</h2>
						<hr>
						<p class="card-text">Pilih salah satu kondisi Anda sekarang untuk berpartisipasi di Tracer Study</p>
						<a href="<?php echo base_url() ?>Login/alumni" class="btn btn-alumni form-control mb-3">Alumni</a>
						<a href="<?php echo base_url() ?>Login/mitra" class="btn btn-industri form-control">Dunia Usaha dan Industri</a>
					</div>
				</div>
				<div class="copyright" style="text-align: center;">
			        &copy; 2022 Layanan pengelola di bidang Teknologi Informasi - build by <strong><span>PT Arkatama Multisolusindo</span></strong>.
			    </div>
			    <div id="preloader"></div>
			</div>
		</div>
	</div>
</section>
</div>