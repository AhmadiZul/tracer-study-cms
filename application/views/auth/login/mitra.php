<section id="login">
	<div class="row mx-0 vw-100">
		<div class="col-sm-12 col-md-7 col-lg-7 align-self-center">
			<div class="text-center">
				<a href="<?php echo base_url() ?>">
					<img class="rounded mb-3" src="<?php echo base_url() ?>public/assets/img/login/logo-login.png">
				</a>
			</div>
			<div class="col-sm-12 col-md-8 offset-md-2 bg-light-green top-border"></div>
			<div class="card col-sm-12 col-md-8 offset-md-2">
				<div class="card-body">
				<?php if ($this->session->flashdata('errorMessage')) : ?>
								<div class="alert alert-danger alert-has-icon">
									<div class="alert-icon"><i class="far fa-lightbulb"></i></div>
									<div class="alert-body">
										<div class="alert-title">Mohon Maaf !</div>
										<p class="text-white"><?php echo $this->session->flashdata('errorMessage') ?></p>
									</div>
								</div>
					<?php endif; ?>
					<?php echo form_open('/', array('id' => 'login-form')); ?>
					<h2 class="card-title">Login</h2>
					<p class="card-text">Silahkan Login menggunakan Username dan Password</p>
					<input type="hidden" name="role" class="form-control" value="0">
					<?php
						if ($message) {
							echo '<div class="alert alert-danger" role="alert">'.$message.'</div>';
						}
					?>
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="username" class="form-control" placeholder="Masukkan username">
					</div>
					<div class="form-group">
						<label>Password</label>
						<div class="input-group mb-3">
							<input type="password" name="password" class="form-control" placeholder="Masukkan password">
							<a class="input-group-text show-password"><i class="fas fa-eye-slash text-secondary"></i></a>
						</div>
					</div>
					<div class="form-group">
                    <div class="d-flex justify-content-around align-items-center">
                        <div id="container-captcha"><?php echo $captcha?></div>
                        <button class="btn bg-light-red rounded-sm py-1 px-3 text-danger" type="button" onclick="loadCaptcha()"><i class="fas fa-sync"></i></button>
                    </div>
                </div>
                <div class="form-group">
                    <input class="form-control" type="number" name="captcha" placeholder="Type the Captcha" required>
                </div>
					<div class="form-group text-end my-2">
						<small>Lupa kata sandi? <a href="<?php echo base_url() ?>auth/register" class="buat-akun">Klik disini</a></small>
					</div>
					<div class="form-group text-center mb-2">
						<div class="row">
							<div class="col-lg-6">
								<a href="<?php echo base_url() ?>Login" class="btn btn-light-danger form-control">KEMBALI</a>
							</div>
							<div class="col-lg-6">
								<button name="login-button" value="login-button" type="submit" class="btn btn-secondary form-control">
									<span>LOGIN</span>
								</button>
							</div>
						</div>
					</div>
					<div class="form-group text-center mb-2">
						<small>Belum mempunyai akun? <a href="<?php echo base_url() ?>auth/register/mitra" class="buat-akun">Buat akun</a></small>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-5 col-lg-5 text-end d-none d-md-block d-lg-block px-0">
			<img class="img-fluid vw-100 vh-100" src="<?php echo base_url() ?>public/assets/img/mitra/login/banner-login.png">
		</div>
	</div>
</section>

<script>
    var site_url = '<?php echo site_url()?>';
    // $(function() {
    //     loadCaptcha();
    // })
    function loadCaptcha() {
        $.ajax({
            type: 'ajax',
            url: site_url + '/Login/fetchCaptha',
            dataType: 'json',
            success: function(response) {
                console.log(response);
                $('#container-captcha').html(response.data);
            },
            error: function(xmlresponse) {
                console.log(xmlresponse);
            },
        });
    }
</script>