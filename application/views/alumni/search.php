<?php
if (isset($output->css_files)) {
  foreach ($output->css_files as $file) {
    echo '<link type="text/css" rel="stylesheet" href="' . $file . '"/>';
  }
}
?>

<script src="<?php echo base_url() . 'public/vendor/highchart/highchart.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/exporting.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/export-data.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/accessibility.js'; ?>"></script>


<main id="main" style="overflow-x: hidden;">
  <section id="hero-cek">
    <div class="bg">
      <div class="d-flex justify-content-center align-items-start" style="height:200px;">
        <div class="card text-center bg-transparent border-0 pt-3">
          <h2 class="animate__animated animate__fadeInDown pt-5">Data Alumni</h2>
        </div>
      </div>
      <div class="d-flex justify-content-center align-items-start" style="height:200px;">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <form action="" class="form-inline">
                  <div class="col-md-4">
                    <label for="filter_tahun">Tahun Lulusan</label>
                    <select name="filter_tahun" id="filter_tahun" class="form-control select2">
                      <option value="all">Semua Tahun Kelulusan</option>
                      <?php foreach ($tahun_lulus as $key => $value) { ?>
                        <option value="<?php echo $key ?>"><?php echo $value ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <label for="filter_fakultas">Pilih Fakultas</label>
                    <select name="filter_fakultas" id="filter_fakultas" class="form-control select2">
                      <option value="">Fakultas</option>
                      <?php foreach ($fakultas as $key => $value) { ?>
                        <option value="<?php echo $key ?>"><?php echo $value ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <label for="filter_prodi">Pilih Prodi</label>
                    <select name="filter_prodi" id="filter_prodi" class="form-control select2">
                      <option value="">Program Studi</option>
                    </select>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- ======= Services Section ======= -->
  <section id="services" class="services bg-light">
    <div class="col-md-12 p-5">
      <div class="row">
        <div class="col-md-12">
          <div class="rounded py-4 px-4 shadow-sm bg-white my-4">
            <h3 class="font-weight-bold title-text-header text-center">Sebaran Data Alumni</h3>
            <div class="row">
              <div class="col-md-4 mt-2">
                <div class="rounded shadow-sm py-4 px-3 h-100 text-white card-alumni" style="background-color: #E95B5D;">
                  <div class="col-6">
                    <b class="font-weight-bold"> <img src="<?= base_url('public/assets/img/data_alumni/jumlah_alumni.png') ?>" alt="" srcset=""> Jumlah Alumni</b>
                  </div>
                  <div class="col-2">
                    <h4 class="font-weight-bold mt-4 total-data">0</h4>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mt-2">
                <div class="rounded shadow-sm py-4 px-3 h-100 text-white card-alumni-tertelusur" style="background-color:#C9D851">
                  <div class="col-6">
                    <img src="<?= base_url('public/assets/img/data_alumni/alumni_yang_diketahui.png') ?>" alt="" srcset="">
                  </div>
                  <div class="col">
                    <b class="font-weight-bold"> Alumni yang Diketahui Statusnya</b>
                  </div>
                  <div class="col-2">
                    <h4 class="font-weight-bold mt-4 total-data">0</h4>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mt-2">
                <div class="rounded shadow-sm py-4 px-3 h-100 text-white card-prosentase-alumni" style="background-color: #5BA5E9">
                  <div class="col">
                    <img src="<?= base_url('public/assets/img/data_alumni/prosentase_diketahui_statusnya.png') ?>">
                  </div>
                  <div class="col">
                    <b class="font-weight-bold"> Prosentase Alumni yang Diketahui Statusnya</b>
                  </div>
                  <div class="col-2">
                    <h4 class="font-weight-bold mt-4 total-data">0</h4>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mt-4">
              <div class="col-md-4 mt-2">
                <div class="rounded shadow-sm py-4 px-3 h-100 text-white card-alumni-bekerja" style="background-color:#E95B5D">
                  <div class="col-6">
                    <img src="<?= base_url('public/assets/img/data_alumni/bekerja_wiraswasta.png') ?>">
                  </div>
                  <div class="col">
                    <b class="font-weight-bold">Bekerja, Wiraswasta dan Melanjutkan Studi</b>
                  </div>
                  <div class="col-2">
                    <h4 class="font-weight-bold mt-4 total-data">0</h4>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mt-2">
                <div class="rounded shadow-sm py-4 px-3 h-100 text-white card-alumni-iddle" style="background-color: #C9D851">
                  <div class="col-6">
                    <img src="<?= base_url('public/assets/img/data_alumni/belum_memungkinkan.png') ?>">
                  </div>
                  <div class="col">
                    <b class="font-weight-bold">Belum Memungkinkan Bekerja</b>
                  </div>
                  <div class="col-2">
                    <h4 class="font-weight-bold mt-4 total-data">0</h4>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mt-2">
                <div class="rounded shadow-sm py-4 px-3 h-100 text-white card-prosentase-iku" style="background-color:#5BA5E9">
                  <div class="col">
                    <img src="<?= base_url('public/assets/img/data_alumni/prosentase_berkegiatan.png') ?>">
                  </div>
                  <div class="col">
                    <b class="font-weight-bold">Presentase Alumni Mempunyai Kegiatan</b>
                  </div>
                  <div class="col-2">
                    <h4 class="font-weight-bold mt-4 total-data">0</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div id="gStatus_alumni_kelamin"></div>
        </div>
        <div class="col-md-6">
          <div id="gStatus_alumni_sektor"></div>
        </div>
      </div>
    </div>
  </section><!-- End Services Section -->
</main><!-- End #main -->

<?php
echo '<script>';
if (isset($scripts) && is_array($scripts)) {
  foreach ($scripts as $script) {
    $this->load->view($script);
  }
}
echo '</script>';

if (isset($output->js_files)) {
  foreach ($output->js_files as $file) {
    echo '<script src="' . $file . '"></script>';
  }
}
?>

<script>
  let site_url = '<?= base_url('data_alumni/Index'); ?>';

  var buttonChart = {
    buttons: {
      contextButton: {
        menuItems: [
          'viewFullscreen',
          'printChart',
          'separator',
          'downloadPNG',
          'downloadJPEG',
          'downloadPDF',
          'downloadSVG',
          'separator',
          'downloadCSV',
          'downloadXLS',
          'viewData'
        ]
      }
    }
  };

  $('[name="filter_fakultas"]').change(function(e) {
    let tahun = $('[name="filter_tahun"]').val();
    var val = e.target.value;
    $.ajax({
      url: site_url + '/getprodi',
      type: "GET",
      data: {
        kode_prodi: val
      },
      dataType: "JSON",
      success: function(response) {
        buatProdi(response.data.prodi);
        fakultas = response.data.fakultas;
        $('[name="filter_prodi"]').select2({
          placeholder: '- Pilih Program Studi/Jurusan -',
        });

        $('.title-text-header').text(`Data Alumni Tahun Lulus ${tahun} ${fakultas}`);
      },
      error: function(xx) {
        alert(xx)
      }


    });
  });

  function buatProdi(data) {
    let optionLoop = '<option value>Pilih Program Studi/Jurusan</option>';
    Object.keys(data).forEach(key => {
      optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
    });
    if ($('[name="filter_prodi"]')) {
      $('[name="filter_prodi"]').children().remove();
    }
    $('[name="filter_prodi"]').append(optionLoop);
  }

  $('#filter_prodi').change(function() {
    let prodi = $(this).val();
    let tahun = $('[name=filter_tahun]').val();
    let fakultas = $('[name=filter_fakultas]').val();

    $.ajax({
      type: 'ajax',
      method: 'POST',
      url: site_url + '/changeProdi',
      data: {
        prodi: prodi,
        tahun: tahun,
        fakultas: fakultas,
      },
      dataType: 'json',
      beforeSend: function() {},
      success: function(response) {
        if ((typeof response === 'string' || response instanceof String)) {
          Swal.fire('Gagal!', 'Aplikasi gagal terhubung dengan server. Silahkan hubungi admin.', 'error');
        }
        const data = response.data;
        prodi = data.prodi;
        if (tahun == 'all') {
          tahun = 'Keseluruhan'
        }
        $('.title-text-header').text(`Data Alumni Tahun Lulus ${tahun} Prodi/Jurusan ${prodi}`);
        statistikAlumni();
        grafikGender();
        grafikStatusAlumni();
        $('.fa-refresh').trigger('click');
      },
      error: function(xmlhttprequest, textstatus, message) {
        // text status value : abort, error, parseerror, timeout
        // default xmlhttprequest = xmlhttprequest.responseJSON.message
      },
    });
  });
</script>

<script>
  $(document).ready(function() {
    $.getJSON(
      `${site_url}/getTahun`,
      null,
      function(data, textStatus, jqXHR) {
        let tahun_lulus;
        if (data.status == 201) {
          $('#filter_tahun').html(`<option value="all">Semua Tahun Kelulusan</option>`);
          data.data.forEach((element) => {
            if (element.is_current == '1') {
              tahun_lulus = element.tahun;
            }
            $('#filter_tahun').append(`<option value="${element.tahun}" ` + (element.is_current == '1' ? 'selected' : '') + `>${element.tahun}</option>`);
          });
        }
        statistikAlumni();
        grafikGender();
        grafikStatusAlumni();
        $('.title-text-header').text(`Data Alumni Tahun Lulus ${tahun_lulus} Semua Prodi/Jurusan`);

      });

    $('#filter_tahun').change(function() {
      let tahun = $(this).val();
      let prodi = $('[name=filter_prodi]').val();
      if (tahun == 'all') {
        tahun = 'Keseluruhan'
      }


      $.ajax({
        type: 'ajax',
        method: 'POST',
        url: site_url + '/changeYears',
        data: {
          tahun: tahun,
          prodi: prodi,
        },
        dataType: 'json',
        beforeSend: function() {},
        success: function(response) {
          if ((typeof response === 'string' || response instanceof String)) {
            Swal.fire('Gagal!', 'Aplikasi gagal terhubung dengan server. Silahkan hubungi admin.', 'error');
          }
          const data = response.data;
          statistikAlumni();
          grafikGender();
          grafikStatusAlumni();
          if (data.prodi == '') {
            prodi = 'Semua Prodi/Jurusan'
          } else {
            prodi = 'Prodi/Jurusan ' + data.prodi;
          }
          $('.title-text-header').text(`Data Alumni Tahun Lulus ${tahun} ${prodi}`);
          $('.fa-refresh').trigger('click');
        },
        error: function(xmlhttprequest, textstatus, message) {
          // text status value : abort, error, parseerror, timeout
          // default xmlhttprequest = xmlhttprequest.responseJSON.message
          console.log(xmlhttprequest.responseJSON);
        },
      });
    });
  });

  function grafikGender() {
    $.ajax({
      type: 'ajax',
      method: 'POST',
      url: site_url + '/grafikGender',
      data: {
        ref_tahun: $('[name=filter_tahun]').val(),
        fakultas: $('[name=filter_fakultas]').val(),
        prodi: $('[name=filter_prodi]').val()
      },
      dataType: 'json',
      beforeSend: function() {},
      success: function(response) {
        if ((typeof response === 'string' || response instanceof String)) {
          Swal.fire('Gagal!', 'Aplikasi gagal terhubung dengan server. Silahkan hubungi admin.', 'error');
        }
        if (response.status == 201) {
          const data = response.grafik;
          let totalLaki = data.g_status_alumni_kelamin[0].y;
          let totalPerempuan = data.g_status_alumni_kelamin[1].y;
          Highcharts.chart("gStatus_alumni_kelamin", {
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: "pie"
            },
            exporting: buttonChart,
            title: {
              text: "Jenis Kelamin"
            },
            subtitle: {
              text: 'Grafik berdasarkan Jenis Kelamin'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: "pointer",
                dataLabels: {
                  enabled: true,
                  format: "<b>{point.name}</b>: {point.y}"
                },
                showInLegend: true,
              }
            },
            series: [{
              name: "Total",
              colorByPoint: true,
              data: [{
                name: 'Laki-laki',
                y: totalLaki,
              }, {
                name: 'Perempuan',
                y: totalPerempuan,
                color: 'rgb(234, 63, 95)'
              }]
            }]
          });
        }
      },
      error: function(xmlhttprequest, textstatus, message) {
        // text status value : abort, error, parseerror, timeout
        // default xmlhttprequest = xmlhttprequest.responseJSON.message
        console.log(xmlhttprequest.responseJSON);
      },
    });
  }

  function statistikAlumni() {
    var tahun_lulus = $('#filter_tahun').val();
    var prodi = $('#filter_prodi').val();
    var fakultas = $('#filter_fakultas').val();
    $.ajax({
      type: 'ajax',
      method: 'POST',
      url: site_url + '/statistik',
      data: {
        ref_tahun: tahun_lulus,
        fakultas: fakultas,
        prodi: prodi,
      },
      dataType: 'json',
      beforeSend: function() {
        loading();
      },
      success: function(response) {
        dismiss_loading();
        if ((typeof response === 'string' || response instanceof String)) {
          Swal.fire('Gagal!', 'Aplikasi gagal terhubung dengan server. Silahkan hubungi admin.', 'error');
        }
        $('.title-statistik').text(`Sebaran Data Alumni Kelulusan ${tahun_lulus}`);
        if (response.status == 201) {
          const data = response.data;

          $('.card-alumni').find('.total-data').text(data.alumni.total);
          $('.card-alumni-tertelusur').find('.total-data').text(data.alumni.alumni_submit);
          $('.card-prosentase-alumni').find('.total-data').text(data.alumni.persentase + '%');
          $('.card-alumni-bekerja').find('.total-data').text(data.bekerja.bekerja);
          $('.card-alumni-iddle').find('.total-data').text(data.bekerja.iddle);
          $('.card-prosentase-iku').find('.total-data').text(data.bekerja.persentase + '%');
        }
      },
      error: function(xmlhttprequest, textstatus, message) {
        // text, status, value : abort, error, parseerror, timeout;
        // default xmlhttprequest = xmlhttprequest.responseJSON.message;
        console.log(xmlhttprequest.responseJSON);
      },
    });
  }

  function grafikStatusAlumni() {
    $.ajax({
      type: 'ajax',
      method: 'POST',
      url: site_url + '/grafikStatusAlumni',
      data: {
        tahun: $('[name=filter_tahun]').val(),
        fakultas: $('[name=filter_fakultas]').val(),
        prodi: $('[name=filter_prodi]').val(),
      },
      dataType: 'json',
      beforeSend: function() {},
      success: function(response) {
        if ((typeof response === 'string' || response instanceof String)) {
          Swal.fire('Gagal!', 'Aplikasi gagal terhubung dengan server. Silahkan hubungi admin.', 'error');
        }
        if (response.status == 201) {
          const data = response.data.g_gender_by_status;
          console.log(data);
          new Highcharts.chart("gStatus_alumni_sektor", {
            chart: {
              type: 'column'
            },
            exporting: buttonChart,
            title: {
              text: 'Status Alumni'
            },
            subtitle: {
              text: 'Grafik Status Alumni berdasarkan Jenis Kelamin'
            },
            xAxis: {
              categories: ['Bekerja (fulltime / part time)', 'Belum memungkinkan bekerja (Menikah/wajib militer/mengurus keluarga)', 'Wiraswasta', 'Melanjutkan Pendidikan', 'Tidak Kerja tetapi sedang mencari kerja'],
              min: 0,
              max: 4
            },
            yAxis: {
              min: 0,
              title: {
                text: 'Total'
              },
              stackLabels: {
                enabled: true,
                style: {
                  fontWeight: 'bold',
                  color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                  ) || 'gray',
                  textOutline: 'none'
                }
              }
            },
            legend: {
              align: 'center',
              verticalAlign: 'bottom',
              floating: false,
              backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || 'white',
              borderColor: '#CCC',
              borderWidth: 1,
              shadow: false
            },
            tooltip: {
              headerFormat: '<b>{point.x}</b><br/>',
              pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            credits: {
              enabled: false
            },
            plotOptions: {
              column: {
                stacking: 'normal',
                dataLabels: {
                  enabled: true
                }
              }
            },
            // series: data.g_status_alumni_sektor
            series: [{
              name: 'Laki-laki',
              data: [
                data[0].y[0].bekerja,
                data[0].y[0].belumMungkin,
                data[0].y[0].wiraswasta,
                data[0].y[0].lanjutPendidikan,
                data[0].y[0].cariKerja,
              ],
              color: 'rgb(85, 161, 255)'

            }, {
              name: 'Perempuan',
              data: [
                data[1].y[0].bekerja,
                data[1].y[0].belumMungkin,
                data[1].y[0].wiraswasta,
                data[1].y[0].lanjutPendidikan,
                data[1].y[0].cariKerja,
              ],
              color: 'rgb(234, 63, 95)'
            }]
          });
        }
      },
      error: function(xmlhttprequest, textstatus, message) {
        // text status value : abort, error, parseerror, timeout
        // default xmlhttprequest = xmlhttprequest.responseJSON.message
        console.log(xmlhttprequest.responseJSON);
      },
    });
  }
</script>