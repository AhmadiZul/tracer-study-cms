<div class="topi card">
	<div class="card-body">
		<div class="row">
			<div class="col-lg-9 col-xs-12">
				<h2>Selamat Datang <?php echo $this->session->userdata('t_nama') ?></h2>
				<p>Selamat Datang di Aplikasi Tracer Study Alumni. Dimohon untuk segera melakukan Pembaharuan Profil</p>
			</div>
			<div class="col-lg-3 col-xs-12">
				<img class="img-fluid" src="<?php echo base_url()?>public/assets/img/admin/selamat-pagi.png" alt="">
			</div>
		</div>
	</div>
</div>

<div class="row">
	<!-- <div class="col-lg-8 col-xs-12">
		<div class="card">
			<div class="card-header">
				<h3>Tahapan Rekrutmen</h3>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-3 col-xs-12">
						<img class="img-fluid" src="<?php echo base_url()?>public/assets/img/admin/rekrutmen.png" alt="">
					</div>
					<div class="col-lg-9 col-xs-12">
						<h2>Segera lakukan pengisian borang tracer study dan lakukan pendaftaran kerja</h2>
						<p>Lamar pekerjaan untuk alumni binaan Pusdiktan dan dapatkan pekerja yang unggul serta kompetitif.</p>
						<a href="" role="button" class="btn btn-danger">Lamar Pekerjaan</a>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<div class="col-lg-4 col-xs-12">
		<div class="card">
			<div class="card-body">
				<div class="col-lg-12 text-center">
					<div class="avatar-item col-sm-1 offset-sm-6 col-3 offset-5 col-lg-3  offset-lg-5">
						<?php
							$photo = ($this->session->userdata('t_url_photo') != '' ? $this->session->userdata('t_url_photo') : 'public/assets/img/unknown.webp');
						?>
						<img class="img-fluid" src="<?php echo base_url() . $photo ?>" data-toggle="tooltip" title="<?php echo $this->session->userdata('t_nama') ?>">
						<a href="<?php echo site_url('profil_alumni') ?>" class="avatar-badge" title="Editor" data-toggle="tooltip"><i class="fas fa-pencil"></i></a>
					</div>
					<h2><?php echo $this->session->userdata('t_nama') ?></h2>
					<p><i class="fas fa-location-dot"></i> <?= $alumni->nama_kab_kota ?>, <?= $alumni->nama_provinsi ?></p>
					<label>Status Anda : <span class="badge badge-danger-old text-white">Alumni</span></label>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-8 col-xs-12">
		<div class="card bg-dark-red">
			<div class="card-body">
				<img class="img-fluid" style="margin-bottom: 24;" src="<?php echo base_url()?>public/assets/img/admin/segera.png" alt="">
				<h2 class="text-white">Segera isi tracer study dan lamar pekerjaan sesuai bidangmu</h2>
				<p class="text-white">Pendataan alumni binaan Pusat Pendidikan Pertanian</p>
				<a href="" role="button" class="btn btn-light-success">Lihat Selengkapnya</a>
			</div>
		</div>
	</div>
	<div class="d-none col-lg-4 col-xs-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-9">
						<h2>Selamat Pagi, <?php echo $this->session->userdata('t_nama') ?></h2>
						<p>Selamat Datang di Aplikasi LinkAN Pusdiktan, Tracer Study Alumni. Dimohon untuk segera melakukan Pembaharuan Profil</p>
					</div>
					<div class="col-3">
						<img class="img-fluid" src="<?php echo base_url()?>public/assets/img/admin/selamat-pagi.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>