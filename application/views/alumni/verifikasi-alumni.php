<section id="banner">
	<div class="container-banner">
		<div class="row justify-content-between align-items-center">
			<div class="col-5">
				<h2>Pengajuan Verifikasi Alumni</h2>
				<img src="<?php echo base_url() ?>public/assets/img/alumni/vector-verifikasi.png" class="img img-fluid img-hero" alt="">
				<p class="mt-4">Alumni dapat mengajukan permintaan verifikasi secara online dengan melengkapi beberapa data alumni yang disediakan</p>
				<a href="" class="btn-get-started">Selengkapnya</a>
			</div>
			<div class="col-6">
				<img src="<?php echo base_url() ?>public/assets/img/alumni/banner-verifikasi.png" class="img img-fluid img-hero" alt="">
			</div>
		</div>
	</div>
</section>

<section id="alur-pengajuan">
	<div class="container">
		<div class="row align-items-center justify-content-between">
			<div class="col-3 align-self-start">
				<div class="section-tittle">
					<h2>Alur Pengajuan</h2>
					<p>Berikut adalah alur pengajuan verifikasi alumni Polbangtan di bawah naungan Pusdiktan</p>
					<img src="<?php echo base_url() ?>public/assets/img/alumni/alur-pengajuan.png">
				</div>
			</div>
			<div class="col-8">
				<div class="row align-items-center">
					<div class="col-1 pr-0">
						<span class="badge">1</span>
					</div>
					<div class="col-5 d-flex pl-0 card-langkah align-items-center">
						<img src="<?php echo base_url() ?>public/assets/img/alumni/isi-formulir.png">
						<h2>Isi Formulir</h2>
					</div>
					<div class="col-6 penjelasan">
						<p>Silahkan mengisi formulir <a href="">Pengajuan Verifikasi Alumni</a> dan lengkapi data dengan sebenar-benarnya.</p>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-1 pr-0">
						<span class="badge">2</span>
					</div>
					<div class="col-5 d-flex pl-0 card-langkah align-items-center">
						<img src="<?php echo base_url() ?>public/assets/img/alumni/proses-verifikasi.png">
						<h2>Proses Verifikasi</h2>
					</div>
					<div class="col-6 penjelasan">
						<p>Setelah formulir dikirimkan, admin akan melakukan proses verifikasi selama lebih kurang 7 hari kerja.</p>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-1 pr-0">
						<span class="badge">3</span>
					</div>
					<div class="col-5 d-flex pl-0 card-langkah align-items-center">
						<img src="<?php echo base_url() ?>public/assets/img/alumni/check-email.png">
						<h2>Check Email Secara Berkala</h2>
					</div>
					<div class="col-6 penjelasan">
						<p>Untuk mengetahui status verifikasi, Admin akan mengirimkan notifikasi di email aktif yang telah Anda tuliskan pada formulir sebelumnya.</p>
					</div>
				</div>
				<div class="row align-items-center">
					<div class="col-1 pr-0">
						<span class="badge">4</span>
					</div>
					<div class="col-5 d-flex pl-0 card-langkah align-items-center">
						<img src="<?php echo base_url() ?>public/assets/img/alumni/terverifikasi.png">
						<h2>Terverifikasi</h2>
					</div>
					<div class="col-6 penjelasan">
						<p>Anda juga dapat mengetahui status verifikasi pada fitur <a href="">Check Status Verifikasi Alumni</a>. Jika selama 7 hari kerja belum terdapat notifikasi pada email, maka silahkan hubungi WA Center Admin berikut: 08123456789</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="form-pengajuan">
	<div class="container">
		<form>
			<div class="row">
				<div class="col-5 align-self-start">
					<div class="section-tittle">
						<h2>Formulir Permohonan Verifikasi Data Alumni</h2>
					</div>
				</div>
				<div class="col-7">

				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="nama" class="form-label">Nama Lengkap <b class="text-danger">*</b></label>
						<input type="text" class="form-control" name="nama" placeholder="Masukkan nama lengkap">
					</div>
					<div class="form-group">
						<label for="nama" class="form-label">Email <b class="text-danger">*</b></label>
						<input type="text" class="form-control" name="nama" placeholder="Masukkan email">
					</div>
					<div class="form-group">
						<label for="nama" class="form-label">NIM/NIS</label>
						<input type="text" class="form-control" name="nama" placeholder="Masukkan NIM">
					</div>
					<div class="form-group">
						<label for="nama" class="form-label">Nomor Telepon <b class="text-danger">*</b></label>
						<input type="text" class="form-control" name="nama" placeholder="Masukkan nomor telepon">
					</div>
					<div class="form-group">
						<label for="nama" class="form-label">Tanggal Lahir <b class="text-danger">*</b></label>
						<input type="text" class="form-control" name="nama" placeholder="Masukkan tanggal lahir">
					</div>
					<div class="form-group">
						<label for="nama" class="form-label">Tanggal Lulus <b class="text-danger">*</b></label>
						<input type="text" class="form-control" name="nama" placeholder="Masukkan tanggal lulus">
					</div>
					<div class="form-group">
						<label for="nama" class="form-label">Jenjang Pendidikan</label>
						<select class="form-control" name="jenjang">
							<option>Polbangtan Bogor</option>
							<option>Polbangtan Malang</option>
							<option>Polbangtan Medan</option>
							<option>Polbangtan Yoma</option>
						</select>
					</div>
					<div class="form-group">
						<label for="nama" class="form-label">Jurusan <b class="text-danger">*</b></label>
						<select class="form-control" name="jenjang">
							<option>Perkebunan</option>
							<option>Perairan</option>
							<option>Pertanian</option>
						</select>
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="nama" class="form-label">Nomor Seri Ijazah <b class="text-danger">*</b></label>
						<input type="text" class="form-control" name="nama" placeholder="Masukkan nomor seri ijazah tanpa spasi">
					</div>
					<div class="form-group">
						<label for="nama" class="form-label">Upload Kartu Identitas <b class="text-danger">*</b></label>
						<input type="file" class="form-control" name="nama" placeholder="Nama Lengkap">
						<small>Type: Pdf | Max: 10 Mb</small>
					</div>
					<div class="form-group">
						<label for="nama" class="form-label">Upload Ijazah <b class="text-danger">*</b></label>
						<input type="file" class="form-control" name="nama" placeholder="Nama Lengkap">
						<small>Type: Pdf | Max: 10 Mb</small>
					</div>
					<div class="form-group">
						<button class="btn btn-warning">Kirim Pengajuan</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

<section id="check-status">
	<div class="container">
            <div class="row icon-box align-items-center">
                <div class="col-3">
                	<img src="<?php echo base_url() ?>public/assets/img/alumni/check-status.png" class="img img-fluid img-hero" alt="">
                </div>
                <div class="col-7">
                	<div class="section-tittle">
                		<h2>Check Status Verifikasi Alumni</h2>
                		<p>Anda dapat melacak status verifikasi Anda sebagai alumni di bawah ini</p>
                	</div>
                	<form>
                		<div class="form-group">
                			<label>Nomor Induk Mahasiswa (NIM) *</label>
                			<input type="text" class="form-control" name="nim" placeholder="Masukkan NIM">
                		</div>
                		<div class="form-group mt-3">
                			<button>Check Status <i class="fa fa-arrow-right"></i></button>
                		</div>
                	</form>
                </div>
            </div>
        </div>
</section>