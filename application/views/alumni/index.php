<main id="main">
    <section id="hero-jadwal" class="">
            <div class="bg">
                <div class="container">
                    <div class="col-md-12 row" style="height:450px;">
                        <div class="carousel-container">
                            <h2 class="animate__animated animate__fadeInDown text-center">Tracer Study Alumni</h2>
                            <p>Studi penelusuran alumni untuk mengetahui kegiatan alumni setelah lulus dari Perguruan Tinggi, transisi dari dunia pendidikan tinggi ke dunia kerja, situasi kerja, pemerolehan kompetensi dan penggunaan kompetensi dalam pekerjaan</p>
                        </div>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section>

    <!-- ======= Services Section ======= -->
    <section id="kuesioner" class="kuesioner">
        <div class="container" data-aos="fade-up">
                <div class="row">
                    <div class="col-md-6">
                        <div class="d-flex justify-content-center align-content-center">
                            <img src="<?php echo base_url() ?>public/assets/img/tracer_alumni/3orang.png" alt="" class="img img-fluid">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3 class="text-tema"><strong>Ada 3 Tracer Study</strong></h3>
                        <div class="row mt-4">
                            <div class="col-md-2 col-xs-2 col-sm-2">
                                <h1><span class="badge badge-pill badge-tema align-items-center">1</span></h1>
                            </div>
                            <div class="col-md-10 col-xs-10 col-sm-10 d-flex justify-content-center align-content-center align-items-center">
                                <p>
                                <strong>Pra Tracer</strong> <br>
                                Diisi oleh alumni yang baru lulus dari perguruan tinggi kurang dari 1 tahun setelah wisuda.
                                </p>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-2 col-xs-2 col-sm-2">
                                <h1><span class="badge badge-pill badge-tema align-items-center">2</span></h1>
                            </div>
                            <div class="col-md-10 col-xs-10 col-sm-10 d-flex justify-content-center align-content-center align-items-center">
                                <p>
                                    <strong>Tracer Study 1</strong> <br>
                                Diisi oleh alumni yang telah lulus dari perguruan tinggi lebih dari 1 tahun dan kurang dari 2 tahun.
                                </p>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-2 col-xs-2 col-sm-2">
                                <h1><span class="badge badge-pill badge-tema align-items-center">3</span></h1>
                            </div>
                            <div class="col-md-8 col-xs-8 col-sm-8 d-flex justify-content-center align-content-center align-items-center">
                                <p>
                                <strong>Tracer Study 2</strong> <br>
                                Diisi oleh alumni yang telah lulus lebih dari 2 tahun.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section><!-- End Services Section -->

    <section class="isian-kuesioner">
        <div class="container">

            <div class="row">
                <div class="col-md-12 col-lg-12 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="fade-up">
                    <div class="col mt-4">
                        <h4 class="text-center"><strong>Pengisian Kuesioner</strong></h4>
                        <div class="row container">
                            <div class="col mt-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img src="<?php echo base_url() ?>public/assets/img/tracer_alumni/image38.png" class="img img-fluid" width="100%" alt=""><br><br>
                                        <b style="margin-top:100px;"><i class="fas fa-file-alt"></i> Pra Tracer</b>
                                        <p class="text-black mt-3 mb-3">Diisi pengguna lulusan dalam pelaksanaan tracer study perguruan tinggi.</p><br>
                                        <div class="center">
                                            <a href="<?php echo base_url() ?>tracer?id=1" class="btn btn-login mt-4 center"><b>Isi Kuesioner <i class="fas fa-arrow-alt-circle-right"></i></b></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col mt-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img src="<?php echo base_url() ?>public/assets/img/tracer_alumni/image38.png" class="img img-fluid" width="100%" alt=""><br><br>
                                        <b><i class="fas fa-file-alt"></i> Tracer Study Pertama</b>
                                        <p class="text-black mt-3 mb-3">Kuesioner ini dipergunakkan untuk alumni dari perguruan tinggi yang baru pertama kali akan mengisi.</p><br>
                                        <div class="center">
                                            <a href="<?php echo base_url() ?>tracer?id=2" class="btn btn-login mt-4 center"><b>Isi Kuesioner <i class="fas fa-arrow-alt-circle-right"></i></b></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col mt-3">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img src="<?php echo base_url() ?>public/assets/img/tracer_alumni/image38.png" class="img img-fluid" width="100%" alt=""><br><br>
                                        <b><i class="fas fa-file-alt"></i> Tracer Study Kedua</b>
                                        <p class="text-black mt-3 mb-3">Kuesioner ini dipergunakkan untuk alumni dari perguruan tinggi akan mengisi untuk kedua kalinya setelah dua tahun berjalan</p>
                                        <div class="center">
                                            <a href="<?php echo base_url() ?>tracer?id=3" class="btn btn-login mt-4 center"><b>Isi Kuesioner <i class="fas fa-arrow-alt-circle-right"></i></b></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->


<script type="text/javascript">
    document.getElementById("defaultOpen").click();

    function opentabs(evt, cityName) {
        // Declare all variables
        var i, tabcontent, tablinks;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" filter-active", "");
        }

        // Show the current tab, and add an "active" class to the button that opened the tab
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " filter-active";
    }
</script>