<!-- ======= Hero Section ======= -->
<section id="hero2">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-6 col-sm-12 align-self-center deskripsi">
            <h2 class="animate__animated animate__fadeInDown hero-title">Alumni Berprestasi</h2>
            <p>Pencapaian para alumni tidak henti-hentinya mencetak prestasi untuk negeri. Berikut adalah segudang prestasi baik akademik maupun non akademik alumni Pusdiktan
                <br>
                <a href="#prestasi" class="btn-get-started animate__animated animate__fadeInUp scrollto">Selengkapnya</a>
            </p>
        </div>
        <div class="col-md-6 d-none d-sm-none d-md-block d-lg-block d-xl-block">
            <img src="<?php echo base_url() ?>public/assets/img/alumni/content/hero.png" alt="" class="img-fluid">
        </div>
    </div>
</section>
<!-- End Hero -->

<section>
    <h2 class="text-center prestasi-title">Rekam Jejak Prestasi Alumni</h2>
    <p class="text-center prestasi-subtitle">Berikut adalah beberapa Alumni Berprestasi dari Lembaga Pendidikan Tinggi Vokasi Polbangtan Seluruh Indonesia</p>
</section>

<section id="prestasi" class="mb-3 mt-0" style="background-color: #F5F5F5;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 d-flex justify-content-center">
                <img src="<?php echo base_url() ?>public/assets/img/alumni/content/satu.png" alt="" class="img img-fluid rounded p-3 mb-3 rounded" style="width: 75%;">
            </div>
            <div class="col-md-6">
                <h2 class="nama-prestasi">Adam Alexa Hassan</h2>
                <p class="duta-prestasi">Duta Polbangtan Medan 2021</p>
                <p class="kata-prestasi">Banyak pengalaman yang tak terduga yang saya dapatkan dari kampus yang saya cintai ini, mulai dari pengalaman akademik sampai dengan pengalaman sosial. Dengan lingkungan ketarunaan membuat saya mengerti arti displin dan kekompakan, dengan teman-teman seperjuangan.</p>
            </div>
        </div>
    </div>
</section>

<section class="mt-4" style="background-color: #F5F5F5;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 align-middle">
                <h2 class="nama-prestasi">Afra Rainey Keysa </h2>
                <p class="duta-prestasi">Duta Polbangtan Medan 2021</p>
                <p class="kata-prestasi">Disini saya belajar bersama dengan teman dari seluruh Indonesia, sehingga bisa berdiskusi dengan berbagai sudut pandang untuk memperluas cakrawala ilmu pengetahuan.</p>
            </div>
            <div class="col-md-6 d-flex justify-content-center">
                <img src="<?php echo base_url() ?>public/assets/img/alumni/content/dua.png" alt="" class="img img-fluid rounded p-3 mb-3 rounded" style="width: 75%;">
            </div>
        </div>
    </div>
</section>

<section>
    <h2 class="text-center title-alumni">Daftar Alumni Berprestasi</h2>
    <p class="text-center p-alumni">Anda dapat melihat daftar alumni berprestasi Polbangtan di bawah naungan Pusdiktan serta SMK-PP di bawah ini</p>
    <p class="text-center sub-alumni">Pusdiktan</p>

    <div class="container-fluid d-flex justify-content-center">
        <div class="row">
            <div class="col-md-4 mt-4 d-flex justify-content-center">
                <div class="card card-alumni-27">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/piala.png" alt="..." class="piala-card">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/1.png" class="img img-fluid rounded text-center" alt="..." style="widht: 25%;">
                    <div class="card-body">
                        <p class="card-text text-center daftar-alumni-card">Daftar Alumni Berprestasi</p>
                        <p class="card-text text-center daftar-alumni">Politeknik Pembangunan Pertanian Bogor</p>
                        <div class="row text-center">
                            <div class="col">
                                <button class="btn btn-outline-success me-2 button-live-preview">Live Preview</button>
                                <button class="btn btn-succes button-download">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-4 d-flex justify-content-center">
                <div class="card text-center card-alumni-27">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/piala.png" alt="..." class="piala-card">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/2.png" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text text-center daftar-alumni-card">Daftar Alumni Berprestasi</p>
                        <p class="card-text text-center daftar-alumni">Politeknik Pembangunan Pertanian Medan</p>
                        <div class="row text-center">
                            <div class="col">
                                <button class="btn btn-outline-success me-2 button-live-preview">Live Preview</button>
                                <button class="btn btn-success button-download">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-4 d-flex justify-content-center">
                <div class="card card-alumni-27">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/piala.png" alt="..." class="piala-card">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/3.png" class="img img-fluid rounded text-center" alt="..." style="widht: 25%;">
                    <div class="card-body">
                        <p class="card-text text-center daftar-alumni-card">Daftar Alumni Berprestasi</p>
                        <p class="card-text text-center daftar-alumni">Politeknik Pembangunan Pertanian YOMA</p>
                        <div class="row text-center">
                            <div class="col">
                                <button class="btn btn-outline-success me-2 button-live-preview">Live Preview</button>
                                <button class="btn btn-success button-download">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-5 d-flex justify-content-center">
                <div class="card card-alumni-27">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/piala.png" alt="..." class="piala-card">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/4.png" class="img img-fluid rounded text-center" alt="..." style="widht: 25%;">
                    <div class="card-body">
                        <p class="card-text text-center daftar-alumni-card">Daftar Alumni Berprestasi</p>
                        <p class="card-text text-center daftar-alumni">Politeknik Pembangunan Pertanian Gowa</p>
                        <div class="row text-center">
                            <div class="col">
                                <button class="btn btn-outline-success me-2 button-live-preview">Live Preview</button>
                                <button class="btn btn-success button-download">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-5 d-flex justify-content-center">
                <div class="card card-alumni-27">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/piala.png" alt="..." class="piala-card">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/5.png" class="img img-fluid rounded text-center" alt="..." style="widht: 25%;">
                    <div class="card-body">
                        <p class="card-text text-center daftar-alumni-card">Daftar Alumni Berprestasi</p>
                        <p class="card-text text-center daftar-alumni">Politeknik Pembangunan Pertanian Manokwari</p>
                        <div class="row text-center">
                            <div class="col">
                                <button class="btn btn-outline-success me-2 button-live-preview">Live Preview</button>
                                <button class="btn btn-success button-download">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-5 d-flex justify-content-center">
                <div class="card card-alumni-27">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/piala.png" alt="..." class="piala-card">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/6.png" class="img img-fluid rounded text-center" alt="..." style="widht: 25%;">
                    <div class="card-body">
                        <p class="card-text text-center daftar-alumni-card">Daftar Alumni Berprestasi</p>
                        <p class="card-text text-center daftar-alumni">Politeknik Pembangunan Pertanian Malang</p>
                        <div class="row text-center">
                            <div class="col">
                                <button class="btn btn-outline-success me-2 button-live-preview">Live Preview</button>
                                <button class="btn btn-success button-download">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col"></div>
            <div class="col-md-4 mt-5 d-flex justify-content-center">
                <div class="card card-alumni-30">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/piala.png" alt="..." class="piala-card">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/7.png" class="img img-fluid rounded text-center mt-1" alt="..." style="widht: 25%;">
                    <div class="card-body">
                        <p class="card-text text-center daftar-alumni-card">Daftar Alumni Berprestasi</p>
                        <p class="card-text text-center daftar-alumni">Politeknik Enjiniring Pertanian Indonesia Serpong</p>
                        <div class="row text-center">
                            <div class="col">
                                <button class="btn btn-outline-success me-2 button-live-preview">Live Preview</button>
                                <button class="btn btn-success button-download">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>

    <!-- SMK RPP -->
    <p class="text-center mt-4 sub-alumni">SMK-PP</p>
    <div class="container-fluid d-flex justify-content-center">
        <div class="row">
            <div class="col-md-4 mt-5 d-flex justify-content-center">
                <div class="card card-alumni-1">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/piala.png" alt="..." class="piala-card">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/banjarbaru.png" class="card-img-top text-center" alt="...">
                    <div class="card-body">
                        <p class="card-text text-center daftar-alumni-card">Daftar Alumni Berprestasi</p>
                        <p class="card-text text-center daftar-alumni">SMK-PPN Banjarbaru</p>
                        <div class="row text-center mb-4">
                            <div class="col">
                                <button class="btn btn-outline-success me-2 button-live-preview">Live Preview</button>
                                <button class="btn btn-success button-download">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-5 d-flex justify-content-center">
                <div class="card card-alumni-1">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/piala.png" alt="..." class="piala-card">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/kupang.png" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text text-center daftar-alumni-card">Daftar Alumni Berprestasi</p>
                        <p class="card-text text-center daftar-alumni">SMK-PPN Kupang</p>
                        <div class="row text-center mb-4">
                            <div class="col">
                                <button class="btn btn-outline-success me-2 button-live-preview">Live Preview</button>
                                <button class="btn btn-success button-download">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-5 d-flex justify-content-center">
                <div class="card card-alumni-1">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/piala.png" alt="..." class="piala-card">
                    <img src="<?php echo base_url() ?>public/assets/img/alumni/berprestasi/sembawa.png" class="card-img-top" alt="...">
                    <div class="card-body">
                        <p class="card-text text-center daftar-alumni-card">Daftar Alumni Berprestasi</p>
                        <p class="card-text text-center daftar-alumni">SMK-PPN Sembawa</p>
                        <div class="row text-center mb-4">
                            <div class="col">
                                <button class="btn btn-outline-success me-2 button-live-preview">Live Preview</button>
                                <button class="btn btn-success button-download">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col"></div>
            <div class="col"></div>
            <div class="col"></div>
        </div>
    </div>
</section>