$(document).ready(function () {
    $.getJSON(
        `${site_url}/getTahun`,
        null,
        function (data, textStatus, jqXHR) {
            if (data != null) {
                data.data.forEach((element) => {
                    $('#filter_tahun').append(`<option value="${element.tahun}" `+(element.is_current=='1'? 'selected': '')+`>${element.tahun}</option>`);
                });
            }
        });
});