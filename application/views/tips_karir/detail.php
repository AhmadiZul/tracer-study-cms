<div class="warpper" style="padding-top: 30px;"></div>
<main id="main">

  <!-- ====== survey mitra karir ===== -->
  <section class="detail">
    <div class="container">
      <h3 class="text-center"><?= $tipsAll->judul?></h3>
      <p class="text-danger text-center"><?= $tipsAll->created_at?></p>
      <hr>
          <img src="<?php echo URL_FILE . $tipsAll->url_photo ?>" style="width: 100%; height: auto;" class="img-fluid" >
          <p><?= $tipsAll->deskripsi?></p>
    </div>
  </section>
  <section id="agenda" class="agenda">
    <div class="container" data-aos="fade-up">
      <div class="d-flex align-items-center">
        <div class="badge badge-tema p-2 rounded-pill text-light mr-4" style="width: 80px;">&nbsp;&nbsp;&nbsp;&nbsp;</div>
        <span class="my-auto " style="font-size: 32px; "><b>Agenda Terdekat</b></span>
      </div>
      <?php if (!empty($agenda)) { ?>
      <div class="card-deck  mt-4">
        <?php foreach ($agenda as $key => $value) { ?>
        <div class="card">
          <img src="<?php echo URL_FILE. $value->flyer ?>" class="card-img-top img-fluid" alt="...">
          <div class="card-body">
            <a class="card-title" href="<?= base_url("agenda/index?key=" . $value->id) ?>"><h5 class=" text-dark font-weight-bold"><?= $value->nama?></h5></a><br>
            <p class="card-text badge p-2 rounded-pill bghr mt-2"><small class="text-light"><?php echo $value->waktu_mulai ?></small></p>
            <p class= "card-text px-2" style="font-size:12px;"><?= $value->waktu_acara?> - <?= $value->waktu_berakhir?></p>
          </div>
        </div>
        <?php }?>
      </div>
      <?php } else { ?>
      <h4 class="text-center">Agenda belum tersedia</h4>
      <?php } ?>
    </div>
  </section>
</main>
<script>
  var site_url = '<?php echo base_url() ?>';

  function cvTips(url, id) {
    $.ajax({
      type: 'ajax',
      method: 'POST',
      url: site_url + 'tips_karir/index/cvTips',
      data: {
        id_berita: id
      },
      dataType: 'json',
      success: function(response) {
        if (response.success) {
          window.location.href = url;
        } else {
          swal({
            title: 'Galat',
            text: 'Hubungi admin',
            type: 'error',
            confirmButtonColor: '#396113',
          });
        }
      },
      error: function(xmlhttprequest, textstatus, message) {
        // text status value : abort, error, parseerror, timeout
        // default xmlhttprequest = xmlhttprequest.responseJSON.message
        console.log(xmlhttprequest.responseJSON);
      },
    });
  }
</script>