
<main id="main">

  <section id="berita" class="berita">
    <div class="container" data-aos="fade-up">
      <div class="badge badge-tema p-2 rounded-pill mt-5 text-light">TIPS KARIR TERBARU</div>
      <div class="row mt-4">
        <?php if (!empty($tips_karir)) { ?>
        <?php foreach ($tips_karir as $key => $value){?>
        <div class="col-md-4 mt-4">
          <img src="<?php echo URL_FILE. $value->url_photo?>" class="img-fluid" alt="..."
            style="width :100%; height:200px;">
          <a href="<?= base_url("tips/$value->id_berita") ?>"><h5 class="mt-2 font-weight-bold"><?= $value->judul?></h5></a>
        </div>
        <?php }?>
        <?php } else { ?>
        <h4 class="text-center">Tips karir belum tersedia</h4>
        <?php } ?>
      </div>
    </div>
  </section>

  <section id="agenda" class="agenda">
    <div class="container" data-aos="fade-up">
      <div class="badge badge-tema p-2 rounded-pill mt-2 text-light">Agenda Terbaru</div>
      <?php if (!empty($agenda)) { ?>
      <div class="card-deck  mt-4">
        <?php foreach ($agenda as $key => $value) { ?>
        <div class="card">
          <img src="<?php echo URL_FILE. $value->flyer ?>" class="card-img-top img-fluid" alt="...">
          <div class="card-body">
            <a class="card-title" href="<?= base_url("agenda/index?key=" . $value->id) ?>"><?= $value->nama?></a><br>
            <p class="card-text badge p-2 rounded-pill bghr mt-2"><small
                class="text-light"><?php echo $value->waktu_mulai ?></small></p>
            <p class="card-text px-2" style="font-size:12px;"><?= $value->waktu_acara?> - <?= $value->waktu_berakhir?>
            </p>
          </div>
        </div>
        <?php }?>
      </div>
      <?php } else { ?>
      <h4 class="text-center">Agenda belum tersedia</h4>
      <?php } ?>
    </div>
  </section>

  <!-- <div class="container">
      <h4 class="d-inline tagline text-white">Semua Tips Karir</h4>
      <div class="d-inline" style="margin-left: 100px;">
        <span class="badge p-2 rounded-pill bg-lime text-lime">Tracer Study</span>
        <span class="badge p-2 rounded-pill bg-lime text-lime">Akademik</span>
        <span class="badge p-2 rounded-pill bg-lime text-lime">karir</span>
        <span class="badge p-2 rounded-pill bg-lime text-lime">Alumni</span>
      </div>
      <div style="margin-top: 50px;" id="tabel_tips">
        <div class="d-flex justify-content-center mt-3">
          <nav aria-label="Page navigation example">
            <ul class="pagination">
              <li class="page-item">
                <a class="page-link text-warning" href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              <li class="page-item"><a class="page-link text-warning" href="#">1</a></li>
              <li class="page-item"><a class="page-link text-warning" href="#">2</a></li>
              <li class="page-item"><a class="page-link text-warning" href="#">3</a></li>
              <li class="page-item">
                <a class="page-link text-warning" href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="d-flex justify-content-center mt-3">
        <nav aria-label="Page navigation example">
          <ul class="pagination">
            <li class="page-item"><button id="btn_prev" class="page-link text-warning">Sebelumnya</button></li>
            <li class="page-item"><span id="page_span" class="page-link text-warning disabled"></span></li>
            <li class="page-item"><button id="btn_next" class="page-link text-warning">Selanjutnya</button></li>
          </ul>
        </nav>
      </div>
    </div> -->

  <!-- End Keterangan Section -->
  <!-- ====== survey mitra karir ===== -->
  <!-- <section id="survey" class="survey">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-8" data-aos="fade-up">
          <div class="row icon-box p-1">
            <div class="col-md-4 img-survey">
              <img src="<?php echo base_url() ?>public/assets/img/news/ornament/ornamen-1.png" alt="" class="img img-fluid">
            </div>
            <div class="col-md-8">
              <div class="mt-3">
                <div style="height:65px;"></div>
                <h2 class="tagline text-white"><b>Dapatkan Notifikasi Tips Karir Terkini</b></h2>
                <p class="sub-tagline text-white">Masukkan Email Anda dan dapatkan informasi seputar Pusdiktan.</p>
                <div class="form-group">
                  <label class="text-white" for="">Email</label>
                  <input type="email" class="form-control" placeholder="Masukkan email">
                </div>
                <a href="#" class="btn btn-warning text-white mt-3">Subscribe <i class="fas fa-arrow-alt-circle-right"></i></a>
                <br><br>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section> -->
  <!-- ======= Services Section ======= -->
</main><!-- End #main -->

<!-- <script>
  var site_url = '<?php echo base_url() ?>';
  let current_page = 1;
  let records_per_page = 5;

  let tipsSurvei__filterKategori = '';

  let objJson = JSON.parse(`<?php echo isset($tips) ? json_encode($tips) : NULL; ?>`);

  function prevPage() {
    if (current_page > 1) {
      current_page--;
      changePage(current_page);
    }
  }

  function nextPage() {
    if (current_page < numPages()) {
      current_page++;
      changePage(current_page);
    }
  }

  function changePage(page) {
    // array objJson baru
    let tempObjJson = objJson.map(function(obj) {
      return obj;
    });

    // FILTER PERGURUAN TINGGI
    if (tipsSurvei__filterKategori != '') {
      tempObjJson = tempObjJson.filter(function(obj) {
        return obj['kategori'] == this.data;
      }, {
        data: tipsSurvei__filterKategori
      });
    }

    // Validate page
    if (page < 1) page = 1;
    if (page > numPages(tempObjJson)) page = numPages(tempObjJson);

    $('#tabel_tips').html('<span class="text-white">Tips karir tidak ada</span>');

    if (tempObjJson.length <= 0) {
      $('#btn_prev, #btn_next').css('visibility', 'hidden');
      $('#page_span').html('0/0');
      $('#page_span').addClass('d-none');
      return;
    }

    // MEMBUAT CARD HTML
    let tempAllCard = '';
    for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < tempObjJson.length; i++) {
      tempAllCard += renderCard(tempObjJson[i]);
    };

    $('#tabel_tips').html(tempAllCard);
    $('#page_span').removeClass('d-none');
    $('#page_span').html(page + '/' + numPages(tempObjJson));

    if (page == 1) {
      $('#btn_prev').css('visibility', 'hidden');
    } else {
      $('#btn_prev').css('visibility', 'visible');
    };

    if (page == numPages(tempObjJson)) {
      $('#btn_next').css('visibility', 'hidden');
    } else {
      $('#btn_next').css('visibility', 'visible');
    };
  };

  function numPages(obj = null) {
    if (obj === null) {
      return Math.ceil(objJson.length / records_per_page);
    };
    return Math.ceil(obj.length / records_per_page);
  }

  $(document).ready(function() {
    if (objJson) {
      changePage(1);
      $('#btn_prev').on('click', prevPage);
      $('#btn_next').on('click', nextPage);

      $('#tipsSurvei__filterKategori').on('change', function() {
        tipsSurvei__filterKategori = $(this).val();
        changePage(1);
      });
    };
  });

  function renderCard(obj) {
    let judul = obj['judul'] ? obj['judul'] : '';
    let image = obj['image_tips'] ? obj['image_tips'] : '';
    let info_tips = obj['info_tips'] ? obj['info_tips'] : '';
    let slug = obj['slug'] ? obj['slug'] : '';
    let id = obj['id'] ? obj['id'] : '';
    let publish_time = obj['publish_time'] ? obj['publish_time'] : '';

    let template = `
                <div class="row mb-2">
                  <div class="col-md-2">
                    <img src="<?php echo base_url() ?>admin/public/uploads/berita/` + image + `" style="width: 100%;" class="img img-fluid" alt="">
                  </div>
                  <div class="col-md-10">
                    <div class="p-2">
                      <h5 class="font-weight-bold">`+judul+`</h5>
                    </div>
                    <div class="content-media">
                      <span class="badge p-2 rounded-pill bg-lime text-lime">Akademik</span>
                      <span class="badge p-2 rounded-pill bg-lime text-lime">Akademik</span>
                      <span class="badge p-2 rounded-pill bg-grey text-grey mt-2 float-right">` + publish_time + `</span>
                      <div class="p-2 mt-2">
                        <p class="text-justify">` + info_tips + `...</p>
                        <div class="d-flex align-items-center">
                          <a href="javascript:cvTips('` + site_url + `tips/` + slug + `','` + id + `')" class="text-white"><b>Lihat Selengkapnya <i class="fas fa-chevron-right"></i></b></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                `;
    return template;
  }

  function cvTips(url, id) {
    $.ajax({
      type: 'ajax',
      method: 'POST',
      url: site_url + 'tips_karir/index/cvTips',
      data: {
        id: id
      },
      dataType: 'json',
      success: function(response) {
        if (response.success) {
          window.location.href = url;
        } else {
          swal({
            title: 'Galat',
            text: 'Hubungi admin',
            type: 'error',
            confirmButtonColor: '#396113',
          });
        }
      },
      error: function(xmlhttprequest, textstatus, message) {
        // text status value : abort, error, parseerror, timeout
        // default xmlhttprequest = xmlhttprequest.responseJSON.message
        console.log(xmlhttprequest.responseJSON);
      },
    });
  }
</script> -->