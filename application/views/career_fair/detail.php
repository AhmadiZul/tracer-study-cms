<link rel="stylesheet" href="<?= base_url('public/vendor/datatables/jquery.dataTables.min.css') ?>">
<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4>Detail kegiatan career fair</h4>
                <table class="table table-sm borderless">
                    <tr>
                        <th>Nama Career Fair</th>
                        <th>:</th>
                        <td><?php echo $data['data']->nama_job_fair ?></td>
                    </tr>
                    <tr>
                        <th>Tempat Pelaksanaan</th>
                        <th>:</th>
                        <td><?php echo $data['data']->tempat ?></td>
                    </tr>
                    <tr>
                        <th>Perguruan Tinggi Pelaksaan</th>
                        <th>:</th>
                        <td><?php echo $data['data']->nama_resmi ?></td>
                    </tr>
                    <tr>
                        <th>Tanggal Pendaftaran</th>
                        <th>:</th>
                        <td>
                            <?php echo $this->tanggalindo->konversi($data['data']->waktu_awal_pendaftaran) . ' - ' . $this->tanggalindo->konversi($data['data']->tanggal_akhir_pendaftaran) ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Pelaksanaan</th>
                        <th>:</th>
                        <td>
                            <?php echo $this->tanggalindo->konversi($data['data']->waktu_awal_pelaksanaan) . ' - ' . $this->tanggalindo->konversi($data['data']->waktu_akhir_pelaksanaan) ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="card-body">
                <?php if (getSessionRoleAccess() == '3') : ?>
                    <?php if ($registered['status'] == 500) : ?>
                        <div class="d-flex justify-content-center">
                            <button class="btn btn-success btn-block btn-lg" id="btn-registrasi"><i class="fas fa-check"></i> Daftar Sekarang</button>
                        </div>
                        <div class="modal fade" id="modal-pendaftaran">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <form action="" id="form-pendaftaran" enctype="multipart/form-data">
                                        <div class="modal-header">
                                            <b class="modal-title">Form Pendaftaran Career Fair <?php echo $data['data']->nama_job_fair ?></b>
                                            <button class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Paket Kerjasama <b class="text-danger">*</b></label>
                                                <select name="id_paket_kerjasama" id="id_paket_kerjasama" class="form-control" required>
                                                    <option value="">Pilih Paket Kerjasama</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Booth <b class="text-danger">*</b></label>
                                                <textarea name="tulisan_di_papan" id="tulisan_di_papan" class="form-control" required style="height: 200px;"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Berkas Formulir <b class="text-danger">*</b></label>
                                                <input type="file" name="url_formulir" class="form-control" onchange="cekFileUpload(this, '')" required>
                                                <span class="file-name text-muted text-bold ml-5"></span>
                                                <span class="file-alert-type text-danger text-bold"></span>
                                                <span class="file-alert-size text-danger text-bold"></span>
                                                <div class="hint-block mt-3">
                                                    Jenis file yang diijinkan: <strong>JPEG, JPG, PNG, BMP, PDF</strong> dengan ukuran file maksimal: <strong>2 MB</strong><br>
                                                </div>
                                                <div id="file-preview"></div>
                                            </div>
                                        </div>
                                        <div class="modal-footer d-flex justify-content-center">
                                            <input type="hidden" name="id_career_fair" value="<?php echo $data['data']->id ?>">
                                            <button class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
                                            <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Daftar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <h4>Data pendaftaran Anda</h4>
                        <table class="table table-sm borderless">
                            <tr>
                                <th>Waktu Pendaftaran</th>
                                <th>:</th>
                                <td><?php echo $this->tanggalindo->konversi_tgl_jam($registered['data']->waktu_pendaftaran) ?></td>
                            </tr>
                            <tr>
                                <th>Pilihan Paket</th>
                                <th>:</th>
                                <td><?php echo $registered['data']->paket ?></td>
                            </tr>
                            <tr>
                                <th>Tulisan Papan</th>
                                <th>:</th>
                                <td><?php echo $registered['data']->tulisan_di_papan ?></td>
                            </tr>
                            <tr>
                                <th>Formulir</th>
                                <th>:</th>
                                <td>
                                    <a href="<?php echo $registered['data']->url_formulir ?>" target="_blank" class="btn btn-info btn-sm"><i class="fas fa-download"></i> Unduh</a>
                                </td>
                            </tr>

                            <tr>
                                <th>Status Pendaftaran</th>
                                <th>:</th>
                                <td>
                                    <?php
                                    switch ($registered['data']->is_approved) {
                                        case '0':
                                            echo '<span class="badge bg-warning">Menunggu verifikasi</span>';
                                            break;

                                        case '1':
                                            echo '<span class="badge bg-success">Valid</span>';
                                            break;

                                        case '2':
                                            echo '<span class="badge bg-danger">Tidak Valid</span>';
                                            break;

                                        default:
                                            echo '<span class="badge bg-warning">Menunggu verifikasi</span>';
                                            break;
                                    }
                                    ?>
                                </td>
                            </tr>

                        </table>
                    <?php endif; ?>
                <?php else : ?>
                    <b>Daftar Mitra</b>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered table-sm" id="tabel-mitra">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Jenis Perusahaan/Sektor</th>
                                    <th>Nama Booth</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url('public/vendor/datatables/datatables.min.js'); ?>"></script>
<script>
    var site_url = '<?php echo base_url() ?>';
    $(document).ready(function() {
        $('#btn-registrasi').click(function(e) {
            e.preventDefault();

            $.getJSON(
                `${site_url}career_fair/index/paket`,
                null,
                function(data, textStatus, jqXHR) {
                    if (data.status != 500) {
                        $('#form-pendaftaran').find('select[name=id_paket_kerjasama]').html('<option value="">Pilih Paket Kerjasama</option>')
                        data.data.forEach((element) => {
                            $('#form-pendaftaran').find('select[name=id_paket_kerjasama]').append(`<option value="${element.id}">${element.paket}</option>`);
                            //type here
                        });
                    }
                    $('#modal-pendaftaran').modal('show');
                });
        });

        $('#form-pendaftaran').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'ajax',
                method: 'POST',
                url: site_url + 'career_fair/index/daftar',
                data: new FormData($('#form-pendaftaran')[0]),
                contentType: false,
                processData: false,
                dataType: 'json',
                beforeSend: function() {},
                success: function(response) {
                    if ((typeof response === 'string' || response instanceof String)) {
                        swal('Gagal!', 'Aplikasi gagal terhubung dengan server. Silahkan hubungi admin.', 'error');
                    }
                    if (response.status == 201) {
                        swal('Berhasil!', response.message, 'success').then(function() {
                            location.reload();
                        })
                    }
                },
                error: function(xmlhttprequest, textstatus, message) {
                    // text status value : abort, error, parseerror, timeout
                    // default xmlhttprequest = xmlhttprequest.responseJSON.message
                    swal('Gagal!', xmlhttprequest.responseJSON.message, 'error');
                    console.log(xmlhttprequest.responseJSON);
                },
            });
        })
    })
</script>