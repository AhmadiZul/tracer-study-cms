<?php
if (isset($output->css_files)) {
    foreach ($output->css_files as $file) {
        echo '<link type="text/css" rel="stylesheet" href="' . $file . '"/>';
    }
}
?>
<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <small>
                    <?php if (isset($output)) : ?>
                        <?php echo $output->output; ?>
                    <?php endif; ?>
                </small>
            </div>
        </div>
    </div>
</div>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>