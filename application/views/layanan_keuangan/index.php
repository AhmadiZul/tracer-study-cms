<section id="layanan">
    <div class="row p-5">
        <?php foreach ($layanan as $key => $value) { ?>
            <?php
            $url_logo = base_url() . 'public/assets/img/logo-kosong.webp';
            $url_web = base_url();
            if (isset($value->url_logo)) {
                $url_logo = $value->url_logo;
            }
            if (isset($value->url_web)) {
                $url_web = $value->url_web;
            }
            ?>
            <div class="card col-md-4">
                <div class="card-body text-center">
                    <a href="javascript:cvKeuangan('<?= $url_web ?>','<?= $value->id_layanan ?>')">
                        <div class="br-mg">
                            <img src="<?= $value->url_logo ?>" class="img-layanan_keuangan">
                        </div>
                    </a>
                    <span id="viewer-<?= $value->id_layanan ?>" class="badge px-2 py-1 rounded-pill float-right font-weight-light"><i class="fas fa-eye"></i> <?php echo $value->visitor ?></span>
                </div>
                <div class="card-body">
                    <span class="badge">
                        <!-- <a href="javascript:cvKeuangan('<?= $url_web ?>','<?= $value->id_layanan ?>')" class="font-weight-light text-info"><i class="fa fa-circle-info"></i> more</a> -->
                    </span>

                </div>
            </div>
        <?php } ?>
    </div>
</section>

<script>
    var site_url = '<?php echo base_url() ?>';

    function cvKeuangan(url, id) {
        $.ajax({
            type: 'ajax',
            method: 'POST',
            url: site_url + 'index/cvKeuangan',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response) {
                if (response.success) {
                    $('#viewer-' + response.kode).html('<i class="fas fa-eye"></i> ' + response.data.viewer + ' Viewer');
                    window.open(
                        url,
                        '_blank' // <- This is what makes it open in a new window.
                    );
                } else {
                    swal({
                        title: 'Galat',
                        text: 'Hubungi admin',
                        type: 'error',
                        confirmButtonColor: '#396113',
                    });
                }
            },
            error: function(xmlhttprequest, textstatus, message) {
                // text status value : abort, error, parseerror, timeout
                // default xmlhttprequest = xmlhttprequest.responseJSON.message
                console.log(xmlhttprequest.responseJSON);
            },
        });
    }
</script>