<main id="main">
    <section id="profile">
        <div class="container">
            <div class="d-flex justify-content-center">
                <img src="<?php echo base_url('public/assets/img/404.png') ?>" class="img-fluid" width="714" height="417.42" alt="">
            </div>
                <h2 class="text-center fw-bold">
                    <b><span class="text-dark">Error</span>
                    <span class="text-danger">404</span></b>
                </h2>
                <p class="text-center">Halaman yang Anda akses tidak dapat ditampilkan. Kami sedang <br> dalam proses perbaikan dan akan kembali secepatnya</p>    
        </div>
    </section>
</main>