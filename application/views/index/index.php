<section id="hero-index" class="hero-index ">
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner vh-100" style="position: relative; height: auto;">

            <?php foreach ($background as $key => $value) { ?>
                <div class="carousel-item <?php echo ($key == 0 ? 'active' : '') ?>">

                    <img src="<?php echo URL_FILE . $value->url_file ?>" class="img-fluid" alt="..."></img>
                </div>
            <?php } ?>

            <div class="text-center caption carousel-bg">
                <br><br><br><br><br><br><br>
                <h2 class="animate__animated animate__fadeInDown text-title font-rubik-bold pb-2"><?= $banner->title ?>
                </h2>
                <h1 class="animate__animated animate__fadeInUp font-rubik-bold pb-2"><strong class="text-univ"><?= $banner->sub_title ?></strong>
                </h1>
                <h6 class="animate__animated animate__fadeInUp text-title pb-2"><?= $banner->deskripsi ?></h6>
                <a href="<?php echo base_url() . 'alumni' ?>" class="btn btn-login animate__animated animate__fadeInUp scrollto">Mulai Mengisi Kuesioner</a>
                <br><br><br><br><br><br><br><br>
            </div>
        </div>
    </div>
</section>
<section id="navs" class="navs" style="margin-top: -30px;">
    <div class="bg-navs">
        <div class="container" data-aos="fade-up" style="margin-top: -110px;">
            <div class="icon-box">
                <div class="section-title mt-3">
                    <div class="text-center">
                        <h4 class="font-weight-bold">Alur Pengisian Tracer Study</h4>
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-sm-3">
                        <div class="card-1">
                            <div class="card-body">
                                <div class="d-flex justify-content-center">
                                    <h4 class="text-center bg w-100 text-white py-3"><i class="fas fa-desktop"></i></h4>
                                </div>
                                <h5 class="card-title font-rubik-bold text-tema  text-center">Registrasi</h5>
                                <p class="card-text">Mengakses halaman registrasi klik tombol <a href="<?php echo base_url() ?>register/index" class="buat-akun">Daftar
                                        disini</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card-1">
                            <div class="card-body">
                                <div class="d-flex justify-content-center">
                                    <h4 class="text-center bg w-100 text-white py-3"><i class="fas fa-file-alt"></i>
                                    </h4>
                                </div>
                                <h5 class="card-title font-rubik-bold text-tema  text-center">Isi data diri</h5>
                                <p class="card-text">Mengisi data diri alumni dengan lengkap dan tepat.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card-1">
                            <div class="card-body">
                                <div class="d-flex justify-content-center">
                                    <h4 class="text-center bg w-100 text-white py-3"><i class="fas fa-folder-plus"></i>
                                    </h4>
                                </div>
                                <h5 class="card-title font-rubik-bold text-tema  text-center">Login</h5>
                                <p class="card-text">Masuk dengan akun yang sudah terdaftar</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card-1">
                            <div class="card-body">
                                <div class="d-flex justify-content-center">
                                    <h4 class="text-center bg w-100 text-white py-3"><i class="fas fa-scroll"></i>
                                        </h1>
                                </div>
                                <h5 class="card-title font-rubik-bold text-tema  text-center">Mengisi Kuesioner</h5>
                                <p class="card-text">Pilih jenis tracer study kalian lalu isi pertanyaan survey</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <div class="container fluid">
            <ul class="row nav nav-pills nav-justified">
                <li class="col-sm-4 nav-item">
                    <a data-toggle="tab" class="text-center nav-link active" href="#home"><strong class="text-light">Pengisian Kuesioner</strong></a>
                </li>
                <li class="col-sm-4 nav-item ">
                    <a data-toggle="tab" class="text-center nav-link " href="#menu1"><strong class="text-light">Laporan</strong></a>
                </li>
                <li class="col-sm-4 nav-item">
                    <a data-toggle="tab" class="text-center nav-link" href="#menu2"><strong class="text-light">Jumlah
                            Lulusan</strong></a>
                </li>
            </ul>
            <div class="card">
                <!-- <div class="card-header"> -->
                <!-- </div> -->
                <div class="card-body">
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in show active pengisian-kuesioner ">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 d-flex align-items-stretch mb-5">
                                    <div class="col mt-4">
                                        <h3 class="text-center"><strong>Pengisian Kuesioner</strong></h3>
                                        <?php if (!empty($jenis_survey)) { ?>
                                            <div class="row container">
                                                <?php foreach ($jenis_survey as $key => $value) { ?>
                                                    <div class="col mt-3">
                                                        <div class="card">
                                                            <div class="card-body text-center">
                                                                <img src="<?php echo base_url() ?>public/assets/img/tracer_alumni/image38.png" class="img img-fluid" width="100%" alt=""><br><br>
                                                                <b style="margin-top:100px;"><?= $value->jenis_survey ?></b>
                                                                <p class="text-black mt-3 mb-3"><?= $value->deskripsi ?></p><br>
                                                                <div class="center">
                                                                    <a href="<?php echo base_url() ?>tracer?id=1" class="btn btn-login mt-4 center"><b>Isi Kuesioner
                                                                            </i></b></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } else { ?>
                                            <h4 class="text-center">Jenis Kuesioner belum ditambahkan</h4>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="laporan tab-pane fade show">
                            <div class="row">
                            <?php foreach ($laporan as $key => $value) { ?>
                                <div class="col-sm-4">
                                    <div class="card-header" style="border-radius: 10px;">
                                        <h4 class="card-title text-center text-dark" style="margin-top: 10px;"><strong><?= $value->judul ?>
                                                <?= $value->tahun?> </strong></h4>
                                    </div>
                                    
                                    <p class="mt-3">
                                        <?= $value -> deskripsi ?>
                                    </p>
                                    <a href="<?= base_url("index/download?file=admin/$value->url_file") ?>" class="btn btn-laporan text-white">Unduh Laporan</a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div id="menu2" class="laporan tab-pane fade show">
                            <div class="form-group row text-center">
                                <?php foreach ($totalRekap as $key => $value) {?>
                                               
                                <div class="col"></div>
                                <div class="col-md-3 bg-tahun">
                                    <h2 style="margin-top: 60px;">Tahun Lulus <?=$value[0]->ref_tahun ?></h2>
                                    <h1><strong class="text-tema"><?= $value[0]->total_alumni?></strong></h1><br><br><br>
                                </div>
                            <?php } ?>
                                <div class="col"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</section>
<main id="main">
    <section id="mitra" class="mitra">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-12" data-aos="fade-down">
                    <div class="form-group row">
                        <div class="col-md-11 text-center" style="margin-top: 40px;">
                            <h1><strong class="text-danger"><?= $totalMitra ?></strong></h1>
                            <strong>Mitra kami</strong>
                        </div>
                        <div class="col-md-0">
                            <div class="garis_vertikal"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-sm-12">
                    <div class="owl-carousel slider-mitra-kami img-mitra">
                        <?php foreach ($mitra as $key => $value) { ?>
                            <div class="card">
                                <div class="slider-mitra">
                                    <img class="img img-fluid rounded " src="<?php echo URL_FILE . $value->url_logo ?>" alt="" width="450" height="400">
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
    </section>

    <section id="greeting" class="greeting">
        <div class="container">
            <?php if (!empty($greeting)) { ?>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="section-title">
                            <div class="form-group row">
                                <div class="col-md-6 col-sm-12">
                                    <h4 class="font-weight-bold">Sambutan Rektor</h4>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <hr class="bghr" style="width:165%; height:3px;">
                                </div>

                            </div>
                        </div>
                        <h6 class="px-5 font-italic" data-aos="fade-right"><?= $greeting->deskripsi ?></h6>
                        <h6 class="px-5 font-weight-bold"><?= $greeting->title ?></h6>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <img class="img img-fluid" src="<?php echo URL_FILE . $greeting->url_file ?>" alt="">
                    </div>
                </div>
            <?php } else { ?>
                <h4 class="text-center">Jenis Kuesioner belum ditambahkan</h4>
            <?php } ?>
        </div>

    </section>

    <!--VIDEO PENGENALAN -->
    <section id="about" class="video-pengenalan">
        <?php if (!empty($setting_video)) { ?>
            <div class="bg container-fluid">
                <?php
                if ($setting_video) { ?>
                    <div class="form-group row icon-box">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <!-- <div class="col-md-6 col-sm-12"></div> -->
                                    <div class="pull-right"> <iframe width="500" height="315" src="<?php echo $setting_video->url_link ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" class="video-pengenalan" allowfullscreen></iframe></div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <h4 class="font-weight-bold" data-aos="fade-left"><?php echo $setting_video->title ?></h4>
                                    <p><?php echo $setting_video->deskripsi ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }/* endforeach */; ?>
            </div>
        <?php } else { ?>
            <h4 class="text-center text-light">Video belum tersedia</h4>
        <?php } ?>
    </section>
    <!--END VIDEO PENGENALAN -->

    <!-- ======= Why Us Section ======= -->
    <section id="alumni-berprestasi" class="alumni-berprestasi">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="text-left font-weight-bold">Alumni Berprestasi</h4>
                    </div>
                    <div class="col-md-4">
                        <hr class="bghr" style="width:150%; height:3px;">
                    </div>
                    <div class="col-md-4">
                        <a class="btn my-previous-button " style="padding-left:50%;">
                            <h4><i class='fa fa-arrow-circle-left text-tema'></i></h4>
                        </a>
                        <a class="btn my-next-button">
                            <h4><i class='fa fa-arrow-circle-right'></i></h4>
                        </a>
                    </div>
                </div>
            </div>
            <?php if (!empty($alumni_berprestasi)) { ?>
                <div class="owl-carousel slider-why-us">
                    <?php foreach ($alumni_berprestasi as $key => $value) { ?>
                        <div class="slider-alumni">
                            <div class="card" style="width:250px; height:450px; margin-left:30px;">
                                <div class="card-header bg-white">
                                    <img class="img" src="<?php echo URL_FILE . $value->url_foto ?>" alt="" style="width:100%; height:290px;">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title font-weight-bold"><?= $value->nama_alumni ?></h4>
                                    <p class="card-text" style="font-size:1vw;"><?= substr(preg_replace("/<.*?>/", "", $value->nama_prestasi), 0, 50) ?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <h4 class="text-center">Belum ada data</h4>
            <?php } ?>
        </div>
    </section><!-- End Why Us Section -->
    <!-- ======= Team Section ======= -->
    <section id="berita" class="berita">
        <div class="container">
            <div class="section-title">
                <div class="row">
                    <div class="col-md-2">
                        <h4 class="text-left font-weight-bold">Tips Karir</h4>
                    </div>
                    <div class="col-md-10">
                        <hr class="bghr" style="width:100%; height:3px;">
                    </div>
                </div>
            </div>
            <h3 class="leading-berita"></h3>
            <div class="media mb-4">
                <?php if (!empty($tips_karir)) { ?>
                    <div class="row">
                        <div class="col-md-6 col-sm-12" data-aos="fade-right">
                            <img src="<?php echo URL_FILE . $tips_karir[0]->url_photo ?>" class="img-fluid" alt="..." style="width:100%; height:300px;">
                            <a href="<?= base_url() . 'tips/' . $tips_karir[0]->id_berita ?>">
                                <h4 class="mt-2 text-dark font-weight-bold"><?= $tips_karir[0]->judul ?></h4>
                            </a>
                            <p class="text-danger"><?= $tips_karir[0]->created_at ?></p>
                            <p><?= substr(preg_replace("/<.*?>/", "", $tips_karir[0]->deskripsi), 0, 90) ?>
                                <a class="text-tema" href="<?= base_url() . 'tips/' . $tips_karir[0]->id_berita ?>"><b>Lihat
                                        Selengkapnya</b></a>
                            </p>
                        </div>
                        <div class="col-md-6 col-sm-12" data-aos="fade-left">
                            <?php foreach ($tips_karir as $key => $value) { ?>
                                <?php if ($key > 0 && $key < 5) : ?>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <img src="<?php echo URL_FILE . $value->url_photo ?>" class="img-fluid" alt="..." style="width:120%; height:110px;">
                                        </div>
                                        <div class="col-md-8 col-sm-12">
                                            <a class="" href="<?= base_url("tips/$value->id_berita") ?>">
                                                <h6 class=" text-dark font-weight-bold"><?= $value->judul ?></h6>
                                            </a>
                                            <p class="text-danger" style="font-size:13px;"><?= $value->created_at ?></p>
                                            <p class="" style="font-size:13px;"> <?= substr(preg_replace("/<.*?>/", "", $value->deskripsi), 0, 90) ?>
                                                <a class="text-tema" href="<?= base_url() . 'tips/' . $value->id_berita ?>"><b>Lihat
                                                        Selengkapnya</b></a>
                                            </p>
                                            <hr>
                                        </div>
                                    </div>
                                <?php endif ?>
                            <?php } ?>
                            <div class="media-body pt-2 text-right">
                                <a class="text-danger" href="<?php echo base_url() . 'tips_karir' ?>"><b>Lihat
                                        Selengkapnya</b></a>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <h4 class="text-center">Tips karir belum tersedia</h4>
                <?php } ?>
            </div>
        </div>
    </section>


    <section id="agenda" class="agenda">
        <div class="container" data-aos="fade-up">
            <div class="section-title">
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="text-left font-weight-bold">Agenda Terbaru</h4>
                    </div>
                    <div class="col-md-8">
                        <hr class="bghr" style="width:90%; height:3px;">
                    </div>
                </div>
            </div>
            <?php if (!empty($agenda)) { ?>
                <div class="card-deck">
                    <?php foreach ($agenda as $key => $value) { ?>
                        <div class="card" style="width:260px;">
                            <img src="<?php echo URL_FILE . $value->flyer ?>" class="card-img-top img-fluid" alt="..." style="width:100%; height:250px;">
                            <div class="card-body">
                                <a class="card-title" href="<?= base_url("agenda/index?key=" . $value->id) ?>">
                                    <h5 class=" text-dark font-weight-bold"><?= $value->nama ?></h5>
                                </a>
                                <p class="card-text"><?= substr(preg_replace("/<.*?>/", "", $value->deskripsi), 0, 90) ?>
                                    <a class="text-tema" href="<?= base_url() . 'agenda/index?key=' . $value->id ?>"><b>Lihat
                                            Selengkapnya</b></a>
                                </p>
                                <p class="card-text badge p-2 rounded-pill bghr mt-2"><small class="text-light"><?php echo $value->waktu_mulai ?></small></p>
                                <p class="card-text px-2" style="font-size:12px;"><?= $value->waktu_acara ?> - <?= $value->waktu_berakhir ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <h4 class="text-center">Agenda belum tersedia</h4>
            <?php } ?>
            <div class="media-body py-2 text-right">
                <a class="text-danger" href="<?php echo base_url() . 'tips_karir' ?>"><b>Lihat Selengkapnya</b></a>
            </div>
        </div>
    </section>

    <section id="faq" class="faq">
        <div class="container">
            <div class="section-title">
                <div class="row">
                    <div class="col-md-4">
                        <hr class="bghr" style="width:100%; height:3px;">
                    </div>
                    <div class="col-md-4">
                        <h4 class="align-items-center font-weight-bold">FAQ Tracer Study<br><?= $banner->sub_title ?>
                        </h4>
                    </div>
                    <div class="col-md-4">
                        <hr class="bghr" style="width:100%; height:3px;">
                    </div>
                </div>
            </div>
            <div class="form-group row" data-aos="fade-up">
                <div class="col-md-12">
                    <?php foreach ($getfaq as $key => $value) { ?>
                        <div class="menu-faq">
                            <a type="button" class="accordion"><?= $value->pertanyaan ?><i class="fa fa-chevron-down"></i></a>
                            <div class="panel">
                                <p><?= $value->jawaban ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</main>

<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
</script>
<style>
    #faq .menu-faq {
        margin-bottom: 20px;
    }

    #faq i {
        float: right;
        margin-top: 5px;
        color: var(--warna);
        font-weight: bold;
    }

    #faq .accordion {
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
        font-weight: bold;
        border-top-right-radius: 15px;
        border-bottom-left-radius: 15px;
    }

    #faq .active,
    .accordion:hover {
        background-color: var(--warna) !important;
        color: #fff !important;
    }

    #faq .panel {
        padding: 0 18px;
        display: none;
        font-weight: lighter;
        background-color: white;
        overflow: hidden;
        box-shadow: 10px 10px 30px rgba(127, 137, 161, 0.25);
    }

    #faq .panel p {
        margin-top: 13px;
    }
</style>