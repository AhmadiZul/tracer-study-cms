<!-- ======= Hero Section ======= -->
<section id="carousel" class="carousel">
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="row align-items-center">
                    <div class="col-md-4 col-sm-12 px-5  caption">
                        <h2 class="font-weight-bold">Belum Mempunyai </h2>
                        <h2 class="text-danger font-weight-bold">Tracer Study </h2>
                        <h2 class="font-weight-bold">untuk Perguruan Tinggi Anda?</h2>
                        <p>Percayakan pembuatan website tracer study Anda pada kami, karena kami disini memberikan pelayanan yang terbaik untuk klien kami.</p>
                        <div class="center">
                            <a href="" class="btn btn-danger  center"><b>Diskusikan Bersama Kami</i></b></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 image">
                        <img src="<?php echo URL_FILE . '/public/assets/img/branding/banner.png'?>" class="d-block w-100" alt="...">
                    </div>
                </div>
            </div>
            <div class="carousel-item">
            <div class="row align-items-center">
                    <div class="col-md-8 col-sm-12 image2">
                        <img src="<?php echo URL_FILE . '/public/assets/img/branding/banner_2.png'?>" class="d-block w-100" alt="...">
                    </div>
                    <div class="col-md-4 col-sm-12 px-5 caption">
                        <h2 class="font-weight-bold">Apa itu </h2>
                        <h2 class="text-danger font-weight-bold">Tracer Study?</h2>
                        <p>Tracer Study adalah studi penelusuran alumni untuk mengetahui kegiatan alumni setelah lulus dari Perguruan Tinggi, transisi dari dunia pendidikan tinggi ke dunia kerja, situasi kerja, pemerolehan kompetensi, penggunaan kompetensi dalam pekerjaan dan perjalanan karir.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hero -->

<main id="main">

    <section id="icon-boxes" class="icon-boxes" style="padding-bottom: 0px !important;">
        <div class="container">
            <div class="row">
            <div class="col-md-12 col-sm-12 d-flex align-items-stretch" data-aos="fade-up">
                <div class="owl-carousel mitra-carousel icon-box">
                    <div class="card">
                        <div class="card-body align-items-center d-flex flex-column justify-content-center">
                            <img src="<?php echo base_url() . '/admin/public/uploads/mitra/tes/delta.png'?>" alt="" width="140" height="100" class="img-mitra">
                        </div>
                    </div>
                    <div class="card my-0">
                        <div class="card-body align-items-center d-flex flex-column justify-content-center">
                            <img src="<?php echo base_url() . '/admin/public/uploads/mitra/tes/arkatama.png'?>" alt=""  width="140" height="100" class="img-mitra">
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>


    <section id="about" class="video-pengenalan">
        <div class="container mt-4" data-aos="fade-up">
            <div class="row icon-box content">
                <div class="col-md-6 col-sm-12">
                    <h4>Profil Perusahaan Jasa Konsultan IT dan Software House Terbaik di Malang</h4>
                    <p>PT Arkatama Multi Solusindo sebagai jasa konsultan IT siap membantu masalah digitalisasi perusahaan Anda. Kami menawarkan berbagai layanan IT  seperti, Custom Software Application, Hadware and IT Infrastucture, Digital Markering Service, dan Training Center of IT. 
                        Info lebih lanjut silahkan kunjungi: https://arkatama.id/</p>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="my-auto">
                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/MLprbkEPw5s"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            class="video-pengenalan" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="tujuan" class="tujuan">
        <div class="container" data-aos="fade-up">
            <div class="row">
                 <div class="col-md-6 col-sm-12">
                    <img src="<?php echo base_url() . '/admin/public/assets/img/branding/landing2.png'?>"
                        class="rounded float-right" alt="..." width="570" height="500">
                </div>
                <div class="col-md-6 col-sm-12">
                    <h2 class="font-weight-bold text-danger">Kenapa Tracer Study dari Kami ?</h2>
                    <p>Kami telah telah dipercaya berbagai korporasi, instansi pemerintah, instansi pendidikan, dan lainya. 
                        Tidak hanya berbasis project, tapi kami dipercaya juga untuk kerjasama dalam jangka panjang.</p>
                    <div class="px-4">
                        <h5 class="text-align-right"><i class="fa-solid fa-user text-danger"></i>
                            <span class="px-3">Berpengalaman, cepat dan professional.</span>
                        </h5>
                        <p class="text-align-right">Project tracer study sudah banyak dikerjakan dengan testimoni yang memuaskan dari pihak klien. 
                            Dipercaya banyak instansi pendidikan dan instansi pemerintahan terkait pengerjaan yang professional dan cepat</p>
                        <h5 class="text-align-right"><i class="fa-solid fa-lock text-danger"></i>
                            <span class="px-3">Kerahasian data Aman dan terpercaya.</span>
                        </h5>
                        <p class="text-align-right">Data para alumni maupun instansi perguruan tinggi akan terjaga sebagaimana data dan asset perguruan tinggi. 
                            Karena kita juga mmelakukkan pemeliharaan sistem secara berkelanjutan</p>
                        <h5 class="text-align-right"><i class="fa-solid fa-thumbs-up text-danger"></i>
                            <span class="px-3">Pengembangan dengan fitur CMS</span>
                        </h5>
                        <p class="text-align-right">Content Management System (CMS) adalah sebuah aplikasi yang berfungsi untuk mengelola, mengubah, dan mempublikasikan konten web. 
                            Jadi admin perguruan tinggi dapat mengkustom sendiri terkait konten perguruan tinggi.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="manfaat" class="manfaat">
        <div class="container-fluid" data-aos="fade-up">
            <div class="row">
                <div class="col-md-12">
                    <img src="<?php echo base_url('public/assets/img/landingpage/landing3.png') ?>"
                        class="rounded float-right" width="520" height="480">
                    <div class="col-md-8">
                        <div class="card-body py-2 ">
                            <h2 class="card-title px-3 text-danger font-weight-bold">Kenapa Tracer Study Diperlukan</h2>
                            <div class="card-text px-3">
                                <p>Tracer study sangat bermanfaat tidak hanya bagi lulusan, tetapi perguruan tinggi dan
                                    perusahaan juga.</p>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-6">
                                            <h5 class="text-align-right"><i class="fas fa-graduation-cap text-danger"></i>
                                                <span class="px-3">Alumni</span>
                                            </h5>
                                            <p class="text-align-right">Project tracer study sudah banyak dikerjakan dengan
                                                testimoni yang memuaskan dari pihak klien.
                                                Dipercaya banyak instansi pendidikan dan instansi pemerintahan terkait
                                                pengerjaan yang professional dan cepat</p>
                                        </div>
                                        <div class="col-6">
                                            <h5 class="text-align-right"><i class="fas fa-university text-danger"></i>
                                                <span class="px-3">Perguruan Tinggi</span>
                                            </h5>
                                            <p class="text-align-right">Mengetahui penyebaran alumni dalam dunia kerja
                                                sebagai evaluasi perguruan tinggi terhadap lulusan yang dihasilkan.</p>
                                        </div>
                                        <div class="col-6">
                                            <h5><i class="fas fa-city text-danger"></i>
                                                <span class="col-md-3">Mitra</span>
                                            </h5>
                                            <p class="text-align-right">Perusahaan dengan mudah melakukan cross check
                                                lulusan ataupun data alumni yang melamar di perusahaan meraka.</p>
                                        </div>
                                        <div class="col-6">
                                            <h5><i class="fas fa-check-circle text-danger"></i>
                                                <span class="col-md-3">Dikti</span>
                                            </h5>
                                            <p class="text-align-right">Sebagai alat monitoring Ditjen DIKTI terhadap
                                                lulusan perguruan tinggi ketika memasuki dunia kerja.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="demo" class="demo">
        <div class="container-fluid" data-aos="fade-up">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                <img src="<?php echo base_url() . '/admin/public/assets/img/branding/demo.png'?>"
                        class="rounded float-right" alt="..." >
                </div>
                <div class="col-md-6 col-sm-12">
                    <h2 class="py-3 font-weight-bold">Demo project</h2>
                    <p>Preview website Tracer Study kami pada tampilan dashboard untuk admin dan landing page untuk para alumni.</p>
                <div class="center">
                    <a href="<?php echo base_url() ?>" class="btn btn-danger mt-4 center"><b>Preview Demo </i></b></a>
                </div>
                </div>
                
            </div>
        </div>
    </section>

    <section id="build" class="build">
        <div class="container-fluid bg"data-aos="fade-up">
            <h4 class="text-light text-center py-5">Mulai Buat Tracer Study Untuk Perguruan Tinggi Anda</h4>
            <p class="text-light text-center">Selalu siap menghadirkan layanan terbaik untuk kebutuhan perguruan tinggi anda.</p>
            <div class="text-center">
                <a href="" class="btn btn-danger"><b>Hubungi Kami</i></b></a>
            </div>
        </div>
    </section>
   
    <section id="testimoni" class="testimoni">
        <div class="container">
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row align-items-center">
                            <div class="col-md-8 col-sm-12 px-5 caption">
                                <h1 class="font-weight-bold">Zulfikar Rahmad</h1>
                                <h2 class="font-weight-bold">Wakil Rektor II Universitas Arkatama</h2>
                                <p>“ Sistem Tracer Study Kami di lingkup Kementerian Pertanian, yang terdiri dari 6 Politeknik Pembangunan Pertanian dan 1 Politeknik Engineering Pertanian menggunakan jasa konsultan perusahaan ini. Sebelumnya kami telah berganti-ganti vendor, tapi selalu bermasalah di dukungan teknis impementasinya. Akan tetapi PT.
                                    Arkatama Multi Solusindo ini tidak hanya membuatkan aplikasi Tracer Study kami tetapi juga membantu permasalahan teknis dan mengawal proses Tracer Study sampai selesai. “</p>
                            </div>
                            <div class="col-md-4 col-sm-12 image">
                                <img src="<?php echo base_url() . '/admin/public/assets/img/branding/testimoni_1.png'?>" class="d-block w-100" alt="..." >
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row align-items-center">
                            <div class="col-md-8 col-sm-12 px-5 caption">
                                <h1 class="font-weight-bold">Dion Maulana</h1>
                                <h2 class="font-weight-bold">Ketua Harian Universitas Arkatama</h2>
                                <p>“ Sistem Tracer Study Kami di lingkup Kementerian Pertanian, yang terdiri dari 6 Politeknik Pembangunan Pertanian dan 1 Politeknik Engineering Pertanian menggunakan jasa konsultan perusahaan ini. Sebelumnya kami telah berganti-ganti vendor, tapi selalu bermasalah di dukungan teknis impementasinya. Akan tetapi PT.
                                    Arkatama Multi Solusindo ini tidak hanya membuatkan aplikasi Tracer Study kami tetapi juga membantu permasalahan teknis dan mengawal proses Tracer Study sampai selesai. “</p>
                            </div>
                            <div class="col-md-4 col-sm-12 image">
                                <img src="<?php echo base_url() . '/admin/public/assets/img/branding/testimoni_2.png'?>" class="d-block w-100" alt="..." >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->