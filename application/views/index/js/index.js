$(document).ready(function () {
    $.getJSON(
        `${site_url}index/getJumlahPeserta`,
        null,
        function (data, textStatus, jqXHR) {
            if (data != null) {
                const result = data.data;
                $('.card-alumni').find('.label-jumlah').text(result.jumlah_alumni);
                $('.card-mitra').find('.label-jumlah').text(result.jumlah_mitra);
                $('.card-lowongan').find('.label-jumlah').text(result.jumlah_lowongan);
                $('.card-magang').find('.label-jumlah').text(result.jumlah_magang);
            }
        });
    $(".lowongan").owlCarousel({
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        items: 4,
        margin: 30,
        smartSpeed: 550,
        loop: false,
        autoWidth: false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            1000: {
                items: 4,
            }
        }
    });
    $(".mitra-carousel").owlCarousel({
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        items: 6,
        margin: 10,
        smartSpeed: 550,
        loop: true,
        autoWidth: false,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        dots:false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 6,
            }
        }
    });
    $(".upt-carousel").owlCarousel({
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        items: 4,
        margin: 10,
        smartSpeed: 550,
        loop: true,
        autoWidth: false,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        dots:false,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3,
            },
            1000: {
                items: 4,
            }
        }
    });
});

var site_url = '<?php echo base_url() ?>';

function cvMitra(url, id) {
    $.ajax({
        type: 'ajax',
        method: 'POST',
        url: site_url + 'index/cvMitra',
        data: {
            id: id
        },
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                $('#viewer-'+response.kode).html('<i class="fas fa-eye"></i> '+response.data.viewer+' Viewer');
                window.open(
                    url,
                    '_blank' // <- This is what makes it open in a new window.
                );
            } else {
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                });
            }
        },
        error: function (xmlhttprequest, textstatus, message) {
            // text status value : abort, error, parseerror, timeout
            // default xmlhttprequest = xmlhttprequest.responseJSON.message
            console.log(xmlhttprequest.responseJSON);
        },
    });
}

function cvKeuangan(url, id) {
    $.ajax({
        type: 'ajax',
        method: 'POST',
        url: site_url + 'index/cvKeuangan',
        data: {
            id: id
        },
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                $('#viewer-'+response.kode).html('<i class="fas fa-eye"></i> '+response.data.viewer+' Viewer');
                window.open(
                    url,
                    '_blank' // <- This is what makes it open in a new window.
                );
            } else {
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                });
            }
        },
        error: function (xmlhttprequest, textstatus, message) {
            // text status value : abort, error, parseerror, timeout
            // default xmlhttprequest = xmlhttprequest.responseJSON.message
            console.log(xmlhttprequest.responseJSON);
        },
    });
}

function cvTips(url, id) {
    $.ajax({
        type: 'ajax',
        method: 'POST',
        url: site_url + 'index/cvTips',
        data: {
            id: id
        },
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                window.location.href = url;
            } else {
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                });
            }
        },
        error: function (xmlhttprequest, textstatus, message) {
            // text status value : abort, error, parseerror, timeout
            // default xmlhttprequest = xmlhttprequest.responseJSON.message
            console.log(xmlhttprequest.responseJSON);
        },
    });
}