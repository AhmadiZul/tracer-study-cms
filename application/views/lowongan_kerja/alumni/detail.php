<a href="<?php echo base_url() ?>lowongan_kerja/alumni/index" class="btn btn-warning mb-3"></i>Kembali</a>
<div class="card">
    <div class="card-header">
        <div class="col-md-12">
            <h2 class="title"><?= $lowongan->posisi ?></h2>
            <span class="font-weight-bold"><?= $lowongan->nama ?></span>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <p for=""><?php echo $lowongan->posisi ?></p>
                <p for=""><b>Jenis Pekerjaan</b> : <?= $lowongan->jenis ?></p>
                <p for=""><b>Lokasi Kerja</b> : <?= $lowongan->nama_kab_kota ?>, <?= $lowongan->nama_provinsi ?></p>
                <p for=""><b>Minimal Pengalaman Kerja</b> : <?= $lowongan->min_pengalaman ?> Tahun</p>
            </div>
            <div class="col-md-6">
                <p for=""><b>Minimal Pendidikan</b> : <?= $lowongan->nama_pendidikan ?></p>
                <p for=""><b>Gaji</b> : Rp. <?= $lowongan->gaji_rate_bawah ?> - Rp. <?= $lowongan->gaji_rate_atas ?></p>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h2>Deskripsi</h2>
    </div>
    <div class="card-body">
        <?= $lowongan->deskripsi ?>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h2>Persyaratan</h2>
    </div>
    <div class="card-body">
        <?= $lowongan->persyaratan ?>
    </div>
</div>