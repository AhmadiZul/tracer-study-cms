<div class="card">
    <div class="card-body">
        <ul class="nav nav-tabs" id="tabLowongan" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="Lowongan-tab" data-toggle="tab" href="#Lowongan" role="tab" aria-controls="Lowongan" aria-selected="true">Lowongan Pekerjaan</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="status-tab" data-toggle="tab" href="#status" role="tab" aria-controls="status" aria-selected="false">Status Lamaran</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="Lowongan" role="tabpanel" aria-labelledby="Lowongan-tab">
                <?php $this->load->view('lowongan_kerja/alumni/daftarlowongan', $lowongan) ?>
            </div>
            <div class="tab-pane fade" id="status" role="tabpanel" aria-labelledby="status-tab">
                <?php $this->load->view('lowongan_kerja/alumni/daftarlamaran', $lamaran) ?>
            </div>
        </div>
    </div>
</div>