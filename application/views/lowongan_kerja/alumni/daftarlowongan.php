<div class="col-md-12">
    <form action="" class="form-inline">
        <div class="row">
            <div class="col">
                <select name="lowongan_filterJenis" id="lowongan__filterJenis" class="form-control select2">
                    <option value="">- Jenis Lowongan -</option>
                    <option value="PART">Part Time</option>
                    <option value="FULL">Full Time</option>
                </select>
            </div>
            <div class="col">
                <select name="lowongan_filterPerusahaan" id="lowongan__filterPerusahaan" class="form-control select2">
                    <option value="">- Nama Perusahaan -</option>
                    <?php foreach ($select_mitra as $key => $value) { ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col">
                <select name="lowongan_filterLokasi" id="lowongan__filterLokasi" class="form-control select2">
                    <option value="">- Lokasi Penempatan -</option>
                    <?php foreach ($select_lokasi as $key => $value) { ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                        <li class="page-item"><button id="btn_prev" class="page-link">Sebelumnya</button></li>
                        <li class="page-item"><span id="page_span" class="page-link disabled"></span></li>
                        <li class="page-item"><button id="btn_next" class="page-link">Selanjutnya</button></li>
                    </ul>
                </nav>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <div class="col-md-2" id="accordion">
        <div class="card">
            <div class="card-header" id="pendidikan">
                <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#pendidikan-tab" aria-expanded="false" aria-controls="pendidikan-tab">
                        Pendidikan
                    </button>
                </h5>
            </div>

            <div id="pendidikan-tab" class="collapse" aria-labelledby="pendidikan" data-parent="#accordion">
                <div class="card-body">
                    <?php foreach ($checkbox_pendidikan as $key => $value) { ?>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="pendidikan[]" id="<?= $value ?>" value="<?= $key ?>">
                            <label class="custom-control-label" for="<?= $value ?>"><?= $value ?></label>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-10" id="tabel_lowongan">
    </div>
</div>

<script>
    var site_url = '<?php echo site_url() ?>lowongan_kerja/alumni/index/';
    let current_page = 1;
    let records_per_page = 19;
    let body = $('body');

    let lowongan__filterJenis = '';
    let lowongan__filterPerusahaan = '';
    let lowongan__filterLokasi = '';

    let objJson = JSON.parse(`<?php echo isset($lowongan) ? json_encode($lowongan) : NULL; ?>`);

    function prevPage() {
        if (current_page > 1) {
            current_page--;
            changePage(current_page);
        }
    }

    function nextPage() {
        if (current_page < numPages()) {
            current_page++;
            changePage(current_page);
        }
    }

    function changePage(page) {
        // array objJson baru
        let tempObjJson = objJson.map(function(obj) {
            return obj;
        });

        // FILTER JENIS
        if (lowongan__filterJenis != '') {
            tempObjJson = tempObjJson.filter(function(obj) {
                return obj['jenis'] == this.data;
            }, {
                data: lowongan__filterJenis
            });
        };

        // FILTER PERUSAHAAN
        if (lowongan__filterPerusahaan != '') {
            tempObjJson = tempObjJson.filter(function(obj) {
                return obj['id_instansi'] == this.data;
            }, {
                data: lowongan__filterPerusahaan
            });
        };

        // FILTER LOKASI
        if (lowongan__filterLokasi != '') {
            tempObjJson = tempObjJson.filter(function(obj) {
                return obj['lokasi_kerja'] == this.data;
            }, {
                data: lowongan__filterLokasi
            });
        };

        // FILTER PENDIDIKAN
        let temp = [];
        $('[name="pendidikan[]"]:checked').each(function() {
            let data = $(this).val();
            Object.keys(tempObjJson).forEach(function(item) {
                if (tempObjJson[item]['id_pendidikan'] == data) {
                    temp[item] = tempObjJson[item];
                }
            });
        });
        if ($('[name="pendidikan[]"]').serializeArray().length !== 0) {
            tempObjJson = temp
        }

        // Validate page
        if (page < 1) page = 1;
        if (page > numPages(tempObjJson)) page = numPages(tempObjJson);

        $('#tabel_lowongan').html('');

        if (tempObjJson.length <= 0) {
            $('#btn_prev, #btn_next').css('visibility', 'hidden');
            $('#page_span').html('0/0');
            return;
        }

        // MEMBUAT CARD HTML
        let tempAllCard = '';
        for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < tempObjJson.length; i++) {
            tempAllCard += renderCard(tempObjJson[i]);
        };

        $('#tabel_lowongan').html(tempAllCard);
        $('#page_span').html(page + '/' + numPages(tempObjJson));

        if (page == 1) {
            $('#btn_prev').css('visibility', 'hidden');
        } else {
            $('#btn_prev').css('visibility', 'visible');
        };

        if (page == numPages(tempObjJson)) {
            $('#btn_next').css('visibility', 'hidden');
        } else {
            $('#btn_next').css('visibility', 'visible');
        };
    };

    function numPages(obj = null) {
        if (obj === null) {
            return Math.ceil(objJson.length / records_per_page);
        };
        return Math.ceil(obj.length / records_per_page);
    }

    $(document).ready(function() {
        if (objJson) {
            lowongan__filterJenis = $('#lowongan__filterJenis').val();
            lowongan__filterPerusahaan = $('#lowongan__filterPerusahaan').val();
            lowongan__filterLokasi = $('#lowongan__filterLokasi').val();
            changePage(1);
            $('#btn_prev').on('click', prevPage);
            $('#btn_next').on('click', nextPage);

            $('#lowongan__filterJenis').on('change', function() {
                lowongan__filterJenis = $(this).val();
                changePage(1);
            });
            $('#lowongan__filterPerusahaan').on('change', function() {
                lowongan__filterPerusahaan = $(this).val();
                changePage(1);
            });
            $('[name="pendidikan[]"').on('click', function() {
                changePage(1);
            });

            body.addClass('sidebar-mini');
            body.removeClass('sidebar-show');

            $(".main-sidebar .sidebar-menu > li").each(function() {
                let me = $(this);

                if (me.find('> .dropdown-menu').length) {
                    me.find('> .dropdown-menu').hide();
                    me.find('> .dropdown-menu').prepend('<li class="dropdown-title pt-3">' + me.find('> a').text() + '</li>');
                } else {
                    me.find('> a').attr('data-toggle', 'tooltip');
                    me.find('> a').attr('data-original-title', me.find('> a').text());
                    $("[data-toggle='tooltip']").tooltip({
                        placement: 'right'
                    });
                }
            });
        };
    });

    function renderCard(obj) {
        let id_lowongan = obj['id_lowongan_pekerjaan'] ? obj['id_lowongan_pekerjaan'] : '';
        let logo = obj['url_logo'] ? obj['url_logo'] : '<?php echo base_url() ?>public/assets/img/logo-kosong.webp';
        let nama = obj['nama'] ? obj['nama'] : '';
        let posisi = obj['posisi'] ? obj['posisi'] : '';
        let tempJenis = obj['jenis'] ? obj['jenis'] : '';
        let lokasi = obj['lokasi_kerja'] ? obj['nama_kab_kota'] + ', ' + obj['nama_provinsi'] : '';
        let endPublish = obj['tgl_selesai'] ? obj['tgl_selesai'] : '';
        let diperbarui = (obj['diperbarui'] ? obj['diperbarui'] : '');
        let pendidikan = obj['nama_pendidikan'] ? obj['nama_pendidikan'] : '';

        let jenis = tempJenis == 'PART' ? 'Part Time' : 'Full Time';
        let lamar = obj['lamaran'] ? `<span class="badge badge-info">Sudah dilamar</span>` : `<a href="javascript:lamar('` + id_lowongan + `')" class="btn btn-success">Lamar</a>`;

        let template = `
            <div class="card">
                <div class="card-body row">
                    <div class="col-md-2">
                        <img src="` + logo + `" class="img-fluid" alt="">
                    </div>
                    <div class="col-md-5">
                        <h2>` + posisi + `</h2>
                        <p>` + nama + `</p>
                        <p>` + lokasi + `</p>
                        <p>` + pendidikan + `</p>
                        <label class="border border-dark rounded p-2"><i class="fas fa-building"></i> ` + jenis + `</label>
                    </div>
                    <div class="col-md-5">
                        <p><strong>Ajukan Pekerjaan sebelum</strong><br> ` + endPublish + `</p>
                        <p class="mb-5">` + diperbarui + `</p>
                        <a href="<?php echo base_url() ?>lowongan_kerja/alumni/index/detail?id=` + id_lowongan + `" class="btn btn-light">Lihat Detail</a>
                        `+lamar+`
                    </div>
                </div>
            </div>
        `;
        return template;
    }

    function lamar(idLowongan) {
        $.ajax({
            url: '<?php echo site_url() ?>lowongan_kerja/alumni/index/lamar/'+idLowongan,
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                swal({
                    title: data.message.title,
                    text: data.message.body,
                    type: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = '<?php echo site_url() ?>lowongan_kerja/alumni/index/';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                dismiss_loading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);

                                if (field == 'start_publish') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'end_publish') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'gaji_rate_atas') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'gaji_rate_bawah') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal({
                            title: response.message.title,
                            html: response.message.body,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    }
</script>