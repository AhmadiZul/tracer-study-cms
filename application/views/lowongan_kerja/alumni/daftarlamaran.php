<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-end">
        <li class="page-item"><button id="btn_prev_lamaran" class="page-link">Sebelumnya</button></li>
        <li class="page-item"><span id="page_span" class="page-link disabled"></span></li>
        <li class="page-item"><button id="btn_next_lamaran" class="page-link">Selanjutnya</button></li>
    </ul>
</nav>
<div class="row">
    <div class="col-md-12" id="tabel_lamaran">
    </div>
</div>

<script>
    let current_page_lamaran = 1;
    let records_per_page_lamaran = 19;

    let objJsonLamaran = JSON.parse(`<?php echo isset($lamaran) ? json_encode($lamaran) : NULL; ?>`);

    function prevPageLamaran() {
        if (current_page_lamaran > 1) {
            current_page_lamaran--;
            changePage(current_page_lamaran);
        }
    }

    function nextPageLamaran() {
        if (current_page_lamaran < numPagesLamaran()) {
            current_page_lamaran++;
            changePage(current_page_lamaran);
        }
    }

    function changePageLamaran(page) {
        // array objJsonLamaran baru
        let tempObjJsonLamaran = objJsonLamaran.map(function(obj) {
            return obj;
        });

        // Validate page
        if (page < 1) page = 1;
        if (page > numPagesLamaran(tempObjJsonLamaran)) page = numPagesLamaran(tempObjJsonLamaran);

        $('#tabel_lamaran').html('');

        if (tempObjJsonLamaran.length <= 0) {
            $('#btn_prev_lamaran, #btn_next_lamaran').css('visibility', 'hidden');
            $('#page_span').html('0/0');
            return;
        }

        // MEMBUAT CARD HTML
        let tempAllCard = '';
        for (var i = (page - 1) * records_per_page_lamaran; i < (page * records_per_page_lamaran) && i < tempObjJsonLamaran.length; i++) {
            tempAllCard += renderCardLamaran(tempObjJsonLamaran[i]);
        };

        $('#tabel_lamaran').html(tempAllCard);
        $('#page_span').html(page + '/' + numPagesLamaran(tempObjJsonLamaran));

        if (page == 1) {
            $('#btn_prev_lamaran').css('visibility', 'hidden');
        } else {
            $('#btn_prev_lamaran').css('visibility', 'visible');
        };

        if (page == numPagesLamaran(tempObjJsonLamaran)) {
            $('#btn_next_lamaran').css('visibility', 'hidden');
        } else {
            $('#btn_next_lamaran').css('visibility', 'visible');
        };
    };

    function numPagesLamaran(obj = null) {
        if (obj === null) {
            return Math.ceil(objJsonLamaran.length / records_per_page_lamaran);
        };
        return Math.ceil(obj.length / records_per_page_lamaran);
    }

    $(document).ready(function() {
        if (objJsonLamaran) {
            changePageLamaran(1);
            $('#btn_prev_lamaran').on('click', prevPageLamaran);
            $('#btn_next_lamaran').on('click', nextPageLamaran);
        };
    });

    function renderCardLamaran(obj) {
        let id_lowongan = obj['id_lowongan_pekerjaan'] ? obj['id_lowongan_pekerjaan'] : '';
        let nama = obj['nama'] ? obj['nama'] : '';
        let posisi = obj['posisi'] ? obj['posisi'] : '';
        let dikirim = obj['applied_time'] ? obj['applied_time'] : '';
        let status = obj['status'] ? obj['status'] : '';

        let template = `
            <div class="card">
                <div class="card-header">
                    <h2>` + nama + `</h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>` + posisi + `</h4>
                            <p>Lamaran dikirim : ` + dikirim + `</p>
                        </div>
                        <div class="col-md-6">
                            <a href="<?php echo base_url() ?>lowongan_kerja/alumni/index/detail?id=` + id_lowongan + `" class="btn btn-light">Lihat Detail</a>
                            <p>Status Lamaran : ` + status + `</p>
                        </div>
                    </div>
                </div>
            </div>
            `;
        return template;
    }
</script>