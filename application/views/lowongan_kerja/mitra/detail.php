<section id="hero-jadwal">
    <div class="card row mx-5" style="background-color: #064514;box-shadow: none;">
        <div class="col-md-12">
            <h2><?= $lowongan->posisi ?></h2>
            <p><?= $lowongan->nama ?></p>
            <div class="row">
                <div class="col-md-6">
                    <p for=""><?php echo $lowongan->posisi ?></p>
                    <p for=""><b>Jenis Pekerjaan</b> : <?= $lowongan->jenis ?></p>
                    <p for=""><b>Lokasi Kerja</b> : <?= $lowongan->nama_kab_kota ?>, <?= $lowongan->nama_provinsi ?></p>
                    <p for=""><b>Minimal Pengalaman Kerja</b> : <?= $lowongan->min_pengalaman ?> Tahun</p>
                </div>
                <div class="col-md-6">
                    <p for=""><b>Minimal Pendidikan</b> : <?= $lowongan->nama_pendidikan ?></p>
                    <p for=""><b>Gaji</b> : Rp. <?= $lowongan->gaji_rate_bawah ?> - Rp. <?= $lowongan->gaji_rate_atas ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hero -->

<section id="detail_lowongan" class="container">
    <div class="card">
        <div class="card-header">
            <h2>Deskripsi</h2>
        </div>
        <div class="card-body">
            <?= $lowongan->deskripsi ?>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h2>Persyaratan</h2>
        </div>
        <div class="card-body">
            <?= $lowongan->persyaratan ?>
        </div>
    </div>
</section>