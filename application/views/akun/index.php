<div class="card">
    <div class="card-body row">
        <div class="col-lg-3">
        </div>
        <div class=" col-lg-9">
            <div class="">
                <div class="row justify-content-between">
                    <div class="col-lg-3 title">
                        <h3><i class="fa fa-key"></i> Akun</h3>
                    </div>
                    <div class="col-lg-1">
                        <button class="btn btn-action-profil"><i class="fa fa-pencil"></i></button>
                    </div>
                </div>
                <form id="form_akun" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label>Username <b class="text-danger">*</b></label>
                        <input type="text" name="username" value="<?php echo $alumni->username ?>" class="form-control" placeholder="Masukkan username anda" required>
                        <div class="invalid-feedback">Email tidak sesuai</div>
                    </div>

                    <div class="form-group">
                        <label>Password Lama<b class="text-danger">*</b></label>
                        <div class="input-group mb-3">
                            <input type="password" name="password_lama" class="form-control" placeholder="Masukkan password" required>
                            <a class="input-group-text show-old-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Password Baru<b class="text-danger">*</b></label>
                        <div class="input-group mb-3">
                            <input type="password" name="password" class="form-control" placeholder="Masukkan password" required>
                            <a class="input-group-text show-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Konfirmasi Password <b class="text-danger">*</b></label>
                        <div class="input-group mb-3">
                            <input type="password" name="konfirmasi_password" class="form-control" placeholder="Masukkan konfirmasi password anda" required>
                            <a class="input-group-text show-confirm-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-outline-tema px-5">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var site_url = '<?php echo site_url() ?>profil_alumni/index/';
    let status = "<?php echo $alumni->id_ref_status_alumni ?>";
    /**
     * submit akun alumni
     */
    $('#form_akun').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: site_url + "update_akun",
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                if (data.success) {
                    swal({
                        title: 'Berhasil',
                        text: 'Ubah data akun berhasil',
                        type: 'success',
                        confirmButtonColor: '#396113',
                    }, ).then(function() {
                        window.location.href = site_url;
                    });
                } else {
                    if (typeof data.text !== "undefined") {
                        swal({
                            title: 'Ups...',
                            html: data.text,
                            type: 'warning',
                            confirmButtonColor: '#396113',
                        }, );
                    } else {
                        swal({
                            title: 'Ups...',
                            text: 'Data Gagal Disimpan',
                            type: 'error',
                            confirmButtonColor: '#396113',
                        }, );
                    }
                }
            },
            error: function(error) {
                dismiss_loading();
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                }, );
            }
        });
    });
</script>

<!-- tabel -->
<script>
    let current_page = 1;
    let records_per_page = 5;

    let objJson;

    // tab pengalaman
    $('#pengalaman-tab').on('click', function(e) {
        $.get(site_url + "getPengalaman", function(data, status) {
            refreshTabel(JSON.parse(data), 'pengalaman')
            kembaliPengalaman()
        });
    });

    // tab organisasi
    $('#organisasi-tab').on('click', function(e) {
        $.get(site_url + "getOrganisasi", function(data, status) {
            refreshTabel(JSON.parse(data), 'organisasi')
            kembaliOrganisasi()
        });
    });

    // tab prestasi
    $('#prestasi-tab').on('click', function(e) {
        $.get(site_url + "getPrestasi", function(data, status) {
            refreshTabel(JSON.parse(data), 'prestasi')
            kembaliPrestasi()
        });
    });

    // tab skill
    $('#skill-tab').on('click', function(e) {
        $.get(site_url + "getSkill", function(data, status) {
            refreshTabel(JSON.parse(data), 'skill')
            kembaliSkill()
        });
    });

    function prevPage(event) {
        if (current_page > 1) {
            current_page--;
            changePage(current_page, event.data.name);
        }
    }

    function nextPage(event) {
        if (current_page < numPages()) {
            current_page++;
            changePage(current_page, event.data.name);
        }
    }

    function changePage(page, name) {
        // array objJson baru
        let tempObjJson = objJson.map(function(obj) {
            return obj;
        });

        // Validate page
        if (page < 1) page = 1;
        if (page > numPages(tempObjJson)) page = numPages(tempObjJson);

        $('#tabel-' + name).html('');

        if (tempObjJson.length <= 0) {
            $('#tabel-' + name).html('<labe>Tidak ada data</label>');
            $('#btn_prev-' + name + ', #btn_next-' + name).css('visibility', 'hidden');
            $('#page_span-' + name).html('');
            $('#total-' + name).html('');
            $('#total-' + name).addClass('d-none');
            return;
        }

        // MEMBUAT CARD HTML
        let tempAllCard = '';
        for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < tempObjJson.length; i++) {
            if (name == 'pengalaman') {
                tempAllCard += renderPengalaman(tempObjJson[i]);
            }

            if (name == 'organisasi') {
                tempAllCard += renderOrganisasi(tempObjJson[i]);
            }

            if (name == 'prestasi') {
                tempAllCard += renderPrestasi(tempObjJson[i]);
            }

            if (name == 'skill') {
                tempAllCard += renderSkill(tempObjJson[i]);
            }
        };

        $('#tabel-' + name).html(tempAllCard);
        $('#page_span-' + name).html(page + '/' + numPages(tempObjJson));
        $('#total-' + name).removeClass('d-none');
        $('#total-' + name).html('Total ' + tempObjJson.length);

        if (page == 1) {
            $('#btn_prev-' + name).css('visibility', 'hidden');
        } else {
            $('#btn_prev-' + name).css('visibility', 'visible');
        };

        if (page == numPages(tempObjJson)) {
            $('#btn_next-' + name).css('visibility', 'hidden');
        } else {
            $('#btn_next-' + name).css('visibility', 'visible');
        };
    };

    function numPages(obj = null) {
        if (obj === null) {
            return Math.ceil(objJson.length / records_per_page);
        };
        return Math.ceil(obj.length / records_per_page);
    }

    $(document).ready(function() {
        if (objJson) {
            changePage(1);
        };
        $('#btn_prev-pengalaman').on('click', {
            name: 'pengalaman'
        }, prevPage);
        $('#btn_next-pengalaman').on('click', {
            name: 'pengalaman'
        }, nextPage);
        $('#btn_prev-organisasi').on('click', {
            name: 'organisasi'
        }, prevPage);
        $('#btn_next-organisasi').on('click', {
            name: 'organisasi'
        }, nextPage);
        $('#btn_prev-prestasi').on('click', {
            name: 'prestasi'
        }, prevPage);
        $('#btn_next-prestasi').on('click', {
            name: 'prestasi'
        }, nextPage);
        $('#btn_prev-skill').on('click', {
            name: 'skill'
        }, prevPage);
        $('#btn_next-skill').on('click', {
            name: 'skill'
        }, nextPage);
    });

    function refreshTabel(data, name) {
        objJson = data;
        current_page = 1;
        changePage(1, name);
    }

    function changeDate(date) {
        var mydate = new Date(date);
        var month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "December"][mydate.getMonth()];
        var str = month + ' ' + mydate.getFullYear();
        return str;
    }

    function renderPengalaman(obj) {
        let id = obj['id'] ? obj['id'] : '';
        let posisi = obj['posisi'] ? obj['posisi'] : '';
        let nama_instansi = obj['nama_instansi'] ? obj['nama_instansi'] : ' ';
        let jenis = obj['jenis_pengalaman'] ? obj['jenis_pengalaman'] : ' ';
        let tanggal = (obj['start_date'] ? changeDate(obj['start_date']) : '') + ' - ' + (obj['end_date'] ? changeDate(obj['end_date']) : '');
        let alamat = obj['alamat'] ? obj['alamat'] : '';

        let template = `
                    <div class="row my-4">
                        <div class="col-md-2 align-self-center">
                            <div class="img-tabel">
                                <img src="<?php echo site_url() ?>public/assets/img/admin/pengalaman.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="posisi-title"><strong>` + posisi.toUpperCase() + `</strong></div>
                            <div class="instansi-title"><strong>` + nama_instansi.toUpperCase() + `</strong></div>
                            <div class="tgl-pengalaman">` + jenis + `</div>
                            <div class="tgl-pengalaman">` + tanggal + `</div>
                            <div class="alamat-pengalaman">` + alamat + `</div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-action-profil" onclick="editPengalaman(` + id + `)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-action-profil" onclick="deletePengalaman(` + id + `)"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    `;
        return template;
    }

    function kembaliPengalaman() {
        $('#form_pengalaman').removeClass('was-validated');
        $('#form-pengalaman').addClass('d-none');
        $('#tabel-pengalaman').removeClass('d-none');
        $('#div-total-pengalaman').removeClass('d-none');
        $('#nav-pengalaman').removeClass('d-none');
        $('#add-pengalaman').removeClass('d-none');
        $('#preview-pengalaman').html('');
    }

    function addPengalaman() {
        $('#form_pengalaman')[0].reset();
        $('[name="jenis_pengalaman"]').val('').trigger('change');
        $('#title-pengalaman').text('Tambah Pengalaman');
        $('#form-pengalaman').removeClass('d-none');
        $('#add-pengalaman').addClass('d-none');
        $('#tabel-pengalaman').addClass('d-none');
        $('#div-total-pengalaman').addClass('d-none');
        $('#nav-pengalaman').addClass('d-none');
        $('[name="simpan_pengalaman"]').val('1');
        $('#ganti-berkas-pengalaman').addClass('d-none');
        $('#new_upload_pengalaman').val('1');
        $('#edit-berkas-pengalaman').html('');
        $('#new-upload-pengalaman').removeClass('d-none');
        $('.file-info-pengalaman').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-pengalaman').html('');
    }

    function editPengalaman(id) {
        $('#form_pengalaman')[0].reset();
        $('[name="jenis_pengalaman"]').val('').trigger('change');
        $('.file-info-pengalaman').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-pengalaman').html('');
        $.ajax({
            url: site_url + 'getPengalaman',
            type: "POST",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                var tipe = data.url_bukti.split(/[#?]/)[0].split('.').pop().trim();
                var url_bukti = data.url_bukti;
                if (tipe == 'pdf') {
                    $('#ganti-berkas-pengalaman').removeClass('d-none');
                    $('#new_upload_pengalaman').val('1');
                    $('#edit-berkas-pengalaman').html('<embed src="' + url_bukti + '" class="mt-4" height="800" width="100%"/>');
                    $('#new-upload-pengalaman').addClass('d-none');
                } else {
                    $('#ganti-berkas-pengalaman').removeClass('d-none');
                    $('#new_upload_pengalaman').val('1');
                    $('#edit-berkas-pengalaman').html('<img src="' + url_bukti + '" class="mt-4" alt="your image" style="width:250"/>');
                    $('#new-upload-pengalaman').addClass('d-none');
                }

                $('#title-pengalaman').text('Edit Pengalaman');
                $('#form-pengalaman').removeClass('d-none');
                $('#tabel-pengalaman').addClass('d-none');
                $('#nav-pengalaman').addClass('d-none');
                $('#div-total-pengalaman').addClass('d-none');
                $('#add-pengalaman').addClass('d-none');

                $('[name="simpan_pengalaman"]').val('0');
                $('[name="id_pengalaman"]').val(data.id);
                $('[name="jenis_pengalaman"]').val(data.jenis_pengalaman).trigger('change');
                $('[name="nama_instansi"]').val(data.nama_instansi);
                $('[name="posisi"]').val(data.posisi);
                $('[name="alamat_instansi"]').val(data.alamat);
                $('[name="start_date"]').val(data.start_date);
                $('[name="end_date"]').val(data.end_date);
                $('[name="deskripsi"]').val(data.deskripsi);

                if (data.is_current_position == 1) {
                    $('[name="is_current_position"]').prop('checked', true);
                }

            },
            error: function(xx) {
                swal("Galat", "Hubungi admin", "error");
            }
        });
    }

    function deletePengalaman(id) {
        swal({
            title: "Ingin Menghapus Data?",
            text: "Data tidak bisa dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Ya',
            confirmButtonColor: '#396113',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result) {
            if (result.value) {
                hapusPengalaman(id);
            }
        }).catch(function(reason) {
            // swal("Gagal Klik.....", "Gagal Pilih", 'error');
        });
    }

    function renderOrganisasi(obj) {
        let id = obj['id'] ? obj['id'] : '';
        let posisi = obj['posisi'] ? obj['posisi'] : '';
        let nama_organisasi = obj['nama_organisasi'] ? obj['nama_organisasi'] : ' ';
        let tanggal = (obj['start_date'] ? changeDate(obj['start_date']) : '') + ' - ' + (obj['end_date'] ? changeDate(obj['end_date']) : '');
        let job_desc = obj['job_desc'] ? obj['job_desc'] : '';

        let template = `
                    <div class="row my-4">
                        <div class="col-md-2 align-self-center">
                            <div class="img-tabel">
                                <img src="<?php echo site_url() ?>public/assets/img/admin/crown.svg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="posisi-title"><strong>` + posisi.toUpperCase() + `</strong></div>
                            <div class="instansi-title"><strong>` + nama_organisasi.toUpperCase() + `</strong></div>
                            <div class="tgl-pengalaman">` + tanggal + `</div>
                            <div class="alamat-pengalaman">` + job_desc + `</div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-action-profil" onclick="editOrganisasi(` + id + `)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-action-profil" onclick="deleteOrganisasi(` + id + `)"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    `;
        return template;
    }

    function kembaliOrganisasi() {
        $('#form_organisasi').removeClass('was-validated');
        $('#form-organisasi').addClass('d-none');
        $('#tabel-organisasi').removeClass('d-none');
        $('#div-total-organisasi').removeClass('d-none');
        $('#nav-organisasi').removeClass('d-none');
        $('#add-organisasi').removeClass('d-none');
        $('#preview-organisasi').html('');
    }

    function addOrganisasi() {
        $('#form_organisasi')[0].reset();
        $('#title-organisasi').text('Tambah Organisasi');
        $('#form-organisasi').removeClass('d-none');
        $('#add-organisasi').addClass('d-none');
        $('#tabel-organisasi').addClass('d-none');
        $('#div-total-organisasi').addClass('d-none');
        $('#nav-organisasi').addClass('d-none');
        $('[name="simpan_organisasi"]').val('1');
        $('#ganti-berkas-organisasi').addClass('d-none');
        $('#new_upload_organisasi').val('1');
        $('#edit-berkas-organisasi').html('');
        $('#new-upload-organisasi').removeClass('d-none');
        $('.file-info-organisasi').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-organisasi').html('');
    }

    function editOrganisasi(id) {
        $('#form_organisasi')[0].reset();
        $('.file-info-organisasi').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-organisasi').html('');
        $.ajax({
            url: site_url + 'getOrganisasi',
            type: "POST",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                var tipe = data.url_bukti.split(/[#?]/)[0].split('.').pop().trim();
                var url_bukti = data.url_bukti;
                if (tipe == 'pdf') {
                    $('#ganti-berkas-organisasi').removeClass('d-none');
                    $('#new_upload_organisasi').val('1');
                    $('#edit-berkas-organisasi').html('<embed src="' + url_bukti + '" class="mt-4" height="800" width="100%"/>');
                    $('#new-upload-organisasi').addClass('d-none');
                } else {
                    $('#ganti-berkas-pengalaman').removeClass('d-none');
                    $('#new_upload_pengalaman').val('1');
                    $('#edit-berkas-pengalaman').html('<img src="' + url_bukti + '" class="mt-4" alt="your image" style="width:250"/>');
                    $('#new-upload-pengalaman').addClass('d-none');
                }

                $('#title-organisasi').text('Edit Organisasi');
                $('#form-organisasi').removeClass('d-none');
                $('#tabel-organisasi').addClass('d-none');
                $('#div-total-organisasi').addClass('d-none');
                $('#nav-organisasi').addClass('d-none');
                $('#add-organisasi').addClass('d-none');

                $('[name="simpan_organisasi"]').val('0');
                $('[name="id_organisasi"]').val(data.id);
                $('[name="nama_organisasi"]').val(data.nama_organisasi);
                $('[name="posisi"]').val(data.posisi);
                $('[name="job_desc"]').val(data.job_desc);
                $('[name="start_date"]').val(data.start_date);
                $('[name="end_date"]').val(data.end_date);
                $('[name="deskripsi"]').val(data.deskripsi);

                if (data.is_current_position == 1) {
                    $('[name="is_current_position"]').prop('checked', true);
                }

            },
            error: function(xx) {
                swal("Galat", "Hubungi admin", "error");
            }
        });
    }

    function deleteOrganisasi(id) {
        swal({
            title: "Ingin Menghapus Data?",
            text: "Data tidak bisa dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Ya',
            confirmButtonColor: '#396113',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result) {
            if (result.value) {
                hapusOrganisasi(id);
            }
        }).catch(function(reason) {
            // swal("Gagal Klik.....", "Gagal Pilih", 'error');
        });
    }

    function renderPrestasi(obj) {
        let id = obj['id'] ? obj['id'] : '';
        let nama_prestasi = obj['nama_prestasi'] ? obj['nama_prestasi'] : ' ';
        let penyelenggara = obj['penyelenggara'] ? obj['penyelenggara'] : '';
        let tahun = obj['tahun'] ? obj['tahun'] : '';
        let tingkat = obj['tingkat'] ? obj['tingkat'] : '';
        let gelar = obj['gelar'] ? obj['gelar'] : '';

        let template = `
                    <div class="row my-4">
                        <div class="col-md-2 align-self-center">
                            <div class="img-tabel">
                                <img src="<?php echo site_url() ?>public/assets/img/admin/award.svg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="posisi-title"><strong>` + gelar + ' ' + nama_prestasi.toUpperCase() + `</strong></div>
                            <div class="instansi-title"><strong>` + penyelenggara.toUpperCase() + `</strong></div>
                            <div class="tgl-pengalaman">` + tingkat + ' ' + tahun + `</div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-action-profil" onclick="editPrestasi(` + id + `)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-action-profil" onclick="deletePrestasi(` + id + `)"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    `;
        return template;
    }

    function kembaliPrestasi() {
        $('#form_prestasi').removeClass('was-validated');
        $('#form-prestasi').addClass('d-none');
        $('#tabel-prestasi').removeClass('d-none');
        $('#div-total-prestasi').removeClass('d-none');
        $('#nav-prestasi').removeClass('d-none');
        $('#add-prestasi').removeClass('d-none');
        $('#preview-prestasi').html('');
    }

    function addPrestasi() {
        $('#form_prestasi')[0].reset();
        $('[name="tahun"]').val('').trigger('change');
        $('[name="tingkat"]').val('').trigger('change');
        $('[name="gelar"]').val('').trigger('change');
        $('#title-prestasi').text('Tambah Prestasi');
        $('#form-prestasi').removeClass('d-none');
        $('#add-prestasi').addClass('d-none');
        $('#tabel-prestasi').addClass('d-none');
        $('#div-total-prestasi').addClass('d-none');
        $('#nav-prestasi').addClass('d-none');
        $('[name="simpan_prestasi"]').val('1');
        $('#ganti-berkas-prestasi').addClass('d-none');
        $('#new_upload_prestasi').val('1');
        $('#edit-berkas-prestasi').html('');
        $('#new-upload-prestasi').removeClass('d-none');
        $('.file-info-prestasi').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-prestasi').html('');
    }

    function editPrestasi(id) {
        $('#form_prestasi')[0].reset();
        $('[name="tahun"]').val('').trigger('change');
        $('[name="tingkat"]').val('').trigger('change');
        $('[name="gelar"]').val('').trigger('change');
        $('.file-info-prestasi').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-prestasi').html('');
        $.ajax({
            url: site_url + 'getPrestasi',
            type: "POST",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                var tipe = data.url_sertifikat.split(/[#?]/)[0].split('.').pop().trim();
                var url_sertifikat = data.url_sertifikat;
                if (tipe == 'pdf') {
                    $('#ganti-berkas-prestasi').removeClass('d-none');
                    $('#new_upload_prestasi').val('1');
                    $('#edit-berkas-prestasi').html('<embed src="' + url_sertifikat + '" class="mt-4" height="800" width="100%"/>');
                    $('#new-upload-prestasi').addClass('d-none');
                } else {
                    $('#ganti-berkas-prestasi').removeClass('d-none');
                    $('#new_upload_prestasi').val('1');
                    $('#edit-berkas-prestasi').html('<img src="' + url_sertifikat + '" class="mt-4" alt="your image" style="width:250"/>');
                    $('#new-upload-prestasi').addClass('d-none');
                }

                $('#title-prestasi').text('Edit Prestasi');
                $('#form-prestasi').removeClass('d-none');
                $('#tabel-prestasi').addClass('d-none');
                $('#div-total-prestasi').addClass('d-none');
                $('#nav-prestasi').addClass('d-none');
                $('#add-prestasi').addClass('d-none');

                $('[name="simpan_prestasi"]').val('0');
                $('[name="id_prestasi"]').val(data.id);
                $('[name="nama_prestasi"]').val(data.nama_prestasi);
                $('[name="penyelenggara"]').val(data.penyelenggara);
                $('[name="tahun"]').val(data.tahun).trigger('change');
                $('[name="tingkat"]').val(data.tingkat).trigger('change');
                $('[name="gelar"]').val(data.gelar).trigger('change');
            },
            error: function(xx) {
                swal("Galat", "Hubungi admin", "error");
            }
        });
    }

    function deletePrestasi(id) {
        swal({
            title: "Ingin Menghapus Data?",
            text: "Data tidak bisa dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Ya',
            confirmButtonColor: '#396113',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result) {
            if (result.value) {
                hapusPrestasi(id);
            }
        }).catch(function(reason) {
            // swal("Gagal Klik.....", "Gagal Pilih", 'error');
        });
    }

    function renderSkill(obj) {
        let id = obj['id'] ? obj['id'] : '';
        let nama_skill = obj['nama_skill'] ? obj['nama_skill'] : ' ';
        let bidang = obj['bidang'] ? obj['bidang'] : '';
        let level = obj['level'] ? obj['level'] : '';

        let template = `
                    <div class="row my-4">
                        <div class="col-md-2 align-self-center">
                            <div class="img-tabel">
                                <img src="<?php echo site_url() ?>public/assets/img/admin/crown.svg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="posisi-title"><strong>` + nama_skill.toUpperCase() + `</strong></div>
                            <div class="tgl-pengalaman">` + bidang + `</div>
                            <div class="tgl-pengalaman">` + level + `</div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-action-profil" onclick="editSkill(` + id + `)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-action-profil" onclick="deleteSkill(` + id + `)"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    `;
        return template;
    }

    function kembaliSkill() {
        $('#form_skill').removeClass('was-validated');
        $('#form-skill').addClass('d-none');
        $('#tabel-skill').removeClass('d-none');
        $('#div-total-skill').removeClass('d-none');
        $('#nav-skill').removeClass('d-none');
        $('#add-skill').removeClass('d-none');
        $('#preview-skill').html('');
    }

    function addSkill() {
        $('#form_skill')[0].reset();
        $('[name="bidang"]').val('').trigger('change');
        $('[name="level"]').val('').trigger('change');
        $('#title-skill').text('Tambah Skill');
        $('#form-skill').removeClass('d-none');
        $('#add-skill').addClass('d-none');
        $('#tabel-skill').addClass('d-none');
        $('#div-total-skill').addClass('d-none');
        $('#nav-skill').addClass('d-none');
        $('[name="simpan_skill"]').val('1');
        $('#ganti-berkas-skill').addClass('d-none');
        $('#new_upload_skill').val('1');
        $('#edit-berkas-skill').html('');
        $('#new-upload-skill').removeClass('d-none');
        $('.file-info-skill').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-skill').html('');
    }

    function editSkill(id) {
        $('#form_skill')[0].reset();
        $('[name="bidang"]').val('').trigger('change');
        $('[name="level"]').val('').trigger('change');
        $('.file-info-skill').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-skill').html('');
        $.ajax({
            url: site_url + 'getSkill',
            type: "POST",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                var tipe = data.url_sertifikat.split(/[#?]/)[0].split('.').pop().trim();
                var url_sertifikat = data.url_sertifikat;
                if (tipe == 'pdf') {
                    $('#ganti-berkas-skill').removeClass('d-none');
                    $('#new_upload_skill').val('1');
                    $('#edit-berkas-skill').html('<embed src="' + url_sertifikat + '" class="mt-4" height="800" width="100%"/>');
                    $('#new-upload-skill').addClass('d-none');
                } else {
                    $('#ganti-berkas-skill').removeClass('d-none');
                    $('#new_upload_skill').val('1');
                    $('#edit-berkas-skill').html('<img src="' + url_sertifikat + '" class="mt-4" alt="your image" style="width:250"/>');
                    $('#new-upload-skill').addClass('d-none');
                }

                $('#title-skill').text('Edit Skill');
                $('#form-skill').removeClass('d-none');
                $('#tabel-skill').addClass('d-none');
                $('#div-total-skill').addClass('d-none');
                $('#nav-skill').addClass('d-none');
                $('#add-skill').addClass('d-none');

                $('[name="simpan_skill"]').val('0');
                $('[name="id_skill"]').val(data.id);
                $('[name="nama_skill"]').val(data.nama_skill);
                $('[name="bidang"]').val(data.bidang).trigger('change');
                $('[name="level"]').val(data.level).trigger('change');
            },
            error: function(xx) {
                swal("Galat", "Hubungi admin", "error");
            }
        });
    }

    function deleteSkill(id) {
        swal({
            title: "Ingin Menghapus Data?",
            text: "Data tidak bisa dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Ya',
            confirmButtonColor: '#396113',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result) {
            if (result.value) {
                hapusSkill(id);
            }
        }).catch(function(reason) {
            // swal("Gagal Klik.....", "Gagal Pilih", 'error');
        });
    }
</script>