<div class="card">
    <div class="card-body row">
        <div class="col-lg-3">
            <ul class="list-group" role="tablist" id="profil-tab">
                <li class="list-group-item"><a id="datadiri-tab" data-toggle="tab" data-target="#datadiri" type="button" role="tab" aria-controls="datadiri" aria-selected="true" class="active"><i class="fas fa-user-pen"></i> Data Diri</a></li>
                <li class="list-group-item"><a id="pengalaman-tab" data-toggle="tab" data-target="#pengalaman" type="button" role="tab" aria-controls="pengalaman" aria-selected="false"><i class="fas fa-briefcase"></i> Pengalaman</a></li>
                <li class="list-group-item"><a id="organisasi-tab" data-toggle="tab" data-target="#organisasi" type="button" role="tab" aria-controls="organisasi" aria-selected="false"><i class="bx bxs-crown"></i> Organisasi</a></li>
                <li class="list-group-item"><a id="prestasi-tab" data-toggle="tab" data-target="#prestasi" type="button" role="tab" aria-controls="prestasi" aria-selected="false"><i class="fas fa-award"></i> Prestasi</a></li>
                <li class="list-group-item"><a id="skill-tab" data-toggle="tab" data-target="#skill" type="button" role="tab" aria-controls="skill" aria-selected="false"><i class="fas fa-crown"></i> Keahlian/Skill</a></li>
                <li class="list-group-item"><a id="sosmed-tab" data-toggle="tab" data-target="#sosmed" type="button" role="tab" aria-controls="sosmed" aria-selected="false"><i class="fas fa-at"></i> Sosmed</a></li>
                <li class="list-group-item"><a id="akun-tab" data-toggle="tab" data-target="#akun" type="button" role="tab" aria-controls="akun" aria-selected="false"><i class="fas fa-key"></i> Akun</a></li>
            </ul>
        </div>
        <div class="tab-content col-lg-9">
            <div class="tab-pane fade active show" class="" id="datadiri" role="tabpanel" aria-labelledby="datadiri-tab">
                <div class="row justify-content-between">
                    <div class="col-lg-3 title">
                        <h3><i class="fa fa-user-pen"></i> Data Diri</h3>
                    </div>
                </div>
                <form id="form_profil" class="needs-validation" enctype="multipart/form-data" novalidate>
                    <span class="badge bg-lime text-danger mb-3">IDENTITAS DIRI</span>
                    <input type="text" name="id_alumni" class="form-control d-none" value="<?php echo $alumni->id_alumni ?>" placeholder="Masukkan nama lengkap anda" required>
                    <div id="ganti-berkas-alumni" class="form-group <?php echo (isset($alumni->url_photo) ? '' : 'd-none') ?>">
                        <img src="<?php echo (isset($alumni->url_photo)  ? $alumni->url_photo : '') ?>" alt="your image" style="width: 150px;" /><br>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" name="new_upload_alumni" id="new_upload_alumni" class="custom-control-input" onclick="uploadFoto('alumni','url_photo')" value="1">
                            <label class="custom-control-label" for="new_upload_alumni">Ganti Berkas</label>
                        </div>
                    </div>
                    <div id="new-upload-alumni" class="form-group <?php echo (!empty($alumni->url_photo) ? 'd-none' : null) ?>">
                        <label for="url_photo">Foto <b class="text-danger">*</b></label><br>
                        <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                        <input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'alumni')" required>
                        <span class="file-info-alumni text-muted">Tidak ada berkas yang dipilih</span>
                        <div id="preview-alumni">

                        </div>
                        <div class="hint-block text-muted mt-3">
                            <small>
                                Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                Ukuran file maksimal: <strong>2 MB</strong>
                            </small>
                        </div>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap <b class="text-danger">*</b></label>
                        <input type="text" name="nama" class="form-control alpha-only" value="<?php echo $alumni->nama ?>" placeholder="Masukkan nama lengkap anda" required>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Nomor Induk Mahasiswa/Siswa (NIM/NIS) <b class="text-danger">*</b></label>
                        <input type="text" name="nim" class="form-control number-only" value="<?php echo $alumni->nim ?>" maxlength="20" placeholder="Masukkan NIM/NIS anda" required>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Nomor Induk Kependudukan (NIK) <b class="text-danger">*</b></label>
                        <input type="text" name="nik" value="<?php echo $alumni->nik ?>" maxlength="16" minlength="16" class="form-control number-only" placeholder="Masukkan NIK anda" required>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Nomor HP <b class="text-danger">*</b></label>
                        <input type="text" name="no_hp" value="<?php echo $alumni->no_hp ?>" minlength="11" maxlength="13" class="form-control number-only" placeholder="Masukkan nomor hp anda" required>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Email <b class="text-danger">*</b></label>
                        <input type="email" name="email" value="<?php echo $alumni->email ?>" class="form-control" placeholder="Masukkan email anda" required>
                        <div class="invalid-feedback">Email tidak sesuai</div>
                    </div>
                    <div class="form-group">
                        <label>Tempat Lahir <b class="text-danger">*</b></label>
                        <input type="text" name="tempat_lahir" value="<?php echo $alumni->tempat_lahir ?>" class="form-control alpha-only" placeholder="Masukkan Tempat Lahir Anda" required>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir <b class="text-danger">*</b></label>
                        <input type="text" name="tgl_lahir" value="<?php echo $alumni->tgl_lahir ?>" class="form-control datepicker-lahir" placeholder="Masukkan Tanggal Lahir Anda" required>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Agama <b class="text-danger">*</b></label>
                        <select name="id_agama" id="id_agama" class="select2 form-control" required>
                            <option value="">- Pilih Agama -</option>
                            <?php foreach ($agama as $key => $value) { ?>
                                <option value="<?php echo $key ?>" <?php echo ($alumni->id_agama == $key ? "selected" : "") ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin <b class="text-danger">*</b></label>
                        <div class="custom-control custom-radio my-2">
                            <input type="radio" class="custom-control-input" id="laki_laki" name="jenis_kelamin" value="L" required <?php echo ($alumni->jenis_kelamin == "L" ? "checked" : "") ?>>
                            <label class="custom-control-label" for="laki_laki">Laki - Laki</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="perempuan" name="jenis_kelamin" value="P" required <?php echo ($alumni->jenis_kelamin == "P" ? "checked" : "") ?>>
                            <label class="custom-control-label" for="perempuan">Perempuan</label>
                            <div class="invalid-feedback my-2">Wajib dipilih</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Provinsi <b class="text-danger">*</b></label>
                        <select name="kode_provinsi" id="kode_provinsi" class="form-control select2" required>
                            <option value="">- Pilih Provinsi -</option>
                            <?php foreach ($provinsi as $key => $value) { ?>
                                <option value="<?php echo $key ?>" <?php echo ($alumni->kode_prov == $key ? "selected" : "") ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Kabupaten/Kota <b class="text-danger">*</b></label>
                        <select name="kode_kabupaten" id="kode_kabupaten" class="select2 form-control" required>
                            <option value="">- Pilih Kabupaten/Kota -</option>
                            <?php foreach ($kab_kota as $key => $value) { ?>
                                <option value="<?php echo $key ?>" <?php echo ($alumni->kode_kab_kota == $key ? "selected" : "") ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Kecamatan <b class="text-danger">*</b></label>
                        <select name="kode_kecamatan" id="kode_kecamatan" class="select2 form-control" required>
                            <option value="">- Pilih Kecamatan -</option>

                            <?php foreach ($kecamatan as $key => $value) { ?>
                                <option value="<?php echo $key ?>" <?php echo ($alumni->kode_kecamatan == $key ? "selected" : "") ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Detail Alamat <b class="text-danger">*</b></label>
                        <textarea name="alamat" id="alamat" class="form-control" required style="height: 100px;"><?php echo $alumni->alamat ?></textarea>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <span class="badge bg-lime text-danger mb-3">PROFIL ALUMNI</span>
                    <div class="form-group">
                        <label>Tahun Kelulusan <b class="text-danger">*</b></label>
                        <select name="tahun_lulus" id="tahun_lulus" class="form-control select2" required>
                            <option value="">- Pilih Tahun Kelulusan -</option>
                            <?php foreach ($tahun as $key => $value) { ?>
                                <option value="<?php echo $key ?>" <?php echo ($alumni->tahun_lulus == $key ? "selected" : "") ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                    <label>Fakultas <b class="text-danger">*</b></label>
                            <select id="kode_fakultas" name="kode_fakultas" class="select2 form-control">
                                <option value="">- Pilih Fakultas -</option>
                                <?php foreach ($fakultas as $key => $value) { ?>
                            <?php if ($data_alumni->id_fakultas == $value['id_fakultas']) : ?>
                                <option value="<?= $value['id_fakultas']; ?>" selected><?php echo $value['nama_fakultas']; ?></option>
                            <?php else : ?>
                                <option value="<?= $value['id_fakultas']; ?>"><?php echo $value['nama_fakultas']; ?></option>
                            <?php endif ?>
                        <?php } ?>

                            </select>
                            <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Prodi/Jurusan <b class="text-danger">*</b></label>
                        <select name="kode_prodi" id="kode_prodi" class="select2 form-control" required>
                            <option value="">- Pilih Prodi -</option>
                            <?php foreach ($prodi as $key => $value) { ?>
                            <?php if ($data_alumni->id_prodi == $value['id_prodi']) : ?>
                                <option value="<?= $value['id_prodi']; ?>" selected><?php echo $value['nama_prodi']; ?></option>
                            <?php else : ?>
                                <option value="<?= $value['id_prodi']; ?>"><?php echo $value['nama_prodi']; ?></option>
                            <?php endif ?>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>IPK Terakhir <b class="text-danger">*</b></label>
                        <input type="text" name="ipk_terakhir" value="<?php echo $alumni->ipk_terakhir ?>" class="form-control ipk" placeholder="Masukkan ipk terakhir anda" required>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group">
                        <label>Judul Skripsi <b class="text-danger">*</b></label>
                        <input type="text" name="judul_skripsi" value="<?php echo $alumni->judul_skripsi ?>" class="form-control" placeholder="Masukkan judul skripsi anda" required>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="badge bg-lime text-danger my-3">STATUS ALUMNI</div>
                    <div class="form-group">
                        <label>Status Alumni <b class="text-danger">*</b></label>
                        <select name="status_alumni" id="status_alumni" class="form-select select2" required>
                            <option value="">- Pilih Status Alumni -</option>
                            <?php foreach ($status_alumni as $key => $value) { ?>
                                <option value="<?php echo $key ?>" <?php echo ($alumni->id_ref_status_alumni == $key ? "selected" : "") ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">Wajib diisi</div>
                        <div class="row pt-3">
                            <div class="col-md">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="chek_riwayat" name="chek_riwayat" value="riwayat_baru">
                                    <label class="form-check-label" for="chek_riwayat">Apakah Riwayat Baru?</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="text" name="id_instansi" class="form-control d-none" value="<?php echo (isset($alumni->id_jobs_study) ? $alumni->id_jobs_study : '') ?>" placeholder="Masukkan nama lengkap anda" required>
                    <div id="div-instansi" class="form-group d-none">
                        <label id="instansi">Nama Instansi <b class="text-danger">*</b></label>
                        <input type="text" name="nama_instansi" value="<?php echo (isset($alumni->nama_instansi) ? $alumni->nama_instansi : '') ?>" class="form-control" placeholder="Masukkan nama instansi anda">
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div id="div-tgl-mulai" class="form-group d-none">
                        <label>Tanggal Masuk <b class="text-danger">*</b></label>
                        <input type="text" name="tgl_mulai" value="<?php echo (isset($alumni->tgl_mulai) ? $alumni->tgl_mulai : '') ?>" class="form-control datepicker2">
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div id="div-jabatan" class="form-group d-none">
                        <label>Jabatan <b class="text-danger">*</b></label>
                        <input type="text" name="jabatan" value="<?php echo (isset($alumni->jabatan) ? $alumni->jabatan : '') ?>" class="form-control" placeholder="Masukkan jabatan anda">
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div id="div-prodi" class="form-group d-none">
                        <label>Prodi <b class="text-danger">*</b></label>
                        <input type="text" name="prodi" value="<?php echo (isset($alumni->prodi) ? $alumni->prodi : '') ?>" class="form-control" placeholder="Masukkan prodi anda">
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div id="div-sektor" class="form-group d-none">
                        <label>Sektor <b class="text-danger">*</b></label>
                        <div class="custom-control custom-radio my-2">
                            <input type="radio" class="custom-control-input" id="pertanian" name="sektor" value="Pertanian" <?php echo (isset($alumni->sektor) ? ($alumni->sektor == "Pertanian" ? "checked" : "") : "") ?>>
                            <label class="custom-control-label" for="pertanian">Pertanian</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="non_pertanian" name="sektor" value="Non Pertanian" <?php echo (isset($alumni->sektor) ? ($alumni->sektor == "Non Pertanian" ? "checked" : "") : "") ?>>
                            <label class="custom-control-label" for="non_pertanian">Non Pertanian</label>
                            <div class="invalid-feedback my-2">Wajib dipilih</div>
                        </div>
                    </div>
                    <div id="div-instansi-provinsi" class="form-group d-none">
                        <label>Provinsi <b class="text-danger">*</b></label>
                        <select name="instansi_kode_prov" id="instansi_kode_prov" class="form-control select2">
                            <option value="">- Pilih Provinsi -</option>
                            <?php foreach ($provinsi as $key => $value) { ?>
                                <option value="<?php echo $key ?>" <?php echo (isset($alumni->kode_prov_instansi) ? ($alumni->kode_prov_instansi == $key ? "selected" : "") : "") ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div id="div-instansi-kab-kota" class="form-group d-none">
                        <label>Kabupaten/Kota <b class="text-danger">*</b></label>
                        <select name="instansi_kode_kab" id="instansi_kode_kab" class="select2 form-control">
                            <option value="">- Pilih Kabupaten/Kota -</option>
                            <?php foreach ($kab_kota_instansi as $key => $value) { ?>
                                <option value="<?php echo $key ?>" <?php echo (isset($alumni->kode_kab_kota_instansi) ? ($alumni->kode_kab_kota_instansi == $key ? "selected" : "") : "") ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div id="div-instansi-kecamatan" class="form-group d-none">
                        <label>Kecamatan <b class="text-danger">*</b></label>
                        <select name="instansi_kode_kec" id="instansi_kode_kec" class="select2 form-control">
                            <option value="">- Pilih Kecamatan -</option>
                            <?php foreach ($kecamatan_instansi as $key => $value) { ?>
                                <option value="<?php echo $key ?>" <?php echo (isset($alumni->kode_kec_instansi) ? ($alumni->kode_kec_instansi == $key ? "selected" : "") : "") ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div id="div-alamat" class="form-group d-none">
                        <label>Detail Alamat <b class="text-danger">*</b></label>
                        <textarea name="instansi_alamat" id="instansi_alamat" class="form-control" style="height: 100px;"><?php echo (isset($alumni->alamat_instansi) ? $alumni->alamat_instansi : '') ?></textarea>
                        <div class="invalid-feedback">Wajib diisi</div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-outline-danger px-5">Simpan</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="pengalaman" role="tabpanel" aria-labelledby="pengalaman-tab">
                <div class="row justify-content-between">
                    <div class="col-lg-4 title">
                        <h3><i class="fa fa-briefcase"></i> Pengalaman</h3>
                    </div>
                    <div id="add-pengalaman" class="col-lg-1">
                        <div class="row">
                            <button class="btn btn-action-profil" onclick="addPengalaman()"><i class="fa fa-square-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div id="div-total-pengalaman" class="col">
                    <ul class="pagination justify-content-start">
                        <li class="page-item"><span id="total-pengalaman" class="page-link disabled">Total</span></li>
                    </ul>
                </div>

                <!-- tabel pengalaman -->
                <div id="tabel-pengalaman">
                </div>

                <nav id="nav-pengalaman" aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                        <li class="page-item"><button id="btn_prev-pengalaman" class="page-link">Sebelumnya</button></li>
                        <li class="page-item"><span id="page_span-pengalaman" class="page-link disabled"></span></li>
                        <li class="page-item"><button id="btn_next-pengalaman" class="page-link">Selanjutnya</button></li>
                    </ul>
                </nav>

                <div id="form-pengalaman" class="d-none">
                    <div class="form-group">
                        <button class="btn btn-light-danger" onclick="kembaliPengalaman()"><i class="fa fa-chevron-left"></i> Kembali</button>
                        <span id="title-pengalaman" class="badge bg-lime text-danger">Tambah Pengalaman</span>
                    </div>
                    <form id="form_pengalaman" class="needs-validation" enctype="multipart/form-data" novalidate>
                        <input type="text" name="simpan_pengalaman" value="1" class="form-control d-none" required>
                        <input type="text" name="id_pengalaman" class="form-control d-none" required>
                        <div class="form-group">
                            <label for="jenis_pengalaman">Jenis Pengalaman <b class="text-danger">*</b></label>
                            <select name="jenis_pengalaman" id="jenis_pengalaman" class="form-control select2" required>
                                <option value="">- Pilih Jenis Pengalaman -</option>
                                <option value="Magang">Magang</option>
                                <option value="Kerja">Kerja</option>
                            </select>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="nama_instansi">Nama Instansi <b class="text-danger">*</b></label>
                            <input type="text" name="nama_instansi" placeholder="Masukkan nama instansi" class="form-control" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="posisi">Posisi <b class="text-danger">*</b></label>
                            <input type="text" name="posisi" placeholder="Masukkan posisi" class="form-control" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="alamat_instansi">Alamat Instansi <b class="text-danger">*</b></label>
                            <textarea name="alamat_instansi" id="alamat_instansi" class="form-control" style="height: 100px;" required></textarea>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" value="1" name="is_current_position" id="current">
                                <label class="custom-control-label" for="current">Saya saat ini bekerja di posisi ini</label>
                                <div class="invalid-feedback">Wajib diisi</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="start_date">Tanggal Mulai <b class="text-danger">*</b></label>
                            <input type="text" name="start_date" placeholder="Masukkan tanggal mulai" class="form-control datepicker2" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="end_date">Tanggal Selesai <b class="text-danger">*</b></label>
                            <input type="text" name="end_date" placeholder="Masukkan tanggal selesai" class="form-control datepicker2" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <label for="url_bukti">Sertifikat <b class="text-danger">*</b></label>
                        <div id="ganti-berkas-pengalaman" class="form-group">
                            <div id="edit-berkas-pengalaman">

                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" name="new_upload_pengalaman" id="new_upload_pengalaman" class="custom-control-input" onclick="uploadFoto('pengalaman','url_photo')" value="1">
                                <label class="custom-control-label" for="new_upload_pengalaman">Ganti Berkas</label>
                            </div>
                        </div>
                        <div id="new-upload-pengalaman" class="form-group">
                            <label for="input_file_pengalaman" class="btn btn-primary text-white">Pilih Berkas</label>
                            <input type="file" name="url_bukti" id="input_file_pengalaman" class="form-control custom-file" onchange="cekFile(this, 'pengalaman')" required>
                            <span class="file-info-pengalaman text-muted">Tidak ada berkas yang dipilih</span>
                            <div id="preview-pengalaman" class="col-md-12">

                            </div>
                            <div class="hint-block text-muted mt-3">
                                <small>
                                    Jenis file yang diijinkan: <strong>PNG, JPEG, JPG, PDF</strong><br>
                                    Ukuran file maksimal: <strong>2 MB</strong>
                                </small>
                            </div>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi <b class="text-danger">*</b></label>
                            <textarea name="deskripsi" id="deskripsi" class="form-control" style="height: 100px;" required></textarea>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="organisasi" role="tabpanel" aria-labelledby="organisasi-tab">
                <div class="row justify-content-between">
                    <div class="col-lg-4 title">
                        <h3><i class="bx bxs-crown"></i> Organisasi</h3>
                    </div>
                    <div id="add-organisasi" class="col-lg-1">
                        <div class="row">
                            <button class="btn btn-action-profil" onclick="addOrganisasi()"><i class="fa fa-square-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div id="div-total-organisasi" class="col">
                    <ul class="pagination justify-content-start">
                        <li class="page-item"><span id="total-organisasi" class="page-link disabled">Total</span></li>
                    </ul>
                </div>

                <!-- tabel organisasi -->
                <div id="tabel-organisasi">
                </div>

                <nav id="nav-organisasi" aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                        <li class="page-item"><button id="btn_prev-organisasi" class="page-link">Sebelumnya</button></li>
                        <li class="page-item"><span id="page_span-organisasi" class="page-link disabled"></span></li>
                        <li class="page-item"><button id="btn_next-organisasi" class="page-link">Selanjutnya</button></li>
                    </ul>
                </nav>

                <div id="form-organisasi" class="d-none">
                    <div class="form-group">
                        <button class="btn btn-light-danger" onclick="kembaliOrganisasi()"><i class="fa fa-chevron-left"></i> Kembali</button>
                        <span id="title-organisasi" class="badge bg-lime text-danger">Tambah organisasi</span>
                    </div>
                    <form id="form_organisasi" class="needs-validation" enctype="multipart/form-data" novalidate>
                        <input type="text" name="simpan_organisasi" value="1" class="form-control d-none" required>
                        <input type="text" name="id_organisasi" class="form-control d-none" required>
                        <div class="form-group">
                            <label for="nama_organisasi">Nama Organisasi <b class="text-danger">*</b></label>
                            <input type="text" name="nama_organisasi" placeholder="Masukkan nama organisasi" class="form-control" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="posisi">Posisi <b class="text-danger">*</b></label>
                            <input type="text" name="posisi" placeholder="Masukkan posisi" class="form-control" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="job_desc">job desc <b class="text-danger">*</b></label>
                            <textarea name="job_desc" id="job_desc" class="form-control" style="height: 100px;" required></textarea>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" value="1" name="is_current_position" id="flexCheckDefault">
                                <label class="custom-control-label" for="flexCheckDefault">Saya saat ini memegang posisi ini</label>
                                <div class="invalid-feedback">Wajib diisi</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="start_date">Tanggal Mulai <b class="text-danger">*</b></label>
                            <input type="text" name="start_date" placeholder="Masukkan tanggal mulai" class="form-control datepicker2" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="end_date">Tanggal Selesai <b class="text-danger">*</b></label>
                            <input type="text" name="end_date" placeholder="Masukkan tanggal selesai" class="form-control datepicker2" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <label for="url_bukti">Sertifikat <b class="text-danger">*</b></label>
                        <div id="ganti-berkas-organisasi" class="form-group">
                            <div id="edit-berkas-organisasi">

                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" name="new_upload_organisasi" id="new_upload_organisasi" class="custom-control-input" onclick="uploadFoto('organisasi','url_photo')" value="1">
                                <label class="custom-control-label" for="new_upload_organisasi">Ganti Berkas</label>
                            </div>
                        </div>
                        <div id="new-upload-organisasi" class="form-group">
                            <label for="input_file_organisasi" class="btn btn-primary text-white">Pilih Berkas</label>
                            <input type="file" name="url_bukti" id="input_file_organisasi" class="form-control custom-file" onchange="cekFile(this, 'organisasi')" required>
                            <span class="file-info-organisasi text-muted">Tidak ada berkas yang dipilih</span>
                            <div id="preview-organisasi" class="col-md-12">

                            </div>
                            <div class="hint-block text-muted mt-3">
                                <small>
                                    Jenis file yang diijinkan: <strong>PNG, JPEG, JPG, PDF</strong><br>
                                    Ukuran file maksimal: <strong>2 MB</strong>
                                </small>
                            </div>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi <b class="text-danger">*</b></label>
                            <textarea name="deskripsi" id="deskripsi" class="form-control" style="height: 100px;" required></textarea>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="prestasi" role="tabpanel" aria-labelledby="prestasi-tab">
                <div class="row justify-content-between">
                    <div class="col-lg-4 title">
                        <h3><i class="fa fa-award"></i> Prestasi</h3>
                    </div>
                    <div id="add-prestasi" class="col-lg-1">
                        <div class="row">
                            <button class="btn btn-action-profil" onclick="addPrestasi()"><i class="fa fa-square-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div id="div-total-prestasi" class="col">
                    <ul class="pagination justify-content-start">
                        <li class="page-item"><span id="total-prestasi" class="page-link disabled">Total</span></li>
                    </ul>
                </div>

                <!-- tabel prestasi -->
                <div id="tabel-prestasi">
                </div>

                <nav id="nav-prestasi" aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                        <li class="page-item"><button id="btn_prev-prestasi" class="page-link">Sebelumnya</button></li>
                        <li class="page-item"><span id="page_span-prestasi" class="page-link disabled"></span></li>
                        <li class="page-item"><button id="btn_next-prestasi" class="page-link">Selanjutnya</button></li>
                    </ul>
                </nav>

                <div id="form-prestasi" class="d-none">
                    <div class="form-group">
                        <button class="btn btn-light-danger" onclick="kembaliPrestasi()"><i class="fa fa-chevron-left"></i> Kembali</button>
                        <span id="title-prestasi" class="badge bg-lime text-danger">Tambah Prestasi</span>
                    </div>
                    <form id="form_prestasi" class="needs-validation" enctype="multipart/form-data" novalidate>
                        <input type="text" name="simpan_prestasi" value="1" class="form-control d-none" required>
                        <input type="text" name="id_prestasi" class="form-control d-none" required>
                        <div class="form-group">
                            <label for="nama_prestasi">Nama Prestasi <b class="text-danger">*</b></label>
                            <input type="text" name="nama_prestasi" placeholder="Masukkan nama prestasi" class="form-control" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="penyelenggara">Nama Penyelenggara <b class="text-danger">*</b></label>
                            <input type="text" name="penyelenggara" placeholder="Masukkan nama penyelenggara" class="form-control" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label>Tahun <b class="text-danger">*</b></label>
                            <select name="tahun" id="tahun" class="form-control select2" required>
                                <option value="">- Pilih Tahun -</option>
                                <?php foreach ($tahun as $key => $value) { ?>
                                    <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="tingkat">Tingkat <b class="text-danger">*</b></label>
                            <select name="tingkat" id="tingkat" class="form-control select2">
                                <option value="">- Pilih Tingkat -</option>
                                <option value="Internasional">Internasional</option>
                                <option value="Nasional">Nasional</option>
                                <option value="Provinsi">Provinsi</option>
                                <option value="Kabupaten/Kota">Kabupaten/Kota</option>
                            </select>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="gelar">Gelar <b class="text-danger">*</b></label>
                            <select name="gelar" id="gelar" class="form-control select2">
                                <option value="">- Pilih Gelar -</option>
                                <option value="Juara 1">Juara 1</option>
                                <option value="Juara 2">Juara 2</option>
                                <option value="Juara 3">Juara 3</option>
                            </select>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <label for="url_sertifikat">Sertifikat <b class="text-danger">*</b></label>
                        <div id="ganti-berkas-prestasi" class="form-group">
                            <div id="edit-berkas-prestasi">

                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" name="new_upload_prestasi" id="new_upload_prestasi" class="custom-control-input" onclick="uploadFoto('prestasi','url_photo')" value="1">
                                <label class="custom-control-label" for="new_upload_prestasi">Ganti Berkas</label>
                            </div>
                        </div>
                        <div id="new-upload-prestasi" class="form-group">
                            <label for="input_file_prestasi" class="btn btn-primary text-white">Pilih Berkas</label>
                            <input type="file" name="url_sertifikat" id="input_file_prestasi" class="form-control custom-file" onchange="cekFile(this, 'prestasi')" required>
                            <span class="file-info-prestasi text-muted">Tidak ada berkas yang dipilih</span>
                            <div id="preview-prestasi" class="col-md-12">

                            </div>
                            <div class="hint-block text-muted mt-3">
                                <small>
                                    Jenis file yang diijinkan: <strong>PNG, JPEG, JPG, PDF</strong><br>
                                    Ukuran file maksimal: <strong>2 MB</strong>
                                </small>
                            </div>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="skill" role="tabpanel" aria-labelledby="skill-tab">
                <div class="row justify-content-between">
                    <div class="col-lg-4 title">
                        <h3><i class="fa fa-crown"></i> Keahlian/Skill</h3>
                    </div>
                    <div id="add-skill" class="col-lg-1">
                        <div class="row">
                            <button class="btn btn-action-profil" onclick="addSkill()"><i class="fa fa-square-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div id="div-total-skill" class="col">
                    <ul class="pagination justify-content-start">
                        <li class="page-item"><span id="total-skill" class="page-link disabled">Total</span></li>
                    </ul>
                </div>

                <!-- tabel skill -->
                <div id="tabel-skill">
                </div>

                <nav id="nav-skill" aria-label="Page navigation example">
                    <ul class="pagination justify-content-end">
                        <li class="page-item"><button id="btn_prev-skill" class="page-link">Sebelumnya</button></li>
                        <li class="page-item"><span id="page_span-skill" class="page-link disabled"></span></li>
                        <li class="page-item"><button id="btn_next-skill" class="page-link">Selanjutnya</button></li>
                    </ul>
                </nav>

                <div id="form-skill" class="d-none">
                    <div class="form-group">
                        <button class="btn btn-light-danger" onclick="kembaliSkill()"><i class="fa fa-chevron-left"></i> Kembali</button>
                        <span id="title-skill" class="badge bg-lime text-danger">Tambah Skill</span>
                    </div>
                    <form id="form_skill" class="needs-validation" enctype="multipart/form-data" novalidate>
                        <input type="text" name="simpan_skill" value="1" class="form-control d-none" required>
                        <input type="text" name="id_skill" class="form-control d-none" required>
                        <div class="form-group">
                            <label for="nama_skill">Nama Skill <b class="text-danger">*</b></label>
                            <input type="text" name="nama_skill" placeholder="Masukkan nama skill" class="form-control" required>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label>Bidang <b class="text-danger">*</b></label>
                            <select name="bidang" id="bidang" class="form-control select2" required>
                                <option value="">- Pilih Bidang -</option>
                                <option value="Pertanian">Pertanian</option>
                                <option value="Non Pertanian">Non Pertanian</option>
                            </select>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group">
                            <label for="level">Level <b class="text-danger">*</b></label>
                            <select name="level" id="level" class="form-control select2">
                                <option value="">- Pilih Level -</option>
                                <option value="Beginner">Beginner</option>
                                <option value="Intermediate">Intermediate</option>
                                <option value="Advance">Advance</option>
                            </select>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <label for="url_sertifikat">Sertifikat <b class="text-danger">*</b></label>
                        <div id="ganti-berkas-skill" class="form-group">
                            <div id="edit-berkas-skill">

                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" name="new_upload_skill" id="new_upload_skill" class="custom-control-input" onclick="uploadFoto('skill','url_photo')" value="1">
                                <label class="custom-control-label" for="new_upload_skill">Ganti Berkas</label>
                            </div>
                        </div>
                        <div id="new-upload-skill" class="form-group">
                            <label for="input_file_skill" class="btn btn-primary text-white">Pilih Berkas</label>
                            <input type="file" name="url_sertifikat" id="input_file_skill" class="form-control custom-file" onchange="cekFile(this, 'skill')" required>
                            <span class="file-info-skill text-muted">Tidak ada berkas yang dipilih</span>
                            <div id="preview-skill" class="col-md-12">

                            </div>
                            <div class="hint-block text-muted mt-3">
                                <small>
                                    Jenis file yang diijinkan: <strong>PNG, JPEG, JPG, PDF</strong><br>
                                    Ukuran file maksimal: <strong>2 MB</strong>
                                </small>
                            </div>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane fade" id="sosmed" role="tabpanel" aria-labelledby="sosmed-tab">
                <div class="row justify-content-between">
                    <div class="col-lg-4 title">
                        <h3><i class="fa fa-at"></i> Sosial Media</h3>
                    </div>
                </div>

                <form id="form_sosmed" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label for="facebook">Facebook</label>
                        <input type="text" class="form-control" name="facebook" placeholder="Masukkan url facebook anda" value="<?php echo $alumni->facebook ?>">
                        <small class="text-info">Contoh : https://www.facebook.com/pusdiktan/</small>
                    </div>
                    <div class="form-group">
                        <label for="twitter">Twitter</label>
                        <input type="text" class="form-control" name="twitter" placeholder="Masukkan url twitter anda" value="<?php echo $alumni->twitter ?>">
                        <small class="text-info">Contoh : https://twitter.com/pusdiktan</small>
                    </div>
                    <div class="form-group">
                        <label for="linkedin">Linkedin</label>
                        <input type="text" class="form-control" name="linkedin" placeholder="Masukkan url linkedin anda" value="<?php echo $alumni->linkedin ?>">
                        <small class="text-info">Contoh : https://www.linkedin.com/in/pusdiktan-b5b94b109/</small>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-outline-danger">SIMPAN</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="akun" role="tabpanel" aria-labelledby="akun-tab">
                <div class="row justify-content-between">
                    <div class="col-lg-3 title">
                        <h3><i class="fa fa-key"></i> Akun</h3>
                    </div>
                    <div class="col-lg-1">
                        <button class="btn btn-action-profil"><i class="fa fa-pencil"></i></button>
                    </div>
                </div>
                <form id="form_akun" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label>Username <b class="text-danger">*</b></label>
                        <input type="text" name="username" value="<?php echo $alumni->username ?>" class="form-control" placeholder="Masukkan username anda" required>
                        <div class="invalid-feedback">Email tidak sesuai</div>
                    </div>

                    <div class="form-group">
                        <label>Password Lama<b class="text-danger">*</b></label>
                        <div class="input-group mb-3">
                            <input type="password" name="password_lama" class="form-control" placeholder="Masukkan password" required>
                            <a class="input-group-text show-old-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Password Baru<b class="text-danger">*</b></label>
                        <div class="input-group mb-3">
                            <input type="password" name="password" class="form-control" placeholder="Masukkan password" required>
                            <a class="input-group-text show-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Konfirmasi Password <b class="text-danger">*</b></label>
                        <div class="input-group mb-3">
                            <input type="password" name="konfirmasi_password" class="form-control" placeholder="Masukkan konfirmasi password anda" required>
                            <a class="input-group-text show-confirm-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                            <div class="invalid-feedback">Wajib diisi</div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-outline-danger px-5">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var site_url = '<?php echo site_url() ?>profil_alumni/index/';
    let status = "<?php echo $alumni->id_ref_status_alumni ?>";

    $(document).ready(function() {
        if (status == 1) {
            // remove or add d-none
            $('#div-instansi').attr('class', 'form-group');
            $('#div-tgl-mulai').attr('class', 'form-group');
            $('#div-jabatan').attr('class', 'form-group');
            $('#div-prodi').attr('class', 'form-group d-none');
            $('#div-sektor').attr('class', 'form-group');
            $('#div-instansi-provinsi').attr('class', 'form-group');
            $('#div-instansi-kab-kota').attr('class', 'form-group');
            $('#div-instansi-kecamatan').attr('class', 'form-group');
            $('#div-alamat').attr('class', 'form-group');

            // change label
            $('#instansi').html('Nama Instansi <b class="text-danger">*</b>');

            // change placeholder
            $('[name="nama_instansi"]').attr('placeholder', 'Masukkan nama instansi anda');

            // remove or add required
            $('[name="nama_instansi"]').attr('required', true);
            $('[name="tgl_mulai"]').attr('required', true);
            $('[name="jabatan"]').attr('required', true);
            $('[name="prodi"]').attr('required', false);
            $('[name="sektor"]').attr('required', true);
            $('[name="instansi_kode_prov"]').attr('required', true);
            $('[name="instansi_kode_kab"]').attr('required', true);
            $('[name="instansi_kode_kec"]').attr('required', true);
            $('[name="instansi_alamat"]').attr('required', true);
        } else if (status == 3) {
            // remove or add d-none
            $('#div-instansi').attr('class', 'form-group');
            $('#div-tgl-mulai').attr('class', 'form-group');
            $('#div-jabatan').attr('class', 'form-group d-none');
            $('#div-prodi').attr('class', 'form-group d-none');
            $('#div-sektor').attr('class', 'form-group');
            $('#div-instansi-provinsi').attr('class', 'form-group');
            $('#div-instansi-kab-kota').attr('class', 'form-group');
            $('#div-instansi-kecamatan').attr('class', 'form-group');
            $('#div-alamat').attr('class', 'form-group');

            // change label
            $('#instansi').html('Nama Usaha <b class="text-danger">*</b>');

            // change placeholder
            $('[name="nama_instansi"]').attr('placeholder', 'Masukkan nama usaha anda');

            // remove or add required
            $('[name="nama_instansi"]').attr('required', true);
            $('[name="tgl_mulai"]').attr('required', true);
            $('[name="jabatan"]').attr('required', false);
            $('[name="prodi"]').attr('required', false);
            $('[name="sektor"]').attr('required', true);
            $('[name="instansi_kode_prov"]').attr('required', true);
            $('[name="instansi_kode_kab"]').attr('required', true);
            $('[name="instansi_kode_kec"]').attr('required', true);
            $('[name="instansi_alamat"]').attr('required', true);
        } else if (status == 4) {
            // remove or add d-none
            $('#div-instansi').attr('class', 'form-group');
            $('#div-tgl-mulai').attr('class', 'form-group');
            $('#div-jabatan').attr('class', 'form-group d-none');
            $('#div-prodi').attr('class', 'form-group');
            $('#div-sektor').attr('class', 'form-group');
            $('#div-instansi-provinsi').attr('class', 'form-group');
            $('#div-instansi-kab-kota').attr('class', 'form-group');
            $('#div-instansi-kecamatan').attr('class', 'form-group');
            $('#div-alamat').attr('class', 'form-group d-none');

            // change label
            $('#instansi').html('Nama Perguruan Tinggi <b class="text-danger">*</b>');

            // change placeholder
            $('[name="nama_instansi"]').attr('placeholder', 'Masukkan nama perguruan tinggi anda');

            // remove or add required
            $('[name="nama_instansi"]').attr('required', true);
            $('[name="tgl_mulai"]').attr('required', true);
            $('[name="jabatan"]').attr('required', false);
            $('[name="prodi"]').attr('required', true);
            $('[name="sektor"]').attr('required', true);
            $('[name="instansi_kode_prov"]').attr('required', true);
            $('[name="instansi_kode_kab"]').attr('required', true);
            $('[name="instansi_kode_kec"]').attr('required', true);
            $('[name="instansi_alamat"]').attr('required', false);
        } else {
            // remove or add d-none
            $('#div-instansi').attr('class', 'form-group d-none');
            $('#div-tgl-mulai').attr('class', 'form-group d-none');
            $('#div-jabatan').attr('class', 'form-group d-none');
            $('#div-prodi').attr('class', 'form-group d-none');
            $('#div-sektor').attr('class', 'form-group d-none');
            $('#div-instansi-provinsi').attr('class', 'form-group d-none');
            $('#div-instansi-kab-kota').attr('class', 'form-group d-none');
            $('#div-instansi-kecamatan').attr('class', 'form-group d-none');
            $('#div-alamat').attr('class', 'form-group d-none');

            // remove or add required
            $('[name="nama_instansi"]').attr('required', false);
            $('[name="tgl_mulai"]').attr('required', false);
            $('[name="jabatan"]').attr('required', false);
            $('[name="prodi"]').attr('required', false);
            $('[name="sektor"]').attr('required', false);
            $('[name="instansi_kode_prov"]').attr('required', false);
            $('[name="instansi_kode_kab"]').attr('required', false);
            $('[name="instansi_kode_kec"]').attr('required', false);
            $('[name="instansi_alamat"]').attr('required', false);
        } {

        }
    });

    /**
     * submit profil alumni
     */
    $('#form_profil').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: site_url + "update_profil",
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                if (data.success) {
                    swal({
                        title: 'Berhasil',
                        text: 'Ubah data alumni berhasil',
                        type: 'success',
                        confirmButtonColor: '#396113',
                    }, ).then(function() {
                        window.location.href = site_url;
                    });
                } else {
                    if (typeof data.text !== "undefined") {
                        swal({
                            title: 'Ups...',
                            html: data.text,
                            type: 'warning',
                            confirmButtonColor: '#396113',
                        }, );
                    } else {
                        swal({
                            title: 'Ups...',
                            text: 'Data Gagal Disimpan',
                            type: 'error',
                            confirmButtonColor: '#396113',
                        }, );
                    }
                }
            },
            error: function(error) {
                dismiss_loading();
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                }, );
            }
        });
    });

    /**
     * submit pengalaman
     */
    $('#form_pengalaman').on('submit', function(e) {
        e.preventDefault();
        var url = site_url + "simpan_pengalaman";
        var simpan = $('[name=simpan_pengalaman]').val();
        var txtSuccess = 'Simpan data pengalaman berhasil';
        var txtError = 'Data gagal disimpan';

        if (simpan == '0') {
            url = site_url + "update_pengalaman";
            txtSuccess = 'Ubah data pengalaman berhasil';
            txtError = 'Data gagal diubah';
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                if (data.success) {
                    swal({
                        title: 'Berhasil',
                        text: txtSuccess,
                        type: 'success',
                        confirmButtonColor: '#396113',
                    }, ).then(function() {
                        refreshTabel(data.pengalaman, 'pengalaman');
                        kembaliPengalaman();
                    });
                } else {
                    if (typeof data.text !== "undefined") {
                        swal({
                            title: 'Ups...',
                            html: data.text,
                            type: 'warning',
                            confirmButtonColor: '#396113',
                        }, );
                    } else {
                        swal({
                            title: 'Ups...',
                            text: txtError,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        }, );
                    }
                }
            },
            error: function(error) {
                dismiss_loading();
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                }, );
            }
        });
    });

    /**
     * hapus pengalaman
     */
    function hapusPengalaman(id) {
        $.ajax({
            type: 'ajax',
            method: 'POST',
            data: {
                id_pengalaman: id
            },
            url: site_url + 'delete_pengalaman',
            dataType: 'JSON',
            success: function(data) {
                swal({
                    title: 'Berhasil',
                    text: 'Data Berhasil Dihapus',
                    type: 'success',
                    confirmButtonColor: '#396113',
                }, ).then(function() {
                    refreshTabel(data.pengalaman, 'pengalaman');
                });
            },
            error: function(xx) {
                console.log(xx);
            }
        });
    }

    /**
     * submit organisasi
     */
    $('#form_organisasi').on('submit', function(e) {
        e.preventDefault();
        var url = site_url + "simpan_organisasi";
        var simpan = $('[name=simpan_organisasi]').val();
        var txtSuccess = 'Simpan data organisasi berhasil';
        var txtError = 'Data gagal disimpan';

        if (simpan == '0') {
            url = site_url + "update_organisasi";
            txtSuccess = 'Ubah data organisasi berhasil';
            txtError = 'Data gagal diubah';
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                if (data.success) {
                    swal({
                        title: 'Berhasil',
                        text: txtSuccess,
                        type: 'success',
                        confirmButtonColor: '#396113',
                    }, ).then(function() {
                        refreshTabel(data.organisasi, 'organisasi');
                        kembaliOrganisasi();
                    });
                } else {
                    if (typeof data.text !== "undefined") {
                        swal({
                            title: 'Ups...',
                            html: data.text,
                            type: 'warning',
                            confirmButtonColor: '#396113',
                        }, );
                    } else {
                        swal({
                            title: 'Ups...',
                            text: txtError,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        }, );
                    }
                }
            },
            error: function(error) {
                dismiss_loading();
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                }, );
            }
        });
    });

    /**
     * hapus organisasi
     */
    function hapusOrganisasi(id) {
        $.ajax({
            type: 'ajax',
            method: 'POST',
            data: {
                id_organisasi: id
            },
            url: site_url + 'delete_organisasi',
            dataType: 'JSON',
            success: function(data) {
                swal({
                    title: 'Berhasil',
                    text: 'Data Berhasil Dihapus',
                    type: 'success',
                    confirmButtonColor: '#396113',
                }, ).then(function() {
                    refreshTabel(data.organisasi, 'organisasi');
                });
            },
            error: function(xx) {
                console.log(xx);
            }
        });
    }

    /**
     * submit prestasi
     */
    $('#form_prestasi').on('submit', function(e) {
        e.preventDefault();
        var url = site_url + "simpan_prestasi";
        var simpan = $('[name=simpan_prestasi]').val();
        var txtSuccess = 'Simpan data prestasi berhasil';
        var txtError = 'Data gagal disimpan';

        if (simpan == '0') {
            url = site_url + "update_prestasi";
            txtSuccess = 'Ubah data prestasi berhasil';
            txtError = 'Data gagal diubah';
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                if (data.success) {
                    swal({
                        title: 'Berhasil',
                        text: txtSuccess,
                        type: 'success',
                        confirmButtonColor: '#396113',
                    }, ).then(function() {
                        refreshTabel(data.prestasi, 'prestasi');
                        kembaliPrestasi();
                    });
                } else {
                    if (typeof data.text !== "undefined") {
                        swal({
                            title: 'Ups...',
                            html: data.text,
                            type: 'warning',
                            confirmButtonColor: '#396113',
                        }, );
                    } else {
                        swal({
                            title: 'Ups...',
                            text: txtError,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        }, );
                    }
                }
            },
            error: function(error) {
                dismiss_loading();
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                }, );
            }
        });
    });

    /**
     * hapus prestasi
     */
    function hapusPrestasi(id) {
        $.ajax({
            type: 'ajax',
            method: 'POST',
            data: {
                id_prestasi: id
            },
            url: site_url + 'delete_prestasi',
            dataType: 'JSON',
            success: function(data) {
                swal({
                    title: 'Berhasil',
                    text: 'Data Berhasil Dihapus',
                    type: 'success',
                    confirmButtonColor: '#396113',
                }, ).then(function() {
                    refreshTabel(data.prestasi, 'prestasi');
                });
            },
            error: function(xx) {
                console.log(xx);
            }
        });
    }

    /**
     * submit skill
     */
    $('#form_skill').on('submit', function(e) {
        e.preventDefault();
        var url = site_url + "simpan_skill";
        var simpan = $('[name=simpan_skill]').val();
        var txtSuccess = 'Simpan data skill berhasil';
        var txtError = 'Data gagal disimpan';

        if (simpan == '0') {
            url = site_url + "update_skill";
            txtSuccess = 'Ubah data skill berhasil';
            txtError = 'Data gagal diubah';
        }

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                if (data.success) {
                    swal({
                        title: 'Berhasil',
                        text: txtSuccess,
                        type: 'success',
                        confirmButtonColor: '#396113',
                    }, ).then(function() {
                        refreshTabel(data.skill, 'skill');
                        kembaliSkill();
                    });
                } else {
                    if (typeof data.text !== "undefined") {
                        swal({
                            title: 'Ups...',
                            html: data.text,
                            type: 'warning',
                            confirmButtonColor: '#396113',
                        }, );
                    } else {
                        swal({
                            title: 'Ups...',
                            text: txtError,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        }, );
                    }
                }
            },
            error: function(error) {
                dismiss_loading();
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                }, );
            }
        });
    });

    /**
     * hapus skill
     */
    function hapusSkill(id) {
        $.ajax({
            type: 'ajax',
            method: 'POST',
            data: {
                id_skill: id
            },
            url: site_url + 'delete_skill',
            dataType: 'JSON',
            success: function(data) {
                swal({
                    title: 'Berhasil',
                    text: 'Data Berhasil Dihapus',
                    type: 'success',
                    confirmButtonColor: '#396113',
                }, ).then(function() {
                    refreshTabel(data.skill, 'skill');
                });
            },
            error: function(xx) {
                console.log(xx);
            }
        });
    }

    /**
     * submit profil alumni
     */
    $('#form_sosmed').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: site_url + "update_sosmed",
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                if (data.success) {
                    swal({
                        title: 'Berhasil',
                        text: 'Ubah data alumni berhasil',
                        type: 'success',
                        confirmButtonColor: '#396113',
                    }, ).then(function() {
                        window.location.href = site_url;
                    });
                } else {
                    if (typeof data.text !== "undefined") {
                        swal({
                            title: 'Ups...',
                            html: data.text,
                            type: 'warning',
                            confirmButtonColor: '#396113',
                        }, );
                    } else {
                        swal({
                            title: 'Ups...',
                            text: 'Data Gagal Disimpan',
                            type: 'error',
                            confirmButtonColor: '#396113',
                        }, );
                    }
                }
            },
            error: function(error) {
                dismiss_loading();
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                }, );
            }
        });
    });

    /**
     * submit akun alumni
     */
    $('#form_akun').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: site_url + "update_akun",
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                if (data.success) {
                    swal({
                        title: 'Berhasil',
                        text: 'Ubah data akun berhasil',
                        type: 'success',
                        confirmButtonColor: '#396113',
                    }, ).then(function() {
                        window.location.href = site_url;
                    });
                } else {
                    if (typeof data.text !== "undefined") {
                        swal({
                            title: 'Ups...',
                            html: data.text,
                            type: 'warning',
                            confirmButtonColor: '#396113',
                        }, );
                    } else {
                        swal({
                            title: 'Ups...',
                            text: 'Data Gagal Disimpan',
                            type: 'error',
                            confirmButtonColor: '#396113',
                        }, );
                    }
                }
            },
            error: function(error) {
                dismiss_loading();
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                }, );
            }
        });
    });
</script>

<!-- tabel -->
<script>
    let current_page = 1;
    let records_per_page = 5;

    let objJson;

    // tab pengalaman
    $('#pengalaman-tab').on('click', function(e) {
        $.get(site_url + "getPengalaman", function(data, status) {
            refreshTabel(JSON.parse(data), 'pengalaman')
            kembaliPengalaman()
        });
    });

    // tab organisasi
    $('#organisasi-tab').on('click', function(e) {
        $.get(site_url + "getOrganisasi", function(data, status) {
            refreshTabel(JSON.parse(data), 'organisasi')
            kembaliOrganisasi()
        });
    });

    // tab prestasi
    $('#prestasi-tab').on('click', function(e) {
        $.get(site_url + "getPrestasi", function(data, status) {
            refreshTabel(JSON.parse(data), 'prestasi')
            kembaliPrestasi()
        });
    });

    // tab skill
    $('#skill-tab').on('click', function(e) {
        $.get(site_url + "getSkill", function(data, status) {
            refreshTabel(JSON.parse(data), 'skill')
            kembaliSkill()
        });
    });

    function prevPage(event) {
        if (current_page > 1) {
            current_page--;
            changePage(current_page, event.data.name);
        }
    }

    function nextPage(event) {
        if (current_page < numPages()) {
            current_page++;
            changePage(current_page, event.data.name);
        }
    }

    function changePage(page, name) {
        // array objJson baru
        let tempObjJson = objJson.map(function(obj) {
            return obj;
        });

        // Validate page
        if (page < 1) page = 1;
        if (page > numPages(tempObjJson)) page = numPages(tempObjJson);

        $('#tabel-' + name).html('');

        if (tempObjJson.length <= 0) {
            $('#tabel-' + name).html('<labe>Tidak ada data</label>');
            $('#btn_prev-' + name + ', #btn_next-' + name).css('visibility', 'hidden');
            $('#page_span-' + name).html('');
            $('#total-' + name).html('');
            $('#total-' + name).addClass('d-none');
            return;
        }

        // MEMBUAT CARD HTML
        let tempAllCard = '';
        for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < tempObjJson.length; i++) {
            if (name == 'pengalaman') {
                tempAllCard += renderPengalaman(tempObjJson[i]);
            }

            if (name == 'organisasi') {
                tempAllCard += renderOrganisasi(tempObjJson[i]);
            }

            if (name == 'prestasi') {
                tempAllCard += renderPrestasi(tempObjJson[i]);
            }

            if (name == 'skill') {
                tempAllCard += renderSkill(tempObjJson[i]);
            }
        };

        $('#tabel-' + name).html(tempAllCard);
        $('#page_span-' + name).html(page + '/' + numPages(tempObjJson));
        $('#total-' + name).removeClass('d-none');
        $('#total-' + name).html('Total ' + tempObjJson.length);

        if (page == 1) {
            $('#btn_prev-' + name).css('visibility', 'hidden');
        } else {
            $('#btn_prev-' + name).css('visibility', 'visible');
        };

        if (page == numPages(tempObjJson)) {
            $('#btn_next-' + name).css('visibility', 'hidden');
        } else {
            $('#btn_next-' + name).css('visibility', 'visible');
        };
    };

    function numPages(obj = null) {
        if (obj === null) {
            return Math.ceil(objJson.length / records_per_page);
        };
        return Math.ceil(obj.length / records_per_page);
    }

    $(document).ready(function() {
        if (objJson) {
            changePage(1);
        };
        $('#btn_prev-pengalaman').on('click', {
            name: 'pengalaman'
        }, prevPage);
        $('#btn_next-pengalaman').on('click', {
            name: 'pengalaman'
        }, nextPage);
        $('#btn_prev-organisasi').on('click', {
            name: 'organisasi'
        }, prevPage);
        $('#btn_next-organisasi').on('click', {
            name: 'organisasi'
        }, nextPage);
        $('#btn_prev-prestasi').on('click', {
            name: 'prestasi'
        }, prevPage);
        $('#btn_next-prestasi').on('click', {
            name: 'prestasi'
        }, nextPage);
        $('#btn_prev-skill').on('click', {
            name: 'skill'
        }, prevPage);
        $('#btn_next-skill').on('click', {
            name: 'skill'
        }, nextPage);
    });

    function refreshTabel(data, name) {
        objJson = data;
        current_page = 1;
        changePage(1, name);
    }

    function changeDate(date) {
        var mydate = new Date(date);
        var month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "December"][mydate.getMonth()];
        var str = month + ' ' + mydate.getFullYear();
        return str;
    }

    function renderPengalaman(obj) {
        let id = obj['id'] ? obj['id'] : '';
        let posisi = obj['posisi'] ? obj['posisi'] : '';
        let nama_instansi = obj['nama_instansi'] ? obj['nama_instansi'] : ' ';
        let jenis = obj['jenis_pengalaman'] ? obj['jenis_pengalaman'] : ' ';
        let tanggal = (obj['start_date'] ? changeDate(obj['start_date']) : '') + ' - ' + (obj['end_date'] ? changeDate(obj['end_date']) : '');
        let alamat = obj['alamat'] ? obj['alamat'] : '';

        let template = `
                    <div class="row my-4">
                        <div class="col-md-2 align-self-center">
                            <div class="img-tabel">
                                <img src="<?php echo site_url() ?>public/assets/img/admin/pengalaman.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="posisi-title"><strong>` + posisi.toUpperCase() + `</strong></div>
                            <div class="instansi-title"><strong>` + nama_instansi.toUpperCase() + `</strong></div>
                            <div class="tgl-pengalaman">` + jenis + `</div>
                            <div class="tgl-pengalaman">` + tanggal + `</div>
                            <div class="alamat-pengalaman">` + alamat + `</div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-action-profil" onclick="editPengalaman(` + id + `)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-action-profil" onclick="deletePengalaman(` + id + `)"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    `;
        return template;
    }

    function kembaliPengalaman() {
        $('#form_pengalaman').removeClass('was-validated');
        $('#form-pengalaman').addClass('d-none');
        $('#tabel-pengalaman').removeClass('d-none');
        $('#div-total-pengalaman').removeClass('d-none');
        $('#nav-pengalaman').removeClass('d-none');
        $('#add-pengalaman').removeClass('d-none');
        $('#preview-pengalaman').html('');
    }

    function addPengalaman() {
        $('#form_pengalaman')[0].reset();
        $('[name="jenis_pengalaman"]').val('').trigger('change');
        $('#title-pengalaman').text('Tambah Pengalaman');
        $('#form-pengalaman').removeClass('d-none');
        $('#add-pengalaman').addClass('d-none');
        $('#tabel-pengalaman').addClass('d-none');
        $('#div-total-pengalaman').addClass('d-none');
        $('#nav-pengalaman').addClass('d-none');
        $('[name="simpan_pengalaman"]').val('1');
        $('#ganti-berkas-pengalaman').addClass('d-none');
        $('#new_upload_pengalaman').val('1');
        $('#edit-berkas-pengalaman').html('');
        $('#new-upload-pengalaman').removeClass('d-none');
        $('.file-info-pengalaman').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-pengalaman').html('');
    }

    function editPengalaman(id) {
        $('#form_pengalaman')[0].reset();
        $('[name="jenis_pengalaman"]').val('').trigger('change');
        $('.file-info-pengalaman').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-pengalaman').html('');
        $.ajax({
            url: site_url + 'getPengalaman',
            type: "POST",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                var tipe = data.url_bukti.split(/[#?]/)[0].split('.').pop().trim();
                var url_bukti = data.url_bukti;
                if (tipe == 'pdf') {
                    $('#ganti-berkas-pengalaman').removeClass('d-none');
                    $('#new_upload_pengalaman').val('1');
                    $('#edit-berkas-pengalaman').html('<embed src="' + url_bukti + '" class="mt-4" height="800" width="100%"/>');
                    $('#new-upload-pengalaman').addClass('d-none');
                } else {
                    $('#ganti-berkas-pengalaman').removeClass('d-none');
                    $('#new_upload_pengalaman').val('1');
                    $('#edit-berkas-pengalaman').html('<img src="' + url_bukti + '" class="mt-4" alt="your image" style="width:250"/>');
                    $('#new-upload-pengalaman').addClass('d-none');
                }

                $('#title-pengalaman').text('Edit Pengalaman');
                $('#form-pengalaman').removeClass('d-none');
                $('#tabel-pengalaman').addClass('d-none');
                $('#nav-pengalaman').addClass('d-none');
                $('#div-total-pengalaman').addClass('d-none');
                $('#add-pengalaman').addClass('d-none');

                $('[name="simpan_pengalaman"]').val('0');
                $('[name="id_pengalaman"]').val(data.id);
                $('[name="jenis_pengalaman"]').val(data.jenis_pengalaman).trigger('change');
                $('[name="nama_instansi"]').val(data.nama_instansi);
                $('[name="posisi"]').val(data.posisi);
                $('[name="alamat_instansi"]').val(data.alamat);
                $('[name="start_date"]').val(data.start_date);
                $('[name="end_date"]').val(data.end_date);
                $('[name="deskripsi"]').val(data.deskripsi);

                if (data.is_current_position == 1) {
                    $('[name="is_current_position"]').prop('checked', true);
                }

            },
            error: function(xx) {
                swal("Galat", "Hubungi admin", "error");
            }
        });
    }

    function deletePengalaman(id) {
        swal({
            title: "Ingin Menghapus Data?",
            text: "Data tidak bisa dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Ya',
            confirmButtonColor: '#396113',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result) {
            if (result.value) {
                hapusPengalaman(id);
            }
        }).catch(function(reason) {
            // swal("Gagal Klik.....", "Gagal Pilih", 'error');
        });
    }

    function renderOrganisasi(obj) {
        let id = obj['id'] ? obj['id'] : '';
        let posisi = obj['posisi'] ? obj['posisi'] : '';
        let nama_organisasi = obj['nama_organisasi'] ? obj['nama_organisasi'] : ' ';
        let tanggal = (obj['start_date'] ? changeDate(obj['start_date']) : '') + ' - ' + (obj['end_date'] ? changeDate(obj['end_date']) : '');
        let job_desc = obj['job_desc'] ? obj['job_desc'] : '';

        let template = `
                    <div class="row my-4">
                        <div class="col-md-2 align-self-center">
                            <div class="img-tabel">
                                <img src="<?php echo site_url() ?>public/assets/img/admin/crown.svg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="posisi-title"><strong>` + posisi.toUpperCase() + `</strong></div>
                            <div class="instansi-title"><strong>` + nama_organisasi.toUpperCase() + `</strong></div>
                            <div class="tgl-pengalaman">` + tanggal + `</div>
                            <div class="alamat-pengalaman">` + job_desc + `</div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-action-profil" onclick="editOrganisasi(` + id + `)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-action-profil" onclick="deleteOrganisasi(` + id + `)"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    `;
        return template;
    }

    function kembaliOrganisasi() {
        $('#form_organisasi').removeClass('was-validated');
        $('#form-organisasi').addClass('d-none');
        $('#tabel-organisasi').removeClass('d-none');
        $('#div-total-organisasi').removeClass('d-none');
        $('#nav-organisasi').removeClass('d-none');
        $('#add-organisasi').removeClass('d-none');
        $('#preview-organisasi').html('');
    }

    function addOrganisasi() {
        $('#form_organisasi')[0].reset();
        $('#title-organisasi').text('Tambah Organisasi');
        $('#form-organisasi').removeClass('d-none');
        $('#add-organisasi').addClass('d-none');
        $('#tabel-organisasi').addClass('d-none');
        $('#div-total-organisasi').addClass('d-none');
        $('#nav-organisasi').addClass('d-none');
        $('[name="simpan_organisasi"]').val('1');
        $('#ganti-berkas-organisasi').addClass('d-none');
        $('#new_upload_organisasi').val('1');
        $('#edit-berkas-organisasi').html('');
        $('#new-upload-organisasi').removeClass('d-none');
        $('.file-info-organisasi').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-organisasi').html('');
    }

    function editOrganisasi(id) {
        $('#form_organisasi')[0].reset();
        $('.file-info-organisasi').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-organisasi').html('');
        $.ajax({
            url: site_url + 'getOrganisasi',
            type: "POST",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                var tipe = data.url_bukti.split(/[#?]/)[0].split('.').pop().trim();
                var url_bukti = data.url_bukti;
                if (tipe == 'pdf') {
                    $('#ganti-berkas-organisasi').removeClass('d-none');
                    $('#new_upload_organisasi').val('1');
                    $('#edit-berkas-organisasi').html('<embed src="' + url_bukti + '" class="mt-4" height="800" width="100%"/>');
                    $('#new-upload-organisasi').addClass('d-none');
                } else {
                    $('#ganti-berkas-pengalaman').removeClass('d-none');
                    $('#new_upload_pengalaman').val('1');
                    $('#edit-berkas-pengalaman').html('<img src="' + url_bukti + '" class="mt-4" alt="your image" style="width:250"/>');
                    $('#new-upload-pengalaman').addClass('d-none');
                }

                $('#title-organisasi').text('Edit Organisasi');
                $('#form-organisasi').removeClass('d-none');
                $('#tabel-organisasi').addClass('d-none');
                $('#div-total-organisasi').addClass('d-none');
                $('#nav-organisasi').addClass('d-none');
                $('#add-organisasi').addClass('d-none');

                $('[name="simpan_organisasi"]').val('0');
                $('[name="id_organisasi"]').val(data.id);
                $('[name="nama_organisasi"]').val(data.nama_organisasi);
                $('[name="posisi"]').val(data.posisi);
                $('[name="job_desc"]').val(data.job_desc);
                $('[name="start_date"]').val(data.start_date);
                $('[name="end_date"]').val(data.end_date);
                $('[name="deskripsi"]').val(data.deskripsi);

                if (data.is_current_position == 1) {
                    $('[name="is_current_position"]').prop('checked', true);
                }

            },
            error: function(xx) {
                swal("Galat", "Hubungi admin", "error");
            }
        });
    }

    function deleteOrganisasi(id) {
        swal({
            title: "Ingin Menghapus Data?",
            text: "Data tidak bisa dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Ya',
            confirmButtonColor: '#396113',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result) {
            if (result.value) {
                hapusOrganisasi(id);
            }
        }).catch(function(reason) {
            // swal("Gagal Klik.....", "Gagal Pilih", 'error');
        });
    }

    function renderPrestasi(obj) {
        let id = obj['id'] ? obj['id'] : '';
        let nama_prestasi = obj['nama_prestasi'] ? obj['nama_prestasi'] : ' ';
        let penyelenggara = obj['penyelenggara'] ? obj['penyelenggara'] : '';
        let tahun = obj['tahun'] ? obj['tahun'] : '';
        let tingkat = obj['tingkat'] ? obj['tingkat'] : '';
        let gelar = obj['gelar'] ? obj['gelar'] : '';

        let template = `
                    <div class="row my-4">
                        <div class="col-md-2 align-self-center">
                            <div class="img-tabel">
                                <img src="<?php echo site_url() ?>public/assets/img/admin/award.svg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="posisi-title"><strong>` + gelar + ' ' + nama_prestasi.toUpperCase() + `</strong></div>
                            <div class="instansi-title"><strong>` + penyelenggara.toUpperCase() + `</strong></div>
                            <div class="tgl-pengalaman">` + tingkat + ' ' + tahun + `</div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-action-profil" onclick="editPrestasi(` + id + `)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-action-profil" onclick="deletePrestasi(` + id + `)"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    `;
        return template;
    }

    function kembaliPrestasi() {
        $('#form_prestasi').removeClass('was-validated');
        $('#form-prestasi').addClass('d-none');
        $('#tabel-prestasi').removeClass('d-none');
        $('#div-total-prestasi').removeClass('d-none');
        $('#nav-prestasi').removeClass('d-none');
        $('#add-prestasi').removeClass('d-none');
        $('#preview-prestasi').html('');
    }

    function addPrestasi() {
        $('#form_prestasi')[0].reset();
        $('[name="tahun"]').val('').trigger('change');
        $('[name="tingkat"]').val('').trigger('change');
        $('[name="gelar"]').val('').trigger('change');
        $('#title-prestasi').text('Tambah Prestasi');
        $('#form-prestasi').removeClass('d-none');
        $('#add-prestasi').addClass('d-none');
        $('#tabel-prestasi').addClass('d-none');
        $('#div-total-prestasi').addClass('d-none');
        $('#nav-prestasi').addClass('d-none');
        $('[name="simpan_prestasi"]').val('1');
        $('#ganti-berkas-prestasi').addClass('d-none');
        $('#new_upload_prestasi').val('1');
        $('#edit-berkas-prestasi').html('');
        $('#new-upload-prestasi').removeClass('d-none');
        $('.file-info-prestasi').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-prestasi').html('');
    }

    function editPrestasi(id) {
        $('#form_prestasi')[0].reset();
        $('[name="tahun"]').val('').trigger('change');
        $('[name="tingkat"]').val('').trigger('change');
        $('[name="gelar"]').val('').trigger('change');
        $('.file-info-prestasi').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-prestasi').html('');
        $.ajax({
            url: site_url + 'getPrestasi',
            type: "POST",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                var tipe = data.url_sertifikat.split(/[#?]/)[0].split('.').pop().trim();
                var url_sertifikat = data.url_sertifikat;
                if (tipe == 'pdf') {
                    $('#ganti-berkas-prestasi').removeClass('d-none');
                    $('#new_upload_prestasi').val('1');
                    $('#edit-berkas-prestasi').html('<embed src="' + url_sertifikat + '" class="mt-4" height="800" width="100%"/>');
                    $('#new-upload-prestasi').addClass('d-none');
                } else {
                    $('#ganti-berkas-prestasi').removeClass('d-none');
                    $('#new_upload_prestasi').val('1');
                    $('#edit-berkas-prestasi').html('<img src="' + url_sertifikat + '" class="mt-4" alt="your image" style="width:250"/>');
                    $('#new-upload-prestasi').addClass('d-none');
                }

                $('#title-prestasi').text('Edit Prestasi');
                $('#form-prestasi').removeClass('d-none');
                $('#tabel-prestasi').addClass('d-none');
                $('#div-total-prestasi').addClass('d-none');
                $('#nav-prestasi').addClass('d-none');
                $('#add-prestasi').addClass('d-none');

                $('[name="simpan_prestasi"]').val('0');
                $('[name="id_prestasi"]').val(data.id);
                $('[name="nama_prestasi"]').val(data.nama_prestasi);
                $('[name="penyelenggara"]').val(data.penyelenggara);
                $('[name="tahun"]').val(data.tahun).trigger('change');
                $('[name="tingkat"]').val(data.tingkat).trigger('change');
                $('[name="gelar"]').val(data.gelar).trigger('change');
            },
            error: function(xx) {
                swal("Galat", "Hubungi admin", "error");
            }
        });
    }

    function deletePrestasi(id) {
        swal({
            title: "Ingin Menghapus Data?",
            text: "Data tidak bisa dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Ya',
            confirmButtonColor: '#396113',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result) {
            if (result.value) {
                hapusPrestasi(id);
            }
        }).catch(function(reason) {
            // swal("Gagal Klik.....", "Gagal Pilih", 'error');
        });
    }

    function renderSkill(obj) {
        let id = obj['id'] ? obj['id'] : '';
        let nama_skill = obj['nama_skill'] ? obj['nama_skill'] : ' ';
        let bidang = obj['bidang'] ? obj['bidang'] : '';
        let level = obj['level'] ? obj['level'] : '';

        let template = `
                    <div class="row my-4">
                        <div class="col-md-2 align-self-center">
                            <div class="img-tabel">
                                <img src="<?php echo site_url() ?>public/assets/img/admin/crown.svg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="posisi-title"><strong>` + nama_skill.toUpperCase() + `</strong></div>
                            <div class="tgl-pengalaman">` + bidang + `</div>
                            <div class="tgl-pengalaman">` + level + `</div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-action-profil" onclick="editSkill(` + id + `)"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-action-profil" onclick="deleteSkill(` + id + `)"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                    `;
        return template;
    }

    function kembaliSkill() {
        $('#form_skill').removeClass('was-validated');
        $('#form-skill').addClass('d-none');
        $('#tabel-skill').removeClass('d-none');
        $('#div-total-skill').removeClass('d-none');
        $('#nav-skill').removeClass('d-none');
        $('#add-skill').removeClass('d-none');
        $('#preview-skill').html('');
    }

    function addSkill() {
        $('#form_skill')[0].reset();
        $('[name="bidang"]').val('').trigger('change');
        $('[name="level"]').val('').trigger('change');
        $('#title-skill').text('Tambah Skill');
        $('#form-skill').removeClass('d-none');
        $('#add-skill').addClass('d-none');
        $('#tabel-skill').addClass('d-none');
        $('#div-total-skill').addClass('d-none');
        $('#nav-skill').addClass('d-none');
        $('[name="simpan_skill"]').val('1');
        $('#ganti-berkas-skill').addClass('d-none');
        $('#new_upload_skill').val('1');
        $('#edit-berkas-skill').html('');
        $('#new-upload-skill').removeClass('d-none');
        $('.file-info-skill').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-skill').html('');
    }

    function editSkill(id) {
        $('#form_skill')[0].reset();
        $('[name="bidang"]').val('').trigger('change');
        $('[name="level"]').val('').trigger('change');
        $('.file-info-skill').addClass('text-muted').html('Tidak ada berkas yang dipilih');
        $('#preview-skill').html('');
        $.ajax({
            url: site_url + 'getSkill',
            type: "POST",
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                var tipe = data.url_sertifikat.split(/[#?]/)[0].split('.').pop().trim();
                var url_sertifikat = data.url_sertifikat;
                if (tipe == 'pdf') {
                    $('#ganti-berkas-skill').removeClass('d-none');
                    $('#new_upload_skill').val('1');
                    $('#edit-berkas-skill').html('<embed src="' + url_sertifikat + '" class="mt-4" height="800" width="100%"/>');
                    $('#new-upload-skill').addClass('d-none');
                } else {
                    $('#ganti-berkas-skill').removeClass('d-none');
                    $('#new_upload_skill').val('1');
                    $('#edit-berkas-skill').html('<img src="' + url_sertifikat + '" class="mt-4" alt="your image" style="width:250"/>');
                    $('#new-upload-skill').addClass('d-none');
                }

                $('#title-skill').text('Edit Skill');
                $('#form-skill').removeClass('d-none');
                $('#tabel-skill').addClass('d-none');
                $('#div-total-skill').addClass('d-none');
                $('#nav-skill').addClass('d-none');
                $('#add-skill').addClass('d-none');

                $('[name="simpan_skill"]').val('0');
                $('[name="id_skill"]').val(data.id);
                $('[name="nama_skill"]').val(data.nama_skill);
                $('[name="bidang"]').val(data.bidang).trigger('change');
                $('[name="level"]').val(data.level).trigger('change');
            },
            error: function(xx) {
                swal("Galat", "Hubungi admin", "error");
            }
        });
    }

    function deleteSkill(id) {
        swal({
            title: "Ingin Menghapus Data?",
            text: "Data tidak bisa dikembalikan",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Ya',
            confirmButtonColor: '#396113',
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }).then(function(result) {
            if (result.value) {
                hapusSkill(id);
            }
        }).catch(function(reason) {
            // swal("Gagal Klik.....", "Gagal Pilih", 'error');
        });
    }

    $('[name="kode_fakultas"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + '/getprodi',
            type: "GET",
            data: {
                kode_prodi: val
            },
            dataType: "JSON",
            success: function(data) {
                buatProdi(data);
                $('[name="kode_prodi"]').select2({
                    placeholder: '- Pilih Program Studi/Jurusan -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatProdi(data) {
        let optionLoop = '<option value>Pilih Program Studi/Jurusan</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        if ($('[name="kode_prodi"]')) {
            $('[name="kode_prodi"]').children().remove();
        }
        $('[name="kode_prodi"]').append(optionLoop);
    }
</script>