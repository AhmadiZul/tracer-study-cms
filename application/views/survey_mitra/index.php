<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            <h3>Survey Mitra</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-3 col-xs-12">
                    <img class="img-fluid" src="<?php echo base_url() ?>public/assets/img/admin/rekrutmen.png" alt="">
                </div>
                <div class="col-lg-9 col-xs-12">
                    <h2>Segera lakukan pengisian survey mitra</h2>
                    <a href="<?php echo base_url() ?>tracer_mitra/index" class="btn btn-primary">Isi survey</a>
                </div>
            </div>
        </div>
    </div>
</div>