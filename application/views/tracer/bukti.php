<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <title>Bukti telah mengisi kuesioner dengan NIRM <?= $dataAlumni->nim ?></title>
    <style>
        @page {
            margin: 0;
        }

        body {
            margin-top: 1cm;
            margin-left: 1cm;
            margin-right: 1cm;
            /* margin-bottom: 1cm; */
            font-family: Arial, sans-serif;
        }

        table {
            width: 100%;
            /* border-collapse:inherit; */
            border-spacing: 0;
        }

        .logo {
            text-align: left !important;
            width: 1%;
            white-space: nowrap;
            margin-top: 10px;

        }

        .border-bottom {
            margin-top: 10px;
            margin-bottom: 10px;
            padding: 5px 0;
            width: 100%;
            border-bottom: 2px solid #000;
            display: inline-block;
        }

        .border-bottom:before {
            content: "";
            display: block;
            left: 0;
            bottom: 2px;
            width: 100%;
            height: 4px;
            background: #000;
        }

        .header {
            text-align: center;
            font-weight: bold;
            color: #000;
            white-space: pre-line;
            vertical-align: top;
            font-size: 18px;
        }


        #nama_perguruan {
            white-space: pre-line;
            font-size: 18pt;
            padding-left: 15px;
        }

        /* .header td {
            vertical-align: top;
        }
        .header td h3{
            padding-bottom: 20px;
        } */

        .address {
            font-size: 14px;
            font-weight: normal;
        }

        .body .header td {
            font-size: 16px;
            border: none;
            text-transform: uppercase;
            line-height: 1.2;
            padding-bottom: 50px;
        }

        .content td {
            text-align: left;
            vertical-align: top;
            padding: 0 5px 5px 5px;
        }

        .content th {
            vertical-align: top;
            white-space: nowrap;
            font-weight: bold;
            padding: 0 5px 0 0;
        }

        .faux-borders {
            padding: 1px 25%;
        }

        .signature {
            border-bottom: 2px dotted #000;
        }

        .photo {
            width: 200px;
            height: 300px;
        }
    </style>
</head>

<body>
    <table border="0" class="header">
        <tr>
            <td class="logo"><img src=<?= URL_FILE.$perguruan->path_logo ?> style="width: 115px;"></td>
            <td>
                <a id="nama_perguruan"><?= $perguruan->nama_resmi ?>
                    <br ><?= $data->nama_fakultas ?> <br>Program Studi <?= $data->nama_prodi ?></a>
                <div class="address">
                    <?= $perguruan->alamat ?><br>
                    Telepon : <?= $perguruan->no_telp ?> ||
                    Fax:<?= $perguruan->no_fax ?><br>
                    SITUS :<?= $perguruan->website ?><br>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="border-bottom"></div>
            </td>
        </tr>

    </table>
    <table class="body" border="0">
        <tr class="header">
            <td>
                KARTU BUKTI PENGISIAN TRACER STUDY<br>
            </td>
        </tr>
        <tr class="content">
            <td>
                <table border="0">
                    <tr>
                        <th width="10" align="left">Jenis Tracer</th>
                        <th width="1">:</th>
                        <td width="400"><?= $dataSurvey['jenis_survey'] ?></td>
                    </tr>
                    <tr>
                        <th width="10" align="left">NIM</th>
                        <th width="1">:</th>
                        <td width="400"><?= $dataAlumni->nim ?></td>
                    </tr>
                    <tr>
                        <th align="left">Nama</th>
                        <th>:</th>
                        <td><?= $dataAlumni->nama ?></td>
                    </tr>
                    <tr>
                        <th align="left">Jenis Kelamin</th>
                        <th>:</th>
                        <td><?= $dataAlumni->jenis_kelamin ?></td>
                    </tr>
                    <tr>
                        <th align="left">Prodi</th>
                        <th>:</th>
                        <td><?= $data->nama_prodi ?></td>
                    </tr>
                    <tr>
                        <th align="left">Tahun Lulus</th>
                        <th>:</th>
                        <td><?= $dataAlumni->tahun_lulus ?></td>
                    </tr>
                    <tr>
                        <th align="left">Waktu pengisian</th>
                        <th>:</th>
                        <td><?= $dataAlumni->inserted_time ?></td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>
    <br><br><br>
    <table border="0">
        <tr>
            <td width="50%" valign="top" align="right">
                <img src=<?= URL_FILE.$dataAlumni->url_photo ?> style="width: 115px;">
            </td>
        </tr>
    </table>
    <sub style="margin-top:100px;">Waktu Cetak: <?= date('d-m-Y H:i:s') ?></sub>
</body>

</html>