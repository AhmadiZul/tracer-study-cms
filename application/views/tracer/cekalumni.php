<section id="hero-cek" class="d-flex justify-content-center align-items-start">
    <div class="bg">
        <div class="container carousel carousel-fade" style="height: 400px;">

            <!-- Slide 1 -->
            <div class="carousel-item active">
                <div class="carousel-container">
                    <div class="row justify-content-center mb-3">
                        <img class="img-fluid" src="<?php echo base_url() ?>public/assets/img/sidebar/logo_white.png" alt="" style="">
                    </div>
                    <div class="col">
                        <p>Diisi oleh alumni yang baru lulus dari perguruan tinggi kurang dari 1 tahun setelah wisuda.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="wrapper"></div>
<link rel="stylesheet" href="<?= base_url('public/vendor/datatables/jquery.dataTables.min.css') ?>">
<main id="main">
    <section class="kuesioner">
        <div class="container" data-aos="fade-up">
            <div class="card">
                <div class="card-header">
                    <a href="<?php echo base_url("tracer?id=$id_jenis") ?>" class="btn btn-login"> Kembali</a>
                    <strong> Identitas diri</strong>
                </div>
                <div class="card-body">
                    <form id="form_kuisi" method="post">
                        <div class="row">
                            <input type="hidden" class="form-control" name="tahun" value="<?php echo $alumni->untuk_tahun_lulus; ?>">
                            <input type="hidden" class="form-control" name="prodi" value="<?php echo $alumni->untuk_id_prodi; ?>">
                            <input type="hidden" class="form-control" name="fakultas" value="<?php echo $alumni->untuk_id_fakultas; ?>">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="states">
                                        <label for="nim">Nomor Induk Mahasiswa/Siswa (NIM/NIS) <b class="text-danger">*</b></label>
                                        <input class="form-control" name="nim" id="nim" type="text">
                                        <div class="invalid-feedback" for="nim"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="states">
                                        <label for="id_prodi">Nomor Induk Kependudukan (NIK) <b class="text-danger">*</b></label>
                                        <input class="form-control" name="nik" id="nik" type="text">
                                        <div class="invalid-feedback" for="nik"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="states">
                                        <button class="btn btn-login btn-block" type="submit" id="btn-input-cek">Selanjutnya</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>

<script src="<?= base_url('public/vendor/datatables/datatables.min.js'); ?>"></script>

<script>
    var site_url = '<?php echo site_url() ?>tracer/index/';
    $('#form_kuisi').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: site_url + "inputCek",
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                if (data.success) {
                    swal({
                        title: 'Berhasil',
                        text: 'Anda bisa masuk halaman berikutnya',
                        type: 'success',
                        confirmButtonColor: '#396113',
                    }, ).then(function() {
                        console.log(data);
                        window.location.href = site_url + 'kuesioner?id_alumni=' + data.id_alumni + '&s_id=' + '<?= $id_s ?>';
                    });
                } else {
                    if (typeof data.text !== "undefined") {
                        swal({
                            title: 'Ups...',
                            html: data.text,
                            type: 'warning',
                            confirmButtonColor: '#396113',
                        }, );
                    } else {
                        swal({
                            title: 'Ups...',
                            text: 'Data Gagal Disimpan',
                            type: 'error',
                            confirmButtonColor: '#396113',
                        }, );
                    }
                }
            },
            error: function(error) {
                dismiss_loading();
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                }, );
            }
        });
    });
</script>