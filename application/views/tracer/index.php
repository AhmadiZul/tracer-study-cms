<!-- ======= Hero Section ======= -->
<section id="hero-cek" class="d-flex justify-content-center align-items-start">
    <div class="bg">
        <div class="container carousel carousel-fade" style="height: 400px;">

            <!-- Slide 1 -->
            <div class="carousel-item active">
                <div class="carousel-container">

                    <h2 class="animate__animated animate__fadeInDown"><?= $jenis_survey ?></h2>
                    <div class="d-flex justify-content-center align-items-start" style="height:200px;">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <form action="" class="form-inline">
                                            <div class="col-md-4">
                                                <label for="filter_tahun">Tahun Lulusan</label>
                                                <select name="filter_tahun" id="jadwalSurvei__filterTahunLulus" class="form-control select2">
                                                    <option value="">Semua Tahun Kelulusan</option>
                                                    <?php foreach ($tahun as $key => $value) { ?>
                                                        <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="filter_fakultas">Pilih Fakultas</label>
                                                <select name="filter_fakultas" id="jadwalSurvei__filterPerguruanTinggi" class="form-control select2">
                                                    <option value="">Fakultas</option>
                                                    <?php foreach ($fakultas as $key => $value) { ?>
                                                        <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="filter_prodi">Pilih Prodi</label>
                                                <select name="prodi" id="prodi" class="form-control select2">
                                                    <option value="">Program Studi</option>
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End Hero -->

<section id="jadwal-tracer" class="jadwal">
    <div class="row" id="tabel_jadwal">

    </div>
</section>

<script>
    var site_url = '<?php echo site_url() ?>tracer/index/';
    let current_page = 1;
    let records_per_page = 9;

    let jadwalSurvei__filterPerguruanTinggi = '';
    let jadwalSurvei__filterProdi = '';
    let jadwalSurvei__filterTahunLulus = '';

    let objJson = JSON.parse(`<?php echo isset($jadwal) ? json_encode($jadwal) : NULL; ?>`);
    //console.log(objJson);

    function prevPage() {
        if (current_page > 1) {
            current_page--;
            changePage(current_page);
        }
    }

    function nextPage() {
        if (current_page < numPages()) {
            current_page++;
            changePage(current_page);
        }
    }

    function changePage(page) {
        // array objJson baru
        let tempObjJson = objJson.map(function(obj) {
            return obj;
        });

        // FILTER PERGURUAN TINGGI
        if (jadwalSurvei__filterPerguruanTinggi != '') {
            tempObjJson = tempObjJson.filter(function(obj) {
                return obj['untuk_id_fakultas'] == this.data;
            }, {
                data: jadwalSurvei__filterPerguruanTinggi
            });
        }
        // FILTER PRODI
        if (jadwalSurvei__filterProdi != '') {
            tempObjJson = tempObjJson.filter(function(obj) {
                return obj['untuk_id_prodi'] == this.data;
            }, {
                data: jadwalSurvei__filterProdi
            });
        };

        // FILTER TAHUN LULUS
        if (jadwalSurvei__filterTahunLulus != '') {
            tempObjJson = tempObjJson.filter(function(obj) {
                return obj['untuk_tahun_lulus'] == this.data;
            }, {
                data: jadwalSurvei__filterTahunLulus
            });
        };

        // Validate page
        if (page < 1) page = 1;
        if (page > numPages(tempObjJson)) page = numPages(tempObjJson);

        $('#tabel_jadwal').html('');

        if (tempObjJson.length <= 0) {
            $('#btn_prev, #btn_next').css('visibility', 'hidden');
            $('#page_span').html('0/0');
            return;
        }

        // MEMBUAT CARD HTML
        let tempAllCard = '';
        for (var i = (page - 1) * records_per_page; i < (page * records_per_page) && i < tempObjJson.length; i++) {
            tempAllCard += renderCard(tempObjJson[i]);
        };

        $('#tabel_jadwal').html(tempAllCard);
        $('#page_span').html(page + '/' + numPages(tempObjJson));

        if (page == 1) {
            $('#btn_prev').css('visibility', 'hidden');
        } else {
            $('#btn_prev').css('visibility', 'visible');
        };

        if (page == numPages(tempObjJson)) {
            $('#btn_next').css('visibility', 'hidden');
        } else {
            $('#btn_next').css('visibility', 'visible');
        };
    };

    function numPages(obj = null) {
        if (obj === null) {
            return Math.ceil(objJson.length / records_per_page);
        };
        return Math.ceil(obj.length / records_per_page);
    }

    $(document).ready(function() {
        if (objJson) {
            changePage(1);
            $('#btn_prev').on('click', prevPage);
            $('#btn_next').on('click', nextPage);

            $('#jadwalSurvei__filterPerguruanTinggi').on('change', function() {
                jadwalSurvei__filterPerguruanTinggi = $(this).val();
                $.ajax({
                    url: site_url + 'getProdi',
                    type: "POST",
                    data: {
                        kode_prodi: jadwalSurvei__filterPerguruanTinggi
                    },
                    dataType: "JSON",
                    success: function(data) {
                        if (jadwalSurvei__filterPerguruanTinggi == "") {
                            $('[name="prodi"]').children().remove();
                            jadwalSurvei__filterProdi = "";
                        } else {
                            buatProdi(data);
                        }

                        $('[name="prodi"]').select2({
                            placeholder: '- Pilih Prodi/Jurusan -',
                        });
                        changePage(1);
                    },
                    error: function(xx) {
                        alert(xx)
                    }
                });
            });
            $('#jadwalSurvei__filterProdi').on('change', function() {
                jadwalSurvei__filterProdi = $(this).val();
                changePage(1);
            });
            $('#jadwalSurvei__filterTahunLulus').on('change', function() {
                jadwalSurvei__filterTahunLulus = $(this).val();
                changePage(1);
            });
        };
    });

    function renderCard(obj) {
        let jenis_survey = obj['jenis_survey'] ? obj['jenis_survey'] : 'Survei Alumni';
        let nama_resmi = obj['nama_resmi'] ? obj['nama_resmi'] : '';
        let nama_prodi = obj['nama_prodi'] ? obj['nama_prodi'] : '';
        let nama_fakultas = obj['nama_fakultas'] ? obj['nama_fakultas'] : '';
        let angkatan_tahun = obj['untuk_tahun_lulus'] ? obj['untuk_tahun_lulus'] : '';
        let survei_mulai = obj['start_date'] ? obj['start_date'] : '';
        let survei_selesai = obj['end_date'] ? obj['end_date'] : '';
        let waktu_acara = obj['waktu_acara'] ? obj['waktu_acara'] : '';
        let waktu_berakhir = obj['waktu_berakhir'] ? obj['waktu_berakhir'] : '';
        let url = site_url + 'cekalumni?id=' + obj['id_survey'];

        let template = `
            <div class="col-sm-6 col-md-4 col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title text-center">
                            <h3>${jenis_survey}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <span class="badge badge-pill badge-primary">${angkatan_tahun}</span>
                        <span class="badge badge-pill badge-warning">${nama_fakultas}</span>
                        <span class="badge badge-pill badge-success">${nama_prodi}</span>
                        <br>
                        <label><strong>Periode Pengisian Survey : </strong><div class="row pt-3"><div class="col">${survei_mulai} <div class="text-danger">${waktu_acara} </div></div> - <div class="col">${survei_selesai} <div class="text-danger"> ${waktu_berakhir} </div></div></div></label>
                        <hr>
                        <a href="${url}" class="btn btn-login mt-4 center"><b>Isi Kuesioner <i class="fas fa-arrow-alt-circle-right"></i></b></a>
                    </div>
                </div>
            </div>
        `;
        return template;
    }

    function buatProdi(data) {
        let optionLoop = '<option value>Pilih Prodi/Jurusan</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        if ($('[name="prodi"]')) {
            $('[name="prodi"]').children().remove();
        }
        $('[name="prodi"]').append(optionLoop);
    }
</script>