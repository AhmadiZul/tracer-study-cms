<div class="wrapper"></div>
<link rel="stylesheet" href="<?=base_url('public/vendor/datatables/jquery.dataTables.min.css')?>">
<main id="main">
    <section class="kuesioner">
        <div class="container" data-aos="fade-up">
            <div class="text-center">
                <?php
                    $urlLogo = base_url().'public/assets/img/logo-kosong.webp';
                    
                    if (!empty($jadwal['path_logo'])) {
                        $urlLogo = URL_LOGO_UNIV.$jadwal['path_logo'];
                    }
                ?>
                <img src="<?php echo $urlLogo ?>" style="width:200px" class="img-fluid">
                <div class="row mt-5">
                    <h2 class="text-center"><?= (!empty($jadwal['jenis_survey']) ? $jadwal['jenis_survey'] : '') ?></h2>
                </div>
            </div>
            <div class="row mt-2">
                <p align="left" class="lead"><?= (!empty($jadwal['jenis_survey']) ? $jadwal['jenis_survey'] : '') ?></p>
            </div>
            <div class="row mt-2">
                <p align="left" class="lead">Data daftar alumni <?= (!empty($jadwal['nama_resmi']) ? $jadwal['nama_resmi'] : '') ?>.<br>Infokan link ini kepada sesama alumni yang mungkin belum mengisi tracer study</p>
            </div>
            <form id="form_kuis" name="form_kuis" method="post" role="form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="states">
                                <label for="id_prodi">Periode</label>
                                <br>
                                <input class="form-control d-inline-block w-auto" readonly value="<?php echo $this->tanggalindo->konversi($jadwal['start_date']); ?>">
                                -
                                <input class="form-control d-inline-block w-auto" readonly value="<?php echo $this->tanggalindo->konversi($jadwal['end_date']); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="thn_angkatan">Tahun Lulus</label>
                            <input class="form-control" readonly value="<?php echo $jadwal['untuk_tahun_lulus']; ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="states">
                                <label for="id_prodi">Prodi</label>
                                <input class="form-control" readonly value="<?php echo $jadwal['nama_prodi']; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div id="result" class="row mt-3 d-none">
                <div class="table-responsive">
                    <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                        <div class="row mt-5">
                            <div class="col-sm-12">
                                <table id="example" class="table table-striped table-bordered dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIM</th>
                                            <th>Nama</th>
                                            <th>Prodi</th>
                                            <th>Link</th>
                                        </tr>
                                    </thead>
                                    <tbody id="resultBody">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

        </div>
    </section>
</main>

<script src="<?=base_url('public/vendor/datatables/datatables.min.js'); ?>"></script>
<script type="text/javascript">
    var site_url = '<?php echo site_url() ?>tracer/index/';
    let id_survey = "<?=$jadwal['id_survey']?>";
    let jenis_survey = "<?=$jadwal['id_ref_jenis_survey']?>";
    let untuk_id_prodi = '<?=$jadwal['untuk_id_prodi']; ?>';
    let tahun_lulus = '<?=$jadwal['untuk_tahun_lulus']; ?>';

    function loadTabelALumni() {
        $('.loader').removeClass('d-none');
        $('#result').addClass('d-none');
        $.ajax({
            url: site_url + "getDaftarAlumni",
            dataType: 'json',
            type: 'POST',
            data: {
                id_survey : id_survey,
                id_prodi: untuk_id_prodi,
                tahun_lulus: tahun_lulus,
            },
            success: function(data) {
                let tbody = '';
                console.log(data)
                if (data['status']) {
                    let iteration = 0;
                    data['isi'].forEach(function(isi) {
                        iteration += 1;
                        link = '<h5><a href="'+ site_url + 'kuesioner?id_alumni=' + isi['id_alumni'] + '&s_id='+id_survey+'">Link kuesioner</a></h5>';
                        if (isi['id_answer'] != null) {
                            link = '<h5><a class="text-info" href="'+ site_url + 'cetakBukti?kode=' + isi['id_alumni'] + '&s_id='+id_survey+'">Cetak bukti</a></h5>';
                        }
                        if (isi['status_data'] != 1) {
                            link = '<h5 class="text-danger">Belum tervalidasi</h5>';
                        }
                        tbody += availableData(iteration, isi['nim'], isi['nama'], isi['nama_prodi'], link);
                    });

                    $('#totalRow').text(data['jumlah']);
                } else if (!data['status']) {
                    tbody = `
                            <tr class="odd">
                            <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                            </tr>`;
                } else {
                    tbody = `
                            <tr class="odd">
                                <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                                </tr>`;
                }

                $('.loader').addClass('d-none');
                $('#result').removeClass('d-none');

                $('#resultBody').html(null);
                $('#resultBody').append(tbody);

                $('#example').DataTable({
                    pageLength: 30,
                    lengthMenu: [
                        30,
                        60,
                        90,
                    ],
                });
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    };

    function availableData(no, nim, nama, prodi, link) {
        return `
            <tr>
                <td>${no}</td>
                <td>${nim}</td>
                <td>${nama}</td>
                <td>${prodi}</td>
                <td>${link}</td>
            </tr>`;
    }
    $(document).ready(function(e) {
        loadTabelALumni();
    });
</script>