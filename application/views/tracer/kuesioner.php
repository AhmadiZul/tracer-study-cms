<section id="hero-jadwal" class="d-flex justify-content-center align-items-start">
    <div class="bg">
        <div class="container carousel carousel-fade">

            <!-- Slide 1 -->
            <div class="carousel-item active">
                <div class="carousel-container">
                    <div class="row justify-content-center mb-3">
                        <img class="img-fluid" src="<?php echo base_url() ?>public/assets/img/sidebar/logo_white.png" alt="" style="">
                    </div>
                    <div class="col">
                        <p>Diisi oleh alumni yang baru lulus dari perguruan tinggi kurang dari 1 tahun setelah wisuda.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="wrapper"></div>
<main id="main">
    <section class="kuesioner">
        <div class="container">
            <div class="modal fade" id="modalKuesioner" tabindex="-1" role="dialog" aria-hidden="true">>
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Yth Saudara <b><?= $alumni->nama ?></b></h3>
                            <button type="button" class="close" onclick="modal_hide()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col">
                                    <p>Kami saat ini sedang terus melengkapi data alumninya, termasuk melalui Studi Penelusuran/Pelacakan alumni (Tracer Study).</p>
                                    <p>
                                        Kuesioner Tracer Study ini diprogramkan oleh KEMENDIKBUDRISTEK dan digunakan untuk menjadi alat evaluasi kinerja perguruan tinggi serta syarat kelengkapan akreditasi institusi dan program studi. Partisipasi alumni dalam mengisi tracer study juga menjadi indikator perankingan atau klasterisasi perguruan tinggi yang setiap tahun dilakukan oleh DIKTI.
                                    </p>
                                    <p>
                                        Berkaitan dengan hal tersebut, kami mohon kesediaan para alumni yang terhormat untuk bekerjasama mengisi kuesioner Tracer Study ini. Peran serta Saudara dalam mengisi kuesioner ini akan memberi manfaat kepada untuk dapat melakukan evaluasi, inovasi dan perbaikan pelayanan akademik serta dapat memberikan kontribusi besar untuk masyarakat Indonesia dan internasional.
                                    </p>
                                    <p>
                                        Atas nama Rektor, kami mengucapkan terima kasih yang sebesar-besarnya atas kesediaan Saudara meluangkan waktu sejenak untuk berpartisipasi dalam pengisian kuesioner Tracer Study ini.
                                    </p>
                                    <b>Team tracer study</b>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col text-center">
                                <button type="button" class="btn btn-login " id="btn" onclick="modal_hide()">Mengerti</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalSimpan" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title text-danger"><b>Kuesioner Berhasil Diisi</b></h3>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col">
                                    <p>Pengisian Tracer Study berhasil masuk ke dalam sistem perguruan tinggi. Terima kasih atas partisipasi alumni dalam melakukkan pengisian Kuesioner, data yang diisikan akan sangat berguna bagi pihak kampus maupun alumni.</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="col text-center">
                                <a class="btn btn-login btn-block" href="<?= base_url() ?>">Kembali Ke Beranda</a>
                                <a class="btn btn-block text-danger border border-danger" href="<?= base_url() . 'tracer/index/cetakBukti?kode=' . $id_alumni . '&s_id=' . $jadwal['id_survey'] ?>"><b>Kartu Bukti Pengisiaan Tracer Study</b></a>
                            </div>
                            \
                        </div>
                    </div>
                </div>
            </div>
            <form id="form-kuesioner" class="needs-validation" novalidate>
                <!-- form data diri -->

                <div class="row shadow p-3 mb-5 bg-white rounded" id="biodata">
                    <input type="text" value="<?php echo $alumni->id_alumni ?>" name="id_alumni" class="form-control d-none">
                    <input type="text" value="<?php echo $s_id ?>" name="id_survey" class="form-control d-none">
                    <div class="invalid-feedback" for="id_alumni"></div>
                    <div class="form-group col-2 text-center" id="foto">
                        <img src="<?php echo URL_FILE . $alumni->url_photo ?>" class="rounded-circle">
                        <label for="nama " id="nama"><b><?php echo $alumni->nama ?></b></label>
                        <label for="prodi" id="nama"><b><?php echo $alumni->nama_prodi ?></b></label>
                    </div>
                    <div class="form-group col-4">
                        <label for="tahun_lulus">Tahun Lulus</label>
                        <input type="text" value="<?php echo $alumni->tahun_lulus ?>" name="tahun_lulus" class="form-control" disabled>
                        <label for="prodi">Program Studi/Jurusan</label>
                        <input type="text" value="<?php echo $alumni->nama_prodi ?>" name="prodi" class="form-control" disabled>
                    </div>
                    <div class="form-group col-4">
                        <label for="nim">Nomor Induk Mahasiswa/Siswa (NIM/NIS)</label>
                        <input type="text" value="<?php echo $alumni->nim ?>" name="nim" class="form-control" disabled>
                        <label for="jenis_kelamin">Jenis Kelamin</label><br>
                        <input type="text" value="<?php echo ($alumni->jenis_kelamin == "L" ? "LAKI-LAKI" : "PEREMPUAN") ?>" name="jenis_kelamin" class="form-control" disabled>
                    </div>
                </div>
                <!-- form kuesioner -->
                <div id="question_category" class="row mt-5">
                    <div class="col-md-6">
                        <sup class="text-danger"><?= $pivotKuesioner['question_code'] ?></sup>
                        <b class="question"><?= $pivotKuesioner['question'] ?></b>
                        <input type="hidden" class="form-control" name="question_id-0" value="<?= $pivotKuesioner['id_kuesioner'] ?>">
                    </div>
                    <div class="form-group col-md-6">
                        <?php foreach ($answer as $key => $value) { ?>
                            <div class="custom-control custom-radio my-2">
                                <input type="radio" class="custom-control-input" name="inpt-0" id="<?php echo $value['id_opsi'] ?>" value="<?php echo $value['answer_code'] ?>">
                                <label class="custom-control-label" for="<?php echo $value['id_opsi'] ?>"><?php echo $value['opsi'] ?></label>
                            </div>
                        <?php } ?>
                        <div class="invalid-feedback" for="inpt-0"></div>
                    </div>
                    <hr>
                </div>
                <div id="question" class="row">

                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-login form-control">Simpan</button>
                </div>
            </form>
            <?php if (isset($cek)) { ?>
                <input type="hidden" name="cek" value="<?= $cek ?>">
            <?php } ?>
        </div>
    </section>
</main>

<script>
    $(document).ready(function() {
        var cek = $('[name=cek]').val()
        if (cek) {
            dismiss_loading();
            $('#modalSimpan').modal({
                backdrop: 'static',
                keyboard: false
            }, 'show');
        } else {
            dismiss_loading();
            $('#modalKuesioner').modal({
                backdrop: 'static',
                keyboard: false
            }, 'show');
        }

    });

    function modal_hide() {

        dismiss_loading();
        $("#modalKuesioner").modal("hide");
    }
</script>
<script>
    var site_url = '<?php echo site_url() ?>tracer/index/';
    let status = "<?php echo $alumni->id_ref_status_alumni ?>";

    // jika tombol radio button di question category berubah
    $('input[name=inpt-0]').on('change', function(e) {
        $.ajax({
            url: site_url + "getAllKuesionerByCategory",
            data: {
                id_status_alumni: e.target.value
            },
            dataType: 'json',
            type: 'POST',
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                $('#question').html(data.question);
                $('.select2').select2();
            },
            error: function(error) {
                dismiss_loading();
                swal({
                    title: 'Galat',
                    text: 'Hubungi admin',
                    type: 'error',
                    confirmButtonColor: '#396113',
                }, );
            }
        });
    });

    // simpan survey
    $('#form-kuesioner').on('submit', function(e) {
        e.preventDefault();
        $('#form-kuesioner').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + "simpan_survey",
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                $("#form-kuesioner").html(data);
                $('#modalSimpan').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');

            },
            error: function(jqXHR, textStatus, errorThrown) {
                dismiss_loading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                field_isian,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');

                                //remove [] from field
                                field = field.replace('[]', '');
                                if (message != null) {
                                    $(`.invalid-feedback[for="${field}"]`).html(message);
                                } else {
                                    $(`.invalid-feedback[for="${field}"]`).removeClass('d-none');
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }

                                if (typeof field_isian != "undefined") {
                                    $(`[name="${field_isian}"]`).addClass('is-invalid');
                                    $(`.invalid-feedback[for="${field}"]`).removeClass('d-none');
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal({
                            title: response.message.title,
                            html: response.message.body,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    });


    jQuery(document).on('change', "input[type='radio'], input[type='checkbox']", function(e) {
        $('[name="' + e.target.name + '"]').removeClass('is-invalid');
        $('[name="' + e.target.name + '"]').addClass('is-valid');
        $('[name="' + e.target.name + '"]').parent().removeClass('is-invalid');
        $('.invalid-feedback[for="' + e.target.name + '"]').addClass('d-none');
        $('.invalid-feedback[for="' + e.target.name + '"]').removeClass('d-block');
    });

    /**
     * input type number-only
     */

    jQuery(document).on('keyup', ".number-only", function(e) {
        if (/\D/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/\D/g, '');
        }
    });

    /**
     * input type alpha-only
     */

    jQuery(document).on('keydown', ".alpha-only", function(e) {
        if (e.altKey) {
            e.preventDefault();
        } else {
            var key = e.keyCode;
            if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                e.preventDefault();
            }
        }
    });

    /**
     * is-valid input type text
     */

    jQuery(document).on('keyup', "input[type='text'], textarea", function(e) {
        $('[name=' + e.target.name + ']').removeClass('is-invalid');
        $('[name=' + e.target.name + ']').addClass('is-valid');
        $('[name=' + e.target.name + ']').parent().removeClass('is-invalid');

        // pesan pilihan ganda isian
        if (/isian/.test(e.target.name)) {
            let name = e.target.id.replace('isian-', '')
            $('[name="' + name + '"]').removeClass('is-invalid');
            $('[name="' + name + '"]').addClass('is-valid');
            $('[name="' + name + '"]').parent().removeClass('is-invalid');
            $('.invalid-feedback[for="' + name + '"]').addClass('d-none');
            $('.invalid-feedback[for="' + name + '"]').removeClass('d-block');
        }

        // pesan checkbox lainnya
        if (/lainnya/.test(e.target.name)) {
            let name = e.target.id.replace('lainnya-', '')
            $('[name="' + name + '[]"]').removeClass('is-invalid');
            $('[name="' + name + '[]"]').addClass('is-valid');
            $('[name="' + name + '[]"]').parent().removeClass('is-invalid');
            $('.invalid-feedback[for="' + name + '"]').addClass('d-none');
            $('.invalid-feedback[for="' + name + '"]').removeClass('d-block');
        }
    });

    /**
     * select kabupaten/kota kuesioner
     */
    jQuery(document).on('change', '#kuesioner_kode_provinsi', function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'getKabKota',
            type: "GET",
            data: {
                kode_provinsi: val
            },
            dataType: "JSON",
            success: function(data) {
                buatKabKotKuesioner(data);
                $('#kuesioner_kode_kabupaten').select2({
                    placeholder: '- Pilih Kabupaten/Kota -',
                });
                $('.invalid-feedback[for="' + e.target.name + '"]').addClass('d-none');
                $('.invalid-feedback[for="' + e.target.name + '"]').removeClass('d-block');
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatKabKotKuesioner(data) {
        let optionLoop = '<option value>Pilih Kabupaten / Kota</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        $('#kuesioner_kode_kecamatan').children().remove();
        $('#kuesioner_kode_kecamatan').select2({
            placeholder: '- Pilih Kecamatan -',
        });
        if ($('#kuesioner_kode_kabupaten')) {
            $('#kuesioner_kode_kabupaten').children().remove();
        }
        $('#kuesioner_kode_kabupaten').append(optionLoop);
    }

    jQuery(document).on('change', '#kuesioner_kode_kabupaten', function(e) {
        $('.invalid-feedback[for="' + e.target.name + '"]').addClass('d-none');
        $('.invalid-feedback[for="' + e.target.name + '"]').removeClass('d-block');
    });

    jQuery(document).on("focus", ".datepicker2", function() {
        $('.datepicker2').datepicker({
            format: 'yyyy-mm-dd',
            language: 'id',
            daysOfWeekHighlighted: "0",
            autoclose: true,
            todayHighlight: true
        });
    });
</script>