<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Tracer-Study</title>
  <style>
    :root {
      --warna : <?php echo $warna_tema->deskripsi ?>;
      --warna-title : <?php echo $banner->warna_1 ?>;
      --warna-univ : <?php echo $banner->warna_2 ?>;
      --warna-footer : <?php echo $footer->warna_1 ?>;
      --footer-font : <?php echo $footer->warna_2 ?>;
      --warna-header : <?php echo $header->warna_1 ?>;
      --header-font : <?php echo $header->warna_2 ?>;
      --bg-video : <?php echo $video_perkenalan->url_file ?>;
    }
  </style>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo URL_FILE. $logo_title->url_file ?>" rel="icon">
  <link href="<?php echo URL_FILE. $logo_title->url_file ?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <!-- <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Rubik:wght@300&display=swap" rel="stylesheet"> -->
  <link href="<?php echo base_url() ?>public/assets/fonts/rubik/static/Rubik-Regular.ttf" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url() ?>public/vendor/animate.css/animate.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/aos/aos.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/boxicons/css/boxicons.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/glightbox/css/glightbox.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/remixicon/remixicon.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/swiper/swiper-bundle.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/owl.carousel.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/owl.theme.default.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/animate.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/select2/select2.min.css" rel="stylesheet" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/assets/loading_page.css" rel="stylesheet" onload="this.media='all'">


  <!-- Template Main CSS File -->
  <link href="<?php echo base_url() ?>public/assets/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/assets/css/custom-mitra.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/assets/css/custom.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Anyar - v4.7.1
  * Template URL: https://bootstrapmade.com/anyar-free-multipurpose-one-page-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
  <!-- ======= Top Bar ======= -->
  <!-- ======= Header ======= -->
  <div id="spinner-front">
    <img src="<?php echo base_url() ?>public/assets/ajax-loader.gif" /><br>
    Loading...
  </div>
  <div id="spinner-back"></div>
  <header id="header" class="fixed-top d-flex align-items-center ">
    <div class="mx-4 w-100 d-flex align-items-center justify-content-between">

      <h1 class="logo" style="margin-left: 5rem;"><a href="<?php echo base_url() ?>index.php"><img src="<?php echo URL_FILE. $logo_utama->url_file?>" alt=""></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href=index.html" class="logo"><img src="assets/img/logo.webp" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto <?php echo active_page('', 'active') ?>" href="<?php echo base_url() ?>">Beranda</a></li>
          <li><a class="nav-link scrollto <?php echo active_page('alumni', 'active') ?>" href="<?php echo base_url() . 'alumni' ?>">Tracer Study Alumni</a></li>
          <!-- <li class="dropdown"><a class="<?php echo active_page('mitra', 'active') ?><?php echo active_page('alumni', 'active') ?><?php echo active_page('tracer', 'active') ?><?php echo active_page('tracer_mitra', 'active') ?>" href="#"><span class="text-light">Tracer Study</span> <i class="bi bi-chevron-down"></i></a> -->
          <!-- <ul>
              <li><a class="nav-link scrollto <?php echo active_page('alumni', 'active') ?>" href="<?php echo base_url() . 'alumni' ?>">Tracer Study Alumni</a></li>
              <li><a class="nav-link scrollto <?php echo active_page('mitra', 'active') ?>" href="<?php echo base_url() . 'mitra' ?>">Tracer Study Mitra Karir</a></li>
            </ul> -->
          <!-- </li> -->
          <li><a class="nav-link scrollto" href="<?php echo base_url() . 'data_alumni' ?>">Alumni</a></li>
          <!-- <li class="dropdown"><a class="<?php echo active_page('data_alumni', 'active') ?><?php echo active_page('alumni_berprestasi', 'active') ?>" href="#"><span class="text-light">Alumni</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="<?php echo base_url() . 'data_alumni' ?>">Data Alumni</a></li>
              <li><a href="<?php echo base_url() . 'alumni_berprestasi' ?>">Alumni Berprestasi</a></li>
            </ul>
          </li> -->
          <!-- <li class="dropdown"><a class="<?php echo active_page('lowongan_kerja', 'active') ?><?php echo active_page('lowongan_magang', 'active') ?><?php echo active_page('layanan_keuangan', 'active') ?>" href="#"><span class="text-light">Informasi</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="<?php echo base_url() . 'lowongan_kerja' ?>">Lowongan Kerja</a></li>
              <li><a href="<?php echo base_url() . 'lowongan_magang' ?>">Informasi Magang</a></li>
              <li><a href="<?php echo base_url() . 'layanan_keuangan' ?>">Informasi Layanan Keuangan</a></li>
            </ul> -->
          <li><a class="nav-link scrollto <?php echo active_page('tips', 'active') ?>" href="<?php echo base_url() . 'tips_karir' ?>"><span>Tips Karir</span></a></li>
          <li><a class="nav-link scrollto" href="<?=$perguruan_tinggi->website?>"><span>Official Website</span></a></li>
          </li>
          <li>
            <?php if ($this->session->userdata("t_idGroup") == 3) { ?>
              <a href="<?php echo base_url() . 'mitra/dashboard' ?>" class="btn-login-1" role="button">DASHBOARD</a>
            <?php } else if ($this->session->userdata("t_idGroup") == 4) { ?>
              <a href="<?php echo base_url() . 'alumni/dashboard' ?>" class="btn-login-1" role="button">DASHBOARD</a>
            <?php } else { ?>
              <a href="<?php echo base_url() ?>Login" class="btn-login-1" role="button">Login</a>
            <?php } ?>
          </li>
          <li><a href="<?php echo base_url() ?>register/index" class="btn-login" role="button">Register</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->
  <script src="<?php echo base_url() ?>public/assets/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/select2/select2.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/sweetalert2/sweetalert2.all.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  {CONTENT}
  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="footer-top-bg">
        <div class="container-fluid footer-container">
          <div class="row ">
            <div class="col-lg-4 py-5 footer-info text-center">
              <img src="<?php echo URL_FILE. $logo_utama->url_file?>" width="relative" height="120" alt="">
            </div>
            <div class="col-lg-4 py-5 footer-info">
              <h4>About Us</h4>
              <p style="color: var(--footer-font);"><?php echo $footer->deskripsi?></p>
            </div>
            <div class="col-lg-4">
              <div class="row">
                <div class="col align-self-center footer-links py-5">
                  <ul>
                    <li><a href="<?php echo base_url() ?>">Beranda</a></li>
                    <li><a href="<?php echo base_url() ?>alumni">Tracer Study Alumni</a></li>
                    <li><a href="<?php echo base_url() ?>data_alumni">Alumni</a></li>
                    <li><a href="<?php echo base_url() ?>tips_karir">Tips Karir</a></li>
                    <li><a href="<?php echo base_url() ?>landing_branding">Branding</a></li>
                    <!-- <li><a href="<?php echo base_url() ?>mitra" class="text-light">Tracer Study Mitra Karir</a></li> -->
                    <!-- <li><a href="<?php echo base_url() ?>data_alumni" class="text-light">Data Alumni</a></li> -->
                  </ul>
                </div>
                <!-- <div class="col align-self-center footer-links  py-5">
                  <ul>
                    <li><a href="<?php echo base_url() ?>alumni_berprestasi" class="text-light">Alumni Berprestasi</a></li> 
                    <li><a href="<?php echo base_url() ?>lowongan_kerja" class="text-light">Lowongan Kerja</a></li>
                    <li><a href="<?php echo base_url() ?>lowongan_magang" class="text-light">Lowongan Magang</a></li>
                  </ul>
                </div> -->
                <div class="col align-self-center footer-links  py-5">
                  <h4>Official Account</h4>
                  <div class="social-links mt-3">
                    <?php 
                    // echo "<pre>";
                    // print_r($social_media);
                    // echo "</pre>";
                    // die();
                    foreach ($social_media as $key=>$value) {
                    //  if ($value){
                      if ($key == 'instagram') {?>
                        <a href="<?= $value?>" class="instagram"><i class="fa-brands fa-instagram" style="color: var(--footer-font);"></i></a>
                      <?php } if($key == 'twitter'){?>
                     <a href="<?= $value?>" class="twitter"><i class="fa-brands fa-twitter" style="color: var(--footer-font);"></i></a>
                      <?php } if($key == 'youtube'){?>
                        <a href="<?= $value?>" class="youtube"><i class="fa-brands fa-youtube" style="color: var(--footer-font);"></i></a>
                      <!-- <a href="<?= $value->linkedin?>" class="linkedin"><i class="bx bxl-linkedin text-light"></i></a> -->
                    <!-- <a href="" class="instagram"><i class="bx bxl-instagram-alt text-light"></i></a> -->
                    <!-- <a href="https://www.youtube.com/c/PusdiktanBPPSDMPKementan" class="google-plus"><i class="bx bxl-youtube text-light"></i></a> -->
                    <!-- <a href='https://bachelorthesiswritingservice.com/' class="d-none">Author Bachelor's Thesis</a>
                    <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=4c8277cc0e6f5e34754cc9629be085f689cce819'></script>
                    <script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/968524/t/0"></script> -->
                    <?php }}?>
                  </div>
                </div>

              </div>
            </div>
          </div>

          </div>
          <div class="copyright">
            <?php echo $footer->sub_title ?> - build by<strong><span> Arkatama</span></strong>.
          </div>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url() ?>public/vendor/aos/aos.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/owl-carousel/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/jquery-mask/jquery.mask.min.js"></script>


  <!-- Template Main JS File -->
  <script src="<?php echo base_url() ?>public/assets/js/main.js"></script>
  <script>
    $(document).ready(function() {
      $(".slider-why-us").owlCarousel({
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        items: 5,
        margin: 0,
        loop: true,
        smartSpeed: 550,
        autoWidth: true,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true
      });
      $(".my-next-button").click(function() {
        $(".slider-why-us").trigger('next.owl.carousel');
      });

      $(".my-previous-button").click(function() {
        $(".slider-why-us").trigger('prev.owl.carousel');
        console.log('apa?');
      });

      $('.select2').select2();
      $('#testimonials-list').owlCarousel({
		    loop: true,
		    center: true,
		    items: 3,
		    margin: 30,
		    autoplay: true,
		    dots:true,
		    autoplayTimeout: 8500,
		    smartSpeed: 450,
		    responsive: {
		      0: {
		        items: 1
		      },
		      768: {
		        items: 2
		      },
		      1170: {
		        items: 3
		      }
		     }
		  });
      $(".slider-mitra-kami").owlCarousel({
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        items: 7,
        margin: 30,
        loop: true,
        smartSpeed: 550,
        autoWidth: true,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true
      });
    });
    /**
     * only number
     */
    $('.number-only').keyup(function(e) {
      if (/\D/g.test(this.value)) {
        // Filter non-digits from input value.
        this.value = this.value.replace(/\D/g, '');
      }
    });

    /**
     * only alpha
     */
    $('.alpha-only').bind('keydown', function(e) {
      if (e.altKey) {
        e.preventDefault();
      } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
        }
      }
    });
    /**
     * Loading
     */
    function loading() {
      document.getElementById("spinner-front").classList.add("show");
      document.getElementById("spinner-back").classList.add("show");
    }

    function dismiss_loading() {
      document.getElementById("spinner-front").classList.remove("show");
      document.getElementById("spinner-back").classList.remove("show");
    }
  </script>

  <?php
  echo '<script>';
  if (isset($scripts) && is_array($scripts)) {
    foreach ($scripts as $script) {
      $this->load->view($script);
    }
  }
  echo '</script>';
  ?>

</body>

</html>