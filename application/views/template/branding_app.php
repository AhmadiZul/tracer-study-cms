<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Tracer-Study</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url() ?>public/assets/img/logo_icon.png" rel="icon">
  <link href="<?php echo base_url() ?>public/assets/img/logo_icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <!-- <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Rubik:wght@300&display=swap" rel="stylesheet"> -->
  <link href="<?php echo base_url() ?>public/assets/fonts/rubik/static/Rubik-Regular.ttf" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url() ?>public/vendor/animate.css/animate.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/aos/aos.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/boxicons/css/boxicons.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/glightbox/css/glightbox.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/remixicon/remixicon.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/swiper/swiper-bundle.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/owl.carousel.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/owl.theme.default.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/animate.min.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet" media="print" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/select2/select2.min.css" rel="stylesheet" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet" onload="this.media='all'">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/assets/loading_page.css" rel="stylesheet" onload="this.media='all'">


  <!-- Template Main CSS File -->
  <link href="<?php echo base_url() ?>public/assets/css/style_branding.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/assets/css/custom-mitra.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/assets/css/custom.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Anyar - v4.7.1
  * Template URL: https://bootstrapmade.com/anyar-free-multipurpose-one-page-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
  <div id="spinner-front">
    <img src="<?php echo base_url() ?>public/assets/ajax-loader.gif" /><br>
    Loading...
  </div>
  <div id="spinner-back"></div>
  <!-- ======= Top Bar ======= -->
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center ">
    <div class="mx-4 w-100 d-flex align-items-center justify-content-between">

      <h1 class="logo" style="margin-left: 5rem;"><a href="<?php echo base_url() ?>index.php"><img src="<?php echo base_url() ?>public/assets/img/logo.png" alt=""></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href=index.html" class="logo"><img src="assets/img/logo.webp" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto <?php echo active_page('', 'active') ?>" href="<?php echo base_url() ?>">Beranda</a></li>
          <li><a class="nav-link scrollto" href="<?php echo base_url() . '#about' ?>">Pengenalan</a></li>
          <li><a class="nav-link scrollto" href="<?php echo base_url() . '#tujuan' ?>">Keuntungan</a></li>
          <li><a class="nav-link scrollto" href="<?php echo base_url() . '#demo' ?>"><span>Demo</span></a></li>
          <li><a class="nav-link scrollto" href="<?php echo base_url() . '#testimoni' ?>"><span>Testimoni</span></a></li>
          <li>
            <?php if ($this->session->userdata("t_idGroup") == 3) { ?>
              <a href="<?php echo base_url() . 'mitra/dashboard' ?>" class="btn-login" role="button">DASHBOARD</a>
            <?php } else if ($this->session->userdata("t_idGroup") == 4) { ?>
              <a href="<?php echo base_url() . 'alumni/dashboard' ?>" class="btn-login" role="button">DASHBOARD</a>
            <?php } else { ?>
              <a href="<?php echo base_url() .''?>" class="btn-login" role="button">Hubungi Kami</a>
            <?php } ?>
          </li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->
  <script src="<?php echo base_url() ?>public/assets/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/select2/select2.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/sweetalert2/sweetalert2.all.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  {CONTENT}
  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="footer-top-bg">
        <div class="container footer-container">
          <div class="row ">
            <div class="col-lg-4 py-5 footer-info text-center">
            <img src="<?php echo base_url() ?>public/assets/img/logo.png" class="img img-fluid " alt="" width="550" height="500">
            </div>
            <div class="col-lg-4 py-5 footer-info">
              <h4 class="text-light">About Us</h4>
              <p class="text-light">Website Tracer Study adalah studi penelusuran alumni untuk mengetahui kegiatan alumni setelah lulus dari Perguruan Tinggi Universitas, transisi dari dunia pendidikan tinggi ke dunia kerja, situasi kerja, pemerolehan kompetensi, </p>
            </div>
            <div class="col-lg-4">
              <div class="row">
                <div class="col align-self-center footer-links py-5">
                  <ul>
                    <li><a href="<?php echo base_url() ?>" class="text-light">Beranda</a></li>
                    <li><a href="<?php echo base_url() ?>#about" class="text-light">Pengenalan</a></li>
                    <li><a href="<?php echo base_url() ?>#tujuan" class="text-light">Keuntungan</a></li>
                    <li><a href="<?php echo base_url() ?>#demo" class="text-light">Demo</a></li>
                    <li><a href="<?php echo base_url() ?>#testimoni" class="text-light">Testimoni</a></li>
                  </ul>
                </div>
                <div class="col align-self-center footer-links  py-5">
                  <h4 class="text-light">Official Account</h4>
                  <div class="social-links mt-3">
                    <a href="" class="facebook"><i class="bx bxl-facebook-circle"></i></a>
                    <a href="" class="twitter"><i class="bx bxl-twitter text-light"></i></a>
                    <a href="" class="linkedin"><i class="bx bxl-linkedin text-light"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
          <div class="copyright">
          2022 Layanan Tracer Study di Perguruan Tinggi - build by<strong><span> Arkatama</span></strong>.
          </div>
        </div>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url() ?>public/vendor/aos/aos.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/owl-carousel/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/jquery-mask/jquery.mask.min.js"></script>


  <!-- Template Main JS File -->
  <script src="<?php echo base_url() ?>public/assets/js/main.js"></script>
  <script>
    $(document).ready(function() {
      $('.select2').select2();
      $(".mitra-carousel").owlCarousel({
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        items: 7,
        margin: 30,
        loop: true,
        smartSpeed: 550,
        autoWidth: true,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true
      });
      $('.carousel').carousel({
        interval: 2000
      });
      // $(".carousel-inner").testimoni({
      //   animateOut: 'slideOutDown',
      //   animateIn: 'flipInX',
      //   items: 2,
      //   loop: true,
      //   smartSpeed: 200,
      //   autoWidth: true,
      //   autoplay: true,
      //   autoplayTimeout: 20,
      //   autoplayHoverPause: true
      // });
    });
    /**
     * only number
     */
    $('.number-only').keyup(function(e) {
      if (/\D/g.test(this.value)) {
        // Filter non-digits from input value.
        this.value = this.value.replace(/\D/g, '');
      }
    });

    /**
     * only alpha
     */
    $('.alpha-only').bind('keydown', function(e) {
      if (e.altKey) {
        e.preventDefault();
      } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
        }
      }
    });
    /**
     * Loading
     */
    function loading() {
      document.getElementById("spinner-front").classList.add("show");
      document.getElementById("spinner-back").classList.add("show");
    }

    function dismiss_loading() {
      document.getElementById("spinner-front").classList.remove("show");
      document.getElementById("spinner-back").classList.remove("show");
    }
  </script>

  <?php
  echo '<script>';
  if (isset($scripts) && is_array($scripts)) {
    foreach ($scripts as $script) {
      $this->load->view($script);
    }
  }
  echo '</script>';
  ?>

</body>

</html>