<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Tracer Study</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo URL_FILE. $logo_title->url_file?>" rel="icon">
  <link href="<?php echo URL_FILE. $logo_title->url_file?>" rel="apple-touch-icon">

  <style>
    :root {
      --warna : <?php echo $warna_tema->deskripsi ?>;
      --warna-text : <?php echo $warna_tema->array ?>;
    }
  </style>

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <!-- <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Poppins:wght@300&display=swap" rel="stylesheet"> -->
  <link href="<?php echo base_url() ?>public/assets/fonts/poppins/Poppins-Regular.otf" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url() ?>public/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/select2/select2.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/datepicker/daterangepicker.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/sweetalert2/sweetalert2.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/assets/loading_page.css" rel="stylesheet">

  <link href="<?php echo base_url('public/vendor/datetimepicker/build/jquery.datetimepicker.min.css') ?>" rel="stylesheet">

  <!-- Template Stisla CSS -->
  <link rel="stylesheet" href="<?php echo base_url() ?>public/assets/stisla/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>public/assets/stisla/css/components.css">
</head>

<body>
  <div id="spinner-front">
    <img src="<?php echo base_url() ?>public/assets/ajax-loader.gif" /><br>
    Loading...
  </div>
  <div id="spinner-back"></div>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar justify-content-between">
        <form class="form-inline mr-auto d-flex">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown dropdown-list-toggle dropstart">
            <a href="#" class="nav-link notification-toggle nav-link-lg beep" id="dropdownMessage" data-toggle="dropdown" aria-expanded="false"><i class="far fa-bell"></i></a>
            <div class="dropdown-left dropdown-menu dropdown-list dropdown-menu-right" id="dropdownMessage">
              <div class="dropdown-header">Notifications
                <div class="float-right">
                  <a href="#">Mark All As Read</a>
                </div>
              </div>
              <div class="dropdown-list-content dropdown-list-icons">
                <a href="#" class="dropdown-item dropdown-item-unread">
                  <div class="dropdown-item-icon bg-primary text-white">
                    <i class="fas fa-code"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    Template update is available now!
                    <div class="time text-primary">2 Min Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <div class="dropdown-item-icon bg-info text-white">
                    <i class="far fa-user"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    <b>You</b> and <b>Dedik Sugiharto</b> are now friends
                    <div class="time">10 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <div class="dropdown-item-icon bg-success text-white">
                    <i class="fas fa-check"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    <b>Kusnaedi</b> has moved task <b>Fix bug header</b> to <b>Done</b>
                    <div class="time">12 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <div class="dropdown-item-icon bg-danger text-white">
                    <i class="fas fa-exclamation-triangle"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    Low disk space. Let's clean it!
                    <div class="time">17 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <div class="dropdown-item-icon bg-info text-white">
                    <i class="fas fa-bell"></i>
                  </div>
                  <div class="dropdown-item-desc">
                    Welcome to Stisla template!
                    <div class="time">Yesterday</div>
                  </div>
                </a>
              </div>
              <div class="dropdown-footer text-center">
                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
              </div>
            </div>
          </li>
          <li class="dropdown">
            <a href="#" class="nav-link dropdown-toggle nav-link-lg nav-link-user" id="dropdownProfile" data-toggle="dropdown" aria-expanded="false">
              <?php
              $photo = ($this->session->userdata('t_url_photo') != '' ? $this->session->userdata('t_url_photo') : 'public/assets/img/unknown.webp');
              ?>
              <img alt="image" src="<?php echo $photo ?>" class="rounded-circle mr-3">
              <div class="d-sm-none d-lg-inline-block"><?php echo $this->session->userdata('t_nama') ?></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right" id="dropdownProfile">
              <!-- <div class="dropdown-title">Logged in 5 min ago</div>
              <a href="features-profile.html" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <a href="features-activities.html" class="dropdown-item has-icon">
                <i class="fas fa-bolt"></i> Activities
              </a>
              <a href="features-settings.html" class="dropdown-item has-icon">
                <i class="fas fa-cog"></i> Settings
              </a> -->
              <div class="dropdown-divider"></div>
              <a href="<?php echo base_url() ?>logout" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>

      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          
            <div class="sidebar-brand text-center">
              <a class="nav-link" href="<?php echo base_url() ?>alumni/dashboard"><img class="img-fluid" src="<?php echo URL_FILE . $logo_utama->url_file ?>" alt="" style="height:80px;margin-top:-190px;"></a>
            </div>
            <div class="sidebar-brand text-center sidebar-brand-sm">
              <a class="nav-link" href="<?php echo base_url() ?>alumni/dashboard"><img class="img-fluid" src="<?php echo URL_FILE . $logo_title->url_file ?>" alt="" style="margin-top:-190px;margin-bottom:0px;"></a>
            </div>
          <ul class="sidebar-menu" style="margin-top:-120px;">
            <?php if (in_array("alumni.access", $userMenus)) : ?>
              <li class="<?php echo active_page('alumni', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('alumni/dashboard') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard"><i class="fas fa-house"></i> <span>Dashboard</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("profil_alumni.access", $userMenus)) : ?>
              <li class="<?php echo active_page('profil_alumni', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('profil_alumni') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Profil"><i class="fas fa-clipboard-list"></i> <span>Profil</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("tracer_alumni.access", $userMenus)) : ?>
              <li class="<?php echo active_page('tracer_alumni', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('tracer_alumni') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Tracer Study"><i class="fas fa-clipboard-list"></i> <span>Tracer Study</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("alumni.access", $userMenus)) : ?>
              <!-- <li class="menu-header">PROGRAM ALUMNI</li> -->

             <!--  <?php if (in_array("company_talk.access", $userMenus)) : ?>
                <li>
                  <a class="nav-link" href="<?php echo base_url('company_talk/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Company Talk">
                    <i class="fas fa-comments"></i> <span>Company Talk</span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if (in_array("career_fair.access", $userMenus)) : ?>
                <li>
                  <a class="nav-link" href="<?php echo site_url('career_fair/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Career Fair">
                    <i class="fas fa-envelope-open-text"></i> <span>Career Fair</span>
                  </a>
                </li>
              <?php endif; ?> -->
              <?php if (in_array("campus_hiring.access", $userMenus)) : ?>
                <li>
                  <a class="nav-link" href="<?php echo base_url() ?>404" data-bs-toggle="tooltip" data-bs-placement="right" title="Campus Hiring">
                    <i class="fas fa-graduation-cap"></i> <span>Campus Hiring</span>
                  </a>
                </li>
              <?php endif; ?>

              <li class="menu-header">SETTING</li>
              <li class="<?php echo active_page('akun', 'active') ?>"><a class="nav-link" href="<?php echo base_url() ?>akun/index" data-bs-toggle="tooltip" data-bs-placement="right" title="Akun"><i class="fas fa-briefcase"></i> <span>Akun</span></a></li>
              <?php if (in_array("sistem_setting.access", $userMenus)) : ?>
              <li class="<?php echo active_page('sistem_setting', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('sistem_setting') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Sistem"><i class="fas fa-clipboard-list"></i> <span>Sistem Setting</span></a>
              </li>
            <?php endif; ?>
            <?php endif; ?>
            <?php if (in_array("mitra.access", $userMenus)) : ?>
              <li class="<?php echo active_page('mitra', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('mitra/dashboard') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard"><i class="bx bxs-dashboard"></i> <span>Dashboard</span></a>
              </li>
              <li class="<?php echo active_page('profil_mitra', 'active') ?>"><a class="nav-link" href="<?php echo base_url() ?>profil_mitra/index" data-bs-toggle="tooltip" data-bs-placement="right" title="Profil"><i class="fas fa-clipboard-list"></i> <span>Profil</span></a></li>
              <li class="<?php echo active_page('survey_mitra', 'active') ?>"><a class="nav-link" href="<?php echo base_url() ?>survey_mitra" data-bs-toggle="tooltip" data-bs-placement="right" title="Survey Mitra"><i class="fas fa-chart-simple"></i> <span>Survey Mitra</span></a></li>
              <li class="menu-header">PROGRAM MITRA</li>
              <?php if (in_array("company_talk.access", $userMenus)) : ?>
                <li>
                  <a class="nav-link" href="<?php echo base_url('company_talk/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Company Talk">
                    <i class="fas fa-comments"></i> <span>Company Talk</span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if (in_array("career_fair.access", $userMenus)) : ?>
                <li>
                  <a class="nav-link" href="<?php echo site_url('career_fair/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Career Fair">
                    <i class="fas fa-envelope-open-text"></i> <span>Career Fair</span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if (in_array("campus_hiring.access", $userMenus)) : ?>
                <li>
                  <a class="nav-link" href="<?php echo base_url() ?>404" data-bs-toggle="tooltip" data-bs-placement="right" title="Campus Hiring">
                    <i class="fas fa-graduation-cap"></i> <span>Campus Hiring</span>
                  </a>
                </li>
              <?php endif; ?>
              <li class="menu-header">SETTING</li>
              <li class="<?php echo active_page('lowongan_kerja', 'active') ?>"><a href="<?php echo base_url() ?>lowongan_kerja/mitra/index" class="nav-link" href="<?php echo base_url() ?>404" data-bs-toggle="tooltip" data-bs-placement="right" title="Lowongan Kerja"><i class="fas fa-briefcase"></i> <span>Lowongan Kerja</span></a></li>
              <li class="<?php echo active_page('lowongan_magang', 'active') ?>"><a href="<?php echo base_url() ?>lowongan_magang/back" class="nav-link" href="<?php echo base_url() ?>404" data-bs-toggle="tooltip" data-bs-placement="right" title="Lowongan magang"><i class="fas fa-briefcase"></i> <span>Lowongan Magang</span></a></li>
              <li class="<?php echo active_page('pelamar', 'active') ?>"><a class="nav-link" href="<?php echo base_url() ?>pelamar/index" data-bs-toggle="tooltip" data-bs-placement="right" title="Kandidat"><i class="fas fa-user-check"></i> <span>Kandidat</span></a></li>
            <?php endif; ?>
            <li><a class="nav-link" href="<?php echo base_url() ?>logout" data-bs-toggle="tooltip" data-bs-placement="right" title="Keluar"><i class="fas fa-right-from-bracket"></i> <span>Keluar</span></a></li>
          </ul>

        </aside>
      </div>

      <script src="<?php echo base_url() ?>public/assets/js/jquery-3.5.1.min.js"></script>
      <script src="<?php echo base_url() ?>public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="<?php echo base_url() ?>public/vendor/sweetalert2/sweetalert2.all.min.js"></script>

      <div class="main-content">
        <section class="section">
          <?php if ($is_home === true) : ?>
            <?php if ($user === 'mitra') : ?>
              <div class="row">
                <div class="col-xs-12 col-lg-12">
                  <div class="card">
                    <div class="card-header">
                      <h3>Tahapan Rekrutmen</h3>
                      <select class="select2 form-control mx-3" style="width:20%;">
                        <option>Agriculture</option>
                        <option>Agriculture</option>
                        <option>Agriculture</option>
                      </select>
                      <a class="float-end">Lihat Semua Lowongan ></a>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-xs-12 col-lg-3">
                          <img class="img-fluid" src="<?php echo base_url() ?>public/assets/img/admin/rekrutmen.png" alt="">
                        </div>
                        <div class="col-xs-12 col-lg-9">
                          <h2>Anda belum membuka lowongan pekerjaan</h2>
                          <p>Buka kesempatan pekerjaan untuk alumni binaan Pusdiktan dan dapatkan pekerja yang unggul serta kompetitif.</p>
                          <a href="" role="button" class="btn btn-primary">Buka Lowongan</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-lg-4 d-none">
                  <div class="card">
                    <div class="card-body">
                      <div class="col-lg-12 text-center">
                        <div class="avatar-item col-sm-1 offset-sm-6 col-3 offset-5 col-lg-3  offset-lg-5">
                          <img class="img-fluid" src="<?php echo $photo ?>" data-toggle="tooltip" title="<?php echo $this->session->userdata('t_nama') ?>">
                          <div class="avatar-badge" title="Editor" data-toggle="tooltip"><i class="fas fa-pencil"></i></div>
                        </div>
                        <h2><?php echo $this->session->userdata('t_nama') ?></h2>
                        <p><i class="fas fa-location-dot"></i> Malang, Jawa Timur</p>
                        <div class="btn-group">
                          <a href="#" class="sosmed"><i class="fa-brands fa-instagram"></i></a>
                          <a href="#" class="sosmed"><i class="fa-brands fa-facebook"></i></a>
                          <a href="#" class="sosmed"><i class="fa-brands fa-twitter"></i></a>
                          <a href="https://www.youtube.com/c/PusdiktanBPPSDMPKementan" class="sosmed"><i class="fa-brands fa-youtube"></i></a>
                          <a href="#" class="sosmed"><i class="fa-brands fa-whatsapp"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php else : ?>
              <div class="topi card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-12 col-xs-12">
                      <h2>Selamat Datang <?php echo $this->session->userdata('tracer_nama') ?></h2>
                      <p class="pb-3">Alumni terdata tahun lulus <?= $this->session->userdata("tracer_tahun") ?> sejumlah <?= (isset($total) ? $total : '-') ?></p>
                      <div class="text-center h5">
                        <select name="asal_alumni" id="asal_alumni" class="form-control select2" style="width: 100%;text-align:center;">
                          <option value="">- Filter Berdasarkan Asal Kampus/Sekolah -</option>
                          <?php if (isset($selectPolbangtan)) { ?>
                            <?php foreach ($selectPolbangtan as $key => $value) { ?>
                              <option value="<?= $value->id_perguruan_tinggi ?>"><?= $value->nama_resmi ?></option>
                            <?php } ?>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>
          <?php else : ?>
            <div class="row">
              <div class="col-sm-10">
                <h4><?php echo (isset($icon) ? $icon : "Not found") ?><span class="text-dark"> <?php echo (isset($title) ? $title : "Not found") ?></span></h4>
              </div>
              <div class="col-sm-2" style="margin-top:-10px; left:50px;">
                <a href="" role="button" class="btn btn-outline-tema float-end" style="border-radius: 16px;"><i class="fas fa-rotate"></i> Refresh</a>
              </div>
            </div>
          <?php endif; ?>
        </section>
        <section class="content">
          {CONTENT}
        </section>

      </div>
    </div>
  </div>

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url() ?>public/vendor/datepicker/moment.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/aos/aos.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/owl-carousel/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/select2/select2.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/datepicker/daterangepicker.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/jquery-mask/jquery.mask.min.js"></script>

  <!-- ninescroll -->
  <script src="<?php echo base_url() ?>public/vendor/jquery.nicescroll-3.7.6/jquery.nicescroll.min.js"></script>

  <!-- Template Stisla JS File -->
  <script src="<?php echo base_url() ?>public/assets/stisla/js/stisla.js"></script>

  <!-- Template JS File -->
  <script src="<?php echo base_url() ?>public/assets/stisla/js/scripts.js"></script>
  <script src="<?php echo base_url() ?>public/assets/stisla/js/custom.js"></script>
  <script src="<?php echo base_url('public/vendor/datetimepicker/build/jquery.datetimepicker.full.js') ?>"></script>

  <script>
    $('.select2').select2({
      theme: 'bootstrap4',
    });

    $('.fulldate').datetimepicker({
      datepicker: true,
      format: 'Y-m-d H:i:s',
    });
    $('.date-only').datetimepicker({
      datepicker: true,
      timepicker: false,
      format: 'Y-m-d',
    });

    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }

    function showLoading() {
      document.getElementById("spinner-front").classList.add("show");
      document.getElementById("spinner-back").classList.add("show");
    }

    function hideLoading() {
      document.getElementById("spinner-front").classList.remove("show");
      document.getElementById("spinner-back").classList.remove("show");
    }

    function getFileSize(_size) {
      var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
        i = 0;
      while (_size > 900) {
        _size /= 1024;
        i++;
      }
      var exactSize = (Math.round(_size * 100) / 100) + ' ' + fSExt[i];
      return exactSize;
    }


    function cekFileUpload(input, id = "") {
      var listTypeAllowedPreviewed = [];
      var listType = [];
      var limit = 2097152; // 2 MB
      var filetype = (input.files[0].name).split('.').pop().toLowerCase();
      listType = ['jpeg', 'pdf', 'jpg', 'gif', 'bmp', 'png', 'docx', 'doc', 'xlsx', 'xls', 'ppt', 'pptx'];

      listTypeAllowedPreviewed = ['jpeg', 'pdf', 'jpg', 'gif', 'bmp', 'png'];

      $('.file-alert-type,.file-alert-size').html('');
      if (input.files && input.files[0]) {
        var filesize = input.files[0].size;
        var filetype = (input.files[0].name).split('.').pop().toLowerCase();
        if (filesize < limit && listType.includes(filetype)) {
          var reader = new FileReader();
          reader.onload = function(e) {
            if (listTypeAllowedPreviewed.includes(filetype)) {
              if (filetype == 'pdf') {
                $(`#file-preview${id}`).html('<embed type="application/pdf" src="' + e.target.result + '#toolbar=0&navpanes=0&scrollbar=0" width="100%" height="500"></embed>');
              } else {
                $(`#file-preview${id}`).html('<img src="' + e.target.result + '" class="img img-responsive rounded" style="width:100%;">');
              }
            } else {
              $(`#file-preview${id}`).html(`<div class="alert alert-info"><b>Mohon Maaf!</b><p>Berkas yang diupload tidak /* bi */sa ditampilkan</p></div>`);
            }
          }
          reader.readAsDataURL(input.files[0]);
          $(`.file-name${id}`).html(input.files[0].name + ' (' + getFileSize(filesize) + ') <i class="fa fa-check text-green"></i>');
          $('.file-alert-type' + id + '').html("");
          $(`.file-alert-size${id}`).html('');
        } else {
          $(input).val('');
          $(`#file-target${id}`).attr('src', '').attr('width', '0');
          $(`#file-preview${id}`).html(``);
          $(`.file-name${id}`).html('');

          if (listType.includes(filetype) === false)
            $(`.file-alert-type${id}`).html("<i>Jenis file '" + filetype.toUpperCase() + "' tidak diijinkan. </i>")
          if (filesize > limit)
            $(`.file-alert-size${id}`).html('<i>Ukuran file tidak boleh lebih dari 2 MB</i>');
        }

      }
    }
  </script>
  <?php
  echo '<script>';
  if (isset($scripts) && is_array($scripts)) {
    foreach ($scripts as $script) {
      $this->load->view($script);
    }
  }
  echo '</script>';
  ?>
</body>