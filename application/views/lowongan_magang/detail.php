<section id="hero-jadwal">
    <div class="card row mx-5" style="background-color: #064514;box-shadow: none;">
        <div class="col-md-12">
            <h2><?= $lowongan->posisi ?></h2>
            <p><?= $lowongan->nama ?></p>
            <div class="row">
                <div class="col-md-6">
                    <p for=""><b>Lokasi Magang</b> : <?= $lowongan->nama_kab_kota ?>, <?= $lowongan->nama_provinsi ?></p>

                </div>
                <div class="col-md-6">
                    <p for=""><b>Pendidikan</b> : <?= $lowongan->nama_pendidikan ?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="detail_lowongan" class="container">
    <div class="card">
        <div class="card-header">
            <h2>Deskripsi</h2>
        </div>
        <div class="card-body">
            <?= $lowongan->deskripsi ?>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h2>Persyaratan</h2>
        </div>
        <div class="card-body">
            <?= $lowongan->persyaratan ?>
        </div>
    </div>
</section>