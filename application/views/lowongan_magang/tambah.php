<link href="<?php echo base_url() ?>public/vendor/summernote/summernote-bs4.min.css" rel="stylesheet">
<div class="card">
    <form id="form_lowongan">
        <div class="card-body">
            <div class="form-group row">
                <label for="status" class="col-md-3 col-form-label"></label>
                <div class="col-md-9">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="publish" name="status" value="Publish">
                        <label class="custom-control-label" for="publish">Publish</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="draft" name="status" value="Draft" checked>
                        <label class="custom-control-label" for="draft">Draft</label>
                    </div>
                    <div class="invalid-feedback" for="status"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="id_instansi" class="col-md-3 col-form-label">Mitra <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <select name="id_instansi" id="id_instansi" class="form-control select2">
                        <option value="">Pilih Mitra</option>
                        <?php foreach ($select_mitra as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value ?></option>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback" for="id_instansi"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="tgl_mulai" class="col-md-3 col-form-label">Tanggal Publish <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <div class="input-group input-daterange">
                        <input type="text" class="form-control" name="start_publish" name="start_publish">
                        <div class="input-group-text">to</div>
                        <input type="text" class="form-control" name="end_publish" id="end_publish">
                    </div>
                    <div class="row">
                        <div class="invalid-feedback col-md-6" for="start_publish"></div>
                        <div class="invalid-feedback col-md-6" for="end_publish"></div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="posisi" class="col-md-3 col-form-label">Posisi <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" name="posisi" id="posisi" class="form-control" maxlength="">
                    <div class="invalid-feedback" for="posisi"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <textarea name="deskripsi" id="deskripsi" class="form-control"></textarea>
                    <div class="invalid-feedback" for="deskripsi"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="lokasi_kerja" class="col-md-3 col-form-label">Lokasi Kerja <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <select name="lokasi_kerja" id="lokasi_kerja" class="form-control select2">
                        <option value="">Pilih Lokasi</option>
                        <?php foreach ($select_kabupaten as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value ?></option>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback" for="lokasi_kerja"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="id_pendidikan" class="col-md-3 col-form-label">Minimal Pendidikan <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <select name="id_pendidikan" id="id_pendidikan" class="form-control select2">
                        <option value="">Pilih Pendidikan</option>
                        <?php foreach ($select_pendidikan as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value ?></option>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback" for="id_pendidikan"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="persyaratan" class="col-md-3 col-form-label">Persyaratan <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <textarea name="persyaratan" id="persyaratan" class="form-control"></textarea>
                    <div class="invalid-feedback" for="persyaratan"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="url_photo" class="col-md-3 col-form-label">Upload Poster</label>
                <div id="new-upload-lowongan" class="col-md-9">
                    <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                    <input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'lowongan')">
                    <span class="file-info-lowongan text-muted">Tidak ada berkas yang dipilih</span>
                    <div id="preview-lowongan">

                    </div>
                    <div class="hint-block text-muted mt-3">
                        <small>
                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                            Ukuran file maksimal: <strong>2 MB</strong>
                        </small>
                    </div>
                    <div class="invalid-feedback" for="url_photo"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="<?php echo base_url('lowongan_magang/back') ?>" class="btn btn-warning">Kembali</a>
            <button type="submit" class="btn btn-success">Simpan</button>
        </div>
    </form>
</div>
<script src="<?php echo base_url() ?>public/vendor/summernote/summernote-bs4.min.js"></script>
<script>
    var site_url = '<?php echo site_url() ?>';
    var toolbar = [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link']],
        ['misc', ['fullscreen']]
    ];
    $('#deskripsi').summernote({
        tabsize: 2,
        height: 100,
        toolbar: toolbar
    });
    $('#persyaratan').summernote({
        tabsize: 2,
        height: 100,
        toolbar: toolbar
    });
    $('#form_lowongan').on('submit', function(e) {
        e.preventDefault();

        $('#form_lowongan').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $(`.invalid-feedback`).removeClass('d-block');
        $.ajax({
            url: site_url + 'lowongan_magang/back/create',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                swal({
                    title: data.message.title,
                    text: data.message.body,
                    type: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = site_url + 'lowongan_magang/back';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                dismiss_loading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);

                                if (field == 'start_publish') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'end_publish') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'gaji_rate_atas') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'gaji_rate_bawah') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal({
                            title: response.message.title,
                            html: response.message.body,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    });
</script>