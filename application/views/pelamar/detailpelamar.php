<link rel="stylesheet" href="<?= base_url('public/vendor/datatables/jquery.dataTables.min.css') ?>">
<a href="<?php echo base_url() ?>pelamar/index/pelamar?id_lowongan=<?= $id_lowongan ?>" class="btn btn-warning mb-3">Kembali</a>
<div class="card">
    <div class="card-body">
        <h5 class="my-3">Data Diri</h5>
        <div class="row">
            <div class="form-group col-md-12 text-center">
                <img src="<?= $alumni['url_photo'] ?>" style="max-width: 200px;">
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Nama</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['nama'] ?></label>
            </div>
            <div class="form-group col-md-6"></div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">NIK</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['nik'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">NIM/NIRM/NIS</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['nim'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Tempat Lahir</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['tempat_lahir'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Tanggal Lahir</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['tgl_lahir'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Agama</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['nama_agama'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Jenis Kelamin</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['jenis_kelamin'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Nomer Hp</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['no_hp'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Facebook</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['sosmed']->facebook ? '<a href="' . $alumni['sosmed']->facebook . '">facebook</a>' : '-' ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Twitter</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['sosmed']->twitter ? '<a href="' . $alumni['sosmed']->twitter . '">Twitter</a>' : '-' ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Linkedin</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['sosmed']->linkedin ? '<a href="' . $alumni['sosmed']->linkedin . '">Linkedin</a>' : '-' ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Email</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['email'] ?></label>
            </div>
            <div class="col-md-6"></div>
        </div>
    </div>
    <div class="card-body">
        <h5 class="my-3">Alamat</h5>
        <div class="row">
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Provinsi</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['nama_provinsi'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Kabupaten/Kota</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['nama_kab_kota'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Kecamatan</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['nama_kecamatan'] ?></label>
            </div>
            <div class="col-md-6"></div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Alamat</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['alamat'] ?></label>
            </div>
            <div class="col-md-6"></div>
        </div>
    </div>
    <div class="card-body">
        <h5 class="my-3">Asal Kampus/Sekolah</h5>
        <div class="row">
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Jalur Masuk</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['jalur_masuk'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Tahun Lulus</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['tahun_lulus'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Asal Kampus/Sekolah</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['nama_resmi'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Program Studi/Jurusan</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['nama_prodi'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">IPK Terakhir</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['ipk_terakhir'] ?></label>
            </div>
            <div class="form-group col-md-6 row">
                <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Judul Tugas Akhir</label>
                <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= $alumni['judul_skripsi'] ?></label>
            </div>
        </div>
    </div>
    <div class="card-body">
        <h5>Status Alumni</h5>
        <span><?= $alumni['status_alumni'] ?></span>
        <div class="row">
            <?php if ($alumni['id_ref_status_alumni'] == 1) : ?>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Nama Tempat Kerja</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_jobs->nama_instansi) ? $alumni_jobs->nama_instansi : '' ?></label>
                </div>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Jabatan</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_jobs->jabatan) ? $alumni_jobs->jabatan : '' ?></label>
                </div>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Sektor</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_jobs->sektor) ? $alumni_jobs->sektor : '' ?></label>
                </div>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Alamat</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_jobs->alamat) ? $alumni_jobs->alamat : '' ?></label>
                </div>
            <?php elseif ($alumni['id_ref_status_alumni'] == 3) : ?>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Nama Tempat Kerja</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_jobs->nama_instansi) ? $alumni_jobs->nama_instansi : '' ?></label>
                </div>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Jabatan</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_jobs->jabatan) ? $alumni_jobs->jabatan : '' ?></label>
                </div>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Sektor</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_jobs->sektor) ? $alumni_jobs->sektor : '' ?></label>
                </div>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Alamat</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_jobs->alamat) ? $alumni_jobs->alamat : '' ?></label>
                </div>
            <?php elseif ($alumni['id_ref_status_alumni'] == 4) : ?>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Nama Perguruan Tinggi</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_study->nama_perguruan_tinggi) ? $alumni_study->nama_perguruan_tinggi : '' ?></label>
                </div>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Program Studi</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_study->prodi) ? $alumni_study->prodi : '' ?></label>
                </div>
                <div class="form-group col-md-6 row">
                    <label for="input_nama" class="col-md-3 col-form-label font-weight-bold text-primary">Sektor</label>
                    <label for="input_nama" class="col-md-9 col-form-label text-dark"><?= isset($alumni_study->sektor) ? $alumni_study->sektor : '' ?></label>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="card-body table-responsive">
        <h5>Prestasi</h5>
        <table id="prestasi" class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Penyelenggara</th>
                    <th>Tahun</th>
                    <th>Tingkat</th>
                    <th>Gelar</th>
                    <th>Bukti</th>
                </tr>
            </thead>
            <tbody id="prestasiBody">
            </tbody>
        </table>
    </div>
    <div class="card-body table-responsive">
        <h5>Organisasi</h5>
        <table id="organisasi" class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Organisasi</th>
                    <th>Posisi</th>
                    <th>Deskripsi Pekerjaan</th>
                    <th>Bukti</th>
                </tr>
            </thead>
            <tbody id="organisasiBody">
            </tbody>
        </table>
    </div>
    <div class="card-body table-responsive">
        <h5>Keahlian/Skill</h5>
        <table id="skill" class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Skill</th>
                    <th>Bidang</th>
                    <th>Level</th>
                    <th>Bukti</th>
                </tr>
            </thead>
            <tbody id="skillBody">
            </tbody>
        </table>
    </div>
    <div class="card-body table-responsive">
        <h5>Pengalaman Kerja/Magang</h5>
        <table id="pengalaman" class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Jenis</th>
                    <th>Nama Instansi</th>
                    <th>Posisi</th>
                    <th>Alamat</th>
                    <th>Tanggal Mulai</th>
                    <th>Tanggal Selesai</th>
                    <th>Bukti</th>
                </tr>
            </thead>
            <tbody id="pengalamanBody">
            </tbody>
        </table>
    </div>
</div>

<!-- modal preview -->
<div id="modal_preview" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Bukti</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group preview">

                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('public/vendor/datatables/datatables.min.js'); ?>"></script>
<script>
    let id_alumni = '<?php echo $alumni['id_alumni'] ?>';

    function loadTabelPrestasi() {
        // $('.loader').removeClass('d-none');
        // $('#result').addClass('d-none');
        $.ajax({
            url: '<?php echo site_url() ?>pelamar/index/getDaftarPrestasi?id_alumni=' + id_alumni,
            dataType: 'json',
            type: 'POST',
            success: function(data) {
                let tbody = '';
                console.log(data)
                if (data['status']) {
                    let iteration = 0;
                    data['isi'].forEach(function(isi) {
                        iteration += 1;

                        bukti = '<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_preview" data-url="' + isi['url_sertifikat'] + '" data-type="' + isi['type'] + '" data-title="Sertifikat"><i class="fa fa-eye"></i> Preview</button>';


                        tbody += availableDataPrestasi(iteration, isi['nama_prestasi'], isi['penyelenggara'], isi['tahun'], isi['tingkat'], isi['gelar'], bukti);
                    });

                    // $('#totalRow').text(data['jumlah']);
                } else if (!data['status']) {
                    tbody = `
                            <tr class="odd">
                            <td valign="top" colspan="7" class="dataTables_empty">Data Tidak Ditemukan</td>
                            </tr>`;
                } else {
                    tbody = `
                            <tr class="odd">
                                <td valign="top" colspan="7" class="dataTables_empty">Data Tidak Ditemukan</td>
                                </tr>`;
                }

                // $('.loader').addClass('d-none');
                // $('#result').removeClass('d-none');

                $('#prestasiBody').html(null);
                $('#prestasiBody').append(tbody);

                $('#prestasi').DataTable({
                    pageLength: 30,
                    lengthMenu: [
                        30,
                        60,
                        90,
                    ],
                });
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    };

    function availableDataPrestasi(no, prestasi, nama_penyelenggara, tahun, tingkat, gelar, aksi) {
        return `
            <tr>
                <td>${no}</td>
                <td>${prestasi}</td>
                <td>${nama_penyelenggara}</td>
                <td>${tahun}</td>
                <td>${tingkat}</td>
                <td>${gelar}</td>
                <td>${aksi}</td>
            </tr>`;
    }

    function loadTabelOrganisasi() {
        // $('.loader').removeClass('d-none');
        // $('#result').addClass('d-none');
        $.ajax({
            url: '<?php echo site_url() ?>pelamar/index/getDaftarOrganisasi?id_alumni=' + id_alumni,
            dataType: 'json',
            type: 'POST',
            success: function(data) {
                let tbody = '';
                console.log(data)
                if (data['status']) {
                    let iteration = 0;
                    data['isi'].forEach(function(isi) {
                        iteration += 1;

                        bukti = '<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_preview" data-url="' + isi['url_bukti'] + '" data-type="' + isi['type'] + '" data-title="Bukti"><i class="fa fa-eye"></i> Preview</button>';


                        tbody += availableDataOrganisasi(iteration, isi['nama_organisasi'], isi['posisi'], isi['job_desc'], bukti);
                    });

                    // $('#totalRowOrganisasi').text(data['jumlah']);
                } else if (!data['status']) {
                    tbody = `
                            <tr class="odd">
                            <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                            </tr>`;
                } else {
                    tbody = `
                            <tr class="odd">
                                <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                                </tr>`;
                }

                // $('.loader').addClass('d-none');
                // $('#result').removeClass('d-none');

                $('#organisasiBody').html(null);
                $('#organisasiBody').append(tbody);

                $('#organisasi').DataTable({
                    pageLength: 30,
                    lengthMenu: [
                        30,
                        60,
                        90,
                    ],
                });
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    };

    function availableDataOrganisasi(no, organisasi, posisi, jobDesc, aksi) {
        return `
            <tr>
                <td>${no}</td>
                <td>${organisasi}</td>
                <td>${posisi}</td>
                <td>${jobDesc}</td>
                <td>${aksi}</td>
            </tr>`;
    }
    function loadTabelSkill() {
        // $('.loader').removeClass('d-none');
        // $('#result').addClass('d-none');
        $.ajax({
            url: '<?php echo site_url() ?>pelamar/index/getDaftarSkill?id_alumni=' + id_alumni,
            dataType: 'json',
            type: 'POST',
            success: function(data) {
                let tbody = '';
                console.log(data)
                if (data['status']) {
                    let iteration = 0;
                    data['isi'].forEach(function(isi) {
                        iteration += 1;

                        bukti = '<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_preview" data-url="' + isi['url_sertifikat'] + '" data-type="' + isi['type'] + '" data-title="Sertifikat"><i class="fa fa-eye"></i> Preview</button>';


                        tbody += availableDataSkill(iteration, isi['nama_skill'], isi['bidang'], isi['level'], bukti);
                    });

                    // $('#totalRowOrganisasi').text(data['jumlah']);
                } else if (!data['status']) {
                    tbody = `
                            <tr class="odd">
                            <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                            </tr>`;
                } else {
                    tbody = `
                            <tr class="odd">
                                <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                                </tr>`;
                }

                // $('.loader').addClass('d-none');
                // $('#result').removeClass('d-none');

                $('#skillBody').html(null);
                $('#skillBody').append(tbody);

                $('#skill').DataTable({
                    pageLength: 30,
                    lengthMenu: [
                        30,
                        60,
                        90,
                    ],
                });
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    };

    function availableDataSkill(no, skill, bidang, level, aksi) {
        return `
            <tr>
                <td>${no}</td>
                <td>${skill}</td>
                <td>${bidang}</td>
                <td>${level}</td>
                <td>${aksi}</td>
            </tr>`;
    }

    function loadTabelPengalaman() {
        // $('.loader').removeClass('d-none');
        // $('#result').addClass('d-none');
        $.ajax({
            url: '<?php echo site_url() ?>pelamar/index/getDaftarPengalaman?id_alumni=' + id_alumni,
            dataType: 'json',
            type: 'POST',
            success: function(data) {
                let tbody = '';
                console.log(data)
                if (data['status']) {
                    let iteration = 0;
                    data['isi'].forEach(function(isi) {
                        iteration += 1;

                        bukti = '<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_preview" data-url="' + isi['url_bukti'] + '" data-type="' + isi['type'] + '" data-title="Sertifikat"><i class="fa fa-eye"></i> Preview</button>';


                        tbody += availableDataPengalaman(iteration, isi['jenis_pengalaman'], isi['nama_instansi'], isi['posisi'], isi['alamat'], isi['start_date'], isi['end_date'], bukti);
                    });

                    // $('#totalRowOrganisasi').text(data['jumlah']);
                } else if (!data['status']) {
                    tbody = `
                            <tr class="odd">
                            <td valign="top" colspan="8" class="dataTables_empty">Data Tidak Ditemukan</td>
                            </tr>`;
                } else {
                    tbody = `
                            <tr class="odd">
                                <td valign="top" colspan="8" class="dataTables_empty">Data Tidak Ditemukan</td>
                                </tr>`;
                }

                // $('.loader').addClass('d-none');
                // $('#result').removeClass('d-none');

                $('#pengalamanBody').html(null);
                $('#pengalamanBody').append(tbody);

                $('#pengalaman').DataTable({
                    pageLength: 30,
                    lengthMenu: [
                        30,
                        60,
                        90,
                    ],
                });
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    };

    function availableDataPengalaman(no, jenis, instansi, posisi, alamat, startDate, endDate, aksi) {
        return `
            <tr>
                <td>${no}</td>
                <td>${jenis}</td>
                <td>${instansi}</td>
                <td>${posisi}</td>
                <td>${alamat}</td>
                <td>${startDate}</td>
                <td>${endDate}</td>
                <td>${aksi}</td>
            </tr>`;
    }
    $(document).ready(function(e) {
        loadTabelPrestasi();
        loadTabelOrganisasi();
        loadTabelSkill();
        loadTabelPengalaman();
    });

    $('#modal_preview').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget);
        var url = button.data('url');
        var type = button.data('type');
        var title = button.data('title');
        var allowed = [];
        var sizePreview = [];

        var modal = $(this)
        modal.find('.modal-title').text(title);

        if (type == 'url_photo') {
            allowed = ['png', 'jpg', 'jpeg'];
            sizePreview = ['250px', '250px'];
        } else {
            allowed = ['png', 'jpg', 'jpeg', 'pdf'];
            sizePreview = ['100%', '800'];
        }

        if (type == 'pdf') {
            modal.find('.preview').html('<embed src="' + url + '" class="mt-4" width="' + sizePreview[0] + '" height="' + sizePreview[1] + '" />')
        } else {
            modal.find('.preview').html('<img src="' + url + '" class="mt-4" alt="your image" style="width: ' + sizePreview[0] + '"/>')
        }
    })
</script>