<link rel="stylesheet" href="<?= base_url('public/vendor/datatables/jquery.dataTables.min.css') ?>">

<div class="card">
    <div class="card-body">
        <table id="example" class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Posisi</th>
                    <th>Minimal Pendidikan</th>
                    <th>FULL/PART Time</th>
                    <th>Peminat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody id="resultBody">
            </tbody>
        </table>
    </div>
</div>

<script src="<?= base_url('public/vendor/datatables/datatables.min.js'); ?>"></script>
<script type="text/javascript">
    var site_url = '<?php echo site_url() ?>pelamar/index/';

    function loadTabelALumni() {
        $('.loader').removeClass('d-none');
        $('#result').addClass('d-none');
        $.ajax({
            url: '<?php echo site_url() ?>pelamar/index/getDaftarLowongan',
            dataType: 'json',
            type: 'POST',
            success: function(data) {
                let tbody = '';
                console.log(data)
                if (data['status']) {
                    let iteration = 0;
                    data['isi'].forEach(function(isi) {
                        iteration += 1;

                        peminat = isi['peminat']+' Pelamar';
                        if (isi['peminat']==0) {
                            peminat = 'Belum ada pelamar';
                        }

                        detail = '<a href="<?php echo  base_url()?>pelamar/index/pelamar?id_lowongan='+isi['id_lowongan_pekerjaan']+'" class="btn btn-info"><i class="fa fa-eye"></i></a>';
                        tbody += availableData(iteration, isi['posisi'], isi['nama_pendidikan'],isi['jenis'],peminat, detail);
                    });

                    $('#totalRow').text(data['jumlah']);
                } else if (!data['status']) {
                    tbody = `
                            <tr class="odd">
                            <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                            </tr>`;
                } else {
                    tbody = `
                            <tr class="odd">
                                <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                                </tr>`;
                }

                $('.loader').addClass('d-none');
                $('#result').removeClass('d-none');

                $('#resultBody').html(null);
                $('#resultBody').append(tbody);

                $('#example').DataTable({
                    pageLength: 30,
                    lengthMenu: [
                        30,
                        60,
                        90,
                    ],
                });
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    };

    function availableData(no, posisi, min_pendidikan, jenis, peminat, detail) {
        return `
            <tr>
                <td>${no}</td>
                <td>${posisi}</td>
                <td>${min_pendidikan}</td>
                <td>${jenis}</td>
                <td>${peminat}</td>
                <td>${detail}</td>
            </tr>`;
    }
    $(document).ready(function(e) {
        loadTabelALumni();
    });
</script>