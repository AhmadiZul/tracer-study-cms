<link rel="stylesheet" href="<?= base_url('public/vendor/datatables/jquery.dataTables.min.css') ?>">
<a href="<?php echo base_url() ?>pelamar/index" class="btn btn-warning mb-3">Kembali</a>
<div class="card">
    <div class="card-body">
        <table id="example" class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Asal Kampus/Sekolah</th>
                    <th>Program Studi/Jurusan</th>
                    <th>Waktu Apply</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody id="resultBody">
            </tbody>
        </table>
    </div>
</div>


<div id="modal_edit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="status_form">
                <div class="modal-header">
                    <h5 class="modal-title">Status Pelamar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" name="id_apply" id="id_apply" value="" readonly>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nama Pelamar</label>
                        <input type="text" class="form-control" name="nama" id="nama" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Asal Sekolah/Kampus</label>
                        <input type="text" class="form-control" name="perguruan_tinggi" id="perguruan_tinggi" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Program Studi/Jurusan</label>
                        <input type="text" class="form-control" name="prodi" id="prodi" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Status Lamaran</label>
                        <select name="status" id="status" class="form-control select2">
                            <option value="Terkirim">Terkirim</option>
                            <option value="Lamaran Diterima">Lamaran Diterima</option>
                            <option value="Lamaran Direview">Lamaran Direview</option>
                            <option value="Proses Seleksi">Proses Seleksi</option>
                            <option value="Diterima Kerja">Diterima Kerja</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?= base_url('public/vendor/datatables/datatables.min.js'); ?>"></script>
<script type="text/javascript">
    var site_url = '<?php echo site_url() ?>pelamar/index/';
    var id_lowongan = '<?php echo $id_lowongan ?>';

    function loadTabelALumni() {
        $('.loader').removeClass('d-none');
        $('#result').addClass('d-none');
        $.ajax({
            url: '<?php echo site_url() ?>pelamar/index/getPelamar?id_lowongan=' + id_lowongan,
            dataType: 'json',
            type: 'POST',
            success: function(data) {
                let tbody = '';
                console.log(data)
                if (data['status']) {
                    let iteration = 0;
                    data['isi'].forEach(function(isi) {
                        iteration += 1;

                        detail = '<a href="<?php echo  base_url() ?>pelamar/index/detailPelamar?id_alumni=' + isi['id_alumni'] + '&id_lowongan='+id_lowongan+'" class="btn btn-info"><i class="fa fa-eye"></i></a>';
                        edit = '<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal_edit" data-nama="' + isi['nama'] + '" data-namapt="' + isi['nama_resmi'] + '" data-namaprodi="' + isi['nama_prodi'] + '" data-status="' + isi['status'] + '" data-id="' + isi['id_apply'] + '"><i class="fa fa-pencil"></i></button>';

                        tbody += availableData(iteration, isi['nama'], isi['nama_resmi'], isi['nama_prodi'], isi['applied_time'], isi['status'], detail + '' + edit);
                    });

                    $('#totalRow').text(data['jumlah']);
                } else if (!data['status']) {
                    tbody = `
                            <tr class="odd">
                            <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                            </tr>`;
                } else {
                    tbody = `
                            <tr class="odd">
                                <td valign="top" colspan="5" class="dataTables_empty">Data Tidak Ditemukan</td>
                                </tr>`;
                }

                $('.loader').addClass('d-none');
                $('#result').removeClass('d-none');

                $('#resultBody').html(null);
                $('#resultBody').append(tbody);

                $('#example').DataTable({
                    pageLength: 30,
                    lengthMenu: [
                        30,
                        60,
                        90,
                    ],
                });
            },
            error: function(jqXHR) {
                console.log(jqXHR);
            }
        });
    };

    function availableData(no, nama, asal, prodi, waktu, status, aksi) {
        return `
            <tr>
                <td>${no}</td>
                <td>${nama}</td>
                <td>${asal}</td>
                <td>${prodi}</td>
                <td>${waktu}</td>
                <td>${status}</td>
                <td>${aksi}</td>
            </tr>`;
    }
    $(document).ready(function(e) {
        loadTabelALumni();
    });

    $('#modal_edit').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget);
        var id_apply = button.data('id');
        var nama = button.data('nama');
        var namapt = button.data('namapt');
        var namaprodi = button.data('namaprodi');
        var status = button.data('status');

        var modal = $(this)
        modal.find('.modal-title').text('Status Pelamar ');
        modal.find('#id_apply').val(id_apply);
        modal.find('#nama').val(nama);
        modal.find('#perguruan_tinggi').val(namapt);
        modal.find('#prodi').val(namaprodi);
        modal.find('#status').val(status).trigger('change');
    })

    $('#status_form').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: site_url + "ubahStatus",
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            data: new FormData(this),
            beforeSend: function() {
                loading();
            },
            success: function(data) {
                dismiss_loading();
                swal({
                    title: data.message.title,
                    text: data.message.body,
                    type: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = site_url + 'pelamar?id_lowongan='+id_lowongan;
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                dismiss_loading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);

                                if (field == 'start_publish') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'end_publish') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'gaji_rate_atas') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                                if (field == 'gaji_rate_bawah') {
                                    $(`.invalid-feedback[for="${field}"]`).addClass('d-block');
                                }
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal({
                            title: response.message.title,
                            html: response.message.body,
                            type: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    });
</script>