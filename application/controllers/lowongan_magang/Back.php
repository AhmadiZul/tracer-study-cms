<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Back extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "lowongan_magang";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_lowongan_magang','lowongan');
    }

    public function index()
    {
        $this->data['title'] = 'Lowongan Magang';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Lowongan Magang');
        $crud->setTable('lowongan_magang');

        $crud->setRelation('id_instansi', 'mitra', 'nama', ['id_user' => $this->session->userdata('t_userId')]);
        $crud->setRelation('id_pendidikan', 'ref_pendidikan', 'nama_pendidikan');
        $crud->setRelation('lokasi_kerja', 'ref_kab_kota', 'nama_kab_kota');

        $crud->where([
            'lowongan_magang.start_publish >= ?' => $this->session->userdata("tracer_tahun").'-01-01',
            'lowongan_magang.start_publish <= ?' => $this->session->userdata("tracer_tahun").'-12-31',
            'lowongan_magang.is_active' => '1'
        ]);

        // Removes only the PDF button on export
        $crud->unsetExportPdf();
        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->unsetPrint();

        $column = ['id_instansi', 'posisi', 'lokasi_kerja', 'id_pendidikan', 'persyaratan'];
        $fields = ['id_instansi', 'posisi', 'lokasi_kerja', 'id_pendidikan', 'persyaratan'];
        $requireds = ['id_instansi', 'posisi', 'lokasi_kerja', 'id_pendidikan', 'persyaratan'];
        $fieldsDisplay = [
            'id_instansi' => 'Perusahaan',
            'posisi' => 'posisi',
            'lokasi_kerja' => 'Lokasi Kerja', 
            'id_pendidikan' => 'Minimal Pendidikan', 
            'persyaratan' => 'Persyaratan',
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);

        $crud->fieldType('jenis', 'dropdown_search', [
            'PART' => 'PART TIME',
            'FULL' => 'FULL TIME'
        ]);

        $crud->editFields(['jenis_survey']);

        $crud->callbackDelete(array($this, 'delete'));
        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('lowongan_magang/back/edit?key=' . $row->id_lowongan_magang);
        }, false);

        // $crud->setActionButton('Detail', 'fa fa-book', function ($row) {
        //     return site_url('alumni/dashboard/detail/' . $row->id_instansi);
        // }, false);

        $output = $crud->render();
        $this->_setOutput('lowongan_magang/back', $output);
    }

    public function add()
    {
        $this->data['title'] = 'Tambah Lowongan Magang';
        $this->data['is_home'] = false;
        $this->data['select_mitra'] = setMitra($this->session->userdata('t_userId'));
        $this->data['select_kabupaten'] = setKabupaten();
        $this->data['select_pendidikan'] = setPendidikan();
        $this->render('tambah');
    }

    public function edit()
    {
        $data = $this->input->get();
        if ($data['key'] == "" || empty($data['key'])) {
            redirect('lowongan_magang/back');
        }
        $this->data['title'] = 'Edit Lowongan Magang';
        $this->data['is_home'] = false;
        $this->data['id_lowongan'] = $id_lowongan = $data['key'];
        $this->data['lowongan'] = $lowongan = $this->lowongan->getLowongan($id_lowongan);
        $this->data['select_mitra'] = setMitra($this->session->userdata('t_userId'));
        $this->data['select_kabupaten'] = setKabupaten();
        $this->data['select_pendidikan'] = setPendidikan();
        $this->render('edit');
    }

    function _urlphoto($id_lowongan, $id_instansi)
    {
        $lowongan = $this->lowongan->getLowongan($id_lowongan);
        $id_mitra = $id_instansi;

        $id_lowongan = str_replace("-", "", $id_lowongan);
        $id_mitra = str_replace("-", "", $id_mitra);

        if (!file_exists('admin/public/uploads/mitra/')) {
            mkdir('admin/public/uploads/mitra/', 0755, true);
        }
        if (!file_exists('admin/public/uploads/mitra/' .$id_mitra)) {
            mkdir('admin/public/uploads/mitra/'.$id_mitra, 0755,  true);
        }
        if (!file_exists('admin/public/uploads/mitra/' .$id_mitra.'/lowongan_magang/')) {
            mkdir('admin/public/uploads/mitra/'.$id_mitra.'/lowongan_magang/', 0755,  true);
        }

        $oldUrlBukti = '';
        if (!empty($lowongan)) {
            $base_url = base_url();
            $oldUrlBukti = str_replace($base_url, '',$lowongan->url_poster);
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'admin/public/uploads/mitra/'.$id_mitra.'/lowongan_magang/'; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'poster' . $id_lowongan;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = base_url() . 'admin/public/uploads/mitra/'.$id_mitra.'/lowongan_magang/' . $file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function create()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_lowongan_kerja = getUUID();
        $input = $this->input->post();

        $data = array(
            'id_lowongan_magang' => $id_lowongan_kerja,
            'id_instansi' => $input['id_instansi'],
            'start_publish' => $input['start_publish'],
            'end_publish' => $input['end_publish'],
            'posisi' => $input['posisi'],
            'deskripsi' => $input['deskripsi'],
            'lokasi_kerja' => $input['lokasi_kerja'],
            'id_pendidikan' => $input['id_pendidikan'],
            'persyaratan' => $input['persyaratan'],
            'status' => $input['status'],
            'last_modified_by' => $this->session->userdata('t_userId')
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_lowongan_kerja, $input['id_instansi']);
            $data['url_poster'] = $urlPhoto['file_name'];
        }

        $simpan = $this->lowongan->simpan_lowongan($data);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan lowongan magang',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan lowongan magang',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function update()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $input = $this->input->post();
        $id_lowongan_magang = $input['id_lowongan'];

        $data = array(
            'id_instansi' => $input['id_instansi'],
            'start_publish' => $input['start_publish'],
            'end_publish' => $input['end_publish'],
            'posisi' => $input['posisi'],
            'deskripsi' => $input['deskripsi'],
            'lokasi_kerja' => $input['lokasi_kerja'],
            'id_pendidikan' => $input['id_pendidikan'],
            'persyaratan' => $input['persyaratan'],
            'status' => $input['status'],
            'last_modified_by' => $this->session->userdata('t_userId')
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_lowongan_magang, $input['id_instansi']);
            $data['url_poster'] = $urlPhoto['file_name'];
        }

        $ubah = $this->lowongan->ubah_lowongan($data, $id_lowongan_magang);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan lowongan magang',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan lowongan magang',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function delete($row)
    {
        return $this->db->update('lowongan_magang', array('is_active' => '0', 'last_modified_by' => $this->session->userdata('t_userId')), array('id_lowongan_magang' => $row->primaryKeyValue));
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('status', 'Status', 'trim');
        $this->form_validation->set_rules('id_instansi', 'Mitra', 'trim|required');
        $this->form_validation->set_rules('start_publish', 'Tanggal Mulai Publish', 'trim|required|callback_validDate');
        $this->form_validation->set_rules('end_publish', 'Tanggal Selesai Publish', 'trim|required|callback_validDate');
        $this->form_validation->set_rules('posisi', 'Posisi', 'trim|required|callback_whitespace|max_length[100]');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|callback_whitespace|callback_notOnlyNumber');
        $this->form_validation->set_rules('lokasi_kerja', 'Lokasi Kerja', 'trim|required');
        $this->form_validation->set_rules('id_pendidikan', 'Minimal Pendidikan', 'trim|required');
        $this->form_validation->set_rules('persyaratan', 'Persyaratan', 'trim|required|callback_whitespace|callback_notOnlyNumber');
        $this->form_validation->set_rules('url_photo', 'Upload Poster', 'trim');

        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');
        $this->form_validation->set_message('validDate', '{field} format tanggal belum seusai. Format : yyyy-mm-dd');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }
    public function notOnlyNumber($str)
    {
        if (ctype_digit($str)) {
            return false;
        }
        return true;
    }

    function validDate($date)
    {
        $date = DateTime::createFromFormat('Y-m-d', $date);
        $date_errors = DateTime::getLastErrors();
        if ($date_errors['warning_count'] + $date_errors['error_count'] > 0) {
            return false;
        }

        return true;
    }

}
