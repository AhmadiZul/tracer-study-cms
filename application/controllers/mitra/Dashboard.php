<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "mitra";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title'] = 'Dashboard';
        $this->data['is_home'] = true;
        $this->data['user'] = 'mitra';
        $this->render('dashboard/dashboard');
    }
}
