<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Index extends BaseController
{
    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "pelamar";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_lowongan_kerja', 'lowongan');
        $this->load->model('m_profil', 'profil');
    }

    public function index()
    {
        $this->data['title'] = '<i class="fa fa-user-check"></i> Kandidat';
        $this->data['is_home'] = false;
        $this->render('index');
    }

    public function getDaftarLowongan()
    {
        $mitra = $this->lowongan->getMitraByUser();
        $where = array(
            'id_instansi' => $mitra->id_instansi,
        );
        $pelamar = $this->lowongan->getLowonganPeminat($where);
        echo json_encode($pelamar);
    }

    public function pelamar()
    {
        $this->data['title'] = '<i class="fa fa-user-check"></i> Kandidat';
        $this->data['is_home'] = false;
        $this->data['id_lowongan'] = $this->input->get('id_lowongan');
        $this->render('daftarpelamar');
    }

    public function getPelamar()
    {
        $mitra = $this->lowongan->getMitraByUser();
        $where = array(
            'id_lowongan_pekerjaan' => $this->input->get('id_lowongan'),
        );
        $pelamar = $this->lowongan->getPelamarPerLowongan($where);

        foreach ($pelamar['isi'] as $key => $value) {
            $pelamar['isi'][$key]['applied_time'] = $this->tanggalindo->konversi_tgl_jam($value['applied_time']);
        }
        echo json_encode($pelamar);
    }

    public function ubahStatus()
    {
        $input = $this->input->post();
        $htmlCodeNumber = 201;
        $response = '';

        $this->db->trans_begin();
        $dbError = [];

        $id_apply = $input['id_apply'];

        $status = array(
            'status' => $input['status'],
        );

        $ubahStatus = $this->lowongan->ubah_apply($status, $id_apply);
        $dbError[] = $this->db->error();

        $datalog = array(
            'id_apply' => $id_apply,
            'activity' => $input['status'],
            'id_user' => $this->session->userdata("t_userId"),
            'ip_address' => $_SERVER['REMOTE_ADDR'],
        );
        $this->lowongan->simpan_log($datalog);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal mengubah status',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengubah status',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function detailPelamar()
    {
        $id_alumni = $this->input->get('id_alumni');
        $id_lowongan = $this->input->get('id_lowongan');
        $alumni = $this->profil->getAlumniById($id_alumni);

        if (!$alumni['url_photo'] || $alumni['url_photo'] == '') {
            $alumni['url_photo'] = base_url().'public/assets/img/unknown.webp';
        }

        if (isset($alumni['jenis_kelamin']) && $alumni['jenis_kelamin'] == 'P') {
            $alumni['jenis_kelamin'] = 'PEREMPUAN';
        } else {
            $alumni['jenis_kelamin'] = 'LAKI-LAKI';
        }

        $alumni['sosmed'] = (isset($alumni['sosmed']) ? json_decode($alumni['sosmed']) : (object) array('facebook' => '', 'twitter' => '', 'linkedin' => ''));

        if (isset($alumni['id_ref_status_alumni']) && ($alumni['id_ref_status_alumni'] == 1 || $alumni['id_ref_status_alumni'] == 3)) {
            $this->data['alumni_jobs'] = $this->profil->getAlumniJobs($alumni['id_alumni']);
        }

        if (isset($alumni['id_ref_status_alumni']) && $alumni['id_ref_status_alumni'] == 4) {
            $this->data['alumni_study'] = $this->profil->getAlumniStudy($alumni['id_alumni']);
        }

        $this->data['title'] = '<i class="fa fa-user-check"></i> Kandidat';
        $this->data['is_home'] = false;
        $this->data['alumni'] = $alumni;
        $this->data['id_lowongan'] = $id_lowongan;
        $this->render('detailpelamar');
    }

    public function getDaftarPrestasi()
    {
        $id_alumni = $this->input->get('id_alumni');
        $where = array(
            'p.id_alumni' => $id_alumni,
        );
        $prestasi = $this->profil->getPrestasi($where);
        
        foreach ($prestasi['isi'] as $key => $value) {
            $prestasi['isi'][$key]['type'] = pathinfo($value['url_sertifikat'], PATHINFO_EXTENSION);
        }
        echo json_encode($prestasi);
    }

    public function getDaftarOrganisasi()
    {
        $id_alumni = $this->input->get('id_alumni');
        $where = array(
            'p.id_alumni' => $id_alumni,
        );
        $organisasi = $this->profil->getOrganisasi($where);
        
        foreach ($organisasi['isi'] as $key => $value) {
            $organisasi['isi'][$key]['type'] = pathinfo($value['url_bukti'], PATHINFO_EXTENSION);
        }
        echo json_encode($organisasi);
    }

    public function getDaftarSkill()
    {
        $id_alumni = $this->input->get('id_alumni');
        $where = array(
            'p.id_alumni' => $id_alumni,
        );
        $skill = $this->profil->getSkill($where);
        
        foreach ($skill['isi'] as $key => $value) {
            $skill['isi'][$key]['type'] = pathinfo($value['url_sertifikat'], PATHINFO_EXTENSION);
        }
        echo json_encode($skill);
    }

    public function getDaftarPengalaman()
    {
        $id_alumni = $this->input->get('id_alumni');
        $where = array(
            'p.id_alumni' => $id_alumni,
        );
        $pengalaman = $this->profil->getPengalaman($where);
        
        foreach ($pengalaman['isi'] as $key => $value) {
            $pengalaman['isi'][$key]['type'] = pathinfo($value['url_bukti'], PATHINFO_EXTENSION);
        }
        echo json_encode($pengalaman);
    }
}
