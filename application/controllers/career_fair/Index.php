<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "career_fair";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_career', 'career');
    }

    public function index()
    {
        $this->data['title'] = 'Data Career Fair';
        $this->data['is_home'] = false;

        $referensi = $this->initData();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Career Fair');
        $crud->setTable('career_fair');
        $crud->setRelation('id_perguruan_tinggi_pelaksana', 'ref_perguruan_tinggi', 'nama_pendek');

        $crud->columns($referensi['columns']);
        $crud->fields($referensi['fields']);
        $crud->requiredFields($referensi['requireds']);
        $crud->displayAs($referensi['displays']);

        $crud->callbackColumn('waktu_awal_pendaftaran', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('tanggal_akhir_pendaftaran', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('waktu_awal_pelaksanaan', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('waktu_akhir_pelaksanaan', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->setActionButton('Detail', 'fa fa-th-list', function ($row) {
            return site_url('career_fair/index/detail?key=' . $row->id);
        }, false);
        $crud->callbackColumn('url_formulir_kepesertaan', function ($value, $row) {
            if ($value != '' || $value != null) {
                $ext = explode('.', $value);
                if ($ext[1] == 'pdf') {
                    return '<a href="' . base_url('public/uploads/job-fairs/' . $value) . '" class="btn btn-info btn-sm" target="_blank"><i class="fas fa-download"></i> Unduh</a>';
                } else {
                    return '<img src="' . base_url('public/uploads/job-fairs/' . $value) . '" class="img-fluid">';
                }
            } else {
                return 'formulir tidak tersedia';
            }
        });
        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->unsetDelete();
        $crud->unsetExportPdf();
        $crud->unsetPrint();

        $output = $crud->render();
        $this->_setOutput('career_fair/index/index', $output);
    }


    public function initData()
    {

        $columns = [
            'nama_job_fair',
            'tempat',
            'id_perguruan_tinggi_pelaksana',
            'waktu_awal_pendaftaran',
            'tanggal_akhir_pendaftaran',
            'waktu_awal_pelaksanaan',
            'waktu_akhir_pelaksanaan',
            'url_formulir_kepesertaan',
        ];

        $fields = [
            'nama_job_fair',
            'tempat',
            'id_perguruan_tinggi_pelaksana',
            'waktu_awal_pendaftaran',
            'tanggal_akhir_pendaftaran',
            'waktu_awal_pelaksanaan',
            'waktu_akhir_pelaksanaan',
            'url_formulir_kepesertaan',
        ];
        $requireds = [
            'nama_job_fair',
            'tempat',
            'id_perguruan_tinggi_pelaksana',
            'waktu_awal_pendaftaran',
            'tanggal_akhir_pendaftaran',
            'waktu_awal_pelaksanaan',
            'waktu_akhir_pelaksanaan',
        ];

        $displays = [
            'modified_by'                   => 'Aksi',
            'nama_job_fair'                 => 'Nama Job Fair',
            'tempat'                        => 'Tempat Pelaksanaan',
            'id_perguruan_tinggi_pelaksana' => 'Perguruan Tinggi Pelaksana',
            'waktu_awal_pendaftaran'        => 'Waktu Pendaftaran',
            'tanggal_akhir_pendaftaran'     => 'Batas Pendaftaran',
            'waktu_awal_pelaksanaan'        => 'Waktu Pelaksanaan',
            'waktu_akhir_pelaksanaan'       => 'Batas Pelaksanaan',
            'url_formulir_kepesertaan'      => 'Formulir Kepesertaan',
        ];

        $data['columns']    = $columns;
        $data['fields']     = $fields;
        $data['requireds']  = $requireds;
        $data['displays']   = $displays;
        return $data;
    }

    public function detail()
    {
        $id = $this->input->get('key');
        $this->data['data']     = $data = $this->career->edit($id);;
        $this->data['title']    = 'Detail Career Fair' . ($data['status'] == 201 ? ' ' . $data['data']->nama_job_fair : '');
        $this->data['is_home']  = false;
        $this->data['scripts']  = ['career_fair/js/detail.js'];
        $this->data['registered'] = $this->career->checkRegisterAvailable($id);
        $this->data['id_career'] = $id;
        $this->render('detail');
    }

    public function paket()
    {
        $result = $this->career->paket();
        responseJson($result['status'], $result);
    }

    public function daftar()
    {
        $return['status']  = 0;
        $return['message'] = '';


        $params = $this->input->post(null, true);
        $career = $this->career->edit($params['id_career_fair']);

        if ($career['status'] != 201) {
            $return['status']  = 500;
            $return['message'] = 'Data carrer fair tidak ditemukan';
            goto End;
        }

        $current = date('Y-m-d H:i:s');
        $convert_current = strtotime($current);
        $convert_awal = strtotime($career['data']->waktu_awal_pendaftaran);
        $convert_akhir = strtotime($career['data']->tanggal_akhir_pendaftaran);

        if ($convert_current > $convert_awal && $convert_current > $convert_akhir ) {
            $return['status']  = 500;
            $return['message'] = 'Waktu pendaftaran telah berakhir';
            goto End;
        }

        $data = filterFieldsOfTable('career_fair_pendaftar', $params);
        $data['waktu_pendaftaran'] = date('Y-m-d H:i:s');
        $data['is_approved'] = '0';
        $data['id_mitra'] = getMitraID();

        if ($_FILES['url_formulir']['name'] != null) {
            $url = uploadBerkas('url_formulir', 'public/uploads/career-fair', 'career-fair');

            if ($url['success']) {
                $data['url_formulir'] = $url['file_name'];
            } else {
                $data['url_formulir'] = null;
            }
        } else {
            $data['url_formulir'] = null;
        }
        $response = $this->career->daftar($data);
        if ($response['status'] == 201) {
            $return['status']  = 201;
            $return['message'] = $response['message'];
            goto End;
        }else{
            $return['status']  = 500;
            $return['message'] = $response['message'];
            goto End;
        }

        End:
        responseJson($return['status'], $return);
    }

    public function getDaftarMitra()
    {
        $return = [];

        $field = [
            'sSearch',
            'iSortCol_0',
            'sSortDir_0',
            'iDisplayStart',
            'iDisplayLength',
            'id_career',
        ];

        foreach ($field as $v) {
            $$v = $this->input->get_post($v);
        }

        $return = [
            "sEcho"                 => $this->input->post('sEcho'),
            "iTotalRecords"         => 0,
            "iTotalDisplayRecords"  => 0,
            "aaData"                => []
        ];

        $params = [
            'sSearch'   => $sSearch,
            'start'     => $iDisplayStart,
            'limit'     => $iDisplayLength,
            'id_career' => $id_career,
        ];

        $data = $this->career->mitra($params);
        if ($data['total'] > 0) {
            $return['iTotalRecords'] = $data['total'];
            $return['iTotalDisplayRecords'] = $return['iTotalRecords'];

            foreach ($data['rows'] as $k => $row) {

                $row['no']                  = '<p class="text-center">' . ($iDisplayStart + ($k + 1)) . '</p>';
                $row['nama']                = $row['nama'];
                $row['jenis']               = $row['jenis'] . ' / ' . $row['sektor'];
                $row['waktu_pendaftaran']   = $row['waktu_pendaftaran'];
                $row['paket_kerjasama']     = $row['paket'];
                $row['status']              = ($row['is_approved'] == '1' ? 'Diterima' : ($row['is_approved'] == '2' ? 'Tidak Diterima' : 'Belum Diverifikasi'));
                $row['formulir']            = ($row['url_formulir'] != null ? '<a href="' . base_url($row['url_formulir']) . '" class="btn btn-info btn-sm" target="_blank"><i class="fa fa-download"></i> Unduh</a>' : '');

                $kelola = '';
                if ($row['is_approved'] == 0) {
                    $kelola = '<button type="button" class="btn btn-primary btn-sm" id="btn-validasi" onclick=validasi(' . $row['id'] . ')><i class="fa fa-check"></i></button>';
                }

                if ($row['is_approved'] == 1) {
                    $kelola = '<span class="badge badge-success">Sudah diverifikasi</span>';
                }

                if ($row['is_approved'] == 2) {
                    $kelola = '<span class="badge badge-danger">Ditolak</span>';
                }
                $row['kelola'] = $kelola;


                $return['aaData'][] = $row;
            }
        }
        $this->db->flush_cache();
        echo json_encode($return);
    }
}
