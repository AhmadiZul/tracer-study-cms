<?php

use Dompdf\Dompdf;

if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "landing_app";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_tracer');
        $this->load->model('M_setting', 'setting');
    }

    public function index()
    {
        $idJenisSurvey = $this->input->get('id');
        if ($idJenisSurvey == "" || empty($idJenisSurvey)) {
            redirect();
        }

        $this->data['title'] = 'alumni';
        $this->data['is_full'] = true;
        $this->data['jenjang'] = setJenjangPendidikan();
        $this->data['fakultas'] = setFakultas();
        $this->data['prodi'] = setProdi();
        $this->data['tahun'] = setTahun();
        $this->data['jenis_survey'] = $this->m_tracer->get_jenis_survey(array('id' => $idJenisSurvey), true)['jenis_survey'];
        $this->data['jadwal'] = $this->m_tracer->get_jadwal_survey(array('id_ref_jenis_survey' => $idJenisSurvey), false);
        $this->data['copyright'] = $copyright = $this->setting->copyright();
        $this->data['about_us'] = $about_us = $this->setting->about_us();
        $this->data['logo_utama'] = $logo_utama = $this->setting->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->setting->logo_title();
        $this->data['warna_tema'] = $warna_tema = $this->setting->warna_tema();
        $this->data['perguruan_tinggi'] = $perguruan_tinggi = $this->setting->perguruan_tinggi();
        $this->data['header'] = $header = $this->setting->header();
        $this->data['footer'] = $footer = $this->setting->footer();

        $social_media = $this->setting->social_media();
        $sosial = json_decode($social_media->deskripsi);
        $this->data['social_media'] = $sosial;

        // ubah format tanggal
        foreach ($this->data['jadwal'] as $key => $value) {
            $value->start_date = $this->tanggalindo->konversi($value->start_date);
            $value->end_date = $this->tanggalindo->konversi($value->end_date);
        }

        $this->renderTo('tracer/index');
    }

    public function daftarAlumni()
    {
        $idSurvey = $this->input->get('id');
        if ($idSurvey == "" || empty($idSurvey)) {
            redirect();
        }
        $this->data['title'] = 'alumni';
        $this->data['instansi_name'] = 'Politeknik Pembangunan Pertanian Malang';
        $this->data['halaman'] = 'Tracer Study 2';
        $this->data['jadwal'] = $this->m_tracer->get_jadwal_survey(array('id_survey' => $idSurvey), true);

        $this->renderTo('tracer/daftaralumni');
    }

    public function cekAlumni()
    {
        $this->data['id_s'] = $idSurvey = $this->input->get('id');
        $alumni = $this->m_tracer->getTahunProdi($idSurvey);
        $this->data['title'] = 'alumni';
        $this->data['is_home'] = false;
        $this->data['alumni'] = $alumni;
        $this->data['jenis_survey'] = $jenis_survey = $this->m_tracer->get_jenis_survey(array('id_survey' => $idSurvey), true);
        $this->data['id_jenis'] = $jenis_survey['id'];
       
        $this->data['jadwal'] = $this->m_tracer->get_cek_survey(array('id_survey' => $idSurvey), true);
        $this->data['copyright'] = $copyright = $this->setting->copyright();
        $this->data['about_us'] = $about_us = $this->setting->about_us();
        $this->data['logo_utama'] = $logo_utama = $this->setting->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->setting->logo_title();
        $this->data['warna_tema'] = $this->setting->warna_tema();
        $this->data['perguruan_tinggi'] = $perguruan_tinggi = $this->setting->perguruan_tinggi();
        $this->data['header'] = $this->setting->header();
        $this->data['footer'] = $this->setting->footer();

        $social_media = $this->setting->social_media();
        $sosial = json_decode($social_media->deskripsi);
        $this->data['social_media'] = $sosial;
        $this->renderTo('tracer/cekalumni');
    }

    public function inputCek()
    {
        $dataI = $this->input->post();

        $this->form_validation->set_rules('nik', 'Nomor Induk Keluarga', 'trim|required');
        $this->form_validation->set_rules('nim', 'Nomor Induk Mahasiswa', 'trim|required');

        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');

        if ($this->form_validation->run() == TRUE) {

            $this->db->trans_begin();

            $nim = $this->input->post('nim');
            $nik = $this->input->post('nik');
            $tahun = $this->input->post('tahun');
            $prodi = $this->input->post('prodi');
            $fakultas = $this->input->post('fakultas');


            $ceknim = $this->m_tracer->cekUserSurvey($nim, $nik, $tahun, $prodi, $fakultas);


            if (empty($ceknim)) {
                $return['success'] = false;
                $return['text']   = 'Data yang anda masukan tidak ada.';
                echo json_encode($return);
                return;
            }

            $dataAlumni = $this->m_tracer->getUserInfoByNim($nim, $nik);


            $text = '';

            $kondisi = false;

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
            }

            $this->db->trans_complete();
            if ($text == '') {
                $return = array('success' => $kondisi, "kode" => trim($dataI['nim']), 'id_alumni' => $dataAlumni->id_alumni);
            } else {
                $return = array('success' => $kondisi, "kode" => trim($dataI['nim']), 'text' => $text);
            }
            echo json_encode($return);
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }

    public function cekProses()
    {
        $this->input->post('nim');
        $this->input->post('nik');
        redirect('tracer/index/daftaralumni');
    }


    public function kuesioner()
    {
        $data = $this->input->get();
        if ($data['s_id'] == "" || empty($data['s_id']) || $data['id_alumni'] == "" || empty($data['id_alumni'])) {
            redirect();
        }

        // $this->data['id_s'] = 
        $this->data['jadwal'] = $survey = $this->m_tracer->get_jadwal_survey(array('id_survey' => $data['s_id']), true);
        $this->data['alumni'] = $alumni = $this->getAlumni($data['id_alumni']);
        $this->data['warna_tema'] = $warna_tema = $this->setting->warna_tema();

        $this->data['title'] = 'alumni';
        $this->data['halaman'] = $survey['jenis_survey'];
        $this->data['status_alumni'] = setStatusAlumni();
        $this->data['agama'] = setAgama();
        $this->data['question_category'] = $question = $this->m_tracer->get_question();
        $this->data['pivotKuesioner'] = $pivotKuesioner = $this->m_tracer->getKuesionerPivot();
        $this->data['answer'] = $this->m_tracer->get_answer_option_by_kuesioner($question[0]['id_kuesioner']);
        $this->data['copyright'] = $copyright = $this->setting->copyright();
        $this->data['about_us'] = $about_us = $this->setting->about_us();
        $this->data['logo_utama'] = $logo_utama = $this->setting->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->setting->logo_title();
        $this->data['perguruan_tinggi'] = $perguruan_tinggi = $this->setting->perguruan_tinggi();
        $this->data['header'] = $header= $this->setting->header();
        $this->data['footer'] = $footer= $this->setting->footer();

        $social_media = $this->setting->social_media();
        $sosial = json_decode($social_media->deskripsi);
        $this->data['social_media'] = $sosial;

        $nim = $this->input->post('nim');
        $nik = $this->input->post('nik');

        $dataAlumni = $this->m_tracer->getUserInfoByNim($nim, $nik);
        $checkIsi = $this->m_tracer->checkAlumniAnswer(['id_survey' => $this->input->get('s_id'), 'id_alumni' => $this->input->get('id_alumni')]);
        if ($checkIsi > 0) {
            $this->data['cek'] = 1;
        }

        $this->renderTo('tracer/kuesioner');
    }

    public function getDaftarAlumni()
    {
        $data = $this->input->post();
        $where = array(
            'a.id_prodi' => $data['id_prodi'],
            'a.tahun_lulus' => $data['tahun_lulus'],
        );
        $alumni = $this->m_tracer->get_daftar_alumni($where, $data['id_survey']);
        echo json_encode($alumni);
    }

    public function getAlumni($id_alumni)
    {
        $alumni = $this->m_tracer->get_alumni_by_id_alumni($id_alumni);

        if ($alumni->id_ref_status_alumni == 1 || $alumni->id_ref_status_alumni == 3) {
            $jobs_study = $this->m_tracer->get_jobs_by_id_alumni($id_alumni, $alumni->id_ref_status_alumni);
            $alumni->id_jobs_study = $jobs_study->id_job;
            $alumni->nama_instansi = $jobs_study->nama_instansi;
            $alumni->tgl_mulai = $jobs_study->tgl_mulai;
            $alumni->jabatan = $jobs_study->jabatan;
            $alumni->kode_prov_instansi = $jobs_study->kode_provinsi;
            $alumni->kode_kab_kota_instansi = $jobs_study->kode_kab_kota;
            $alumni->kode_kec_instansi = $jobs_study->kode_kecamatan;
            $alumni->alamat_instansi = $jobs_study->alamat;
        } elseif ($alumni->id_ref_status_alumni == 4) {
            $jobs_study = $this->m_tracer->get_study_by_id_alumni($id_alumni);
            $alumni->id_jobs_study = $jobs_study->id_study;
            $alumni->nama_instansi = $jobs_study->nama_perguruan_tinggi;
            $alumni->tgl_mulai = $jobs_study->tgl_mulai;
            $alumni->prodi = $jobs_study->prodi;
            $alumni->kode_prov_instansi = $jobs_study->kode_provinsi;
            $alumni->kode_kab_kota_instansi = $jobs_study->kode_kab_kota;
            $alumni->kode_kec_instansi = $jobs_study->kode_kecamatan;
        }

        return $alumni;
    }

    public function getKabKota()
    {
        $kode_provinsi = $this->input->get('kode_provinsi');
        $kabKota = setKabupaten($kode_provinsi);
        echo json_encode($kabKota);
    }

    public function getKecamatan()
    {
        $kode_kab_kota = $this->input->get('kode_kab_kota');
        $kecamatan = setKecamatan($kode_kab_kota);
        echo json_encode($kecamatan);
    }

    public function getProdi()
    {
        $kode_prodi = $this->input->post('kode_prodi');
        $prodi = setProdi($kode_prodi);
        echo json_encode($prodi);
    }

    public function getAllKuesionerByCategory()
    {
        $id_status = $this->input->post('id_status_alumni');
        $dropdown = $this->m_tracer->get_answer_option_by_kuesioner('id_opsi');
        $question = '';
        $kuesioner = $this->m_tracer->get_kuesioner_by_status($id_status);

        // echo "<pre>";     
        // print_r($kuesioner);
        // echo "</pre>";
        // die;

        $no = 1;
        foreach ($kuesioner as $key => $value) {

            if ($value['question_type'] == 8) {
                // pertanyaan
                $question .= '<div class="card col-md-12">
                            <div class="card-body">
                            <sup class="text-danger">' . $value['question_code'] . '</sup>
                            <b class="question">' . $value['question'] . '</b> ' . ($value['question_code'] == 'f510' ? '' : '<b class="text-danger">*</b>') . '
                            <input class="form-control d-none" name="question[type][]" value="' . $value['question_type'] . '">
                            <input class="form-control d-none" name="question[id_kuesioner][]" value="' . $value['id_kuesioner'] . '">
                            <input class="form-control d-none" name="question[question_code][]" value="' . $value['question_code'] . '">
                            <input class="form-control d-none" name="question[answer_length][]" value="' . count($value['answer']) . '">
                            <input class="form-control d-none" name="question[others][]" value="' . $value['others'] . '">
                            </div>
                        </div>';
            } else {
                // pertanyaan
                $question .= '<div class="card col-md-6 my-3">
                                <div class="card-body">
                                <sup class="text-danger">' . $value['question_code'] . '</sup>
                                <b class="question">' . $value['question'] . '</b> ' . ($value['question_code'] == 'f510' ? '' : '<b class="text-danger">*</b>') . '
                                <input class="form-control d-none" name="question[type][]" value="' . $value['question_type'] . '">
                                <input class="form-control d-none" name="question[id_kuesioner][]" value="' . $value['id_kuesioner'] . '">
                                <input class="form-control d-none" name="question[question_code][]" value="' . $value['question_code'] . '">
                                <input class="form-control d-none" name="question[answer_length][]" value="' . count($value['answer']) . '">
                                <input class="form-control d-none" name="question[others][]" value="' . $value['others'] . '">
                                </div>
                            </div>';
            }

            // jawaban
            if ($value['question_type'] != 8) {
                $question .= '<div class="card col-md-6 my-3"><div class="card-body">';
            }

            if ($value['question_type'] == 1) {
                $rule = '';
                $type = 'text';
                if ($value['answer_rules']  == 'integer') {
                    $rule = 'number-only';
                }
                if ($value['answer_rules']  == 'is_date') {
                    $rule = 'datepicker2';
                }
                if ($value['answer_rules'] == 'valid_email') {
                    $type = 'email';
                }
                $question .= '<input type="' . $type . '" class="form-control ' . $rule . '" id="inpt-' . $no . '" name="inpt-' . $no . '">';
            } else if ($value['question_type'] == 2) {
                $question .= '<textarea class="form-control" id="inpt-' . $no . '" name="inpt-' . $no . '"></textarea>';
            } else if ($value['question_type'] == 3) {
                for ($i = 0; $i < count($value['answer']); $i++) {
                    if ($value['answer'][$i]['isian'] == 'isian[on]') {
                        $rule = '';
                        $type = 'text';
                        if ($value['answer'][$i]['answer_rules']  == 'integer') {
                            $rule = 'number-only';
                        }
                        if ($value['answer'][$i]['answer_rules']  == 'is_date') {
                            $rule = 'datepicker2';
                        }
                        if ($value['answer'][$i]['answer_rules'] == 'valid_email') {
                            $type = 'email';
                        }
                        $question .= '<div class="custom-control custom-radio my-2">
                                        <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['id_opsi'] . '" name="inpt-' . $no . '" value="' . $value['answer'][$i]['id_opsi'] . '">
                                        <label class="custom-control-label" for="' . $value['answer'][$i]['id_opsi'] . '">' . $value['answer'][$i]['opsi'] . '</label>
                                        <input type="' . $type . '" class="form-control ' . $rule . '" id="inpt-isian-' . $no . '" name="inpt-isian-' . $value['answer'][$i]['id_opsi'] . '">
                                    </div>';
                    } else {
                        $question .= '<div class="custom-control custom-radio my-2">
                                        <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['id_opsi'] . '" name="inpt-' . $no . '" value="' . $value['answer'][$i]['id_opsi'] . '">
                                        <label class="custom-control-label" for="' . $value['answer'][$i]['id_opsi'] . '">' . $value['answer'][$i]['opsi'] . '</label>
                                    </div>';
                    }
                }
            } else if ($value['question_type'] == 4) {
                for ($i = 0; $i < count($value['answer']); $i++) {
                    if ($value['answer'][$i]['isian'] == 'isian[on]') {
                        $rule = '';
                        $type = 'text';
                        if ($value['answer'][$i]['answer_rules']  == 'integer') {
                            $rule = 'number-only';
                        }
                        if ($value['answer'][$i]['answer_rules']  == 'is_date') {
                            $rule = 'datepicker2';
                        }
                        if ($value['answer'][$i]['answer_rules'] == 'valid_email') {
                            $type = 'email';
                        }
                        $question .= '<div class="custom-control custom-checkbox my-2">
                                        <input type="checkbox" class="custom-control-input" id="' . $value['answer'][$i]['id_opsi'] . '" name="inpt-' . $no . '[]" value="' . $value['answer'][$i]['id_opsi'] . '">
                                        <label class="custom-control-label" for="' . $value['answer'][$i]['id_opsi'] . '">' . $value['answer'][$i]['opsi'] . '</label>
                                        <input type="' . $type . '" class="form-control ' . $rule . '" id="inpt-isian-' . $no . '" name="inpt-isian-' . $value['answer'][$i]['id_opsi'] . '">
                                    </div>';
                    } else {
                        $question .= '<div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="' . $value['answer'][$i]['id_opsi'] . '" name="inpt-' . $no . '[]" value="' . $value['answer'][$i]['id_opsi'] . '">
                                    <label class="custom-control-label" for="' . $value['answer'][$i]['id_opsi'] . '">' . $value['answer'][$i]['opsi'] . '</label>
                                 </div>';
                    }
                }

                // if ($value['others'] == 'lainnya[on]') {
                //     $question .= '<div class="custom-control custom-checkbox">
                //                 <input class="custom-control-input" type="checkbox" id="' . $value['id_kuesioner'] . 'lainnya[on]" name="inpt-' . $no . '[]" value="lainnya[on]">
                //                 <label class="custom-control-label" for="' . $value['id_kuesioner'] . 'lainnya[on]">Lainnya : </label>
                //                 <input type="text" class="form-control" id="inpt-lainnya-' . $no . '" name="inpt-lainnya-' . $no . '">
                //              </div>';
                // }
            } else if ($value['question_type'] == 5) {
                $huruf = 'A';
                $question .= '<table class="table table-responsive"><tbody>';

                if ($value['question_code'] != 'f2') {
                    $question .= '<tr>
                                    <td></td>
                                    <td colspan="3" class="text-center">Sangat Rendah</td>
                                    <td colspan="2" class="text-center">Sangat Tinggi</td>
                                 <tr>';
                }

                for ($i = 0; $i < count($value['answer']); $i++) {
                    if ($value['question_code'] == 'f2') {
                        $question .= '<tr>
                                        <td rowspan="5"><label class="form-check-label">' . $huruf . '. ' . $value['answer'][$i]['pertanyaan'] . '</label>&nbsp<sup class="text-danger">' . $value['answer'][$i]['likert_code'] . '</sup><input type="text" class="form-control d-none" name="inpt-' . $no . '[]" value="' . $value['answer'][$i]['id_likert'] . '"><div class="invalid-feedback d-none" for="' . $no . '_' . $huruf . '">Skala likert dengan kode ' . $value['answer'][$i]['likert_code'] . ' belum diisi</div></td>
                                        <td>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_5" name="' . $no . '_' . $huruf . '" value="5">
                                                <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_5">Sangat Besar</label>
                                            </div>
                                        </td>
                                     </tr>';
                        $question .= '<tr>
                                        <td>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_4" name="' . $no . '_' . $huruf . '" value="4">
                                                <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_4">Besar</label>
                                            </div>
                                        </td>
                                     </tr>
                                     <tr>
                                        <td>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_3" name="' . $no . '_' . $huruf . '" value="3">
                                                <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_3">Cukup Besar</label>
                                            </div>
                                        </td>
                                     </tr>
                                     <tr>
                                        <td>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_2" name="' . $no . '_' . $huruf . '" value="2">
                                                <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_2">Kurang</label>
                                            </div>
                                        </td>
                                     </tr>
                                     <tr>
                                        <td>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_1" name="' . $no . '_' . $huruf . '" value="1">
                                                <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_1">Tidak Sama Sekali</label>
                                            </div>
                                        </td>
                                     </tr>';
                    } else {
                        $question .= '<tr>
                                        <td><label class="form-check-label">' . $huruf . '. ' . $value['answer'][$i]['pertanyaan'] . '</label>&nbsp<sup class="text-danger">' . $value['answer'][$i]['likert_code'] . '</sup><input type="text" class="form-control d-none" name="inpt-' . $no . '[]" value="' . $value['answer'][$i]['id_likert'] . '"><div class="invalid-feedback d-none" for="' . $no . '_' . $huruf . '">Skala likert dengan kode ' . $value['answer'][$i]['likert_code'] . ' belum diisi</div></td>
                                        <td><div class="custom-control custom-radio col text-center my-2">
                                            <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_1" name="' . $no . '_' . $huruf . '" value="1"><br>
                                            <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_1">1</label>
                                        </div></td>
                                        <td><div class="custom-control custom-radio col text-center my-2">
                                            <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . $huruf . '_2" name="' . $no . '_' . $huruf . '" value="2"><br>
                                            <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . $huruf . '_2">2</label>
                                        </div></td>
                                        <td><div class="custom-control custom-radio col text-center my-2">
                                            <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_3" name="' . $no . '_' . $huruf . '" value="3"><br>
                                            <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_3">3</label>
                                        </div></td>
                                        <td><div class="custom-control custom-radio col text-center my-2">
                                            <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_4" name="' . $no . '_' . $huruf . '" value="4"><br>
                                            <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_4">4</label>
                                        </div></td>
                                        <td><div class="custom-control custom-radio col text-center my-2">
                                            <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_5" name="' . $no . '_' . $huruf . '" value="5"><br>
                                            <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_5">5</label>
                                        </div></td>
                                     </tr>';
                    }
                    $huruf++;
                }
                $question .= '</tbody></table>';
            } else if ($value['question_type'] == 6) {
                $huruf = 'A';
                $question .= '<table class="table"><tbody>';

                for ($i = 0; $i < count($value['answer']); $i++) {
                    $rule = '';
                    $input_type = 'text';
                    if ($value['answer'][$i]['answer_rules']  == 'integer') {
                        $rule = 'number-only';
                    }
                    if ($value['answer'][$i]['answer_rules']  == 'is_date') {
                        $rule = 'datepicker2';
                    }
                    if ($value['answer'][$i]['answer_rules']  == 'valid_email') {
                        $input_type = 'email';
                    }

                    $question .= '<tr>
                                    <td>' . $huruf . '. ' . $value['answer'][$i]['pertanyaan'] .
                        '<sup class="text-danger">' . $value['answer'][$i]['sub_code'] . '</sup>
                                        <input type="text" class="form-control d-none" name="inpt-' . $no . '[]" value="' . $value['answer'][$i]['id_sub'] . '">
                                        <div class="invalid-feedback d-none" for="inpt-' . $no . '-' . $huruf . '">Sub pertanyaan dengan kode ' . $value['answer'][$i]['sub_code'] . ' belum diisi</div>
                                    </td>
                                    <td>
                                        <input type="' . $input_type . '" name="inpt-' . $no . '-' . $huruf . '" class="form-control ' . $rule . '">
                                    </td>
                                </tr>';

                    $huruf++;
                }

                $question .= '</tbody></table>';
            } else if ($value['question_type'] == 7) {

                if ($value['others'] == 'provinsi') {
                    $provinsi = setPronvisi();
                    $option = '';
                    foreach ($provinsi as $key2 => $value2) {
                        $option .= '<option value="' . $key2 . '">' . $value2 . '</option>';
                    }
                    $question .= '<input type="text" class="form-control d-none" name="inpt-' . $no . '" value="isi">';
                    $question .= '
                             <select name="kuesioner_kode_provinsi_' . $no . '" id="kuesioner_kode_provinsi" class="form-control select2">
                                <option value="">- Pilih Provinsi -</option>
                                ' . $option . '
                             </select>
                             <div class="invalid-feedback" for="kuesioner_kode_provinsi_' . $no . '"></div>';
                } elseif ($value['others'] == 'kabupaten') {
                    $question .= '<input type="text" class="form-control d-none" name="inpt-' . $no . '" value="isi">';
                    $question .= '
                             <select name="kuesioner_kode_kabupaten_' . $no . '" id="kuesioner_kode_kabupaten" class="select2 form-control">
                                <option value="">- Pilih Kabupaten/Kota -</option>
                             </select>
                             <div class="invalid-feedback" for="kuesioner_kode_kabupaten_' . $no . '"></div>';
                }
            } else if ($value['question_type'] == 9) {
                $option = '';
                for ($i = 0; $i < count($value['answer']); $i++) {
                    $option .= '<option value="' . $value['answer'][$i]['id_opsi'] . '">' . $value['answer'][$i]['opsi'] . '</option>';
                }
                $question .= '
                             <select name="inpt-' . $no . '" id="inpt-' . $no . '" class="select2 form-control">
                                <option value="">- Pilih -</option>
                                ' . $option . '
                             </select>';
            }

            if ($value['question_type'] != 8) {
                $question .= '<div class="invalid-feedback" for="inpt-' . $no . '"></div></div></div>';
            }

            // garis batas pertanyaan
            $question .= '<hr style="height:2px;">';

            $no++;

            if (isset($value['subquestion'])) {
                $sub = $this->getSubquestion($value['subquestion'], $no);
                $question .= $sub['question'];
                $no = $sub['no'];
            }
        }
        echo json_encode(array('success' => true, "question" => $question));
    }


    public function getSubquestion($subquestion, $number)
    {
        $question = '';
        $no = $number;
        $return = [];
        foreach ($subquestion as $key => $value) {

            // pertanyaan
            $question .= '<div class="card col-md-6 my-3">
            <div class="card-body">
            <sup class="text-danger">' . $value['question_code'] . '</sup>
            <b class="question">' . $value['question'] . '</b> ' . ($value['question_code'] == 'f510' ? '' : '<b class="text-danger">*</b>') . '
            <input class="form-control d-none" name="question[type][]" value="' . $value['question_type'] . '">
            <input class="form-control d-none" name="question[id_kuesioner][]" value="' . $value['id_kuesioner'] . '">
            <input class="form-control d-none" name="question[question_code][]" value="' . $value['question_code'] . '">
            <input class="form-control d-none" name="question[answer_length][]" value="' . count($value['answer']) . '">
            <input class="form-control d-none" name="question[others][]" value="' . $value['others'] . '">
            </div>
         </div>';

            // jawaban
            $question .= '<div class="card col-md-6 my-3"><div class="card-body">';
            if ($value['question_type'] == 1) {
                $rule = '';
                $type = 'text';
                if ($value['answer_rules']  == 'integer') {
                    $rule = 'number-only';
                }
                if ($value['answer_rules']  == 'is_date') {
                    $rule = 'datepicker2';
                }
                if ($value['answer_rules'] == 'valid_email') {
                    $type = 'email';
                }
                $question .= '<input type="' . $type . '" class="form-control ' . $rule . '" id="inpt-' . $no . '" name="inpt-' . $no . '">';
            } else if ($value['question_type'] == 2) {
                $question .= '<textarea class="form-control" id="inpt-' . $no . '" name="inpt-' . $no . '"></textarea>';
            } else if ($value['question_type'] == 3) {
                for ($i = 0; $i < count($value['answer']); $i++) {
                    if ($value['answer'][$i]['isian'] == 'isian[on]') {
                        $rule = '';
                        $type = 'text';
                        if ($value['answer'][$i]['answer_rules']  == 'integer') {
                            $rule = 'number-only';
                        }
                        if ($value['answer'][$i]['answer_rules']  == 'is_date') {
                            $rule = 'datepicker2';
                        }
                        if ($value['answer'][$i]['answer_rules'] == 'valid_email') {
                            $type = 'email';
                        }
                        $question .= '<div class="custom-control custom-radio my-2">
                        <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['id_opsi'] . '" name="inpt-' . $no . '" value="' . $value['answer'][$i]['id_opsi'] . '">
                        <label class="custom-control-label" for="' . $value['answer'][$i]['id_opsi'] . '">' . $value['answer'][$i]['opsi'] . '</label>
                        <input type="' . $type . '" class="form-control ' . $rule . '" id="inpt-isian-' . $no . '" name="inpt-isian-' . $value['answer'][$i]['id_opsi'] . '">
                    </div>';
                    } else {
                        $question .= '<div class="custom-control custom-radio my-2">
                        <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['id_opsi'] . '" name="inpt-' . $no . '" value="' . $value['answer'][$i]['id_opsi'] . '">
                        <label class="custom-control-label" for="' . $value['answer'][$i]['id_opsi'] . '">' . $value['answer'][$i]['opsi'] . '</label>
                    </div>';
                    }
                }
            } else if ($value['question_type'] == 4) {
                for ($i = 0; $i < count($value['answer']); $i++) {
                    if ($value['answer'][$i]['isian'] == 'isian[on]') {
                        $rule = '';
                        $type = 'text';
                        if ($value['answer'][$i]['answer_rules']  == 'integer') {
                            $rule = 'number-only';
                        }
                        if ($value['answer'][$i]['answer_rules']  == 'is_date') {
                            $rule = 'datepicker2';
                        }
                        if ($value['answer'][$i]['answer_rules'] == 'valid_email') {
                            $type = 'email';
                        }
                        $question .= '<div class="custom-control custom-checkbox my-2">
                        <input type="checkbox" class="custom-control-input" id="' . $value['answer'][$i]['id_opsi'] . '" name="inpt-' . $no . '[]" value="' . $value['answer'][$i]['id_opsi'] . '">
                        <label class="custom-control-label" for="' . $value['answer'][$i]['id_opsi'] . '">' . $value['answer'][$i]['opsi'] . '</label>
                        <input type="' . $type . '" class="form-control ' . $rule . '" id="inpt-isian-' . $no . '" name="inpt-isian-' . $value['answer'][$i]['id_opsi'] . '">
                    </div>';
                    } else {
                        $question .= '<div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="' . $value['answer'][$i]['id_opsi'] . '" name="inpt-' . $no . '[]" value="' . $value['answer'][$i]['id_opsi'] . '">
                    <label class="custom-control-label" for="' . $value['answer'][$i]['id_opsi'] . '">' . $value['answer'][$i]['opsi'] . '</label>
                 </div>';
                    }
                }

                // if ($value['others'] == 'lainnya[on]') {
                //     $question .= '<div class="custom-control custom-checkbox">
                //                 <input class="custom-control-input" type="checkbox" id="' . $value['id_kuesioner'] . 'lainnya[on]" name="inpt-' . $no . '[]" value="lainnya[on]">
                //                 <label class="custom-control-label" for="' . $value['id_kuesioner'] . 'lainnya[on]">Lainnya : </label>
                //                 <input type="text" class="form-control" id="inpt-lainnya-' . $no . '" name="inpt-lainnya-' . $no . '">
                //              </div>';
                // }
            } else if ($value['question_type'] == 5) {
                $huruf = 'A';
                $question .= '<table class="table table-responsive"><tbody>';

                if ($value['question_code'] != 'f2') {
                    $question .= '<tr>
                    <td></td>
                    <td colspan="3" class="text-center">Sangat Rendah</td>
                    <td colspan="2" class="text-center">Sangat Tinggi</td>
                 <tr>';
                }

                for ($i = 0; $i < count($value['answer']); $i++) {
                    if ($value['question_code'] == 'f2') {
                        $question .= '<tr>
                        <td rowspan="5"><label class="form-check-label">' . $huruf . '. ' . $value['answer'][$i]['pertanyaan'] . '</label>&nbsp<sup class="text-danger">' . $value['answer'][$i]['likert_code'] . '</sup><input type="text" class="form-control d-none" name="inpt-' . $no . '[]" value="' . $value['answer'][$i]['id_likert'] . '"><div class="invalid-feedback d-none" for="' . $no . '_' . $huruf . '">Skala likert dengan kode ' . $value['answer'][$i]['likert_code'] . ' belum diisi</div></td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_5" name="' . $no . '_' . $huruf . '" value="5">
                                <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_5">Sangat Besar</label>
                            </div>
                        </td>
                     </tr>';
                        $question .= '<tr>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_4" name="' . $no . '_' . $huruf . '" value="4">
                                <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_4">Besar</label>
                            </div>
                        </td>
                     </tr>
                     <tr>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_3" name="' . $no . '_' . $huruf . '" value="3">
                                <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_3">Cukup Besar</label>
                            </div>
                        </td>
                     </tr>
                     <tr>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_2" name="' . $no . '_' . $huruf . '" value="2">
                                <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_2">Kurang</label>
                            </div>
                        </td>
                     </tr>
                     <tr>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_1" name="' . $no . '_' . $huruf . '" value="1">
                                <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_1">Tidak Sama Sekali</label>
                            </div>
                        </td>
                     </tr>';
                    } else {
                        $question .= '<tr>
                        <td><label class="form-check-label">' . $huruf . '. ' . $value['answer'][$i]['pertanyaan'] . '</label>&nbsp<sup class="text-danger">' . $value['answer'][$i]['likert_code'] . '</sup><input type="text" class="form-control d-none" name="inpt-' . $no . '[]" value="' . $value['answer'][$i]['id_likert'] . '"><div class="invalid-feedback d-none" for="' . $no . '_' . $huruf . '">Skala likert dengan kode ' . $value['answer'][$i]['likert_code'] . ' belum diisi</div></td>
                        <td><div class="custom-control custom-radio col text-center my-2">
                            <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_1" name="' . $no . '_' . $huruf . '" value="1"><br>
                            <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_1">1</label>
                        </div></td>
                        <td><div class="custom-control custom-radio col text-center my-2">
                            <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . $huruf . '_2" name="' . $no . '_' . $huruf . '" value="2"><br>
                            <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . $huruf . '_2">2</label>
                        </div></td>
                        <td><div class="custom-control custom-radio col text-center my-2">
                            <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_3" name="' . $no . '_' . $huruf . '" value="3"><br>
                            <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_3">3</label>
                        </div></td>
                        <td><div class="custom-control custom-radio col text-center my-2">
                            <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_4" name="' . $no . '_' . $huruf . '" value="4"><br>
                            <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_4">4</label>
                        </div></td>
                        <td><div class="custom-control custom-radio col text-center my-2">
                            <input type="radio" class="custom-control-input" id="' . $value['answer'][$i]['likert_code'] . '_5" name="' . $no . '_' . $huruf . '" value="5"><br>
                            <label class="custom-control-label" for="' . $value['answer'][$i]['likert_code'] . '_5">5</label>
                        </div></td>
                     </tr>';
                    }
                    $huruf++;
                }
                $question .= '</tbody></table>';
            } else if ($value['question_type'] == 6) {
                $huruf = 'A';
                $question .= '<table class="table"><tbody>';

                for ($i = 0; $i < count($value['answer']); $i++) {
                    $rule = '';
                    $input_type = 'text';
                    if ($value['answer'][$i]['answer_rules']  == 'integer') {
                        $rule = 'number-only';
                    }
                    if ($value['answer'][$i]['answer_rules']  == 'is_date') {
                        $rule = 'datepicker2';
                    }
                    if ($value['answer'][$i]['answer_rules']  == 'valid_email') {
                        $input_type = 'email';
                    }

                    $question .= '<tr>
                    <td>' . $huruf . '. ' . $value['answer'][$i]['pertanyaan'] .
                        '<sup class="text-danger">' . $value['answer'][$i]['sub_code'] . '</sup>
                        <input type="text" class="form-control d-none" name="inpt-' . $no . '[]" value="' . $value['answer'][$i]['id_sub'] . '">
                        <div class="invalid-feedback d-none" for="inpt-' . $no . '-' . $huruf . '">Sub pertanyaan dengan kode ' . $value['answer'][$i]['sub_code'] . ' belum diisi</div>
                    </td>
                    <td>
                        <input type="' . $input_type . '" name="inpt-' . $no . '-' . $huruf . '" class="form-control ' . $rule . '">
                    </td>
                </tr>';

                    $huruf++;
                }

                $question .= '</tbody></table>';
            } else if ($value['question_type'] == 7) {
                if ($value['others'] == 'provinsi') {
                    $provinsi = setPronvisi();
                    $option = '';
                    foreach ($provinsi as $key => $value) {
                        $option .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                    $question .= '<input type="text" class="form-control d-none" name="inpt-' . $no . '" value="isi">';
                    $question .= '
             <select name="kuesioner_kode_provinsi_' . $no . '" id="kuesioner_kode_provinsi" class="form-control select2">
                <option value="">- Pilih Provinsi -</option>
                ' . $option . '
             </select>
             <div class="invalid-feedback" for="kuesioner_kode_provinsi_' . $no . '"></div>';
                } elseif ($value['others'] == 'kabupaten') {
                    $question .= '<input type="text" class="form-control d-none" name="inpt-' . $no . '" value="isi">';
                    $question .= '
             <select name="kuesioner_kode_kabupaten_' . $no . '" id="kuesioner_kode_kabupaten" class="select2 form-control">
                <option value="">- Pilih Kabupaten/Kota -</option>
             </select>
             <div class="invalid-feedback" for="kuesioner_kode_kabupaten_' . $no . '"></div>';
                }
            } else if ($value['question_type'] == 9) {
                $option = '';
                for ($i = 0; $i < count($value['answer']); $i++) {
                    $option .= '<option value="' . $value['answer'][$i]['id_opsi'] . '">' . $value['answer'][$i]['opsi'] . '</option>';
                }
                $question .= '
             <select name="inpt-' . $no . '" id="inpt-' . $no . '" class="select2 form-control">
                <option value="">- Pilih -</option>
                ' . $option . '
             </select>';
            }

            $question .= '<div class="invalid-feedback" for="inpt-' . $no . '"></div></div></div>';

            // garis batas pertanyaan
            $question .= '<hr style="height:2px;">';
            $no++;
        }
        $return = [
            'question' => $question,
            'no' => $no
        ];

        return $return;
    }
    public function simpan_survey()
    {
        $data = $this->input->post();
        $answer = array();
        $offeredAnswer = array();

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation($data);

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        // Get Answer
        $this->db->trans_begin();
        $dbError = [];

        $id_answer = getUUID();

        $answer = array(
            'id_answer' => $id_answer,
            'id_survey' => $data['id_survey'],
            'id_alumni' => $data['id_alumni'],
            'id_status_alumni' => $data['inpt-0']
        );

        $this->m_tracer->simpan_answer($answer);
        $dbError[] = $this->db->error();

        $offeredAnswer[0]['id_kuesioner'] = $data['question_id-0'];
        $offeredAnswer[0]['id_answer'] = $id_answer;
        $offeredAnswer[0]['text'] = $data['inpt-0'];
        if (!empty($data['question'])) {
            foreach ($data['question']['id_kuesioner'] as $key => $value) {
                if ($data['question']['type'][$key] == 8) {
                    continue;
                }
                $offeredAnswer[($key + 1)]['id_kuesioner'] = $value;
                $offeredAnswer[($key + 1)]['id_answer'] = $id_answer;
            }
            foreach ($data['question']['type'] as $key => $value) {

                if (empty($data['inpt-' . ($key + 1)])) {
                    continue;
                }

                if ($value == 1) {
                    $offeredAnswer[($key + 1)]['text'] = $data['inpt-' . ($key + 1)];
                } else if ($value == 2) {
                    $offeredAnswer[($key + 1)]['text'] = $data['inpt-' . ($key + 1)];
                } else if ($value == 3) {
                    if (empty($data['inpt-isian-' . $data['inpt-' . ($key + 1)]])) {
                        $offeredAnswer[($key + 1)]['text'] = $data['inpt-' . ($key + 1)];
                    } else {
                        $temp = array($data['inpt-' . ($key + 1)] => $data['inpt-isian-' . $data['inpt-' . ($key + 1)]]);
                        $offeredAnswer[($key + 1)]['text'] = json_encode($temp);
                    }
                } else if ($value == 4) {
                    $temp = array();
                    foreach ($data['inpt-' . ($key + 1)] as $key2 => $value2) {
                        // if ($value2 == 'lainnya[on]') {
                        //     $temp[$key2]['lainnya'] = $data['inpt-lainnya-' . ($key + 1)];
                        // }
                        if (!empty($data['inpt-isian-' . $value2])) {
                            $temp[$key2] = [$value2 => $data['inpt-isian-' . $value2]];
                        } else {
                            $temp[$key2] = $value2;
                        }
                    }
                    $offeredAnswer[($key + 1)]['text'] = json_encode($temp);
                } else if ($value == 5) {
                    $huruf = 'A';
                    $temp = array();
                    foreach ($data['inpt-' . ($key + 1)] as $key2 => $value2) {
                        if (!empty($data['' . ($key + 1) . '_' . $huruf])) {
                            $temp[$value2] = $data['' . ($key + 1) . '_' . $huruf];
                        }

                        $huruf++;
                    }
                    $offeredAnswer[($key + 1)]['text'] = json_encode($temp);
                } else if ($value == 6) {
                    $huruf = 'A';
                    $temp = array();
                    foreach ($data['inpt-' . ($key + 1)] as $key2 => $value2) {
                        if (!empty($data['inpt-' . ($key + 1) . '-' . $huruf])) {
                            $temp[$value2] = $data['inpt-' . ($key + 1) . '-' . $huruf];
                        }

                        $huruf++;
                    }
                    $offeredAnswer[($key + 1)]['text'] = json_encode($temp);
                } else if ($value == 7) {
                    // $temp = array('kode_prov' => $data['kuesioner_kode_provinsi_' . ($key + 1)], 'kode_kab' => $data['kuesioner_kode_kabupaten_' . ($key + 1)]);
                    if ($data['question']['others'][($key)] == 'provinsi') {
                        $offeredAnswer[($key + 1)]['text'] = $data['kuesioner_kode_provinsi_' . ($key + 1)];
                    }
                    if ($data['question']['others'][($key)] == 'kabupaten') {
                        $offeredAnswer[($key + 1)]['text'] = $data['kuesioner_kode_kabupaten_' . ($key + 1)];
                    }
                } else if ($value == 9) {
                    $offeredAnswer[($key + 1)]['text'] = $data['inpt-' . ($key + 1)];
                } else if ($value == 8) {
                    continue;
                }
            }

            $this->m_tracer->simpan_offered_answer($offeredAnswer);
            $dbError[] = $this->db->error();
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan jawaban kuesioner',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Terimakasih jawaban kuesioner anda telah tersimpan',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation($data)
    {
        $errors = FALSE;

        $this->form_validation->set_rules('id_alumni', 'Id alumni', 'required', array('required' => '{field} kosong2'));
        $this->form_validation->set_rules('inpt-0', 'f8', 'required', array('required' => 'Pertanyaan dengan kode f8 belum diisi'));

        if (empty($data['inpt-0'])) {
            $errors[] = array(
                'field' => 'inpt-0',
                'message' => 'Pertanyaan dengan kode f8 belum diisi'
            );
        }

        if (!empty($data['question']['type'])) {
            foreach ($data['question']['type'] as $key => $value) {
                if ($data['question']['question_code'][$key] == 'f510') {
                    continue;
                }

                if ($value == 1) {
                    $this->form_validation->set_rules('inpt-' . ($key + 1), $data['question']['question_code'][$key], 'required', array('required' => 'Pertanyaan dengan kode ' . $data['question']['question_code'][$key] . ' belum diisi'));
                } else if ($value == 2) {
                    $this->form_validation->set_rules('inpt-' . ($key + 1), $data['question']['question_code'][$key], 'required', array('required' => 'Pertanyaan dengan kode ' . $data['question']['question_code'][$key] . ' belum diisi'));
                } else if ($value == 3) {
                    if (empty($data['inpt-' . ($key + 1)])) {
                        $errors[] = [
                            'field' => 'inpt-' . ($key + 1),
                            'message' => 'Pertanyaan dengan kode ' . $data['question']['question_code'][$key] . ' belum diisi',
                        ];
                    } else {
                        // var_dump(($key + 1));
                        // die;
                        if (isset($data['inpt-isian-' . $data['inpt-' . ($key + 1)]]) and empty($data['inpt-isian-' . $data['inpt-' . ($key + 1)]])) {
                            $errors[] = [
                                'field' => 'inpt-' . ($key + 1),
                                'field_isian' => 'inpt-isian-' . $data['inpt-' . ($key + 1)],
                                'message' => 'Isian belum diisi',
                            ];
                        }
                    }
                } else if ($value == 4) {
                    if (empty($data['inpt-' . ($key + 1)])) {
                        $errors[] = [
                            'field' => 'inpt-' . ($key + 1) . '[]',
                            'message' => 'Pertanyaan dengan kode ' . $data['question']['question_code'][$key] . ' belum diisi',
                        ];
                    } else {
                        for ($i = 0; $i < count($data['inpt-' . ($key + 1)]); $i++) {
                            if ($data['inpt-' . ($key + 1)][$i] == 'lainnya[on]' and empty($data['inpt-lainnya-' . ($key + 1)])) {
                                $errors[] = [
                                    'field' => 'inpt-' . ($key + 1) . '[]',
                                    'field_isian' => 'inpt-lainnya-' . ($key + 1),
                                    'message' => 'Lainnya belum diisi',
                                ];
                            }
                        }
                    }
                } else if ($value == 5) {
                    $huruf = 'A';
                    for ($i = 0; $i < $data['question']['answer_length'][$key]; $i++) {
                        if (empty($data[($key + 1) . '_' . $huruf])) {
                            $errors[] = [
                                'field' => ($key + 1) . '_' . $huruf,
                                'message' => null,
                            ];
                        }
                        $huruf++;
                    }
                } else if ($value == 6) {
                    $huruf = 'A';
                    for ($i = 0; $i < $data['question']['answer_length'][$key]; $i++) {
                        if (empty($data['inpt-' . ($key + 1) . '-' . $huruf])) {
                            $errors[] = [
                                'field' => 'inpt-' . ($key + 1) . '-' . $huruf,
                                'message' => null,
                            ];
                        }
                        $huruf++;
                    }
                } else if ($value == 7) {
                    $this->form_validation->set_rules('kuesioner_kode_provinsi_' . ($key + 1), $data['question']['question_code'][$key], 'required', array('required' => 'Provinsi belum diisi'));
                    $this->form_validation->set_rules('kuesioner_kode_kabupaten_' . ($key + 1), $data['question']['question_code'][$key], 'required', array('required' => 'Kabupaten/Kota belum diisi'));
                } else if ($value == 9) {
                    $this->form_validation->set_rules('inpt-' . ($key + 1), $data['question']['question_code'][$key], 'required');
                }
            }
        }

        if ($this->form_validation->run() == FALSE) {
            foreach ($data as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function cetakBukti()
    {
        $data = array();
        $id_survey = $this->input->get('s_id');
        $id_alumni = $this->input->get('kode');
        $data['dataSurvey'] = $this->m_tracer->get_jadwal_survey(array('id_survey' => $id_survey), true);
        $data['dataAlumni'] = $dataAlumni = $this->getAlumni($id_alumni);
        $data['data'] = $this->m_tracer->cetak_bukti();
        $data['perguruan'] = $this->m_tracer->get_perguruan_tinggi();

        $data['dataAlumni']->jenis_kelamin = ($dataAlumni->jenis_kelamin == 'L' ? 'LAKI-LAKI' : 'PEREMPUAN');

        require_once APPPATH . 'third_party/dompdf/autoload.inc.php';
        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml($this->load->view('tracer/bukti', $data, true));

        // // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'potrait');
        $dompdf->set_option('isRemoteEnabled', true);
        // // Render the HTML as PDF
        $dompdf->render();

        // // Output the generated PDF to Browser
        $dompdf->stream('bukti-telah-mengisi-tracerstudy-' . $dataAlumni->nim);
        // $this->load->view('tracer/bukti', $data);
    }
}
