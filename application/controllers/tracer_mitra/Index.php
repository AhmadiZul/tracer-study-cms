<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Index extends BaseController
{

    public $loginBehavior = false;
    protected $template = "landing_app";
    protected $module = "tracer_mitra";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_kuesioner_mitra', 'mitra');
        $this->load->helper('function_helper');
        $this->load->library('form_validation');
        $this->load->library('tanggalindo');
    }

    public function index()
    {
        $this->data['halaman'] = 'Survey Stakeholder';
        $this->data['survei'] = $this->daftarSurvey();
        $this->renderTo("tracer_mitra/index");
    }

    public function formMitra()
    {
        if ($this->session->userdata("M_idAnswer") !== null) {
            redirect('tracer_mitra/index/isiKuesioner?a_id='.$this->session->userdata("M_idAnswer"));
        }

        $input = $this->input->get();

        $id_survey = $input['s_id'];
        if (!$id_survey) {
            return redirect('tracer_mitra');
        }

        $survey = $this->mitra->cekSurveiTersedia($id_survey);

        if (!$survey || !is_array($survey) || !isset($survey['id_survey'])) {
            return show_404();
        }

        if (date('Y-m-d H:i:s') < $survey['start_date'] || date('Y-m-d H:i:s') > $survey['end_date']) {
            return show_404();
        };
        $this->data['halaman'] = 'Survey Mitra';
        $this->data['survey'] = $survey;
        $this->data['id_survey'] = $id_survey;
        $this->data['perguruan_tinggi'] = setJenjangPendidikan();
        $this->data['survey']['deskripsi'] = 'Survey ' . $survey['nama'];
        $this->renderTo("tracer_mitra/form");
    }
    public function isiKuesioner()
    {
        if (empty($this->input->get('a_id'))) {
            redirect('tracer_mitra');
        }

        $checkIsi = $this->mitra->checkSudahIsi($this->input->get('a_id'));
        if ($checkIsi > 0) {
            redirect('tracer_mitra');
        }
        $this->data['id_answer'] = $this->input->get('a_id');
        $this->data['question_stakeholder'] = $this->mitra->load_question_stakeholder();
        $this->renderTo('tracer_mitra/isikuesioner');
    }
    public function simpan_mitra()
    {
        $input = $this->input->post();

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_answer = getUUID();

        $mitra = array(
            'id_answer' => $id_answer,
            'id_survey' => $input['id_survey'],
            'id_perguruan_tinggi' => $input['perguruan_tinggi'],
            'id_prodi' => $input['kode_prodi'],
            'jenis' => $input['jenis'],
            'sektor' => $input['sektor'],
            'email' => $input['email'],
            'telepon' => $input['telepon'],
            'alamat' => $input['alamat']
        );

        $save = $this->mitra->simpan_form_mitra($mitra);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal simpan mitra',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Silahkan isi kuesioner',
            ],
            'data' => $id_answer
        ];
        $this->session->set_userdata("M_idAnswer", $id_answer);

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    public function getProdi()
    {
        $kode_prodi = $this->input->get('kode_prodi');
        $prodi = setProdi($kode_prodi);
        echo json_encode($prodi);
    }

    public function daftarSurvey($limit = NULL, $offset = NULL)
    {
        $queryLimit = '';
        if (is_string($limit) || is_int($limit)) {
            $queryLimit = " LIMIT $limit ";
        }
        $queryOffset = '';
        if (is_string($offset) || is_int($offset)) {
            $queryOffset = " OFFSET $offset ";
        }

        $surveys = $this->db->query("SELECT
                ts.id_survey            AS id_survey,
                ts.start_date           AS survei_mulai,
                ts.end_date             AS survei_selesai,
                ts.nama                 AS nama
            FROM ref_jadwal_survey_mitra ts 
            WHERE ts.is_active = '1'
            AND ts.end_date >= NOW()
            ORDER BY ts.start_date ASC
            $queryLimit
            $queryOffset
        ")->result_array();

        foreach ($surveys as $key => $survei) {

            $surveys[$key]['untuk_survei_text'] = $survei['nama'];

            $surveys[$key]['url'] = site_url('tracer_mitra/index/formMitra?s_id=' . $survei['id_survey']);
            /* Menambah survei_kategori                                                 */

            /* Menambah status kuesioner                                                  */
            if (date('Y-m-d H:i:s') < $survei['survei_mulai']) {
                $surveys[$key]['status'] = 'delayed';
            } elseif (date('Y-m-d H:i:s') >= $survei['survei_mulai'] && date('Y-m-d H:i:s') <= $survei['survei_selesai']) {
                $surveys[$key]['status'] = 'ongoing';
            } elseif (date('Y-m-d H:i:s') > $survei['survei_selesai']) {
                $surveys[$key]['status'] = 'expired';
            };
            /* Menambah status kuesioner                                                  */

            $surveys[$key]['survei_mulai']   = $this->tanggalindo->konversi($surveys[$key]['survei_mulai']);
            $surveys[$key]['survei_selesai'] = $this->tanggalindo->konversi($surveys[$key]['survei_selesai']);
        };

        return $surveys;
    }
    public function inputKuesioner()
    {
        $input = $this->input->post();
        // var_dump($input);
        $id_answer = $input['id_answer'];
        $this->db->trans_begin();
        $p_type = count($input['type']);
        for ($i = 0; $i < $p_type; $i++) {
            if ($input['type'][$i] == '1' || $input['type'][$i] == '2' || $input['type'][$i] == '3') {
                $others = $this->db->query("SELECT others FROM kuesioner_mitra_question WHERE id_kuesioner='" . $input['idquestion'][$i] . "'")->row_array();
                if (isset($others['others']) && $others['others'] == 'lainnya[on]') {
                    $options = $this->db->query("SELECT id_opsi FROM kuesioner_mitra_question_option WHERE id_kuesioner='" . $input['idquestion'][$i] . "'")->result_array();
                    $options = array_map(function ($el) {
                        return $el['id_opsi'];
                    }, $options);
                    if (!in_array(trim($input['inpt' . ($i + 1)]), $options)) {
                        $jawaban = array(
                            $others['others'] => trim($input['inpt' . ($i + 1)])
                        );
                        $input['inpt' . ($i + 1)] = json_encode($jawaban);
                    }
                } else {
                    $isian = $this->db->query("SELECT id_opsi, isian FROM kuesioner_mitra_question_option WHERE id_kuesioner='" . $input['idquestion'][$i] . "'")->result_array();

                    foreach ($isian as $kI => $vI) {
                        if ($vI['isian'] == 'isian[on]') {
                            if (isset($input['inpt' . ($i + 1)]) && $input['inpt' . ($i + 1)] == $vI['id_opsi']) {
                                $tmpJwb = array(
                                    $input['inpt' . ($i + 1)] => trim($input[$input['inpt' . ($i + 1)] . '_isian' . ($i + 1)]),
                                );
                                $input['inpt' . ($i + 1)] = json_encode($tmpJwb);
                                break;
                            }
                        }
                    }
                }
                $jawaban = (isset($input['inpt' . ($i + 1)])) ? $input['inpt' . ($i + 1)] : null;
                $this->mitra->simpan_answer_mitra(
                    array(
                        'id_answer' => $id_answer, 'id_kuesioner' => $input['idquestion'][$i],
                        'text' => $jawaban
                    )
                );
            } else if ($input['type'][$i] == '4') {
                // TIPE CHECKBOX
                if ((!isset($input['inpt' . ($i + 1)])) || ($input['inpt' . ($i + 1)] === NULL) || ($input['inpt' . ($i + 1)] === [])) { // cek apakah inpt tidak kosong. jika kosong, jangan dimasukkan tabel
                    continue;
                }
                $temp_answer = array();
                if (isset($input['inpt' . ($i + 1)])) {
                    $others = $this->db->query("SELECT others FROM kuesioner_mitra_question WHERE id_kuesioner='" . $input['idquestion'][$i] . "'")->row_array();
                    $options = array();
                    if ($others['others'] == 'lainnya[on]') {
                        $options = $this->db->query("SELECT id_opsi FROM kuesioner_mitra_question_option WHERE id_kuesioner='" . $input['idquestion'][$i] . "'")->result_array();
                        $options = array_map(function ($el) {
                            return $el['id_opsi'];
                        }, $options);
                    }
                    $p_check = count($input['inpt' . ($i + 1)]);
                    for ($j = 0; $j < $p_check; $j++) {
                        if (!in_array(trim($input['inpt' . ($i + 1)][$j]), $options)) {
                            $tmpJwb = array(
                                $others['others'] => trim($input['inpt' . ($i + 1)][$j]),
                            );
                            $input['inpt' . ($i + 1)][$j] = $tmpJwb;
                        }
                        $temp_answer[] = $input['inpt' . ($i + 1)][$j];
                    }
                    $temp_answer = json_encode($temp_answer);
                }
                $this->mitra->simpan_answer_mitra(
                    array(
                        'id_answer' => $id_answer, 'id_kuesioner' => $input['idquestion'][$i],
                        'text' => $temp_answer
                    )
                );
            } else if ($input['type'][$i] == '5') {
                // TIPE skala likert /linier
                $count = "A";

                if ((!isset($input['inpt' . ($i + 1) . '_' . $count])) || ($input['inpt' . ($i + 1) . '_' . $count] === NULL) || ($input['inpt' . ($i + 1) . '_' . $count] === '')) {
                    // cek apakah inpt tidak kosong. jika kosong, jangan dimasukkan tabel
                    /** yg dicek cuma yg inpt_A karena
                     * jika inpt_A tidak kosong, maka yg inpt_ lainnya pasti juga tidak kosong,
                     * jika inpt_A kosong, maka yg inpt_ lainnya pasti juga kosong,
                     **/
                    continue;
                }

                $countless = 0;
                $count_num     = 0;
                $temp_answer   = array();
                $id_question   = $input['idquestion'][$i];
                $pilihanLikert = $this->db->query(
                    "SELECT id_likert
                    FROM kuesioner_mitra_question_likert
                    WHERE id_kuesioner = '$id_question'
                    "
                )->result_array();



                while (true) {
                    if (isset($input['inpt' . ($i + 1) . '_' . $count])) {
                        $countless = 0;

                        $inptI_count  = $input['inpt' . ($i + 1) . '_' . $count];
                        $idLikert     = $pilihanLikert[$count_num]['id_likert'];

                        $temp_answer[$idLikert] = $inptI_count;

                        $count_num++;
                        $count++;
                    } else {
                        if ($countless == 3) {
                            break;
                        }
                        $countless++;
                    }
                }
                $temp_answer = json_encode($temp_answer);
                $this->mitra->simpan_answer_mitra(
                    array(
                        'id_answer' => $id_answer,
                        'id_kuesioner' => $id_question,
                        'text' => $temp_answer
                    )
                );
            }
        }
        $return['success'] = false;
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $return['success'] = true;

            if ($this->session->userdata("M_idAnswer") !== null) {
                $this->session->unset_userdata("M_idAnswer");
            }
            
        } else {
            $this->db->trans_rollback();
            $return['success'] = false;
        }
        $this->db->trans_complete();
        echo json_encode($return);
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('id_survey', 'ID', 'trim|required');
        $this->form_validation->set_rules('perguruan_tinggi', 'Asal kampus/sekolah', 'trim|required');
        $this->form_validation->set_rules('kode_prodi', 'Prodi/Jurusan', 'trim|required');
        $this->form_validation->set_rules('jenis', 'Jenis', 'trim|required');
        $this->form_validation->set_rules('sektor', 'Sektor', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[100]');
        $this->form_validation->set_rules('telepon', 'Telepon', 'trim|required|max_length[13]');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|callback_notOnlyNumber|callback_whitespace|max_length[255]');

        if (!$this->input->post('sektor')) {
            $errors[] = [
                'field'   => 'sektor',
                'message' => 'Sektor belum diisi',
            ];
        }

        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('checkDate', '{field} tidak boleh lebih dari tanggal hari ini');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');
        $this->form_validation->set_message('numberOnly', '{field} tidak boleh selain angka');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }
    public function notOnlyNumber($str)
    {
        if (ctype_digit($str)) {
            return false;
        }
        return true;
    }

    public function numberOnly($str)
    {
        if (!preg_match('/\D/', $str)) {
            return false;
        }

        return true;
    }
}
