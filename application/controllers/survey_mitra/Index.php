<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Index extends BaseController
{
    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "survey_mitra";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_kuesioner_mitra');
    }

    public function index()
    {
        $this->data['title'] = '<i class="fa fa-clipboard-list"></i> Tracer';
        $this->data['is_home'] = false;
        $this->data['survey'] = $this->m_kuesioner_mitra->checkTracerMitra();
        $this->render('index');
    }
}
