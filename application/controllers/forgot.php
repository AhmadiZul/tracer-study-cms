<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model("Model_auth", "auth");
	}

public function emailverif()
	{
		$this->form_validation->set_rules('email','Email','required|valid_email');
		if ($this->form_validation->run()==FALSE) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" 
			role="alert"> Email Belum Diregistrasi </div>');
			$this->load->view('auth/forgot');
		} else {
			$email = $this->input->post('email');
			$user= $this->auth-->reset($email);
			if($user){
				$data['email'] = $email;
				$this->session->set_userdata($data); 
				$this->session->set_flashdata('message', '<div class="alert alert-success" 
					role="alert"> Email Terdaftar !!! </div>');
				redirect('password');
		}
		$this->load->view('auth/forgot');
		}
	}

	public function resetPassword()
	{
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]|matches[password2]', [
			'matches' => 'Password tidak sesuai!',
			'min_length' => 'Password terlalu pendek!'
		]);
		$this->form_validation->set_rules('password2', 'Password','matches[password]', [
			'matches' => 'Password tidak sesuai!',
		]);
		if ($this->form_validation->run() == false) {
			$this->load->view('auth/password');
		} else{
			$data = [
				'password' => hash('sha256',$this->input->post("password")),
			];
			$email = $this->session->userdata('email');
			$this->auth->update($email , $data);
			$this->session->unset_userdata('email');
			$this->session->set_flashdata('message', '<div class="alert alert-success" 
			role="alert"> Selamat Akunmu Di Recover!!!. Silahkan Login</div>');
			redirect('auth');
		
		}
	}
}