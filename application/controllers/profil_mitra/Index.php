<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "profil_mitra";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('m_profil_mitra');
    }

    public function index()
    {
        $this->data['title'] = '<i class="fa fa-clipboard-list"></i> Profil';
        $this->data['is_home'] = false;
        $this->data['mitra'] = $this->m_profil_mitra->getMitraByUser();
        $this->render('index');
    }

    function _urlphoto($id_instansi)
    {
        $mitra = $this->m_profil_mitra->getMitra($id_instansi);

        $id_instansi = str_replace("-", "", $id_instansi);

        if (!file_exists('admin/public/uploads/mitra/')) {
            mkdir('admin/public/uploads/mitra/', 0755, true);
        }
        if (!file_exists('admin/public/uploads/mitra/' .$id_instansi)) {
            mkdir('admin/public/uploads/mitra/'.$id_instansi, 0755,  true);
        }

        $oldUrlBukti = '';
        if (!empty($mitra)) {
            $base_url = base_url();
            $oldUrlBukti = str_replace($base_url, '',$mitra['url_logo']);
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'admin/public/uploads/mitra/'.$id_instansi; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'Logo' . $id_instansi;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = base_url() . 'admin/public/uploads/mitra/'.$id_instansi.'/'. $file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }
    
    public function editMitra()
    {
        $this->onUpdate = 1;

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_instansi = $this->input->post('id_instansi');
        $mitra = $this->m_profil_mitra->getMitra($id_instansi);

        $data = array(
            'nama' => $this->input->post('nama'),
            'jenis' => $this->input->post('jenis'),
            'sektor' => $this->input->post('sektor'),
            'telephone' => $this->input->post('telephone'),
            'url_web' => $this->input->post('website'),
            'alamat' => $this->input->post('alamat'),
            'last_modified_by' => $this->session->userdata("tracer_userId")
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_instansi);
            $data['url_logo'] = $urlPhoto['file_name'];
        }

        $this->m_profil_mitra->edit_mitra($data, array('id_instansi' => $id_instansi));
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal mengubah mitra',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengubah mitra',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function editAkun()
    {
        $this->onUpdate = 0;

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidationUser();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_instansi = $this->input->post('id_instansi');
        $mitra = $this->m_profil_mitra->getMitra($id_instansi);
        $user = array();

        if ($this->input->post('username') != $mitra['email']) {
            $user['email'] = $this->input->post('username');
            $user['username'] = $this->input->post('username');
        }

        if ($this->input->post('password')) {
            $user['password'] = hash('sha256', $this->input->post('password'));
        }

        if (!empty($user)) {
            $this->m_profil_mitra->edit_user($user, array('id_user' => $mitra['id_user']));
            $dbError[] = $this->db->error();
        }

        $data = array(
            'email' => $this->input->post('username'),
            'last_modified_by' => $this->session->userdata("tracer_userId")
        );

        $this->m_profil_mitra->edit_mitra($data, array('id_instansi' => $id_instansi));
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal mengubah akun',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengubah akun',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $id_instansi = $this->input->post('id_instansi');
        $mitra = $this->m_profil_mitra->getMitra($id_instansi);

        $this->form_validation->set_rules('nama', 'Nama instansi', 'trim|required|max_length[100]|callback_whitespace|callback_notOnlyNumber');
        $this->form_validation->set_rules('jenis', 'Jenis', 'trim|required');
        $this->form_validation->set_rules('sektor', 'Sektor', 'trim|required');
        $this->form_validation->set_rules('telephone', 'Telepon', 'trim|required|max_length[13]|callback_validHp');
        $this->form_validation->set_rules('website', 'Website', 'trim|required|callback_validUrl|max_length[255]');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|callback_whitespace|callback_notOnlyNumber|max_length[255]');

        if (isset($mitra->url_logo) && $mitra->url_logo == '' && isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $errors[] = [
                'field'   => 'url_photo',
                'message' => 'Logo belum diisi',
            ];
        }

        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('checkMitra', '{field} mitra sudah terdaftar');
        $this->form_validation->set_message('checkUser', '{field} user sudah terdaftar');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');
        $this->form_validation->set_message('validHp', '{field} diawali dengan kode negara (08), minimal 11 digit dan maksimal 13 digit');
        $this->form_validation->set_message('validUrl', '{field} tidak sesuai');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidationUser()
    {
        $errors = FALSE;

        $id_instansi = $this->input->post('id_instansi');
        $mitra = $this->m_profil_mitra->getMitra($id_instansi);

        $this->form_validation->set_rules('username', 'Username', 'trim|required|valid_email|max_length[100]|callback_checkMitra[email]|callback_checkUser[email]');
        $this->form_validation->set_rules('password_lama', 'Password lama', 'trim|required|callback_checkPasswordLama[password]');
        $this->form_validation->set_rules('password', 'Password baru', 'trim|required');
        $this->form_validation->set_rules('konfirmasi_password', 'Konfirmasi password', 'trim|required|matches[password]');

        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('checkMitra', '{field} mitra sudah terdaftar');
        $this->form_validation->set_message('checkPasswordLama', '{field} tidak sesuai');
        $this->form_validation->set_message('checkUser', '{field} user sudah terdaftar');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function checkMitra($str, $col)
    {
        if ($str == "") {
            return false;
        }
        return $this->m_profil_mitra->checkMitra(array($col => $str), $this->input->post('id_instansi'));
    }

    public function checkUser($str, $col)
    {
        if ($str == "") {
            return true;
        }
        $id_user = $this->m_profil_mitra->getMitra($this->input->post('id_instansi'))['id_user'];
        return $this->m_profil_mitra->checkUser(array($col => $str), $id_user);
    }

    public function checkPasswordLama($str, $col)
    {
        if ($str == "") {
            return true;
        }
        $id_user = $this->m_profil_mitra->getMitra($this->input->post('id_instansi'))['id_user'];
        return $this->m_profil_mitra->checkPasswordLama(array($col => hash('sha256', $str)), $id_user);
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }
    public function notOnlyNumber($str)
    {
        if (ctype_digit($str)) {
            return false;
        }
        return true;
    }

    public function validHp($str)
    {
        if (preg_match('/^(\+62|62|0)8[1-9][0-9]{6,10}$/', $str)) {
            return true;
        }

        return false;
    }

    function validUrl($url)
    {
        if (preg_match('/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $url)) {
            return true;
        }

        return false;
    }
}
