<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "landing_app";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_profil','alumni');
    }

    public function index()
    {
        $this->data['title'] = 'Beranda';
        $this->data['is_full'] = true;
        $this->render('alumni/alumni');
    }
}