<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "alumni";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_profil');
        $this->load->model('M_setting', 'setting');
    }

    public function index()
    {
        $this->data['icon'] = '<i class="bx bxs-dashboard"></i>';
        $this->data['title'] = 'Dasboard';
        $this->data['is_home'] = false;
        $this->data['alumni'] = $alumni = $this->m_profil->get_alumni_by_id_user($this->session->userdata("t_userId"));
        $this->data['logo_title'] = $logo_title= $this->setting->logo_title();
        $this->data['warna_tema']  = $warna_tema = $this->setting->warna_tema();
        $this->data['logo_utama']  = $logo_utama = $this->setting->logo_utama();
        $this->render('dashboard/dashboard');
    }
}
