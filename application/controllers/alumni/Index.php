<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "landing_app";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_profil','alumni');
        $this->load->model('M_setting','setting');
    }

    public function index()
    {
        $this->data['title'] = 'alumni';
        $this->data['is_full'] = true;
        $this->data['copyright'] = $copyright = $this->setting->copyright();
        $this->data['about_us'] = $about_us = $this->setting->about_us();
        $this->data['logo_utama'] = $logo_utama = $this->setting->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->setting->logo_title();
        $this->data['warna_tema'] = $warna_tema = $this->setting->warna_tema();
        $this->data['header'] = $header = $this->setting->header();
        $this->data['footer'] = $footer = $this->setting->footer();
        $this->data['perguruan_tinggi'] = $perguruan_tinggi = $this->setting->perguruan_tinggi();
        $social_media = $this->setting->social_media();
        $sosial = json_decode($social_media->deskripsi);
        $this->data['social_media'] = $sosial;
        $this->render('alumni/index');
        
    }

    public function alumni()
    {
        $this->data['title'] = 'Beranda';
        $this->data['is_full'] = true;
        $this->render('alumni/alumni');
    }
}
