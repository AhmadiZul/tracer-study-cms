<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "landing_app";
    protected $module = "layanan_keuangan";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_layanan_keuangan','layanan');
    }

    public function index()
    {
        $this->data['title'] = 'Beranda';
        $this->data['is_full'] = true;
        $this->data['layanan'] = $this->layanan->getLayanan();
        $this->renderTo('layanan_keuangan/index');
    }
}
