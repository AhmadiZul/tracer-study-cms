<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "landing_tips";
    protected $module = "tips_karir";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_tips','tips');
        $this->load->model('M_setting','setting');
        $this->load->model('M_agenda','agenda');
    }

    public function index()
    {
        $this->data['title'] = 'Beranda';
        $this->data['is_full'] = true;
        $this->data['tips'] =  $tips = $this->tips->getTips();
        $this->data['agenda'] =  $agenda = $this->agenda->getAgenda(array('waktu_mulai >= '=>date("U")),false);
        $this->data['copyright'] = $copyright = $this->setting->copyright();
        $this->data['about_us'] = $about_us = $this->setting->about_us();
        $this->data['logo_utama'] = $logo_utama = $this->setting->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->setting->logo_title();
        $this->data['tips_karir'] = $tips_karir = $this->setting->tips_karir();
        $this->data['warna_tema'] = $warna_tema = $this->setting->warna_tema();
        $this->data['perguruan_tinggi'] = $perguruan_tinggi = $this->setting->perguruan_tinggi();
        $this->data['footer'] = $this->setting->footer();
        $this->data['header'] = $this->setting->header();
        $this->data['perguruan_tinggi'] = $perguruan_tinggi = $this->setting->perguruan_tinggi();

        $social_media = $this->setting->social_media();
        $sosial = json_decode($social_media->deskripsi);
        $this->data['social_media'] = $sosial;

        foreach ($agenda as $key => $value) {
            $value->waktu_mulai = $this->tanggalindo->konversi($value->waktu_mulai);
            $value->hari = $this->tanggalindo->get_hari_from_date($value->waktu_mulai);
        }
        
        foreach ($tips_karir as $key => $value) {
            $value->deskripsi = substr(trim(preg_replace('/<.*?>/', '', $value->deskripsi)),0,90);
            $value->created_at = $this->tanggalindo->konversi($value->created_at);
        }
        $this->data['tipsRecom'] = $this->tips->getRekomendation();
        $this->renderTo('tips_karir/index');
    }

    public function tips($id)
    {
        $this->data['title'] = 'Beranda';
        $this->data['is_full'] = true;
        $this->data['agenda'] =  $agenda = $this->agenda->getAgenda(array('waktu_mulai >= '=>date("U")),false);
        $this->data['tipsAll'] =  $tipsAll = $this->tips->getDetailtips($id);
        $this->data['copyright'] = $copyright = $this->setting->copyright();
        $this->data['about_us'] = $about_us = $this->setting->about_us();
        $this->data['logo_utama'] = $logo_utama = $this->setting->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->setting->logo_title();
        $this->data['warna_tema'] = $warna_tema = $this->setting->warna_tema();
        $this->data['footer'] = $this->setting->footer();
        $this->data['header'] = $this->setting->header();
        $this->data['perguruan_tinggi'] = $perguruan_tinggi = $this->setting->perguruan_tinggi();

        $social_media = $this->setting->social_media();
        $sosial = json_decode($social_media->deskripsi);
        $this->data['social_media'] = $sosial;

        $this->renderTo('tips_karir/detail');
    }

    public function cvTips()
    {
        $data = $this->input->post();
        $viewer = $this->tips->getTips(['id' => $data['id']])->viewer;
        $return = array();

        $this->db->trans_begin();

        $tambah = $this->tips->update_tips(array('viewer' => $viewer+1),$data['id']);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $kondisi = true;
            $return = array('success' => $kondisi, "kode" => $data['id'], "data" => ['viewer'=>$viewer+1]);
        } else {
            $this->db->trans_rollback();
            $kondisi = false;
            $return = array('success' => $kondisi, "kode" => $data['id']);
        }
        echo json_encode($return);
    }

}
