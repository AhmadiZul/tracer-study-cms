<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Login extends BaseController
{

    public $loginBehavior = false;
    public $template = "login_app";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_setting', 'setting');
    }

    public function index()
    {
        /* if ($this->session->userdata("t_idGroup")==4) {
            redirect('alumni/dashboard');
        }
        if ($this->session->userdata("t_idGroup")==3) {
            redirect('mitra/dashboard');
        }
        $this->data['title'] = 'Login';
        $this->data['is_full'] = true;
        $this->data['warna_tema'] = $warna_tema = $this->setting->warna_tema();
      
        $this->renderTo('auth/login/index'); */
        if ($this->session->userdata("t_idGroup")==4) {
            redirect('alumni/dashboard');
        }
        if ($this->session->userdata("t_idGroup")==3) {
            redirect('mitra/dashboard');
        }
        $this->data['message'] = '';
        if ($this->input->get('message')) {
            $this->data['message'] = $this->input->get('message');
        }
        $this->data['title'] = 'alumni';
        $this->data['is_full'] = true;
        $this->data['captcha'] = $this->create_captcha();
        $this->data['warna_tema']  = $warna_tema = $this->setting->warna_tema();
        $this->data['logo_utama']  = $logo_utama = $this->setting->logo_utama();
        $this->data['logo_title']  = $logo_title = $this->setting->logo_title();

        $this->renderTo('/auth/login/alumni');
    }

    public function alumni()
    {
        if ($this->session->userdata("t_idGroup")==4) {
            redirect('alumni/dashboard');
        }
        if ($this->session->userdata("t_idGroup")==3) {
            redirect('mitra/dashboard');
        }
        $this->data['message'] = '';
        if ($this->input->get('message')) {
            $this->data['message'] = $this->input->get('message');
        }
        $this->data['title'] = 'alumni';
        $this->data['is_full'] = true;
        $this->data['captcha'] = $this->create_captcha();
        $this->data['warna_tema']  = $warna_tema = $this->setting->warna_tema();
        $this->data['logo_utama']  = $logo_utama = $this->setting->logo_utama();
        $this->data['logo_title']  = $logo_title = $this->setting->logo_title();

        $this->renderTo('/auth/login/alumni');
    }

    public function mitra()
    {
        if ($this->session->userdata("t_idGroup")==4) {
            redirect('alumni/dashboard');
        }
        if ($this->session->userdata("t_idGroup")==3) {
            redirect('mitra/dashboard');
        }
        $this->data['message'] = '';
        if ($this->input->get('message')) {
            $this->data['message'] = $this->input->get('message');
        }
        $this->data['title'] = 'mitra';
        $this->data['is_full'] = true;
        $this->data['captcha'] = $this->create_captcha();
        $this->renderTo('auth/login/mitra');
    }

    public function fetchCaptha()
    {
        $response['data'] = $this->create_captcha();
        echo json_encode($response);
    }
    
}
