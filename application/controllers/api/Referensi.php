<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Referensi extends REST_Controller
{

    private $ok = '200';
    private $created = '201';
    private $nocontent = '204';
    private $badrequest = '400';
    private $notfound = '404';
    private $unauthorized = '401';

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/api_referensi', 'referensi');
    }

    public function index_get()
    {
        $this->response([
            'status' => $this->badrequest,
            'message' => 'Bad request',
            'data' => null
        ], REST_Controller::HTTP_BAD_REQUEST);
    }

    public function index_post() {
        $this->response([
            'status' => $this->badrequest,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

    public function index_put() {
        $this->response([
            'status' => $this->badrequest,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

    public function index_delete() {
        $this->response([
            'status' => $this->badrequest,
            'error' => 'Bad Request'
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

    public function index_patch() {
        $this->response([
            'status'  => $this->badrequest,
            'message' => 'Bad Request',
            'data'    => null,  
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

    public function periode_get()
    {
        $data = $this->referensi->getPeriode();

        if ($this->uri->segment(4) || $this->input->get() != null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if ($data != null) {
            $this->response([
                'status'    => $this->ok,
                'message'   => 'Data referensi ditemukan',
                'data'      => $data,
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status'    => $this->notfound,
                'message'   => 'Data referensi ditmeukan',
                'data'      => $data,
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function perguruanTinggi_get()
    {
        $header = $this->input->request_headers();

        if ($this->uri->segment(4) || $this->input->get() != null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $data = $this->referensi->getPerguruanTinggi(array('id_user' => $decoded_token->id_user));

                if ($data != null) {
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => 'Data referensi ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => 'Data referensi tidak ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function prodi_get()
    {
        $header = $this->input->request_headers();

        if ($this->uri->segment(4) || $this->input->get() != null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }
        
        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            $token_key = $header['api_key'];
            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $data = $this->referensi->getProdi($decoded_token->id_user);

                if ($data != null) {
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => 'Data referensi ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => 'Data referensi tidak ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function provinsi_get()
    {
        $header = $this->input->request_headers();

        if ($this->uri->segment(4) || $this->input->get() != null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $data = $this->referensi->getProvinsi();

                if ($data != null) {
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => 'Data referensi ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => 'Data referensi tidak ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function kab_kota_get()
    {
        $header = $this->input->request_headers();

        if ($this->uri->segment(4)) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if ($this->get() && $this->get('kode_provinsi') == null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $data = $this->referensi->getKabKota();

                if ($this->get('kode_provinsi')) {
                    $data = $this->referensi->getKabKota(array('kode_provinsi' => $this->get('kode_provinsi'))); # code...
                }

                if ($data != null) {
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => 'Data referensi ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => 'Data referensi tidak ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function kecamatan_get()
    {
        $header = $this->input->request_headers();

        if ($this->uri->segment(4)) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if ($this->get() && $this->get('kode_kab_kota') == null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $data = $this->referensi->getKecamatan();

                if ($this->get('kode_kab_kota')) {
                    $data = $this->referensi->getKecamatan(array('kode_kab_kota' => $this->get('kode_kab_kota'))); # code...
                }

                if ($data != null) {
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => 'Data referensi ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => 'Data referensi tidak ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function agama_get()
    {
        $header = $this->input->request_headers();

        if ($this->uri->segment(4) || $this->input->get() != null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $data = $this->referensi->getAgama();

                if ($data != null) {
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => 'Data referensi ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => 'Data referensi tidak ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function jenisSurvey_get()
    {
        $header = $this->input->request_headers();

        if ($this->uri->segment(4) || $this->input->get() != null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $data = $this->referensi->getJenissurvey();

                if ($data != null) {
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => 'Data referensi ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => 'Data referensi tidak ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function statusAlumni_get()
    {
        $header = $this->input->request_headers();

        if ($this->uri->segment(4) || $this->input->get() != null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $data = $this->referensi->getStatusAlumni();

                if ($data != null) {
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => 'Data referensi ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => 'Data referensi tidak ditemukan',
                        'data'      => $data,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
