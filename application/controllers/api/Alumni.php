<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Alumni extends REST_Controller
{

    private $ok = '200';
    private $created = '201';
    private $nocontent = '204';
    private $badrequest = '400';
    private $notfound = '404';
    private $unauthorized = '401';
    public $onUpdate = 0;

    function __construct()
    {
        parent::__construct();
        $this->methods['data_post']['limit'] = 100;
        $this->load->helper('validation');
        $this->load->model('api/api_alumni', 'alumni');
        $this->load->model('api/api_referensi', 'referensi');
    }

    public function index_get() {
        $header = $this->input->request_headers();

        if ($this->uri->segment(3) || $this->input->get() != null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            // check token
            $token_key = $header['api_key'];
            
            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $data = $this->alumni->getAlumni($decoded_token->id_user);

                if ($data != null) {
                    foreach ($data as $key => $value) {
                        if ($value->url_photo) {
                            $value->url_photo = base_url().$value->url_photo;
                        }
    
                        $value->bekerja[] = $this->alumni->getAlumniBekerja($value->id_alumni);
                        $value->wiraswasta[] = $this->alumni->getAlumniWira($value->id_alumni);
                        $value->study[] = $this->alumni->getAlumniStudy($value->id_alumni);
                        
                    }
                    
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => "Data alumni ditemukan",
                        'data'      => $data,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => "Data alumni tidak ditemukan",
                        'data'      => $data,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_post() {
        $header = $this->input->request_headers();

        if ($this->uri->segment(3) || $this->input->get() != null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            // check token
            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $params = json_decode(trim(file_get_contents('php://input')), true);

                $isEmpty = $this->filter($params);
                $checkNimAlumni = $this->checkAlumni(array('nim' => $params['nim']));
                $checkNimUser = $this->checkUser(array('username' => $params['nim']));
                $checkNik = $this->checkAlumni(array('nik' => $params['nik']));
                $checkEmailAlumni = $this->checkAlumni(array('email' => $params['email']));
                $checkEmailUser = $this->checkUser(array('email' => $params['email']));

                if ($isEmpty['error']) {
                    $insert['success'] = false;
                    $insert['message'] = "Data yang diisi belum sesuai. Lihat dokumentasi.";
                    $insert['error'] = $isEmpty['pesan'];
                } elseif ($checkNimAlumni || $checkNimUser) {
                    $insert['success'] = false;
                    $insert['message'] = "Nim sudah terdaftar";
                    $insert['error'] = null;
                } elseif ($checkNik) {
                    $insert['success'] = false;
                    $insert['message'] = "Nik sudah terdaftar";
                    $insert['error'] = null;
                } elseif ($checkEmailAlumni || $checkEmailUser) {
                    $insert['success'] = false;
                    $insert['message'] = "Email sudah terdaftar";
                    $insert['error'] = null;
                } else {
                    $data = array(
                        'nim' => $params['nim'],
                        'nama' => $params['nama'],
                        'nik' => $params['nik'],
                        'tempat_lahir' => $params['tempat_lahir'],
                        'tgl_lahir' => $params['tgl_lahir'],
                        'id_agama' => $params['id_agama'],
                        'jenis_kelamin' => $params['jenis_kelamin'],
                        'jalur_masuk' => $params['jalur_masuk'],
                        'no_hp' => $params['no_hp'],
                        'email' => $params['email'],
                        'alamat' => $params['alamat'],
                        'kode_prov' => $params['kode_prov'],
                        'kode_kab_kota' => $params['kode_kab_kota'],
                        'kode_kecamatan' => $params['kode_kecamatan'],
                        'url_photo' => $params['url_photo'],
                        'tahun_lulus' => $params['tahun_lulus'],
                        'ipk_terakhir' => $params['ipk_terakhir'],
                        'id_perguruan_tinggi' => $params['id_perguruan_tinggi'],
                        'id_prodi' => $params['id_prodi'],
                        'judul_skripsi' => $params['judul_skripsi'],
                        'id_ref_status_alumni' => $params['id_ref_status_alumni'],
                        'status_data' => '1',
                        'is_active' => '1',
                        'last_action_user' => 'CREATE',
                        'last_modified_user' => $decoded_token->id_user
                    );

                    $data['id_alumni'] = getUUID();
                    $id_job_study = '';

                    $data2 = array(
                        'username' => $params['nim'],
                        'email' => $params['email'],
                        'real_name' => $params['nama'],
                        'id_group' => 4,
                        'password' => hash('sha256', $params['nim']),
                        'last_modified_user' => $decoded_token->id_user
                    );

                    $data3 = array();
                    if ($params['id_ref_status_alumni'] == '1' || $params['id_ref_status_alumni'] == '3') {
                        $id_job_study = getUUID();
                        $data3 = [
                            'id_job' => $id_job_study,
                            'id_ref_status_alumni' => $params['id_ref_status_alumni'],
                            'id_alumni' => $data['id_alumni'],
                            'sektor' => $params['sektor'],
                            'kode_provinsi' => null,
                            'kode_kab_kota' => null,
                            'kode_kecamatan' => null,
                            'created_by' => $decoded_token->id_user,
                            'last_modified_by' => $decoded_token->id_user
                        ];
                    } elseif ($params['id_ref_status_alumni'] == '4') {
                        $id_job_study = getUUID();
                        $data3 = [
                            'id_study' => $id_job_study,
                            'nama_perguruan_tinggi' => $params['nama_instansi'],
                            'id_alumni' => $data['id_alumni'],
                            'sektor_prodi' => $params['sektor'],
                            'kode_provinsi' => null,
                            'kode_kab_kota' => null,
                            'kode_kecamatan' => null,
                            'created_by' => $decoded_token->id_user,
                            'last_modified_by' => $decoded_token->id_user
                        ];
                    }


                    $insert = $this->alumni->simpanData($data, $data2, $data3);
                }

                if ($insert['success']) {
                    //data harus berisi id_alumni, id_job_study, url_photo

                    $response = array(
                        'id_alumni' => $data['id_alumni'],
                        'id_jobs_study' => $id_job_study
                    );
                    $this->response([
                        'status'    => $this->created,
                        'message'   => $insert['message'],
                        'data'      => $response,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => $insert['message'],
                        'error'      => $insert['error'],
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_put()
    {
        $header = $this->input->request_headers();
        $this->onUpdate = 1;

        if ($this->uri->segment(3)) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if ($this->input->get('id_alumni') == null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Id Alumni belum ada",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $params = json_decode(trim(file_get_contents('php://input')), true);
                $id_alumni = $this->input->get('id_alumni');
                $alumniOld = $this->alumni->getAlumniById($id_alumni);

                $isEmpty = $this->filter($params,$id_alumni);

                if ($isEmpty['error']) {
                    $insert['success'] = false;
                    $insert['message'] = "Data yang diisi belum sesuai. Lihat dokumentasi.";
                    $insert['error'] = $isEmpty['pesan'];
                } elseif ($this->checkAlumni(array('nim' => $params['nim']),$id_alumni) || $this->checkUser(array('username' => $params['nim']),$alumniOld->id_user)) {
                    $insert['success'] = false;
                    $insert['message'] = "Nim sudah terdaftar";
                    $insert['error'] = null;
                } elseif ($this->checkAlumni(array('nik' => $params['nik']),$id_alumni)) {
                    echo $this->db->last_query();
                    exit();
                    $insert['success'] = false;
                    $insert['message'] = "Nik sudah terdaftar";
                    $insert['error'] = null;
                } elseif ($this->checkAlumni(array('email' => $params['email']),$id_alumni) || $this->checkUser(array('email' => $params['email']),$alumniOld->id_user)) {
                    $insert['success'] = false;
                    $insert['message'] = "Email sudah terdaftar";
                    $insert['error'] = null;
                } else {
                    $data = array(
                        'nim' => $params['nim'],
                        'nama' => $params['nama'],
                        'nik' => $params['nik'],
                        'tempat_lahir' => $params['tempat_lahir'],
                        'tgl_lahir' => $params['tgl_lahir'],
                        'id_agama' => $params['id_agama'],
                        'jenis_kelamin' => $params['jenis_kelamin'],
                        'jalur_masuk' => $params['jalur_masuk'],
                        'no_hp' => $params['no_hp'],
                        'email' => $params['email'],
                        'alamat' => $params['alamat'],
                        'kode_prov' => $params['kode_prov'],
                        'kode_kab_kota' => $params['kode_kab_kota'],
                        'kode_kecamatan' => $params['kode_kecamatan'],
                        'url_photo' => $params['url_photo'],
                        'tahun_lulus' => $params['tahun_lulus'],
                        'ipk_terakhir' => $params['ipk_terakhir'],
                        'id_perguruan_tinggi' => $params['id_perguruan_tinggi'],
                        'id_prodi' => $params['id_prodi'],
                        'judul_skripsi' => $params['judul_skripsi'],
                        'id_ref_status_alumni' => $params['id_ref_status_alumni'],
                        'status_data' => '1',
                        'is_active' => '1',
                        'last_action_user' => 'UPDATE',
                        'last_modified_user' => $decoded_token->id_user
                    );

                    $data2 = array();
                    if ($params['nim']!= $alumniOld->nim) {
                        $data2['username'] = $params['nim'];
                    }

                    if ($params['email'] != $alumniOld->email) {
                        $data2['email'] = $params['email'];
                    }

                    if ($params['nama'] != $alumniOld->nama) {
                        $data2['real_name'] = $params['nama'];
                    }

                    if (!empty($data2)) {
                        $data2['last_modified_user'] = $decoded_token->id_user;
                    }

                    if ($params['id_ref_status_alumni'] == '1' || $params['id_ref_status_alumni'] == '3') {
                        $data3 = [
                            'id_job' => $params['id_job_study'],
                            'id_ref_status_alumni' => $params['id_ref_status_alumni'],
                            'id_alumni' => $id_alumni,
                            'sektor' => $params['sektor'],
                            'kode_provinsi' => null,
                            'kode_kab_kota' => null,
                            'kode_kecamatan' => null,
                            'last_modified_by' => $decoded_token->id_user
                        ];
                    } elseif ($params['id_ref_status_alumni'] == '4') {
                        $data3 = [
                            'id_study' => $params['id_job_study'],
                            'id_alumni' => $id_alumni,
                            'sektor_prodi' => $params['sektor'],
                            'kode_provinsi' => null,
                            'kode_kab_kota' => null,
                            'kode_kecamatan' => null,
                            'last_modified_by' => $decoded_token->id_user
                        ];
                    }


                    $insert = $this->alumni->ubahData($id_alumni, $data, $data2, $data3);
                }
        
                if ($insert['success']) {
                    $response = array(
                        'id_alumni' => $id_alumni
                    );

                    if ($insert['data']) {
                        $response['id_jobs_study'] = $insert['data'];
                    }

                    $this->response([
                        'status'    => $this->ok,
                        'message'   => $insert['message'],
                        'data'      => $response,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => $insert['message'],
                        'error'      => $insert['error'],
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_delete()
    {
        $header = $this->input->request_headers();

        if ($this->uri->segment(3)) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if ($this->input->get('id_alumni') == null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Id Alumni belum ada",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (!$this->checkAlumni(array('id_alumni' => $this->input->get('id_alumni')))) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "ID tidak sesuai",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $id_alumni = $this->input->get('id_alumni');
                $alumni = array(
                    'is_active' => '0',
                    'last_action_user' => 'UPDATE',
                    'last_modified_user' => $decoded_token->id_user
                );
                $user = array(
                    'is_active' => '0',
                    'last_modified_user' => $decoded_token->id_user
                );
                $insert = $this->alumni->hapusData($id_alumni, $alumni, $user);

                if ($insert['success']) {
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => $insert['message'],
                        'data'      => null,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => $insert['message'],
                        'data'      => null,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_patch() {
        $this->response([
            'status'  => $this->bad,
            'message' => 'Bad Request',
            'data'    => null,  
                ], REST_Controller::HTTP_BAD_REQUEST);
    }

    public function kuesioner_get()
    {
        $header = $this->input->request_headers();

        if ($this->uri->segment(4) || $this->input->get() != null) {
            $this->response([
                'code' => REST_Controller::HTTP_BAD_REQUEST,
                'message' => "Bad Request",
                'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
            return;
        }

        if (array_key_exists('api_key', $header) && !empty($header['api_key'])) {
            // check token

            $token_key = $header['api_key'];

            $decoded_token = AUTHORIZATION::validateToken($token_key);

            if ($decoded_token != false && property_exists($decoded_token, 'id_user') && property_exists($decoded_token, 'id_group') && property_exists($decoded_token, 'akses') && $decoded_token->akses) {
                $data = $this->alumni->get_all_kuesioner_category();

                foreach ($data as $key => $value) {
                    $data[$key]['questionnaires'] =  $this->alumni->get_kuesioner_by_category($value['id_category']);
                }

                if ($data != null) {
                    $this->response([
                        'status'    => $this->ok,
                        'message'   => "Data kuesioner ditemukan",
                        'data'      => $data,
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => $this->notfound,
                        'message'   => "Data kuesioner tidak ditemukan",
                        'data'      => $data,
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            } else {
                $this->response([
                    'status'    => $this->unauthorized,
                    'message'   => 'Unathorized/Invalid Api key',
                    'data'      => null,
                ], REST_Controller::HTTP_UNAUTHORIZED);
            }
        } else {
            $this->response([
                'status'     => $this->badrequest,
                'message'    => 'Api key tidak ditemukan.',
                'data'       => null,
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function filter($params, $id_alumni = null)
    {
        $field = ['nim', 'nik','nama','tempat_lahir','tgl_lahir','id_agama','jenis_kelamin','no_hp','email','alamat','kode_prov','kode_kab_kota','kode_kecamatan','url_photo','tahun_lulus','ipk_terakhir','id_perguruan_tinggi','id_prodi','judul_skripsi','id_ref_status_alumni'];
        $jobs = ['sektor'];
        $study = ['sektor'];
        $isExist = array();
        $pesan = array();
        $jenis_kelamin = ['L','P'];
        $jalur_masuk = ['RPL','Reguler'];
        $sektor = ['Pertanian','Non Pertanian'];
        $isError = false;
        $isId = true;

        for ($i = 0; $i < count($field); $i++) {
            $isExist[$field[$i]] = true;

            if (empty($params[$field[$i]]) || $params[$field[$i]] == null || $params[$field[$i]] == '') {
                $isExist[$field[$i]] = false;
                $pesan[] = $field[$i].' belum diisi';
            }
        }

        if ($this->onUpdate && !$this->checkAlumni(array('id_alumni' => $id_alumni))) {
            $isError = true;
            $isId = false;
            $pesan[] = 'id_alumni tidak sesuai';
        }

        if (!empty($params['id_ref_status_alumni']) && ($params['id_ref_status_alumni'] == '1' || $params['id_ref_status_alumni'] == '3')) {
            for ($i=0; $i < count($jobs); $i++) { 
                $isExist[$jobs[$i]] = true;

                if (empty($params[$jobs[$i]]) || $params[$jobs[$i]] == null || $params[$jobs[$i]] == '') {
                    $isExist[$jobs[$i]] = false;
                    $pesan[] = $jobs[$i].' belum diisi';
                }
            }

            $alumniOld = $this->alumni->getAlumniById($id_alumni);
            if ($this->onUpdate && $isId && $params['id_ref_status_alumni'] == $alumniOld->id_ref_status_alumni) {
                if (empty($params['id_job_study'])) {
                    $isError = true;
                    $pesan[] = 'id_job_study belum diisi';
                } else {
                    if (!$this->checkJobStudy($params['id_ref_status_alumni'], $params['id_job_study'])) {
                        $isError = true;
                        $pesan[] = 'id_job_study tidak sesuai';
                    }
                }
            }

            if (!in_array($params['sektor'],$sektor)) {
                $isError = true;
                $pesan[] = 'sektor tidak sesuai';
            }
        } elseif (!empty($params['id_ref_status_alumni']) && $params['id_ref_status_alumni'] == '4') {
            for ($i=0; $i < count($study); $i++) { 
                $isExist[$study[$i]] = true;

                if (empty($params[$study[$i]]) || $params[$study[$i]] == null || $params[$study[$i]] == '') {
                    $isExist[$study[$i]] = false;
                    $pesan[] = $study[$i].' belum diisi';
                }
            }

            $alumniOld = $this->alumni->getAlumniById($id_alumni);
            if ($this->onUpdate && $isId && $params['id_ref_status_alumni'] == $alumniOld->id_ref_status_alumni) {
                if (empty($params['id_job_study'])) {
                    $isError = true;
                    $pesan[] = 'id_job_study belum diisi';
                } else {
                    if (!$this->checkJobStudy($params['id_ref_status_alumni'], $params['id_job_study'])) {
                        $isError = true;
                        $pesan[] = 'id_job_study tidak sesuai';
                    }
                }
            }

            if (!in_array($params['sektor'],$sektor)) {
                $isError = true;
                $pesan[] = 'sektor tidak sesuai';
            }
        }

        foreach ($isExist as $key => $value) {
            if (!$value) {
                $isError = true;
            }
        }

        if (!empty($params['nama']) && (!whitespace(trim($params['nama'])) || !onlyAlpha(trim($params['nama'])))) {
            $isError = true;
            $pesan[] = 'nama tidak sesuai';
        }

        if (!empty($params['nik']) && !validNik(trim($params['nik']))) {
            $isError = true;
            $pesan[] = 'nik tidak sesuai';
        }

        if (!empty($params['tempat_lahir']) && (!whitespace(trim($params['tempat_lahir'])) || !onlyAlpha(trim($params['tempat_lahir'])))) {
            $isError = true;
            $pesan[] = 'tempat_lahir tidak sesuai';
        }

        if (!empty($params['tgl_lahir']) && !validDate(trim($params['tgl_lahir']))) {
            $isError = true;
            $pesan[] = 'tgl_lahir tidak sesuai';
        }

        if (!empty($params['id_agama']) && !$this->checkAgama(trim($params['id_agama']))) {
            $isError = true;
            $pesan[] = 'id_agama tidak sesuai';
        }

        if (!empty($params['jenis_kelamin']) && !in_array($params['jenis_kelamin'],$jenis_kelamin)) {
            $isError = true;
            $pesan[] = 'jenis_kelamin tidak sesuai';
        }

        if (!empty($params['jalur_masuk']) && !in_array($params['jalur_masuk'],$jalur_masuk)) {
            $isError = true;
            $pesan[] = 'jalur_masuk tidak sesuai';
        }

        if (!empty($params['no_hp']) && !validHp(trim($params['no_hp']))) {
            $isError = true;
            $pesan[] = 'no_hp tidak sesuai';
        }

        if (!empty($params['email']) && !validEmail($params['email'])) {
            $isError = true;
            $pesan[] = 'email tidak sesuai';
        }

        if (!empty($params['alamat']) && (!whitespace(trim($params['alamat'])) || !notOnlyNumber(trim($params['alamat'])))) {
            $isError = true;
            $pesan[] = 'alamat tidak sesuai';
        }

        if (!empty($params['kode_prov']) && !$this->checkProvinsi(trim($params['kode_prov']))) {
            $isError = true;
            $pesan[] = 'kode_prov tidak sesuai';
        }

        if (!empty($params['kode_kab_kota']) && !$this->checkKab(trim($params['kode_prov']), trim($params['kode_kab_kota']))) {
            $isError = true;
            $pesan[] = 'kode_kab_kota tidak sesuai';
        }

        if (!empty($params['kode_kecamatan']) && !$this->checkKecamatan(trim($params['kode_kab_kota']), trim($params['kode_kecamatan']))) {
            $isError = true;
            $pesan[] = 'kode_kecamatan tidak sesuai';
        }

        if (!empty($params['url_photo']) && !validUrl($params['url_photo'])) {
            $isError = true;
            $pesan[] = 'url_photo tidak sesuai';
        }

        if (!empty($params['tahun_lulus']) && !$this->checkTahunLulus($params['tahun_lulus'])) {
            $isError = true;
            $pesan[] = 'tahun_lulus tidak sesuai';
        }

        if (!empty($params['ipk_terakhir']) && !lessEqual($params['ipk_terakhir'])) {
            $isError = true;
            $pesan[] = 'ipk_terakhir tidak boleh kurang dari 0.00 atau lebih dari 4.00';
        }

        if (!empty($params['id_perguruan_tinggi']) && !$this->checkPerguruanTinggi($params['id_perguruan_tinggi'], 'id_perguruan_tinggi')) {
            $isError = true;
            $pesan[] = 'id_perguruan_tinggi tidak sesuai';
        }

        if (!empty($params['id_prodi']) && !$this->checkProdi($params['id_prodi'], $params['id_perguruan_tinggi'])) {
            $isError = true;
            $pesan[] = 'id_prodi tidak sesuai';
        }

        if (!empty($params['id_ref_status_alumni']) && !$this->checkStatus($params['id_ref_status_alumni'])) {
            $isError = true;
            $pesan[] = 'id_ref_status_alumni tidak sesuai';
        }

        return array('error' => $isError, 'pesan' => $pesan);
    }

    public function checkAlumni($where, $id = null)
    {
        $check = $this->alumni->cek_alumni($where,$id);
        if ($check > 0) {
            return true;
        }

        return false;
    }

    public function checkUser($where, $id = null)
    {
        $check = $this->alumni->cek_user($where,$id);
        if ($check > 0) {
            return true;
        }

        return false;
    }

    public function checkPerguruanTinggi($value, $col)
    {
        $check = $this->alumni->cek_perguruan_tinggi(array($col => $value));

        if ($check > 0) {
            return true;
        }

        return false;
    }

    public function checkProdi($idProdi, $idPerguruan)
    {
        $check = $this->alumni->cek_prodi(array('id_perguruan_tinggi' => $idPerguruan,'id_prodi' => $idProdi));

        if ($check > 0) {
            return true;
        }

        return false;
    }

    public function checkAgama($id)
    {
        $check = $this->alumni->cek_agama(array('id_agama' => $id));

        if ($check > 0) {
            return true;
        }

        return false;
    }

    public function checkProvinsi($kode_prov)
    {
        $check = $this->alumni->cek_provinsi(array('kode_provinsi' => $kode_prov));

        if ($check > 0) {
            return true;
        }

        return false;
    }

    public function checkKab($kode_prov, $kode_kab)
    {
        $check = $this->alumni->cek_kab(array('kode_provinsi' => $kode_prov, 'kode_kab_kota' => $kode_kab));

        if ($check > 0) {
            return true;
        }

        return false;
    }

    public function checkKecamatan($kode_kab, $kode_kecamatan)
    {
        $check = $this->alumni->cek_kecamatan(array('kode_kab_kota' => $kode_kab, 'kode_kecamatan' => $kode_kecamatan));

        if ($check > 0) {
            return true;
        }

        return false;
    }

    public function checkTahunLulus($tahun)
    {
        $check = $this->alumni->cek_tahun_lulus(array('tahun' => $tahun));

        if ($check > 0) {
            return true;
        }

        return false;
    }

    public function checkStatus($id)
    {
        $check = $this->alumni->cek_status_alumni(array('id' => $id));

        if ($check > 0) {
            return true;
        }

        return false;
    }

    public function checkJobStudy($id_ref_status_alumni, $id)
    {
        $check = $this->alumni->cek_jobs(array('id_job' => $id));
        if ($id_ref_status_alumni == 4) {
            $check = $this->alumni->cek_study(array('id_study' => $id));
        }

        if ($check > 0) {
            return true;
        }

        return false;
    }
}
