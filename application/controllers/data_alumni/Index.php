<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "landing_app";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_profil','alumni');
        $this->load->model('M_statistik','statistik');
        $this->load->model('M_setting','setting');
    }

    public function index()
    {
        $tahun = setTahun();
        $this->data['title'] = 'alumni';
        $this->data['is_full'] = true;
        $this->data['tahun_lulus'] = setTahun();
        $this->data['fakultas'] = setFakultas();
        $this->data['perguruan_tinggi'] = setJenjangPendidikan();
        $this->data['selectStatusAlumni'] = $this->alumni->getStatusALumniAll();
        $this->data['copyright'] = $copyright = $this->setting->copyright();
        $this->data['about_us'] = $about_us = $this->setting->about_us();
        $this->data['logo_utama'] = $logo_utama = $this->setting->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->setting->logo_title();
        $this->data['warna_tema'] = $warna_tema = $this->setting->warna_tema();
        $this->data['footer'] = $this->setting->footer();
        $this->data['header'] = $this->setting->header();
        $this->data['perguruan_tinggi'] = $perguruan_tinggi = $this->setting->perguruan_tinggi();
        $social_media = $this->setting->social_media();
        $sosial = json_decode($social_media->deskripsi);
        
        $this->data['social_media'] = $sosial;
        $this->data['total'] = $this->alumni->getCountAlumniBy(null);

        $this->renderTo('alumni/search');
    }

    public function _callbackStatus($value, $row)
    {
        $status = $this->alumni->get_status_alumni(array('id' => $row->id_ref_status_alumni));
        switch ($row->id_ref_status_alumni) {
            case 1:
                return '<span class="badge" style="background-color:#89C64C;border-radius: 100px;">' . $status->status_alumni . '</span>';
                break;

            case 2:
                return '<span class="badge" style="background-color:#F6C566;border-radius: 100px;">' . $status->status_alumni . '</span>';
                break;

            case 3:
                return '<span class="badge" style="background-color:#5BEB7B;border-radius: 100px;">' . $status->status_alumni . '</span>';
                break;

            case 4:
                return '<span class="badge" style="background-color:#2D8CFF;border-radius: 100px;">' . $status->status_alumni . '</span>';
                break;

            case 5:
                return '<span class="badge" style="background-color:#F6C566;border-radius: 100px;">' . $status->status_alumni . '</span>';
                break;

            default:
                return "-";
                break;
        }
    }

    public function getStatusAlumni()
    {
        $status = $this->alumni->getStatusALumniAll();
        $return = array();

        foreach ($status as $key => $value) {
            $return[] = $value->status_alumni;
        }

        return $return;
    }

    public function getGrafikJenisKelamin($params)
    {
        $laki_laki = $this->statistik->countLaki($params);
        $perempuan = $this->statistik->countPerempuan($params);
        $grafik = array();
        $data = array();
        $grafik[0] = array(
            'name' => 'LAKI-LAKI',
            'y' => $laki_laki,
            'color' => 'rgb(144,237,125)'
        );
        $grafik[1] = array(
            'name' => 'PEREMPUAN',
            'y' => $perempuan,
            'color' => 'rgb(247,163,92)'
        );
        return $grafik;
    }

    public function getGrafikGenderByStatusAlumni($params)
    {
        $laki_laki = $this->statistik->countLakiStatus($params);
        $perempuan = $this->statistik->countPerempuanStatus($params);
        $grafik = array();

        $grafik[0] = array(
            'name' => 'LAKI-LAKI',
            'y' => $laki_laki,
            'color' => 'rgb(144,237,125)'
        );
        $grafik[1] = array(
            'name' => 'PEREMPUAN',
            'y' => $perempuan,
            'color' => 'rgb(144,237,125)'
        );
    
        return $grafik;
    }

    public function verifikasiAlumni()
    {
        $this->data['title'] = 'Beranda';
        $this->data['is_full'] = true;
        $this->renderTo('alumni/verifikasi-alumni');
    }

    public function getTahun()
    {
        $response['status'] = 0;
        $response['data'] = [];

        $tahun = setTahun();

        $current = date('Y');
        $current = $current - 1;
        $this->session->set_userdata('filter_tahun', $current);
        $data = [];
        if (!empty($tahun)) {
            $no = 0;
            foreach ($tahun as $k => $rows) {
                $data[$no]['tahun'] = $rows;
                $data[$no]['is_current'] = ($rows == $current ? '1' : '0');
                $no++;
            }

            $response['status'] = 201;
            $response['data'] = $data;
        } else {
            $response['status'] = 500;
            $response['data'] = [];
        }
        responseJson($response['status'], $response);
    }

    public function changeProdi()
    {
        $prodi = $this->alumni->getProdiById($this->input->post('prodi'));
        $this->session->set_userdata('filter_prodi', $this->input->post('prodi'));
        responseJson(201, [
            'data' => [
                'status' => 201,
                'message' => 'berhasil',
                'prodi' => $prodi->nama_prodi,
            ],
        ]);
    }

    public function changeYears()
    {
        $prodi_output = '';
        $this->session->set_userdata('filter_tahun', $this->input->post('tahun'));
        if (!empty($this->input->post('prodi'))) {
            $prodi = $this->alumni->getProdiById($this->input->post('prodi'));
            $prodi_output = $prodi->nama_prodi;
        }
        responseJson(201, [
            'data' => [
                'status' => 201,
                'message' => 'berhasil',
                'prodi' => $prodi_output,
            ]
        ]);
    }

    public function statistik()
    {
        $params = $this->input->post(null, true);
        $result = $this->statistik->statistik($params);

        responseJson($result['status'], $result);
    }

    public function grafikGender()
    {
        $result['status'] = 201;

        $params = $this->input->post(null, true);
        $data['g_status_alumni_kelamin'] = $this->getGrafikJenisKelamin($params);

        $result['grafik'] = $data;

        responseJson($result['status'], $result);
    }

    public function grafikStatusAlumni()
    {
        $params = $this->input->post(null, true);
        $data['g_gender_by_status'] = $this->getGrafikGenderByStatusAlumni($params);

        responseJson(201, [
            'status' => 201,
            'data'  => $data,
        ]);
    }

    public function getProdi()
    {
        $id_fakultas = $this->input->get('kode_prodi');
        $fakultas = $this->alumni->getFakultasName($id_fakultas);
        $fakultas_output = '';
        if (!empty($fakultas)) {
            $fakultas_output = $fakultas->nama_fakultas;
        }
        $prodi = setProdiByFakultas($id_fakultas);
        // echo json_encode($prodi);
        responseJson(201, [
            'status' => 201,
            'data'  => [
                'fakultas' => $fakultas_output,
                'prodi' => $prodi,
            ]
        ]);
    }
}
