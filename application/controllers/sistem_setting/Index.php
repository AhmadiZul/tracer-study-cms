<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Index extends BaseController
{
    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "sistem_setting";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title'] = '<i class="fa fa-clipboard-list"></i> Sistem Setting';
        $this->data['is_home'] = false;
        $this->render('index');
    }
}
