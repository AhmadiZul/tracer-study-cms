<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Index extends BaseController
{
    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "tracer_alumni";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_tracer');
        $this->load->model('M_setting', 'setting');
    }

    public function index()
    {
        $this->data['icon'] = '<i class="fa fa-clipboard-list"></i>';
        $this->data['title'] = 'Tracer';
        $this->data['is_home'] = false;
        $this->data['logo_title'] = $logo_title= $this->setting->logo_title();
        $this->data['logo_utama'] = $logo_utama= $this->setting->logo_utama();
        $this->data['warna_tema']  = $warna_tema = $this->setting->warna_tema();
        $social_media = $this->setting->social_media();
        $sosial = json_decode($social_media->deskripsi);
        $this->data['social_media'] = $sosial;
        $this->render('index');
    }

    public function getDaftarKuesioner()
    {
        $alumni = $this->m_tracer->get_alumni_by_id_alumni($this->session->userdata("t_alumniId"));
        $where = array(
            'id_alumni' => $this->session->userdata("t_alumniId"),
            'status_data' => $alumni->status_data,
            'untuk_id_prodi' => $alumni->id_prodi,
            'untuk_tahun_lulus' => $alumni->tahun_lulus, 
        );
        $jadwal = $this->m_tracer->get_daftar_kuesioner($where);
        echo json_encode($jadwal);
    }
    
}
