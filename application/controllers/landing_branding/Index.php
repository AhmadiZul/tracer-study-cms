<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "branding_app";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_mitra','mitra');
        
        
   
    }

    public function index()
    {
        $this->data['title'] = 'Beranda';
        $this->data['is_full'] = true;
        $this->data['mitra'] =  $mitra = $this->mitra->getMitra();
        $this->renderTo('index/landing_page');
    }
}