<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "login_app";
    protected $module = "register";

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("t_idGroup")==4) {
            redirect('alumni/dashboard');
        }
        if ($this->session->userdata("t_idGroup")==3) {
            redirect('mitra/dashboard');
        }
        $this->load->library('form_validation');
        $this->load->model('m_register');
    }

    public function index()
    {
        $this->data['title'] = 'Registrasi Alumni';
        $this->data['is_full'] = true;
        $this->data['provinsi'] = setPronvisi();
        $this->data['jenjang'] = setJenjangPendidikan();
        $this->data['tahun'] = setTahun();
        $this->data['status_alumni'] = setStatusAlumni();
        $this->data['agama'] = setAgama();
        $this->renderTo('auth/register/index');
    }

    public function getKabKota()
    {
        $kode_provinsi = $this->input->get('kode_provinsi');
        $kabKota = setKabupaten($kode_provinsi);
        echo json_encode($kabKota);
    }

    public function getKecamatan()
    {
        $kode_kab_kota = $this->input->get('kode_kab_kota');
        $kecamatan = setKecamatan($kode_kab_kota);
        echo json_encode($kecamatan);
    }

    public function getProdi()
    {
        $kode_prodi = $this->input->get('kode_prodi');
        $prodi = setProdi($kode_prodi);
        echo json_encode($prodi);
    }

    public function checkDate($date)
    {
        $current = date('Y-m-d');
        if ($date>$current) {
            return false;
        }
        return true;
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        }else {
            return true;
        }
    }
    public function notOnlyNumber($str)
    {
        if (ctype_digit($str)) {
            return false;
        }
        return true;
    }

    public function simpan_registrasi()
    {
        $dataI = $this->input->post();
        $nama_instansi = '';
        $prodi = '';
        $tgl_mulai = '';
        $jabatan = '';
        $sektor = '';
        $instansi_kode_prov = '';
        $instansi_kode_kab = '';
        $instansi_kode_kec = '';
        $instansi_alamat = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        if ($dataI['status_alumni'] == '1') {
            $nama_instansi = trim($dataI['nama_instansi']);
            $tgl_mulai = $dataI['tgl_mulai'];
            $jabatan = $dataI['jabatan'];
            $sektor = $dataI['sektor'];
            $instansi_kode_prov = $dataI['instansi_kode_prov'];
            $instansi_kode_kab = $dataI['instansi_kode_kab'];
            $instansi_kode_kec = $dataI['instansi_kode_kec'];
            $instansi_alamat = trim($dataI['instansi_alamat']);
        }

        if ($dataI['status_alumni'] == '3') {
            $nama_instansi = trim($dataI['nama_instansi']);
            $tgl_mulai = $dataI['tgl_mulai'];
            $sektor = $dataI['sektor'];
            $instansi_kode_prov = $dataI['instansi_kode_prov'];
            $instansi_kode_kab = $dataI['instansi_kode_kab'];
            $instansi_kode_kec = $dataI['instansi_kode_kec'];
            $instansi_alamat = trim($dataI['instansi_alamat']);
        }

        if ($dataI['status_alumni'] == '4') {
            $nama_instansi = trim($dataI['nama_instansi']);
            $tgl_mulai = $dataI['tgl_mulai'];
            $prodi = trim($dataI['prodi']);
            $sektor = $dataI['sektor'];
            $instansi_kode_prov = $dataI['instansi_kode_prov'];
            $instansi_kode_kab = $dataI['instansi_kode_kab'];
            $instansi_kode_kec = $dataI['instansi_kode_kec'];
            $instansi_alamat = trim($dataI['instansi_alamat']);
        }

        $this->db->trans_begin();
        $dbError = [];

        $dataUser = array(
            'username'  => trim($dataI['email']),
            'real_name' => trim($dataI['nama']),
            'email'     => trim($dataI['email']),
            'id_group'  => "4",
            'password'  => hash('sha256', $dataI['password']),
        );

        $id_user = $this->m_register->simpan_user($dataUser);
        $dbError[] = $this->db->error();

        $id_alumni = getUUID();
        $id_job = getUUID();
        $id_study = getUUID();

        $data = array(
            'id_alumni'                => $id_alumni,
            'nama'                  => trim($dataI['nama']),
            'nim'                   => trim($dataI['nim']),
            'nik'                   => trim($dataI['nik']),
            'no_hp'                 => trim($dataI['no_hp']),
            'tempat_lahir'          => trim($dataI['tempat_lahir']),
            'tgl_lahir'             => trim($dataI['tgl_lahir']),
            'id_agama'             => trim($dataI['id_agama']),
            'jenis_kelamin'         => trim($dataI['jenis_kelamin']),
            'jalur_masuk'           => trim($dataI['jalur_masuk']),
            'email'                 => trim($dataI['email']),
            'kode_prov'             => $dataI['kode_provinsi'],
            'kode_kab_kota'         => $dataI['kode_kabupaten'],
            'kode_kecamatan'        => $dataI['kode_kecamatan'],
            'alamat'                => trim($dataI['alamat']),
            'tahun_lulus'        => trim($dataI['tahun_lulus']),
            'id_perguruan_tinggi'   => $dataI['perguruan_tinggi'],
            'id_prodi'              => $dataI['kode_prodi'],
            'ipk_terakhir'          => $dataI['ipk_terakhir'],
            'judul_skripsi'         => trim($dataI['judul_skripsi']),
            'id_ref_status_alumni'  => $dataI['status_alumni'],
            'status_data'           => '0',
            'is_active'             => '1',
            'last_modified_user'    => $id_user,
            'id_user'               => $id_user,
        );

        $this->m_register->simpan_alumni($data);
        $dbError[] = $this->db->error();

        if ($dataI['status_alumni'] == 1 || $dataI['status_alumni'] == 3) {
            $dataJobs = array(
                'id_job'                => $id_job,
                'id_alumni'                => $id_alumni,
                'id_ref_status_alumni'  => $dataI['status_alumni'],
                'nama_instansi'         => trim($nama_instansi),
                'tgl_mulai'             => trim($tgl_mulai),
                'jabatan'               => trim($jabatan),
                'sektor'                => trim($sektor),
                'kode_provinsi'         => $instansi_kode_prov,
                'kode_kab_kota'         => $instansi_kode_kab,
                'kode_kecamatan'        => $instansi_kode_kec,
                'alamat'                => trim($instansi_alamat),
                'created_by'            => $id_user,
                'last_modified_by'      => $id_user
            );

            $this->m_register->simpan_alumnijobs($dataJobs);
            $dbError[] = $this->db->error();
        }

        if ($dataI['status_alumni'] == 4) {
            $dataStudy = array(
                'id_study'              => $id_study,
                'id_alumni'                => $id_alumni,
                'nama_perguruan_tinggi' => trim($nama_instansi),
                'tgl_mulai'             => trim($tgl_mulai),
                'prodi'                 => trim($prodi),
                'sektor_prodi'          => trim($sektor),
                'kode_provinsi'         => $instansi_kode_prov,
                'kode_kab_kota'         => $instansi_kode_kab,
                'kode_kecamatan'        => $instansi_kode_kec,
                'created_by'            => $id_user,
                'last_modified_by'      => $id_user
            );

            $this->m_register->simpan_alumnistudy($dataStudy);
            $dbError[] = $this->db->error();
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal registrasi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil registrasi',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function registrasi_berhasil()
    {
        $this->data['title'] = 'Registrasi';
        $this->renderTo('auth/register/berhasil');
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('nama', 'Nama lengkap', 'trim|required|callback_notOnlyNumber|callback_whitespace|max_length[60]');
        $this->form_validation->set_rules('nim', 'NIM/NIRM', 'trim|required|is_unique[alumni.nim]|is_unique[user.username]|max_length[20]');
        $this->form_validation->set_rules('nik', 'NIK', 'trim|required|min_length[16]|max_length[16]|is_unique[alumni.nik]');
        $this->form_validation->set_rules('no_hp', 'No HP', 'trim|required|max_length[13]|min_length[11]');
        $this->form_validation->set_rules('tempat_lahir', 'Tempat lahir', 'trim|required|callback_whitespace|callback_notOnlyNumber');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'trim|required|callback_checkDate');
        $this->form_validation->set_rules('id_agama', 'Agama', 'trim|required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis kelamin', 'trim|required');
        $this->form_validation->set_rules('jalur_masuk', 'Jalur masuk', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|callback_notOnlyNumber|callback_whitespace|max_length[255]');
        $this->form_validation->set_rules('kode_provinsi', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('kode_kabupaten', 'Kabupaten/Kota', 'trim|required');
        $this->form_validation->set_rules('kode_kecamatan', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('tahun_lulus', 'Tahun Lulus', 'trim|required');
        $this->form_validation->set_rules('perguruan_tinggi', 'Jenjang pendidikan', 'trim|required');
        $this->form_validation->set_rules('kode_prodi', 'Prodi/Jurusan', 'trim|required');
        $this->form_validation->set_rules('ipk_terakhir', 'IPK terakhir', 'trim|required|less_than_equal_to[4]');
        $this->form_validation->set_rules('judul_skripsi', 'Judul skripsi', 'trim|required|callback_notOnlyNumber|callback_whitespace|max_length[1000]');
        $this->form_validation->set_rules('status_alumni', 'Status alumni', 'trim|required');

        if (empty($this->input->post('jenis_kelamin'))) {
            $errors[] = array(
                'field' => 'jenis_kelamin',
                'message' => 'Jenis kelamin wajib dipilih'
            );
        }

        if (empty($this->input->post('jalur_masuk'))) {
            $errors[] = array(
                'field' => 'jalur_masuk',
                'message' => 'Jalur masuk wajib dipilih'
            );
        }

        if ($this->input->post('status_alumni') == '1') {
            $this->form_validation->set_rules('nama_instansi', 'Instansi', 'trim|required|callback_whitespace|callback_notOnlyNumber|max_length[255]');
            $this->form_validation->set_rules('tgl_mulai', 'Tanggal Masuk', 'trim|required');
            $this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required|max_length[255]');
            $this->form_validation->set_rules('sektor', 'sektor', 'required');
            $this->form_validation->set_rules('instansi_kode_prov', 'Provinsi instansi', 'required');
            $this->form_validation->set_rules('instansi_kode_kab', 'Kabupaten/Kota instansi', 'required');
            $this->form_validation->set_rules('instansi_kode_kec', 'Kecamatan instansi', 'required');
            $this->form_validation->set_rules('instansi_alamat', 'Alamat instansi', 'trim|required|callback_whitespace|callback_notOnlyNumber|max_length[255]');

            if (empty($this->input->post('instansi_kode_kec'))) {
                $errors[] = array(
                    'field' => 'instansi_kode_kec',
                    'message' => 'Kecamatan instansi belum diisi'
                );
            }

            if (empty($this->input->post('sektor'))) {
                $errors[] = array(
                    'field' => 'sektor',
                    'message' => 'Sektor wajib dipilih'
                );
            }
        }
        if ($this->input->post('status_alumni') == '3') {
            $this->form_validation->set_rules('nama_instansi', 'Nama usaha', 'trim|required|callback_whitespace|callback_notOnlyNumber|max_length[255]');
            $this->form_validation->set_rules('tgl_mulai', 'Tanggal Masuk', 'trim|required');
            $this->form_validation->set_rules('sektor', 'sektor', 'required');
            $this->form_validation->set_rules('instansi_kode_prov', 'Provinsi instansi', 'required');
            $this->form_validation->set_rules('instansi_kode_kab', 'Kabupaten/Kota instansi', 'required');
            $this->form_validation->set_rules('instansi_kode_kec', 'Kecamatan instansi', 'required');
            $this->form_validation->set_rules('instansi_alamat', 'Alamat usaha', 'trim|required|callback_whitespace|callback_notOnlyNumber|max_length[255]');

            if (empty($this->input->post('instansi_kode_kec'))) {
                $errors[] = array(
                    'field' => 'instansi_kode_kec',
                    'message' => 'Kecamatan instansi belum diisi'
                );
            }

            if (empty($this->input->post('sektor'))) {
                $errors[] = array(
                    'field' => 'sektor',
                    'message' => 'Sektor wajib dipilih'
                );
            }
        }
        if ($this->input->post('status_alumni') == '4') {
            $this->form_validation->set_rules('nama_instansi', 'Nama perguruan tinggi', 'trim|required|callback_whitespace|callback_notOnlyNumber|max_length[255]');
            $this->form_validation->set_rules('tgl_mulai', 'Tanggal Masuk', 'trim|required');
            $this->form_validation->set_rules('prodi', 'Prodi', 'trim|required|callback_whitespace|callback_notOnlyNumber|max_length[50]');
            $this->form_validation->set_rules('sektor', 'sektor', 'required');
            $this->form_validation->set_rules('instansi_kode_prov', 'Provinsi', 'required');
            $this->form_validation->set_rules('instansi_kode_kab', 'Kabupaten/Kota', 'required');
            $this->form_validation->set_rules('instansi_kode_kec', 'Kecamatan', 'required');

            if (empty($this->input->post('instansi_kode_kec'))) {
                $errors[] = array(
                    'field' => 'instansi_kode_kec',
                    'message' => 'Kecamatan instansi belum diisi'
                );
            }

            if (empty($this->input->post('sektor'))) {
                $errors[] = array(
                    'field' => 'sektor',
                    'message' => 'Sektor wajib dipilih'
                );
            }
        }

        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('konfirmasi_password', 'Konfirmasi password', 'trim|required|matches[password]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[alumni.email]|is_unique[user.email]|max_length[50]');

        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('less_than_equal_to', '{field} harus kurang atau sama dengan {param}.00');
        $this->form_validation->set_message('checkDate', '{field} tidak boleh lebih dari tanggal hari ini');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');

		if ($this->form_validation->run() == FALSE) {
			foreach ($this->input->post() as $field => $value) {
				if (form_error($field)) {
					$errors[] = [
						'field'   => $field,
						'message' => trim(form_error($field, ' ', ' ')),
					];
				};
			};
		};

		return $errors;
    }
}
