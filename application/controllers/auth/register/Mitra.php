<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Mitra extends BaseController
{

    public $loginBehavior = false;
    public $template = "login_app";
    protected $module = "register";

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata("t_idGroup")==4) {
            redirect('alumni/dashboard');
        }
        if ($this->session->userdata("t_idGroup")==3) {
            redirect('mitra/dashboard');
        }
        $this->load->library('form_validation');
        $this->load->model('m_register');
    }

    public function index()
    {
        $this->data['title'] = 'Registrasi Mitra';
        $this->data['jenjang'] = setJenjangPendidikan();
        $this->renderTo('auth/register/mitra');
    }

    public function getProdi()
    {
        $kode_prodi = $this->input->get('kode_prodi');
        $prodi = setProdi($kode_prodi);
        echo json_encode($prodi);
    }

    public function checkDate($date)
    {
        $current = date('Y-m-d');
        if ($date>$current) {
            return false;
        }
        return true;
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        }else {
            return true;
        }
    }
    public function notOnlyNumber($str)
    {
        if (ctype_digit($str)) {
            return false;
        }
        return true;
    }

    function _urlphoto($id_instansi)
    {
        $id_instansi = str_replace("-", "", $id_instansi);

        if (!file_exists('admin/public/uploads/mitra/')) {
            mkdir('admin/public/uploads/mitra/', 0755, true);
        }
        if (!file_exists('admin/public/uploads/mitra/' .$id_instansi)) {
            mkdir('admin/public/uploads/mitra/'.$id_instansi, 0755,  true);
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'admin/public/uploads/mitra/'.$id_instansi; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'Logo' . $id_instansi;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = base_url() . 'admin/public/uploads/mitra/'.$id_instansi.'/'. $file['file_name'];
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function simpan_registrasi()
    {
        $dataI = $this->input->post();

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $dataUser = array(
            'username'  => trim($dataI['email']),
            'real_name' => trim($dataI['nama']),
            'email'     => trim($dataI['email']),
            'id_group'  => "3",
            'password'  => hash('sha256', $dataI['password']),
        );

        $id_user = $this->m_register->simpan_user($dataUser);
        $dbError[] = $this->db->error();

        $id_instansi = getUUID();

        $data = array(
            'id_instansi'           => $id_instansi,
            'nama'                  => trim($dataI['nama']),
            'telephone'             => trim($dataI['telephone']),
            'url_web'             => trim($dataI['website']),
            'jenis'                 => trim($dataI['jenis']),
            'sektor'                => trim($dataI['sektor']),
            'email'                 => trim($dataI['email']),
            'alamat'                => trim($dataI['alamat']),
            'status_data'           => '0',
            'is_active'             => '1',
            'created_by'            => $id_user,
            'last_modified_by'      => $id_user,
            'id_user'               => $id_user,
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_instansi);
            $data['url_logo'] = $urlPhoto['file_name'];
        }

        $this->m_register->simpan_mitra($data);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal registrasi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil registrasi',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function registrasi_berhasil()
    {
        $this->data['title'] = 'Registrasi';
        $this->renderTo('auth/register/berhasil');
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('nama', 'Nama mitra', 'trim|required|callback_notOnlyNumber|callback_whitespace');
        $this->form_validation->set_rules('jenis', 'Jenis', 'trim|required');
        $this->form_validation->set_rules('sektor', 'Sektor', 'trim|required');
        $this->form_validation->set_rules('telephone', 'Telephone', 'trim|required');
        $this->form_validation->set_rules('website', 'Website', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|callback_notOnlyNumber|callback_whitespace');

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[alumni.email]|is_unique[user.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('konfirmasi_password', 'Konfirmasi password', 'trim|required|matches[password]');

        if ($_FILES['url_photo']['name'] == "") {
            $errors[] = [
                'field'   => 'url_photo',
                'message' => 'Logo harus diisi',
            ];
        }

        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('less_than_equal_to', '{field} harus kurang atau sama dengan {param}.00');
        $this->form_validation->set_message('checkDate', '{field} tidak boleh lebih dari tanggal hari ini');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');

		if ($this->form_validation->run() == FALSE) {
			foreach ($this->input->post() as $field => $value) {
				if (form_error($field)) {
					$errors[] = [
						'field'   => $field,
						'message' => trim(form_error($field, ' ', ' ')),
					];
				};
			};
		};

		return $errors;
    }
}
