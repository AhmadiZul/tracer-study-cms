<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "company_talk";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('tanggalindo');
        $this->load->model('M_talk', 'talk');
    }

    public function index()
    {
        switch (getSessionRoleAccess()) {
            case '3':
                $this->mitra();
                break;

            case '4':
                $this->alumni();
                break;

            default:
                $this->mitra();
                break;
        }

    }

    public function mitra()
    {
        $this->data['title'] = 'Data Company Talk';
        $this->data['is_home'] = false;
        $this->data['scripts'] = ['company_talk/js/index.js'];

        $referensi = $this->initData();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Company Talk');
        $crud->setTable('company_talks');
        $crud->setRelation('id_perguruan_tinggi_lokasi', 'ref_perguruan_tinggi', 'nama_pendek');
        /* $crud->setRelation('id_perguruan_tinggi_lokasi', 'ref_perguruan_tinggi', 'nama_pendek', ['id_user' => $this->session->userdata('t_userId')]); */

        $crud->where(['id_mitra' => getMitraID()]);
        $crud->columns($referensi['columns']);
        $crud->displayAs($referensi['displays']);


        $crud->callbackColumn('waktu_awal_daftar', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('waktu_akhir_daftar', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('tanggal_pelaksanaan', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->fieldType('is_approved', 'dropdown_search', [
            '0' => 'Belum Diverifikasi',
            '1' => 'Disetujui',
            '2' => 'Tidak Disetujui',
        ]);
        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->unsetDelete();
        $crud->callbackColumn('approved_by', [$this, 'callbackActionColumn']);
        /* $crud->callbackColumn('fasilitas', [$this, 'callbackActionFasilitas']); */
        $crud->unsetExportPdf();
        $crud->unsetPrint();
        $output = $crud->render();
        $this->_setOutput('company_talk/index/index', $output);
    }

    public function halamanTambah()
    {
        $this->data['title'] = 'Tambah Company Talk';
        $this->data['perguruan_tinggi'] = setJenjangPendidikan($this->session->userdata('t_userId'));
        $this->data['is_home'] = false;
        $this->render('tambah');
    }

    public function halamanEdit()
    {
        if (!empty($this->input->get('key'))) {
            # code...
            $this->data['data']     = $data =$this->talk->edit($this->input->get('key'));;
            $this->data['title']    = 'Edit Company Talk';
            $this->data['perguruan_tinggi'] = setJenjangPendidikan($this->session->userdata('t_userId'));
            $this->data['is_home']  = false;
            $this->render('edit');
        } else {
            $this->data['title']    = 'Data tidak ditemukan';
            $this->data['is_home']  = false;
            $this->render('not-found');
        }
    }

    public function alumni()
    {
        $this->data['title'] = 'Data Pendaftaran Company Talk';
        $this->data['is_home'] = false;
        $this->data['scripts'] = ['company_talk/js/alumni.js'];
        $this->render('alumni');
    }

    public function callbackAddFasilitas($type, $name)
    {
        $fasilitas = $this->talk->getFasilitas(null, false);
        if ($fasilitas['status'] == 201) {
            $html = '';
            foreach ($fasilitas['data'] as $k => $v) {
                $html .= '<div class="custom-control custom-checkbox">
                      <input type="checkbox" name="fasilitas[]" class="custom-control-input" value="'.$v->id.'" id="customCheck'.$v->id.'">
                      <label class="custom-control-label" for="customCheck'.$v->id.'">'.$v->nama.'</label>
                    </div>';
            }
            return $html;
        }

    }

    public function callbackActionFasilitas($value, $rows)
    {
        if ($value != '' || $value != null) {
            $result = json_decode($value, true);
            $id = [];
            foreach ($result as $key => $value) {
                $id[$key] = $value['fasilitas'];
            }
            $result = $this->talk->getFasilitas($id,false);
            if ($result['status'] == 201) {
                $fasilitas = '<ol>';
                foreach ($result['data'] as $rows ) {
                    $fasilitas .= '<li>'.$rows->nama.'</li>';
                }
                $fasilitas .= '</ol>';
                return $fasilitas;
            }else{
                return 'Tidak ada permintaan fasilitas';
            }
        }else{
            return '-';
        }
    }

    public function callbackActionColumn($value, $rows)
    {
        $return = '<div class="btn-group">';
        $return .= '<a href="'.site_url('company_talk/index/detail?key='.$rows->id).'" class="btn btn-warning"><i class="fas fa-th-list"></i></a>';
        if ($rows->is_approved == '0') {
            $return .= '<a class="btn btn-info" href="'.site_url('company_talk/index/halamanEdit?key='.$rows->id).'"><i class="fas fa-edit"></i></a>';
            $return .= '<button class="btn btn-danger" onclick="return hapusCompanyTalk('."'".$rows->id."'".')" data-toggle="tooltip" data-placement="top" title="hapus pendaftaran company talk"><i class="fas fa-trash"></i></button>';
        }
        $return .= '</div>';
        return $return;
    }


    public function initData()
    {

        $columns = [
            'approved_by',
            'judul_presentasi',
            'waktu_awal_daftar',
            'waktu_akhir_daftar',
            'tanggal_pelaksanaan',
            'id_perguruan_tinggi_lokasi',
            'di_ruang',
            'kapasitas',
            'fasilitas',
            'is_approved',
        ];

        $fields = [
            'judul_presentasi',
            'deskripsi',
            'waktu_awal_daftar',
            'waktu_akhir_daftar',
            'tanggal_pelaksanaan',
            'id_perguruan_tinggi_lokasi',
            'di_ruang',
            'kapasitas',
            'fasilitas',
            'fasilitas_tambahan',
        ];
        $requireds = [
            'judul_presentasi',
            'deskripsi',
            'waktu_awal_daftar',
            'waktu_akhir_daftar',
            'tanggal_pelaksanaan',
            'id_perguruan_tinggi_lokasi',
            'di_ruang',
            'kapasitas',
            'fasilitas_tambahan',
        ];

        $displays = [
            'deskripsi'                  => 'Deskripsi',
            'approved_by'                => 'Kelola',
            'id_mitra'                   => 'Mitra',
            'judul_presentasi'           => 'Judul Presentasi',
            'waktu_awal_daftar'          => 'Awal Pendaftaran',
            'waktu_akhir_daftar'         => 'Akhir Pendaftaran',
            'tanggal_pelaksanaan'        => 'Pelaksanaan',
            'id_perguruan_tinggi_lokasi' => 'Lokasi',
            'di_ruang'                   => 'Ruang',
            'kapasitas'                  => 'Kapasitas',
            'fasilitas'                  => 'Fasilitas',
            'fasilitas_tambahan'         => 'Fasilitas Tambahan',
            'is_approved'                => 'Status',
        ];

        $data['fields']     = $fields;
        $data['columns']    = $columns;
        $data['requireds']  = $requireds;
        $data['displays']   = $displays;
        return $data;
    }

    public function detail()
    {
        if (!empty($this->input->get('key'))) {
            # code...
            $this->data['data']     = $data =$this->talk->detail($this->input->get('key'));;
            $this->data['title']    = 'Detail Company Talk'.($data['status'] == 201 ? ' '.$data['data']->judul_presentasi : '');
            $this->data['is_home']  = false;
            if (getSessionRoleAccess() == 4) {
                $this->data['registered']  = $this->talk->checkRegistered($this->input->get('key'));
                $this->data['scripts']  = ['company_talk/js/daftar.js'];
            }else{
                $this->data['scripts']  = ['company_talk/js/detail.js'];
            }
            $this->render('detail');
        } else {
            $this->data['title']    = 'Data tidak ditemukan';
            $this->data['is_home']  = false;
            $this->render('not-found');
        }
    }


    public function fasilitas()
    {
        $result = $this->talk->getFasilitas(null, false);
        responseJson($result['status'], $result);
    }
    public function universitas()
    {
        $result = $this->talk->getUniversitas(null, false);
        responseJson($result['status'], $result);
    }

    public function simpan()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

       /*  $input = $this->input->post('judul_presentasi');
        var_dump($this->input->post('judul_presentasi'));
        die(); */
        $mitra = getMitraID();

        $data = array(
            'id_mitra' => $mitra,
            'judul_presentasi' => $this->input->post('judul_presentasi'),
            'deskripsi' => $this->input->post('deskripsi'),
            'waktu_awal_daftar' => $this->input->post('waktu_awal_daftar'),
            'waktu_akhir_daftar' => $this->input->post('waktu_akhir_daftar'),
            'tanggal_pelaksanaan' => $this->input->post('tanggal_pelaksanaan'),
            'di_ruang' => $this->input->post('di_ruang'),
            'id_perguruan_tinggi_lokasi' => $this->input->post('id_perguruan_tinggi_lokasi'),
            'kapasitas' => $this->input->post('kapasitas'),
            'fasilitas' => $this->input->post('fasilitas'),
            'fasilitas_tambahan' => $this->input->post('fasilitas_tambahan'),
            'created_by' => $this->session->userdata('t_userId'),
            'last_modified_by' => $this->session->userdata('t_userId')
        );

        $this->talk->simpan_talk($data);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan company talk',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan company talk',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function edit()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

       /*  $input = $this->input->post('judul_presentasi');
        var_dump($this->input->post('judul_presentasi'));
        die(); */
        $mitra = getMitraID();
        $id = $this->input->post('id');

        $data = array(
            'id_mitra' => $mitra,
            'judul_presentasi' => $this->input->post('judul_presentasi'),
            'deskripsi' => $this->input->post('deskripsi'),
            'waktu_awal_daftar' => $this->input->post('waktu_awal_daftar'),
            'waktu_akhir_daftar' => $this->input->post('waktu_akhir_daftar'),
            'tanggal_pelaksanaan' => $this->input->post('tanggal_pelaksanaan'),
            'di_ruang' => $this->input->post('di_ruang'),
            'id_perguruan_tinggi_lokasi' => $this->input->post('id_perguruan_tinggi_lokasi'),
            'kapasitas' => $this->input->post('kapasitas'),
            'fasilitas' => $this->input->post('fasilitas'),
            'fasilitas_tambahan' => $this->input->post('fasilitas_tambahan'),
            'created_by' => $this->session->userdata('t_userId'),
            'last_modified_by' => $this->session->userdata('t_userId')
        );

        $this->talk->edit($data, $id);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal mengedit company talk',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit company talk',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function delete()
    {
        $params = $this->input->post(null, true);
        $result = $this->talk->delete($params['id']);
        responseJson($result['status'], $result);
    }

    

    public function update()
    {
        $response['status'] = 0;
        $response['message'] = '';
        $params = $this->input->post(null, true);

        $current = strtotime(date('Y-m-d H:i:s'));
        $tgl_daftar = strtotime($params['waktu_awal_daftar']);
        $tgl_daftar_akhir = strtotime($params['waktu_akhir_daftar']);
        $tgl_pelaksanaan = strtotime($params['tanggal_pelaksanaan']);

        if ($tgl_daftar > $tgl_daftar_akhir) {
            $response['status'] = 500;
            $response['message'] = 'Tanggal akhir pendaftaran tidak boleh kurang dari tanggal awal pendaftaran';
            goto End;
        }
        if ($tgl_pelaksanaan < $tgl_daftar) {
            $response['status'] = 500;
            $response['message'] = 'Tanggal pelaksanaan tidak boleh kurang dari tanggal awal pendaftaran';
            goto End;
        }
        if ($tgl_pelaksanaan < $current) {
            $response['status'] = 500;
            $response['message'] = 'Tanggal pelaksanaan tidak boleh kurang dari tanggal aktif';
            goto End;
        }

        $data = filterFieldsOfTable('company_talks', $params);
        $data['id_mitra'] = getMitraID();
        $data['created_by'] = getSessionID();
        unset($data['id']);

        if (isset($params['fasilitas'])) {
            $fasilitas = [];
            for ($i = 0; $i < count($params['fasilitas']); $i++) {
                $fasilitas[$i]['fasilitas'] = $params['fasilitas'][$i];
            }
            $data['fasilitas'] = json_encode($fasilitas);
        }

        $result = $this->talk->update($data, $params['id']);

        if ($result['status'] == 201) {
            $response['status'] = 201;
            $response['message'] = $result['message'];
        } else {
            $response['status'] = 500;
            $response['message'] = $result['message'];
        }

        End:
        responseJson($response['status'], $response);
    }

    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('judul_presentasi', 'Judul Presentasi', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
        $this->form_validation->set_rules('waktu_awal_daftar', 'Waktu awal daftar', 'trim|required');
        $this->form_validation->set_rules('waktu_akhir_daftar', 'Waktu akhir daftar', 'trim|required');
        $this->form_validation->set_rules('tanggal_pelaksanaan', 'Tanggal pelaksanaan', 'trim|required');
        $this->form_validation->set_rules('id_perguruan_tinggi_lokasi', 'Perguruan Tinggi', 'trim|required');
        $this->form_validation->set_rules('di_ruang', 'Ruang', 'trim|required');
        $this->form_validation->set_rules('kapasitas', 'Kapasitas', 'trim|required');
        $this->form_validation->set_rules('fasilitas', 'Fasilitas', 'trim|min_length[50]');
        $this->form_validation->set_rules('fasilitas_tambahan', 'Fasilitas Tambahan', 'trim|min_length[100]');
        

        /* if ($this->input->post('jenis') == null) {
            $errors[] = [
                'field'   => 'jenis',
                'message' => 'Jenis belum dipilih ',
            ];
        }

        if ($this->input->post('id_pendidikan') == null) {
            $errors[] = [
                'field'   => 'id_pendidikan',
                'message' => 'Minimal Pendidikan belum dipilih ',
            ];
        } */

        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');
        $this->form_validation->set_message('validDate', '{field} format tanggal belum seusai. Format : yyyy-mm-dd');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function peserta()
    {
        $return = [];

        $field = [
            'sSearch',
            'iSortCol_0',
            'sSortDir_0',
            'iDisplayStart',
            'iDisplayLength',
            'idCT',
            'status',
        ];

        foreach ($field as $v) {
            $$v = $this->input->get_post($v);
        }

        $return = [
            "sEcho"                 => $this->input->post('sEcho'),
            "iTotalRecords"         => 0,
            "iTotalDisplayRecords"  => 0,
            "aaData"                => []
        ];

        $params = [
            'sSearch'   => $sSearch,
            'start'     => $iDisplayStart,
            'limit'     => $iDisplayLength,
            'idCT'      => $idCT,
            'status'    => $status,
        ];

        $data = $this->talk->peserta($params);
        if ($data['total'] > 0) {
            $return['iTotalRecords'] = $data['total'];
            $return['iTotalDisplayRecords'] = $return['iTotalRecords'];

            foreach ($data['rows'] as $k => $row) {

                $row['no']              = '<p class="text-center">' . ($iDisplayStart + ($k + 1)) . '</p>';
                $row['nama']            = $row['nama'];
                $row['email']           = $row['email'] . ' / ' . $row['no_hp'];
                $row['jenis_kelamin']   = ($row['jenis_kelamin'] == 'L' ? 'Laki-laki' : 'Perempuan');
                $row['asal_kampus']     = $row['nama_pendek'];
                $row['prodi']           = $row['nama_prodi'];
                $row['alamat']          = $row['alamat'];
                $row['status']          = ($row['is_valid'] == '1' ? 'Diterima' : ($row['is_valid'] == '2' ? 'Tidak Diterima' : 'Belum Diverifikasi'));
                $row['kelola']          = ($row['is_valid'] == '0' ? '<button class="btn btn-primary btn-sm" id="btn-validasi" data="' . $row['id'] . '"><i class="fa fa-check"></i></button>' : '');


                $return['aaData'][] = $row;
            }
        }
        $this->db->flush_cache();
        echo json_encode($return);
    }

    public function checkData()
    {
        $result = $this->talk->validasi($this->input->post('kode'));
        responseJson($result['status'], $result);
    }

    public function verifikasi()
    {
        $params = $this->input->post(null, true);

        $result = $this->talk->verifikasi(['is_valid' => $params['is_valid']], $params['id']);

        responseJson($result['status'], $result);
    }

    public function getCompanyTalkList()
    {
        $result = $this->talk->getCompanyTalkList();

        responseJson($result['status'], $result);
    }

    public function registered()
    {
        $params = $this->input->post(null, true);

        $data['id_company_talks'] = $params['id_company_talk'];
        $data['id_alumni']        = getAlumniID();
        $data['waktu_daftar']     = date('Y-m-d H:i:s');
        $data['is_valid']         = '0';

        $result = $this->talk->registered($data);

        responseJson($result['status'], $result);
    }
}
