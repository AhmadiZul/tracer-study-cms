<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use Dompdf\Dompdf;
class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "landing_app";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_lowongan_kerja', 'lowongan_kerja');
        $this->load->model('M_lowongan_magang', 'lowongan_magang');
        $this->load->model('M_mitra', 'mitra');
        $this->load->model('M_layanan_keuangan', 'layanan_keuangan');
        $this->load->model('M_tips', 'tips');
        $this->load->model('M_agenda', 'agenda');
        $this->load->model('M_laporan', 'laporan');
        $this->load->model('M_setting', 'setting');
        $this->load->model('M_faq', 'faq');
        $this->load->helper('download');
    }

    public function index()
    {
        $this->data['title'] = 'Beranda';
        $this->data['is_full'] = true;
        $this->data['mitra'] =  $mitra = $this->mitra->getMitra();
        $this->data['tips'] =  $tips = $this->tips->getTips(null, 5);
        $this->data['agenda'] =  $agenda = $this->agenda->getAgenda(array('waktu_mulai >= ' => date("U")), false);
        $this->data['layanan'] =  $layanan_keuangan = $this->layanan_keuangan->getLayanan();
        $this->data['layanan'] =  $layanan_keuangan = $this->layanan_keuangan->getLayanan();
        $this->data['upt'] = getDetailUPT(array('web_tracer !=' => null));
        $this->data['setting_video'] = $settig_video = $this->setting->setting_video();
        $this->data['copyright'] = $copyright = $this->setting->copyright();
        $this->data['about_us'] = $about_us = $this->setting->about_us();
        $this->data['logo_utama'] = $logo_utama = $this->setting->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->setting->logo_title();
        $this->data['header'] = $header = $this->setting->header();
        $this->data['footer'] = $footer = $this->setting->footer();
        $this->data['alumni_berprestasi'] = $alumni_berprestasi = $this->setting->alumni_berprestasi();
        $this->data['banner'] = $banner = $this->setting->banner();
        $this->data['getfaq'] = $getfaq = $this->faq->getFaq();
        $this->data['warna_tema'] = $warna_tema = $this->setting->warna_tema();
        $this->data['background'] = $background = $this->setting->background();
        $this->data['alumni_update'] = $alumni_update = $this->setting->alumni_update();
        $this->data['perguruan_tinggi'] = $perguruan_tinggi = $this->setting->perguruan_tinggi();
        $this->data['jenis_survey'] = $jenis_survey = $this->setting->jenis_survey();
        $this->data['tips_karir'] = $tips_karir = $this->setting->tips_karir();
        $this->data['greeting'] = $greeting = $this->setting->greeting();
        $this->data['laporan'] = $this->laporan->getTahunLaporan();
        $this->data['totalMitra'] = $total_mitra = $this->setting->totalMitra();
        $tahun = $this->setting->get_list_tahun();
        $count_rekap = [];
        foreach ($tahun as $key => $value) {
            $rekap = $this->setting->count_rekap_alumni($value->ref_tahun);
            // echo $this->db->last_query();
            array_push($count_rekap, $rekap);
        }
        $this->data['totalRekap'] = $count_rekap;

        $social_media = $this->setting->social_media();
        $sosial = json_decode($social_media->deskripsi);
        $this->data['social_media'] = $sosial;

        foreach ($agenda as $key => $value) {
            $value->waktu_mulai = $this->tanggalindo->konversi($value->waktu_mulai);
            $value->hari = $this->tanggalindo->get_hari_from_date($value->waktu_mulai);
        }
        foreach ($tips_karir as $key => $value) {
            $value->created_at = $this->tanggalindo->konversi($value->created_at);
        }
        $lowongan_kerja = $this->lowongan_kerja->getLowongan();
        foreach ($lowongan_kerja as $key => $value) {
            $value->nama = strtoupper($value->nama);
            $value->posisi = strtoupper($value->posisi);
            $value->tgl_selesai = $this->tanggalindo->konversi($value->end_publish);
        }
        $lowongan_magang = $this->lowongan_magang->getLowongan();
        foreach ($lowongan_magang as $key => $value) {
            $value->nama = strtoupper($value->nama);
            $value->posisi = strtoupper($value->posisi);
            $value->tgl_selesai = $this->tanggalindo->konversi($value->end_publish);
        }
        $this->data['lowongan_magang']  = $lowongan_magang;
        $this->data['lowongan_kerja']   = $lowongan_kerja;
        $this->data['scripts']          = ['index/js/index.js'];
        $this->renderTo('index/index');
    }

    public function cvMitra()
    {
        $data = $this->input->post();
        $visitor = $this->mitra->getMitra($data['id'])->visitor;
        $return = array();

        $this->db->trans_begin();

        $tambah = $this->mitra->update_mitra(array('visitor' => $visitor + 1), $data['id']);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $kondisi = true;
            $return = array('success' => $kondisi, "kode" => $data['id'], "data" => ['viewer' => $visitor + 1],);
        } else {
            $this->db->trans_rollback();
            $kondisi = false;
            $return = array('success' => $kondisi, "kode" => $data['id']);
        }
        echo json_encode($return);
    }

    public function cvKeuangan()
    {
        $data = $this->input->post();
        $visitor = $this->layanan_keuangan->getLayanan($data['id'])->visitor;
        $return = array();

        $this->db->trans_begin();

        $tambah = $this->layanan_keuangan->update_layanan(array('visitor' => $visitor + 1), $data['id']);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $kondisi = true;
            $return = array('success' => $kondisi, "kode" => $data['id'], "data" => ['viewer' => $visitor + 1]);
        } else {
            $this->db->trans_rollback();
            $kondisi = false;
            $return = array('success' => $kondisi, "kode" => $data['id']);
        }
        echo json_encode($return);
    }

    public function cvTips()
    {
        $data = $this->input->post();
        $viewer = $this->tips->getTips(['id_berita' => $data['id_berita']])->viewer;
        $return = array();

        $this->db->trans_begin();

        $tambah = $this->tips->update_tips(array('viewer' => $viewer + 1), $data['id']);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $kondisi = true;
            $return = array('success' => $kondisi, "kode" => $data['id'], "data" => ['viewer' => $viewer + 1]);
        } else {
            $this->db->trans_rollback();
            $kondisi = false;
            $return = array('success' => $kondisi, "kode" => $data['id']);
        }
        echo json_encode($return);
    }

    public function logout()
    {
        $id_user = $this->session->userdata('t_userId');
        $activity = "LOG OUT";
        $page_url = base_url("?logout=true");

        $this->m_activity_log->insert($id_user, $activity, $page_url);

        $this->unSetUserData();
        $this->load->database("default", FALSE, TRUE); //CHANGE DB TO DEFAULT sialogin
        if ($this->session->flashdata('changepassword')) {
            $this->session->set_flashdata('true', 'Ubah password berhasil, silahkan login kembali.');
            redirect("login?logout=true");
        }
        redirect("Login?logout=true");
    }

    public function getJumlahPeserta()
    {
        $result = $this->mitra->getJumlahPeserta();

        responseJson($result['status'], $result);
    }

    public function download()
	{
        $value = $this->input->get('file');
		force_download($value,NULL);
	}
}