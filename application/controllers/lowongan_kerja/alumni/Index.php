<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "app_back";
    protected $module = "lowongan_kerja";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_lowongan_kerja', 'lowongan');
        $this->load->model('m_profil');
    }

    public function index()
    {
        $this->data['title'] = 'Lowongan';
        $this->data['is_home'] = false;
        $this->data['select_mitra'] = setMitra();
        $this->data['select_lokasi'] = setKabupaten();
        $this->data['checkbox_pendidikan'] = setPendidikan();

        $lowongan = $this->lowongan->getLowonganNonDeskripsi();
        $id_alumni = $this->m_profil->get_alumni_by_id_user($this->session->userdata("t_userId"))->id_alumni;
        $lamaran = $this->lowongan->getLamaran($id_alumni);

        foreach ($lowongan as $key => $value) {
            $lamar = $this->lowongan->getApplyLowongan($id_alumni, $value->id_lowongan_pekerjaan);

            $value->nama = strtoupper($value->nama);
            $value->posisi = strtoupper($value->posisi);
            $value->tgl_selesai = $this->tanggalindo->konversi($value->end_publish);
            $value->diperbarui = $this->countDiperbarui($value->last_modified_time);
            $value->lamaran = ($lamar ? true : false);
        }

        foreach ($lamaran as $key => $value) {
            $value->applied_time = $this->tanggalindo->konversi_tgl_jam($value->applied_time);
        }

        $this->data['lowongan'] = $lowongan;
        $this->data['lamaran'] = $lamaran;
        $this->renderTo('lowongan_kerja/alumni/index');
    }

    public function countDiperbarui($date)
    {
        $selisih = floor((strtotime(date('Y-m-d H:i:s')) - strtotime($date)) / (60 * 60 * 24));

        if ($selisih == 0) {
            $selisih = floor((strtotime(date('Y-m-d H:i:s')) - strtotime($date)) / (60 * 60));
            return 'Diperbarui ' . $selisih . ' jam yang lalu';
        }

        return 'Diperbarui ' . $selisih . ' hari yang lalu';
    }

    public function detail()
    {
        if (!$this->input->get('id')) {
            redirect();
        }
        $id_lowongan = $this->input->get('id');
        $this->data['title'] = 'Lowongan';
        $this->data['is_home'] = false;
        $this->data['select_mitra'] = setMitra();
        $this->data['select_lokasi'] = setKabupaten();
        $this->data['checkbox_pendidikan'] = setPendidikan();
        $lowongan = $this->lowongan->getLowongan($id_lowongan);
        $lowongan->nama = strtoupper($lowongan->nama);
        $lowongan->posisi = strtoupper($lowongan->posisi);
        $lowongan->tgl_selesai = $this->tanggalindo->konversi($lowongan->end_publish);
        $lowongan->jenis = ($lowongan->jenis == 'PART' ? 'Part Time' : 'Full Time');
        $lowongan->url_logo = ($lowongan->url_logo ? $lowongan->url_logo : base_url() . 'public/assets/img/logo-kosong.webp');
        $this->data['lowongan'] = $lowongan;
        $this->renderTo('lowongan_kerja/alumni/detail');
    }

    public function lamar($id_lowongan)
    {
        $htmlCodeNumber = 201;
        $response = '';

        $this->db->trans_begin();
        $dbError = [];

        $id_alumni = $this->m_profil->get_alumni_by_id_user($this->session->userdata("t_userId"))->id_alumni;
        $id_apply = getUUID();

        $lamaran = array(
            'id_apply' => $id_apply,
            'id_alumni' => $id_alumni,
            'id_lowongan_pekerjaan' => $id_lowongan,
            'id_user_pelamar' => $this->session->userdata("t_userId"),
        );

        $simpan_lamaran = $this->lowongan->simpan_lamaran($lamaran);
        $dbError[] = $this->db->error();

        $datalog = array(
            'id_apply' => $id_apply,
            'activity' => 'Melamar Pekerjaan',
            'id_user' => $this->session->userdata("t_userId"),
            'ip_address' => $_SERVER['REMOTE_ADDR'],
        );
        $this->lowongan->simpan_log($datalog);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal melamar pekerjaan',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil melamar pekerjaan',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
}
