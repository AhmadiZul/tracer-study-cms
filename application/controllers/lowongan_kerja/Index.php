<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "landing_app";
    protected $module = "lowongan_kerja";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_lowongan_kerja','lowongan');
    }

    public function index()
    {
        $this->data['title'] = 'Beranda';
        $this->data['is_full'] = true;
        $this->data['select_mitra'] = setMitra();
        $this->data['select_lokasi'] = setKabupaten();
        $this->data['checkbox_pendidikan'] = setPendidikan();
        $lowongan = $this->lowongan->getLowonganNonDeskripsi();
        foreach ($lowongan as $key => $value) {
            $value->nama = strtoupper($value->nama);
            $value->posisi = strtoupper($value->posisi);
            $value->tgl_selesai = $this->tanggalindo->konversi($value->end_publish);
            $value->diperbarui = $this->countDiperbarui($value->last_modified_time);
        }
        $this->data['lowongan'] = $lowongan;
        $this->renderTo('lowongan_kerja/index');
    }

    public function countDiperbarui($date)
    {
        $selisih = floor((strtotime(date('Y-m-d H:i:s')) - strtotime($date)) / (60 * 60 * 24));

        if ($selisih == 0) {
            $selisih = floor((strtotime(date('Y-m-d H:i:s')) - strtotime($date)) / (60 * 60));
            return 'Diperbarui '.$selisih.' jam yang lalu';
        }

        return 'Diperbarui '.$selisih.' hari yang lalu';
    }

    public function detail()
    {
        if (!$this->input->get('id')) {
            redirect();
        }
        $id_lowongan = $this->input->get('id');
        $this->data['title'] = 'Beranda';
        $this->data['is_full'] = true;
        $this->data['select_mitra'] = setMitra();
        $this->data['select_lokasi'] = setKabupaten();
        $this->data['checkbox_pendidikan'] = setPendidikan();
        $lowongan = $this->lowongan->getLowongan($id_lowongan);
        $lowongan->nama = strtoupper($lowongan->nama);
        $lowongan->posisi = strtoupper($lowongan->posisi);
        $lowongan->tgl_selesai = $this->tanggalindo->konversi($lowongan->end_publish);
        $lowongan->jenis = ($lowongan->jenis == 'PART' ? 'Part Time' : 'Full Time');
        $lowongan->url_logo = ($lowongan->url_logo ? $lowongan->url_logo : base_url().'public/assets/img/logo-kosong.webp');
        $this->data['lowongan'] = $lowongan;
        $this->renderTo('lowongan_kerja/mitra/detail');
    }

}
