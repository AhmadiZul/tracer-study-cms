<?php

class M_kuesioner_mitra extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    public function getMitra()
    {
        $this->db->start_cache();
        $this->db->select('m.*');
        $this->db->from('mitra m');
        $this->db->where('m.is_active',"1");
        $data = $this->db->get()->result_array();
        $this->db->stop_cache();
        $return['isi']    = $data;
        $return['jumlah'] = count($data);
        $return['status'] = true;
        $this->db->flush_cache();
        return $return;
    }
    function cekSurveiTersedia($id_survey)
    {
        $return = null;
        $this->db->select('rjsm.*');
        $this->db->where(['rjsm.is_active'=> '1', 'rjsm.id_survey'=> $id_survey]);
        $survey = $this->db->get('ref_jadwal_survey_mitra rjsm');
        if($survey->num_rows() > 0){
            $return = $survey->row_array();
        }
        
        return $return;
    }
    public function checkSudahIsi($id_asnwer)
    {
        $this->db->from('kuesioner_mitra_offered_answer kmoa');
        $this->db->where('kmoa.id_answer',$id_asnwer);
        return $this->db->count_all_results();
    }

    public function checkTracerMitra()
    {
        $this->db->select('rjsm.*');
        $this->db->where(['rjsm.is_active'=> '1']);
        $survey = $this->db->get('ref_jadwal_survey_mitra rjsm');
        if($survey->num_rows() > 0){
            return true;
        }
        
        return false;
    }
    public function simpan_stakeholder($data){
        $this->db->insert('ts_stakeholder',$data);
        return $this->db->affected_rows();
    }
    public function load_question_stakeholder(){
        $this->db->select('kmq.*');
        $this->db->from('kuesioner_mitra_question kmq');
        $this->db->where('kmq.is_active',"1");
        $this->db->where('kmq.status',"1");
        $this->db->order_by('kmq.urutan','ASC');
        return $this->db->get()->result_array();
    }
    public function simpan_form_mitra($data){
        $this->db->insert('kuesioner_mitra_answer',$data);
    }
    public function simpan_answer_mitra($data){
        $this->db->insert('kuesioner_mitra_offered_answer',$data);
    }

}
