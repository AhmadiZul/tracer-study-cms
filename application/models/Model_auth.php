<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_auth extends CI_Model
{
    function reset($email)
    {

        $fungsi = $this->db->get_where('users', ["email" => $email])->row();
        return $fungsi;
    }

    function update($email, $data)
    {

        $this->db->where('email', $email);
        return $this->db->update('users', $data);
    }


}