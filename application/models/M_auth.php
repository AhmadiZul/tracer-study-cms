<?php

class M_auth extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('function_helper');
        $this->load->helper('date');
    }

    public function getUsersBy($by, $fields = '*', $limit = null)
    {
        $this->db->select($fields);
        $this->db->where($by);

        if ($limit != null || $limit > 0) {
            $this->db->limit($limit);
        }

        $query = $this->db->get('user');


        if ($limit == 1) {
            return $query->row();
        }

        return $query->result();
    }

    public function requestResetToken($user_id, $expired_in)
    {
        $token = substr(sha1(rand()), 0, 30);
        $hours = $expired_in * 60 * 60;
        $expired_at = date("Y-m-d H:i:s", (time() + $hours));

        $data = array(
            'token' => $token,
            'user_id' => $user_id,
            'expired_at' => $expired_at,
        );
        $this->db->insert('reset_tokens', $data);

        return $data;
    }

    public function statusResetToken($token)
    {
        $this->db->where('token', $token);
        $query = $this->db->get('reset_tokens');
        $row = $query->row();

        if ($row == null) {
            return null;
        }

        $isValid = ($row->is_valid == 1) ? true : false;
        $isExpired = ($row->expired_at < date("Y-m-d H:i:s")) ? true : false;
        $isUsed =  !empty($row->is_used) ? true : false;

        return [
            "is_valid"      => $isValid,
            "is_expired"    => $isExpired,
            "is_used"       => $isUsed,
            "user_id"       => $row->user_id
        ];
    }

    public function invalidateResetToken($token)
    {
        $this->db->set('is_valid', 0);
        $this->db->where('token', $token);
        $this->db->update('reset_tokens');
    }

    public function expireResetToken($token)
    {
        $this->db->set('used_at', date("Y-m-d H:i:s"));
        $this->db->where('token', $token);
        $this->db->update('reset_tokens');
    }

    public function changePassword($user_id, $password)
    {
        $this->db->set('password', $password);
        $this->db->where('id_user', $user_id);
        $this->db->update('user');
    }

    public function updateCookie($data, $id_user)
    {
        $this->db->where('id_user', $id_user);
        $this->db->update('user', $data);
    }

    public function get_by_cookie($cookie)
    {
        $this->db->where('cookie', $cookie);
        return $this->db->get("user");
    }
}
