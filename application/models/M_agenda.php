<?php

class M_agenda extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getAgenda($where = null, $row = false)
    {
        $this->db->limit(4);
        $this->db->order_by('waktu_mulai','ASC');
        if ($where) {
            $this->db->where($where);

            if ($row == false) {
                return $this->db->get('agenda')->result();
            }
            return $this->db->get('agenda')->row();
        }
        return $this->db->get('agenda')->result();
    }

    public function getDetailagenda($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('agenda')->row();
    }

    public function getRekomendation($where = null)
    {
        $this->db->join('user','tips_karir.id_user=user.id_user','LEFT');
        $this->db->limit(5);
        $this->db->order_by('viewer','DESC');
        if ($where) {
            $this->db->where($where);
            return $this->db->get('tips_karir')->row();
        }
        return $this->db->get('tips_karir')->result();
    }

    public function update_tips($data, $id)
    {
        $this->db->update('tips_karir', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
}
