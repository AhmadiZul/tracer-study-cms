<?php

class M_tips extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getTips($where = null, $limit = null)
    {
        $this->db->join('user','tips_karir.id_user=user.id_user','LEFT');
        if ($limit) {
            $this->db->limit($limit);
        }
        
        $this->db->order_by('publish_time','ASC');
        if ($where) {
            $this->db->where($where);
            return $this->db->get('tips_karir')->row();
        }
        return $this->db->get('tips_karir')->result();
    }

    public function getTipsLainnya($slug)
    {
        $this->db->where('slug !=',$slug);
        return $this->db->get('tips_karir')->result();
    }

    // public function getTipsLainnya($id)
    // {
    //     $this->db->where('id_berita !=',$id);
    //     return $this->db->get('berita_crud')->result();
    // }

    public function getDetailtips($id)
    {
        $this->db->where('id_berita',$id);
        return $this->db->get('berita_crud')->row();
    }

    public function getRekomendation($where = null)
    {
        $this->db->join('user','tips_karir.id_user=user.id_user','LEFT');
        $this->db->limit(5);
        $this->db->order_by('viewer','DESC');
        if ($where) {
            $this->db->where($where);
            return $this->db->get('tips_karir')->row();
        }
        return $this->db->get('tips_karir')->result();
    }

    public function update_tips($data, $id)
    {
        $this->db->update('berita_crud', $data, ['id_berita' => $id]);
        return $this->db->affected_rows();
    }
}
