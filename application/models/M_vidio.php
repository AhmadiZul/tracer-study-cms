<?php

class M_vidio extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function vidio_landing(){
        /*  $id = $this->session->userdata('id'); */
         $this->db->select('d.judul,d.paragraf, d.url_vidio, d.is_publish');
         $this->db->from('vidio_landing as d');
         $this->db->where('is_publish','1');
         $query= $this->db->order_by("d.id_vidio desc");
         return $query->get()->row();
    }
}