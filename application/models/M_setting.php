<?php

class M_setting extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function setting_video()
    {
        $this->db->select('key, deskripsi, url_link, title, url_file');
        $this->db->where('key', 'video_perkenalan');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }
    public function perguruan_tinggi()
    {
        $this->db->select('id_perguruan_tinggi, website');
        $this->db->where('id_perguruan_tinggi', '01c1ecc8-5f1e-11eb-8f9b-ec0ec4f44130');
        $query=$this->db->get('ref_perguruan_tinggi');

        return $query->row();
    }
    public function copyright()
    {
        $this->db->select('key, sub_title');
        $this->db->where('key', 'copyright');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }
    public function about_us()
    {
        $this->db->select('key, deskripsi, title');
        $this->db->where('key', 'about_us');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }
    public function social_media()
    {
        $this->db->select('deskripsi');
        $this->db->where('key', 'official_account');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }
    public function logo_utama()
    {
        $this->db->select('key, url_file');
        $this->db->where('key', 'logo_utama');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }
    public function background()
    {
        $this->db->select('key, url_file');
        $this->db->where('key', 'background_1');
        $this->db->or_where('key', 'background_2');
        $this->db->or_where('key', 'background_3');
        $query=$this->db->get('sistem_setting');

        return $query->result();
    }
    public function logo_title()
    {
        $this->db->select('key, url_file');
        $this->db->where('key', 'logo_title');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }
    public function header()
    {
        $this->db->select('key, warna_1, warna_2');
        $this->db->where('key', 'header');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }
    public function footer()
    {
        $this->db->select('key, sub_title, deskripsi, warna_1, warna_2');
        $this->db->where('key', 'footer');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }
    public function banner()
    {
        $this->db->select('key, title, sub_title, warna_1, warna_2, deskripsi');
        $this->db->where('key', 'banner');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }

    public function greeting()
    {
        $this->db->select('key, deskripsi, url_file, title');
        $this->db->where('key', 'rektor');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }

    public function warna_tema()
    {
        $this->db->select('key, deskripsi');
        $this->db->where('key', 'warna_tema');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }

    public function alumni_berprestasi()
    {

        $this->db->where('is_publish', '1');
        $query=$this->db->get('alumni_berprestasi');

        return $query->result();
    }

    public function totalMitra(){
        $this->db->where('is_active', '1');
        $get = $this->db->get('mitra');
             
        return $get->num_rows();
    }

    public function alumni_update()
    {
        $this->db->where('last_modified_user = id_user');
        $this->db->limit(5);
        $this->db->order_by('last_modified_time','DESC');
        $query=$this->db->get('alumni');
        return $query->result();
    }
    public function jenis_survey()
    {
        $this->db->from('ref_jenis_survey');
        $this->db->limit(3);
        $this->db->where('is_active','1');
        return $this->db->get()->result();
       
    }
    public function tips_karir()
    {
        return $this->db->get('berita_crud')->result();
    }

    public function get_list_tahun()
    {
        $result = $this->db->query("SELECT DISTINCT ref_tahun FROM alumni_total_per_prodi ORDER BY ref_tahun DESC LIMIT 3");
        return $result->result();
    }

    public function count_rekap_alumni($tahun)
    {
        $this->db->where('ref_tahun', $tahun);
        $this->db->select_sum('total_alumni');
        $this->db->select('ref_tahun');
        $this->db->from('alumni_total_per_prodi');
        $get = $this->db->get();
        return $get->result();
    }


}