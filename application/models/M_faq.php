<?php 

class M_faq extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function getFaq()
    {
        $this->db->from('faq');

        return $this->db->get()->result();
    }
}
?>