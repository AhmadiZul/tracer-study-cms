<?php

class M_lowongan_magang extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getLowongan($id_lowongan = null)
    {
        $this->db->select('lp.id_lowongan_magang, m.id_instansi, m.url_logo, m.nama, lp.posisi, lp.lokasi_kerja, lp.deskripsi, lp.start_publish, lp.end_publish, lp.last_modified_time, lp.status, lp.url_poster, rkk.nama_kab_kota, rp.nama_provinsi, lp.id_pendidikan, rpk.nama_pendidikan, lp.persyaratan');
        $this->db->join('mitra m','lp.id_instansi=m.id_instansi','LEFT');
        $this->db->join('ref_kab_kota rkk','lp.lokasi_kerja=rkk.kode_kab_kota','LEFT');
        $this->db->join('ref_provinsi rp','rkk.kode_provinsi=rp.kode_provinsi','LEFT');
        $this->db->join('ref_pendidikan rpk','lp.id_pendidikan=rpk.id_pendidikan');

        if ($id_lowongan) {
            $this->db->where('id_lowongan_magang',$id_lowongan);
            return $this->db->get('lowongan_magang lp')->row();
        }
        $this->db->where('lp.is_active','1');
        $this->db->where('lp.status','Publish');
        $this->db->where('lp.end_publish >= ',date("Y-m-d"));
        $this->db->where('lp.start_publish <= ',date("Y-m-d"));
        return $this->db->get('lowongan_magang lp')->result();
    }

    public function getLowonganNonDeskripsi($id_lowongan = null)
    {
        $this->db->select('lp.id_lowongan_magang, m.id_instansi, m.url_logo, m.nama, lp.posisi, lp.lokasi_kerja, lp.start_publish, lp.end_publish, lp.last_modified_time, lp.status, lp.url_poster, rkk.nama_kab_kota, rp.nama_provinsi, lp.id_pendidikan, rpk.nama_pendidikan');
        $this->db->join('mitra m','lp.id_instansi=m.id_instansi','LEFT');
        $this->db->join('ref_kab_kota rkk','lp.lokasi_kerja=rkk.kode_kab_kota','LEFT');
        $this->db->join('ref_provinsi rp','rkk.kode_provinsi=rp.kode_provinsi','LEFT');
        $this->db->join('ref_pendidikan rpk','lp.id_pendidikan=rpk.id_pendidikan');

        if ($id_lowongan) {
            $this->db->where('id_lowongan_magang',$id_lowongan);
            return $this->db->get('lowongan_magang lp')->row();
        }
        $this->db->where('lp.is_active','1');
        $this->db->where('lp.status','Publish');
        $this->db->where('lp.end_publish >= ',date("Y-m-d"));
        $this->db->where('lp.start_publish <= ',date("Y-m-d"));
        return $this->db->get('lowongan_magang lp')->result();
    }

    public function getMitra($id_instansi)
    {
        $this->db->where('id_instansi',$id_instansi);
        return $this->db->get('mitra')->row();
    }

    public function simpan_lowongan($data)
    {
        $this->db->insert('lowongan_magang', $data);
        return $this->db->affected_rows();
    }

    public function ubah_lowongan($data, $id)
    {
        $this->db->update('lowongan_magang', $data, ['id_lowongan_magang' => $id]);
        return $this->db->affected_rows();
    }

}
