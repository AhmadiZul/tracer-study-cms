<?php

class M_profil_mitra extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getMitraByUser()
    {
        $this->db->select('m.id_instansi, m.nama, m.jenis, m.sektor, m.alamat, m.telephone, m.email, m.url_logo, m.url_web, u.username');
        $this->db->from('mitra m');
        $this->db->join('user u','m.id_user=u.id_user','LEFT');
        $this->db->where('m.id_user',$this->session->userdata("t_userId"));
        return $this->db->get()->row();
    }
    
    public function checkMitra($where, $id = null)
    {
        $this->db->from('mitra');
        $this->db->where($where);

        if ($id) {
            $this->db->where('id_instansi !=', $id);
        }

        if ($this->db->get()->num_rows() > 0) {
            return false;
        }

        return true;
    }

    public function checkUser($where, $id = null)
    {
        $this->db->from('user');
        $this->db->where($where);

        if ($id) {
            $this->db->where('id_user !=', $id);
        }

        if ($this->db->get()->num_rows() > 0) {
            return false;
        }

        return true;
    }

    public function checkPasswordLama($where, $id)
    {
        $this->db->from('user');
        $this->db->where($where);
        $this->db->where('id_user', $id);

        if ($this->db->get()->num_rows() > 0) {
            return true;
        }

        return false;
    }

    public function getMitra($id_mitra)
    {
        $this->db->select('*');
        $this->db->from('mitra');
        $this->db->where('id_instansi', $id_mitra);
        $get = $this->db->get()->row_array();
        return $get;
    }

    public function edit_user($data, $where)
    {
        $this->db->update('user', $data, $where);
        return $this->db->affected_rows();
    }

    public function edit_mitra($data, $where)
    {
        $this->db->update('mitra', $data, $where);
        return $this->db->affected_rows();
    }
}
