<?php

class M_tracer extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getAlumniWhere($where)
    {
        $this->db->where($where);
        return $this->db->get('alumni')->row_array();
    }
    public function get_jenis_survey($where, $row)
    {
        $this->db->select('rjs.*');
        $this->db->from('ref_jenis_survey rjs');
        $this->db->join('ref_jadwal_survey', 'ref_jadwal_survey.id_ref_jenis_survey = rjs.id');
        
        $this->db->where($where);

        if ($row) {
            return $this->db->get()->row_array();
        }
        return $this->db->get()->result();
    }

    public function get_jadwal_survey($where, $row)
    {
        $this->db->select('rjs.id_survey, rjs.untuk_id_prodi, rp.nama_prodi, rjs.untuk_tahun_lulus, rjs.start_date, rjs.end_date, rjs.id_ref_jenis_survey, rjes.jenis_survey, rjs.untuk_id_fakultas, rjs.waktu_acara, rjs.waktu_berakhir, rf.nama_fakultas');
        $this->db->from('ref_jadwal_survey rjs');
        $this->db->join('ref_jenis_survey rjes','rjs.id_ref_jenis_survey=rjes.id','LEFT');
        $this->db->join('ref_fakultas rf','rjs.untuk_id_fakultas=rf.id_fakultas','LEFT');
        $this->db->join('ref_prodi rp','rjs.untuk_id_prodi=rp.id_prodi','LEFT');
        $this->db->where($where);
        $this->db->where('rjs.is_active',1);
        // $this->db->where('rjs.start_date <= NOW() AND rjs.end_date >= NOW()');

        if ($row) {
            return $this->db->get()->row_array();
        }
        return $this->db->get()->result();
    }

    public function getCek($id = null)
    {
        $this->db->from('alumni al');

        if ($id != null) {
            $this->db->where('id_alumni', $id);
        }

        $this->db->order_by('nim', 'ASC');
        $univ = $this->db->get();

        if ($univ->num_rows() != 0) {
            return $univ->result_array();
        } else {
            return null;
        }
    }

    public function getUserInfoByNim($nim, $nik)
    {
        $this->db->select('id_alumni');
        $this->db->from('alumni');
        $this->db->where('nim', $nim);
        $this->db->where('nik', $nik);
        $q = $this->db->get();
        return $q->row();
    }

    public function cekUserSurvey($nim, $nik, $tahun, $prodi, $fakultas)
    {
        $this->db->select('rjs.id_survey');
        $this->db->from('ref_jadwal_survey rjs');
        $this->db->join('alumni a', 'rjs.untuk_id_prodi=a.id_prodi');
        $this->db->where('a.nim', $nim);
        $this->db->where('a.nik', $nik);
        $this->db->where('rjs.untuk_tahun_lulus', $tahun);
        $this->db->where('rjs.untuk_id_prodi', $prodi);
        $this->db->where('rjs.untuk_id_fakultas', $fakultas);
        $q = $this->db->get();
        return $q->result();
    }

    public function getTahunProdi($idSurvey)
    {
        $this->db->select('untuk_tahun_lulus,untuk_id_prodi,untuk_id_fakultas');
        $this->db->from('ref_jadwal_survey');
        $this->db->where('id_survey', $idSurvey);
        $q = $this->db->get();
        return $q->row();
    }

    public function cek_nim($nim, $id_user)
    {
        $this->db->select('*');
        $this->db->from('alumni');
        $this->db->where('nim', $nim);
        $this->db->where('id_user', $id_user);
        return $this->db->count_all_results();
    }

    public function cek_nik($nik)
    {
        $this->db->select('*');
        $this->db->from('alumni');
        $this->db->where('nik', $nik);
        return $this->db->count_all_results();
    }

    public function get_cek_survey($where, $row)
    {
        $this->db->select('rjs.id_survey, rjs.untuk_id_prodi, rp.nama_prodi, rjs.untuk_tahun_lulus, rjs.start_date, rjs.end_date, rjs.id_ref_jenis_survey, rjes.jenis_survey,a.nim,a.nik');
        $this->db->from('ref_jadwal_survey rjs');
        $this->db->join('ref_jenis_survey rjes', 'rjs.id_ref_jenis_survey=rjes.id', 'LEFT');
        $this->db->join('ref_prodi rp', 'rjs.untuk_id_prodi=rp.id_prodi', 'LEFT');
        $this->db->join('alumni a', 'rjs.untuk_id_prodi=a.id_prodi', 'LEFT');
        $this->db->where($where);
        $this->db->where('rjs.is_active', 1);
        $this->db->where('rjs.start_date <= NOW() AND rjs.end_date >= NOW()');

        if ($row) {
            return $this->db->get()->row_array();
        }
        return $this->db->get()->result();
    }

    public function get_alumni_by_id_alumni($id)
    {
        $this->db->select('al.id_alumni, al.nim, al.nama, al.nik, al.nip, al.tempat_lahir, al.tgl_lahir, al.id_agama, al.jenis_kelamin, al.no_hp, al.email, al.url_photo, al.alamat, al.kode_prov, al.kode_kab_kota, al.kode_kecamatan, al.url_photo, al.tahun_lulus, al.ipk_terakhir,  al.id_prodi, al.judul_skripsi, al.id_ref_status_alumni, al.id_user, rp.nama_prodi, al.status_data, kaa.inserted_time');
        $this->db->from('alumni al');
        $this->db->join('kuesioner_alumni_answer kaa', 'al.id_alumni=kaa.id_alumni', 'LEFT');
        $this->db->join('ref_prodi rp', 'al.id_prodi=rp.id_prodi', 'LEFT');
        $this->db->where('al.id_alumni', $id);
        return $this->db->get()->row();
    }
    public function checkAlumniAnswer($where)
    {
        $this->db->where($where);
        return $this->db->count_all_results('kuesioner_alumni_answer');
    }

    public function get_jobs_by_id_alumni($id, $id_status)
    {
        $this->db->select('aj.id_job, aj.nama_instansi, aj.tgl_mulai, aj.jabatan, aj.alamat, aj.kode_provinsi, aj.kode_kab_kota, aj.kode_kecamatan');
        $this->db->from('alumni_jobs aj');
        $this->db->where('id_alumni', $id);
        $this->db->where('id_ref_status_alumni', $id_status);
        $this->db->order_by('tgl_mulai', 'DESC');
        return $this->db->get()->row();
    }

    public function get_study_by_id_alumni($id)
    {
        $this->db->select('ast.id_study, ast.nama_perguruan_tinggi, ast.tgl_mulai, ast.prodi, ast.kode_provinsi, ast.kode_kab_kota, ast.kode_kecamatan');
        $this->db->from('alumni_study ast');
        $this->db->where('id_alumni', $id);
        $this->db->order_by('tgl_mulai', 'DESC');
        return $this->db->get()->row();
    }

    public function get_daftar_alumni($where, $id_survey)
    {
        $this->db->start_cache();
        $this->db->select('a.id_alumni, a.nim, a.nama, rp.nama_prodi, a.status_data, (SELECT ksa.id_answer FROM kuesioner_alumni_answer ksa where ksa.id_survey="' . $id_survey . '" AND ksa.id_alumni=a.id_alumni) as id_answer, (SELECT ksa.inserted_time from kuesioner_alumni_answer ksa where ksa.id_survey="' . $id_survey . '" AND ksa.id_alumni=a.id_alumni) as inserted_time');
        $this->db->from('alumni a');
        $this->db->join('ref_prodi rp', 'a.id_prodi=rp.id_prodi', 'LEFT');
        $this->db->where($where);
        $this->db->where('a.is_active', 1);
        $result = $this->db->get()->result_array();
        $this->db->stop_cache();
        $return['isi']    = $result;
        $return['jumlah'] = count($result);
        $return['status'] = true;
        $return['last_query'] = $this->db->last_query();
        $this->db->flush_cache();
        return $return;
    }

    public function get_daftar_kuesioner($where)
    {

        $this->db->start_cache();
        $this->db->select('rjs.id_survey, rjs.id_ref_jenis_survey, rjes.jenis_survey, (SELECT ksa.id_answer FROM kuesioner_alumni_answer ksa where ksa.id_survey=rjs.id_survey AND ksa.id_alumni="' . $where['id_alumni'] . '") as id_answer, (SELECT ksa.inserted_time from kuesioner_alumni_answer ksa where ksa.id_survey=rjs.id_survey AND ksa.id_alumni="' . $where['id_alumni'] . '") as inserted_time');
        $this->db->from('ref_jadwal_survey rjs');
        $this->db->join('ref_jenis_survey rjes', 'rjs.id_ref_jenis_survey=rjes.id', 'LEFT');
        $this->db->where('rjs.untuk_id_prodi', $where['untuk_id_prodi']);
        $this->db->where('rjs.untuk_tahun_lulus', $where['untuk_tahun_lulus']);
        $this->db->where('rjs.is_active', 1);
        $this->db->order_by('rjes.id');
        $result = $this->db->get()->result_array();
        $this->db->stop_cache();
        $return['isi']    = $result;
        $return['status_data'] = $where['status_data'];
        $return['jumlah'] = count($result);
        $return['status'] = true;
        $this->db->flush_cache();
        return $return;
    }

    public function getKuesionerPivot()
    {
        $this->db->from('kuesioner_alumni_question kaq');
        $this->db->where('urutan','1');
        $this->db->where('is_active','1');
        return $this->db->get()->row_array();
    }

    public function get_kuesioner_by_status($id_status)
    {
        $data = array();

        $this->db->select('klq.id_kuesioner, klq.question_code, klq.question, klq.question_type, klq.answer_rules, klq.others, klq.urutan, klq.is_active, klqr.id_status_alumni');
        $this->db->from('kuesioner_alumni_question klq');
        $this->db->join('kuesioner_alumni_question_relation klqr', 'klq.id_kuesioner=klqr.id_kuesioner', 'LEFT');
        $this->db->where('klqr.id_status_alumni', $id_status);
        $this->db->where('klq.is_active', '1');
        $this->db->order_by('klq.urutan');
        $kuesioner = $this->db->get()->result_array();

        
        foreach ($kuesioner as $key => $value) {
            $answer = array();
            if ($value['question_type'] == 3 || $value['question_type'] == 4 || $value['question_type'] == 9) {
                $answer = $this->get_answer_option_by_kuesioner($value['id_kuesioner']);
            }

            if ($value['question_type'] == 5) {
                $answer = $this->get_answer_likert_by_kuesioner($value['id_kuesioner']);
            }

            $data[$key] = array(
                'id_kuesioner' => $value['id_kuesioner'],
                'question_code' => $value['question_code'],
                'question' => $value['question'],
                'question_type' => $value['question_type'],
                'answer_rules' => $value['answer_rules'],
                'others' => $value['others'],
                'urutan' => $value['urutan'],
                'id_status_alumni' => $value['id_status_alumni'],
                'is_active' => $value['is_active'],
                'answer' => $answer

            );
            $subquestion = $this->get_subquestion($value['question_code']);

            if ($subquestion) {
                $data[$key]['subquestion'] = $subquestion;
            }
            
            
        }
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die;
    
        return $data;
    }

    public function get_subquestion($question_code)
    {
        $data = array();

        $this->db->select('klq.id_kuesioner, klq.question_code, klq.question, klq.question_type, klq.answer_rules, klq.others, klq.urutan, klq.is_active, klq.id_kuesioner, klqr.id_status_alumni, klq.parent_question_code');
        $this->db->from('kuesioner_alumni_question klq');
        $this->db->join('kuesioner_alumni_question_relation klqr', 'klq.id_kuesioner=klqr.id_kuesioner', 'LEFT');
        $this->db->where('klq.parent_question_code', $question_code);
        $this->db->where('klq.is_active', '1');
        $this->db->order_by('klq.urutan');
        $kuesioner = $this->db->get()->result_array();
        foreach ($kuesioner as $key => $value) {
            $answer = array();


            if ($value['question_type'] == 3 || $value['question_type'] == 4 || $value['question_type'] == 9) {
                $answer = $this->get_answer_option_by_kuesioner($value['id_kuesioner']);
            }

            if ($value['question_type'] == 5) {
                $answer = $this->get_answer_likert_by_kuesioner($value['id_kuesioner']);
            }

            $data[] = array(
                'id_kuesioner' => $value['id_kuesioner'],
                'question_code' => $value['question_code'],
                'question' => $value['question'],
                'question_type' => $value['question_type'],
                'answer_rules' => $value['answer_rules'],
                'others' => $value['others'],
                'urutan' => $value['urutan'],
                'id_status_alumni' => $value['id_status_alumni'],
                'is_active' => $value['is_active'],
                'answer' => $answer
            );
        }
        return $data;
    }


    public function get_answer_option_by_kuesioner($id_kuesioner)
    {
        $this->db->select('klqo.id_opsi, klqo.id_kuesioner, klqo.answer_code, klqo.opsi, klqo.isian, klqo.answer_rules');
        $this->db->from('kuesioner_alumni_question_option klqo');
        $this->db->where('klqo.is_active', '1');
        $this->db->where('id_kuesioner', $id_kuesioner);
        $this->db->order_by('id_opsi');
        return $this->db->get()->result_array();
    }

    public function get_answer_likert_by_kuesioner($id_kuesioner)
    {
        $this->db->select('klql.id_likert, klql.id_kuesioner, klql.likert_code, klql.pertanyaan');
        $this->db->from('kuesioner_alumni_question_likert klql');
        $this->db->where('klql.is_active', '1');
        $this->db->where('id_kuesioner', $id_kuesioner);
        $this->db->order_by('id_likert');
        return $this->db->get()->result_array();
    }

    public function get_question()
    {
        $this->db->select('klql.id_kuesioner, klql.question_code, klql.question , klql.urutan');
        $this->db->from('kuesioner_alumni_question klql');
        $this->db->where('klql.is_active', '1');
        $this->db->order_by('urutan');
        return $this->db->get()->result_array();
    }

    public function cetak_bukti()
    {
        $this->db->select('rf.id_fakultas, rf.nama_fakultas, rp.id_prodi, rp.nama_prodi');
        $this->db->from('ref_prodi rp');
        $this->db->join('ref_fakultas rf', 'rf.id_fakultas = rp.id_fakultas', 'LEFT');
        $this->db->where('rf.is_active', '1');
        $this->db->where('rp.is_active', '1');
        return $this->db->get()->row();
    }

    public function get_alumni_perguruan()
    {
        $this->db->select('a.is_active,a.id_alumni, a.nim,a.nama,a.url_photo');
        $this->db->from('alumni a');
        $this->db->where('a.is_active', '1');
        return $this->db->get()->row();
    }

    public function get_perguruan_tinggi()
    {
        $this->db->from('ref_perguruan_tinggi pt');
        return $this->db->get()->row();
    }

    public function get_sub_by_kuesioner($id_kuesioner)
    {
        $this->db->select('sub.id_sub, sub.id_kuesioner, sub.sub_code, sub.pertanyaan, sub.answer_rules');
        $this->db->from('kuesioner_alumni_question_sub sub');
        $this->db->where('sub.is_active', '1');
        $this->db->where('id_kuesioner', $id_kuesioner);
        $this->db->order_by('id_sub');
        return $this->db->get()->result_array();
    }

    public function get_all_kuesioner_category()
    {
        $this->db->select('klqc.id_opsi, klqc.id_kuesioner, klqc.answer_code, klqc.is_active , klqc.opsi , klqc.urutan');
        $this->db->from('kuesioner_alumni_question_option klqc');
        $this->db->where('is_active', '1');
        return $this->db->get()->result_array();
    }

    public function simpan_answer($data)
    {
        $this->db->insert('kuesioner_alumni_answer', $data);
        return $this->db->insert_id();
    }

    public function simpan_offered_answer($data)
    {
        $this->db->insert_batch('kuesioner_alumni_offered_answer', $data);
        return $this->db->affected_rows();
    }

    public function getUserInfo($nim, $nik)
    {
        $this->db->where('nim', $nim);
        $this->db->where('nik', $nik);
        $q = $this->db->get('alumni');
        return $q->row();
    }
}
