<?php

class M_layanan_keuangan extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getLayanan($id_layanan = null){

        if ($id_layanan) {
            $this->db->where('id_layanan',$id_layanan);
            return $this->db->get('layanan_keuangan')->row();
        }
        return $this->db->get('layanan_keuangan')->result();
    }

    public function update_layanan($data, $id_layanan)
    {
        $this->db->update('layanan_keuangan', $data, ['id_layanan' => $id_layanan]);
        return $this->db->affected_rows();
    }
}
