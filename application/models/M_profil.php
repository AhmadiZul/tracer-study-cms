<?php

class M_profil extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getCountAlumniBy($where = null, $count = true)
    {
        if ($where) {
            $this->db->where($where);
        }

        if ($count) {
            return $this->db->count_all_results('alumni a');
        }

        $this->db->select('a.id_alumni, a.id_ref_status_alumni');
        return $this->db->get('alumni a')->result();
    }

    public function getAlumniById($id_alumni)
    {
        $this->db->select('al.id_alumni, al.nim, al.nama, al.nik, al.nip, al.tempat_lahir, al.tgl_lahir, al.id_agama, al.jenis_kelamin, al.no_hp, al.email, al.sosmed, al.alamat, al.kode_prov, rp.nama_provinsi, al.kode_kab_kota, rkk.nama_kab_kota, al.kode_kecamatan, rk.nama_kecamatan, al.url_photo, al.tahun_lulus, al.ipk_terakhir, al.id_prodi, rpd.nama_prodi, al.judul_skripsi, al.id_ref_status_alumni, rsa.status_alumni, ra.nama_agama');
        $this->db->from('alumni al');
        $this->db->join('ref_provinsi rp', 'al.kode_prov=rp.kode_provinsi', 'LEFT');
        $this->db->join('ref_kab_kota rkk', 'al.kode_kab_kota=rkk.kode_kab_kota', 'LEFT');
        $this->db->join('ref_kecamatan rk', 'al.kode_kecamatan=rk.kode_kecamatan', 'LEFT');
        // $this->db->join('ref_perguruan_tinggi rpt', 'al.id_perguruan_tinggi=rpt.id_perguruan_tinggi', 'LEFT');
        $this->db->join('ref_prodi rpd', 'al.id_prodi=rpd.id_prodi', 'LEFT');
        $this->db->join('ref_agama ra', 'al.id_agama=ra.id_agama', 'LEFT');
        $this->db->join('ref_status_alumni rsa', 'al.id_ref_status_alumni=rsa.id', 'LEFT');
        $this->db->where('al.id_alumni', $id_alumni);
        return $this->db->get()->row();
    }

    public function getProdi($params = null)
  {
    $prodi = $this->db->query("SELECT * FROM ref_prodi order by nama_prodi asc");

    if ($prodi->num_rows() != 0) {
      return $prodi->result_array();
    } else {
      return null;
    }
  }

  function getSelectProdi($id_fakultas = null)
  {
    $data = $this->getProdiByFakultas($id_fakultas);
    $html = '<option value="">Pilih Prodi</option>';
    if ($data != null) {
      foreach ($data as $rows) {
        $html .= '<option value="' . $rows['id_prodi'] . '">' . ucwords(strtolower($rows['nama_prodi'])) . '</option>';
      }
    }
    $return = $html;
    return $return;
  }


  public function getAlumni($where)
    {
        $this->db->from('alumni');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row_array();
        }

        return $this->db->get()->result();
    }
  function getProdiByFakultas($id)
  {
    $get = $this->db->query("SELECT * FROM ref_prodi where id_fakultas=? order by nama_prodi ", array($id));

    if ($get->num_rows() != 0) {
      return $get->result_array();
    } else {
      return null;
    }
  }

  public function getFakultas()
  {
    $this->db->from('ref_fakultas rf');
    $this->db->order_by('nama_fakultas', 'ASC');
    $fakultas = $this->db->get();

    if ($fakultas->num_rows() != 0) {
      return $fakultas->result_array();
    } else {
      return null;
    }
  }

    public function getStatusALumniAll()
    {
        return $this->db->get('ref_status_alumni')->result();
    }

    public function getAlumniJobs($id_alumni)
    {
        $this->db->select('nama_instansi, jabatan, alamat, tipe_usaha, lainnya, tgl_mulai, sektor');
        $this->db->where('id_alumni', $id_alumni);
        $this->db->order_by('tgl_mulai', 'DESC');
        return $this->db->get('alumni_jobs')->row();
    }

    public function getAlumniStudy($id_alumni)
    {
        $this->db->select('nama_perguruan_tinggi, prodi, tgl_mulai, sektor_prodi as sektor');
        $this->db->where('id_alumni', $id_alumni);
        $this->db->order_by('tgl_mulai', 'DESC');
        return $this->db->get('alumni_study')->row();
    }

    public function getPolbangtanAll()
    {
        return $this->db->get('ref_perguruan_tinggi')->result();
    }

    public function getPrestasi($where)
    {

        $this->db->start_cache();
        $this->db->select('p.id, p.id_alumni, p.nama_prestasi, p.penyelenggara, p.tahun, p.tingkat, p.gelar, p.url_sertifikat');
        $this->db->from('alumni_prestasi p');
        $this->db->where($where);
        $result = $this->db->get()->result_array();
        $this->db->stop_cache();
        $return['isi']    = $result;
        $return['jumlah'] = count($result);
        $return['status'] = true;
        $this->db->flush_cache();
        return $return;
    }

    public function getOrganisasi($where)
    {

        $this->db->start_cache();
        $this->db->select('p.id, p.id_alumni, p.nama_organisasi, p.posisi, p.job_desc, p.url_bukti');
        $this->db->from('alumni_organisasi p');
        $this->db->where($where);
        $result = $this->db->get()->result_array();
        $this->db->stop_cache();
        $return['isi']    = $result;
        $return['jumlah'] = count($result);
        $return['status'] = true;
        $this->db->flush_cache();
        return $return;
    }

    public function getSkill($where)
    {

        $this->db->start_cache();
        $this->db->select('p.id, p.id_alumni, p.nama_skill, p.bidang, p.level, p.url_sertifikat');
        $this->db->from('alumni_skill p');
        $this->db->where($where);
        $result = $this->db->get()->result_array();
        $this->db->stop_cache();
        $return['isi']    = $result;
        $return['jumlah'] = count($result);
        $return['status'] = true;
        $this->db->flush_cache();
        return $return;
    }

    public function getPengalaman($where)
    {

        $this->db->start_cache();
        $this->db->select('p.id, p.id_alumni, p.jenis_pengalaman, p.nama_instansi, p.posisi, p.alamat, p.start_date, p.end_date, p.is_current_position, p.url_bukti');
        $this->db->from('alumni_pengalaman p');
        $this->db->where($where);
        $result = $this->db->get()->result_array();
        $this->db->stop_cache();
        $return['isi']    = $result;
        $return['jumlah'] = count($result);
        $return['status'] = true;
        $this->db->flush_cache();
        return $return;
    }

    public function get_alumni_by_id_user($id)
    {
        $this->db->select('al.id_alumni, al.nim, al.nama, al.nik, al.nip, al.tempat_lahir, al.tgl_lahir, al.id_agama, al.jenis_kelamin, al.no_hp, al.email, al.sosmed, al.alamat, al.kode_prov, rp.nama_provinsi, al.kode_kab_kota, rkk.nama_kab_kota, al.kode_kecamatan, al.url_photo, al.tahun_lulus, al.ipk_terakhir, al.id_prodi, al.judul_skripsi, al.id_ref_status_alumni, al.id_user, u.username');
        $this->db->from('alumni al');
        $this->db->join('user u', 'al.id_user=u.id_user', 'LEFT');
        $this->db->join('ref_provinsi rp', 'al.kode_prov=rp.kode_provinsi', 'LEFT');
        $this->db->join('ref_kab_kota rkk', 'al.kode_kab_kota=rkk.kode_kab_kota', 'LEFT');
        $this->db->where('al.id_user', $id);
        return $this->db->get()->row();
    }

    public function get_status_alumni($where)
    {
        $this->db->where($where);
        return $this->db->get('ref_status_alumni')->row();
    }

    public function get_jobs_by_id_alumni($id, $id_status)
    {
        $this->db->select('aj.id_job, aj.nama_instansi, aj.tgl_mulai, aj.jabatan, aj.alamat, aj.kode_provinsi, aj.kode_kab_kota, aj.kode_kecamatan');
        $this->db->from('alumni_jobs aj');
        $this->db->where('id_alumni', $id);
        $this->db->where('id_ref_status_alumni', $id_status);
        $this->db->order_by('tgl_mulai', 'DESC');
        return $this->db->get()->row();
    }

    public function get_study_by_id_alumni($id)
    {
        $this->db->select('ast.id_study, ast.nama_perguruan_tinggi, ast.tgl_mulai, ast.prodi, ast.kode_provinsi, ast.kode_kab_kota, ast.kode_kecamatan');
        $this->db->from('alumni_study ast');
        $this->db->where('id_alumni', $id);
        $this->db->order_by('tgl_mulai', 'DESC');
        return $this->db->get()->row();
    }

    public function get_pengalaman($id_alumni, $id = null)
    {
        $this->db->from('alumni_pengalaman');
        $this->db->where('id_alumni', $id_alumni);
        if ($id != null) {
            $this->db->where('id', $id);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function get_organisasi($id_alumni, $id = null)
    {
        $this->db->from('alumni_organisasi');
        $this->db->where('id_alumni', $id_alumni);
        if ($id != null) {
            $this->db->where('id', $id);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function get_prestasi($id_alumni, $id = null)
    {
        $this->db->from('alumni_prestasi');
        $this->db->where('id_alumni', $id_alumni);
        if ($id != null) {
            $this->db->where('id', $id);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function get_skill($id_alumni, $id = null)
    {
        $this->db->from('alumni_skill');
        $this->db->where('id_alumni', $id_alumni);
        if ($id != null) {
            $this->db->where('id', $id);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function cek_alumni($where, $id_alumni)
    {
        $this->db->select('*');
        $this->db->from('alumni');
        $this->db->where($where);
        $this->db->where('id_alumni != ', $id_alumni);
        return $this->db->count_all_results();
    }

    public function cek_password($password, $id_user)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('password', $password);
        $this->db->where('id_user', $id_user);
        return $this->db->count_all_results();
    }

    public function cek_user($where, $id_user)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($where);
        $this->db->where('id_user != ', $id_user);
        return $this->db->count_all_results();
    }

    public function simpan_alumnijobs($data)
    {
        $this->db->insert('alumni_jobs', $data);
        return $this->db->affected_rows();
    }

    public function simpan_alumnistudy($data)
    {
        $this->db->insert('alumni_study', $data);
        return $this->db->affected_rows();
    }

    public function simpan_pengalaman($data)
    {
        $this->db->insert('alumni_pengalaman', $data);
        return $this->db->affected_rows();
    }

    public function simpan_organisasi($data)
    {
        $this->db->insert('alumni_organisasi', $data);
        return $this->db->affected_rows();
    }

    public function simpan_prestasi($data)
    {
        $this->db->insert('alumni_prestasi', $data);
        return $this->db->affected_rows();
    }

    public function simpan_skill($data)
    {
        $this->db->insert('alumni_skill', $data);
        return $this->db->affected_rows();
    }

    public function update_user($data, $id)
    {
        $this->db->update('user', $data, array('id_user' => $id));
        return $this->db->affected_rows();
    }

    public function update_alumni($data, $id)
    {
        $this->db->update('alumni', $data, array('id_alumni' => $id));
        return $this->db->affected_rows();
    }

    public function update_alumnijobs($data, $id)
    {
        $this->db->update('alumni_jobs', $data, array('id_job' => $id));
        return $this->db->affected_rows();
    }

    public function update_alumnistudy($data, $id)
    {
        $this->db->update('alumni_study', $data, array('id_study' => $id));
        return $this->db->affected_rows();
    }

    public function update_pengalaman($data, $id)
    {
        $this->db->update('alumni_pengalaman', $data, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function update_organisasi($data, $id)
    {
        $this->db->update('alumni_organisasi', $data, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function update_prestasi($data, $id)
    {
        $this->db->update('alumni_prestasi', $data, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function update_skill($data, $id)
    {
        $this->db->update('alumni_skill', $data, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function update_sosmed($data, $id)
    {
        $this->db->update('alumni', $data, array('id_alumni' => $id));
        return $this->db->affected_rows();
    }

    public function delete_pengalaman($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('alumni_pengalaman');
        return $this->db->affected_rows();
    }

    public function delete_organisasi($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('alumni_organisasi');
        return $this->db->affected_rows();
    }

    public function delete_prestasi($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('alumni_prestasi');
        return $this->db->affected_rows();
    }

    public function delete_skill($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('alumni_skill');
        return $this->db->affected_rows();
    }

    public function getProdiById($id_prodi)
    {
        $this->db->select('nama_prodi');
        $this->db->where('id_prodi', $id_prodi);
        return $this->db->get('ref_prodi')->row();
    }

    public function getFakultasName($id_fakultas)
    {
        $this->db->select('nama_fakultas');
        $this->db->where('id_fakultas', $id_fakultas);
        return $this->db->get('ref_fakultas')->row();
    }
}
