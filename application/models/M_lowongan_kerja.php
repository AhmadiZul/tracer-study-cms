<?php 

class M_lowongan_kerja extends CI_Model
{
    public function getLowongan($id_lowongan = null)
    {
        $this->db->select('lp.id_lowongan_pekerjaan, m.id_instansi, m.url_logo, m.nama, lp.posisi, lp.lokasi_kerja, lp.jenis, lp.deskripsi, lp.min_pengalaman, lp.start_publish, lp.end_publish, lp.last_modified_time, lp.status, lp.url_poster, rkk.nama_kab_kota, rp.nama_provinsi, lp.id_pendidikan, rpk.nama_pendidikan, lp.gaji_rate_bawah, lp.gaji_rate_atas, lp.persyaratan');
        $this->db->join('mitra m','lp.id_instansi=m.id_instansi','LEFT');
        $this->db->join('ref_kab_kota rkk','lp.lokasi_kerja=rkk.kode_kab_kota','LEFT');
        $this->db->join('ref_provinsi rp','rkk.kode_provinsi=rp.kode_provinsi','LEFT');
        $this->db->join('ref_pendidikan rpk','lp.id_pendidikan=rpk.id_pendidikan');

        if ($id_lowongan) {
            $this->db->where('id_lowongan_pekerjaan',$id_lowongan);
            return $this->db->get('lowongan_pekerjaan lp')->row();
        }
        $this->db->where('lp.is_active','1');
        $this->db->where('lp.status','Publish');
        $this->db->where('lp.end_publish >= ',date("Y-m-d"));
        $this->db->where('lp.start_publish <= ',date("Y-m-d"));
        return $this->db->get('lowongan_pekerjaan lp')->result();
    }

    public function getLowonganNonDeskripsi($id_lowongan = null)
    {
        $this->db->select('lp.id_lowongan_pekerjaan, m.id_instansi, m.url_logo, m.nama, lp.posisi, lp.lokasi_kerja, lp.jenis, lp.min_pengalaman, lp.start_publish, lp.end_publish, lp.last_modified_time, lp.status, lp.url_poster, rkk.nama_kab_kota, rp.nama_provinsi, lp.id_pendidikan, rpk.nama_pendidikan, lp.gaji_rate_bawah, lp.gaji_rate_atas');
        $this->db->join('mitra m','lp.id_instansi=m.id_instansi','LEFT');
        $this->db->join('ref_kab_kota rkk','lp.lokasi_kerja=rkk.kode_kab_kota','LEFT');
        $this->db->join('ref_provinsi rp','rkk.kode_provinsi=rp.kode_provinsi','LEFT');
        $this->db->join('ref_pendidikan rpk','lp.id_pendidikan=rpk.id_pendidikan');

        if ($id_lowongan) {
            $this->db->where('id_lowongan_pekerjaan',$id_lowongan);
            return $this->db->get('lowongan_pekerjaan lp')->row();
        }
        $this->db->where('lp.is_active','1');
        $this->db->where('lp.status','Publish');
        $this->db->where('lp.end_publish >= ',date("Y-m-d"));
        $this->db->where('lp.start_publish <= ',date("Y-m-d"));
        return $this->db->get('lowongan_pekerjaan lp')->result();
    }

    public function getLamaran($id_alumni)
    {
        $this->db->select('m.nama, lp.posisi, lp.id_lowongan_pekerjaan, apl.status, apl.applied_time');
        $this->db->join('lowongan_pekerjaan lp','apl.id_lowongan_pekerjaan=lp.id_lowongan_pekerjaan','LEFT');
        $this->db->join('mitra m','lp.id_instansi=m.id_instansi','LEFT');
        $this->db->where('id_alumni',$id_alumni);
        return $this->db->get('alumni_apply_lowongan apl')->result();
    }

    public function getMitra($id_instansi)
    {
        $this->db->where('id_instansi',$id_instansi);
        return $this->db->get('mitra')->row();
    }

    public function getMitraByUser()
    {
        $this->db->where('id_user',$this->session->userdata("t_userId"));
        return $this->db->get('mitra')->row();
    }

    public function getApplyLowongan($id_alumni,$id_lowongan)
    {
        $this->db->where('id_lowongan_pekerjaan',$id_lowongan);
        $this->db->where('id_alumni',$id_alumni);
        return $this->db->get('alumni_apply_lowongan')->row_array();
    }

    public function getLowonganPeminat($where) {

        $this->db->start_cache();
        $this->db->select('rp.id_lowongan_pekerjaan, rp.posisi, rep.nama_pendidikan, rp.jenis, (Select count(*) from alumni_apply_lowongan where id_instansi = "'.$where['id_instansi'].'" and id_lowongan_pekerjaan = rp.id_lowongan_pekerjaan) as peminat');
        $this->db->from('lowongan_pekerjaan rp');
        $this->db->join('ref_pendidikan rep','rp.id_pendidikan=rep.id_pendidikan','LEFT');
        $this->db->where('rp.is_active',1);
        $this->db->where('rp.id_instansi',$where['id_instansi']);
        $this->db->order_by('rp.start_publish','DESC');
        $result = $this->db->get()->result_array();
        $this->db->stop_cache();
        $return['isi']    = $result;
        $return['jumlah'] = count($result);
        $return['status'] = true;
        $this->db->flush_cache();
        return $return;
    }

    public function getPelamarPerLowongan($where) {

        $this->db->start_cache();
        $this->db->select('aap.id_apply, aap.applied_time, aap.status, a.id_alumni, a.nama, rep.nama_prodi');
        $this->db->from('alumni_apply_lowongan aap');
        $this->db->join('alumni a','aap.id_alumni=a.id_alumni','LEFT');
        $this->db->join('ref_perguruan_tinggi rpt','a.id_perguruan_tinggi=rpt.id_perguruan_tinggi','LEFT');
        $this->db->join('ref_prodi rep','a.id_prodi=rep.id_prodi','LEFT');
        $this->db->where('aap.id_lowongan_pekerjaan',$where['id_lowongan_pekerjaan']);
        $this->db->order_by('aap.applied_time','DESC');
        $result = $this->db->get()->result_array();
        $this->db->stop_cache();
        $return['isi']    = $result;
        $return['jumlah'] = count($result);
        $return['status'] = true;
        $this->db->flush_cache();
        return $return;
    }

    public function simpan_lowongan($data)
    {
        $this->db->insert('lowongan_pekerjaan', $data);
        return $this->db->affected_rows();
    }

    public function simpan_log($data)
    {
        $this->db->insert('alumni_apply_lowongan_log', $data);
        return $this->db->affected_rows();
    }

    public function simpan_lamaran($data)
    {
        $this->db->insert('alumni_apply_lowongan', $data);
        return $this->db->affected_rows();
    }

    public function ubah_lowongan($data, $id)
    {
        $this->db->update('lowongan_pekerjaan', $data, ['id_lowongan_pekerjaan' => $id]);
        return $this->db->affected_rows();
    }

    public function ubah_apply($data, $id)
    {
        $this->db->update('alumni_apply_lowongan', $data, ['id_apply' => $id]);
        return $this->db->affected_rows();
    }
}
