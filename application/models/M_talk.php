<?php

class M_talk extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function edit($id)
    {
        $return['status'] = 0;
        $return['data']   = [];

        $get = $this->db
        ->select('ct.*, pt.nama_pendek')
        ->join('ref_perguruan_tinggi pt', 'pt.id_perguruan_tinggi=ct.id_perguruan_tinggi_lokasi', 'left')
        ->where('ct.id', $id)
        ->get('company_talks ct');
        if ($get->num_rows() != 0) {
            $data = $get->row_object();

            $return['status'] = 201;
            $return['data']   = $data;
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }

        return $return;
    }

    public function simpan_talk($data)
    {
        $this->db->insert('company_talks', $data);
        return $this->db->affected_rows();
    }

    public function ubah_talk($data, $id)
    {
        $this->db->update('company_talks', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }
    public function detail($id)
    {
        $return['status'] = 0;
        $return['data']   = [];

        $get = $this->db
        ->select('ct.*, pt.nama_pendek')
        ->join('ref_perguruan_tinggi pt', 'pt.id_perguruan_tinggi=ct.id_perguruan_tinggi_lokasi', 'left')
        ->where('ct.id', $id)
        ->get('company_talks ct');
        if ($get->num_rows() != 0) {
            $data = $get->row_object();

            $fasilitas = [];
            if ($data->fasilitas != null) {
                $dataFasilitas = json_decode($data->fasilitas);
                if (count($dataFasilitas)) {
                    foreach ($dataFasilitas as $k => $v) {
                        $fasilitas[$k] = $v->fasilitas;
                    }
                }
            }
            $data->fasilitas = $this->getFasilitas($fasilitas, false);
            $return['status'] = 201;
            $return['data']   = $data;
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }

        return $return;
    }

    public function peserta($params)
    {
        $return = array('total' => 0, 'rows' => array());

        $this->db->start_cache();
        $this->db->select('ctp.id, ctp.waktu_daftar, ctp.is_valid , a.nim, a.nama ,a.jenis_kelamin, a.email,
        a.no_hp, rpt.nama_pendek, rp.nama_prodi, a.alamat');

        if (isset($data['sSearch']) || $params['sSearch'] != '') {
            $search = $this->db->escape_str($params['sSearch']);
            $this->db->where("(a.nama LIKE '%{$search}%')");
        }
        if (!empty($data['idCT']) || $params['idCT'] != '') {
            $idCT = $this->db->escape_str($params['idCT']);
            $this->db->where("ctp.id_company_talks", $idCT);
        }
        if (!empty($data['status']) || $params['status'] != '') {
            $status = $this->db->escape_str($params['status']);
            $this->db->where("ctp.is_valid", $status);
        }
        $this->db->join('alumni a', 'a.id_alumni = ctp.id_alumni', 'left');
        $this->db->join('ref_perguruan_tinggi rpt', 'rpt.id_perguruan_tinggi = a.id_perguruan_tinggi', 'left');
        $this->db->join('ref_prodi rp', 'rp.id_prodi = a.id_prodi', 'left');

        $this->db->stop_cache();
        $rs = $this->db->count_all_results('company_talks_pendaftar ctp');
        $return['total'] = $rs;
        if ($return['total'] > 0) {
            $this->db->limit($params['limit'], $params['start']);
            $this->db->order_by('ctp.waktu_daftar', 'desc');
            $rs = $this->db->get('company_talks_pendaftar ctp');
            if ($rs->num_rows())
                $return['rows'] = $rs->result_array();
        }
        $this->db->flush_cache();
        return $return;
    }
    public function getFasilitas($params = null, $single = false)
    {
        $return['status'] = 0;
        $return['data']   = [];

        $this->db->select('*');
        $this->db->from('company_talks_fasilitas');
        if ($params != null) {
            if ($single) {
                $this->db->where('id', $params);
            }else{
                $this->db->where_in('id', $params);
            }
        }

        $get = $this->db->get();
        if ($get->num_rows() != 0) {
            $data = ($single ? $get->row_object():$get->result_object());
            $return['status'] = 201;
            $return['data']   = $data;
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }
        return $return;
    }

    public function insert($params)
    {
        $return['status']  = 0;
        $return['message'] = '';

        $this->db->insert('company_talks', $params);
        if ($this->db->affected_rows()) {
            $return['status']  = 201;
            $return['message'] = 'Pendaftaran company talk berhasil. Silahkan tunggu verifikasi dari admin.';
        } else {
            $return['status']  = 500;
            $return['message'] = 'Pendaftaran company talk gagal. Silahkan cek kembali data anda.';
        }
        return $return;
    }
    public function update($params, $id)
    {
        $return['status']  = 0;
        $return['message'] = '';

        $this->db->where('id', $id);
        $this->db->update('company_talks', $params);
        if ($this->db->affected_rows()) {
            $return['status']  = 201;
            $return['message'] = 'Data company talk berhasil diupdate.';
        } else {
            $return['status']  = 500;
            $return['message'] = 'Data company talk gagal diupdate.';
        }
        return $return;
    }

    public function getUniversitas($params = null, $single = false)
    {
        $return['status'] = 0;
        $return['data']   = [];

        $this->db->select('*');
        $this->db->from('ref_perguruan_tinggi');
        if ($params != null) {
            $this->db->where($params);
        }

        $get = $this->db->get();
        if ($get->num_rows() != 0) {
            $data = ($single ? $get->row_object() : $get->result_object());
            $return['status'] = 201;
            $return['data']   = $data;
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }
        return $return;
    }

    public function delete($id)
    {
        $return['status']  = 0;
        $return['message'] = '';

        $check = $this->pesertaCT($id);
        if ($check['status'] == 201) {
            $return['status']  = 500;
            $return['message'] = 'Data tidak bisa dihapus. Data sudah digunakan alumni';
        } else {
            $this->db->where('id', $id);
            $this->db->delete('company_talks');
            if ($this->db->affected_rows()) {
                $return['status']  = 201;
                $return['message'] = 'Data company talk berhasil dihapus';
            } else {
                $return['status']  = 500;
                $return['message'] = 'Data company talk gagal dihapus';
            }
        }
        return $return;

    }

    public function pesertaCT($id)
    {
        $return['status'] = 0;
        $return['data']   = [];

        $get = $this->db->where('id_company_talks', $id)->get('company_talks_pendaftar');

        if ($get->num_rows() != 0) {
            $return['status'] = 201;
            $return['data']   = $get->result_object();
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }
        return $return;
    }

    public function validasi($id)
    {
        $return['status'] = 0;
        $return['data']   = [];

        $this->db->select('ctp.id, ctp.waktu_daftar, ctp.is_valid , a.nim, a.nama ,a.jenis_kelamin, a.email,
        a.no_hp, rpt.nama_pendek, rp.nama_prodi, a.alamat');
        $this->db->join('alumni a', 'a.id_alumni = ctp.id_alumni', 'left');
        $this->db->join('ref_perguruan_tinggi rpt', 'rpt.id_perguruan_tinggi = a.id_perguruan_tinggi', 'left');
        $this->db->join('ref_prodi rp', 'rp.id_prodi = a.id_prodi', 'left');
        $this->db->where('ctp.id', $id);

        $get = $this->db->get('company_talks_pendaftar ctp');
        if ($get->num_rows() != 0) {
            $return['status'] = 201;
            $return['data']   = $get->row_object();
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }
        return $return;
    }

    public function verifikasi($data, $id)
    {
        $return['status']  = 0;
        $return['message'] = '';

        $this->db->where('id', $id);
        $this->db->update('company_talks_pendaftar', $data);
        if ($this->db->affected_rows()) {
            $return['status']  = 201;
            $return['message'] = 'Verifikasi peserta company talk berhasil dilakukan.';
        } else {
            $return['status']  = 500;
            $return['message'] = 'Verifikasi peserta company talk gagal dilakukan.';
        }
        return $return;
    }

    public function getCompanyTalkList()
    {
        $return['status'] = 0;
        $return['data']   = [];

        // $id_pt = getAlumniIDPT();

        $get = $this->db
            ->select('ct.*, pt.nama_pendek, m.nama as mitra, m.sektor')
            ->join('ref_perguruan_tinggi pt', 'pt.id_perguruan_tinggi=ct.id_perguruan_tinggi_lokasi', 'left')
            ->join('mitra m', 'm.id_instansi=ct.id_mitra', 'left')
            ->where('ct.is_approved', '1')
            ->get('company_talks ct');
        if ($get->num_rows() != 0) {
            $data = [];
            foreach ($get->result_array() as $k => $v) {
                $data[$k] = $v;
                $data[$k]['waktu_akhir_daftar'] = $this->tanggalindo->konversi($v['waktu_akhir_daftar']);;
                $data[$k]['waktu_awal_daftar'] = $this->tanggalindo->konversi($v['waktu_awal_daftar']);;
                $data[$k]['tanggal_pelaksanaan'] = $this->tanggalindo->konversi($v['tanggal_pelaksanaan']);;
                $fasilitas = [];
                if ($v['fasilitas'] != null) {
                    $dataFasilitas = json_decode($v['fasilitas']);
                    if (count($dataFasilitas)) {
                        foreach ($dataFasilitas as $key => $vall) {
                            $fasilitas[$key] = $vall->fasilitas;
                        }
                    }
                }
                $data[$k]['fasilitas'] = $this->getFasilitas($fasilitas, false);
                $data[$k]['is_available'] = $this->checkRegistered($v['id']);
            }

            $return['status'] = 201;
            $return['data']   = $data;
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }

        return $return;
    }

    public function checkRegistered($talk_id)
    {
        $alumni_id = getAlumniID();

        $get = $this->db
        ->where('id_company_talks', $talk_id)
        ->where('id_alumni', $alumni_id)
        ->get('company_talks_pendaftar');
        if ($get->num_rows() != 0) {
            $row = $get->row_object();
            return $row;
        } else {
            return [];
        }
    }

    public function checkQuota($id)
    {
        $return['status']  = 0;
        $return['message'] = '';

        $current = $this->edit($id);

        if ($current['status'] == 201) {
            $date_now = strtotime(date('Y-m-d H:i:s'));
            $tgl_awal = strtotime($current['data']->waktu_awal_daftar);
            $tgl_akhir = strtotime($current['data']->waktu_akhir_daftar);

            if ($tgl_awal < $date_now && $date_now > $tgl_akhir) {
                $return['status']  = 500;
                $return['message'] = 'Waktu pendaftaran sudah berakhir.';
            }else if ($current['data']->is_approved != '1') {
                $return['status']  = 500;
                $return['message'] = 'Company talk belum diverifikasi oleh admin Polbangtan/Pepi.';
            } else{

                $get = $this->db->select('count(*) as total')
                    ->from('company_talks_pendaftar')
                    ->where('id_company_talks', $id)
                    ->get();
                if ($get->num_rows() != 0) {
                    $row = $get->row_object();

                    if ($row->total == $current['data']->kapasitas) {
                        $return['status']  = 500;
                        $return['message'] = 'Kapasitas peserta sudah memenuhi kuota.';
                    }else{
                        $return['status']  = 201;
                        $return['message'] = 'Pendaftaran bisa dilakukan.';
                    }
                } else {
                    $return['status']  = 201;
                    $return['message'] = 'Pendaftaran bisa dilakukan.';
                }
            }

        } else {
            $return['status']  = 500;
            $return['message'] = 'Data tidak ditemukan';
        }
        return $return;
    }

    public function registered($data)
    {
        $return['status']  = 0;
        $return['message'] = '';

        $check = $this->checkQuota($data['id_company_talks']);
        if ($check['status'] == 500) {
            $return['status']  = 500;
            $return['message'] = $check['message'];
        } else {
            $this->db->insert('company_talks_pendaftar', $data);
            if ($this->db->affected_rows()) {
                $return['status']  = 201;
                $return['message'] = 'Pendaftaran sebagai peserta company talk berhasil dilakukan. Silahkan tunggu verifikasi penyelelenggara';
            } else {
                $return['status']  = 500;
                $return['message'] = 'Pendaftaran sebagai peserta company talk gagal dilakukan. Silahkan cek kembali data anda';
            }
        }
        return $return;
    }
}
