<?php

class M_register extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function simpan_user($data){
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

    public function simpan_alumni($data) {
        $this->db->insert('alumni', $data);
        return $this->db->affected_rows();
    }

    public function simpan_alumnijobs($data) {
        $this->db->insert('alumni_jobs', $data);
        return $this->db->affected_rows();
    }

    public function simpan_alumnistudy($data) {
        $this->db->insert('alumni_study', $data);
        return $this->db->affected_rows();
    }

    public function simpan_mitra($data) {
        $this->db->insert('mitra', $data);
        return $this->db->affected_rows();
    }
}
