<?php

class M_career extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function edit($id)
    {
        $return['status'] = 0;
        $return['data']   = [];

        $get = $this->db
        ->select('cf.*, pt.nama_resmi, pt.nama_pendek')
        ->join('ref_perguruan_tinggi pt', 'pt.id_perguruan_tinggi=cf.id_perguruan_tinggi_pelaksana', 'left')
        ->where('cf.id', $id)
        ->get('career_fair cf');
        if ($get->num_rows() != 0) {
            $return['status'] = 201;
            $return['data']   = $get->row_object();
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }

        return $return;
    }

    public function mitra($params)
    {
        $return = array('total' => 0, 'rows' => array());

        $this->db->start_cache();
        $this->db->select('cfp.id, cfp.waktu_pendaftaran, cfp.tulisan_di_papan,
        cfp.url_formulir, cfp.is_approved, m.nama, m.jenis, m.sektor, cp.paket');

        if (isset($data['sSearch']) || $params['sSearch'] != '') {
            $search = $this->db->escape_str($params['sSearch']);
            $this->db->where("(m.nama LIKE '%{$search}%')");
        }
        if (!empty($data['id_career']) || $params['id_career'] != '') {
            $id_career = $this->db->escape_str($params['id_career']);
            $this->db->where("cfp.id_career_fair", $id_career);
        }
        $this->db->join('mitra m', 'm.id_instansi = cfp.id_mitra', 'left');
        $this->db->join('career_fair_paket cp', 'cp.id = cfp.id_paket_kerjasama', 'left');
        $this->db->where('cfp.is_approved','1');

        $this->db->stop_cache();
        $rs = $this->db->count_all_results('career_fair_pendaftar cfp');
        $return['total'] = $rs;
        if ($return['total'] > 0) {
            $this->db->limit($params['limit'], $params['start']);
            $this->db->order_by('cfp.waktu_pendaftaran', 'desc');
            $rs = $this->db->get('career_fair_pendaftar cfp');
            if ($rs->num_rows())
                $return['rows'] = $rs->result_array();
        }
        $this->db->flush_cache();
        return $return;
    }

    public function checkRegisterAvailable($career_id)
    {
        $return['status'] = 0;
        $return['data']   = [];

        $mitra = getMitraID();

        $this->db->select('cf.*, cp.paket');
        $this->db->from('career_fair_pendaftar cf');
        $this->db->join('career_fair_paket cp', 'cp.id = cf.id_paket_kerjasama', 'left');
        $this->db->where('cf.id_career_fair', $career_id);
        $this->db->where('cf.id_mitra', $mitra);
        $get = $this->db->get();

    if ($get->num_rows() != 0) {
            $data = $get->row_object();
            $data->url_formulir = base_url('admin/'.$data->url_formulir);
            $return['status'] = 201;
            $return['data']   = $data;
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }
        return $return;
    }

    public function paket()
    {
        $return['status'] = 0;
        $return['data']   = [];

        $get = $this->db->get('career_fair_paket');

        if ($get->num_rows() != 0) {
            $return['status'] = 201;
            $return['data']   = $get->result_object();
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }
        return $return;
    }

    public function daftar($params)
    {
        $return['status']  = 0;
        $return['message'] = '';

        $this->db->insert('career_fair_pendaftar', $params);
        if ($this->db->affected_rows()) {
            $return['status']  = 201;
            $return['message'] = 'Pendaftaran berhasil. Silahkan tunggu verifikasi dari penyelenggara';
        } else {
            $return['status']  = 500;
            $return['message'] = 'Pendaftaran gagal. Silahkan cek kembali berkas pendaftaran anda';
        }
        return $return;
    }
}
