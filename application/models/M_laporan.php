<?php

class M_laporan extends CI_Model
{

    private $table = 'laporan';

    function __construct()
    {
        parent::__construct();
    }

    public function getTahunLaporan()
    {
        $result = $this->db->query("SELECT * FROM laporan ORDER BY tahun DESC LIMIT 3");
        // var_dump($result->result());
        // die();
        return $result->result();
    }

    public function getLaporan($where)
    {
        $this->db->from('laporan');
        $this->db->order_by('tahun');
        if ($where) {
            $this->db->where($where);
            $this->db->order_by('tahun');
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }
}