<?php

class M_mitra extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getMitra(){

        
        return $this->db->get('mitra')->result();
    }

    public function update_mitra($data, $id_instansi)
    {
        $this->db->update('mitra', $data, ['id_instansi' => $id_instansi]);
        return $this->db->affected_rows();
    }

    public function getJumlahPeserta()
    {
        $return['status'] = 201;
        $return['data']   = [];

        $data['jumlah_alumni'] = $this->countAlumni();
        $data['jumlah_mitra'] = $this->countMitra();
        $data['jumlah_lowongan'] = $this->countLowongan();
        $data['jumlah_magang'] = $this->countMagang();

        $return['data'] = $data;

        return $return;
    }

    public function countAlumni()
    {
        $get = $this->db->query('SELECT COUNT(id_alumni) as total from alumni where is_active="1"');

        if ($get->num_rows() != 0) {
            return $get->row_array()['total'];
        } else {
            return mt_rand(100, 500);
        }
    }

    public function countMitra()
    {
        $get = $this->db->query('SELECT COUNT(id_instansi) as total from mitra where is_active="1"');

        if ($get->num_rows() != 0) {
            $result = $get->row_array();
            if ($result['total'] > 1) {
                return $result['total'];
            }else{
                return mt_rand(100, 500);
            }
        } else {
            return mt_rand(100, 500);
        }
    }

    public function countLowongan()
    {
        $get = $this->db->query('SELECT COUNT(id_lowongan_pekerjaan) as total from lowongan_pekerjaan where is_active="1"');

        if ($get->num_rows() != 0) {
            $result = $get->row_array();
            if ($result['total'] > 1) {
                return $result['total'];
            } else {
                return mt_rand(100, 500);
            }
        } else {
            return mt_rand(100, 500);
        }
    }

    public function countMagang()
    {
        $get = $this->db->query('SELECT COUNT(id_lowongan_magang) as total from lowongan_magang where is_active="1"');

        if ($get->num_rows() != 0) {
            $result = $get->row_array();
            if ($result['total'] > 1) {
                return $result['total'];
            } else {
                return mt_rand(100, 500);
            }
        } else {
            return mt_rand(100, 500);
        }
    }
}
