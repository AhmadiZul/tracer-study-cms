<?php

class BaseController extends CI_Controller
{

    protected $template = "app_back";
    protected $module = "";
    protected $data = array();
    private $whitelistUrl = [
        '',
        'forgot_password',
        'forgot_password_reset',
        'aktifkan_akun',
        'berita',
        'logout',
        'program',
        'index/login',
        'tracer'
    ];
    public $loginBehavior = true;

    public function __construct()
    {

        parent::__construct();
        $this->load->model('m_activity_log');
        $this->load->model('M_auth');

        $userId = $this->session->userdata('t_userId');
        if (uri_string() == "" && $this->input->post("login-button") != null) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $role = $this->input->post('role');
            $captcha = trim($this->input->post('captcha'));
            $captcha_answer = trim($this->session->userdata('captchaword'));
            if ($captcha != $captcha_answer) {
                $this->session->set_flashdata('errorMessage', "Gagal login, Capcha tidak sesuai.");
                $this->data["errorMessage"] = "Gagal login, Capcha tidak sesuai.";
                $this->template = "login";
                // $this->data['recaptcha'] = $this->create_captcha();
                $this->data['title'] = 'Login';
                    redirect(site_url('Login/alumni'));
                
            }else {
            
            $result = $this->db->query("SELECT a.id_user, a.username, a.email, a.real_name,"
                . " a.id_group, a.is_active, b.nama_group, b.keterangan, b.dbusername, al.id_alumni, al.url_photo, m.url_logo "
                . " FROM user a INNER JOIN user_group b ON a.id_group = b.id_group "
                . " LEFT JOIN alumni al ON a.id_user=al.id_user"
                . " LEFT JOIN mitra m ON a.id_user=m.id_user"
                . " WHERE a.username = ? and a.password=SHA2(?,256) "
                . " AND a.is_active = 1", array($username, $password));

            if ($result->num_rows() != 0) {
                $row = $result->first_row();
                $id_user = $row->id_user;
                $id_alumni = $row->id_alumni;
                $nama = $row->real_name;
                $email = $row->email;
                $id_group = $row->id_group;
                $nama_group = $row->nama_group;
                $is_active = $row->is_active;
                $dbusername = $row->dbusername;
                $url_photo = $row->url_photo;

                if ($id_group == 3) {
                    $url_photo = $row->url_logo;
                }

                $this->session->set_userdata("t_userId", $id_user);
                $this->session->set_userdata("t_alumniId", $id_alumni);
                $this->session->set_userdata("t_url_photo", $url_photo);
                $this->session->set_userdata("t_nama", $nama);
                $this->session->set_userdata("t_email", $email);
                $this->session->set_userdata("t_idGroup", $id_group);
                $this->session->set_userdata("t_namaGroup", $nama_group);
                $this->session->set_userdata("t_isActive", $is_active);
                $this->session->set_userdata("t_dbUsername", $dbusername);
                $this->config->set_item('database_name', $this->session->userdata('t_dbUsername'));

                $this->setDatabase();
                //CHANGE DATABASE BASED ON USER

                $this->setUserData();
                $this->setLog($id_user);
                redirect('alumni/dashboard');
            } else {
                // $this->session->set_flashdata('errorMessage', "Gagal login, silahkan periksa kembali informasi login Anda.");
                $message = "Gagal login, silahkan periksa kembali informasi login Anda.";
                // $this->data['recaptcha'] = $this->create_captcha();
                if ($role == 1) {
                    redirect('Login/alumni?message=' . $message, 'refresh');
                }
                redirect('Login/mitra?message=' . $message, 'refresh');
            }
        }
        } else if (!$userId && uri_string() == "dashboard") { // Accessing index page and there is no user session (login form state)
            //$this->template = "app_front";

            // cookie
            $cookie = get_cookie('yess_auth');

            if ($cookie <> '') {
                $result = $this->db->query("SELECT a.id_user, a.username, a.email, a.real_name,"
                    . " a.id_group, a.is_active,a.cookie, b.nama_group, b.keterangan, b.dbusername "
                    . " FROM user a INNER JOIN user_group b ON a.id_group = b.id_group "
                    . " WHERE a.cookie = ? "
                    . " AND a.is_active = 1", array($cookie));

                if ($result->num_rows() == 0) {
                    $this->session->set_flashdata('errorMessage', "Gagal login, silahkan periksa kembali informasi login Anda.");
                    // $this->data["errorMessage"] = "Gagal login, silahkan periksa kembali informasi akun login Anda.";
                    $this->template = "login_app";
                    // $this->data['recaptcha'] = $this->create_captcha();
                    $this->data['title'] = 'Login';
                    redirect(base_url('Login'));
                    // $this->render("auth/login");
                } else {
                    $row = $result->first_row();
                    $id_user = $row->id_user;
                    $nik = $row->username;
                    $nama = $row->real_name;
                    $email = $row->email;
                    $id_group = $row->id_group;
                    $nama_group = $row->nama_group;
                    $is_active = $row->is_active;
                    $dbusername = $row->dbusername;

                    $this->session->set_userdata("t_userId", $id_user);
                    $this->session->set_userdata("t_nik", $nik);
                    $this->session->set_userdata("t_nama", $nama);
                    $this->session->set_userdata("t_email", $email);
                    $this->session->set_userdata("t_idGroup", $id_group);
                    $this->session->set_userdata("t_namaGroup", $nama_group);
                    $this->session->set_userdata("t_isActive", $is_active);
                    $this->session->set_userdata("t_dbUsername", $dbusername);
                    $this->config->set_item('database_name', $this->session->userdata('t_dbUsername'));

                    $this->setDatabase();
                    //CHANGE DATABASE BASED ON USER

                    $this->setUserData();
                    $this->setLog($id_user);

                    redirect('dashboard');
                }
            } else {
                $this->template = "login";
                if ($this->input->get("access_without_login") == "true") {
                    $this->data["errorMessage"] = "Session anda telah berakhir, silahkan login kembali untuk masuk ke dashboard.";
                } else if ($this->input->get("logout") == "true") {
                    $this->data["successMessage"] = "Anda telah keluar.";
                } else if ($this->input->get("forgot_password") == "true") {
                    $this->data["successMessage"] = "Password anda berhasil diperbarui. Silahkan login dengan password baru.";
                }
                //$this->data['title'] = 'Beranda';
                //$this->render("index");
                $this->data['title'] = 'Beranda';
                $this->data['captcha'] = $this->create_captcha();
                redirect(base_url('Login'));

                // $this->render("beranda");
            }


            // end


        } else if (!$userId && !in_array(uri_string(), $this->whitelistUrl) && $this->loginBehavior) { // Accessing user page and there is no user session
            //$this->template = "app_front";
            $this->template = "login_app";
            redirect("Login?access_without_login=true");
        } else if ($userId != null) { // Accessing user page and there is user session
            $this->setUserData();
        }


        if (is_array($this->input->get())) {
            foreach ($this->input->get() as $key => $value) {
                $this->data[$key] = $value;
            }
        }

        if (is_array($this->input->post())) {
            foreach ($this->input->post() as $key => $value) {
                if ($key == "description" || $key == "email" || $key == "is_active" || $key == "is_soft_delete" || $key == "username" || $key == "real_name") {
                    $this->data[$key . "Input"] = $value;
                } else {
                    $this->data[$key] = $value;
                }
            }
        }
    }

    protected function setRegisterStatus($id_user)
    {
        $get = $this->db->query("SELECT registration_type, is_ppiu_approved from cpm where id_user=?", array($id_user));
        if ($get->num_rows() != 0) {
            $row = $get->row_array();
            if (in_array($row['registration_type'], [2, 3, 4]) && $row['is_ppiu_approved'] != 1) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    protected function setUserData()
    {
        $this->data["t_userId"] = $this->session->userdata("t_userId");
        $this->data["t_url_photo"] = $this->session->userdata("t_url_photo");
        $this->data["t_nama"] = $this->session->userdata("t_nama");
        $this->data["t_email"] = $this->session->userdata("t_email");
        $this->data["t_idGroup"] = $this->session->userdata("t_idGroup");
        $this->data["t_isActive"] = $this->session->userdata("t_isActive");
        $this->data["t_dbUsername"] = $this->session->userdata("t_dbUsername");

        $result = $this->db->query("SELECT DISTINCT nama_modul, hak_akses FROM akses_group_modul WHERE id_group = ? ORDER BY nama_modul", array($this->session->userdata("t_idGroup")));

        $this->data["userMenus"] = array();
        if ($result) {
            foreach ($result->result() as $row) {
                $this->data["userMenus"][] = $row->nama_modul . "." . $row->hak_akses;
            }
        }
    }

    protected function create_captcha()
    {

        $files = glob('./public/captcha/*'); // get all file names
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file); // delete file
        }

        $number = ' ' . rand(1000, 5000) . ' ';
        $vals = array(
            'word'          => $number,
            'img_path'      => './public/captcha/',
            'img_url'       => base_url('public/captcha'),
            'font_path'     => FCPATH . 'public/assets/fonts/poppins/Poppins-SemiBold.ttf',
            'img_width'     => '250',
            'img_height'    => 60,
            'expiration'    => 3600,
            'word_length'   => 12,
            'font_size'     => 24,
            'img_id'        => 'imageid',
            'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

            // White background and border, black text and red grid
            'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
            )
        );

        $cap = create_captcha($vals);
        $this->session->set_userdata('captchaword', $cap['word']);
        return $cap['image'];
    }

    protected function unSetUserData()
    {
        $this->session->unset_userdata("t_userId");
        $this->session->unset_userdata("t_url_photo");
        $this->session->unset_userdata("t_nama");
        $this->session->unset_userdata("t_email");
        $this->session->unset_userdata("t_idGroup");
        $this->session->unset_userdata("t_isActive");
        $this->session->unset_userdata("t_dbUsername");
        delete_cookie('yess_auth');
    }

    protected function render($filename = null)
    {
        $template = $this->load->view("template/" . $this->template, $this->data, true);

        $content = $this->load->view(($this->module != "" ? $this->module . "/" : "") . $filename, $this->data, true);

        if ($this->module != NULL) {
            if (in_array($this->module . ".access", $this->data["userMenus"]) == 0) {
                $message = "Maaf, Anda tidak memiliki akses ke halaman ini.";
                echo "<script type='text/javascript'>alert('$message');</script>";

                if ($this->session->userdata("t_idGroup") == 4) {
                    redirect('alumni/dashboard');
                }
                if ($this->session->userdata("t_idGroup") == 3) {
                    redirect('mitra/dashboard');
                }
            }
        }
        exit(str_replace("{CONTENT}", $content, $template));
    }

    protected function cek_hak_akses($hak_akses)
    {
        $cek = $this->db->query("SELECT * FROM `akses_group_modul` WHERE nama_modul=? AND hak_akses=? AND id_group=?", array($this->module, $hak_akses, $this->session->userdata("t_idGroup")))->row_array();
        if (empty($cek)) {
            $message = "Maaf, Anda tidak memiliki akses ke halaman ini.";
            echo "<script type='text/javascript'>alert('$message');</script>";
            redirect();
        } else {
            $hak_akses = $this->db->query("SELECT hak_akses FROM `akses_group_modul` WHERE nama_modul=? AND id_group=?", array($this->module, $this->session->userdata("t_idGroup")))->result_array();
            foreach ($hak_akses as $row) {
                $hasil[] = $row['hak_akses'];
            }
            return $hasil;
        }
    }

    protected function setDatabase()
    {
        $dbUsername = $this->session->userdata('t_dbUsername');
        $this->load->database($dbUsername, FALSE, TRUE); //CHANGE DATABASE BASED ON USER
    }

    protected function setLog($id_user)
    {
        $activity = "LOG IN";
        $page_url = base_url();

        $this->m_activity_log->insert($id_user, $activity, $page_url);
    }

    protected function sendEmail($to, $subject, $data, $template)
    {
        $this->email->from('admin@yess.com', 'YESS');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($this->load->view($template, $data, true));
        $this->email->send();
    }

    protected function renderTo($filename = null)
    {
        $template = $this->load->view("template/" . $this->template, $this->data, true);

        $content = $this->load->view("/" . $filename, $this->data, true);

        exit(str_replace("{CONTENT}", $content, $template));
    }

    protected function responseJson($response, $code = 200)
    {
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($code)
            ->set_output(json_encode($response));
    }

    protected function _setOutput($view, $output = null, $template = 'template/app_back', $scripts = null)
    {
        if ($this->module != NULL) {
            if (in_array($this->module . ".access", $this->data["userMenus"]) == 0) {
                $this->session->set_flashdata('false', 'Mohon Maaf, Anda tidak memiliki akses ke halaman tersebut.');
                $message = "Maaf, Anda tidak memiliki akses ke halaman ini.";
                echo "<script type='text/javascript'>alert('$message');</script>";
                redirect();
            }
        }

        if (isset($output->isJSONResponse) && $output->isJSONResponse) {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $x = array_merge($this->data, ['output' => $output]);

        if ($scripts != null) {
            $x = array_merge($x, ['scripts' => $scripts]);
        }

        $this->layout->set_template($template);
        $this->layout->CONTENT->view($view, $x);
        $this->layout->publish();
    }
}
