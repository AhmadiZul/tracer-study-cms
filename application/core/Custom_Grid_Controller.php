<?php
class GridController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function uploadDoc($uploadData, $uploadPath, $max_size = 2048, $allowed_types = "jpg|png|jpeg", callable $callbackRename = null)
    {
        $fieldName = $uploadData->field_name;
        $filename = ($callbackRename != null) ? $callbackRename($_FILES[$fieldName]['name']) : $_FILES[$fieldName]['name'];

        $filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
        $filename = preg_replace("/([^a-zA-Z0-9\-\_]+?){1}/i", '_', $filename);

        $config['file_name']        = $filename;
        $config['upload_path']      = $uploadPath;
        $config['allowed_types']    = $allowed_types;
        $config['max_size']         = $max_size;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($uploadData->field_name)) {
            return [
                "status"  => false,
                "message" => $this->upload->display_errors()
            ];
        }
        return [
            "status"    => true,
            "message"   => "Berhasil upload",
            "data"      => $this->upload->data()
        ];
    }

    protected function _setOutput($view, $output = null, $template = 'template/app', $scripts = null)
    {
        if (isset($output->isJSONResponse) && $output->isJSONResponse) {
            header('Content-Type: application/json; charset=utf-8');
            echo $output->output;
            exit;
        }
        $x = array_merge($this->data, ['output' => $output]);

        if ($scripts != null) {
            $x = array_merge($x, ['scripts' => $scripts]);
        }

        $this->layout->set_template($template);
        $this->layout->CONTENT->view($view, $x);
        $this->layout->publish();
    }
}
