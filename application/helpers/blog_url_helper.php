<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function uploadImage($nama_file, $path_folder, $prefix)
{
  $ci = &get_instance();

  $response['success'] = false;
  $response['file_name'] = '';
  $nama_foto = "";
  if (!empty($_FILES[$nama_file]['name'])) {
    list($width, $height) = getimagesize($_FILES[$nama_file]['tmp_name']);
    $config['upload_path'] = 'public/uploads/' . $path_folder; //path folder file upload
    $config['allowed_types'] = 'gif|jpg|jpeg|png|jpeg|bmp'; //type file yang boleh di upload
    $config['max_size'] = '3000';
    $config['file_name'] = $prefix . '_' . date('ymdhis'); //enkripsi file name upload
    $ci->load->library('upload');
    $ci->upload->initialize($config);
    if ($ci->upload->do_upload($nama_file)) {
      $file_foto = $ci->upload->data();
      // $con['image_library']='gd2';
      // $con['source_image']= './public/uploads/'.$path_folder.'/'.$file_foto['file_name'];
      // $con['create_thumb']= FALSE;
      // $con['maintain_ratio']= TRUE;
      // $con['quality']= '50%';
      // $con['width']= round($width/5);
      // $con['height']= round($height/5);
      // $con['new_image']= './public/uploads/'.$path_folder.'/'.$file_foto['file_name'];
      // $ci->load->library('image_lib');
      // $ci->image_lib->initialize($con);
      $nama_foto = '/public/uploads/' . $path_folder . '/' . $file_foto['file_name'];
      $response['success'] = true;
      $response['file_name'] = $nama_foto;
    }
  }
  return $response;
}

function uploadBerkas($nama_file, $path_folder, $prefix)
{
  $ci = &get_instance();

  $response['success'] = false;
  $response['file_name'] = '';
  $nama_foto = "";
  if (!empty($_FILES[$nama_file]['name'])) {
    $config['upload_path'] = 'admin/public/uploads/career-fair/'; //path folder file upload
    // $config['upload_path'] = 'public/uploads/' . $path_folder; //path folder file upload
    $config['allowed_types'] = 'pdf|docx|xlsx|xls|jpeg|jpg|png|bmp'; //type file yang boleh di upload
    $config['max_size'] = '3000';
    $config['file_name'] = $prefix . '-' . date('ymdhis'); //enkripsi file name upload
    $ci->load->library('upload');
    $ci->upload->initialize($config);
    if ($ci->upload->do_upload($nama_file)) {
      $file_foto = $ci->upload->data();
      $nama_foto =  $path_folder . '/' . $file_foto['file_name'];
      $response['success'] = true;
      $response['file_name'] = $nama_foto;
    }else{
      $response['success'] = false;
      $response['error'] = $ci->upload->display_errors();

    }
  }
  return $response;
}

function getID()
{
  $ci =& get_instance();
  $get = $ci->db->query("SELECT id_pendaftar from pendaftar where id_user=?", array($ci->session->userdata('pwmp_userId')));
  if ($get->num_rows()!=0) {
    return $get->row_array()['id_pendaftar'];
  } else {
    return 0;
  }

}


function getTahun()
{
  $ci  = &get_instance();

  $prov = $ci->db->query("SELECT * FROM ref_tahun where is_active='1' ");

  if ($prov->num_rows() != 0) {
    return $prov->result_array();
  } else {
    return null;
  }
}

function getProvinsi()
{
  $ci  = &get_instance();

  $prov = $ci->db->query("SELECT * FROM ref_propinsi where status='1' order by nama_prop asc");

  if ($prov->num_rows() != 0) {
    return $prov->result_array();
  } else {
    return null;
  }
}

function getProvinsi2()
{
  $ci  = &get_instance();

  $prov = $ci->db->query("SELECT * FROM ref_propinsi order by nama_prop asc");

  if ($prov->num_rows() != 0) {
    return $prov->result_array();
  } else {
    return null;
  }
}

function getKotaByProv($id)
{
  $ci = &get_instance();

  $get = $ci->db->query("SELECT * FROM ref_kabupaten where kode_prop=? and status='1' order by nama_kab ", array($id));

  if ($get->num_rows() != 0) {
    return $get->result_array();
  } else {
    return null;
  }
}

function getKotaByProv2($id)
{
  $ci = &get_instance();

  $get = $ci->db->query("SELECT * FROM ref_kabupaten where kode_prop=? order by nama_kab ", array($id));

  if ($get->num_rows() != 0) {
    return $get->result_array();
  } else {
    return null;
  }
}

function getSelectKabupaten($kode_provinsi = null){
  $data = getKotaByProv2($kode_provinsi);
  $html = '<option value="">Pilih Kabupaten</option>';
  if ($data != null) {
    foreach ($data as $rows) {
      $html .= '<option value="'.$rows['kode_kab'].'">'.ucwords(strtolower($rows['nama_kab'])).'</option>';
    }
  }
  $return = $html;
  return $return;
}

function getKecamatanByKota($id)
{
  $ci = &get_instance();

  $get = $ci->db->query("SELECT * FROM ref_kecamatan where kode_kab=? order by nama_kec", array($id));

  if ($get->num_rows() != 0) {
    return $get->result_array();
  } else {
    return null;
  }
}

function getKecamatanByKota2($id)
{
  $ci = &get_instance();

  $get = $ci->db->query("SELECT * FROM ref_kecamatan where kode_kab=? order by nama_kec", array($id));

  if ($get->num_rows() != 0) {
    return $get->result_array();
  } else {
    return null;
  }
}

function getSelectKecamatan($kode_kabupaten = null){
  $data = getKecamatanByKota2($kode_kabupaten);
  $html = '<option value="">Pilih Kecamatan</option>';
  if ($data != null) {
    foreach ($data as $rows) {
      $html .= '<option value="'.$rows['kode_kec'].'">'.ucwords(strtolower($rows['nama_kec'])).'</option>';
    }
  }
  $return = $html;
  return $return;
}

function getDesaByKec($id)
{
  $ci = &get_instance();

  $get = $ci->db->query("SELECT * FROM ref_desa where kode_kec=? order by nama_desa", array($id));

  if ($get->num_rows() != 0) {
    return $get->result_array();
  } else {
    return null;
  }
}

function setFolder()
{
  $ci = &get_instance();
  $get = $ci->db->query("SELECT id from cpm where id_user=?", array($ci->session->userdata('hk_userId')));
  $row = ($get->num_rows() != 0 ? $get->row_array() : null);
  $return = 'cpm/' . $row['id'];
  return $return;
}

function logVerVal($data, $type)
{
  $ci = &get_instance();
  if ($type == 'verifikasi') {
    $ci->db->insert('hk_log_verifikator', $data);
    if ($ci->db->affected_rows()) {
      return true;
    } else {
      return false;
    }
  } else if ($type == 'validasi') {
    $ci->db->insert('hk_log_validator', $data);
    if ($ci->db->affected_rows()) {
      return true;
    } else {
      return false;
    }
  }
}

function umur($tanggal_lahir)
{
  $birthDate = new DateTime($tanggal_lahir);
  $today = new DateTime("today");
  if ($birthDate > $today) {
    exit("0 tahun 0 bulan 0 hari");
  }
  $y = $today->diff($birthDate)->y;
  $m = $today->diff($birthDate)->m;
  $d = $today->diff($birthDate)->d;
  // return $y." tahun ".$m." bulan ".$d." hari";
  return $y;
}

function setUriClass($uri = "", $type = "")
{
  $ci = &get_instance();
  if ($ci->session->userdata('hk_mainModule') == $uri) {
    if ($type == 'navbar') {
      return 'label bg-info rounded-xl text-white';
    } else {
      return 'text-white';
    }
  }
}

function setArrayResponse($data, $column)
{
  $return = array();

  $decoded = json_decode($data, true);
  if ($decoded != null) {
    $countData = count($decoded);
    for ($i = 0; $i < $countData; $i++) {
      array_push($return, $decoded[$i]['' . $column . '']);
    }
  } else {
    $return = null;
  }
  return $return;
}

function getJadwalPalang($id = "")
{
  $return['success'] = false;
  $return['data'] = '';

  $ci = &get_instance();
  $ci->db->select("*");
  $ci->db->from("jadwal_palang");
  if ($id != "" || $id != null) {
    $ci->db->where('id', $id);
  }
  $ci->db->where('is_active', '1');
  $get = $ci->db->get();
  if ($get->num_rows() != 0) {
    $return['success'] = true;
    $return['data'] = $get->row_array();
  } else {
    $return['success'] = false;
    $return['data'] = null;
  }
  return $return;
}
function activePage($page)
{
    $_this = &get_instance();
    if ($page == $_this->uri->segment(2)) {
        return 'active';
    }
}

function cekParameter($arrayKey = null, $params = null){
  $return['status'] = true;
  $return['message'] = null;
  if(!empty($arrayKey) && !empty($params)){
    for ($i=0; $i < count($arrayKey); $i++) {
      if(!array_key_exists($arrayKey[$i], $params)){
        $return['status'] = false;
        $return['message'] = 'Parameter request harus lengkap, Silahkan lihat API Documentation';
        break;
      }
    }
  }

  return $return;
}

/**
 * fungsi untuk melakukan pencocokan field tabel dengan form
 *
 * @param  mixed $table_name = referensi tabel
 * @param  mixed $data = data yang dicek
 * @return void $return
 */
function filterFieldsOfTable($table_name, $data)
{
  $return = [];
  $ci = &get_instance();

  if ($ci->db->table_exists($table_name)) {
    $get = $ci->db->list_fields($table_name);

    foreach ($get as $key => $rows) {
      if (array_key_exists($rows, $data)) {
        $return[$rows] = $data[$rows];
      }
    }
  } else {
    $return = [];
  }
  return $return;
}
