<?php
function send_wa_file($datasend){
	$userkey = 'ea5c28cfdf6d';
	$passkey = '7ef4275f5b6ee669820cbfb7';
	$image_link = 'https://picsum.photos/id/995/200/300.jpg';
	$caption  = "Selamat ".$datasend['nama']."
Anda telah bergabung menjadi ".$datasend['member_type']. " BG SKIN, 

Username : ".$datasend['username']."
Password : ".$datasend['password']."

Silahkan perbaharui informasi profil anda melalui link ".site_url('login')."

Pesan otomatis dikirim oleh sistem, mohon tidak dibalas.";
	$url = 'https://console.zenziva.net/wareguler/api/sendWA/';
	$curlHandle = curl_init();
	curl_setopt($curlHandle, CURLOPT_URL, $url);
	curl_setopt($curlHandle, CURLOPT_HEADER, 0);
	curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
	curl_setopt($curlHandle, CURLOPT_POST, 1);
	curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
	    'userkey' => $userkey,
	    'passkey' => $passkey,
	    'to' => $datasend['no_hp'],
	    'link' => $image_link,
	    'caption' => $caption
	));
	$results = json_decode(curl_exec($curlHandle), true);
	curl_close($curlHandle);
}


function send_wa($datasend){
	$userkey = 'ea5c28cfdf6d';
	$passkey = '7ef4275f5b6ee669820cbfb7';
	$message = "Selamat ".$datasend['nama']."
Anda telah bergabung menjadi ".$datasend['member_type']. " BG SKIN, 

Username : ".$datasend['username']."
Password : ".$datasend['password']."

Silahkan perbaharui informasi profil anda melalui link ".site_url('login')."

Pesan otomatis dikirim oleh sistem, mohon tidak dibalas.";
	$url = 'https://console.zenziva.net/wareguler/api/sendWA/';
	$curlHandle = curl_init();
	curl_setopt($curlHandle, CURLOPT_URL, $url);
	curl_setopt($curlHandle, CURLOPT_HEADER, 0);
	curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
	curl_setopt($curlHandle, CURLOPT_POST, 1);
	curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
	    'userkey' => $userkey,
	    'passkey' => $passkey,
	    'to' => $datasend['no_hp'],
	    'message' => $message
	));
	$results = json_decode(curl_exec($curlHandle), true);
	curl_close($curlHandle);
}


function send_wa_po($datasend){
	$userkey = 'ea5c28cfdf6d';
	$passkey = '7ef4275f5b6ee669820cbfb7';
	$message = "Selamat pre order atas nama ".$datasend['member_type']." ".$datasend['nama']." (".$datasend['member_id'].")" ."
pada tanggal ".tgl_indo($datasend['tanggal'])." *Telah ".$datasend['status']. "*, 


Silahkan cek link dibawah untuk informasi lebih lengkap: ".site_url('login')."


Pesan otomatis dikirim oleh sistem, mohon tidak dibalas.";
	$url = 'https://console.zenziva.net/wareguler/api/sendWA/';
	$curlHandle = curl_init();
	curl_setopt($curlHandle, CURLOPT_URL, $url);
	curl_setopt($curlHandle, CURLOPT_HEADER, 0);
	curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
	curl_setopt($curlHandle, CURLOPT_POST, 1);
	curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
	    'userkey' => $userkey,
	    'passkey' => $passkey,
	    'to' => $datasend['no_hp'],
	    'message' => $message
	));
	$results = json_decode(curl_exec($curlHandle), true);
	curl_close($curlHandle);
}


function sendEmail($recipient, $subyek, $message) {
	$ci = &get_instance();
	date_default_timezone_set('Asia/Jakarta');
	$ci->config->load('email');
	$sender = $ci->config->item('smtp_user');
	$ci->email->from($sender, 'Admin BG Skin Care Barcode System');
	$ci->email->to($recipient);
	$ci->email->subject($subyek);
	$ci->email->message($message);
	return $ci->email->send();
}