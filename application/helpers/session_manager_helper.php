<?php
if (!function_exists('getLoginSession')) {
    /**
     * getLoginSession
     * function to get all session data
     * t_userId
     * t_alumniId
     * t_url_photo
     * t_nama
     * t_email
     * t_idGroup
     * t_namaGroup
     * t_isActive
     * t_dbUsername
     * @return array
     */
    function getLoginSession()
    {
        $ci =& get_instance();

        $result = $ci->session->all_userdata();
        return $result;
    }

    if (!function_exists('getSessionRoleAccess')) {
        /**
         * getSessionRoleAccess
         * function to get role access
         * @return void
         */
        function getSessionRoleAccess()
        {
            $result = getLoginSession();

            return $result['t_idGroup'];
        }
    }

    if (!function_exists('getSessionID')) {
        function getSessionID()
        {
            $result = getLoginSession();
            return $result['t_userId'];
        }
    }

    if (!function_exists('getMitra')) {
        function getMitra()
        {
            $id = getSessionID();
            $ci =& get_instance();

            $ci->db->select('*');
            $ci->db->from('mitra');
            $ci->db->where('id_user', $id);
            $get = $ci->db->get();

            if ($get->num_rows() != 0) {
                return $get->row_object();
            } else {
                return [];
            }
        }
    }
    if (!function_exists('getMitraID')) {
        function getMitraID()
        {
            $data = getMitra();

            if (!empty($data)) {
                return $data->id_instansi;
            }else{
                return null;
            }
        }
    }

    if (!function_exists('getAlumni')) {
        function getAlumni()
        {
            $id = getSessionID();
            $ci = &get_instance();

            $ci->db->select('*');
            $ci->db->from('alumni');
            $ci->db->where('id_user', $id);
            $get = $ci->db->get();

            if ($get->num_rows() != 0) {
                return $get->row_object();
            } else {
                return [];
            }
        }
    }

    if (!function_exists('getAlumniID')) {
        function getAlumniID()
        {
            $data = getAlumni();

            if (!empty($data)) {
                return $data->id_alumni;
            } else {
                return null;
            }
        }
    }
    if (!function_exists('getAlumniIDPT')) {
        function getAlumniIDPT()
        {
            $data = getAlumni();

            if (!empty($data)) {
                return $data->id_perguruan_tinggi;
            } else {
                return null;
            }
        }
    }


}

?>