<?php

function setPronvisi()
{
    $return = [];
    $ci =& get_instance();
    $get = $ci->db->query("SELECT * FROM ref_provinsi order by kode_provinsi");
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['kode_provinsi']] = $rows['nama_provinsi'];
        }
    }
    return $return;

}
function setKabupaten($kode_provinsi = null)
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_kab_kota');
    if ($kode_provinsi != null) {
        $ci->db->where('kode_provinsi', $kode_provinsi);
    }
    $ci->db->order_by('kode_kab_kota');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['kode_kab_kota']] = $rows['nama_kab_kota'];
        }
    }
    return $return;

}
function setKecamatan($kode_kab_kota = null)
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_kecamatan');
    if ($kode_kab_kota != null) {
        $ci->db->where('kode_kab_kota', $kode_kab_kota);
    }
    $ci->db->order_by('kode_kecamatan');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['kode_kecamatan']] = $rows['nama_kecamatan'];
        }
    }
    return $return;

}

function setJenjangPendidikan()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_perguruan_tinggi');
    $ci->db->order_by('nama_resmi');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_perguruan_tinggi']] = $rows['nama_resmi'];
        }
    }
    return $return;

}

function setFakultas()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_fakultas');
    $ci->db->order_by('kode_fakultas');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_fakultas']] = $rows['nama_fakultas'];
        }
    }
    return $return;

}

function setProdi()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_prodi');
    $ci->db->order_by('nama_prodi');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_prodi']] = $rows['nama_prodi'];
        }
    }
    return $return;

}

function setProdiByFakultas($id_fakultas)
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_prodi');
    if ($id_fakultas != null) {
        $ci->db->where('id_fakultas', $id_fakultas);
    }
    $ci->db->order_by('nama_prodi');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_prodi']] = $rows['nama_prodi'];
        }
    }
    return $return;
}

function setTahun()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_tahun');
    $ci->db->order_by('tahun','DESC');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['tahun']] = $rows['tahun'];
        }
    }
    return $return;
}
function setStatusAlumni()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_status_alumni');
    $ci->db->order_by('id');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id']] = $rows['status_alumni'];
        }
    }
    return $return;

}
function setAgama()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_agama');
    $ci->db->order_by('id_agama');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_agama']] = $rows['nama_agama'];
        }
    }
    return $return;

}
function setMitra($id_user = null)
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('mitra');

    if ($id_user) {
        $ci->db->where('id_user',$id_user);
    }
    $ci->db->order_by('nama');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_instansi']] = $rows['nama'];
        }
    }
    return $return;

}
function setPendidikan()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_pendidikan');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_pendidikan']] = $rows['nama_pendidikan'];
        }
    }
    return $return;

}
?>