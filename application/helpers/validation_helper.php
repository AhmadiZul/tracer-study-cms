<?php
function lessEqual($str)
{
    if ($str >= 0 && $str <= 4.00) {
        return true;
    }

    return false;
}

function whitespace($str)
{
    if (ctype_space($str)) {
        return false;
    } else {
        return true;
    }
}
function notOnlyNumber($str)
{
    if (ctype_digit($str)) {
        return false;
    }
    return true;
}
function onlyAlpha($str)
{
    if (!preg_match('/^[a-zA-Z ]*$/', $str)) {
        return false;
    }

    return true;
}
function validNik($nik)
{
    if (preg_match('/^\d{16,16}$/',$nik)) {
        return true;
    }

    return false;
}
function validDate($date)
{
    $date = DateTime::createFromFormat('Y-m-d', $date);
    $date_errors = DateTime::getLastErrors();
    if ($date_errors['warning_count'] + $date_errors['error_count'] > 0) {
        return false;
    }

    return true;
}
function validEmail($email)
{
    if (preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i',$email)) {
        return true;
    }

    return false;
}

function validUrl($url)
{
    if (preg_match('/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i',$url)) {
        return true;
    }

    return false;
}

function validHp($hp)
{
    if (preg_match('/^(\+62|62|0)8[1-9][0-9]{6,9}$/',$hp)) {
        return true;
    }

    return false;
}
