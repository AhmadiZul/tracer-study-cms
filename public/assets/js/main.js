/**
* Template Name: Anyar - v4.7.1
* Template URL: https://bootstrapmade.com/anyar-free-multipurpose-one-page-bootstrap-theme/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
*/
(function () {
  "use strict";

  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
      if (all) {
        selectEl.forEach(e => e.addEventListener(type, listener))
      } else {
        selectEl.addEventListener(type, listener)
      }
    }
  }

  /**
   * Easy on scroll event listener 
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
   * Navbar links active state on scroll
   */
  let navbarlinks = select('#navbar .scrollto', true)
  const navbarlinksActive = () => {
    let position = window.scrollY + 200
    navbarlinks.forEach(navbarlink => {
      if (!navbarlink.hash) return
      let section = select(navbarlink.hash)
      if (!section) return
      if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
        navbarlink.classList.add('active')
      } else {
        navbarlink.classList.remove('active')
      }
    })
  }
  window.addEventListener('load', navbarlinksActive)
  onscroll(document, navbarlinksActive)

  /**
   * Scrolls to an element with header offset
   */
  const scrollto = (el) => {
    let header = select('#header')
    let offset = header.offsetHeight

    if (!header.classList.contains('fixed-top')) {
      offset += 70
    }

    let elementPos = select(el).offsetTop
    window.scrollTo({
      top: elementPos - offset,
      behavior: 'smooth'
    })
  }

  /**
   * Header fixed top on scroll
   */
  let selectHeader = select('#header')
  let selectTopbar = select('#topbar')
  if (selectHeader) {
    const headerScrolled = () => {
      if (window.scrollY > 100) {
        selectHeader.classList.add('header-scrolled')
        if (selectTopbar) {
          selectTopbar.classList.add('topbar-scrolled')
        }
      } else {
        selectHeader.classList.remove('header-scrolled')
        if (selectTopbar) {
          selectTopbar.classList.remove('topbar-scrolled')
        }
      }
    }
    window.addEventListener('load', headerScrolled)
    onscroll(document, headerScrolled)
  }

  /**
   * Back to top button
   */
  let backtotop = select('.back-to-top')
  if (backtotop) {
    const toggleBacktotop = () => {
      if (window.scrollY > 100) {
        backtotop.classList.add('active')
      } else {
        backtotop.classList.remove('active')
      }
    }
    window.addEventListener('load', toggleBacktotop)
    onscroll(document, toggleBacktotop)
  }

  /**
   * Mobile nav toggle
   */
  on('click', '.mobile-nav-toggle', function (e) {
    select('#navbar').classList.toggle('navbar-mobile')
    this.classList.toggle('bi-list')
    this.classList.toggle('bi-x')
  })

  /**
   * Mobile nav dropdowns activate
   */
  on('click', '.navbar .dropdown > a', function (e) {
    if (select('#navbar').classList.contains('navbar-mobile')) {
      e.preventDefault()
      this.nextElementSibling.classList.toggle('dropdown-active')
    }
  }, true)

  /**
   * Scrool with ofset on links with a class name .scrollto
   */
  on('click', '.scrollto', function (e) {
    if (select(this.hash)) {
      e.preventDefault()

      let navbar = select('#navbar')
      if (navbar.classList.contains('navbar-mobile')) {
        navbar.classList.remove('navbar-mobile')
        let navbarToggle = select('.mobile-nav-toggle')
        navbarToggle.classList.toggle('bi-list')
        navbarToggle.classList.toggle('bi-x')
      }
      scrollto(this.hash)
    }
  }, true)

  /**
   * Scroll with ofset on page load with hash links in the url
   */
  window.addEventListener('load', () => {
    if (window.location.hash) {
      if (select(window.location.hash)) {
        scrollto(window.location.hash)
      }
    }
  });

  /**
   * Preloader
   */
  let preloader = select('#preloader');
  if (preloader) {
    window.addEventListener('load', () => {
      preloader.remove()
    });
  }

  /**
   * Clients Slider
   */
  new Swiper('.clients-slider', {
    speed: 400,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 40
      },
      480: {
        slidesPerView: 3,
        spaceBetween: 60
      },
      640: {
        slidesPerView: 4,
        spaceBetween: 80
      },
      992: {
        slidesPerView: 6,
        spaceBetween: 120
      }
    }
  });

  /**
   * Porfolio isotope and filter
   */
  window.addEventListener('load', () => {
    let portfolioContainer = select('.portfolio-container');
    if (portfolioContainer) {
      let portfolioIsotope = new Isotope(portfolioContainer, {
        itemSelector: '.portfolio-item',
        layoutMode: 'fitRows'
      });

      let portfolioFilters = select('#portfolio-flters li', true);

      on('click', '#portfolio-flters li', function (e) {
        e.preventDefault();
        portfolioFilters.forEach(function (el) {
          el.classList.remove('filter-active');
        });
        this.classList.add('filter-active');

        portfolioIsotope.arrange({
          filter: this.getAttribute('data-filter')
        });
        portfolioIsotope.on('arrangeComplete', function () {
          AOS.refresh()
        });
      }, true);
    }

  });

  /**
   * Initiate portfolio lightbox 
   */
  const portfolioLightbox = GLightbox({
    selector: '.portfolio-lightbox'
  });

  /**
   * Initiate glightbox 
   */
  const gLightbox = GLightbox({
    selector: '.glightbox'
  });

  /**
   * Portfolio details slider
   */
  new Swiper('.portfolio-details-slider', {
    speed: 400,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    }
  });

  /**
   * Animation on scroll
   */
  window.addEventListener('load', () => {
    AOS.init({
      duration: 1000,
      easing: 'ease-in-out',
      once: true,
      mirror: false
    })
  });

  /**
 * select kabupaten/kota
 */
  $('[name="kode_provinsi"]').change(function (e) {
    var val = e.target.value;
    $.ajax({
      url: site_url + 'getKabKota',
      type: "GET",
      data: {
        kode_provinsi: val
      },
      dataType: "JSON",
      success: function (data) {
        buatKabKot(data);
        $('[name="kode_kabupaten"]').select2({
          placeholder: '- Pilih Kabupaten/Kota -',
        });
      },
      error: function (xx) {
        alert(xx)
      }
    });
  });

  function buatKabKot(data) {
    let optionLoop = '<option value>Pilih Kabupaten / Kota</option>';
    Object.keys(data).forEach(key => {
      optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
    });
    $('[name="kode_kecamatan"]').children().remove();
    $('[name="kode_kecamatan"]').select2({
      placeholder: '- Pilih Kecamatan -',
    });
    if ($('[name="kode_kabupaten"]')) {
      $('[name="kode_kabupaten"]').children().remove();
    }
    $('[name="kode_kabupaten"]').append(optionLoop);
  }

  /**
  * select kecamatan
  */
  $('[name="kode_kabupaten"]').change(function (e) {
    var val = e.target.value;
    $.ajax({
      url: site_url + 'getKecamatan',
      type: "GET",
      data: {
        kode_kab_kota: val
      },
      dataType: "JSON",
      success: function (data) {
        buatKecamatan(data);
        $('[name="kode_kecamatan"]').select2({
          placeholder: '- Pilih Kecamatan -',
        });
      },
      error: function (xx) {
        alert(xx)
      }
    });
  });

  function buatKecamatan(data) {
    let optionLoop = '<option value>Pilih Kecamatan</option>';
    Object.keys(data).forEach(key => {
      optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
    });
    if ($('[name="kode_kecamatan"]')) {
      $('[name="kode_kecamatan"]').children().remove();
    }
    $('[name="kode_kecamatan"]').append(optionLoop);
  }

  /**
   * select kabupaten/kota instansi
   */
  $('[name="instansi_kode_prov"]').change(function (e) {
    var val = e.target.value;
    $.ajax({
      url: site_url + 'getKabKota',
      type: "GET",
      data: {
        kode_provinsi: val
      },
      dataType: "JSON",
      success: function (data) {
        buatKabKotInstansi(data);
        $('[name="instansi_kode_kab"]').select2({
          placeholder: '- Pilih Kabupaten/Kota -',
        });
      },
      error: function (xx) {
        alert(xx)
      }
    });
  });

  function buatKabKotInstansi(data) {
    let optionLoop = '<option value>Pilih Kabupaten / Kota</option>';
    Object.keys(data).forEach(key => {
      optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
    });
    $('[name="instansi_kode_kec"]').children().remove();
    $('[name="instansi_kode_kec"]').select2({
      placeholder: '- Pilih Kecamatan -',
    });
    if ($('[name="instansi_kode_kab"]')) {
      $('[name="instansi_kode_kab"]').children().remove();
    }
    $('[name="instansi_kode_kab"]').append(optionLoop);
  }

  /**
    * select kecamatan instansi
    */
  $('[name="instansi_kode_kab"]').change(function (e) {
    var val = e.target.value;
    $.ajax({
      url: site_url + 'getKecamatan',
      type: "GET",
      data: {
        kode_kab_kota: val
      },
      dataType: "JSON",
      success: function (data) {
        buatKecamatanInstansi(data);
        $('[name="instansi_kode_kec"]').select2({
          placeholder: '- Pilih Kecamatan -',
        });
      },
      error: function (xx) {
        alert(xx)
      }
    });
  });

  function buatKecamatanInstansi(data) {
    let optionLoop = '<option value>Pilih Kecamatan</option>';
    Object.keys(data).forEach(key => {
      optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
    });
    if ($('[name="instansi_kode_kec"]')) {
      $('[name="instansi_kode_kec"]').children().remove();
    }
    $('[name="instansi_kode_kec"]').append(optionLoop);
  }

  /**
 * select status alumni
 */
  let year_now = new Date();
  let month = (year_now.getMonth() > 9 ? '' : '0') + (year_now.getMonth() + 1);
  let day = (year_now.getDate() > 9 ? '' : '0') + year_now.getDate();
  let start_date = year_now.getFullYear() + '-' + month + '-' + day;

  $('[name="status_alumni"]').change(function (e) {
    var val = e.target.value;
    if (val == 1) {
      // remove or add d-none
      $('#div-instansi').attr('class', 'form-group');
      $('#div-tgl-mulai').attr('class', 'form-group');
      $('#div-jabatan').attr('class', 'form-group');
      $('#div-prodi').attr('class', 'form-group d-none');
      $('#div-sektor').attr('class', 'form-group');
      $('#div-instansi-provinsi').attr('class', 'form-group');
      $('#div-instansi-kab-kota').attr('class', 'form-group');
      $('#div-instansi-kecamatan').attr('class', 'form-group');
      $('#div-alamat').attr('class', 'form-group');

      // change label
      $('#instansi').html('Nama Instansi <b class="text-danger">*</b>');

      // change placeholder
      $('[name="nama_instansi"]').attr('placeholder', 'Masukkan nama instansi anda');

      // remove or add required
      $('[name="nama_instansi"]').attr('required', true);
      $('[name="tgl_mulai"]').attr('required', true);
      $('[name="jabatan"]').attr('required', true);
      $('[name="prodi"]').attr('required', false);
      $('[name="sektor"]').attr('required', true);
      $('[name="instansi_kode_prov"]').attr('required', true);
      $('[name="instansi_kode_kab"]').attr('required', true);
      $('[name="instansi_kode_kec"]').attr('required', true);
      $('[name="instansi_alamat"]').attr('required', true);

      // remove value
      $('[name="nama_instansi"]').val('');
      $('[name="tgl_mulai"]').val(start_date);
      $('[name="jabatan"]').val('');
      $('[name="prodi"]').val('');
      $('[name="sektor"]').prop('checked', false);
      $('[name="instansi_kode_prov"]').val('').trigger('change');
      $('[name="instansi_kode_kab"]').val('').trigger('change');
      $('[name="instansi_kode_kec"]').val('').trigger('change');
      $('[name="instansi_alamat"]').val('');
    } else if (val == 3) {
      // remove or add d-none
      $('#div-instansi').attr('class', 'form-group');
      $('#div-tgl-mulai').attr('class', 'form-group');
      $('#div-jabatan').attr('class', 'form-group d-none');
      $('#div-prodi').attr('class', 'form-group d-none');
      $('#div-sektor').attr('class', 'form-group');
      $('#div-instansi-provinsi').attr('class', 'form-group');
      $('#div-instansi-kab-kota').attr('class', 'form-group');
      $('#div-instansi-kecamatan').attr('class', 'form-group');
      $('#div-alamat').attr('class', 'form-group');

      // change label
      $('#instansi').html('Nama Usaha <b class="text-danger">*</b>');

      // change placeholder
      $('[name="nama_instansi"]').attr('placeholder', 'Masukkan nama usaha anda');

      // remove or add required
      $('[name="nama_instansi"]').attr('required', true);
      $('[name="tgl_mulai"]').attr('required', true);
      $('[name="jabatan"]').attr('required', false);
      $('[name="prodi"]').attr('required', false);
      $('[name="sektor"]').attr('required', true);
      $('[name="instansi_kode_prov"]').attr('required', true);
      $('[name="instansi_kode_kab"]').attr('required', true);
      $('[name="instansi_kode_kec"]').attr('required', true);
      $('[name="instansi_alamat"]').attr('required', true);

      // remove value
      $('[name="nama_instansi"]').val('');
      $('[name="tgl_mulai"]').val(start_date);
      $('[name="jabatan"]').val('');
      $('[name="prodi"]').val('');
      $('[name="sektor"]').prop('checked', false);
      $('[name="instansi_kode_prov"]').val('').trigger('change');
      $('[name="instansi_kode_kab"]').val('').trigger('change');
      $('[name="instansi_kode_kec"]').val('').trigger('change');
      $('[name="instansi_alamat"]').val('');
    } else if (val == 4) {
      // remove or add d-none
      $('#div-instansi').attr('class', 'form-group');
      $('#div-tgl-mulai').attr('class', 'form-group');
      $('#div-jabatan').attr('class', 'form-group d-none');
      $('#div-prodi').attr('class', 'form-group');
      $('#div-sektor').attr('class', 'form-group');
      $('#div-instansi-provinsi').attr('class', 'form-group');
      $('#div-instansi-kab-kota').attr('class', 'form-group');
      $('#div-instansi-kecamatan').attr('class', 'form-group');
      $('#div-alamat').attr('class', 'form-group d-none');

      // change label
      $('#instansi').html('Nama Perguruan Tinggi <b class="text-danger">*</b>');

      // change placeholder
      $('[name="nama_instansi"]').attr('placeholder', 'Masukkan nama perguruan tinggi anda');

      // remove or add required
      $('[name="nama_instansi"]').attr('required', true);
      $('[name="tgl_mulai"]').attr('required', true);
      $('[name="jabatan"]').attr('required', false);
      $('[name="prodi"]').attr('required', true);
      $('[name="sektor"]').attr('required', true);
      $('[name="instansi_kode_prov"]').attr('required', true);
      $('[name="instansi_kode_kab"]').attr('required', true);
      $('[name="instansi_kode_kec"]').attr('required', true);
      $('[name="instansi_alamat"]').attr('required', false);

      // remove value
      $('[name="nama_instansi"]').val('');
      $('[name="tgl_mulai"]').val(start_date);
      $('[name="jabatan"]').val('');
      $('[name="prodi"]').val('');
      $('[name="sektor"]').prop('checked', false);
      $('[name="instansi_kode_prov"]').val('').trigger('change');
      $('[name="instansi_kode_kab"]').val('').trigger('change');
      $('[name="instansi_kode_kec"]').val('').trigger('change');
      $('[name="instansi_alamat"]').val('');
    } else {
      // remove or add d-none
      $('#div-instansi').attr('class', 'form-group d-none');
      $('#div-tgl-mulai').attr('class', 'form-group d-none');
      $('#div-jabatan').attr('class', 'form-group d-none');
      $('#div-prodi').attr('class', 'form-group d-none');
      $('#div-sektor').attr('class', 'form-group d-none');
      $('#div-instansi-provinsi').attr('class', 'form-group d-none');
      $('#div-instansi-kab-kota').attr('class', 'form-group d-none');
      $('#div-instansi-kecamatan').attr('class', 'form-group d-none');
      $('#div-alamat').attr('class', 'form-group d-none');

      // remove or add required
      $('[name="nama_instansi"]').attr('required', false);
      $('[name="tgl_mulai"]').attr('required', false);
      $('[name="jabatan"]').attr('required', false);
      $('[name="prodi"]').attr('required', false);
      $('[name="sektor"]').attr('required', false);
      $('[name="instansi_kode_prov"]').attr('required', false);
      $('[name="instansi_kode_kab"]').attr('required', false);
      $('[name="instansi_kode_kec"]').attr('required', false);
      $('[name="instansi_alamat"]').attr('required', false);

      // remove value
      $('[name="nama_instansi"]').val('');
      $('[name="tgl_mulai"]').val(start_date);
      $('[name="jabatan"]').val('');
      $('[name="prodi"]').val('');
      $('[name="sektor"]').prop('checked', false);
      $('[name="instansi_kode_prov"]').val('').trigger('change');
      $('[name="instansi_kode_kab"]').val('').trigger('change');
      $('[name="instansi_kode_kec"]').val('').trigger('change');
      $('[name="instansi_alamat"]').val('');
    } 
  });

  /**
   * only number
   */
  $('.number-only').keyup(function (e) {
    if (/\D/g.test(this.value)) {
      // Filter non-digits from input value.
      this.value = this.value.replace(/\D/g, '');
    }
  });

  /**
   * only alpha
   */
  $('.alpha-only').bind('keydown', function (e) {
    if (e.altKey) {
      e.preventDefault();
    } else {
      var key = e.keyCode;
      if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
        e.preventDefault();
      }
    }
  });

  /**
   * format ipk 0.00 maks 4.00
   */
  $('.ipk').mask('0.00', {
    reverse: true,
    min: 0,
    max: 4
  });

})()