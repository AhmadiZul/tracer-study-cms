<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="form_edit">
                    <div class="form-group row">
                        <label for="key" class="col-md-3 col-form-label">Key <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="key" name="key" value="<?= $sistem->key; ?>" maxlength="60" disabled>
                            <input type="hidden" class="form-control" id="key" name="key" value="<?= $sistem->key; ?>" maxlength="60">
                        </div>
                    </div>
                    <?php if ($sistem->id == '1') : ?>
                        <div class="form-group row">
                            <input type="hidden" class="form-control" name="id" id="id" value="<?= $sistem->id ?>">
                            <input type="hidden" class="form-control" name="is_berkas" id="is_berkas" value="0">
                            <label for="url_file" class="col-md-3 col-form-label">Logo <b class="text-danger">*</b></label>
                            <div id="ganti-berkas-logo-perguruan" class="col-md-9">
                                <img src="<?= base_url() . $sistem->url_file ?>" alt="your image" style="width: 150px;" /><br>
                                <label class="form-label"><input name="new_upload_logo-perguruan" type="checkbox" onclick="uploadFoto('logo-perguruan','url_photo')" value="1"> Ganti Berkas</label>
                            </div>
                            <div class="col-md-3"></div>
                            <div id="new-upload-logo-perguruan" class="col-md-9">
                                <label for="input_file" class="btn btn-primary">Pilih Berkas</label>
                                <input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo-perguruan')">
                                <span class="file-info-logo-perguruan text-muted">Tidak ada berkas yang dipilih</span>
                                <div id="preview-logo-perguruan">

                                </div>
                                <div class="hint-block text-muted mt-3">
                                    <small>
                                        Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                        Ukuran file maksimal: <strong>2 MB</strong>
                                    </small>
                                </div>
                                <!-- <div class="invalid-feedback" for="flyer"></div> -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="<?php echo base_url('sistem_setting/index/') ?>" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
                        </div>
                    <?php endif; ?>
                    <?php if ($sistem->id == '2') : ?>
                        <div class="form-group row">
                            <input type="hidden" class="form-control" name="id" id="id" value="<?= $sistem->id ?>">
                            <input type="hidden" class="form-control" name="is_berkas" id="is_berkas" value="0">
                            <label for="url_file" class="col-md-3 col-form-label">Logo <b class="text-danger">*</b></label>
                            <div id="ganti-berkas-logo-perguruan" class="col-md-9">
                                <img src="<?= base_url() . $sistem->url_file ?>" alt="your image" style="width: 150px;" /><br>
                                <label class="form-label"><input name="new_upload_logo-perguruan" type="checkbox" onclick="uploadFoto('logo-perguruan','url_photo')" value="1"> Ganti Berkas</label>
                            </div>
                            <div class="col-md-3"></div>
                            <div id="new-upload-logo-perguruan" class="col-md-9">
                                <label for="input_file" class="btn btn-primary">Pilih Berkas</label>
                                <input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo-perguruan')">
                                <span class="file-info-logo-perguruan text-muted">Tidak ada berkas yang dipilih</span>
                                <div id="preview-logo-perguruan">

                                </div>
                                <div class="hint-block text-muted mt-3">
                                    <small>
                                        Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                        Ukuran file maksimal: <strong>2 MB</strong>
                                    </small>
                                </div>
                                <!-- <div class="invalid-feedback" for="flyer"></div> -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="<?php echo base_url('sistem_setting/index/') ?>" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
                        </div>
                    <?php endif; ?>
                    <?php if ($sistem->id == '3') : ?>
                        <div class="form-group row">
                            <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <textarea class="form-control alpha-only" id="deskripsi" name="deskripsi" maxlength="250"><?= $sistem->deskripsi; ?></textarea>
                                <div class="invalid-feedback" for="deskripsi"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="url_link" class="col-md-3 col-form-label">url link <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="url_link" name="url_link" value="<?= $sistem->url_link; ?>" maxlength="60">
                                <small class="text-info">Contoh : https://www.youtube.com/embed/53IHdiCabSc</small>
                                <div class="invalid-feedback" for="url_link"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="waktu_selesai" class="col-md-3 col-form-label">Title <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="title" name="title" value="<?= $sistem->title; ?>" maxlength="60">
                                <div class="invalid-feedback" for="title"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="<?php echo base_url('sistem_setting/index/') ?>" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
                        </div>
                    <?php endif; ?>
                    <?php if ($sistem->id == '4') : ?>
                        <div class="form-group row">
                            <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <textarea class="form-control alpha-only" id="deskripsi" name="deskripsi" maxlength="250"><?= $sistem->deskripsi; ?></textarea>
                                <div class="invalid-feedback" for="deskripsi"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="waktu_selesai" class="col-md-3 col-form-label">Title <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="title" name="title" value="<?= $sistem->title; ?>" maxlength="60">
                                <div class="invalid-feedback" for="title"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="<?php echo base_url('sistem_setting/index/') ?>" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
                        </div>
                    <?php endif; ?>
                    <?php if ($sistem->id == '6') : ?>
                        <div class="form-group row">
                            <label for="waktu_selesai" class="col-md-3 col-form-label">Title <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="title" name="title" value="<?= $sistem->title; ?>" maxlength="60">
                                <div class="invalid-feedback" for="title"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="waktu_selesai" class="col-md-3 col-form-label">Sub Title <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <textarea class="form-control alpha-only" id="sub_title" name="sub_title" maxlength="250"><?= $sistem->sub_title; ?></textarea>
                                <div class="invalid-feedback" for="sub_title"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="<?php echo base_url('sistem_setting/index/') ?>" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
                        </div>
                    <?php endif; ?>
                    <?php if ($sistem->id == '7') : ?>
                        <div class="form-group row">
                            <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <textarea class="form-control alpha-only" id="deskripsi" name="deskripsi" maxlength="250"><?= $sistem->deskripsi; ?></textarea>
                                <div class="invalid-feedback" for="deskripsi"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="waktu_selesai" class="col-md-3 col-form-label">Title <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="title" name="title" value="<?= $sistem->title; ?>" maxlength="60">
                                <div class="invalid-feedback" for="title"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="<?php echo base_url('sistem_setting/index/') ?>" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
                        </div>
                    <?php endif; ?>
                    <?php if ($sistem->id == '8') : ?>
                        <div class="form-group row">
                            <label for="waktu_selesai" class="col-md-3 col-form-label">Sub Title <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="sub_title" name="sub_title" value="<?= $sistem->sub_title; ?>" maxlength="60">
                                <div class="invalid-feedback" for="sub_title"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="<?php echo base_url('sistem_setting/index/') ?>" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
                        </div>
                    <?php endif; ?>
                </form>
                <form id="form_sosmed">
                    <?php if ($sistem->id == '5') : ?>
                        <input type="hidden" class="form-control" id="key" name="key" value="<?= $sistem->key; ?>">
                        <div class="form-group row">
                            <label for="waktu_selesai" class="col-md-3 col-form-label">Instagram <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control instagram" id="instagram" name="instagram" value="<?php echo $sistem->instagram ?>" maxlength="60">
                                <div class="invalid-feedback" for="instagram"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="waktu_selesai" class="col-md-3 col-form-label">Twitter <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="twitter" name="twitter" value="<?php echo $sistem->twitter ?>" maxlength="60">
                                <div class="invalid-feedback" for="twitter"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="waktu_selesai" class="col-md-3 col-form-label">Youtube <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="youtube" name="youtube" value="<?php echo $sistem->youtube ?>" maxlength="60">
                                <div class="invalid-feedback" for="youtube"></div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="<?php echo base_url('sistem_setting/index/') ?>" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
                        </div>
                    <?php endif; ?>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    ClassicEditor
        .create(document.querySelector('#deskripsi'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });

    /* ClassicEditor
        .create(document.querySelector('#sub_title'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        }); */
</script>

<script>
    $('#form_edit').on('submit', function(e) {
        e.preventDefault();

        $('#form_edit').find('input, select,textarea').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editSistem',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    title: data.message.title,
                    text: data.message.body,
                    icon: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#form_sosmed').on('submit', function(e) {
        e.preventDefault();

        $('#form_sosmed').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/update_sosmed',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    title: data.message.title,
                    text: data.message.body,
                    icon: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $(".isian_instagram").hide(200);
    $(".isian_twitter").hide(200);
    $(".isian_youtube").hide(200);

    function addInstagram() {
        var x = document.getElementById("add_instagram").checked;
        console.log(x);
        var isian = ".isian_instagram";
        if (x) {
            $(isian).show(300);
        } else {
            $(isian).hide(200);
        }
    }

    function addTwitter() {
        var x = document.getElementById("add_twitter").checked;
        console.log(x);
        var isian = ".isian_twitter";
        if (x) {
            $(isian).show(300);
        } else {
            $(isian).hide(200);
        }
    }

    function addYoutube() {
        var x = document.getElementById("add_youtube").checked;
        console.log(x);
        var isian = ".isian_youtube";
        if (x) {
            $(isian).show(300);
        } else {
            $(isian).hide(200);
        }
    }
</script>
<?php $this->load->view("template/template_scripts") ?>