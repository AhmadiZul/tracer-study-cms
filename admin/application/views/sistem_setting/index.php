<div class="row">
    <div class="sistem">
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card" style="border-radius:20px;">
            <div id="bg-setting">
                <img src="<?php echo base_url() . 'public/assets/img/sidebar/setting_background.png' ?>" alt="..." style="border-radius:20px;" width="100%">
            </div>
            <div class="card-header">
                <ul class="nav navbar-tabs">
                    <li class="nav-item nav-link active"><a data-toggle="tab" href="#home" class="text-tema"><strong>Penganturan Umum</strong></a></li>
                    <li class="nav-item nav-link"><a data-toggle="tab" href="#menu1" class="text-tema"><strong>Banner</strong></a></li>
                    <li class="nav-item nav-link"><a data-toggle="tab" href="#menu2" class="text-tema"><strong>Video Perkenalan</strong></a></li>
                    <li class="nav-item nav-link"><a data-toggle="tab" href="#menu3" class="text-tema"><strong>Sambutan Rektor</strong></a></li>
                    <li class="nav-item nav-link"><a data-toggle="tab" href="#menu4" class="text-tema"><strong>Official Akun</strong></a></li>
                    <li class="nav-item nav-link"><a data-toggle="tab" href="#menu5" class="text-tema"><strong>Header</strong></a></li>
                    <li class="nav-item nav-link"><a data-toggle="tab" href="#menu6" class="text-tema"><strong>Footer</strong></a></li>
                    <li class="nav-item nav-link"><a data-toggle="tab" href="#menu7" class="text-tema"><strong>Tema</strong></a></li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in show active">
                        <form id="form_utama">
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Tab Title</h5>
                            <div class="form-group row">
                                <div class="col-md-5">
                                    <img src="<?php echo base_url() . 'public/assets/img/sidebar/image114.png' ?>" alt="...">
                                </div>
                                <div class="col-md-7">
                                    <input type="hidden" class="form-control" name="id" id="id" value="<?= $logo_title->id ?>">
                                    <input type="hidden" class="form-control" name="key" id="key" value="<?= $logo_title->key ?>">
                                    <div class="col-md-2">
                                        <?php if (isset($logo_title->url_file)) { ?>
                                            <input type="hidden" name="oldLogo" value="<?= $logo_title->url_file ?>">
                                            <div id="ganti-berkas-logo-perguruan" class="col-md-3">
                                                <div id="preview-logo-perguruan" style="width:20px;">
                                                    <img src="<?php echo base_url() . $logo_title->url_file ?>" height="100%" alt="..." class="image-title">
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-9">
                                        <label for="input_file" class="btn btn-primary text-white" style="margin-top:20px;">Pilih Berkas</label>
                                        <input type="file" accept=".png, .jpg, .jpeg " name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo-perguruan')">
                                        <span class="file-info-logo-perguruan text-muted">Tidak ada berkas yang dipilih</span>

                                        <div class="hint-block text-muted mt-3">
                                            <small>
                                                Ukuran dimensi yang direkomendasikan: <strong>520 x 520 px</strong><br>
                                                Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                                Ukuran file maksimal: <strong>2 MB</strong>
                                            </small>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
                            </div>
                        </form>
                        <hr class="bg-dark">
                        <form id="form_logo_utama">
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Logo Universitas</h5>
                            <div class="form-group row">
                                <input type="hidden" class="form-control" id="id" name="id" value="<?= $logo_utama->id; ?>" maxlength="60">
                                <input type="hidden" class="form-control" id="key" name="key" value="<?= $logo_utama->key; ?>" maxlength="60">
                                <div class="col-md-3">
                                    <?php if (isset($logo_utama->url_file)) { ?>
                                        <input type="hidden" name="oldLogo" value="<?= $logo_utama->url_file ?>">
                                        <div id="ganti-berkas-logo-perguruan" class="col-md-4">
                                            <div id="preview-logo" style="width:30px;">
                                                <img src="<?php echo base_url() . $logo_utama->url_file ?>" alt="..." width="100%" class="image-logo">
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-8">
                                    <label for="input" class="btn btn-primary text-white">Pilih Berkas</label>
                                    <input type="file" accept=".png, .jpg, .jpeg " name="url_photo" id="input" class="form-control custom-file" onchange="cekFile(this, 'logo')">
                                    <span class="file-info-logo text-muted">Tidak ada berkas yang dipilih</span>

                                    <div class="hint-block text-muted mt-3">
                                        <small>
                                            Ukuran dimensi yang direkomendasikan: <strong>1920 x 900 px</strong><br>
                                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                            Ukuran file maksimal: <strong>2 MB</strong>
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
                            </div>
                        </form>
                    </div>
                    <div id="menu1" class="tab-pane fade show">
                        <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Background Banner</h5>
                        <div class="form-group row">
                            <div class="col-md-9">
                                <img src="<?php echo base_url() . 'public/assets/img/sidebar/contoh_bg.png' ?>" alt="..." style="width: 100%;" class="image-bg-besar">
                            </div>
                            <div class="col-md-3">
                                <img src="<?php echo base_url() . $background_1->url_file ?>" alt="..." width="100%" class="image-bg">
                                <br>
                                <img src="<?php echo base_url() . $background_2->url_file ?>" alt="..." width="100%" class="image-bg">
                                <br>
                                <img src="<?php echo base_url() . $background_3->url_file ?>" alt="..." width="100%" class="image-bg">
                            </div>
                        </div>
                        <form id="background_1">
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Background 1</h5>
                            <div class="form-group row">
                                <input type="hidden" class="form-control" id="id" name="id" value="<?= $background_1->id; ?>" maxlength="60">
                                <input type="hidden" class="form-control" id="key" name="key" value="<?= $background_1->key; ?>" maxlength="60">
                                <div class="col-md-4">
                                    <div id="ganti-berkas-banner1" class="col-md-9">
                                        <div id="preview-banner1">
                                            <input type="hidden" name="oldLogo" value="<?= $background_1->url_file ?>">
                                            <img src="<?php echo base_url() . $background_1->url_file ?>" alt="..." width="100%" class="image-previewer">
                                        </div><br>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <label for="input_banner1" class="btn btn-primary text-white">Pilih Gambar</label>
                                    <input type="file" name="url_photo" id="input_banner1" accept=".png, .jpg, .jpeg " class="form-control custom-file" onchange="cekFile(this, 'banner1')">
                                    <span class="file-info-banner1 text-muted">Tidak ada berkas yang dipilih</span>
                                    <div class="hint-block text-muted mt-3">
                                        <small>
                                            Ukuran dimensi yang direkomendasikan: <strong>1440 x 700 px</strong><br>
                                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                            Ukuran file maksimal: <strong>2 MB</strong>
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
                            </div>
                        </form>
                        <hr class="bg-dark">
                        <form id="background_2">
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Background 2</h5>
                            <div class="form-group row">
                                <input type="hidden" class="form-control" id="id" name="id" value="<?= $background_2->id; ?>" maxlength="60">
                                <input type="hidden" class="form-control" id="key" name="key" value="<?= $background_2->key; ?>" maxlength="60">
                                <div class="col-md-4">
                                    <div id="ganti-berkas-banner2" class="col-md-9">
                                        <div id="preview-banner2">
                                            <input type="hidden" name="oldLogo" value="<?= $background_2->url_file ?>">
                                            <img src="<?php echo base_url() . $background_2->url_file ?>" alt="..." width="100%" class="image-previewer">
                                        </div><br>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <label for="input_banner2" class="btn btn-primary text-white">Pilih Gambar</label>
                                    <input type="file" name="url_photo" id="input_banner2" accept=".png, .jpg, .jpeg " class="form-control custom-file" onchange="cekFile(this, 'banner2')">
                                    <span class="file-info-banner2 text-muted">Tidak ada berkas yang dipilih</span>
                                    <div class="hint-block text-muted mt-3">
                                        <small>
                                            Ukuran dimensi yang direkomendasikan: <strong>1440 x 700 px</strong><br>
                                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                            Ukuran file maksimal: <strong>2 MB</strong>
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
                            </div>
                        </form>
                        <hr class="bg-dark">
                        <form id="background_3">
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Background 3</h5>
                            <div class="form-group row">
                                <input type="hidden" class="form-control" id="id" name="id" value="<?= $background_3->id; ?>" maxlength="60">
                                <input type="hidden" class="form-control" id="key" name="key" value="<?= $background_3->key; ?>" maxlength="60">
                                <div class="col-md-4">
                                    <div id="ganti-berkas-banner3" class="col-md-9">
                                        <div id="preview-banner3" class="col-md-9">
                                            <input type="hidden" name="oldLogo" value="<?= $background_3->url_file ?>">
                                            <img src="<?php echo base_url() . $background_3->url_file ?>" alt="..." width="100%" class="image-previewer">
                                        </div><br>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <label for="input_banner3" class="btn btn-primary text-white">Pilih Gambar</label>
                                    <input type="file" name="url_photo" id="input_banner3" accept=".png, .jpg, .jpeg " class="form-control custom-file" onchange="cekFile(this, 'banner3')">
                                    <span class="file-info-banner3 text-muted">Tidak ada berkas yang dipilih</span>
                                    <div class="hint-block text-muted mt-3">
                                        <small>
                                            Ukuran dimensi yang direkomendasikan: <strong>1440 x 700 px</strong><br>
                                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                            Ukuran file maksimal: <strong>2 MB</strong>
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
                            </div>
                        </form>
                        <hr class="bg-dark">
                        <form id="form_banner">
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Banner</h5>
                            <input type="hidden" class="form-control" id="key" name="key" value="<?= $banner->key; ?>" maxlength="60">
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Title <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="title" name="title" value="<?= $banner->title; ?>" maxlength="61">
                                    <div class="invalid-feedback" for="title"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Warna Title&nbsp;<b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="warna_1" value="<?= $banner->warna_1; ?>" data-jscolor="{}" , onInput="update(this.jscolor)" maxlength="7">
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sub_title" class="col-md-3 col-form-label">Universitas <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="sub_title" name="sub_title" value="<?= $banner->sub_title; ?>" maxlength="60">
                                    <div class="invalid-feedback" for="sub_title"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Warna Universitas&nbsp;<b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="warna_2" value="<?= $banner->warna_2; ?>" data-jscolor="{}" , onInput="update(this.jscolor)" maxlength="7">
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="" name="deskripsi" maxlength="250" style="height: 150%;"><?= $banner->deskripsi; ?></textarea>
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni" style="margin-top:50px;">Simpan Perubahan</button>
                            </div>
                        </form>
                    </div>
                    <div id="menu2" class="tab-pane fade show">
                        <form id="form_video">
                            <input type="hidden" class="form-control" id="key" name="key" value="<?= $video_perkenalan->key; ?>" maxlength="60">
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Video Perkenalan</h5>
                            <div class="form-group row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="my-auto">
                                        <iframe width="100%" height="315" src="<?php echo $video_perkenalan->url_link ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" class="video-pengenalan" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="url_link" class="col-md-3 col-form-label">url link <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="url_link" name="url_link" value="<?php echo $video_perkenalan->url_link ?>" maxlength="100">
                                    <button class="btn" id="tutorVideo" data-toggle="modal" data-target="#modalVideo" type="button">
                                        <i class="fa-solid fa-circle-info"></i>
                                    </button>
                                    <small class="text-info">Contoh : https://www.youtube.com/embed/53IHdiCabSc</small>
                                    <div class="invalid-feedback" for="url_link"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Title <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="title" name="title" value="<?= $video_perkenalan->title; ?>" maxlength="60">
                                    <div class="invalid-feedback" for="title"></div>
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <input type="hidden" class="form-control" id="id" name="id" value="<?= $video_perkenalan->id; ?>" maxlength="60">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Background <b class="text-danger">*</b></label>
                                <?php if (isset($video_perkenalan->url_file)) { ?>
                                <div class="col-md-4">
                                    <div id="ganti-berkas-video" class="col-md-9">
                                        <div id="preview-video">
                                            <input type="hidden" name="oldLogo" value="<?= $video_perkenalan->url_file ?>">
                                            <img src="<?php echo base_url() . $video_perkenalan->url_file ?>" alt="..." width="100%" class="image-previewer">
                                        </div><br>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="col-md-4">
                                    <label for="input_video" class="btn btn-primary text-white">Pilih Gambar</label>
                                    <input type="file" name="url_photo" id="input_video" accept=".png, .jpg, .jpeg " class="form-control custom-file" onchange="cekFile(this, 'video')">
                                    <span class="file-info-video text-muted">Tidak ada berkas yang dipilih</span>
                                    <div class="hint-block text-muted mt-3">
                                        <small>
                                            Ukuran dimensi yang direkomendasikan: <strong>1440 x 700 px</strong><br>
                                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                            Ukuran file maksimal: <strong>2 MB</strong>
                                        </small>
                                    </div>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="deskripsi" name="deskripsi" style="height: 240%;" maxlength="500"><?= $video_perkenalan->deskripsi; ?></textarea>
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni" style="margin-top:120px;">Simpan Perubahan</button>
                            </div>
                        </form>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle"><b>CARA MENGAMBIL LINK EMBED YOUTUBE</b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    1. Di komputer, buka video atau playlist YouTube yang ingin disematkan.<br>
                                    2. Klik BAGIKAN Bagikan.<br>
                                    3. Dari daftar opsi Bagikan, klik Sematkan.<br>
                                    4. Dari kotak yang muncul, salin kode HTML.<br>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="menu3" class="tab-pane fade show">
                        <form id="form_rektor">
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Sambutan Rektor</h5>
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Foto <b class="text-danger">*</b></label>
                                <input type="hidden" class="form-control" name="id" id="id" value="<?= $rektor->id ?>">
                                <input type="hidden" class="form-control" name="key" id="key" value="<?= $rektor->key ?>">
                                <div class="col-md-2">
                                    <div id="ganti-berkas-rektor" class="col-md-2">
                                        <div id="preview-rektor" style="width:15px;">
                                            <input type="hidden" name="oldLogo" value="<?= $rektor->url_file ?>">
                                            <img src="<?php echo base_url() . $rektor->url_file ?>" height="100%" alt="..." class="image-rektor">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3"></div>
                                <div class="col-md-5">
                                    <label for="input_rektor" class="btn btn-primary text-white" style="margin-top:20px;">Pilih Berkas</label>
                                    <input type="file" accept=".png, .jpg, .jpeg " name="url_photo" id="input_rektor" class="form-control custom-file" onchange="cekFile(this, 'rektor')">
                                    <span class="file-info-rektor text-muted">Tidak ada berkas yang dipilih</span>

                                    <div class="hint-block text-muted mt-3">
                                        <small>
                                            Ukuran dimensi yang direkomendasikan: <strong>600 x 800 px</strong><br>
                                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                            Ukuran file maksimal: <strong>2 MB</strong>
                                        </small>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Nama <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="title" name="title" value="<?= $rektor->title; ?>" maxlength="60">
                                    <div class="invalid-feedback" for="title"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="" name="deskripsi" maxlength="250" style="height: 150%;"><?= $rektor->deskripsi; ?></textarea>
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni" style="margin-top:50px;">Simpan Perubahan</button>
                            </div>
                        </form>
                    </div>
                    <div id="menu4" class="tab-pane fade show">
                        <form id="form_sosmed">
                            <input type="hidden" class="form-control" id="key" name="key" value="<?= $sistem->key; ?>">
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Instagram</h5>
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Link Instagram <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control instagram" id="instagram" name="instagram" value="<?php echo $sistem->instagram ?>" maxlength="60">
                                    <div class="invalid-feedback" for="instagram"></div>
                                </div>
                            </div>
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Twitter</h5>
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Link Twitter <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="twitter" name="twitter" value="<?php echo $sistem->twitter ?>" maxlength="60">
                                    <div class="invalid-feedback" for="twitter"></div>
                                </div>
                            </div>
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Youtube</h5>
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Link Youtube <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="youtube" name="youtube" value="<?php echo $sistem->youtube ?>" maxlength="60">
                                    <div class="invalid-feedback" for="youtube"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
                            </div>
                        </form>
                    </div>
                    <div id="menu5" class="tab-pane fade show">
                        <form id="form_header">
                            <h5 class="text-dark">
                                <span class="badge badge-pill badge-merah">&nbsp;</span>
                                <span class="badge badge-pill badge-warning">&nbsp;</span>
                                <span class="badge badge-pill badge-success">&nbsp;</span>
                                Header
                            </h5>
                            <input type="hidden" class="form-control" id="key" name="key" value="<?= $header->key; ?>">
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Warna Header&nbsp;<b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="warna_1" value="<?= $header->warna_1; ?>" data-jscolor="{}" , onInput="update(this.jscolor)" maxlength="7">
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Warna Font&nbsp;<b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="warna_2" value="<?= $header->warna_2; ?>" data-jscolor="{}" , onInput="update(this.jscolor)" maxlength="7">
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
                            </div>
                        </form>
                    </div>
                    <div id="menu6" class="tab-pane fade show">
                        <form id="form_footer">
                            <h5 class="text-dark">
                                <span class="badge badge-pill badge-merah">&nbsp;</span>
                                <span class="badge badge-pill badge-warning">&nbsp;</span>
                                <span class="badge badge-pill badge-success">&nbsp;</span>
                                Footer
                            </h5>
                            <input type="hidden" class="form-control" id="key" name="key" value="<?= $footer->key; ?>">
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Warna Footer&nbsp;<b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="warna_1" value="<?= $footer->warna_1; ?>" data-jscolor="{}" , onInput="update(this.jscolor)" maxlength="7">
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Warna Font&nbsp;<b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="warna_2" value="<?= $footer->warna_2; ?>" data-jscolor="{}" , onInput="update(this.jscolor)" maxlength="7">
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="deskripsi" class="col-md-3 col-form-label">About Us <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="" name="deskripsi" maxlength="250" style="height: 150%;"><?= $footer->deskripsi; ?></textarea>
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top:80px;">
                                <label for="sub_title" class="col-md-3 col-form-label">Copyright <b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="sub_title" name="sub_title" value="<?= $footer->sub_title; ?>" maxlength="60">
                                    <div class="invalid-feedback" for="sub_title"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
                            </div>
                        </form>
                    </div>
                    <div id="menu7" class="tab-pane fade show">
                        <form id="form_warna">
                            <h5 class="text-dark">
                                <span class="badge badge-pill badge-merah">&nbsp;</span>
                                <span class="badge badge-pill badge-warning">&nbsp;</span>
                                <span class="badge badge-pill badge-success">&nbsp;</span>
                                Pilih Tema
                            </h5>
                            <div class="form-group row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <em id="pr2" style="display:inline-block; padding:1em; height:160px; width:300px;border-radius:20px;"></em>
                                </div>
                            </div>
                            <input type="hidden" class="form-control" id="key" name="key" value="<?= $warna_tema->key; ?>">
                            <div class="form-group row">
                                <label for="waktu_selesai" class="col-md-3 col-form-label">Warna Tema&nbsp;<b class="text-danger">*</b></label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="deskripsi" value="<?= $warna_tema->deskripsi; ?>" data-jscolor="{}" , onInput="update(this.jscolor)" maxlength="7">
                                    <div class="invalid-feedback" for="deskripsi"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view("template/template_scripts") ?>
<script src="https://cdn.jsdelivr.net/gh/BossBele/cropzee@v2.0/dist/cropzee.js" defer></script>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>

<style>
    .image-title {
        height: 200px;
        width: 200px;
        display: flex;
        border-radius: 10px;
        object-fit: scale-down;
        border: 1px solid lightgrey;
    }

    .image-logo {
        height: 150px;
        width: 300px;
        display: flex;
        border-radius: 10px;
        object-fit: scale-down;
        border: 1px solid lightgrey;
    }

    .image-previewer {
        height: 150px;
        width: 300px;
        display: flex;
        border-radius: 10px;
        object-fit: cover;
        border: 1px solid lightgrey;
    }

    .image-bg {
        height: 100px;
        width: 220px;
        display: flex;
        object-fit: cover;
        border-radius: 10px;
        border: 1px solid lightgrey;
    }

    .image-rektor {
        width: 150px;
        height: 200px;
        display: flex;
        border-radius: 10px;
        object-fit: scale-down;
        border: 1px solid lightgrey;
    }

    .image-bg-besar {
        display: flex;
        border-radius: 10px;
        border: 1px solid lightgrey;
    }
</style>

<script>
    // Here we can adjust defaults for all color pickers on page:
    jscolor.presets.default = {
        position: 'right',
        palette: [
            '#000000', '#7d7d7d', '#870014', '#ec1c23', '#ff7e26',
            '#fef100', '#22b14b', '#00a1e7', '#3f47cc', '#a349a4',
            '#ffffff', '#c3c3c3', '#b87957', '#feaec9', '#ffc80d',
            '#eee3af', '#b5e61d', '#99d9ea', '#7092be', '#c8bfe7',
        ],
        //paletteCols: 12,
        //hideOnPaletteClick: true,
    };
    function update(picker) {
        document.getElementById('pr2').style.background = picker.toBackground();
    }
    jscolor.trigger('input');
</script>

<script>
    $(document).ready(function() {
        $("#input_banner").cropzee();
    });
    var destroyPlugin = function($elem, eventNamespace) {
        var isInstantiated = !!$.data($elem.get(0));
        if (isInstantiated) {
            $.removeData($elem.get(0));
            $elem.off(eventNamespace);
            $elem.unbind('.' + eventNamespace);
        }
    };
</script>

<script>
    $('#form_utama').on('submit', function(e) {
        e.preventDefault();

        $('#form_utama').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editsistem',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Logo Tab Title',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#form_logo_utama').on('submit', function(e) {
        e.preventDefault();

        $('#form_logo_utama').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editsistem',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Logo Universitas',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#form_about').on('submit', function(e) {
        e.preventDefault();

        $('#form_about').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editAbout_Us',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Sistem Setting Tentang Universitas',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#form_header').on('submit', function(e) {
        e.preventDefault();

        $('#form_header').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editAbout_Us',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Warna Header',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>
<script>
    $('#form_footer').on('submit', function(e) {
        e.preventDefault();

        $('#form_footer').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editAbout_Us',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit footer',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: 'var(--warna)',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#form_video').on('submit', function(e) {
        e.preventDefault();

        $('#form_video').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/updateVideo',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Video Perkenalan',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>
<script>
    $('#form_rektor').on('submit', function(e) {
        e.preventDefault();

        $('#form_rektor').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/updateVideo',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Sambutan Rektor',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#form_sosmed').on('submit', function(e) {
        e.preventDefault();

        $('#form_sosmed').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/update_sosmed',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Sosial Media',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#background_1').on('submit', function(e) {
        e.preventDefault();

        $('#background_1').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editsistem',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Background Banner',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#background_2').on('submit', function(e) {
        e.preventDefault();

        $('#background_2').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editsistem',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Background Banner',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#background_3').on('submit', function(e) {
        e.preventDefault();

        $('#background3').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editsistem',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Background Banner',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                }, 30000);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>
<script>
    $('#form_banner').on('submit', function(e) {
        e.preventDefault();

        $('#form_banner').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editAbout_Us',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Banner',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#form_tema').on('submit', function(e) {
        e.preventDefault();

        $('#form_tema').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editTema',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Tema',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>

<script>
    $('#form_warna').on('submit', function(e) {
        e.preventDefault();

        $('#form_warna').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editTema',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Tema',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    showCloseButton: true,
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>