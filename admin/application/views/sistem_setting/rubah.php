<div class="row">
    <div class="col-xs-12 col-lg-12">
        <form id="form_edit">
            <div class="card">
                <div class="card-body">

                    <div class="form-group row">
                        <label for="key" class="col-md-3 col-form-label">Nama <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="key" name="key" value="<?= $sistem->key; ?>" maxlength="60">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="url_link" class="col-md-3 col-form-label">Url Link <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="url_link" name="url_link" value="<?= $sistem->url_link; ?>" maxlength="60">
                            <!-- <div class="invalid-feedback" for="penyelenggara"></div> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="url_file" class="col-md-3 col-form-label">File <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="url_file" name="url_file" value="<?= $sistem->url_file; ?>" maxlength="60">
                            <!-- <div class="invalid-feedback" for="lokasi"></div> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="title" class="col-md-3 col-form-label">Title <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="title" name="title" value="<?= $sistem->title; ?>" maxlength="60">
                            <!-- <div class="invalid-feedback" for="lokasi"></div> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="sub_title" class="col-md-3 col-form-label">Sub Title <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="sub_title" name="sub_title" value="<?= $sistem->sub_title; ?>" maxlength="60">
                            <!-- <div class="invalid-feedback" for="lokasi"></div> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="deskripsi" name="deskripsi" maxlength="250"><?= $sistem->deskripsi; ?></textarea>
                            <div class="invalid-feedback" for="deskripsi"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo base_url('agenda/index/') ?>" class="btn btn-warning">Kembali</a>
                    <button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    ClassicEditor
        .create(document.querySelector('#deskripsi'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>

<script>
    $('#form_edit').on('submit', function(e) {
        e.preventDefault();

        $('#form_edit').find('input, select,textarea').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'sistem_setting/Index/editSistem',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    title: data.message.title,
                    text: data.message.body,
                    icon: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = site_url + 'sistem_setting/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>
<?php $this->load->view("template/template_scripts") ?>