<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Tracer - Study</title>
  <style>
    :root {
      --warna: <?php echo $warna_tema->deskripsi ?>;
      --warna-text: <?php echo $warna_tema->array ?>;
    }
  </style>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo URL_FILE . $logo_title->url_file ?>" rel="icon">
  <link href="<?php echo URL_FILE . $logo_title->url_file ?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <!-- <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Poppins:wght@300&display=swap" rel="stylesheet"> -->
  <link href="<?php echo base_url() ?>public/assets/fonts/poppins/Poppins-Regular.otf" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url() ?>public/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/select2/select2.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/datepicker/daterangepicker.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/datatables/datatables.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/assets/loading_page.css" rel="stylesheet">
  <script src="https://cdn.ckeditor.com/ckeditor5/35.2.1/classic/ckeditor.js"></script>

  <!-- Template Stisla CSS -->

  <link rel="stylesheet" href="<?php echo base_url() ?>public/assets/stisla/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>public/assets/stisla/css/components.css">

  <link href="<?php echo base_url() ?>public/assets/css/detail_alumni.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/assets/css/input.css" rel="stylesheet">
  <!-- alert -->
  <link rel="stylesheet" href="<?= base_url('public/vendor/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') ?>">
  <!-- izitoast -->
  <link href="<?= base_url('public/vendor/iziToast/dist/css/') ?>iziToast.min.css" rel="stylesheet" />
  <!-- izitoast -->
  <link href="<?php echo base_url('public/vendor/iziToast/dist/css/') ?>iziToast.min.css" rel="stylesheet" />
  <!-- end alert -->
  <script src="<?php echo base_url() ?>public/vendor/js/jquery-3.5.1.min.js"></script>
  <script src="<?php echo base_url() ?>public/assets/jscolor/jscolor.js"></script>
  <?php
  if (isset($output->css_files)) {
    foreach ($output->css_files as $file) {
      echo '<link type="text/css" rel="stylesheet" href="' . $file . '"/>';
    }
  }
  ?>
</head>

<body style="overflow-x: hidden;">
  <div id="spinner-front">
    <div id="loading"></div>
  </div>
  <div id="spinner-back"></div>
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar justify-content-between">
        <form class="form-inline mr-auto d-flex">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars" style="margin-top: 5px;"></i></a></li>
            <li style="width: 95px;">
              <select name="data_tahun" id="data_tahun" class="form-control select2">
                <?php foreach ($tahun as $key => $value) { ?>
                  <option value="<?= $key ?>" <?php echo ($this->session->userdata("tracer_tahun") == $key ? 'selected' : '') ?>><?= $value ?></option>
                <?php } ?>
              </select>
            </li>
          </ul>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="nav-link dropdown-toggle nav-link-lg nav-link-user" id="dropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php
              $urlLogo = base_url() . 'public/assets/img/logo-besar.png';
              if ($tracer_idGroup == 2) {
                $urlLogo = base_url() . 'public/assets/img/unknown.webp';
                if (!empty($this->session->userdata("tracer_urlLogo"))) {
                  $urlLogo = base_url() . $this->session->userdata("tracer_urlLogo");
                }
              }
              ?>

              <div class="d-sm-none d-lg-inline-block"><i class="fas fa-user">&nbsp;&nbsp;</i> <?php echo showTitleName(); ?></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right" id="dropdownProfile">
              <!-- <div class="dropdown-title">Logged in 5 min ago</div> -->
              <!-- <a href="features-profile.html" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <a href="features-activities.html" class="dropdown-item has-icon">
                <i class="fas fa-bolt"></i> Activities
              </a>
              <a href="features-settings.html" class="dropdown-item has-icon">
                <i class="fas fa-cog"></i> Settings
              </a> -->
              <div class="dropdown-divider"></div>
              <a href="<?php echo site_url('logout') ?>" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>

      <div class="main-sidebar">
        <aside id="sidebar-wrapper">

          <div class="sidebar-brand">
            <a class="nav-link" href="<?php echo base_url() ?>alumni/dashboard"><img class="logo img-fluid text-center" src="<?php echo URL_FILE . $logo_utama->url_file ?>" alt="" style="height:80px;margin-top:-180px;"></a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a class="nav-link" href="<?php echo base_url() ?>alumni/dashboard"><img class="logo img-fluid text-center" src="<?php echo URL_FILE . $logo_title->url_file ?>" alt="" style="margin-top:-180px;"></a>
          </div>

          <ul class="sidebar-menu" style="margin-top:-120px;">
            <li class="<?php echo active_page("index", "active") ?>"><a class="nav-link" href="<?php echo site_url('index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard"><i class="fas fa-house"></i> <span>Dashboard</span></a></li>
            <?php if (in_array("universitas_profil.access", $userMenus)) : ?>
              <li class="<?php echo active_page('universitas_profil', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('universitas_profil/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Profil"><i class="fas fa-calendar-days"></i> <span>Profil</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("tahun.access", $userMenus) || in_array("perguruan_tinggi.access", $userMenus)) : ?>
            <?php endif; ?>
            <?php if (in_array("tahun.access", $userMenus)) : ?>
              <li class="<?php echo active_page('tahun', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('tahun/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Tahun"><i class="fas fa-calendar-days"></i> <span>Tahun</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("alumni_berprestasi.access", $userMenus)) : ?>
              <li class="<?php echo active_page('alumni_berprestasi', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('alumni_berprestasi/index') ?>"><i class="fas fa-user-graduate"></i><span>Alumni Berprestasi</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("agenda.access", $userMenus)) : ?>
              <li class="<?php echo active_page('agenda', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('agenda/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Agenda"><i class="fas fa-calendar-check"></i> <span>Agenda</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("berita.access", $userMenus)) : ?>
              <li class="<?php echo active_page('berita', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('tips_karir/Index') ?>"><i class="far fa-list-alt"></i><span>Tips Karir</span></a>
              </li>
            <?php endif; ?>


            <li class="menu-header">DATA POKOK</li>
            <!-- <?php if (in_array("alumni.access", $userMenus) || in_array("jenis_survey.access", $userMenus) || in_array("kuesioner_survey.access", $userMenus) || in_array("jadwal_survey.access", $userMenus) || in_array("legalisir.access", $userMenus)) : ?>
              <?php if (in_array("jenis_survey.access", $userMenus) || in_array("kuesioner_survey.access", $userMenus) || in_array("alumni.access", $userMenus)) : ?>
                <li class="nav-item dropdown <?php echo active_page('alumni', 'active'); ?><?php echo active_page('rekap_alumni', 'active') ?>">
                  <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-book"></i> <span>Alumni</span></a>
                  <ul class="dropdown-menu"> -->
            <?php if (in_array("alumni.access", $userMenus)) : ?>
              <li class="<?php echo active_page('alumni', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('alumni/dashboard/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Alumni"><i class="fas fa-book"></i><span>Alumni</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("rekap_alumni.access", $userMenus)) : ?>
              <li class="<?php echo active_page('rekap_alumni', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('rekap_alumni/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Alumni"><i class="far fa-list-alt"></i><span>Rekap Alumni</span></a>
              </li>
            <?php endif; ?>
            <!-- <?php if (in_array("rekap_alumni.access", $userMenus)) : ?>
                      <li class="<?php echo active_page('rekap_alumni', 'active') ?>">
                        <a class="nav-link" href="<?php echo site_url('rekap_alumni/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Alumni"><span>Pengaturan Alumni</span></a>
                      </li>
                    <?php endif; ?>
                  </ul>
                </li>
              <?php endif; ?>
            <?php endif; ?> -->
            <?php if (in_array("perguruan_tinggi.access", $userMenus)) : ?>
              <li class="<?php echo active_page('perguruan_tinggi', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('perguruan_tinggi') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Perguruan Tinggi"><i class="fas fa-landmark"></i> <span>Perguruan Tinggi</span></a>
              </li>
            <?php endif; ?>

            <?php if (in_array("prodi.access", $userMenus)) : ?>
              <li class="<?php echo active_page('prodi', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('prodi/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Prodi"><i class="fas fa-graduation-cap"></i> <span>Program Studi</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("jenis_survey.access", $userMenus) || in_array("kuesioner_survey.access", $userMenus)) : ?>
              <li class="nav-item dropdown <?php echo active_page('jenis_survey', 'active') ?><?php echo active_page('kuesioner_survey', 'active') ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-scroll"></i> <span>Survey</span></a>
                <ul class="dropdown-menu">
                  <?php if (in_array("jenis_survey.access", $userMenus)) : ?>
                    <li class="<?php echo active_page('jenis_survey', 'active') ?>"><a class="nav-link dropdown-item" href="<?php echo site_url('jenis_survey/index') ?>"><span>Jenis survey</span></a></li>
                  <?php endif; ?>
                  <?php if (in_array("kuesioner_survey.access", $userMenus)) : ?>
                    <li class="<?php echo active_page('kuesioner_survey', 'active') ?>"><a class="nav-link dropdown-item" href="<?php echo site_url('kuesioner_survey/index') ?>"><span>Kuesioner survey</span></a></li>
                  <?php endif; ?>
                </ul>
              </li>
            <?php endif; ?>
            <?php if (in_array("jadwal_survey.access", $userMenus)) : ?>
              <li class="<?php echo active_page('jadwal_survey', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('jadwal_survey/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Jadwal Survey"><i class="fas fa-calendar-days"></i> <span>Jadwal survey</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("laporan.access", $userMenus)) : ?>
              <li class="<?php echo active_page('laporan', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('laporan/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Laporan"><i class="fa-solid fa-scroll"></i> <span>Laporan</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("legalisir.access", $userMenus)) : ?>
              <li class="<?php echo active_page('legalisir', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('legalisir/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Legalisir"><i class="fas fa-file-circle-check"></i> <span>Legalisir</span></a>
              </li>
            <?php endif; ?>
            <!-- <?php if (in_array("mitra.access", $userMenus) || in_array("kuesioner_mitra.access", $userMenus) || in_array("jadwal_mitra.access", $userMenus) || in_array("lowongan_kerja.access", $userMenus) || in_array("lowongan_magang.access", $userMenus)) : ?> -->
          <?php endif; ?>
          <?php if (in_array("mitra.access", $userMenus)) : ?>
            <li class="<?php echo active_page('mitra', 'active') ?>">
              <a class="nav-link" href="<?php echo site_url('mitra/dashboard') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Mitra"><i class="fas fa-handshake"></i> <span>Mitra Kami</span></a>
            </li>
          <?php endif; ?>
          <!-- <?php if (in_array("lowongan_kerja.access", $userMenus)) : ?>
              <li class="<?php echo active_page('lowongan_kerja', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('lowongan_kerja/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Lowongan Kerja"><i class="fas fa-briefcase"></i> <span>Lowongan Kerja</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("lowongan_magang.access", $userMenus)) : ?>
              <li class="<?php echo active_page('lowongan_magang', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('lowongan_magang/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Lowongan Magang"><i class="fas fa-briefcase"></i> <span>Lowongan Magang</span></a>
              </li>
            <?php endif; ?> -->
          <!-- <?php if (in_array("kuesioner_mitra.access", $userMenus)) : ?>
              <li class="<?php echo active_page('kuesioner_mitra', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('kuesioner_mitra/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Kuesioner Mitra"><i class="fas fa-square-poll-vertical"></i> <span>Kuesioner Mitra</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("jadwal_mitra.access", $userMenus)) : ?>
              <li class="<?php echo active_page('jadwal_mitra', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('jadwal_mitra/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Jadwal Survey Mitra"><i class="fas fa-calendar-days"></i> <span>Jadwal Survey Mitra</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("layanan_keuangan.access", $userMenus)) : ?>
              <li class="<?php echo active_page('layanan_keuangan', 'active') ?>">
                <a class="nav-link" href="<?php echo site_url('layanan_keuangan/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Layanan Keuangan"><i class="fas fa-briefcase"></i> <span>Layanan Keuangan</span></a>
              </li>
            <?php endif; ?>
            <?php if (in_array("company_talk.access", $userMenus)) : ?>
              <li>
                <a class="nav-link" href="<?php echo site_url('company_talk/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Company Talk">
                  <i class="fas fa-comments"></i> <span>Company Talk</span>
                </a>
              </li>
            <?php endif; ?>
            <?php if (in_array("career_fair.access", $userMenus)) : ?>
              <li>
                <a class="nav-link" href="<?php echo site_url('career_fair/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Career Fair">
                  <i class="fas fa-building"></i> <span>Career Fair</span>
                </a>
              </li>
            <?php endif; ?> -->
          <!-- <li class="menu-header">Lap. Survey Alumni</li>
            <li><a class="nav-link" href="<?php echo site_url('my404') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Laporan Keselarasan Horizontal"><i class="fas fa-briefcase"></i> <span>Laporan keselarasan horizontal</span></a></li>
            <li><a class="nav-link" href="<?php echo site_url('my404') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Laporan Vertikal"><i class="fas fa-briefcase"></i> <span>Laporan vertikal </span></a></li>
            <li><a class="nav-link" href="<?php echo site_url('my404') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Laporan Kompetensi Dasar"><i class="fas fa-briefcase"></i> <span>Laporan kompetensi alumni</span></a></li>
            <li><a class="nav-link" href="<?php echo site_url('my404') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Laporan Pekerjaan"><i class="fas fa-briefcase"></i> <span>Laporan pekekerjaan</span></a></li>
            <li><a class="nav-link" href="<?php echo site_url('my404') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Laporan Penghasilan Alumni"><i class="fas fa-briefcase"></i> <span>Laporan penghasilan alumni</span></a></li>
            <li><a class="nav-link" href="<?php echo site_url('my404') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Laporan Statistik Per Prodi"><i class="fas fa-briefcase"></i> <span>Laporan statistik per prodi</span></a></li>
            <li><a class="nav-link" href="<?php echo site_url('my404') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Laporan Tracer Alumni"><i class="fas fa-briefcase"></i> <span>Laporan tracer alumni</span></a></li>
            <li><a class="nav-link" href="<?php echo site_url('my404') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Laporan Statistik Per Polbangtan PEPI"><i class="fas fa-briefcase"></i> <span> Laporan statistik per Polbangtan PEPI</span></a></li> -->

          <?php if (in_array("group_module.access", $userMenus) || in_array("modul.access", $userMenus)) : ?>
            <li class="menu-header">SETTINGS</li>
          <?php endif; ?>
          <?php if (in_array("sistem_setting.access", $userMenus)) : ?>
            <li class="<?php echo active_page('sistem_setting', 'active') ?>">
              <a class="nav-link" href="<?php echo site_url('sistem_setting/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Group Module"><i class="fas fa-gears"></i> <span>Sistem Setting</span></a>
            </li>
          <?php endif; ?>
          <?php if (in_array("faq.access", $userMenus)) : ?>
            <li class="<?php echo active_page('faq', 'active') ?>">
              <a class="nav-link" href="<?php echo site_url('faq/Index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Group Module"><i class="fas fa-layer-group"></i> <span>FAQ</span></a>
            </li>
          <?php endif; ?>
          <?php if (in_array("group_module.access", $userMenus)) : ?>
            <li class="<?php echo active_page('group_module', 'active') ?>">
              <a class="nav-link" href="<?php echo site_url('group_module') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Group Module"><i class="fas fa-layer-group"></i> <span>Group Module</span></a>
            </li>
          <?php endif; ?>
          <?php if (in_array("modul.access", $userMenus)) : ?>
            <li class="<?php echo active_page('modul', 'active') ?>">
              <a class="nav-link" href="<?php echo site_url('modul') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="Modul Aplikasi"><i class="fas fa-database"></i> <span>Modul Aplikasi</span></a>
            </li>
          <?php endif; ?>
          <!-- <?php if (in_array("vidio.access", $userMenus) || in_array("alumni_berprestasi.access", $userMenus) || in_array("banner.access", $userMenus) || in_array("perkenalan.access", $userMenus) || in_array("berita.access", $userMenus) || in_array("footer.access", $userMenus) || in_array("visi_misi.access", $userMenus) || in_array("setting_tema.access", $userMenus)) : ?>
            <li class="nav-item dropdown <?php echo active_page('vidio', 'active') ?><?php echo active_page('alumni_berprestasi', 'active') ?><?php echo active_page('banner', 'active') ?><?php echo active_page('perkenalan', 'active') ?><?php echo active_page('berita', 'active') ?><?php echo active_page('footer', 'active') ?><?php echo active_page('visi_misi', 'active') ?><?php echo active_page('setting_tema', 'active') ?>">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class='fas fa-paint-brush'></i> <span>Pengaturan Landing</span></a>
              <ul class="dropdown-menu">
                <?php if (in_array("vidio.access", $userMenus)) : ?>
                  <li class="<?php echo active_page('vidio', 'active') ?>"><a class="nav-link dropdown-item" href="<?php echo site_url('vidio/index') ?>"><span>Setting Video</span></a></li>
                <?php endif; ?>
                <?php if (in_array("setting_tema.access", $userMenus)) : ?>
                  <li class="<?php echo active_page('setting_tema', 'active') ?>"><a class="nav-link dropdown-item" href="<?php echo site_url('setting_tema/index/') ?>"><span>Setting Tema</span></a></li>
                <?php endif; ?>
                
                <?php if (in_array("banner.access", $userMenus)) : ?>
                  <li class="<?php echo active_page('banner', 'active') ?>"><a class="nav-link dropdown-item" href="<?php echo site_url('banner/index') ?>"><span>Banner</span></a></li>
                <?php endif; ?>
                <?php if (in_array("berita.access", $userMenus)) : ?>
                  <li class="<?php echo active_page('berita', 'active') ?>"><a class="nav-link dropdown-item" href="<?php echo site_url('berita/index') ?>"><span>Tips Karir</span></a></li>
                <?php endif; ?>
                <?php if (in_array("footer.access", $userMenus)) : ?>
                  <li class="<?php echo active_page('footer', 'active') ?>"><a class="nav-link dropdown-item" href="<?php echo site_url('footer/index') ?>"><span>Footer</span></a></li>
                <?php endif; ?>
                <?php if (in_array("visi_misi.access", $userMenus)) : ?>
                  <li class="<?php echo active_page('visi_misi', 'active') ?>"><a class="nav-link dropdown-item" href="<?php echo site_url('visi_misi/index') ?>"><span>Visi Misi</span></a></li>
                <?php endif; ?>
              </ul>
            </li>
          <?php endif; ?> -->
          <?php if (in_array("user_akses.access", $userMenus)) : ?>
            <li class="<?php echo active_page('user_akses', 'active') ?>">
              <a class="nav-link" href="<?php echo site_url('user_akses/index') ?>" data-bs-toggle="tooltip" data-bs-placement="right" title="User Akses"><i class="fa-solid fa-user"></i> <span>User Akses</span></a>
            </li>
          <?php endif; ?>
          <li><a class="nav-link" href="<?php echo base_url() ?>logout" data-bs-toggle="tooltip" data-bs-placement="right" title="Keluar"><i class="fas fa-right-from-bracket"></i> <span>Keluar</span></a></li>
          </ul>

        </aside>
      </div>

      <div class="main-content">
        <section class="section">
          <?php if ($is_home === true) : ?>
            <h2 style="margin-top:40px;"><i class="bx bxs-dashboard bx-sm text-tema" style="margin-top: 5px;"></i> <span class="text-dark">Dasboard</span></h2>
            <div class="topi card">
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-10 col-xs-10">
                    <h2>Selamat Datang <?php echo $this->session->userdata('tracer_nama') ?></h2>
                    <p class="">Selamat Datang di Aplikasi Tracer Study. Anda masuk sebagai admin perguruan tinggi, </p>
                    <p>pastikan data yang ingin kamu kelola sudah terisi dengan benar.</p>
                    <p class="font-weight-bold text-dark">Alumni terdata tahun lulus <?= $this->session->userdata("tracer_tahun") ?> sejumlah <?= (isset($total) ? $total : '-') ?></p>

                  </div>
                </div>
              </div>
            </div>
          <?php else : ?>
            <div class="section-header justify-content-between">
              <div class="col-md-6">
                <h1 style="margin-left:-30px;"><i class="<?php echo (isset($icon) ? $icon : "Not found") ?> text-tema"></i> <strong class="text-dark"><?php echo (isset($title) ? $title : "Not found") ?></strong></h1>
              </div>
              <div class="pull-right">
                <a href="<?php echo base_url() ?>index" type="button" class="text-tema">Dasboard </a><a href="<?php echo base_url() ?><?php echo (isset($url_back) ? $url_back : "Not found") ?>" type="button" class="text-tema"><?php echo (isset($back) ? $back : "") ?></a><a class="text-dark"> / <?php echo (isset($title) ? $title : "Not found") ?></a>
                <a href="" role="button" class="btn btn-theme-dark float-end" style="border-radius: 16px;margin-right:-20px;"><i class="fas fa-rotate"></i> Refresh</a>
              </div>
            </div>
          <?php endif; ?>
        </section>
        <section class="content">
          {CONTENT}
        </section>

      </div>

    </div>
  </div>

</body>

<script>
  $('[name="data_tahun"]').change(function(e) {
    var tahun = e.target.value;
    $.ajax({
      url: site_url + 'session_tahun/tahun',
      type: "POST",
      data: {
        tahun: tahun
      },
      dataType: "JSON",
      success: function(data) {
        location.reload();
      },
      error: function(xx) {
        alert(xx)
      }
    });
  });
</script>

</html>