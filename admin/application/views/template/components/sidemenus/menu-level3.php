<li>
  <a href="<?= site_url($url) ?>">
    <i class="fa-fw <?= $icon ?>"></i>
    <span><?= $nama ?></span>
  </a>
</li>