<li class="nav-item with-sub">
  <a href="" class="nav-link">
    <i class="fa-lg fa-fw <?= $icon ?>"></i>
    <span><?= $nama ?></span>
  </a>
  <ul>
    <?php
    foreach ($children as $k => $child) :
      $this->load->view("template/components/sidemenus/menu-level3", $child);
    endforeach; ?>

  </ul>

</li>