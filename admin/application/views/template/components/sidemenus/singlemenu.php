<li class="nav-item">
  <a href="<?= site_url($url) ?>" class="nav-link">
    <i class="fa-lg fa-fw <?= $icon ?>"></i>
    <span><?= $nama ?></span>
  </a>
</li>