<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Tracer - Study</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <style>
    :root {
      --warna : <?php echo $warna_tema->deskripsi ?>;
      --warna-text : <?php echo $warna_tema->array ?>;
    }
  </style>

  <!-- Favicons -->
  <link href="<?php echo URL_FILE . $logo_title->url_file ?>" rel="icon">
  <link href="<?php echo base_url() ?>public/assets/img/logo_icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <!-- <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Poppins:wght@300&display=swap" rel="stylesheet"> -->
  <link href="<?php echo base_url() ?>public/assets/fonts/poppins/Poppins-Regular.otf" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url() ?>public/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/owl-carousel/css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/@fortawesome/fontawesome-free/css/all.css" rel="stylesheet">
  <link href="<?php echo base_url() ?>public/vendor/select2/select2.min.css" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url() ?>public/assets/css/login.css">

  <!-- Template Stisla CSS -->
  
    <link rel="stylesheet" href="<?php echo base_url() ?>public/assets/stisla/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>public/assets/stisla/css/components.css">
  
  <!-- <link rel="stylesheet" href="<?php echo base_url() ?>public/assets/stisla/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>public/assets/stisla/css/components.css"> -->
</head>

<body>

  {CONTENT}
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url() ?>public/assets/js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/select2/select2.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/aos/aos.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/owl-carousel/js/owl.carousel.min.js"></script>

  <script src="<?php echo base_url() ?>public/vendor/jquery.nicescroll-3.7.6/jquery.nicescroll.min.js"></script>

  <!-- Template Stisla JS File -->
  <script src="<?php echo base_url() ?>public/assets/stisla/js/stisla.js"></script>

  <!-- Template JS File -->
  <script src="<?php echo base_url() ?>public/assets/stisla/js/scripts.js"></script>
  <script src="<?php echo base_url() ?>public/assets/stisla/js/custom.js"></script>
  <script>
    function onlyNumber(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }

    $('.select2').select2();
  </script>

</body>

</html>