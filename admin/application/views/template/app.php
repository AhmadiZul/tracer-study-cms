<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="refresh" content="1800;url=<?= site_url('auth/logout') ?>" />
    <!-- Favicon -->
    <link rel="apple-touch-icon" href="<?= base_url("/public/assets/img/logo_icon.png") ?>">
    
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url("/public/assets/img/yess/pusdiktan.png") ?>">
    <title><?= SITENAME ?> &mdash; <?= $title ?></title>

    <!-- font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!-- <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet"> -->
    <link href="<?php echo base_url() ?>public/assets/fonts/poppins/Poppins-Regular.otf" rel="stylesheet">

    <!-- vendor css -->
    <link href="<?= base_url("public/lib/@fortawesome/fontawesome-free/css/all.min.css") ?>" rel="stylesheet">
    <link href="<?= base_url("public/lib/ionicons/css/ionicons.min.css") ?>" rel="stylesheet">
    <link href="<?= base_url("public/lib/jqvmap/jqvmap.min.css") ?>" rel="stylesheet">
    <!-- <link rel="stylesheet" href="<?= base_url('public/lib/datatables/bootstrap/datatables.bootstrap.css') ?>">
  <link rel="stylesheet" href="<?= base_url('public/lib/datatables/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>"> -->

    <link href="<?= base_url('public/lib') ?>/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="<?= base_url('public/lib') ?>/datatables.net-responsive-dt/css/responsive.dataTables.min.css"
        rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="<?= base_url("public/assets/css/dashforge.css") ?>">
    <!-- <link rel="stylesheet" href="<?= base_url("public/assets/css/dashforge.dashboard.css") ?>">  -->
    <!-- <link rel="stylesheet" href="<?= base_url('public') ?>/assets/css/skin.light.css"> -->

    <link rel="stylesheet" href="<?= base_url("public/assets/css/skin.yess.css") ?>">
    <!-- sweetalert -->
    <link rel="stylesheet" href="<?= base_url('public/lib/sweetalert2-theme-bootstrap-4/bootstrap-4.css') ?>">
    <!-- izitoast -->
    <link href="<?= base_url('public/lib/iziToast/dist/css/') ?>iziToast.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?= base_url('public/lib/select2/css/select2.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/lib/select2-bootstrap4-theme/select2-bootstrap4.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/lib/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('public/assets/css/customcss.css') ?>">
    <link href="<?= base_url('public') ?>/lib/summernote/summernote-bs4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('public') ?>/lib/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?= base_url('public/') ?>lib/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <link href="<?= base_url("/public/loading/loading_page.css"); ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url("/public/lib/bootstrap-datepicker/css/bootstrap-datepicker.min.css"); ?>" rel="stylesheet"
        type="text/css">
    <link href="<?= base_url("/public/assets/css/custom_css.css"); ?>" rel="stylesheet" type="text/css">
    <!-- <link href="https://api.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.css" rel="stylesheet" /> -->
    <?php if (isset($loadFontAwesomePicker) && $loadFontAwesomePicker) : ?>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/css/fontawesome-iconpicker.css" />

    <style>
    .fade.in {
        opacity: 1;
    }

    .ui-datepicker {
        z-index: 9999999 !important;
    }
    </style>
    <?php endif; ?>

    <?php if (isset($loadSortableJs) && $loadSortableJs) : ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css"
        integrity="sha512-yOW3WV01iPnrQrlHYlpnfVooIAQl/hujmnCmiM3+u8F/6cCgA3BdFjqQfu8XaOtPilD/yYBJR3Io4PO8QUQKWA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet" href="<?= base_url("public/assets/css/dragable-extra.css") ?>">

    <?php endif; ?>
</head>
<!-- Head Libs -->
<?php
if (isset($output->css_files)) {
    foreach ($output->css_files as $file) {
        echo '<link type="text/css" rel="stylesheet" href="' . $file . '"/>';
    }
}
?>

<body class="text-dark">
    <div id="spinner-front">
        <img src="<?= base_url('public/loading/ajax-loader.gif'); ?>" /><br>
        Loading...
    </div>
    <div id="spinner-back"></div>
    <aside class="aside aside-fixed">
        <div class="aside-header">
            <a href="<?= site_url('dashboard') ?>" class="aside-custom-logo">
                <img src="<?= base_url("public/assets/img/yess/logo-font-white.svg") ?>" alt="Logo YESS">
            </a>

            <!-- <a href="" class="aside-menu-link">
                <i data-feather="menu"></i>
                <i data-feather="x"></i>
            </a> -->
        </div>
        <div class="aside-body">
            <ul class="nav nav-aside">
                <!-- <li class="nav-label mg-t-25">Menu</li> -->
                <li class="nav-item <?= active_subpage(['dashboard']) ?>"><a href="<?= site_url('dashboard') ?>"
                        class="nav-link"><i class="fa fa-home fa-lg fa-fw"></i> <span>Dashboard</span></a></li>
                <!-- Start master hk -->
                <?php if (in_array("skema_pwmp.access", $userMenus)) : ?>
                <li class="nav-label">Data Pokok</li>
                <li class="nav-item <?= active_uri('skema_pwmp') ?>"><a href="<?= site_url('skema_pwmp') ?>"
                        class="nav-link"><i class="fa fa-list-ol fa-lg fa-fw"></i> <span>Skema PWMP</span></a></li>
                <?php endif; ?>
                <?php if (in_array("pelaksana.access", $userMenus)) : ?>
                <li class="nav-item <?= active_uri('pelaksana') ?>"><a href="<?= site_url('pelaksana') ?>"
                        class="nav-link"><i class="fa fa-user-astronaut fa-lg fa-fw"></i> <span>Pelaksana</span></a>
                </li>
                <?php endif; ?>
                <?php if (in_array("korwil.access", $userMenus)) : ?>
                <li class="nav-item <?= active_uri('korwil') ?>"><a href="<?= site_url('korwil') ?>" class="nav-link"><i
                            class="fa fa-user-astronaut fa-lg fa-fw"></i> <span>Koordinator Wilayah</span></a></li>
                <?php endif; ?>
                <?php if (in_array("mentor.access", $userMenus)) : ?>
                <li class="nav-item <?= active_uri('mentor') ?>"><a href="<?= site_url('mentor') ?>" class="nav-link"><i
                            class="fa fa-user-astronaut fa-lg fa-fw"></i> <span>Mentor</span></a></li>
                <?php endif; ?>
                <?php if (in_array("pembimbing.access", $userMenus)) : ?>
                <li class="nav-item <?= active_uri('pembimbing') ?>"><a href="<?= site_url('pembimbing') ?>"
                        class="nav-link"><i class="fa fa-user-astronaut fa-lg fa-fw"></i> <span>Pembimbing</span></a>
                </li>
                <?php endif; ?>
                <?php if (in_array("institusi.access", $userMenus)) : ?>
                <li class="nav-item <?= active_uri('institusi') ?>"><a href="<?= site_url('institusi') ?>"
                        class="nav-link"><i class="fa fa-user-astronaut fa-lg fa-fw"></i> <span>Institusi</span></a>
                </li>
                <?php endif; ?>
                <?php if (in_array("profil.access", $userMenus)) : ?>
                <li class="nav-item <?= active_uri('profil') ?>"><a href="<?= site_url('profil') ?>" class="nav-link"><i
                            class="fa fa-user fa-lg fa-fw"></i> <span>profil</span></a>
                </li>
                <?php endif; ?>

                <!-- PWMP -->
                <?php if (in_array("pendaftar.access", $userMenus)) { ?>
                <li class="nav-label mg-t-25">Peserta PWMP</li>
                <?php } ?>
                <?php if (in_array("pendaftar.access", $userMenus)) : ?>
                <li class="nav-item <?= active_subpage(['pendaftar']) ?>"><a href="<?= site_url('pendaftar') ?>"
                        class="nav-link"><i class="fa fa-user-check fa-lg fa-fw"></i> <span>Pendaftar PWMP</span></a>
                </li>
                <?php endif; ?>
                <?php if (in_array("peserta.access", $userMenus)) : ?>
                <li class="nav-item <?= active_subpage(['peserta']) ?>"><a href="<?= site_url('peserta') ?>"
                        class="nav-link"><i class="fa fa-user-check fa-lg fa-fw"></i> <span>Peserta PWMP</span></a>
                </li>
                <?php endif; ?>
                <!-- END PWMP -->




                <!-- Institution Program -->
                <?php if (in_array("informasi.access", $userMenus)) { ?>
                <li class="nav-label mg-t-25">Information</li>
                <?php } ?>
                <?php if (in_array("berita.access", $userMenus)) { ?>
                <li class="nav-item <?= active_page('berita', 'active') ?>"><a href="<?= site_url('berita') ?>"
                        class="nav-link"><i class="fa fa-newspaper fa-lg fa-fw"></i> <span>Berita</span></a></li>
                <?php } ?>

                <?php if (in_array("informasi.access", $userMenus)) { ?>
                <li class="nav-item <?= active_page('informasi', 'active') ?>"><a href="<?= site_url('informasi') ?>"
                        class="nav-link"><i class="fa fa-bullhorn fa-lg fa-fw"></i> <span>Pengumuman</span></a></li>
                <?php } ?>
                <?php if (in_array("dokumen.access", $userMenus)) { ?>
                <li class="nav-item <?= active_page('dokumen', 'active') ?>"><a
                        href="<?= site_url('dashboard/dokumen') ?>" class="nav-link"><i
                            class="fa fa-file-alt fa-lg fa-fw"></i> <span>Dokumen</span></a></li>
                <?php } ?>

                <!-- Institution Program -->


                <?php if (in_array("system_app.access", $userMenus)) { ?>
                <li class="nav-label mg-t-25">Setting</li>

                <li class="nav-item <?= active_subpage(['setting', 'modules']) ?>"><a
                        href="<?= site_url('setting/modules') ?>" class="nav-link"><i
                            class="fas fa-puzzle-piece fa-lg fa-fw"></i> <span>System Modules</span></a></li>

                <li class="nav-item <?= active_subpage(['setting', 'groups']) ?>"><a
                        href="<?= site_url('setting/groups') ?>" class="nav-link"><i
                            class="fas fa-users-cog fa-lg fa-fw"></i> <span>Group Access</span></a></li>

                <li class="nav-item"><a href="<?= site_url('system/settings') ?>" class="nav-link"><i
                            data-feather="settings"></i> <span>Pengaturan Sistem</span></a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </aside>
    <div class="content ht-100v pd-0">
        <div class="content-header" style="height: 70px !important;">
            <div class="content-search">
                <a href="" class="aside-menu-link">
                    <i data-feather="menu"></i>
                    <i data-feather="x"></i>
                </a>
            </div>



            <nav class="nav">
                <!-- <a href="" class="nav-link"><i data-feather="help-circle"></i></a>
                <a href="" class="nav-link"><i data-feather="grid"></i></a> -->



                <div class=" d-flex justify-content-between align-items-center w-100" data-toggle="dropdown"
                    aria-haspopup="true" type="button" aria-expanded="false">
                    <div class="d-flex align-items-center mr-2">
                        <div class="avatar mr-2"><span
                                class="avatar-initial rounded-circle"><?= getInitial($this->session->userdata('pwmp_nama')) ?></span>
                        </div>
                        <span>
                            <strong class="tx-12"><?= $this->session->userdata('pwmp_nama') ?></strong>
                            <br>
                            <small class="tx-11 tx-gray-600"><?= $this->session->userdata('pwmp_namaGroup') ?></small>
                        </span>
                    </div>
                    <i class="fas fa-chevron-down fa-fw fa-sm"></i>
                </div>

                <div class="dropdown-menu dropdown-menu-right">

                    <a href="<?= site_url() ?>" role="button" class="dropdown-item" type="button">
                        <i class="fas fa-home mr-2"></i>
                        <span>Beranda</span>
                    </a>

                    <a href="<?= site_url('informasi/berita') ?>" role="button" class="dropdown-item" type="button">
                        <i class="fas fa-newspaper mr-2"></i>
                        <span>Berita</span>
                    </a>

                    <a href="<?= site_url('informasi/pengumuman') ?>" role="button" class="dropdown-item" type="button">
                        <i class="fas fa-bullhorn mr-2"></i>
                        <span>Pengumuman</span>
                    </a>
                    <a href="<?= site_url('informasi/dokumen') ?>" role="button" class="dropdown-item" type="button">
                        <i class="fas fa-bullhorn mr-2"></i>
                        <span>Dokumen</span>
                    </a>

                    <a href="<?= site_url('auth/logout') ?>" role="button" class="dropdown-item" type="button">
                        <i class="fas fa-sign-out-alt mr-2"></i>
                        <span>Keluar</span>
                    </a>

                </div>

            </nav>
        </div>
        <!-- content-header -->
        <!--         <div class="content-body"> -->
        <div class="container-fluid mb-4 pb-4">
            <div class="mt-2 mb-4 pb-4">
                {CONTENT}
            </div>
            <!-- </div> -->
            <!-- </div> -->
        </div>


        <div class="footer-copyright bg-green-dark fixed-bottom d-none">
            <div class="container py-1">
                <div class="pt-1 pb-0 d-flex align-items-center justify-content-center ">
                    <p class="text-white">&copy; Copyright <?= date('Y') ?>. All Rights Reserved.</p>
                </div>

            </div>
        </div>
    </div>

</body>

<!-- <script src="https://api.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.js"></script> -->

<?php if (isset($loadHighChart) && $loadHighChart) : ?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<?php endif; ?>
<?php
echo '<script>';
if (isset($scripts) && is_array($scripts)) {
    foreach ($scripts as $script) {
        $this->load->view($script);
    }
}
echo '</script>';
?>
<script>
function showLoading() {
    document.getElementById("spinner-front").classList.add("show");
    document.getElementById("spinner-back").classList.add("show");
}

function hideLoading() {
    document.getElementById("spinner-front").classList.remove("show");
    document.getElementById("spinner-back").classList.remove("show");
}
</script>

</html>