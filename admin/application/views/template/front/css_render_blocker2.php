<style>
    /*! CSS Used from: http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/bootstrap/css/bootstrap.min.css */
    :root {
        --blue: #007bff;
        --indigo: #6610f2;
        --purple: #6f42c1;
        --pink: #e83e8c;
        --red: #dc3545;
        --orange: #fd7e14;
        --yellow: #ffc107;
        --green: #28a745;
        --teal: #20c997;
        --cyan: #17a2b8;
        --white: #fff;
        --gray: #6c757d;
        --gray-dark: #343a40;
        --primary: #007bff;
        --secondary: #6c757d;
        --success: #28a745;
        --info: #17a2b8;
        --warning: #ffc107;
        --danger: #dc3545;
        --light: #f8f9fa;
        --dark: #343a40;
        --breakpoint-xs: 0;
        --breakpoint-sm: 576px;
        --breakpoint-md: 768px;
        --breakpoint-lg: 992px;
        --breakpoint-xl: 1200px;
        --font-family-sans-serif: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        --font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
    }

    *, ::after, ::before {
        box-sizing: border-box;
    }

    html {
        font-family: sans-serif;
        line-height: 1.15;
        -webkit-text-size-adjust: 100%;
        -webkit-tap-highlight-color: transparent;
    }

    footer, header, nav, section {
        display: block;
    }

    body {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        text-align: left;
        background-color: #fff;
    }

    [tabindex="-1"]:focus {
        outline: 0 !important;
    }

    h1, h2, h4, h5 {
        margin-top: 0;
        margin-bottom: .5rem;
    }

    p {
        margin-top: 0;
        margin-bottom: 1rem;
    }

    ul {
        margin-top: 0;
        margin-bottom: 1rem;
    }

    b, strong {
        font-weight: bolder;
    }

    a {
        color: #007bff;
        text-decoration: none;
        background-color: transparent;
    }

    a:hover {
        color: #0056b3;
        text-decoration: underline;
    }

    a:not([href]):not([tabindex]) {
        color: inherit;
        text-decoration: none;
    }

    a:not([href]):not([tabindex]):focus, a:not([href]):not([tabindex]):hover {
        color: inherit;
        text-decoration: none;
    }

    a:not([href]):not([tabindex]):focus {
        outline: 0;
    }

    img {
        vertical-align: middle;
        border-style: none;
    }

    svg {
        overflow: hidden;
        vertical-align: middle;
    }

    table {
        border-collapse: collapse;
    }

    th {
        text-align: inherit;
    }

    label {
        display: inline-block;
        margin-bottom: .5rem;
    }

    button {
        border-radius: 0;
    }

    button:focus {
        outline: 1px dotted;
        outline: 5px auto -webkit-focus-ring-color;
    }

    button, input, select {
        margin: 0;
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }

    button, input {
        overflow: visible;
    }

    button, select {
        text-transform: none;
    }

    select {
        word-wrap: normal;
    }

    [type=button], [type=submit], button {
        -webkit-appearance: button;
    }

    [type=button]::-moz-focus-inner, [type=submit]::-moz-focus-inner, button::-moz-focus-inner {
        padding: 0;
        border-style: none;
    }

    input[type=checkbox] {
        box-sizing: border-box;
        padding: 0;
    }

    [type=search] {
        outline-offset: -2px;
        -webkit-appearance: none;
    }

    h1, h2, h4, h5 {
        margin-bottom: .5rem;
        font-weight: 500;
        line-height: 1.2;
    }

    h1 {
        font-size: 2.5rem;
    }

    h2 {
        font-size: 2rem;
    }

    h4 {
        font-size: 1.5rem;
    }

    h5 {
        font-size: 1.25rem;
    }

    .img-fluid {
        max-width: 100%;
        height: auto;
    }

    .img-thumbnail {
        padding: .25rem;
        background-color: #fff;
        border: 1px solid #dee2e6;
        border-radius: .25rem;
        max-width: 100%;
        height: auto;
    }

    .container {
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }

    @media (min-width: 576px) {
        .container {
            max-width: 540px;
        }
    }

    @media (min-width: 768px) {
        .container {
            max-width: 720px;
        }
    }

    @media (min-width: 992px) {
        .container {
            max-width: 960px;
        }
    }

    @media (min-width: 1200px) {
        .container {
            max-width: 1140px;
        }
    }

    .container-fluid {
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }

    .row {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: -15px;
    }

    .col, .col-6, .col-lg-12, .col-lg-3, .col-lg-6, .col-lg-7, .col-md-3, .col-md-4, .col-md-6, .col-md-8, .col-md-9, .col-sm-3, .col-sm-6, .col-sm-9 {
        position: relative;
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
    }

    .col {
        -ms-flex-preferred-size: 0;
        flex-basis: 0;
        -ms-flex-positive: 1;
        flex-grow: 1;
        max-width: 100%;
    }

    .col-6 {
        -ms-flex: 0 0 50%;
        flex: 0 0 50%;
        max-width: 50%;
    }

    .order-2 {
        -ms-flex-order: 2;
        order: 2;
    }

    @media (min-width: 576px) {
        .col-sm-3 {
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-sm-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-sm-9 {
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }
    }

    @media (min-width: 768px) {
        .col-md-3 {
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-md-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }

        .col-md-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-md-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }

        .col-md-9 {
            -ms-flex: 0 0 75%;
            flex: 0 0 75%;
            max-width: 75%;
        }
    }

    @media (min-width: 992px) {
        .col-lg-3 {
            -ms-flex: 0 0 25%;
            flex: 0 0 25%;
            max-width: 25%;
        }

        .col-lg-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }

        .col-lg-7 {
            -ms-flex: 0 0 58.333333%;
            flex: 0 0 58.333333%;
            max-width: 58.333333%;
        }

        .col-lg-12 {
            -ms-flex: 0 0 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }

        .order-lg-1 {
            -ms-flex-order: 1;
            order: 1;
        }
    }

    .table {
        width: 100%;
        margin-bottom: 1rem;
        color: #212529;
    }

    .table td, .table th {
        padding: .75rem;
        vertical-align: top;
        border-top: 1px solid #dee2e6;
    }

    .table thead th {
        vertical-align: bottom;
        border-bottom: 2px solid #dee2e6;
    }

    .table-bordered {
        border: 1px solid #dee2e6;
    }

    .table-bordered td, .table-bordered th {
        border: 1px solid #dee2e6;
    }

    .table-bordered thead td, .table-bordered thead th {
        border-bottom-width: 2px;
    }

    .table-hover tbody tr:hover {
        color: #212529;
        background-color: rgba(0, 0, 0, .075);
    }

    .table-active {
        background-color: rgba(0, 0, 0, .075);
    }

    .table-hover .table-active:hover {
        background-color: rgba(0, 0, 0, .075);
    }

    .form-control {
        display: block;
        width: 100%;
        height: calc(1.5em + .75rem + 2px);
        padding: .375rem .75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }

    @media (prefers-reduced-motion: reduce) {
        .form-control {
            transition: none;
        }
    }

    .form-control::-ms-expand {
        background-color: transparent;
        border: 0;
    }

    .form-control:focus {
        color: #495057;
        background-color: #fff;
        border-color: #80bdff;
        outline: 0;
        box-shadow: 0 0 0 .2rem rgba(0, 123, 255, .25);
    }

    .form-control::-webkit-input-placeholder {
        color: #6c757d;
        opacity: 1;
    }

    .form-control::-moz-placeholder {
        color: #6c757d;
        opacity: 1;
    }

    .form-control:-ms-input-placeholder {
        color: #6c757d;
        opacity: 1;
    }

    .form-control::-ms-input-placeholder {
        color: #6c757d;
        opacity: 1;
    }

    .form-control::placeholder {
        color: #6c757d;
        opacity: 1;
    }

    .form-control:disabled {
        background-color: #e9ecef;
        opacity: 1;
    }

    .form-control-lg {
        height: calc(1.5em + 1rem + 2px);
        padding: .5rem 1rem;
        font-size: 1.25rem;
        line-height: 1.5;
        border-radius: .3rem;
    }

    .form-group {
        margin-bottom: 1rem;
    }

    .form-row {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -5px;
        margin-left: -5px;
    }

    .form-row > .col, .form-row > [class*=col-] {
        padding-right: 5px;
        padding-left: 5px;
    }

    .btn {
        display: inline-block;
        font-weight: 400;
        color: #212529;
        text-align: center;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-color: transparent;
        border: 1px solid transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }

    @media (prefers-reduced-motion: reduce) {
        .btn {
            transition: none;
        }
    }

    .btn:hover {
        color: #212529;
        text-decoration: none;
    }

    .btn:focus {
        outline: 0;
        box-shadow: 0 0 0 .2rem rgba(0, 123, 255, .25);
    }

    .btn:disabled {
        opacity: .65;
    }

    .btn-primary {
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
    }

    .btn-primary:hover {
        color: #fff;
        background-color: #0069d9;
        border-color: #0062cc;
    }

    .btn-primary:focus {
        box-shadow: 0 0 0 .2rem rgba(38, 143, 255, .5);
    }

    .btn-primary:disabled {
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
    }

    .btn-success {
        color: #fff;
        background-color: #28a745;
        border-color: #28a745;
    }

    .btn-success:hover {
        color: #fff;
        background-color: #218838;
        border-color: #1e7e34;
    }

    .btn-success:focus {
        box-shadow: 0 0 0 .2rem rgba(72, 180, 97, .5);
    }

    .btn-success:disabled {
        color: #fff;
        background-color: #28a745;
        border-color: #28a745;
    }

    .btn-danger {
        color: #fff;
        background-color: #dc3545;
        border-color: #dc3545;
    }

    .btn-danger:hover {
        color: #fff;
        background-color: #c82333;
        border-color: #bd2130;
    }

    .btn-danger:focus {
        box-shadow: 0 0 0 .2rem rgba(225, 83, 97, .5);
    }

    .btn-danger:disabled {
        color: #fff;
        background-color: #dc3545;
        border-color: #dc3545;
    }

    .btn-outline-dark {
        color: #343a40;
        border-color: #343a40;
    }

    .btn-outline-dark:hover {
        color: #fff;
        background-color: #343a40;
        border-color: #343a40;
    }

    .btn-outline-dark:focus {
        box-shadow: 0 0 0 .2rem rgba(52, 58, 64, .5);
    }

    .btn-outline-dark:disabled {
        color: #343a40;
        background-color: transparent;
    }

    .fade {
        transition: opacity .15s linear;
    }

    @media (prefers-reduced-motion: reduce) {
        .fade {
            transition: none;
        }
    }

    .fade:not(.show) {
        opacity: 0;
    }

    .collapse:not(.show) {
        display: none;
    }

    .dropdown {
        position: relative;
    }

    .dropdown-toggle {
        white-space: nowrap;
    }

    .dropdown-toggle::after {
        display: inline-block;
        margin-left: .255em;
        vertical-align: .255em;
        content: "";
        border-top: .3em solid;
        border-right: .3em solid transparent;
        border-bottom: 0;
        border-left: .3em solid transparent;
    }

    .dropdown-toggle:empty::after {
        margin-left: 0;
    }

    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 10rem;
        padding: .5rem 0;
        margin: .125rem 0 0;
        font-size: 1rem;
        color: #212529;
        text-align: left;
        list-style: none;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, .15);
        border-radius: .25rem;
    }

    .dropdown-item {
        display: block;
        width: 100%;
        padding: .25rem 1.5rem;
        clear: both;
        font-weight: 400;
        color: #212529;
        text-align: inherit;
        white-space: nowrap;
        background-color: transparent;
        border: 0;
    }

    .dropdown-item:focus, .dropdown-item:hover {
        color: #16181b;
        text-decoration: none;
        background-color: #f8f9fa;
    }

    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #007bff;
    }

    .dropdown-item:disabled {
        color: #6c757d;
        pointer-events: none;
        background-color: transparent;
    }

    .btn-group {
        position: relative;
        display: -ms-inline-flexbox;
        display: inline-flex;
        vertical-align: middle;
    }

    .btn-group > .btn {
        position: relative;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
    }

    .btn-group > .btn:hover {
        z-index: 1;
    }

    .btn-group > .btn:active, .btn-group > .btn:focus {
        z-index: 1;
    }

    .input-group {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -ms-flex-align: stretch;
        align-items: stretch;
        width: 100%;
    }

    .input-group > .form-control {
        position: relative;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        width: 1%;
        margin-bottom: 0;
    }

    .input-group > .form-control:focus {
        z-index: 3;
    }

    .input-group > .form-control:not(:last-child) {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }

    .input-group-append {
        display: -ms-flexbox;
        display: flex;
    }

    .input-group-append .btn {
        position: relative;
        z-index: 2;
    }

    .input-group-append .btn:focus {
        z-index: 3;
    }

    .input-group-append {
        margin-left: -1px;
    }

    .input-group > .input-group-append > .btn {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
    }

    .custom-control {
        position: relative;
        display: block;
        min-height: 1.5rem;
        padding-left: 1.5rem;
    }

    .custom-control-input {
        position: absolute;
        z-index: -1;
        opacity: 0;
    }

    .custom-control-input:checked ~ .custom-control-label::before {
        color: #fff;
        border-color: #007bff;
        background-color: #007bff;
    }

    .custom-control-input:focus ~ .custom-control-label::before {
        box-shadow: 0 0 0 .2rem rgba(0, 123, 255, .25);
    }

    .custom-control-input:disabled ~ .custom-control-label {
        color: #6c757d;
    }

    .custom-control-input:disabled ~ .custom-control-label::before {
        background-color: #e9ecef;
    }

    .custom-control-label {
        position: relative;
        margin-bottom: 0;
        vertical-align: top;
    }

    .custom-control-label::before {
        position: absolute;
        top: .25rem;
        left: -1.5rem;
        display: block;
        width: 1rem;
        height: 1rem;
        pointer-events: none;
        content: "";
        background-color: #fff;
        border: #adb5bd solid 1px;
    }

    .custom-control-label::after {
        position: absolute;
        top: .25rem;
        left: -1.5rem;
        display: block;
        width: 1rem;
        height: 1rem;
        content: "";
        background: no-repeat 50%/50% 50%;
    }

    .custom-checkbox .custom-control-label::before {
        border-radius: .25rem;
    }

    .custom-checkbox .custom-control-input:checked ~ .custom-control-label::after {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%23fff' d='M6.564.75l-3.59 3.612-1.538-1.55L0 4.26 2.974 7.25 8 2.193z'/%3e%3c/svg%3e");
    }

    .custom-checkbox .custom-control-input:disabled:checked ~ .custom-control-label::before {
        background-color: rgba(0, 123, 255, .5);
    }

    .custom-control-label::before {
        transition: background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    }

    @media (prefers-reduced-motion: reduce) {
        .custom-control-label::before {
            transition: none;
        }
    }

    .nav {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }

    .card {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, .125);
        border-radius: .25rem;
    }

    .card-body {
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.25rem;
    }

    .card-title {
        margin-bottom: .75rem;
    }

    .card-text:last-child {
        margin-bottom: 0;
    }

    .card-header {
        padding: .75rem 1.25rem;
        margin-bottom: 0;
        background-color: rgba(0, 0, 0, .03);
        border-bottom: 1px solid rgba(0, 0, 0, .125);
    }

    .card-header:first-child {
        border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
    }

    .card-footer {
        padding: .75rem 1.25rem;
        background-color: rgba(0, 0, 0, .03);
        border-top: 1px solid rgba(0, 0, 0, .125);
    }

    .card-footer:last-child {
        border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px);
    }

    .card-img-top {
        width: 100%;
        border-top-left-radius: calc(.25rem - 1px);
        border-top-right-radius: calc(.25rem - 1px);
    }

    .breadcrumb {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        padding: .75rem 1rem;
        margin-bottom: 1rem;
        list-style: none;
        background-color: #e9ecef;
        border-radius: .25rem;
    }

    .pagination {
        display: -ms-flexbox;
        display: flex;
        padding-left: 0;
        list-style: none;
        border-radius: .25rem;
    }

    .page-link {
        position: relative;
        display: block;
        padding: .5rem .75rem;
        margin-left: -1px;
        line-height: 1.25;
        color: #007bff;
        background-color: #fff;
        border: 1px solid #dee2e6;
    }

    .page-link:hover {
        z-index: 2;
        color: #0056b3;
        text-decoration: none;
        background-color: #e9ecef;
        border-color: #dee2e6;
    }

    .page-link:focus {
        z-index: 2;
        outline: 0;
        box-shadow: 0 0 0 .2rem rgba(0, 123, 255, .25);
    }

    .page-item:first-child .page-link {
        margin-left: 0;
        border-top-left-radius: .25rem;
        border-bottom-left-radius: .25rem;
    }

    .page-item:last-child .page-link {
        border-top-right-radius: .25rem;
        border-bottom-right-radius: .25rem;
    }

    .page-item.disabled .page-link {
        color: #6c757d;
        pointer-events: none;
        cursor: auto;
        background-color: #fff;
        border-color: #dee2e6;
    }

    .close {
        float: right;
        font-size: 1.5rem;
        font-weight: 700;
        line-height: 1;
        color: #000;
        text-shadow: 0 1px 0 #fff;
        opacity: .5;
    }

    .close:hover {
        color: #000;
        text-decoration: none;
    }

    button.close {
        padding: 0;
        background-color: transparent;
        border: 0;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
    }

    .modal {
        position: fixed;
        top: 0;
        left: 0;
        z-index: 1050;
        display: none;
        width: 100%;
        height: 100%;
        overflow: hidden;
        outline: 0;
    }

    .modal-dialog {
        position: relative;
        width: auto;
        margin: .5rem;
        pointer-events: none;
    }

    .modal.fade .modal-dialog {
        transition: -webkit-transform .3s ease-out;
        transition: transform .3s ease-out;
        transition: transform .3s ease-out, -webkit-transform .3s ease-out;
        -webkit-transform: translate(0, -50px);
        transform: translate(0, -50px);
    }

    @media (prefers-reduced-motion: reduce) {
        .modal.fade .modal-dialog {
            transition: none;
        }
    }
    .modal.show .modal-dialog {
        -webkit-transform: none;
        transform: none;
    }
    .modal-content {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        width: 100%;
        pointer-events: auto;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, .2);
        border-radius: .3rem;
        outline: 0;
    }

    .modal-header {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: start;
        align-items: flex-start;
        -ms-flex-pack: justify;
        justify-content: space-between;
        padding: 1rem 1rem;
        border-bottom: 1px solid #dee2e6;
        border-top-left-radius: .3rem;
        border-top-right-radius: .3rem;
    }

    .modal-header .close {
        padding: 1rem 1rem;
        margin: -1rem -1rem -1rem auto;
    }

    .modal-title {
        margin-bottom: 0;
        line-height: 1.5;
    }

    .modal-body {
        position: relative;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1rem;
    }

    .modal-footer {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-pack: end;
        justify-content: flex-end;
        padding: 1rem;
        border-top: 1px solid #dee2e6;
        border-bottom-right-radius: .3rem;
        border-bottom-left-radius: .3rem;
    }

    .modal-footer > :not(:first-child) {
        margin-left: .25rem;
    }

    .modal-footer > :not(:last-child) {
        margin-right: .25rem;
    }

    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 500px;
            margin: 1.75rem auto;
        }
    }

    .border-0 {
        border: 0 !important;
    }

    .border-success {
        border-color: #28a745 !important;
    }

    .rounded {
        border-radius: .25rem !important;
    }

    .d-block {
        display: block !important;
    }

    .d-flex {
        display: -ms-flexbox !important;
        display: flex !important;
    }

    .justify-content-end {
        -ms-flex-pack: end !important;
        justify-content: flex-end !important;
    }

    .justify-content-center {
        -ms-flex-pack: center !important;
        justify-content: center !important;
    }

    .align-items-center {
        -ms-flex-align: center !important;
        align-items: center !important;
    }

    @media (min-width: 992px) {
        .justify-content-lg-start {
            -ms-flex-pack: start !important;
            justify-content: flex-start !important;
        }
    }

    .float-right {
        float: right !important;
    }

    .m-0 {
        margin: 0 !important;
    }

    .mb-0 {
        margin-bottom: 0 !important;
    }

    .mb-1 {
        margin-bottom: .25rem !important;
    }

    .mt-2 {
        margin-top: .5rem !important;
    }

    .mb-2 {
        margin-bottom: .5rem !important;
    }

    .my-3 {
        margin-top: 1rem !important;
    }

    .mb-3, .my-3 {
        margin-bottom: 1rem !important;
    }

    .mt-4, .my-4 {
        margin-top: 1.5rem !important;
    }

    .mb-4, .my-4 {
        margin-bottom: 1.5rem !important;
    }

    .mt-5 {
        margin-top: 3rem !important;
    }

    .mb-5 {
        margin-bottom: 3rem !important;
    }

    .p-0 {
        padding: 0 !important;
    }

    .pb-1 {
        padding-bottom: .25rem !important;
    }

    .pt-2, .py-2 {
        padding-top: .5rem !important;
    }

    .py-2 {
        padding-bottom: .5rem !important;
    }

    .pl-2 {
        padding-left: .5rem !important;
    }

    .pt-3 {
        padding-top: 1rem !important;
    }

    .py-4 {
        padding-top: 1.5rem !important;
    }

    .px-4 {
        padding-right: 1.5rem !important;
    }

    .py-4 {
        padding-bottom: 1.5rem !important;
    }

    .px-4 {
        padding-left: 1.5rem !important;
    }

    .py-5 {
        padding-top: 3rem !important;
    }

    .pb-5, .py-5 {
        padding-bottom: 3rem !important;
    }

    .mx-auto {
        margin-right: auto !important;
    }

    .mx-auto {
        margin-left: auto !important;
    }

    @media (min-width: 992px) {
        .mb-lg-0 {
            margin-bottom: 0 !important;
        }
    }

    .text-justify {
        text-align: justify !important;
    }

    .text-left {
        text-align: left !important;
    }

    .text-center {
        text-align: center !important;
    }

    .text-uppercase {
        text-transform: uppercase !important;
    }

    .font-weight-normal {
        font-weight: 400 !important;
    }

    .font-weight-bold {
        font-weight: 700 !important;
    }

    .text-warning {
        color: #ffc107 !important;
    }

    .text-dark {
        color: #343a40 !important;
    }

    .visible {
        visibility: visible !important;
    }

    @media print {
        *, ::after, ::before {
            text-shadow: none !important;
            box-shadow: none !important;
        }

        a:not(.btn) {
            text-decoration: underline;
        }

        thead {
            display: table-header-group;
        }

        img, tr {
            page-break-inside: avoid;
        }

        h2, p {
            orphans: 3;
            widows: 3;
        }

        h2 {
            page-break-after: avoid;
        }

        body {
            min-width: 992px !important;
        }

        .container {
            min-width: 992px !important;
        }

        .table {
            border-collapse: collapse !important;
        }

        .table td, .table th {
            background-color: #fff !important;
        }

        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6 !important;
        }
    }

    /*! CSS Used from: http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/css/all.min.css */
    .fa, .fab, .far, .fas {
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased;
        display: inline-block;
        font-style: normal;
        font-variant: normal;
        text-rendering: auto;
        line-height: 1;
    }

    .fa-angle-right:before {
        content: "\f105";
    }

    .fa-bars:before {
        content: "\f0c9";
    }

    .fa-bullhorn:before {
        content: "\f0a1";
    }

    .fa-calendar-alt:before {
        content: "\f073";
    }

    .fa-chevron-down:before {
        content: "\f078";
    }

    .fa-chevron-left:before {
        content: "\f053";
    }

    .fa-chevron-right:before {
        content: "\f054";
    }

    .fa-chevron-up:before {
        content: "\f077";
    }

    .fa-dot-circle:before {
        content: "\f192";
    }

    .fa-envelope:before {
        content: "\f0e0";
    }

    .fa-eraser:before {
        content: "\f12d";
    }

    .fa-eye-slash:before {
        content: "\f070";
    }

    .fa-facebook-f:before {
        content: "\f39e";
    }

    .fa-filter:before {
        content: "\f0b0";
    }

    .fa-instagram:before {
        content: "\f16d";
    }

    .fa-list-alt:before {
        content: "\f022";
    }

    .fa-phone:before {
        content: "\f095";
    }

    .fa-plus:before {
        content: "\f067";
    }

    .fa-step-backward:before {
        content: "\f048";
    }

    .fa-step-forward:before {
        content: "\f051";
    }

    .fa-twitter:before {
        content: "\f099";
    }

    .fa-unlink:before {
        content: "\f127";
    }

    .fab {
        font-family: "Font Awesome 5 Brands";
    }

    .far {
        font-weight: 400;
    }

    .fa, .far, .fas {
        font-family: "Font Awesome 5 Free";
    }

    .fa, .fas {
        font-weight: 900;
    }

    .fa-eye:before {
        content: "\f06e";
    }

    .fa-eye-slash:before {
        content: "\f070";
    }

    /*! CSS Used from: http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/animate/animate.min.css */
    .bounce {
        -webkit-animation-name: bounce;
        -webkit-transform-origin: center bottom;
        animation-name: bounce;
        transform-origin: center bottom;
    }

    .animated {
        -webkit-animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-duration: 1s;
        animation-fill-mode: both;
    }

    @media (prefers-reduced-motion) {
        .animated {
            -webkit-animation: unset !important;
            -webkit-transition: none !important;
            animation: unset !important;
            transition: none !important;
        }
    }

    /*! CSS Used from: http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/owl.carousel/assets/owl.carousel.min.css */
    .owl-carousel,.owl-carousel .owl-item{-webkit-tap-highlight-color:transparent;position:relative}.owl-carousel{display:none;width:100%;z-index:1}.owl-carousel .owl-stage{position:relative;-ms-touch-action:pan-Y;touch-action:manipulation;-moz-backface-visibility:hidden}.owl-carousel .owl-stage:after{content:".";display:block;clear:both;visibility:hidden;line-height:0;height:0}.owl-carousel .owl-stage-outer{position:relative;overflow:hidden;-webkit-transform:translate3d(0,0,0)}.owl-carousel .owl-item,.owl-carousel .owl-wrapper{-webkit-backface-visibility:hidden;-moz-backface-visibility:hidden;-ms-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0);-moz-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0)}.owl-carousel .owl-item{min-height:1px;float:left;-webkit-backface-visibility:hidden;-webkit-touch-callout:none}.owl-carousel .owl-item img{display:block;width:100%}.owl-carousel .owl-dots.disabled,.owl-carousel .owl-nav.disabled{display:none}.no-js .owl-carousel,.owl-carousel.owl-loaded{display:block}.owl-carousel .owl-dot,.owl-carousel .owl-nav .owl-next,.owl-carousel .owl-nav .owl-prev{cursor:pointer;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.owl-carousel .owl-nav button.owl-next,.owl-carousel .owl-nav button.owl-prev,.owl-carousel button.owl-dot{background:0 0;color:inherit;border:none;padding:0!important;font:inherit}.owl-carousel.owl-loading{opacity:0;display:block}.owl-carousel.owl-hidden{opacity:0}.owl-carousel.owl-refresh .owl-item{visibility:hidden}.owl-carousel.owl-drag .owl-item{-ms-touch-action:pan-y;touch-action:pan-y;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.owl-carousel.owl-grab{cursor:move;cursor:grab}.owl-carousel.owl-rtl{direction:rtl}.owl-carousel.owl-rtl .owl-item{float:right}.owl-carousel .animated{animation-duration:1s;animation-fill-mode:both}.owl-carousel .owl-animated-in{z-index:0}.owl-carousel .owl-animated-out{z-index:1}.owl-carousel .fadeOut{animation-name:fadeOut}@keyframes fadeOut{0%{opacity:1}100%{opacity:0}}.owl-height{transition:height .5s ease-in-out}.owl-carousel .owl-item .owl-lazy{opacity:0;transition:opacity .4s ease}.owl-carousel .owl-item .owl-lazy:not([src]),.owl-carousel .owl-item .owl-lazy[src^=""]{max-height:0}.owl-carousel .owl-item img.owl-lazy{transform-style:preserve-3d}.owl-carousel .owl-video-wrapper{position:relative;height:100%;background:#000}.owl-carousel .owl-video-play-icon{position:absolute;height:80px;width:80px;left:50%;top:50%;margin-left:-40px;margin-top:-40px;background:url(owl.video.play.png) no-repeat;cursor:pointer;z-index:1;-webkit-backface-visibility:hidden;transition:transform .1s ease}.owl-carousel .owl-video-play-icon:hover{-ms-transform:scale(1.3,1.3);transform:scale(1.3,1.3)}.owl-carousel .owl-video-playing .owl-video-play-icon,.owl-carousel .owl-video-playing .owl-video-tn{display:none}.owl-carousel .owl-video-tn{opacity:0;height:100%;background-position:center center;background-repeat:no-repeat;background-size:contain;transition:opacity .4s ease}.owl-carousel .owl-video-frame{position:relative;z-index:1;height:100%;width:100%}

    /*! CSS Used from: http://project/FREE/pmb-feeder-pusdiktan/public/portal/css/theme.min.css */
    html {
        direction: ltr;
        overflow-x: hidden;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
        -webkit-font-smoothing: antialiased;
    }

    body {
        background-color: #FFF;
        color: #777;
        font-family: "Open Sans", Arial, sans-serif;
        font-size: 14px;
        line-height: 26px;
        margin: 0;
    }

    body a {
        outline: none !important;
    }

    li {
        line-height: 24px;
    }

    @media (max-width: 575px) {
        body {
            font-size: 13px;
        }
    }

    #header {
        position: relative;
        z-index: 1030;
    }

    #header .header-body {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        background: #FFF;
        -webkit-transition: min-height 0.3s ease;
        transition: min-height 0.3s ease;
        width: 100%;
        border-top: 3px solid #EDEDED;
        border-bottom: 1px solid transparent;
        z-index: 1001;
    }

    #header .header-container {
        position: relative;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-transition: ease height 300ms;
        transition: ease height 300ms;
    }

    #header .container {
        position: relative;
    }

    @media (max-width: 767px) {
        #header .container {
            width: 100%;
        }
    }

    #header .header-row {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-flex: 1;
        -ms-flex-positive: 1;
        flex-grow: 1;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-item-align: stretch;
        align-self: stretch;
        max-height: 100%;
    }

    #header .header-column {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-item-align: stretch;
        align-self: stretch;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-flex: 1;
        -ms-flex-positive: 1;
        flex-grow: 1;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
    }

    #header .header-column .header-row {
        -webkit-box-pack: inherit;
        -ms-flex-pack: inherit;
        justify-content: inherit;
    }

    #header .header-logo {
        margin: 16px 0;
        margin: 1rem 0;
        position: relative;
        z-index: 1;
    }

    #header .header-logo img {
        -webkit-transition: all 0.3s ease;
        transition: all 0.3s ease;
        position: relative;
        top: 0;
    }

    #header .header-nav {
        padding: 16px 0;
        padding: 1rem 0;
        min-height: 70px;
    }

    #header .header-btn-collapse-nav {
        background: #CCC;
        color: #FFF;
        display: none;
        float: right;
        margin: 0 0 0 16px;
        margin: 0 0 0 1rem;
    }

    #header.header-effect-shrink {
        -webkit-transition: ease height 300ms;
        transition: ease height 300ms;
    }

    #header.header-effect-shrink .header-container {
        min-height: 100px;
    }

    #header.header-effect-shrink .header-logo {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    html.sticky-header-active #header .header-body {
        position: fixed;
        border-bottom-color: rgba(234, 234, 234, 0.5);
        -webkit-box-shadow: 0 0 3px rgba(234, 234, 234, 0.5);
        box-shadow: 0 0 3px rgba(234, 234, 234, 0.5);
    }

    html.sticky-header-active #header.header-effect-shrink .header-body {
        position: relative;
    }

    @media (min-width: 992px) {
        #header .header-nav-main {
            display: -webkit-box !important;
            display: -ms-flexbox !important;
            display: flex !important;
            height: auto !important;
        }

        #header .header-nav-main nav {
            display: -webkit-box !important;
            display: -ms-flexbox !important;
            display: flex !important;
        }

        #header .header-nav-main nav > ul > li {
            height: 100%;
            -ms-flex-item-align: stretch;
            align-self: stretch;
            margin-left: 2px;
        }

        #header .header-nav-main nav > ul > li > a {
            display: -webkit-inline-box;
            display: -ms-inline-flexbox;
            display: inline-flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            white-space: normal;
            border-radius: 4px;
            color: #CCC;
            font-size: 12px;
            font-style: normal;
            font-weight: 700;
            padding: 0.5rem 1rem;
            letter-spacing: -0.5px;
            text-transform: uppercase;
        }

        #header .header-nav-main nav > ul > li > a:after {
            display: none;
        }

        #header .header-nav-main nav > ul > li > a:active {
            background-color: transparent;
            text-decoration: none;
            color: #CCC;
        }

        #header .header-nav-main nav > ul > li > a.active {
            background-color: transparent;
        }

        #header .header-nav-main nav > ul > li:hover > a {
            background: #CCC;
            color: #FFF;
        }

        #header .header-nav-main nav > ul > li.dropdown:hover > a {
            border-radius: 4px 4px 0 0;
            position: relative;
        }

        #header .header-nav-main nav > ul > li.dropdown:hover > a:before {
            content: '';
            display: block;
            position: absolute;
            left: 0;
            right: 0;
            bottom: -3px;
            border-bottom: 5px solid #CCC;
        }

        #header .header-nav-main.header-nav-main-square nav > ul > li > a {
            border-radius: 0 !important;
        }
    }

    @media (min-width: 992px) {
        #header .header-nav {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-flex: 1;
            -ms-flex-positive: 1;
            flex-grow: 1;
            -webkit-box-pack: end;
            -ms-flex-pack: end;
            justify-content: flex-end;
            -ms-flex-item-align: stretch;
            align-self: stretch;
        }

        #header .header-nav.header-nav-links {
            padding: 0;
        }

        #header .header-nav.header-nav-links .header-nav-main {
            -ms-flex-item-align: stretch;
            align-self: stretch;
            min-height: 0;
            margin-top: 0;
        }

        #header .header-nav.header-nav-links nav > ul > li > a, #header .header-nav.header-nav-links nav > ul > li:hover > a {
            position: relative;
            background: transparent !important;
            padding: 0 .9rem;
            margin: 1px 0 0;
            min-height: 60px;
            height: 100%;
        }

        #header .header-nav.header-nav-links nav > ul > li:hover > a:before {
            opacity: 1;
        }

        #header .header-nav.header-nav-links nav > ul > li > a.active {
            background: transparent;
        }

        #header .header-nav.header-nav-links nav > ul > li > a.active:before {
            opacity: 1;
        }

        #header .header-nav.header-nav-links nav > ul > li.dropdown > a:before {
            border-bottom: 0;
        }

        #header .header-nav.header-nav-links:not(.header-nav-light-text) nav > ul > li > a {
            color: #444;
        }
    }

    @media (max-width: 991px) {
        #header .header-nav-main {
            position: absolute;
            background: transparent;
            width: 100%;
            top: 100%;
            left: 50%;
            -webkit-transform: translate3d(-50%, 0, 0);
            transform: translate3d(-50%, 0, 0);
        }

        #header .header-nav-main:before {
            content: '';
            display: block;
            position: absolute;
            top: 0;
            left: 50%;
            width: 100vw;
            height: 100%;
            background: #FFF;
            z-index: -1;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%);
        }

        #header .header-nav-main nav {
            max-height: 50vh;
            overflow: hidden;
            overflow-y: auto;
            padding: 0 15px;
            -webkit-transition: ease all 500ms;
            transition: ease all 500ms;
        }

        #header .header-nav-main nav::-webkit-scrollbar {
            width: 5px;
        }

        #header .header-nav-main nav::-webkit-scrollbar-thumb {
            border-radius: 0px;
            background: rgba(204, 204, 204, 0.5);
        }

        #header .header-nav-main nav > ul {
            padding-top: 15px;
            padding-bottom: 15px;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        #header .header-nav-main nav > ul li {
            border-bottom: 1px solid #e8e8e8;
            clear: both;
            display: block;
            float: none;
            margin: 0;
            padding: 0;
            position: relative;
        }

        #header .header-nav-main nav > ul li a {
            font-size: 13px;
            font-style: normal;
            line-height: 20px;
            padding: 7px 8px;
            margin: 1px 0;
            border-radius: 4px;
            text-align: left;
        }

        #header .header-nav-main nav > ul li a:active {
            background-color: #f8f9fa;
            color: inherit;
        }

        #header .header-nav-main nav > ul li:last-child {
            border-bottom: 0;
        }

        #header .header-nav-main nav > ul > li > a {
            text-transform: uppercase;
            font-weight: 700;
            margin-top: 1px;
            margin-bottom: 1px;
            color: #CCC;
        }

        #header .header-nav-main nav > ul > li > a:active {
            color: #CCC;
        }

        #header .header-nav-main nav > ul > li > a.active {
            color: #FFF !important;
            background: #CCC;
        }

        #header .header-nav-main nav > ul > li > a.active:focus, #header .header-nav-main nav > ul > li > a.active:hover {
            color: #FFF;
            background: #CCC;
        }

        #header .header-nav-main.header-nav-main-square nav > ul > li a {
            border-radius: 0 !important;
        }

        #header .header-btn-collapse-nav {
            outline: 0;
            display: block;
            position: relative;
            z-index: 1;
        }

        #header .header-nav.header-nav-links {
            min-height: 70px;
        }
    }

    @media (min-width: 992px) {
        #header .header-body:not(.h-100) {
            height: auto !important;
        }
    }

    @media (max-width: 991px) {
        #header .header-logo img {
            z-index: 1;
        }

        #header .header-nav {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }
    }

    .page-header {
        background-color: #212529;
        margin: 0 0 35px 0;
        padding: 30px 0;
        position: relative;
        text-align: left;
    }

    .page-header .breadcrumb {
        background: none;
        margin: 0;
        padding: 0;
        position: relative;
        z-index: 1;
    }

    .page-header .breadcrumb > li {
        display: inline-block;
        font-size: 0.8em;
        text-transform: uppercase;
        text-shadow: none;
    }

    .page-header .breadcrumb > li + li:before {
        color: inherit;
        opacity: 0.5;
        font-family: 'Font Awesome 5 Free';
        font-weight: 900;
        content: "\f105";
        padding: 0 7px 0 5px;
    }

    .page-header h1 {
        color: #fff;
        display: inline-block;
        font-size: 30px;
        line-height: 1;
        margin: 0;
        padding: 0;
        font-weight: 400;
        position: relative;
        top: 1px;
    }

    .page-header.page-header-classic:after {
        content: '';
        width: 100%;
        height: 5px;
        background: rgba(255, 255, 255, 0.8);
        position: absolute;
        bottom: 0;
        left: 0;
    }

    .page-header.page-header-classic .page-header-title-border {
        width: 0;
        height: 5px;
        position: absolute;
        bottom: 0;
        background: transparent;
        z-index: 1;
    }

    .p-relative {
        position: relative !important;
    }

    .p-static {
        position: static !important;
    }

    .negative-ls-1 {
        letter-spacing: -1px;
    }

    .cur-pointer {
        cursor: pointer;
    }

    .text-1 {
        font-size: .8em !important;
    }

    .text-2 {
        font-size: .9em !important;
    }

    .text-3 {
        font-size: 1em !important;
    }

    .text-4 {
        font-size: 1.2em !important;
    }

    .text-7 {
        font-size: 2em !important;
    }

    .text-8 {
        font-size: 2.30em !important;
    }

    .text-9 {
        font-size: 2.50em !important;
    }

    .line-height-2 {
        line-height: 1.2 !important;
    }

    .top-1 {
        top: 1px !important;
    }

    .top-8 {
        top: 8px !important;
    }

    .text-uppercase {
        text-transform: uppercase !important;
    }

    .text-dark {
        color: #212529 !important;
    }

    .font-weight-normal {
        font-weight: 400 !important;
    }

    .font-weight-semibold {
        font-weight: 600 !important;
    }

    .font-weight-bold, b, strong {
        font-weight: 700 !important;
    }

    .font-weight-extra-bold {
        font-weight: 900 !important;
    }

    .rounded {
        border-radius: 5px !important;
    }

    #footer {
        background: #212529;
        border-top: 4px solid #212529;
        font-size: 0.9em;
        margin-top: 50px;
        padding: 0;
        position: relative;
        clear: both;
    }

    #footer .footer-ribbon {
        background: #999;
        position: absolute;
        margin: -44px 0 0 0;
        padding: 10px 20px 6px 20px;
    }

    #footer .footer-ribbon:before {
        border-right: 10px solid #646464;
        border-top: 16px solid transparent;
        content: "";
        display: block;
        height: 0;
        right: 100%;
        position: absolute;
        top: 0;
        width: 7px;
    }

    #footer .footer-ribbon span {
        color: #FFF;
        font-size: 1.6em;
        font-family: "Shadows Into Light", cursive;
    }

    #footer h5 {
        color: #FFF;
    }

    #footer a:not(.btn) {
        color: #777;
        -webkit-transition: all 0.1s ease-in-out;
        transition: all 0.1s ease-in-out;
    }

    #footer a:not(.btn):hover {
        text-decoration: none;
        color: #FFF;
    }

    #footer a:not(.btn):focus, #footer a:not(.btn):active {
        color: #CCC;
    }

    #footer a:not(.btn).text-color-light {
        color: #FFF !important;
    }

    #footer a:not(.btn).text-color-light:hover {
        color: #e6e6e6 !important;
    }

    #footer a:not(.btn).text-color-light:focus, #footer a:not(.btn).text-color-light:active {
        color: #cccccc !important;
    }

    #footer a:not(.btn).link-hover-style-1 {
        position: relative;
        left: 0px;
        -webkit-transition: all 0.1s ease-in-out;
        transition: all 0.1s ease-in-out;
    }

    #footer a:not(.btn).link-hover-style-1:hover {
        left: 3px;
    }

    #footer .footer-copyright {
        background: #1c2023;
    }

    #footer .footer-copyright p {
        color: #555;
        margin: 0;
        padding: 0;
        font-size: 0.9em;
    }

    /*! CSS Used from: http://project/FREE/pmb-feeder-pusdiktan/public/portal/css/theme-elements.min.css */
    .appear-animation {
        opacity: 0;
    }

    .appear-animation-visible {
        opacity: 1;
    }

    .fadeInUpShorter {
        -webkit-animation-name: fadeInUpShorter;
        animation-name: fadeInUpShorter;
    }

    .bg-color-white {
        background: #FFF !important;
    }

    .btn {
        font-size: 12.8px;
        font-size: 0.8rem;
        padding: 8.528px 14.928px;
        padding: 0.533rem 0.933rem;
        cursor: pointer;
    }

    html body .btn:focus, html body .btn:active {
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
    }

    .btn + .dropdown-menu .dropdown-item {
        font-size: 12.8px;
        font-size: 0.8rem;
    }

    .btn-default {
        color: #333;
        background-color: #fff;
        border-color: #ccc;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
    }

    .btn-default:hover, .btn-default:active, .btn-default:focus {
        color: #333;
        background-color: #e6e6e6;
        border-color: #adadad;
    }

    .btn-modern {
        text-transform: uppercase;
        font-size: .8em;
        padding: 12.8px 24px;
        padding: 0.8rem 1.5rem;
        font-weight: 600;
    }

    .btn.dropdown-toggle:after {
        margin-left: .155em;
        vertical-align: .155em;
    }

    .btn-primary {
        background-color: #CCC;
        border-color: #CCC #CCC #b3b3b3;
        color: #FFF;
    }

    .btn-primary:hover {
        background-color: #dfdfdf;
        border-color: #e6e6e6 #e6e6e6 #CCC;
        color: #FFF;
    }

    .btn-primary:focus {
        -webkit-box-shadow: 0 0 0 3px rgba(204, 204, 204, 0.5);
        box-shadow: 0 0 0 3px rgba(204, 204, 204, 0.5);
    }

    .btn-primary:disabled {
        background-color: #CCC;
        border-color: #CCC #CCC #b3b3b3;
    }

    .btn-primary:active {
        background-color: #b9b9b9;
        background-image: none;
        border-color: #b3b3b3 #b3b3b3 #999999;
    }

    .btn-success {
        background-color: #28a745;
        border-color: #28a745 #28a745 #1e7e34;
        color: #FFF;
    }

    .btn-success:hover {
        background-color: #2fc652;
        border-color: #34ce57 #34ce57 #28a745;
        color: #FFF;
    }

    .btn-success:focus {
        -webkit-box-shadow: 0 0 0 3px rgba(40, 167, 69, 0.5);
        box-shadow: 0 0 0 3px rgba(40, 167, 69, 0.5);
    }

    .btn-success:disabled {
        background-color: #28a745;
        border-color: #28a745 #28a745 #1e7e34;
    }

    .btn-success:active {
        background-color: #218838;
        background-image: none;
        border-color: #1e7e34 #1e7e34 #145523;
    }

    .btn-danger {
        background-color: #dc3545;
        border-color: #dc3545 #dc3545 #bd2130;
        color: #FFF;
    }

    .btn-danger:hover {
        background-color: #e25663;
        border-color: #e4606d #e4606d #dc3545;
        color: #FFF;
    }

    .btn-danger:focus {
        -webkit-box-shadow: 0 0 0 3px rgba(220, 53, 69, 0.5);
        box-shadow: 0 0 0 3px rgba(220, 53, 69, 0.5);
    }

    .btn-danger:disabled {
        background-color: #dc3545;
        border-color: #dc3545 #dc3545 #bd2130;
    }

    .btn-danger:active {
        background-color: #c82333;
        background-image: none;
        border-color: #bd2130 #bd2130 #921925;
    }

    .card {
        position: relative;
        border: 1px solid rgba(0, 0, 0, 0.06);
    }

    .card-header {
        border-bottom: 1px solid rgba(0, 0, 0, 0.06);
    }

    .card-footer {
        border-top: 1px solid rgba(0, 0, 0, 0.06);
    }

    .card-body {
        padding: 32px;
        padding: 2rem;
    }

    .owl-carousel {
        margin-bottom: 20px;
    }

    .owl-carousel .owl-item img {
        -webkit-transform-style: unset;
        transform-style: unset;
    }

    .owl-carousel .owl-nav {
        top: 50%;
        position: absolute;
        width: 100%;
        margin-top: 0;
        -webkit-transform: translate3d(0, -50%, 0);
        transform: translate3d(0, -50%, 0);
    }

    .owl-carousel .owl-nav button.owl-prev, .owl-carousel .owl-nav button.owl-next {
        display: inline-block;
        position: absolute;
        top: 50%;
        width: 30px;
        height: 30px;
        outline: 0;
        margin: 0;
        -webkit-transform: translate3d(0, -50%, 0);
        transform: translate3d(0, -50%, 0);
    }

    .owl-carousel .owl-nav button.owl-prev {
        left: 0;
    }

    .owl-carousel .owl-nav button.owl-prev:before {
        font-family: 'Font Awesome 5 Free';
        font-weight: 900;
        font-size: 8px;
        font-size: 0.5rem;
        content: "\f053";
        position: relative;
        left: -1px;
        top: -1px;
    }

    .owl-carousel .owl-nav button.owl-next {
        right: 0;
    }

    .owl-carousel .owl-nav button.owl-next:before {
        font-family: 'Font Awesome 5 Free';
        font-weight: 900;
        font-size: 8px;
        font-size: 0.5rem;
        content: "\f054";
        position: relative;
        left: 1px;
        top: -1px;
    }

    .owl-carousel.show-nav-hover .owl-nav {
        opacity: 0;
        -webkit-transition: opacity 0.2s ease-in-out;
        transition: opacity 0.2s ease-in-out;
    }

    .owl-carousel.show-nav-hover .owl-nav button.owl-prev {
        left: -15px;
    }

    .owl-carousel.show-nav-hover .owl-nav button.owl-next {
        right: -15px;
    }

    .owl-carousel.show-nav-hover:hover .owl-nav {
        opacity: 1;
    }

    .owl-carousel .owl-dots .owl-dot {
        outline: 0;
    }

    .owl-carousel .owl-dots .owl-dot span {
        width: 8px;
        height: 8px;
        margin: 5px 4px;
    }

    .divider {
        border: 0;
        height: 1px;
        margin: 44px auto;
        background: rgba(0, 0, 0, 0.06);
        text-align: center;
        position: relative;
        clear: both;
    }

    .divider [class*="fa-"] {
        text-align: center;
        background: #FFF;
        border-radius: 50px;
        color: #a9a9a9;
        display: inline-block;
        height: 50px;
        line-height: 50px;
        position: absolute;
        text-align: center;
        width: 50px;
        font-size: 20px;
        margin: 0 auto 0 -25px;
        top: -25px;
        left: 50%;
        z-index: 1;
    }

    .divider.divider-solid {
        background: rgba(0, 0, 0, 0.06);
    }

    .divider.divider-style-4 [class*="fa-"] {
        border: 1px solid #CECECE;
    }

    .divider.divider-style-4 [class*="fa-"]:after {
        border: 3px solid #f7f7f7;
        border-radius: 50%;
        -webkit-box-sizing: content-box;
        box-sizing: content-box;
        content: "";
        display: block;
        height: 100%;
        left: -4px;
        padding: 1px;
        position: absolute;
        top: -4px;
        width: 100%;
    }

    .featured-box {
        background: #FFF;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        border-bottom: 1px solid #DFDFDF;
        border-left: 1px solid #ECECEC;
        border-radius: 8px;
        border-right: 1px solid #ECECEC;
        -webkit-box-shadow: 0 2px 4px 0px rgba(0, 0, 0, 0.05);
        box-shadow: 0 2px 4px 0px rgba(0, 0, 0, 0.05);
        margin-bottom: 20px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 20px;
        min-height: 100px;
        position: relative;
        text-align: center;
        z-index: 1;
    }

    .featured-box h4 {
        font-size: 1.3em;
        font-weight: 400;
        letter-spacing: -0.7px;
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .featured-box .box-content {
        border-radius: 8px;
        border-top: 1px solid rgba(0, 0, 0, 0.06);
        border-top-width: 4px;
        padding: 28.8px;
        padding: 1.8rem;
        position: relative;
    }

    .featured-box .box-content:not(.box-content-border-0) {
        top: -1px;
        border-top-width: 4px;
    }

    .featured-boxes .featured-box {
        margin-bottom: 24px;
        margin-bottom: 1.5rem;
        margin-top: 24px;
        margin-top: 1.5rem;
    }

    form label {
        font-weight: normal;
    }

    select {
        border: 1px solid #E5E7E9;
        border-radius: 6px;
        outline: none;
    }

    .form-group:after {
        content: ".";
        display: block;
        clear: both;
        visibility: hidden;
        line-height: 0;
        height: 0;
    }

    .form-control {
        border-color: rgba(0, 0, 0, 0.09);
    }

    .form-control:not(.form-control-lg) {
        font-size: 12px;
        font-size: 0.75rem;
        line-height: 1.3;
    }

    .form-control:not(.form-control-sm):not(.form-control-lg) {
        font-size: 13.6px;
        font-size: 0.85rem;
        line-height: 1.85;
    }

    .form-control:focus {
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        border-color: #CCC;
    }

    .page-link {
        border-color: rgba(0, 0, 0, 0.06);
    }

    .input-group .form-control {
        height: auto;
    }

    input[type="text"], input[type="password"], input[type="number"], input[type="search"] {
        -webkit-appearance: none;
    }

    .form-control::-webkit-input-placeholder, input[type="text"]::-webkit-input-placeholder, input[type="password"]::-webkit-input-placeholder, input[type="number"]::-webkit-input-placeholder, input[type="search"]::-webkit-input-placeholder {
        color: #bdbdbd;
    }

    .form-control::-moz-placeholder, input[type="text"]::-moz-placeholder, input[type="password"]::-moz-placeholder, input[type="number"]::-moz-placeholder, input[type="search"]::-moz-placeholder {
        color: #bdbdbd;
    }

    .form-control:-ms-input-placeholder, input[type="text"]:-ms-input-placeholder, input[type="password"]:-ms-input-placeholder, input[type="number"]:-ms-input-placeholder, input[type="search"]:-ms-input-placeholder {
        color: #bdbdbd;
    }

    h1, h2, h4, h5 {
        color: #212529;
        font-weight: 200;
        letter-spacing: -.05em;
        margin: 0;
        -webkit-font-smoothing: antialiased;
    }

    h1 {
        font-size: 2.6em;
        line-height: 44px;
        margin: 0 0 32px 0;
    }

    h2 {
        font-size: 2.2em;
        font-weight: 300;
        line-height: 42px;
        margin: 0 0 32px 0;
    }

    h4 {
        font-size: 1.4em;
        font-weight: 600;
        line-height: 27px;
        margin: 0 0 14px 0;
    }

    h5 {
        font-size: 1em;
        font-weight: 600;
        line-height: 18px;
        margin: 0 0 14px 0;
        text-transform: uppercase;
    }

    @media (max-width: 575px) {
        h2 {
            line-height: 40px;
        }
    }

    .img-thumbnail {
        border-radius: 4px;
        position: relative;
    }

    .img-thumbnail img {
        border-radius: 4px;
    }

    .img-thumbnail.img-thumbnail-no-borders {
        border: none;
        padding: 0;
    }

    .list li {
        margin-bottom: 13px;
    }

    .list.list-icons {
        list-style: none;
        padding-left: 0;
        padding-right: 0;
    }

    .list.list-icons li {
        position: relative;
        padding-left: 25px;
    }

    .list.list-icons li > [class*="fa-"]:first-child {
        position: absolute;
        left: 0;
        top: 5px;
    }

    .list.list-icons.list-icons-sm li {
        padding-left: 13px;
        margin-bottom: 5px;
    }

    .list.list-icons.list-icons-sm li > [class*="fa-"]:first-child {
        font-size: 0.8em;
        top: 7px;
    }

    .list.list-icons.list-icons-lg li {
        padding-top: 5px;
        padding-left: 27px;
    }

    .list.list-icons.list-icons-lg li > [class*="fa-"]:first-child {
        font-size: 1.3em;
        top: 10px;
    }

    .loading-overlay {
        -webkit-transition: visibility 0s ease-in-out 0.5s, opacity 0.5s ease-in-out;
        transition: visibility 0s ease-in-out 0.5s, opacity 0.5s ease-in-out;
        bottom: 0;
        left: 0;
        position: absolute;
        opacity: 0;
        right: 0;
        top: 0;
        visibility: hidden;
        background: #FFF;
    }

    body > .loading-overlay {
        position: fixed;
        z-index: 999999;
    }

    .bounce-loader {
        -webkit-transition: all 0.2s;
        transition: all 0.2s;
        margin: -9px 0 0 -35px;
        text-align: center;
        width: 70px;
        left: 50%;
        position: absolute;
        top: 50%;
        z-index: 10000;
    }

    .bounce-loader .bounce1, .bounce-loader .bounce2, .bounce-loader .bounce3 {
        -webkit-animation: 1.4s ease-in-out 0s normal both infinite bouncedelay;
        animation: 1.4s ease-in-out 0s normal both infinite bouncedelay;
        background-color: #CCC;
        border-radius: 100%;
        -webkit-box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.15);
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.15);
        display: inline-block;
        height: 18px;
        width: 18px;
    }

    .bounce-loader .bounce1 {
        -webkit-animation-delay: -0.32s;
        animation-delay: -0.32s;
    }

    .bounce-loader .bounce2 {
        -webkit-animation-delay: -0.16s;
        animation-delay: -0.16s;
    }

    .box-shadow-1:before {
        display: block;
        position: absolute;
        left: 1%;
        top: 1%;
        height: 98%;
        width: 98%;
        opacity: 0.33;
        content: '';
        -webkit-box-shadow: 0 30px 90px #BBB;
        box-shadow: 0 30px 90px #BBB;
        -webkit-transition: all 0.2s ease-in-out;
        transition: all 0.2s ease-in-out;
    }

    .pagination {
        position: relative;
        z-index: 1;
    }

    .pagination > li > a, .pagination > li > span, .pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
        color: #CCC;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
    }

    .border-radius-0 {
        border-radius: 0 !important;
    }

    body[data-plugin-page-transition] {
        -webkit-transition: ease opacity 300ms;
        transition: ease opacity 300ms;
    }

    .modal-footer > :not(:first-child) {
        margin-left: 4px;
        margin-left: .25rem;
        margin-right: 0px;
        margin-right: 0rem;
    }

    .modal-footer > :not(:last-child) {
        margin-right: 4px;
        margin-right: .25rem;
        margin-left: 0px;
        margin-left: 0rem;
    }

    .nav > li > a:hover, .nav > li > a:focus {
        background: transparent;
    }

    ul.nav-pills > li > a.active {
        color: #FFF;
        background-color: #CCC;
    }

    ul.nav-pills > li > a.active:hover, ul.nav-pills > li > a.active:focus {
        color: #FFF;
        background-color: #CCC;
    }

    html .scroll-to-top {
        -webkit-transition: opacity 0.3s;
        transition: opacity 0.3s;
        background: #404040;
        border-radius: 4px 4px 0 0;
        bottom: 0;
        color: #FFF;
        display: block;
        height: 9px;
        opacity: 0;
        padding: 10px 10px 35px;
        position: fixed;
        right: 10px;
        text-align: center;
        text-decoration: none;
        min-width: 50px;
        z-index: 1040;
        font-size: 0.8em;
    }

    html .scroll-to-top:hover {
        opacity: 1;
    }

    html .scroll-to-top.visible {
        opacity: 0.75;
    }

    @media (max-width: 991px) {
        html .scroll-to-top.hidden-mobile {
            display: none !important;
        }
    }

    section.section {
        background: #f7f7f7;
        border-top: 5px solid #f1f1f1;
        margin: 30px 0;
        padding: 50px 0;
    }

    section.section.section-with-divider {
        margin: 56px 0 35px;
    }

    section.section.section-with-divider .divider {
        margin: -56px 0 44px;
    }

    .slider-container {
        background: #151719;
        height: 500px;
        overflow: hidden;
        width: 100%;
        direction: ltr;
    }

    .slider-container .tparrows {
        border-radius: 6px;
    }

    .slider-container .slider-single-slide .tparrows {
        display: none;
    }

    .slider-container .tp-opacity-overlay {
        background: #000;
        opacity: 0.75;
    }

    .rev_slider video {
        border: 0 !important;
    }

    .social-icons {
        margin: 0;
        padding: 0;
        width: auto;
    }

    .social-icons li {
        display: inline-block;
        margin: -1px 1px 0 0;
        padding: 0;
        border-radius: 100%;
        overflow: visible;
    }

    .social-icons li a {
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
        border-radius: 100%;
        display: block;
        height: 28px;
        line-height: 28px;
        width: 28px;
        text-align: center;
        color: #333 !important;
        text-decoration: none;
        font-size: 12.8px;
        font-size: 0.8rem;
    }

    .social-icons li:hover a {
        background: #151719;
        color: #FFF !important;
    }

    .social-icons li:hover.social-icons-twitter a {
        background: #1aa9e1;
    }

    .social-icons li:hover.social-icons-facebook a {
        background: #3b5a9a;
    }

    .social-icons li:hover.social-icons-linkedin a {
        background: #0073b2;
    }

    .social-icons:not(.social-icons-clean):not(.social-icons-dark):not(.social-icons-dark-2) li {
        -webkit-box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.2);
        box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.2);
    }

    .social-icons:not(.social-icons-clean):not(.social-icons-dark):not(.social-icons-dark-2) li a {
        background: #FFF;
    }

    .table td, .table th {
        border-color: rgba(0, 0, 0, 0.06);
    }

    p {
        color: #777;
        line-height: 26px;
        margin: 0 0 20px;
    }

    a, a:hover, a:focus {
        color: #CCC;
    }

    /*! CSS Used from: http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/rs-plugin/css/settings.css */
    [class^=revicon-]:before {
        font-family: revicons;
        font-style: normal;
        font-weight: 400;
        speak: none;
        display: inline-block;
        text-decoration: inherit;
        width: 1em;
        margin-right: .2em;
        text-align: center;
        font-variant: normal;
        text-transform: none;
        line-height: 1em;
        margin-left: .2em;
    }

    .revicon-right-dir:before {
        content: '\e818';
    }

    .rev_slider_wrapper {
        position: relative;
        z-index: 0;
        width: 100%;
    }

    .rev_slider {
        position: relative;
        overflow: visible;
    }

    .rev_slider > ul, .rev_slider > ul > li, .rev_slider > ul > li:before, .tp-revslider-mainul > li, .tp-revslider-mainul > li:before, .tp-simpleresponsive > ul, .tp-simpleresponsive > ul > li, .tp-simpleresponsive > ul > li:before {
        list-style: none !important;
        position: absolute;
        margin: 0 !important;
        padding: 0 !important;
        overflow-x: visible;
        overflow-y: visible;
        background-image: none;
        background-position: 0 0;
        text-indent: 0;
        top: 0;
        left: 0;
    }

    .rev_slider > ul > li, .rev_slider > ul > li:before, .tp-revslider-mainul > li, .tp-revslider-mainul > li:before, .tp-simpleresponsive > ul > li, .tp-simpleresponsive > ul > li:before {
        visibility: hidden;
    }

    .tp-revslider-mainul, .tp-revslider-slidesli {
        padding: 0 !important;
        margin: 0 !important;
        list-style: none !important;
    }

    .rev_slider li.tp-revslider-slidesli {
        position: absolute !important;
    }

    .rev_slider .tp-caption {
        position: relative;
        visibility: hidden;
        white-space: nowrap;
        display: block;
        -webkit-font-smoothing: antialiased !important;
        z-index: 1;
    }

    .rev_slider .tp-caption {
        -moz-user-select: none;
        -khtml-user-select: none;
        -webkit-user-select: none;
        -o-user-select: none;
    }

    .rev_slider .tp-mask-wrap .tp-caption, .rev_slider .tp-mask-wrap :last-child {
        margin-bottom: 0;
    }

    .rev_slider video {
        max-width: none !important;
    }

    .tp-parallax-wrap {
        transform-style: preserve-3d;
    }

    .tp-video-play-button, .tp-video-play-button i {
        line-height: 50px !important;
        vertical-align: top;
        text-align: center;
    }

    .rs-background-video-layer {
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        position: absolute;
    }

    .rs-background-video-layer {
        visibility: hidden;
        z-index: 0;
    }

    .tp-video-play-button {
        background: #000;
        background: rgba(0, 0, 0, .3);
        border-radius: 5px;
        position: absolute;
        top: 50%;
        left: 50%;
        color: #FFF;
        margin-top: -25px;
        margin-left: -25px;
        cursor: pointer;
        width: 50px;
        height: 50px;
        box-sizing: border-box;
        display: inline-block;
        z-index: 4;
        opacity: 0;
        transition: opacity .3s ease-out !important;
    }

    .tp-video-play-button i {
        width: 50px;
        height: 50px;
        display: inline-block;
        font-size: 40px !important;
    }

    .rs-fullvideo-cover, .tp-dottedoverlay {
        height: 100%;
        top: 0;
        left: 0;
        position: absolute;
    }

    .videoisplaying .revicon-right-dir {
        display: none;
    }

    .videoisplaying .tp-revstop {
        display: inline-block;
    }

    .videoisplaying .tp-video-play-button {
        display: none;
    }

    .rs-fullvideo-cover {
        width: 100%;
        background: 0 0;
        z-index: 5;
    }

    .tp-dottedoverlay {
        background-repeat: repeat;
        width: 100%;
        z-index: 3;
    }

    .tp-bannertimer {
        visibility: hidden;
        width: 100%;
        height: 5px;
        background: #000;
        background: rgba(0, 0, 0, .15);
        position: absolute;
        z-index: 200;
        top: 0;
    }

    .tp-loader {
        top: 50%;
        left: 50%;
        z-index: 10000;
        position: absolute;
    }

    .tp-loader.spinner3 {
        margin: -9px 0 0 -35px;
        width: 70px;
        text-align: center;
    }

    .tp-loader.spinner3 .bounce1, .tp-loader.spinner3 .bounce2, .tp-loader.spinner3 .bounce3 {
        width: 18px;
        height: 18px;
        background-color: #fff;
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, .15);
        border-radius: 100%;
        display: inline-block;
        animation: tp-bouncedelay 1.4s infinite ease-in-out;
        animation-fill-mode: both;
    }

    .tp-loader.spinner3 .bounce1 {
        animation-delay: -.32s;
    }

    .tp-loader.spinner3 .bounce2 {
        animation-delay: -.16s;
    }

    .tparrows {
        cursor: pointer;
        background: #000;
        background: rgba(0, 0, 0, .5);
        width: 40px;
        height: 40px;
        position: absolute;
        display: block;
        z-index: 1000;
    }

    .tparrows:hover {
        background: #000;
    }

    .tparrows:before {
        font-family: revicons;
        font-size: 15px;
        color: #fff;
        display: block;
        line-height: 40px;
        text-align: center;
    }

    .tparrows.tp-leftarrow:before {
        content: '\e824';
    }

    .tparrows.tp-rightarrow:before {
        content: '\e825';
    }

    /*! CSS Used from: http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/rs-plugin/css/layers.css */
    * {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .rev_slider {
        overflow: hidden;
    }

    .rev_slider {
        overflow: hidden;
    }

    /*! CSS Used from: http://project/FREE/pmb-feeder-pusdiktan/public/portal/css/skins/skin-cat.min.css */
    a {
        color: #2d5f3c;
    }

    a:hover {
        color: #0099e6;
    }

    a:focus {
        color: #0099e6;
    }

    a:active {
        color: #0077b3;
    }

    html .text-dark {
        color: #212529 !important;
    }

    html .text-color-light {
        color: #FFF !important;
    }

    html .bg-color-light {
        background-color: #FFF !important;
    }

    html .btn-primary {
        background-color: #2d5f3c;
        border-color: #2d5f3c #2d5f3c #006699;
        color: #FFF;
    }

    html .btn-primary:hover {
        background-color: #00a1f2;
        border-color: #00aaff #00aaff #2d5f3c;
        color: #FFF;
    }

    html .btn-primary:focus {
        -webkit-box-shadow: 0 0 0 3px rgba(0, 136, 204, 0.5);
        box-shadow: 0 0 0 3px rgba(0, 136, 204, 0.5);
    }

    html .btn-primary:disabled {
        background-color: #2d5f3c;
        border-color: #2d5f3c #2d5f3c #006699;
    }

    html .btn-primary:active {
        background-color: #006ea6 !important;
        background-image: none !important;
        border-color: #006699 #006699 #004466 !important;
    }

    .pagination > li > a, .pagination > li > span, .pagination > li > a:hover, .pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus {
        color: #2d5f3c;
    }

    .custom-control-input:checked ~ .custom-control-label::before, .custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
        background-color: #2d5f3c;
    }

    html section.section-primary {
        background-color: #2d5f3c !important;
        border-color: #0077b3 !important;
    }

    html section.section-primary h5 {
        color: #FFF;
    }

    html .bg-color-light-scale-1 {
        background-color: #f7f7f7 !important;
    }

    html .section.bg-color-light-scale-1 {
        border-top-color: #efefef !important;
    }

    section.page-header .page-header-title-border {
        background-color: #2d5f3c !important;
    }

    .owl-carousel .owl-dots .owl-dot.active span, .owl-carousel .owl-dots .owl-dot:hover span {
        background-color: #0074ad;
    }

    .owl-carousel .owl-nav button[class*="owl-"] {
        background-color: #2d5f3c;
        border-color: #2d5f3c #2d5f3c #006699;
        color: #FFF;
    }

    .owl-carousel .owl-nav button[class*="owl-"]:hover {
        background-color: #00a1f2;
        border-color: #00aaff #00aaff #2d5f3c;
    }

    .owl-carousel .owl-nav button[class*="owl-"]:active {
        background-color: #006ea6;
        background-image: none;
        border-color: #006699 #006699 #004466;
    }

    .list.list-icons li > [class*="fa-"]:first-child {
        color: #fff;
        border-color: #fff;
    }

    #header .header-btn-collapse-nav {
        background: #2d5f3c;
    }

    @media (min-width: 992px) {
        #header .header-nav-main nav > ul > li > a {
            color: #2d5f3c;
        }

        #header .header-nav-main nav > ul > li:hover > a {
            background: #2d5f3c;
        }

        #header .header-nav-main nav > ul > li.dropdown:hover > a:before {
            border-bottom-color: #2d5f3c;
        }

        #header .header-nav.header-nav-links nav > ul li:hover > a {
            color: #2d5f3c;
        }
    }

    @media (max-width: 991px) {
        #header .header-nav-main:not(.header-nav-main-mobile-dark) nav > ul > li > a {
            color: #2d5f3c;
        }

        #header .header-nav-main:not(.header-nav-main-mobile-dark) nav > ul > li > a:active {
            color: #2d5f3c;
        }

        #header .header-nav-main:not(.header-nav-main-mobile-dark) nav > ul > li > a.active {
            background: #2d5f3c;
        }

        #header .header-nav-main:not(.header-nav-main-mobile-dark) nav > ul > li > a.active:focus, #header .header-nav-main:not(.header-nav-main-mobile-dark) nav > ul > li > a.active:hover {
            background: #2d5f3c;
        }
    }

    html .featured-box-primary h4 {
        color: #2d5f3c;
    }

    html .featured-box-primary .box-content {
        border-top-color: #2d5f3c;
    }

    .slider .tp-bannertimer {
        background-color: #2d5f3c;
    }

    #footer .footer-ribbon {
        background: #2d5f3c;
    }

    #footer .footer-ribbon:before {
        border-right-color: #4b8a5e;
        border-left-color: #4b8a5e;
    }

    #footer {
        background-color: #2d5f3c;
        border-top: 4px solid #1d502c;
        margin-top: 0;
    }

    #footer li, #footer a:not(.btn), #footer p {
        color: #fff;
    }

    #footer .footer-copyright {
        background-color: #1b3823 !important;
    }

    #footer .footer-copyright p {
        color: #fff;
    }

    html .scroll-to-top {
        background: #1b3823;
    }

    .page-header {
        background-color: #2d5f3c;
    }

    .breadcrumb a {
        color: #fff;
    }

    /*! CSS Used from: http://project/FREE/pmb-feeder-pusdiktan/public/portal/css/custom.css */
    li.active {
        color: #ffe103;
    }

    .card-img-top {
        width: 100%;
        height: auto;
    }

    .page-header.page-header-classic .page-header-title-border {
        width: 0 !important;
    }


    /*! CSS Used keyframes */
    @-webkit-keyframes bounce {
        0%, 20%, 53%, 80%, to {
            -webkit-animation-timing-function: cubic-bezier(.215, .61, .355, 1);
            -webkit-transform: translateZ(0);
            animation-timing-function: cubic-bezier(.215, .61, .355, 1);
            transform: translateZ(0);
        }
        40%, 43% {
            -webkit-animation-timing-function: cubic-bezier(.755, .05, .855, .06);
            -webkit-transform: translate3d(0, -30px, 0);
            animation-timing-function: cubic-bezier(.755, .05, .855, .06);
            transform: translate3d(0, -30px, 0);
        }
        70% {
            -webkit-animation-timing-function: cubic-bezier(.755, .05, .855, .06);
            -webkit-transform: translate3d(0, -15px, 0);
            animation-timing-function: cubic-bezier(.755, .05, .855, .06);
            transform: translate3d(0, -15px, 0);
        }
        90% {
            -webkit-transform: translate3d(0, -4px, 0);
            transform: translate3d(0, -4px, 0);
        }
    }

    @keyframes bounce {
        0%, 20%, 53%, 80%, to {
            -webkit-animation-timing-function: cubic-bezier(.215, .61, .355, 1);
            -webkit-transform: translateZ(0);
            animation-timing-function: cubic-bezier(.215, .61, .355, 1);
            transform: translateZ(0);
        }
        40%, 43% {
            -webkit-animation-timing-function: cubic-bezier(.755, .05, .855, .06);
            -webkit-transform: translate3d(0, -30px, 0);
            animation-timing-function: cubic-bezier(.755, .05, .855, .06);
            transform: translate3d(0, -30px, 0);
        }
        70% {
            -webkit-animation-timing-function: cubic-bezier(.755, .05, .855, .06);
            -webkit-transform: translate3d(0, -15px, 0);
            animation-timing-function: cubic-bezier(.755, .05, .855, .06);
            transform: translate3d(0, -15px, 0);
        }
        90% {
            -webkit-transform: translate3d(0, -4px, 0);
            transform: translate3d(0, -4px, 0);
        }
    }

    @-webkit-keyframes fadeInUpShorter {
        from {
            opacity: 0;
            -webkit-transform: translate(0, 50px);
            transform: translate(0, 50px);
        }
        to {
            opacity: 1;
            -webkit-transform: none;
            transform: none;
        }
    }

    @keyframes fadeInUpShorter {
        from {
            opacity: 0;
            -webkit-transform: translate(0, 50px);
            transform: translate(0, 50px);
        }
        to {
            opacity: 1;
            -webkit-transform: none;
            transform: none;
        }
    }

    @keyframes tp-bouncedelay {
        0%, 100%, 80% {
            transform: scale(0);
        }
        40% {
            transform: scale(1);
        }
    }

    /*! CSS Used fontfaces */
    @font-face {
        font-family: "Font Awesome 5 Brands";
        font-style: normal;
        font-weight: normal;
        font-display: auto;
        src: url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-brands-400.eot);
        src: url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-brands-400.eot#iefix) format("embedded-opentype"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-brands-400.woff2) format("woff2"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-brands-400.woff) format("woff"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-brands-400.ttf) format("truetype"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-brands-400.svg#fontawesome) format("svg");
    }

    @font-face {
        font-family: "Font Awesome 5 Free";
        font-style: normal;
        font-weight: 400;
        font-display: auto;
        src: url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-regular-400.eot);
        src: url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-regular-400.eot#iefix) format("embedded-opentype"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-regular-400.woff2) format("woff2"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-regular-400.woff) format("woff"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-regular-400.ttf) format("truetype"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-regular-400.svg#fontawesome) format("svg");
    }

    @font-face {
        font-family: "Font Awesome 5 Free";
        font-style: normal;
        font-weight: 900;
        font-display: auto;
        src: url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-solid-900.eot);
        src: url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-solid-900.eot#iefix) format("embedded-opentype"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-solid-900.woff2) format("woff2"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-solid-900.woff) format("woff"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-solid-900.ttf) format("truetype"), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/fontawesome-free/webfonts/fa-solid-900.svg#fontawesome) format("svg");
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 300;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN_r8OX-hpOqc.woff2) format('woff2');
        unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 300;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN_r8OVuhpOqc.woff2) format('woff2');
        unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 300;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN_r8OXuhpOqc.woff2) format('woff2');
        unicode-range: U+1F00-1FFF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 300;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN_r8OUehpOqc.woff2) format('woff2');
        unicode-range: U+0370-03FF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 300;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN_r8OXehpOqc.woff2) format('woff2');
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 300;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN_r8OXOhpOqc.woff2) format('woff2');
        unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 300;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN_r8OUuhp.woff2) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFWJ0bbck.woff2) format('woff2');
        unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFUZ0bbck.woff2) format('woff2');
        unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFWZ0bbck.woff2) format('woff2');
        unicode-range: U+1F00-1FFF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFVp0bbck.woff2) format('woff2');
        unicode-range: U+0370-03FF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFWp0bbck.woff2) format('woff2');
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFW50bbck.woff2) format('woff2');
        unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-UFVZ0b.woff2) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UNirkOX-hpOqc.woff2) format('woff2');
        unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UNirkOVuhpOqc.woff2) format('woff2');
        unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UNirkOXuhpOqc.woff2) format('woff2');
        unicode-range: U+1F00-1FFF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UNirkOUehpOqc.woff2) format('woff2');
        unicode-range: U+0370-03FF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UNirkOXehpOqc.woff2) format('woff2');
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UNirkOXOhpOqc.woff2) format('woff2');
        unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UNirkOUuhp.woff2) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 700;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN7rgOX-hpOqc.woff2) format('woff2');
        unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 700;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN7rgOVuhpOqc.woff2) format('woff2');
        unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 700;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN7rgOXuhpOqc.woff2) format('woff2');
        unicode-range: U+1F00-1FFF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 700;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN7rgOUehpOqc.woff2) format('woff2');
        unicode-range: U+0370-03FF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 700;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN7rgOXehpOqc.woff2) format('woff2');
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 700;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN7rgOXOhpOqc.woff2) format('woff2');
        unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 700;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN7rgOUuhp.woff2) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 800;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN8rsOX-hpOqc.woff2) format('woff2');
        unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 800;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN8rsOVuhpOqc.woff2) format('woff2');
        unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 800;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN8rsOXuhpOqc.woff2) format('woff2');
        unicode-range: U+1F00-1FFF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 800;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN8rsOUehpOqc.woff2) format('woff2');
        unicode-range: U+0370-03FF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 800;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN8rsOXehpOqc.woff2) format('woff2');
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 800;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN8rsOXOhpOqc.woff2) format('woff2');
        unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 800;
        src: url(https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN8rsOUuhp.woff2) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: 'Shadows Into Light';
        font-style: normal;
        font-weight: 400;
        src: url(https://fonts.gstatic.com/s/shadowsintolight/v10/UqyNK9UOIntux_czAvDQx_ZcHqZXBNQzdcD5.woff2) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    @font-face {
        font-family: revicons;
        src: url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/rs-plugin/fonts/revicons/revicons.eot?5510888);
        src: url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/rs-plugin/fonts/revicons/revicons.eot?5510888#iefix) format('embedded-opentype'), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/rs-plugin/fonts/revicons/revicons.woff?5510888) format('woff'), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/rs-plugin/fonts/revicons/revicons.ttf?5510888) format('truetype'), url(http://project/FREE/pmb-feeder-pusdiktan/public/portal/vendor/rs-plugin/fonts/revicons/revicons.svg?5510888#revicons) format('svg');
        font-weight: 400;
        font-style: normal;
    }
    .card-img-top {
        width: 100%;
        height: auto;
    }
</style
<?php

if (isset($output->css_files)) {
    foreach ($output->css_files as $file) {
        echo '<link type="text/css" rel="stylesheet" href="' . $file . '"/>';
    }
}