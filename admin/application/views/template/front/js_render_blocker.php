<script>
    var jsFiles = [
        "<?php echo base_url('public/portal'); ?>/vendor/jquery/jquery-3.6.0.min.js",
        "<?php echo base_url('public/portal'); ?>/vendor/jquery.appear/jquery.appear.min.js",
        "<?php echo base_url('public/portal'); ?>/vendor/jquery.easing/jquery.easing.min.js",
        "<?php echo base_url('public/portal'); ?>/vendor/bootstrap/js/bootstrap.min.js",
        "<?php echo base_url('public/portal'); ?>/vendor/common/common.min.js",
        "<?php echo base_url('public/portal'); ?>/vendor/owl.carousel/owl.carousel.min.js",
        "<?php echo base_url('public/portal'); ?>/vendor/rs-plugin/js/jquery.themepunch.tools.min.js",
        "<?php echo base_url('public/portal'); ?>/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js",
        "<?php echo base_url('public/portal'); ?>/vendor/jquery.countdown/jquery.countdown.min.js",
        "<?php echo base_url('public/portal'); ?>/js/theme.min.js",
        "<?php echo base_url('public/portal'); ?>/js/theme.init.min.js",
        "<?php echo base_url('public/portal'); ?>/js/custom.js",
        "https://www.google.com/recaptcha/api.js"
    ];
    <?php

    if (isset($output->js_files)) {
        foreach ($output->js_files as $file) {
            echo 'jsFiles.push("' . $file . '");';
        }
    }
    ?>
</script>
<script type="text/javascript">
    jsFiles.forEach(function (src) {
        var script = document.createElement('script');
        script.src = src;
        script.async = false;
        document.head.appendChild(script);
    });

</script>