  <!-- Vendor JS Files -->
  <script src="<?php echo base_url() ?>public/vendor/datatables/datatables.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/datepicker/moment.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/aos/aos.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/select2/select2.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/datepicker/daterangepicker.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/jquery-mask/jquery.mask.min.js"></script>

  <!-- ninescroll -->
  <script src="<?php echo base_url() ?>public/vendor/jquery.nicescroll-3.7.6/jquery.nicescroll.min.js"></script>

  <!-- izitoast -->
  <script src="<?php echo base_url('public/vendor/iziToast/dist/js') ?>/iziToast.min.js"></script>

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Template JS File -->
  <script src="<?php echo base_url() ?>public/assets/stisla/js/stisla.js"></script>
  <script src="<?php echo base_url() ?>public/assets/stisla/js/scripts.js"></script>
  <script src="<?php echo base_url() ?>public/assets/stisla/js/custom.js"></script>
  <script src="<?php echo base_url() ?>public/vendor/sweetalert2/sweetalert2.js"></script>
  <script type="text/javascript">
    var site_url = '<?php echo site_url() ?>';
    var Toast;
    $(function() {
      Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
      });
    })
  </script>

  <script>
    $('.select2').select2({
      theme: 'bootstrap5',
    });

    $('.select2').on('select2:opening', function(e) {
      if ($(this).attr('readonly') == 'readonly') {
        e.preventDefault();
        $(this).select2('close');
        return false;
      }
    });

    /**
     * datepicker lahir
     */
    $('.datepicker-lahir').datepicker({
      startDate: '-100y',
      endDate: '-17y',
      format: 'yyyy-mm-dd',
      language: 'id',
      daysOfWeekHighlighted: "0",
      autoclose: true,
      todayHighlight: true
    });

    /**
     * format ipk 0.00 maks 4.00
     */
    $('.ipk').mask('0.00', {
      reverse: true,
      min: 0,
      max: 4
    });

    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
      return true;
    }

    function showLoading() {
      document.getElementById("spinner-front").classList.add("show");
      document.getElementById("spinner-back").classList.add("show");
    }

    function hideLoading() {
      document.getElementById("spinner-front").classList.remove("show");
      document.getElementById("spinner-back").classList.remove("show");
    }

    /**
     * show password
     */
    $('.show-password').click(function(e) {
      if ('password' == $('[name="password"]').attr('type')) {
        $('[name="password"]').prop('type', 'text');
        $('.show-password').html('<i class="fas fa-eye text-secondary">');
      } else {
        $('[name="password"]').prop('type', 'password');
        $('.show-password').html('<i class="fas fa-eye-slash text-secondary">');
      }
    });

    /**
     * show confirm password
     */
    $('.show-confirm-password').click(function(e) {
      if ('password' == $('[name="confirm_password"]').attr('type')) {
        $('[name="confirm_password"]').prop('type', 'text');
        $('.show-confirm-password').html('<i class="fas fa-eye text-secondary">');
      } else {
        $('[name="confirm_password"]').prop('type', 'password');
        $('.show-confirm-password').html('<i class="fas fa-eye-slash text-secondary">');
      }
    });

    /**
     * only number
     */
    $('.number-only').keyup(function(e) {
      if (/\D/g.test(this.value)) {
        // Filter non-digits from input value.
        this.value = this.value.replace(/\D/g, '');
      }
    });

    /**
     * only alpha
     */
    $('.alpha-only').bind('keydown', function(e) {
      if (e.altKey) {
        e.preventDefault();
      } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
          e.preventDefault();
        }
      }
    });

    /**
     * Ubah Foto
     */
    function uploadFoto(name, file) {
      var upload = $('[name=new_upload_' + name + ']').val();
      $('[name=' + file + ']').val('');
      $('#preview-' + name).html('');
      $('.file-info-' + name).removeClass('text-info').removeClass('text-success').removeClass('text-danger').removeClass('text-muted').html('Tidak ada berkas yang dipilih');
      if (upload == '1') {
        $('#new-upload-' + name).removeClass('d-none');
        $('[name=new_upload_' + name + ']').val('0');
        $('#is_berkas').val("1");
      } else {
        $('#new-upload-' + name).addClass('d-none');
        $('[name=new_upload_' + name + ']').val('1');
        $('#is_berkas').val("0");
      }
    }

    /**
     * Cek Upload File
     */
    function cekFile(input, name) {
      var limit = 2048576; // 2 MB
      var file_info = $('.file-info-' + name);
      $(file_info).removeClass('text-info').removeClass('text-danger').removeClass('text-muted').html('');
      if (input.files && input.files[0]) {
        var filesize = input.files[0].size;
        var filetype = (input.files[0].name).split('.').pop().toLowerCase();
        if (input.name == 'url_photo') {
          var allowed = ['png', 'jpg', 'jpeg'];
          var sizePreview = ['250px', '250px'];
        } else {
          var allowed = ['png', 'jpg', 'jpeg', 'pdf'];
          var sizePreview = ['100%', '800'];
        }
        if (filesize < limit && allowed.includes(filetype)) {
          var reader = new FileReader();
          reader.readAsDataURL(input.files[0]);

          if (filetype == 'pdf') {
            $('#preview-' + name).html('<embed src="' + URL.createObjectURL(input.files[0]) + '" class="mt-4" width="' + sizePreview[0] + '" height="' + sizePreview[1] + '" />')
          } else {
            $('#preview-' + name).html('<img src="' + URL.createObjectURL(input.files[0]) + '" class="mt-4" alt="your image" style="width: ' + sizePreview[0] + '"/>')
          }

          $(file_info).addClass('text-success').html(input.files[0].name + ' (' + getFileSize(filesize) + ') <i class="fa fa-circle-check text-success"></i>');
        } else {
          $(input).val('');
          $('#preview-' + name).html('');
          if (allowed.includes(filetype) === false)
            $(file_info).addClass('text-danger').html("<i>Jenis file '" + filetype.toUpperCase() + "' tidak diijinkan. </i>")
          if (filesize > limit)
            $(file_info).addClass('text-danger').html('<i>Ukuran file tidak boleh lebih dari 2 MB</i>')
        }

      } else {
        $(input).val('');
        $('#preview-' + name).html('');
        $(file_info).addClass('text-muted').html("Tidak ada berkas yang dipilih")
      }
    }

    function getFileSize(_size) {
      var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
        i = 0;
      while (_size > 900) {
        _size /= 1024;
        i++;
      }
      var exactSize = (Math.round(_size * 100) / 100) + ' ' + fSExt[i];
      return exactSize;
    }

    function imageRequired(status) {
      if (status.checked === true) document.getElementById('file_image').setAttribute('required', '');
      else document.getElementById('file_image').removeAttribute('required');
    }
  </script>
  <?php
  echo '<script>';
  if (isset($scripts) && is_array($scripts)) {
    foreach ($scripts as $script) {
      $this->load->view($script);
    }
  }
  echo '</script>';
  ?>