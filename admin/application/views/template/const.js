let baseUrl = site_url
let apiBaseUrl = `${baseUrl}api`
let provinceJson = `${baseUrl}public/json/provinsi.json`
let mapBoxToken = "pk.eyJ1IjoicmllenEyNSIsImEiOiJjanZvaTh6cnQxeXVzNDRxcm5ya2h6ZndhIn0.v7YbJRGuet1_dLKB3sDdHQ"

let mapBoxStyle = "mapbox://styles/riezq25/ckwelm8dj58b115od4qwpdk87"

let urlApiNIK = `${baseUrl}api/auth/nik`
let urlCekDataMis = `${baseUrl}api/auth/mis`
let urlCekUserMis = `${baseUrl}api/auth/cekmis`

let lightGreen = "#3ab50d"