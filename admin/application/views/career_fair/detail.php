<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-sm borderless">
                    <tr>
                        <th>Nama Career Fair</th>
                        <th>:</th>
                        <td><?php echo $data['data']->nama_job_fair ?></td>
                    </tr>
                    <tr>
                        <th>Tempat Pelaksanaan</th>
                        <th>:</th>
                        <td><?php echo $data['data']->tempat ?></td>
                    </tr>
                    <tr>
                        <th>Perguruan Tinggi Pelaksaan</th>
                        <th>:</th>
                        <td><?php echo $data['data']->nama_resmi ?></td>
                    </tr>
                    <tr>
                        <th>Tanggal Pendaftaran</th>
                        <th>:</th>
                        <td>
                            <?php echo $this->tanggalindo->konversi($data['data']->waktu_awal_pendaftaran).' - '. $this->tanggalindo->konversi($data['data']->tanggal_akhir_pendaftaran) ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Pelaksanaan</th>
                        <th>:</th>
                        <td>
                            <?php echo $this->tanggalindo->konversi($data['data']->waktu_awal_pelaksanaan).' - '. $this->tanggalindo->konversi($data['data']->waktu_akhir_pelaksanaan) ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="card-body">
                <b>Daftar Mitra peserta</b>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered table-sm" id="tabel-mitra">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Mitra</th>
                                <th>Jenis Perusahaan/Sektor</th>
                                <th>Waktu Pendaftaran</th>
                                <th>Paket Kerjasama</th>
                                <th>Status</th>
                                <th>Formulir</th>
                                <th>Kelola</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('template/template_scripts')?>
<script>
    let id_career = "<?php echo $data['data']->id ?>";
</script>