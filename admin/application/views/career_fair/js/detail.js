var tabelPendaftar;
$(document).ready(function () {
    tabelPendaftar = $("#tabel-mitra").DataTable({
        sDom: "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        sServerMethod: "POST",
        autoWidth: false,
        bSort: false,
        pageLength: 15,
        bProcessing: false,
        bServerSide: true,
        fnServerParams: function (aoData) {
            var search = "";
            aoData.push({
                name: "sSearch",
                value: search,
            });
            aoData.push({
                name: "id_career",
                value: id_career,
            });
        },
        fnStateSaveParams: function (oSetings, sValue) {
            // body...
        },
        fnStateLoadParams: function (oSetings, oData) {
            // body...
        },
        sAjaxSource: site_url + "career_fair/index/mitra",
        aoColumns: [{
            mDataProp: "no"
        },
        {
            mDataProp: "nama"
        },
        {
            mDataProp: "jenis"
        },
        {
            mDataProp: "waktu_pendaftaran"
        },
        {
            mDataProp: "paket_kerjasama"
        },
        {
            mDataProp: "status"
        },
        {
            mDataProp: "formulir"
        },
        {
            mDataProp: "kelola"
        },
        ],
    });
});

function validasi(id) {
    swal.fire({
        title: "Apakah anda yakin untuk diverifikasi?",
        icon: "warning",
        showConfirmButton: false,
        html: `
            <p>Anda tidak bisa mengubah jika sudah dikonfirmasi</p>
            <div>
            <button class="btn btn-primary" onclick="verifikasi(`+ id + `)">Verifikasi</button>
            <button class="btn btn-danger" onclick="tolak(`+ id + `)">Tolak</button>
            <button class="btn btn-secondary" onclick="swal.close()">Cancel</button>
            </div>`
    })
}

function verifikasi(id) {
    $.ajax({
        type: 'ajax',
        method: 'GET',
        url: site_url + 'career_fair/index/verifyCareer?id=' + id,
        dataType: 'json',
        encode: true,
        success: function (response) {
            if (response.success) {
                Swal.fire(response.message.title, response.message.body, 'success').then(function () {
                    tabelPendaftar.draw();
                })
            } else {
                Swal.fire(response.message.title, response.message.body, 'error');
            }
        },
        error: function (xmlresponse) {
            console.log(xmlresponse);
        }
    });
}

function tolak(id) {
    $.ajax({
        type: 'ajax',
        method: 'GET',
        url: site_url + 'career_fair/index/tolakCareer?id=' + id,
        dataType: 'json',
        encode: true,
        success: function (response) {
            if (response.success) {
                Swal.fire(response.message.title, response.message.body, 'success').then(function () {
                    tabelPendaftar.draw();
                })
            } else {
                Swal.fire(response.message.title, response.message.body, 'error');
            }
        },
        error: function (xmlresponse) {
            console.log(xmlresponse);
        }
    });
}