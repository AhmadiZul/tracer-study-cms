<section id="login">
	<div class="row" style="margin-top: 20px;margin-bottom:20px;">
		<div class="col-md-6" style="margin-top: 40px;margin-bottom:40px;">
			<div class="text-center">
				<img class="rounded mb-4" src="<?php echo URL_FILE . $logo_utama->url_file ?>" height="80">
			</div>
			<div class="card" style="margin-left: 90px;margin-right:90px;margin-top:30px;">
				<div class="card-body">
					<?php if ($this->session->flashdata('errorMessage')) : ?>
						<div class="alert alert-danger alert-has-icon">
							<div class="alert-icon"><i class="far fa-lightbulb"></i></div>
							<div class="alert-body">
								<div class="alert-title">Mohon Maaf !</div>
								<p class="text-white"><?php echo $this->session->flashdata('errorMessage') ?></p>
							</div>
						</div>
					<?php endif; ?>
					<?php echo form_open('/', array('id' => 'login-form')); ?>
					<h2 class="card-title">Login <strong class="text-tema">Tracer Study</strong></h2>
					<p class="card-text">Silahkan Login menggunakan Username dan Password</p>
					<div class="form-group">
						<label>Username</label>
						<div class="input-group">
							<span class="input-group-text text-tema"><a><i class="fas fa-user"></i></a></span>
							<input type="text" name="username" id="username" onkeyup="gantiWarnaButton()" class="form-control" placeholder="Masukkan username" maxlength="25">
						</div>
					</div>
					<div class="form-group">
						<label>Password</label>
						<div class="input-group">
							<span class="input-group-text text-tema"><a><i class="fas fa-lock"></i></a></span>
							<input type="password" name="password" class="form-control" placeholder="Masukkan password" onkeyup="gantiWarnaButton()" id="pass" maxlength="25">
							<a class="input-group-text show-password" onclick="showPass()"><i class="fas fa-eye-slash text-secondary"></i></a>
						</div>
					</div>
					<div class="form-group">
						<label>Tahun</label>
						<select name="data_tahun" id="data_tahun" class="form-control select2">
							<?php foreach ($tahun as $key => $value) {
								if ($tahun_now == $key) { ?>
									<option value="<?= $key ?>" selected><?= $value ?></option>
								<?php  } else { ?>
									<option value="<?= $key ?>"> <?= $value ?></option>
							<?php }
							} ?>
						</select>
					</div>
					<div class="form-group">
						<div class="d-flex justify-content-around align-items-center">
							<div id="container-captcha"><?php echo $captcha ?></div>
							<button class="btn bg-light-red rounded-sm py-1 px-3 text-tema" type="button" onclick="loadCaptcha()"><i class="fas fa-sync"></i></button>
						</div>
					</div>
					<div class="form-group">
						<input class="form-control number-only" type="text" name="captcha" onkeyup="gantiWarnaButton()" placeholder="Type the Captcha" id="captcha" maxlength="4">
					</div>
					<div class="row">
						<div class="col-lg-12">
							<button name="login-button" value="login-button" type="submit" class="btn-login form-control text-light" id="btn">
								<i class="ion-android-exit mr-2"></i>
								<span>Masuk</span>
							</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-6 d-none d-md-block d-lg-block center">
			<div class="row">
				<img class="img-fluid" src="<?php echo base_url() ?>public/assets/img/login/frame_login.png" style="width:95%;">
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	var site_url = '<?php echo site_url() ?>';
	// $(function() {
	//     loadCaptcha();
	// })
	function loadCaptcha() {
		$.ajax({
			type: 'ajax',
			url: site_url + '/Login/fetchCaptha',
			dataType: 'json',
			success: function(response) {
				console.log(response);
				$('#container-captcha').html(response.data);
			},
			error: function(xmlresponse) {
				console.log(xmlresponse);
			},
		});
	}

	function gantiWarnaButton() {
		var inputUsername = document.getElementById("username").value;
		var inputPass = document.getElementById("pass").value;
		var inputCaptcha = document.getElementById("captcha").value;

		if ((inputUsername.length >= 5) && (inputPass.length >= 3) && (inputCaptcha.length >= 4)) {
			document.getElementById("btn").style.background = "var(--warna)";
		} else {
			document.getElementById("btn").style.background = "#c6c9cb";
		}
	}

	/**
	 * show confirm password
	 */
	function showPass() {
		var x = document.getElementById("pass");
		if (x.type === "password") {
			x.type = "text";
			$('.show-password').html('<i class="fas fa-eye text-secondary">');
		} else {
			x.type = "password";
			$('.show-password').html('<i class="fas fa-eye-slash text-secondary">');
		}
	}

	// $('#login-form').on('submit', function(e) {
	// 	e.preventDefault();
	// 	var inputUsername = $("#username").val();
	// 	var inputPass = $("#pass").val();
	// 	var inputCaptcha = $("#captcha").val();

	// 	const pesan = [];
	// 	if(input_username = ""){
	// 		pesan.push("Username Tidak Boleh Kosong!")
	// 	}
	// 	if(inputPass = ""){
	// 		pesan.push("Username Tidak Boleh Kosong!")
	// 	}
	// 	if(inputCaptcha = ""){
	// 		pesan.push("Captcha Tidak Boleh Kosong!")
	// 	}
	// 	if(pesan){
	// 		swal({
	// 			title: 'Ups...',
	// 			html: data.pesan,
	// 			type: 'warning',
	// 			confirmButtonColor: '#396113',
	// 		}, );
	// 	}
	// });

	// $(document).ready(function() {
	// 	$('#username').on('input change', function() {
	// 		if ($(this).val() != '') {
	// 			$('#btn').prop('disabled', false);
	// 		} else {
	// 			$('#btn').prop('disabled', true);
	// 		}
	// 	});
	// });
</script>