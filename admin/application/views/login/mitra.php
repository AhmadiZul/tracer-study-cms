<section id="login" class="vh-100">
	<div class="login-container vh-100" >
		<div class="row vh-100">
			<div class="col-xs-12 col-lg-4 offset-lg-2 align-self-center">
				<div class="text-center">
					<img class="rounded mb-3" src="<?php echo base_url() ?>public/assets/img/login/logo-login.png">
				</div>
				<div class="bg-light-green top-border"></div>
				<div class="card">
					<div class="card-body">
						<h2 class="card-title">Login</h2>
						<p class="card-text">Silahkan Login menggunakan Username dan Password</p>
						<label>Username</label>
						<input type="text" name="username" class="form-control" placeholder="Masukkan username">
						<label>Password</label>
						<input type="password" name="password" class="form-control" placeholder="Masukkan password">
						<small>Lupa kata sandi? Klik disini</small>
						<div class="row">
							<div class="col-lg-6">
								<a href="<?php echo base_url() ?>Login" class="btn btn-light-danger form-control">KEMBALI</a>
							</div>
							<div class="col-lg-6">
								<a href="<?php echo base_url() ?>mitra/dashboard" class="btn btn-secondary form-control">LOGIN</a>
							</div>
						</div>
						<small>Belum mempunyai akun? <a href="<?php echo base_url() ?>" class="buat-akun">Buat akun</a></small>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-lg-6 text-end d-none d-lg-block px-0">
				<img class="img-fluid vh-100" src="<?php echo base_url() ?>public/assets/img/mitra/login/banner-login.png">
			</div>
		</div>
	</div>
</section>