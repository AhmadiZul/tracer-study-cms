<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-2 pr-0">
                        <button type="button" class="btn btn-primary" id="btn-tambah" data-toggle="modal" data-target="#modal-legalisir" data-action="save">Tambah Legalisir</button>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" id="search" placeholder="Cari disini">
                            <button class="btn btn-primary" id="btn_cari">Cari</button>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="table-responsive">
                    <table class="table table-hover" id="tabel">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIM</th>
                                <th>Nama Lengkap</th>
                                <th>Peguruan Tinggi</th>
                                <th>Prodi</th>
                                <th>Jenis Legalisir</th>
                                <th>Status Dokumen</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-legalisir" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal Template</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form_legalisir" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="text" class="form-control d-none" name="action" id="action" value="0">
                    <input type="text" class="form-control d-none" name="id_legalisir" id="id_legalisir" placeholder="id legalisir">
                    <input type="text" class="form-control d-none" name="is_berkas" id="is_berkas" value="1">
                    <div class="form-group">
                        <label for="alumni">Alumni</label>
                        <select name="id_alumni" id="id_alumni" class="form-control select2">
                            <option value="">Pilih Alumni</option>
                            <?php foreach ($alumni as $key => $value) { ?>
                                <option value="<?php echo $key ?>"><?php echo $value ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback" for="id_alumni"></div>
                    </div>
                    <div class="form-group">
                        <label for="jenis">Jenis Dokumen</label>
                        <select id="jenis" name="jenis" class="form-control select2">
                            <option value="">Pilih Jenis Dokumen</option>
                            <option value="Ijazah">Ijazah</option>
                            <option value="Transkrip Nilai">Transkrip Nilai</option>
                        </select>
                        <div class="invalid-feedback" for="jenis"></div>
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <input type="text" class="form-control number-only" maxlength="11" name="jumlah" id="jumlah" placeholder="Jumlah ">
                        <div class="invalid-feedback" for="jumlah"></div>
                    </div>
                    <div class="form-group" id="status-dokumen">
                        <label for="status">Status Dokumen</label>
                        <select id="status" name="status" class="form-control select2">
                            <option value="">Pilih Status Dokumen</option>
                            <option value="Diajukan">Diajukan</option>
                            <option value="Diproses">Diproses</option>
                            <option value="Selesai">Selesai</option>
                        </select>
                        <div class="invalid-feedback" for="status"></div>
                    </div>
                    <div class="form-group" id="ganti-berkas-legalisir">
                        <label><input name="new_upload_legalisir" type="checkbox" onclick="uploadFoto('legalisir','url_berkas')" value="1"> Ganti Berkas</label>
                        <img src="" alt="your image" style="width: 150px;" /><br>
                    </div>
                    <div id="new-upload-legalisir" class="form-group">
                        <label for="url_berkas">Berkas <b class="text-danger">*</b></label><br>
                        <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                        <input type="file" name="url_berkas" id="input_file" class="form-control custom-file" onchange="cekFile(this,'legalisir')">
                        <span class="file-info-legalisir text-muted">Tidak ada berkas yang dipilih</span>
                        <div id="preview-legalisir">

                        </div>
                        <div class="hint-block text-muted mt-3">
                            <small>
                                Jenis file yang diijinkan: <strong>PNG, JPEG, JPG, PDF</strong><br>
                                Ukuran file maksimal: <strong>2 MB</strong>
                            </small>
                        </div>
                        <div class="invalid-feedback" for="url_berkas"></div>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-hapus" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form_hapus" enctype="multipart/form-data">
                <div class="modal-body">
                    <h4>Apakah anda yakin ingin menghapus data ini?</h4>
                    <input type="text" class="form-control d-none" name="id_hapus" id="id_hapus" placeholder="id legalisir">
                    <input type="text" class="form-control d-none" name="url_berkas" id="url_berkas">
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>

<script>
    var oTable;
    var url_operasi;
    $(document).ready(function() {
        oTable = $('#tabel').DataTable({
            "dom": "<'row'<'col-sm-4 col-md-4'i>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'p>>",
            "sServerMethod": "POST",
            "autoWidth": false,
            "bSort": false,
            "pageLength": 10,
            "bProcessing": false,
            "bServerSide": true,
            "pagingType": "numbers",
            "fnServerParams": function(aoData) {
                var search = $('#search').val();
                aoData.push({
                    name: "sSearch",
                    "value": search
                });
            },
            "fnStateSaveParams": function(oSettings, sValue) {},
            "fnStateLoadParams": function(oSettings, oData) {},
            'sAjaxSource': site_url + "/legalisir/index/table",
            'aoColumns': [{
                    mDataProp: 'nomor'
                },
                {
                    mDataProp: 'nim'
                },
                {
                    mDataProp: 'nama'
                },
                {
                    mDataProp: 'nama_resmi'
                },
                {
                    mDataProp: 'nama_prodi'
                },
                {
                    mDataProp: '_jenis'
                },
                {
                    mDataProp: '_status'
                },
                {
                    mDataProp: 'action'
                },
            ],
            "searching": false,
        });

        $("#btn_cari").click(function() {
            oTable.draw();
        });

        // simpan legalisir
        $('#form_legalisir').on('submit', function(e) {
            e.preventDefault();

            $('#form_legalisir').find('input, select').removeClass('is-invalid');
            $('input').parent().removeClass('is-invalid');
            $.ajax({
                url: url_operasi,
                data: new FormData(this),
                dataType: 'json',
                type: 'POST',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    showLoading();
                },
                success: function(data) {
                    hideLoading();
                    swal.fire({
                        title: data.message.title,
                        text: data.message.body,
                        icon: 'success',
                        confirmButtonColor: '#396113',
                    }).then(function() {
                        $('#modal-legalisir').modal('hide');
                        oTable.draw();
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    hideLoading();
                    let response = jqXHR.responseJSON;

                    switch (jqXHR.status) {
                        case 400:
                            // Keadaan saat validasi
                            if (Array.isArray(response.data)) {
                                response.data.forEach(function({
                                    field,
                                    message
                                }, index) {
                                    $(`[name="${field}"]`).addClass('is-invalid');
                                    $(`[name="${field}"]`).parent().addClass('is-invalid');
                                    $(`.invalid-feedback[for="${field}"]`).html(message);
                                });

                                // sengaja diberi timeout,
                                // karena kalau tidak, saat focus ke field error,
                                // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                                setTimeout(function() {
                                    if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                        $(`[name="${response.data[0]['field']}"]`).focus();
                                    } else {
                                        $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                    };
                                }, 650);
                            };
                            break;
                        case 500:
                            swal.fire({
                                title: response.message.title,
                                html: response.message.body,
                                icon: 'error',
                                confirmButtonColor: '#396113',
                            })
                            break;
                        default:
                            break;
                    };
                }
            });
        });

        $('#form_hapus').on('submit', function(e) {
            e.preventDefault();

            $.ajax({
                url: url_operasi,
                data: new FormData(this),
                dataType: 'json',
                type: 'POST',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    showLoading();
                },
                success: function(data) {
                    hideLoading();
                    swal.fire({
                        title: data.message.title,
                        text: data.message.body,
                        icon: 'success',
                        confirmButtonColor: '#396113',
                    }).then(function() {
                        $('#modal-hapus').modal('hide');
                        oTable.draw();
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    hideLoading();
                    let response = jqXHR.responseJSON;

                    swal.fire({
                        title: response.message.title,
                        html: response.message.body,
                        icon: 'error',
                        confirmButtonColor: '#396113',
                    })
                }
            });
        });

        $('#modal-legalisir').on('show.bs.modal', function(e) {
            var data = e.relatedTarget;
            $('#form_legalisir').find('input, select').removeClass('is-invalid');
            $('input').parent().removeClass('is-invalid');

            if (data.getAttribute('data-action') == "save") {
                add()
            } else {
                url_operasi = site_url + "/legalisir/index/ubahLegalisir";
                $('.modal-title').html('Edit Legalisir');
                $('#action').val('1');
                $('#is_berkas').val('1');
                $('#id_legalisir').val(data.getAttribute('data-id'));
                $('#id_alumni').val(data.getAttribute('data-alumni')).trigger('change');
                $('#id_alumni').attr('readonly', true);
                $('#jenis').val(data.getAttribute('data-jenis')).trigger('change');
                $('#jumlah').val(data.getAttribute('data-jumlah'));
                $('#status').val(data.getAttribute('data-status')).trigger('change');
                $('#status-dokumen').removeClass('d-none');
                $('#ganti-berkas-legalisir').addClass('d-none');

                if (data.getAttribute('data-berkas')) {
                    var file = data.getAttribute('data-berkas');
                    var extension = file.substr((file.lastIndexOf('.') + 1));
                    var html = '<img src="' + site_url + data.getAttribute('data-berkas') + '" class="mt-4" alt="your image" style="width: 250px"/>';

                    if (extension == 'pdf') {
                        html = '<embed src="' + site_url + data.getAttribute('data-berkas') + '" class="mt-4" width="100%" height="800px" />';
                    }

                    var name = "'legalisir'";
                    var file = "'url_berkas'";

                    html += '<label><input name="new_upload_legalisir" type="checkbox" onclick="uploadFoto('+name+', '+file+')" value="1"> Ganti Berkas</label>';

                    $('#new-upload-legalisir').addClass('d-none');
                    $('#url_photo').attr('required', false);
                    $('#ganti-berkas-legalisir').html(html);
                    $('#ganti-berkas-legalisir').removeClass('d-none');
                    $('#ganti-berkas-legalisir label').html('<input name="new_upload_legalisir" type="checkbox" onclick="uploadFoto('+name+', '+file+')" value="1"> Ganti Berkas');
                    $('[name=new_upload_legalisir]').removeClass('d-none');
                    $('#jenis').attr('readonly', false);
                    $('#jumlah').attr('readonly', false);
                    $('#is_berkas').val('0');
                }

                if (data.getAttribute('data-status') == "Diproses" || data.getAttribute('data-status') == "Selesai") {
                    $('#id_alumni').attr('readonly', true);
                    $('#jenis').attr('readonly', true);
                    $('#jumlah').attr('readonly', true);
                    $('#ganti-berkas-legalisir label').html('');
                }
            }
        });

        $('#modal-hapus').on('show.bs.modal', function(e) {
            var data = e.relatedTarget;

            $('.modal-title').html('Confirm');
            url_operasi = site_url + "/legalisir/index/hapusLegalisir";
            $('#id_hapus').val(data.getAttribute('data-id'));
            $('#url_berkas').val(data.getAttribute('data-berkas'));
        });
    })

    function add() {
        url_operasi = site_url + "legalisir/index/simpanLegalisir";
        $('.modal-title').html('Tambah Legalisir');
        $('#form_legalisir')[0].reset();
        $('.select2').select2(null);
        $('#id_alumni').attr('readonly', false);
        $('#is_berkas').val("1");
        $('#ganti-berkas-legalisir').addClass('d-none');
        $('#status-dokumen').addClass('d-none');
        $('[name=url_berkas]').val(null);
        $('#new-upload-legalisir').removeClass('d-none');
        $('#preview-legalisir').html('');
        $('.file-info-legalisir').removeClass('text-info').removeClass('text-success').removeClass('text-danger').removeClass('text-muted').html('Tidak ada berkas yang dipilih');
    }
</script>