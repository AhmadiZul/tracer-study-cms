<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <small>
                    <?php echo $output->output; ?>
                </small>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>

<script>
    $(document).ready(function() {
        window.addEventListener('gcrud.datagrid.ready', () => {
            $(".gc-header-tools").append(
                `<div class="floatL">
            <button id="btn-unduh" class="btn btn-default btn-outline-dark t5">
              <i class="fa fa-plus floatL t3"></i>
              <span class="hidden-xs floatL l5">Tambah Lowongan Kerja</span>
              <div class="clear">
              </div>
            </button>
          </div>`
            );

            $('#btn-unduh').click(function(e) {
                e.preventDefault();
                window.location.href = site_url + 'lowongan_kerja/index/add';
            })
        });
    })
</script>
<script>
    function aktif(id) {
            console.log(id)
            var formData = {
                id_jenis:id,
                is_active: '1'
            };
            console.log(formData)
            $.ajax({
                type: 'ajax',
                method: 'POST',
                url: site_url + 'lowongan/index/aktifJenis',
                data: formData,
                dataType: 'json',
                encode: true,
                success: function(response) {
                    if (response.success) {
                        Swal.fire('Proses Berhasil!', 'Berhasil Diaktivasi', 'success').then(function() {
                            $('.fa-refresh').trigger('click');
                        })
                    } else {
                        Swal.fire('Proses Gagal!', response.message, 'error');
                    }
                },
                error: function(xmlresponse) {
                    console.log(xmlresponse);
                }
            });
        }
    
        function noAktif(id) {
            console.log(id)
            var formData = {
                id_jenis:id,
                is_active: '0'
            };
            console.log(formData)
            $.ajax({
                type: 'ajax',
                method: 'POST',
                url: site_url + 'lowongan/index/aktifJenis',
                data: formData,
                dataType: 'json',
                encode: true,
                success: function(response) {
                    if (response.success) {
                        Swal.fire('Proses Berhasil!', 'Berhasil Di Non Aktifkan', 'success').then(function() {
                            $('.fa-refresh').trigger('click');
                        })
                    } else {
                        Swal.fire('Proses Gagal!', response.message, 'error');
                    }
                },
                error: function(xmlresponse) {
                    console.log(xmlresponse);
                }
            });
        }
</script>
