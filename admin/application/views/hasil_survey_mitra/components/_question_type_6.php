<div class="col-lg-6">
    <div class="card">
        <div class="card-header align-items-start">
            <span class="badge badge-success badge-square"><?= $no ?></span>&emsp;&emsp;
            <label><?= $question ?></label>
        </div>
        <div class="card-body">
            <?php foreach ($answer as $key => $value) { ?>
                <table class="table table-bordered table-md">
                    <tbody>
                        <tr>
                            <th style="width: 10%;">No</th>
                            <th><?=$value['pertanyaan']?></th>
                        </tr>
                        <?php if (empty($value['answer'])) { ?>
                            <tr>
                                <td colspan="2">Data kosong</td>
                            </tr>
                        <?php } else { ?>
                            <?php for ($i = 0; $i < count($value['answer']); $i++) { ?>
                                <tr>
                                    <td><?= $i + 1 ?></td>
                                    <td><?= $value['answer'][$i] ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>
</div>