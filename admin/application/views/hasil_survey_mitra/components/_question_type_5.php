<div class="col-lg-12">
    <div class="card">
        <div class="card-header align-items-start">
            <span class="badge badge-success badge-square"><?= $no ?></span>&emsp;&emsp;
            <label><?= $question ?></label>
        </div>
        <div class="card-body">
            <div class="row">
                <?php for ($i = 0; $i < count($pertanyaan); $i++) { ?>
                    <div class="col-md-6">
                        <label for=""><?= $pertanyaan[$i]['pertanyaan'] ?></label>
                        <div id="container_'<?= $pertanyaan[$i]['id_likert'] ?>'"></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php foreach ($answer as $key => $value) { ?>
    <script type="text/javascript">
        var answer = <?php echo $value ?>;
        var buttonChart = {
        buttons: {
            contextButton: {
                menuItems: [
                    'viewFullscreen',
                    'printChart',
                    'separator',
                    'downloadPNG',
                    'downloadJPEG',
                    'downloadPDF',
                    'downloadSVG',
                    'separator',
                    'downloadCSV',
                    'downloadXLS',
                    'viewData'
                ]
            }
        }
    };
        Highcharts.chart("container_'<?= $key ?>'", {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: "pie"
            },
            exporting: buttonChart,
            title: {
                text: ""
            },
            tooltip: {
                pointFormat: "{point.name}: <b> {point.y} ({point.percentage:.1f}%)</b>"
            },
            accessibility: {
                point: {
                    valueSuffix: "%"
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: "pointer",
                    dataLabels: {
                        enabled: true,
                        format: "<b>{point.name}</b>: {point.y} {point.percentage:.1f} %"
                    }
                }
            },
            series: [{
                name: "Data Option",
                colorByPoint: true,
                data: answer
            }]
        });
    </script>
<?php } ?>