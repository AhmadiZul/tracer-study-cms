<script src="<?php echo base_url() . 'public/vendor/highchart/highchart.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/exporting.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/export-data.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/accessibility.js'; ?>"></script>

<div class="card">
    <div class="card-header">
        <h4 class="card-title"><i class="fa fa-chart-pie"></i> Hasil Survey</h4>
    </div>
    <div class="card-body row">
        <?php foreach ($question as $key => $value) { ?>
            <?php if ($value->question_type == 1 || $value->question_type == 2 || $value->question_type == 7) : ?>
                <?php $this->load->view('hasil_survey/components/_question_type_1_2_7', $value); ?>
            <?php endif; ?>
            <?php if ($value->question_type == 3) : ?>
                <?php $this->load->view('hasil_survey/components/_question_type_3', $value); ?>
            <?php endif; ?>
            <?php if ($value->question_type == 4) : ?>
                <?php $this->load->view('hasil_survey/components/_question_type_4', $value); ?>
            <?php endif; ?>
            <?php if ($value->question_type == 5) : ?>
                <?php $this->load->view('hasil_survey/components/_question_type_5', $value); ?>
            <?php endif; ?>
            <?php if ($value->question_type == 6) : ?>
                <?php $this->load->view('hasil_survey/components/_question_type_6', $value); ?>
            <?php endif; ?>
        <?php } ?>
    </div>
</div>

<?php $this->load->view("template/template_scripts") ?>

<script type="text/javascript">
    let body = $('body');

    $(document).ready(function() {
        body.addClass('sidebar-mini');
        body.removeClass('sidebar-show');

        $(".main-sidebar .sidebar-menu > li").each(function() {
            let me = $(this);

            if (me.find('> .dropdown-menu').length) {
                me.find('> .dropdown-menu').hide();
                me.find('> .dropdown-menu').prepend('<li class="dropdown-title pt-3">' + me.find('> a').text() + '</li>');
            } else {
                me.find('> a').attr('data-toggle', 'tooltip');
                me.find('> a').attr('data-original-title', me.find('> a').text());
                $("[data-toggle='tooltip']").tooltip({
                    placement: 'right'
                });
            }
        });
    });
</script>