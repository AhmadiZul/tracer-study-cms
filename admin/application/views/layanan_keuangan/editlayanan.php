<div class="card">
    <div class="card-body">
        <form id="form_layanan">
            <input type="hidden" name="id_layanan" id="id_layanan" class="form-control" value="<?= $id_layanan ?>">
            <div class="form-group row">
                <label class="col-md-3 col-from-label" for="nama_bank">Nama Bank <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" name="nama_bank" id="nama_bank" class="form-control" placeholder="Masukkan nama bank" value="<?= $layanan->nama_bank ?>" maxlength="100">
                    <div class="invalid-feedback" for="nama_bank"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-from-label" for="url_web">Website <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" name="url_web" id="url_web" class="form-control" placeholder="Masukkan website" value="<?= $layanan->url_web ?>" maxlength="255">
                    <div class="invalid-feedback" for="url_web"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="url_photo" class="col-md-3 col-form-label">Upload Logo <b class="text-danger">*</b></label>
                <div id="ganti-berkas-logo" class="col-md-9 <?php echo (isset($layanan->url_logo) ? '' : 'd-none') ?>">
                    <img src="<?php echo (isset($layanan->url_logo)  ? $layanan->url_logo : '') ?>" alt="your image" style="width: 150px;" /><br>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="new_upload_logo" id="new_upload_logo" value="1" onclick="uploadFoto('logo','url_photo')">
                        <label class="custom-control-label" for="new_upload_logo">Ganti Berkas</label>
                    </div>
                </div>
                <div class="col-md-3 <?php echo (isset($layanan->url_logo)  ? '' : 'd-none') ?>"></div>
                <div id="new-upload-logo" class="col-md-9 <?php echo (isset($layanan->url_logo)  ? 'd-none' : '') ?>">
                    <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                    <input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo')">
                    <span class="file-info-logo text-muted">Tidak ada berkas yang dipilih</span>
                    <div id="preview-logo">

                    </div>
                    <div class="hint-block text-muted mt-3">
                        <small>
                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                            Ukuran file maksimal: <strong>2 MB</strong>
                        </small>
                    </div>
                    <div class="invalid-feedback" for="url_photo"></div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url(); ?>layanan_keuangan/index" class="btn btn-warning float-end">Kembali</a>
                <button type="submit" class="btn btn-success float-end">Simpan</button>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view("template/template_scripts") ?>

<script>
    $('#form_layanan').on('submit', function(e) {
        e.preventDefault();

        $('#form_layanan').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'layanan_keuangan/index/editLayanan',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    title: data.message.title,
                    text: data.message.body,
                    icon: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = site_url + 'layanan_keuangan/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>