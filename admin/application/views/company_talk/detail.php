<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body table-responsive">
                <table class="table table-sm borderless">
                    <tr>
                        <th>Nama Company Talk</th>
                        <th>:</th>
                        <td><?php echo $data['data']->judul_presentasi ?></td>
                    </tr>
                    <tr>
                        <th>Tempat Pelaksanaan</th>
                        <th>:</th>
                        <td><?php echo $data['data']->nama_resmi ?></td>
                    </tr>
                    <tr>
                        <th>Ruang</th>
                        <th>:</th>
                        <td><?php echo $data['data']->di_ruang ?></td>
                    </tr>
                    <tr>
                        <th>Tempat Pelaksanaan</th>
                        <th>:</th>
                        <td><?php echo $data['data']->nama_resmi ?></td>
                    </tr>
                    <tr>
                        <th>Perguruan Tinggi Pelaksaan</th>
                        <th>:</th>
                        <td><?php echo $data['data']->nama_resmi ?></td>
                    </tr>
                    <tr>
                        <th>Tanggal Pendaftaran</th>
                        <th>:</th>
                        <td>
                            <?php echo $this->tanggalindo->konversi($data['data']->waktu_awal_daftar).' - '. $this->tanggalindo->konversi($data['data']->waktu_akhir_daftar) ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Tanggal Pelaksanaan</th>
                        <th>:</th>
                        <td>
                            <?php echo $this->tanggalindo->konversi($data['data']->tanggal_pelaksanaan) ?>
                        </td>
                    </tr>
                </table>
                <div class="col-md-12">
                    <h5 class="font-weigt-bold text-success">Deskrispi</h5>
                    <label class="text-dark"><?php echo $data['data']->deskripsi ?></label>
                </div>
            </div>
            <div class="card-body">
                <b>Daftar peserta</b>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered table-sm" id="tabel-mitra">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Asal Kampus/Sekolah</th>
                                <th>Prodi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('template/template_scripts')?>
<script>
    let id_company_talks = "<?php echo $data['data']->id ?>";
</script>