var tabelPendaftar;
$(document).ready(function () {
    tabelPendaftar = $("#tabel-mitra").DataTable({
        sDom: "<'row' <'col-md-12'>><'row'<'col-md-6 col-sm-12'><'col-md-6 col-sm-12'>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        sServerMethod: "POST",
        autoWidth: false,
        bSort: false,
        pageLength: 15,
        bProcessing: false,
        bServerSide: true,
        fnServerParams: function (aoData) {
            var search = "";
            aoData.push({
                name: "sSearch",
                value: search,
            });
            aoData.push({
                name: "id_company_talks",
                value: id_company_talks,
            });
        },
        fnStateSaveParams: function (oSetings, sValue) {
            // body...
        },
        fnStateLoadParams: function (oSetings, oData) {
            // body...
        },
        sAjaxSource: site_url + "company_talk/index/pendaftar",
        aoColumns: [{
                mDataProp: "no"
            },
            {
                mDataProp: "nama"
            },
            {
                mDataProp: "asalKampus"
            },
            {
                mDataProp: "prodi"
            },
        ],
    });
});