<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <small>
                    <?php if (isset($output)) : ?>
                        <?php echo $output->output; ?>
                    <?php endif; ?>
                </small>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>
<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    })

    function validasi(id) {
        swal.fire({
            title: "Apakah anda yakin untuk diverifikasi?",
            icon: "warning",
            showConfirmButton: false,
            html: `
            <p>Anda tidak bisa mengubah jika sudah dikonfirmasi</p>
            <div>
            <button class="btn btn-primary" onclick="verifikasi(` + id + `)">Verifikasi</button>
            <button class="btn btn-danger" onclick="tolak(` + id + `)">Tolak</button>
            <button class="btn btn-secondary" onclick="swal.close()">Cancel</button>
            </div>`
        })
    }

    function verifikasi(id) {
        $.ajax({
            type: 'ajax',
            method: 'GET',
            url: site_url + 'company_talk/index/verifyTalk?id=' + id,
            dataType: 'json',
            encode: true,
            success: function(response) {
                if (response.success) {
                    Swal.fire(response.message.title, response.message.body, 'success').then(function() {
                        $('.fa-refresh').trigger('click');
                    })
                } else {
                    Swal.fire(response.message.title, response.message.body, 'error');
                }
            },
            error: function(xmlresponse) {
                console.log(xmlresponse);
            }
        });
    }

    function tolak(id) {
        $.ajax({
            type: 'ajax',
            method: 'GET',
            url: site_url + 'company_talk/index/tolakTalk?id=' + id,
            dataType: 'json',
            encode: true,
            success: function(response) {
                if (response.success) {
                    Swal.fire(response.message.title, response.message.body, 'success').then(function() {
                        $('.fa-refresh').trigger('click');
                    })
                } else {
                    Swal.fire(response.message.title, response.message.body, 'error');
                }
            },
            error: function(xmlresponse) {
                console.log(xmlresponse);
            }
        });
    }
</script>