<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <small>
                    <?php echo $output->output; ?>
                </small>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>

<script>
    $(document).ready(function() {
        window.addEventListener('gcrud.datagrid.ready', () => {
            $(".gc-header-tools").append(
                `<div class="floatL">
            <button id="btn-unduh" class="btn btn-default btn-outline-dark t5">
              <i class="fa fa-plus floatL t3"></i>
              <span class="hidden-xs floatL l5">Tambah User</span>
              <div class="clear">
              </div>
            </button>
          </div>`
            );

            $('#btn-unduh').click(function(e) {
                e.preventDefault();
                window.location.href = site_url + 'user_akses/index/addUser';
            })
        });
    })

    function password_show_hide2() {
        var x = document.getElementById("password2");
        var show_eye = document.getElementById("show_eye2");
        var hide_eye = document.getElementById("hide_eye2");
        hide_eye.classList.remove("d-none");
        if (x.type === "password") {
            x.type = "text";
            show_eye.style.display = "none";
            hide_eye.style.display = "block";
        } else {
            x.type = "password";
            show_eye.style.display = "block";
            hide_eye.style.display = "none";
        }
    }
</script>