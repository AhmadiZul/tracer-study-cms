<div class="card">
    <div class="card-body">
        <form id="form_mitra">
            <input type="hidden" name="id_user" id="id_user" class="form-control" value="<?= $user['id_user'] ?>">
            <div class="form-group row">
                <label class="col-md-3 col-from-label" for="username">Username <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" name="username" id="username" class="form-control" placeholder="Masukkan username" value="<?= $user['username'] ?>" maxlength="25">
                    <div class="invalid-feedback" for="username"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-from-label" for="password">Password </label>
                <div class="col-md-9">
                    <div class="input-group mb-3">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Masukkan password" maxlength="25">
                        <a class="input-group-text show-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                    </div>
                    <div class="invalid-feedback" for="password"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-from-label" for="cpassword">Konfirmasi Password</label>
                <div class="col-md-9">
                    <div class="input-group mb-3">
                        <input type="password" id="cpassword" name="cpassword" class="form-control" placeholder="Masukkan konfirmasi password anda" maxlength="25">
                        <a class="input-group-text show-confirm-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                    </div>
                    <div class="invalid-feedback" for="cpassword"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-from-label" for="nama">Nama <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="nama" class="form-control" name="nama" id="nama" placeholder="Masukkan nama" value="<?= $user['real_name']  ?>" maxlength="25">
                    <div class="invalid-feedback" for="nama"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-from-label" for="email">Email <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="email" id="email" placeholder="Masukkan email" value="<?= $user['email']  ?>" maxlength="50">
                    <div class="invalid-feedback" for="email"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="role" class="col-md-3 col-form-label">Role <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <select id="role" name="role" class="form-control select2" onchange="update()">
                        <option value="" selected>Pilih Role</option>
                        <?php foreach ($role as $key => $value) { ?>
                            <?php if ($user['id_group']  == $value['id_group']) : ?>
                                <option value="<?= $value['id_group']; ?>" selected><?php echo $value['nama_group']; ?></option>
                            <?php else : ?>
                                <option value="<?= $value['id_group']; ?>"><?php echo $value['nama_group']; ?></option>
                            <?php endif ?>
                        <?php } ?>

                    </select>
                    <div class="invalid-feedback" for="role"></div>
                </div>
            </div>
            <div class="form-group row fakultas">
                <label for="input_fakultas" class="col-md-3 col-form-label">Fakultas<small class="text-danger">*</small></label>
                <div class="col-md-9">
                    <select id="input_fakultas" name="input_fakultas" class="form-control select2">
                    <option value="">Pilih Fakultas</option>
                    <?php foreach($fakultas as $key => $value) {
                        if($value->id_fakultas == $user['id_fakultas']) {?>
                            <option value="<?= $value->id_fakultas ?>" selected><?= $value->nama_fakultas ?></option>
                    <?php } else { ?>
                            <option value="<?= $value->id_fakultas ?>"><?= $value->nama_fakultas ?></option>
                    <?php }} ?>
                    </select>
                    <div class="invalid-feedback" for="input_fakultas"></div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url(); ?>user_akses/index" class="btn btn-theme-dark float-end">Kembali</a>
                <button type="submit" class="btn btn-theme-danger float-end">Simpan</button>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view("template/template_scripts") ?>

<script>

function update() {
    var select = $('#role :selected').val();
    if(select == 2){
        $(".fakultas").show(300);
    }else{
        $(".fakultas").hide(200);
    }
}
    $('#form_mitra').on('submit', function(e) {
        e.preventDefault();

        $('#form_mitra').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'user_akses/index/editSimpan',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    title: data.message.title,
                    text: data.message.body,
                    icon: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = site_url + 'user_akses/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })

    /**
     * show confirm password
     */
    $('.show-confirm-password').click(function(e) {
        if ('password' == $('[name="cpassword"]').attr('type')) {
            $('[name="cpassword"]').prop('type', 'text');
            $('.show-confirm-password').html('<i class="fas fa-eye text-secondary">');
        } else {
            $('[name="cpassword"]').prop('type', 'password');
            $('.show-confirm-password').html('<i class="fas fa-eye-slash text-secondary">');
        }
    });

    $(document).ready(function() {
        var cek = $('#role :selected').val()
        if (cek == 2) {
            // dismiss_loading();
            $(".fakultas").show(300);
        } else {
            $(".fakultas").hide(200);
        }

    });
</script>