<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="top-border"></div>
            <div class="card-body">
                <form action="" method="post" id="myformkuisioner">
                    <div class="form-group row">
                        <label for="input_kode" class="col-md-3 col-form-label">Kode Pertanyaan<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input_kode" name="input_kode" value="" maxlength="10">
                            <div class="invalid-feedback" for="input_kode"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="urutan" class="col-md-3 col-form-label">Urutan<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control number-only" id="urutan" name="urutan" value="" maxlength="10">
                            <div class="invalid-feedback" for="urutan"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_pertanyaan" class="col-md-3 col-form-label">Pertanyaan<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="input_pertanyaan" name="input_pertanyaan" value="" maxlength="150"></textarea>
                            <div class="invalid-feedback" for="input_pertanyaan"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_jenis" class="col-md-3 col-form-label">Jenis Pertanyaan<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <select id="input_jenis" name="input_jenis" class="form-control select2" onChange="update()" required>
                                <option value="" selected>Pilih Jenis Pertanyaan</option>
                                <option value="1">Jawaban Singkat</option>
                                <option value="2">Paragraf</option>
                                <option value="3">Pilihan Ganda</option>
                                <option value="4">Kotak Centang</option>
                                <option value="5">Skala Likert</option>
                                <option value="7">Lokasi</option>
                                <option value="8">Tanpa Jawaban</option>
                                <option value="9">Dropdown</option>
                            </select>
                            <div class="invalid-feedback" for="input_jenis"></div>
                            <!-- is have parent question? -->
                            <div class="row pt-3 isParent">
                                <div class="col-md">
                                    <div class="form-check">
                                        <input type="checkbox" onclick="addParent()" class="form-check-input" id="add_parent" name="add_parent" value="isian[on]">
                                        <label class="form-check-label" for="add_parent">Apakah memiliki parent question?</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row pt-3">
                                <div class="col-md isian_parent">
                                    <select id="isian_parent" class=" form-control select2" name="isian_parent">
                                        <option value="">Pilih parent question</option>
                                        <?php foreach ($parent as $key => $value) { ?>
                                            <option value="<?= $value['question_code'] ?>" style="width : 30%"> <?= $value['question_code'] . " - " . $value['question'] ?> </option>
                                        <?php } ?>
                                    </select>
                                    <div class="invalid-feedback" for="isian_parent"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row lokasi">
                        <label for="lokasi" class="col-md-3 col-form-label">Pilih lokasi<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <select id="lokasi" name="lokasi" class="form-control select2 lokasi" onChange="update()" required>
                                <option value="">Pilih Lokasi</option>
                                <option value="provinsi">Provinsi</option>
                                <option value="kabupaten">Kabupaten</option>
                            </select>
                            <div class="invalid-feedback" for="lokasi"></div>
                        </div>
                    </div>

                    <div class="form-group row singkat">
                        <label for="input_singkat" class="col-md-3 col-form-label">Jawaban<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <select id="input_singkat" name="input_singkat" class="form-control select2" required>
                                <option value="" selected>Teks Biasa (default)</option>
                                <option value="integer">Harus Angka</option>
                                <option value="valid_email">Harus Email Valid</option>
                                <option value="is_date">Harus Tanggal</option>
                            </select>
                        </div>
                    </div>

                    <div class="container pilgan" id="con_pilgan">
                        <input type="hidden" name="total" value="1">
                        <button type="button" class="btn btn-success mb-2" id="jawaban_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>
                        <div class="form-group row" id='pilgan0'>
                            <label for="input_pilgan" class="col-md-3 col-form-label">Pilihan Jawaban<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="input_pilgan" name="input_pilgan0" value="" multiple required>
                                        <div class="invalid-feedback" for="input_pilgan0"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="inputKodePilgan" name="inputKodePilgan0" placeholder="kode jawaban" value="" multiple >
                                        <div class="invalid-feedback" for="inputKodePilgan0"></div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-danger" onclick='hapusPilgan(0)'>Hapus</button>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md">
                                        <div class="form-check">
                                            <input type="checkbox" onclick="addpilgan(0)" class="form-check-input" id="add_pilgan0" name="add_pilgan0" value="isian[on]">
                                            <label class="form-check-label" for="add_pilgan">Tambah Isian</label>
                                        </div>
                                    </div>
                                    <div class="col-md">
                                        <select id="isian_pilgan0" class="isian_pilgan0 form-control" name="isian_pilgan0">
                                            <option value="" selected>Teks Biasa (default)</option>
                                            <option value="integer">Harus Angka</option>
                                            <option value="valid_email">Harus Email Valid</option>
                                            <option value="is_date">Harus Tanggal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container kokcen" id="form_kokcen">
                        <input type="hidden" name="totalU" value="1">
                        <button type="button" class="btn btn-success mb-2" id="kokcen_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>
                        <div class="form-group row" id="kokcen0">
                            <label for="input_kokcen" class="col-md-3 col-form-label">Pilihan Jawaban<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="input_kokcen" name="input_kokcen0" value="" multiple>
                                        <div class="invalid-feedback" for="input_kokcen0"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="inputKodeKokcen" name="inputKodeKokcen0" placeholder="kode jawaban" value="" multiple>
                                        <div class="invalid-feedback" for="inputKodeKokcen0"></div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-danger" onclick='hapusKokcen(0)'>Hapus</button>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md">
                                        <div class="form-check">
                                            <input type="checkbox" onclick="addKokcen(0)" class="form-check-input" id="add_kokcen0" name="add_kokcen0" value="isian[on]">
                                            <label class="form-check-label" for="add_kokcen">Tambah Isian</label>
                                        </div>
                                    </div>
                                    <div class="col-md">
                                        <select id="isian_kokcen0" class="isian_kokcen0 form-control" name="isian_kokcen0">
                                            <option value="" selected>Teks Biasa (default)</option>
                                            <option value="integer">Harus Angka</option>
                                            <option value="valid_email">Harus Email Valid</option>
                                            <option value="is_date">Harus Tanggal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container linier" id="form_linier">
                        <input type="hidden" name="totalL" value="1">
                        <div class="form-group row">
                            <label for="" class="col-md-3 col-form-label">Keterangan<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <p>Skala likert digunakan untuk mengukur sikap atau pendapat. Aplikasi ini menggunakan skala angka 1 - 5 yang berarti bahwa <b> 5: Sangat Setuju (SS), 4: Setuju (S), 3:Ragu-ragu (RG), 2:Tidak Setuju (TS), 1:Sangat Tidak Setuju (STS) </b></p>
                                <div class="row pt-3">
                                    <div class="col-md">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="is_horizontal" name="is_horizontal" value="is_horizontal">
                                            <label class="form-check-label" for="is_horizontal">Apakah penulisan soal horizontal?</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <button type="button" class="btn btn-success mb-2" id="linier_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>
                        <div class="form-group row" id="linier0">
                            <label for="input_linier" class="col-md-3 col-form-label">Pilihan Jawaban<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="input_linier" name="input_linier0" value="" multiple>
                                        <div class="invalid-feedback" for="input_linier0"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="inputKodeLinier" name="inputKodeLinier0" placeholder="kode jawaban" value="" multiple>
                                        <div class="invalid-feedback" for="inputKodeLinier0"></div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-danger" onclick='hapusLinier(0)'>Hapus</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="container dropdown" id="con_dropdown">
                        <input type="hidden" name="totalD" value="1">
                        <button type="button" class="btn btn-success mb-2" id="dropdown_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>
                        <div class="form-group row" id='dropdown0'>
                            <label for="input_dropdown" class="col-md-3 col-form-label">Pilihan Jawaban<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" id="input_dropdown" name="input_dropdown0" value="" multiple required>
                                        <div class="invalid-feedback" for="input_dropdown0"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" id="inputKodedropdown" name="inputKodeDropdown0" placeholder="kode jawaban" value="" multiple>
                                        <div class="invalid-feedback" for="inputKodeDropdown0"></div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-danger" onclick='hapusDropdown(0)'>Hapus</button>
                                    </div>
                                </div><br>
                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url('kuesioner_survey/index/') ?>" class="btn btn-success">Kembali</a>
                <button type="submit" class="btn btn-primary" id="btn-simpan-kuisioner">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>


<?php $this->load->view("template/template_scripts") ?>



<script>
    $(".singkat").hide(200);
    $(".isian_pilgan0").hide(200);
    $(".isian_parent").hide(200);
    $(".isian_kokcen0").hide(200);
    $(".lokasi").hide(200);
    $(".pilgan").hide(200);
    $(".kokcen").hide(200);
    $(".linier").hide(200);
    $(".dropdown").hide(200);

    function update() {
        var select = $('#input_jenis :selected').val();
        switch (select) {
            case '1':
                $(".isParent").show(300);
                $(".singkat").show(300);
                $(".pilgan").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".lokasi").hide(200);
                $(".dropdown").hide(200);
                break;
            case '2':
                $(".isParent").show(300);
                $(".singkat").hide(200);
                $(".pilgan").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".lokasi").hide(200);
                $(".dropdown").hide(200);
                break;
            case '3':
                $(".isParent").show(300);
                $(".singkat").hide(200);
                $(".pilgan").show(300);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".lokasi").hide(200);
                $(".dropdown").hide(200);
                break;
            case '4':
                $(".isParent").show(300);
                $(".singkat").hide(200);
                $(".pilgan").hide(200);
                $(".kokcen").show(300);
                $(".lainnya_kokcen").show(300);
                $(".linier").hide(200);
                $(".lokasi").hide(200);
                $(".dropdown").hide(200);
                break;
            case '5':
                $(".isParent").show(300);
                $(".singkat").hide(200);
                $(".pilgan").hide(200);
                $(".kokcen").hide(200);
                $(".linier").show(300);
                $(".lokasi").hide(200);
                $(".dropdown").hide(200);
                break;
            case '7':
                $(".isParent").show(300);
                $(".singkat").hide(200);
                $(".pilgan").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".lokasi").show(300);
                $(".dropdown").hide(200);
                break;
            case '8':
                $(".singkat").hide(200);
                $(".pilgan").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".isParent").hide(200);
                $(".lokasi").hide(200);
                $(".dropdown").hide(200);
                break;
            case '9':
                $(".singkat").hide(200);
                $(".pilgan").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".isParent").hide(200);
                $(".lokasi").hide(200);
                $(".dropdown").show(300);
                $(".isParent").show(300);
                break;
            default:
                $(".singkat").hide(200);
                $(".pilgan").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".lokasi").hide(200);
                $(".dropdown").hide(200);
                break;

        }

    }

    var total = 1;
    $("#jawaban_plus").click(function() {
        $("#con_pilgan").append("<div class='form-group row' id='pilgan" + total + "'><label for='input_pilgan' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-7'><input type='text' class='form-control' id='input_pilgan' name='input_pilgan" + total + "' value='' multiple><div class='invalid-feedback' for='input_pilgan" + total +"'></div></div><div class='col-md-3'><input type='text' class='form-control' id='inputKodePilgan' name='inputKodePilgan" + total + "' placeholder='kode jawaban' value='' multiple><div class='invalid-feedback' for='inputKodePilgan"+ total +"'></div></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusPilgan(" + total + ")'>Hapus</button></div></div><br><div class='row'><div class='col-md'><div class='form-check'><input type='checkbox' onclick='addpilgan(" + total + ")' class='form-check-input' id='add_pilgan" + total + "' name='add_pilgan" + total + "' value='isian[on]'><label class='form-check-label' for='add_pilgan'>Tambah Isian</label></div></div><div class='col-md'><select id='isian_pilgan" + total + "' class='isian_pilgan" + total + " form-control' name='isian_pilgan" + total + "'><option value='' selected>Teks Biasa (default)</option><option value='integer'>Harus Angka</option><option value='valid_email'>Harus Email Valid</option><option value='is_date'>Harus Tanggal</option></select></div></div></div>");
        var isian_awal = ".isian_pilgan" + total.toString()
        $(isian_awal).hide(200);
        total += 1;
        $('[name=total]').val(total);
    });

    var totalU = 1;
    $("#kokcen_plus").click(function() {
        $("#form_kokcen").append("<div class='form-group row' id='kokcen" + totalU + "'><label for='form_kokcen' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-7'><input type='text' class='form-control' id='input_kokcen' name='input_kokcen" + totalU + "' value='' multiple><div class='invalid-feedback' for='input_kokcen"+ totalU +"'></div></div><div class='col-md-3'><input type='text' class='form-control' id='inputKodeKokcen' name='inputKodeKokcen" + totalU + "' placeholder='kode jawaban' value='' multiple><div class='invalid-feedback' for='inputKodeKokcen"+ totalU +"'></div></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusKokcen(" + totalU + ")'>Hapus</button></div></div><br><div class='row'><div class='col-md'><div class='form-check'><input type='checkbox' onclick='addKokcen(" + totalU + ")' class='form-check-input' id='add_kokcen" + totalU + "' name='add_kokcen" + totalU + "' value='isian[on]'><label class='form-check-label' for='add_kokcen'>Tambah Isian</label></div></div><div class='col-md'><select id='isian_kokcen" + totalU + "' class='isian_kokcen" + totalU + " form-control' name='isian_kokcen" + totalU + "'><option value='' selected>Teks Biasa (default)</option><option value='integer'>Harus Angka</option><option value='valid_email'>Harus Email Valid</option><option value='is_date'>Harus Tanggal</option></select></div></div></div>");
        var isian_awal = ".isian_kokcen" + totalU.toString()
        $(isian_awal).hide(200);
        totalU += 1;
        $('[name=totalU]').val(totalU);
    });

    var totalL = 1;
    $("#linier_plus").click(function() {
        $("#form_linier").append("<div class='form-group row' id='linier" + totalL + "'><label for='input_linier' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-7'><input type='text' class='form-control' id='input_linier' name='input_linier" + totalL + "' value='' multiple><div class='invalid-feedback' for='input_linier"+ totalL +"'></div></div><div class='col-md-3'><input type='text' class='form-control' id='inputKodeLinier' name='inputKodeLinier" + totalL + "' placeholder='kode jawaban' value='' multiple><div class='invalid-feedback' for='inputKodeLinier"+ totalL +"'></div></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusLinier(" + totalL + ")'>Hapus</button></div></div></div>");
        totalL += 1;
        $('[name=totalL]').val(totalL);
    });

    var totalS = 1;
    $("#sub_plus").click(function() {
        $("#form_sub").append("<div class='form-group row' id='sub" + totalS + "'><label for='input_sub' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-7'><input type='text' class='form-control' id=input_sub' name='input_sub" + totalS + "' value='' multiple></div><div class='col-md-3'><input type='text' class='form-control' id='inputKodeSub' name='inputKodeSub" + totalS + "' placeholder='kode sub' value='' multiple></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusSub(" + totalS + ")'>Hapus</button></div><br><div class='row'><div class='col-md-6'><select id='isian_sub" + totalS + "' class='isian_sub" + totalS + " form-control' name='isian_sub" + totalS + "'><option value='' selected>Teks Biasa (default)</option><option value='integer'>Harus Angka</option><option value='valid_email'>Harus Email Valid</option><option value='is_date'>Harus Tanggal</option></select></div></div></div></div>");
        totalS += 1;
        $('[name=totalS]').val(totalS);
    });

    var totalD = 1;
    $("#dropdown_plus").click(function() {
        $("#con_dropdown").append("<div class='form-group row' id='dropdown" + totalD + "'><label for='input_dropdown' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-7'><input type='text' class='form-control' id='input_dropdown' name='input_dropdown" + totalD + "' value='' multiple><div class='invalid-feedback' for='input_dropdown"+ totalD +"'></div></div><div class='col-md-3'><input type='text' class='form-control' id='inputKodeDropdown' name='inputKodeDropdown" + totalD + "' placeholder='kode jawaban' value='' multiple><div class='invalid-feedback' for='inputKodeDropdown"+ totalD +"'></div></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusDropdown(" + totalD + ")'>Hapus</button></div></div><br><div class='row'></div>");
        totalD += 1;
        $('[name=totalD]').val(totalD);
    });

    $('#btn-simpan-kuisioner').click(function(e) {
        // var input_kode = $("#input_kode").val();
        // var input_pertanyaan = $("#input_pertanyaan").val();
        // var input_jenis = $("#input_jenis").val();
        // var input_urutan = $("#input_urutan").val();
        // const isFilled = (input_kode != "" && input_pertanyaan != "" && input_jenis != "" && input_urutan != "");;
        $('#myformkuisioner').find('input, select, textarea').removeClass('is-invalid');
		$('input').parent().removeClass('is-invalid');
        e.preventDefault();

        // if (isFilled) {
            $.ajax({
                type: 'ajax',
                method: 'post',
                url: site_url + 'kuesioner_survey/index/createKuesioner',
                data: new FormData($('#myformkuisioner')[0]),
                async: false,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        Swal.fire('Proses Berhasil!', response.message, 'success').then(function() {
                            window.location.href = "<?php echo base_url('kuesioner_survey/index/') ?>";
                        })
                    } else {
                        Swal.fire('Proses Gagal!', response.message, 'error');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // console.log(xmlresponse);
                    hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
                }
            })
        // } else {
        //     Swal.fire({
        //         confirmButtonColor: '#3ab50d',
        //         icon: 'error',
        //         title: 'Peringatan',
        //         text: 'Isian bertanda bintang wajib diisi',
        //     })
        // }
    })

    function hapusPilgan(id) {
        let hapus = 'pilgan' + id.toString()
        console.log(hapus)
        total = total - 1
        $('[name=total]').val(total);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function hapusKokcen(id) {
        let hapus = 'kokcen' + id.toString()
        console.log(hapus)
        totalU = totalU - 1
        $('[name=totalU]').val(totalU);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function hapusLinier(id) {
        let hapus = 'linier' + id.toString()
        console.log(hapus)
        totalL = totalL - 1
        $('[name=totalL]').val(totalL);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function hapusSub(id) {
        let hapus = 'sub' + id.toString()
        console.log(hapus)
        totalS = totalS - 1
        $('[name=totalS]').val(totalS);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function hapusDropdown(id) {
        let hapus = 'dropdown' + id.toString()
        console.log(hapus)
        total = total - 1
        $('[name=total]').val(total);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function addpilgan(id) {
        var x = document.getElementById("add_pilgan" + id.toString()).checked;
        console.log(x);
        var isian = ".isian_pilgan" + id.toString()
        if (x) {
            $(isian).show(300);
        } else {
            $(isian).hide(200);
        }
    }

    function addParent() {
        var x = document.getElementById("add_parent").checked;
        console.log(x);
        var isian = ".isian_parent";
        if (x) {
            $(isian).show(300);
        } else {
            $(isian).hide(200);
        }
    }

    function addKokcen(id) {
        var x = document.getElementById("add_kokcen" + id.toString()).checked;
        console.log(x);
        var isian = ".isian_kokcen" + id.toString()
        if (x) {

            $(isian).show(300);
        } else {
            $(isian).hide(200);
        }
    }
</script>