<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="mg-b-0 tx-spacing-1"><a href="<?php echo base_url('vidio/index/') ?>" class="btn btn-warning text-danger btn-xs"><i class="fas fa-arrow-left"></i> Kembali</a> || <?= $title ?></h4>
            </div>
            <div class="card-body">
                <div class="my-auto">
                    <iframe width="100%" height="315" src="<?php echo empty($vidio['url_vidio']) ? "-" : $vidio['url_vidio']; ?>"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    class="video-pengenalan" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h5 class="card-header bg-secondary text-white"><?php echo empty($vidio['judul']) ? "-" : $vidio['judul']; ?></h5>
            </div>
            <div class="card-body">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <p><?php echo empty($vidio['paragraf']) ? "-" : $vidio['paragraf']; ?></p>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>

<!-- container -->
<!-- append theme customizer -->
<?php $this->load->view('template/template_scripts') ?>
</body>

</html>

