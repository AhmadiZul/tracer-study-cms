<div class="row">
	<div class="col-xs-12 col-lg-12">
		<form id="form_banner">
			<div class="card">
				<div class="card-body">
					<div class="form-group row">
						<label for="input_banner" class="col-md-3 col-form-label">Nama Banner (foto) <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<input type="text" class="form-control alpha-only" id="input_banner" name="input_banner" value="<?= $banner->nama_banner; ?>" maxlength="60">
							<div class="invalid-feedback" for="input_banner"></div>
						</div>
					</div>
					<div class="form-group row">
						<label for="input_keterangan" class="col-md-3 col-form-label">Keterangan Banner <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<input type="text" class="form-control alpha-only" id="input_keterangan" name="input_keterangan" value="<?= $banner->keterangan; ?>" maxlength="60">
							<div class="invalid-feedback" for="input_keterangan"></div>
						</div>
                        <input type="hidden" name="id_banner" value="<?= $banner->id_banner?>">
						<input type="hidden" name="is_publish" value="<?= $banner->is_publish ?>">
						<input type="hidden" name="oldFoto" value="<?= $banner->url_banner ?>">
					</div>
					<div class="form-group row">
						<label for="input_perguruan_tinggi" class="col-md-3 col-form-label">Asal Kampus/Sekolah <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<select id="input_perguruan_tinggi" name="input_perguruan_tinggi" class="form-control select2">
								<option value="" selected>Pilih Asal Kampus/Asal Sekolah</option>
								<?php foreach ($perguruan_tinggi as $key => $value) { ?>
									<?php if ($banner->id_perguruan_tinggi == $value['id_perguruan_tinggi']) : ?>
										<option value="<?= $value['id_perguruan_tinggi']; ?>" selected><?php echo $value['nama_resmi']; ?></option>
									<?php else : ?>
										<option value="<?= $value['id_perguruan_tinggi']; ?>"><?php echo $value['nama_resmi']; ?></option>
									<?php endif ?>
								<?php } ?>
							</select>
							<div class="invalid-feedback" for="input_perguruan_tinggi"></div>
						</div>
					</div>
					<div class="form-group row">
						<label for="url_photo" class="col-md-3 col-form-label">Upload Foto</label>
						<div id="new-upload-alumni" class="col-md-9">
							<label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
							<input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'alumni')">
							<span class="file-info-alumni text-muted">Tidak ada berkas yang dipilih</span>
							<div id="preview-alumni">

							</div>
							<div class="hint-block text-muted mt-3">
								<small>
									Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
									Ukuran file maksimal: <strong>2 MB</strong>
								</small>
							</div>
							<div class="invalid-feedback" for="url_photo"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="<?php echo base_url('banner/index') ?>" class="btn btn-warning">Kembali</a>
					<button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>


<?php $this->load->view("template/template_scripts") ?>



<script>
	$('#form_banner').on('submit', function(e) {
		e.preventDefault();

		$('#form_banner').find('input, select').removeClass('is-invalid');
		$('input').parent().removeClass('is-invalid');
		$.ajax({
			url: site_url + 'banner/Index/editBanner',
			data: new FormData(this),
			dataType: 'json',
			type: 'POST',
			contentType: false,
			processData: false,
			beforeSend: function() {
				showLoading();
			},
			success: function(data) {
				hideLoading();
				swal.fire({
					title: data.message.title,
					text: data.message.body,
					icon: 'success',
					confirmButtonColor: '#396113',
				}).then(function() {
					window.location.href = site_url + 'banner/index';
				});
			},
			error: function(jqXHR, textStatus, errorThrown) {
				hideLoading();
				let response = jqXHR.responseJSON;

				switch (jqXHR.status) {
					case 400:
						// Keadaan saat validasi
						if (Array.isArray(response.data)) {
							response.data.forEach(function({
								field,
								message
							}, index) {
								$(`[name="${field}"]`).addClass('is-invalid');
								$(`[name="${field}"]`).parent().addClass('is-invalid');
								$(`.invalid-feedback[for="${field}"]`).html(message);
							});

							// sengaja diberi timeout,
							// karena kalau tidak, saat focus ke field error,
							// akan kembali lagi men-scroll ke tempat scroll sebelumnya
							setTimeout(function() {
								if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
									$(`[name="${response.data[0]['field']}"]`).focus();
								} else {
									$(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
								};
							}, 650);
						};
						break;
					case 500:
						swal.fire({
							title: response.message.title,
							html: response.message.body,
							icon: 'error',
							confirmButtonColor: '#396113',
						})
						break;
					default:
						break;
				};
			}
		});
	})
</script>