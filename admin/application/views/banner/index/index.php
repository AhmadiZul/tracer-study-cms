<!-- Map area -->
<div class="card border-0 bg-white-9 rounded-xl p-0 mb-3">
    <div class="card-body">
        <?php echo $output->output; ?>
    </div>
</div>
<br>
</div>
<!-- container -->
<!-- append theme customizer -->
<?php $this->load->view('template/template_scripts') ?>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>

<script>
    $(document).ready(function() {
        var baseUrl = "<?php echo base_url() ?>"
        window.addEventListener('gcrud.datagrid.ready', () => {
            $(".gc-header-tools").append(
                `<div class="floatL">
            <button id="btn-tambah" class="btn btn-default btn-outline-dark t5">
              <i class="fa fa-plus floatL t3"></i>
              <span class="hidden-xs floatL l5">Tambah Banner</span>
              <div class="clear">
              </div>
            </button>
          </div>`
            );
            $("#btn-tambah").click(function() {
                window.location.href = baseUrl + "banner/index/add_banner";
            });
        });
    });

    function publish(id) {
            // console.log(id)
            var formData = {
                id_banner:id,
                is_publish: '1'
            };
            $.ajax({
                type: 'ajax',
                method: 'POST',
                url: site_url + 'banner/index/publishBanner',
                data: formData,
                dataType: 'json',
                encode: true,
                success: function(response) {
                    if (response.success) {
                        Swal.fire('Proses Berhasil!', 'Berhasil Dipublikasikan', 'success').then(function() {
                            $('.fa-refresh').trigger('click');
                        })
                    } else {
                        Swal.fire('Proses Gagal!', response.message, 'error');
                    }
                },
                error: function(xmlresponse) {
                    console.log(xmlresponse);
                }
            });
        }

        function noPublish(id) {
            // console.log(id)
            var formData = {
                id_banner:id,
                is_publish: '0'
            };
            console.log(formData)
            $.ajax({
                type: 'ajax',
                method: 'POST',
                url: site_url + 'banner/index/publishBanner',
                data: formData,
                dataType: 'json',
                encode: true,
                success: function(response) {
                    if (response.success) {
                        Swal.fire('Proses Berhasil!', 'Berhasil Di Non Publikasikan', 'success').then(function() {
                            $('.fa-refresh').trigger('click');
                        })
                    } else {
                        Swal.fire('Proses Gagal!', response.message, 'error');
                    }
                },
                error: function(xmlresponse) {
                    console.log(xmlresponse);
                }
            });
        }
</script>