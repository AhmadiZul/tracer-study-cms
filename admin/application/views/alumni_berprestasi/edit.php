<div class="card">
    <div class="card-body">
        <form id="form_tambah">
            <input type="hidden" class="form-control" name="id" id="id" value="1">
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="nama_prestasi">Prestasi <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="nama_prestasi" id="nama_prestasi" maxlength="50" placeholder="Masukkan Prestasi Alumni" value="<?= $alumni_prestasi->nama_prestasi ?>">
                    <div class="invalid-feedback" for="nama_prestasi"></div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="kesan">Kesan <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="kesan" id="kesan" maxlength="50" placeholder="Masukkan Pesan & Kesan" value="<?= $alumni_prestasi->kesan ?>">
                    <div class="invalid-feedback" for="kesan"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="input_fakultas" class="col-md-3 col-form-label">Fakultas <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <select id="input_fakultas" name="input_fakultas" class="form-control select2">
                        <option value="" selected>Pilih Fakultas</option>
                        <?php foreach ($fakultas as $key => $value) { ?>
                            <?php if ($alumni_prestasi->id_fakultas == $value['id_fakultas']) : ?>
                                <option value="<?= $value['id_fakultas']; ?>" selected><?php echo $value['nama_fakultas']; ?></option>
                            <?php else : ?>
                                <option value="<?= $value['id_fakultas']; ?>"><?php echo $value['nama_fakultas']; ?></option>
                            <?php endif ?>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback" for="input_fakultas"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="input_prodi" class="col-md-3 col-form-label">Program Studi/Jurusan <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <select id="input_prodi" name="input_prodi" class="form-control select2">
                        <option value="" selected>Pilih Program Studi/Jurusan</option>
                        <?php foreach ($prodi as $key => $value) { ?>
                            <?php if ($alumni_prestasi->id_prodi == $value['id_prodi']) : ?>
                                <option value="<?= $value['id_prodi']; ?>" selected><?php echo $value['nama_prodi']; ?></option>
                            <?php else : ?>
                                <option value="<?= $value['id_prodi']; ?>"><?php echo $value['nama_prodi']; ?></option>
                            <?php endif ?>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback" for="input_prodi"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="input_alumni" class="col-md-3 col-form-label">Nama Alumni <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="tel" name="input_alumni" id="input_alumni" value="<?= $alumni_prestasi->nama_alumni ?>" class="form-control" maxlength="15">
                    <div class="invalid-feedback" for="input_alumni"></div>
                </div>
            </div>
            <input type="hidden" name="id" value="<?= $alumni_prestasi->id ?>">
            <input type="hidden" name="is_publish" value="<?= $alumni_prestasi->is_publish ?>">
            <input type="hidden" name="oldFoto" value="<?= $alumni_prestasi->url_foto ?>">
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="url_photo">Foto <b class="text-danger">*</b></label>
                </div>
                <div id="new-upload-logo-perguruan" class="col-md-9">
                    <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                    <input type="file" name="url_photo" accept=".png, .jpg, .jpeg " id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo-perguruan')">
                    <span class="file-info-logo-perguruan text-muted">Tidak ada berkas yang dipilih</span>
                    <div id="preview-logo-perguruan">

                    </div>
                    <div class="hint-block text-muted mt-3">
                        <small>
                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                            Ukuran file maksimal: <strong>2 MB</strong>
                        </small>
                    </div>
                    <div class="invalid-feedback" for="url_photo"></div>
                    <small class="text-danger">Gambar maksimal berukuran  354x472 px </small>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url('alumni_berprestasi/index') ?>" class="btn btn-warning">Kembali</a>
                <button type="submit" class="btn btn-success" id="btn-simpan-alumni-prestasi">Simpan</button>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view("template/template_scripts") ?>

<script>
    // simpan legalisir
    $('#form_tambah').on('submit', function(e) {
        e.preventDefault();

        $('#form_tambah').find('input, select,select2').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + "alumni_berprestasi/index/editPrestasi",
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    title: data.message.title,
                    text: data.message.body,
                    icon: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = site_url + 'alumni_berprestasi/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    });

    $('[name="input_fakultas"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'alumni_berprestasi/Index/getprodi',
            type: "GET",
            data: {
                kode_prodi: val
            },
            dataType: "JSON",
            success: function(data) {
                buatProdi(data);
                $('[name="input_prodi"]').select2({
                    placeholder: '- Pilih Program Studi/Jurusan -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatProdi(data) {
        let optionLoop = '<option value>Pilih Program Studi/Jurusan</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        if ($('[name="input_prodi"]')) {
            $('[name="input_prodi"]').children().remove();
        }
        $('[name="input_prodi"]').append(optionLoop);
    }
</script>