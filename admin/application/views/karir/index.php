<!-- Map area -->
<div class="card border-0 bg-white-9 rounded-xl p-0 mb-3">
    <div class="card-body">
        <?php echo $output->output; ?>
    </div>
</div>
<br>
</div>
<!-- container -->
<!-- append theme customizer -->
<?php $this->load->view('template/template_scripts') ?>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>
<script>
    function verifikasi(id, jenis) {
        Swal.fire({
            icon: 'info',
            title: 'Perhatian!',
            text: 'Apakah anda yakin akan menerima peserta ?',
            showCancelButton: true,
            confirmButtonText: 'Ya verifikasi !',
            confirmButtonColor: '#0D9448',
            cancelButtonText: 'Tidak',
            cancelButtonColor: '#d33'
        }).then(function(isvalid) {
            if (isvalid.value) {
                $.ajax({
                    type: 'ajax',
                    method: 'post',
                    url: site_url + '/mobilizer/verifikasi',
                    data: {
                        id: id,
                        jenis: jenis
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        showLoading();
                    },
                    success: function(response) {
                        hideLoading();
                        if (response.success) {
                            Swal.fire('Sukses!', response.message, 'success').then(function() {
                                $('.fa-refresh').trigger('click');
                            })
                        } else {
                            Swal.fire('Gagal!', response.message, 'error');
                        }
                    },
                    error: function(xmlresponse) {
                        console.log(xmlresponse);
                    }
                })
            }
        })
    }
</script>