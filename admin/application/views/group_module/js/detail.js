$(function () {
    loadModule();
    $("#form-module").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'ajax',
            method: 'POST',
            url: site_url + '/group_module/index/updateModule',
            data: $(this).serialize(),
            dataType: 'json',
            // beforeSend: function () { },
            success: function (response) {
                if (response.status==201) {
                    iziToast.success({
                        title: 'Proses Berhasil !',
                        message: response.message,
                        position: 'topRight',
                        timeout: 7000
                    });
                    loadModule();
                } else {
                    iziToast.error({
                        title: 'Proses Gagal !',
                        message: response.message,
                        position: 'topRight',
                        timeout: 7000
                    });

                }
            },
            error: function (xmlresponse) {
                console.log(xmlresponse);
            },
        });
    })
})

function loadModule() {
    $.ajax({
        type: 'ajax',
        method: 'POST',
        url: site_url + '/group_module/index/loadModule',
        data: { id_group: id_group },
        dataType: 'json',
        // beforeSend: function () { },
        success: function (response) {
            var html = '';
            if (response.status==201) {
                html +='<div class="row">';
                if (response.data.module.length!=0) {
                    response.data.module.forEach(element => {
                        html += '<div class="col-md-4">\
                            <div class="form-group">\
                                <div class="custom-control custom-checkbox">\
                                    <input type="checkbox" class="custom-control-input" name="module[]" value="'+element.nama_modul+'" id="'+element.nama_modul+'" '+(response.data.modul_group.includes(element.nama_modul)?"checked":"")+'>\
                                    <label class="custom-control-label" for="'+element.nama_modul+'">'+element.nama_modul+'</label>\
                                </div>\
                            </div>\
                        </div>';
                    });
                }
                html +='</div>';
            }else{
                html = '<div class="alert alert-info"><b>Infomasi !</b><p>Data modul belum diatur admin</p></div>';
            }
            $('#container-module').html(html);
        },
        error: function (xmlresponse) {
            console.log(xmlresponse);
        },
    });

    // <label for="input"><input type="checkbox" name="module[]" value="'+element.nama_modul+'" '+(response.data.modul_group.includes(element.nama_modul)?"checked":"")+'>&nbsp;'+element.nama_modul+'</label>\
}