<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-header with-bordered">
                <b class="card-title">Akses Group Module</b>
            </div>
            <div class="card-body">
                <form action="" id="form-module">
                    <div id="container-module"></div>
                    <input type="hidden" name="id_group" value="<?php echo $id ?>">
                    <div class="d-flex justify-content-center">
                        <button class="btn btn-success btn-lg" type="submit">Simpan Perubahan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>

<script>
    var id_group = '<?php echo $id ?>';
</script>