<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <small>
                    <?php echo $output->output; ?>
                </small>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>



<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>

<script>
      $(document).ready(function() {
    window.addEventListener('gcrud.datagrid.ready', () => {
      $(".gc-header-tools").append(
        ` <div class="l5 mr-1">
            <button id="btn-tambahSurvey" class="btn btn-default btn-outline-dark t5">
              <i class="fa fa-plus floatL t3"></i>
              <span class="hidden-xs floatL l5">Tambah Jadwal Survey</span>
              <div class="clear">
              </div>
            </button>
          </div>`
      );

      $('#btn-tambahSurvey').click(function(e) {
        e.preventDefault();
        window.location.href = site_url + 'jadwal_survey/index/add_survey';
      })
    });
  })
</script>