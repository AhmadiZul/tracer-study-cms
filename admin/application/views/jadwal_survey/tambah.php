<div class="card">
    <div class="card-body">
        <form id="form_tambah">
            <div class="form-group row">
                <label for="jenis_survey" class="col-md-3 col-form-label">Jenis Survey <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <select id="jenis_survey" name="jenis_survey" class="form-control select2">
                        <option value="">Pilih Jenis Survey</option>
                        <?php foreach ($jenis_survey as $key => $value) { ?>
                            <option value="<?= $value['id']; ?>"><?php echo $value['jenis_survey']; ?></option>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback" for="jenis_survey"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="tahun_lulus" class="col-md-3 col-form-label">Tahun Lulus <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <select id="tahun_lulus" name="tahun_lulus" class="form-control select2">
                        <option value="" selected>Pilih Tahun Lulus</option>
                        <?php foreach ($tahun_lulus as $key => $value) { ?>
                            <option value="<?= $value['tahun']; ?>"><?php echo $value['tahun']; ?></option>
                        <?php } ?>
                    </select>
                    <div class="invalid-feedback" for="tahun_lulus"></div>
                </div>
            </div>
            <?php if ($this->session->userdata("tracer_idGroup") == 2) { ?>
                <div class="form-group row">
                    <label for="input_fakultas" class="col-md-3 col-form-label">Fakultas <b class="text-danger">*</b></label>
                    <div class="col-md-9">
                        <select id="input_fakultas" name="input_fakultas" class="form-control select2">
                            <option value="<?= $id_fakultas ?>"> <?= $nama_fakultas ?></option>
                        </select>
                        <div class="invalid-feedback" for="input_fakultas"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input_prodi" class="col-md-3 col-form-label">Program Studi/Jurusan <b class="text-danger">*</b></label>
                    <div class="col-md-9">
                        <select id="input_prodi" name="input_prodi" class="form-control select2">
                            <?php foreach ($prodi as $key => $value) { ?>
                                <option value="<?= $value['id_prodi'] ?>" selected><?= $value['nama_prodi'] ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback" for="input_prodi"></div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="form-group row">
                    <label for="input_fakultas" class="col-md-3 col-form-label">Fakultas <b class="text-danger">*</b></label>
                    <div class="col-md-9">
                        <select id="input_fakultas" name="input_fakultas" class="form-control select2">
                            <option value="" selected>Pilih Fakultas</option>
                            <?php foreach ($fakultas as $key => $value) { ?>
                                <option value="<?= $value['id_fakultas']; ?>"><?php echo $value['nama_fakultas']; ?></option>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback" for="input_fakultas"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input_prodi" class="col-md-3 col-form-label">Program Studi/Jurusan <b class="text-danger">*</b></label>
                    <div class="col-md-9">
                        <select id="input_prodi" name="input_prodi" class="form-control select2">
                            <option value="" selected>Pilih Program Studi/Jurusan</option>
                            <?php if ($prodi) { ?>
                                <?php foreach ($prodi as $key => $value) { ?>
                                    <option value="<?= $key; ?>" <?php echo ($key == $data['id_prodi'] ? 'selected' : '') ?>><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <div class="invalid-feedback" for="input_prodi"></div>
                    </div>
                </div>
            <?php } ?>
            <div class="form-group row">
                <label for="tanggal_mulai" class="col-md-3 col-form-label">Tanggal Mulai<b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" name="tanggal_mulai" id="tanggal_mulai" class="form-control tanggal-mulai" placeholder="masukan tanggal mulai">
                    <div class="invalid-feedback" for="tanggal_mulai" onkeydown="return false"></div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="tanggal_selesai">Tanggal Selesai <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-9">
                    <input type="text" class="form-control tanggal-selesai" name="tanggal_selesai" id="tanggal_selesai" placeholder="masukkan tanggal selesai">
                    <div class="invalid-feedback" for="tanggal_selesai"></div>
                    <input type="hidden" name="cek_tanggal">
                    <div class="invalid-feedback" for="cek_tanggal"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="time" class="col-md-3 col-form-label">Waktu Survey <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-3 mr-4">
                            Waktu Mulai
                            <input type="time" class="form-control" name="waktu_acara"  />
                            <div class="invalid-feedback" for="waktu_acara"></div>
                        </div>
                        <div class="col-md-3">
                            Waktu Berakhir
                            <input type="time" class="form-control" name="waktu_berakhir"  />
                            <div class="invalid-feedback" for="cek_berakhir"></div>
                        </div>
                    </div>
                    <input type="hidden" name="cek_waktu">
                    <div class="invalid-feedback" for="cek_waktu"></div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url('jadwal_survey/index') ?>" class="btn btn-theme-dark">Kembali</a>
                <button type="submit" class="btn btn-theme-danger" id="btn-simpan-alumni-prestasi">Simpan</button>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view("template/template_scripts") ?>

<script>
    $('#form_tambah').on('submit', function(e) {
        e.preventDefault();

        $('#form_tambah').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + "jadwal_survey/index/createSurvey",
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil menambahkan jadwal survey',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'jadwal_survey/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    });

    /**
     * select prodi
     */
    $('[name="input_fakultas"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'alumni_berprestasi/Index/getprodi',
            type: "GET",
            data: {
                kode_prodi: val
            },
            dataType: "JSON",
            success: function(data) {
                buatProdi(data);
                $('[name="input_prodi"]').select2({
                    placeholder: '- Pilih Program Studi/Jurusan -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatProdi(data) {
        let optionLoop = '<option value>Pilih Program Studi/Jurusan</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        if ($('[name="input_prodi"]')) {
            $('[name="input_prodi"]').children().remove();
        }
        $('[name="input_prodi"]').append(optionLoop);
    }

    $(".tanggal-mulai").datepicker({
		startDate: new Date(),
		format: 'yyyy-mm-dd',
		language: 'id',
		daysOfWeekHighlighted: "0",
		autoclose: true,
		todayHighlight: true
	});

	const today = new Date();
	const tomorrow = new Date(today);
	tomorrow.setDate(tomorrow.getDate() + 1);

	$(".tanggal-selesai").datepicker({
		startDate: today,
		// datesDisabled: new Date(),
		format: 'yyyy-mm-dd',
		language: 'id',
		daysOfWeekHighlighted: "0",
		autoclose: true,
		// todayHighlight: true

	});
</script>