<div class="row">
  <div class="sistem">
  </div>
</div>
<style type="text/css">
  img {
    position: relative;
    z-index: 1;
    top: 0px;
  }

  .title-atas {
    position: absolute;
    top: 45px;
    left: 130px;
    z-index: 2;
    color: #fff;
  }
</style>
<div class="row">
  <div class="col-xs-12 col-lg-12">
    <div class="card" style="border-radius:20px;">
      <div class="col-md-5">
        <h2 class="title-atas"><?= $perguruan_tinggi->nama_resmi ?></h2>
      </div>
      <div id="bg-setting">
        <img src="<?php echo base_url() . 'public/assets/img/sidebar/bg_perguruan.png' ?>" alt="..." style="border-radius:20px;" width="100%">
      </div>
      <div class="card-header">
        <ul class="nav navbar-tabs">
          <li class="nav-item nav-link"><a data-toggle="tab" href="#home" data-bs-toggle="tab" class="text-tema" id="home-tab" aria-controls="home" aria-selected="false"><strong>Informasi Perguruan</strong></a></li>
          <li class="nav-item nav-link"><a data-toggle="tab" href="#fakultas" data-bs-toggle="tab" class="text-tema" id="fakultas-tab" aria-controls="fakultas" aria-selected="false"><strong>Fakultas</strong></a></li>
          <li class="nav-item nav-link"><a data-toggle="tab" href="#akun" data-bs-toggle="tab" class="text-tema" id="akun-tab" aria-controls="akun" aria-selected="false"><strong>Akun</strong></a></li>
        </ul>
      </div>
      <div class="card-body">
        <div class="tab-content">
          <div id="home" class="tab-pane fade show active">
            <?php if($this->session->userdata("tracer_idGroup") == 2){?>
            <?php $this->load->view('perguruan_tinggi/admin_fakultas/perguruan_tinggi'); 
          }else{ ?>
          <?php $this->load->view('perguruan_tinggi/perguruan_tinggi'); 
          } ?>
          </div>
          <div id="fakultas" class="tab-pane fade show">
            <?php $this->load->view('perguruan_tinggi/fakultas'); ?>
          </div>
          <div id="akun" class="tab-pane fade show">
            <?php $this->load->view('perguruan_tinggi/akun'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view("template/template_scripts") ?>
<?php
if (isset($output->js_files)) {
  foreach ($output->js_files as $file) {
    echo '<script src="' . $file . '"></script>';
  }
}
?>

<script>
  $('[name="input_provinsi"]').change(function(e) {
    var val = e.target.value;
    $.ajax({
      url: '<?= base_url() ?>' + '/alumni/dashboard/getKabKota',
      type: "GET",
      data: {
        kode_provinsi: val
      },
      dataType: "JSON",
      success: function(data) {
        buatKabKot(data);
        $('[name="input_kab"]').select2({
          placeholder: 'Pilih Kabupaten/Kota',
        });
      },
      error: function(xx) {
        alert(xx)
      }
    });
  });

  function buatKabKot(data) {
    let optionLoop = '<option value>Pilih Kabupaten / Kota</option>';
    Object.keys(data).forEach(key => {
      optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
    });
    if ($('[name="input_kab"]')) {
      $('[name="input_kab"]').children().remove();
    }
    $('[name="input_kab"]').append(optionLoop);
  }

  $('#form_univ').on('submit', function(e) {
    e.preventDefault();

    $('#form_univ').find('input, select').removeClass('is-invalid');
    $('input').parent().removeClass('is-invalid');
    $.ajax({
      url: site_url + 'perguruan_tinggi/index/editPerguruanTinggi',
      data: new FormData(this),
      dataType: 'json',
      type: 'POST',
      contentType: false,
      processData: false,
      beforeSend: function() {
        showLoading();
      },
      success: function(data) {
        hideLoading();
        swal.fire({
          title: data.message.title,
          text: data.message.body,
          icon: 'success',
          confirmButtonColor: '#396113',
        }).then(function() {
          window.location.href = site_url + 'perguruan_tinggi/index';
        });
      },
      error: function(jqXHR, textStatus, errorThrown) {
        hideLoading();
        let response = jqXHR.responseJSON;

        switch (jqXHR.status) {
          case 400:
            // Keadaan saat validasi
            if (Array.isArray(response.data)) {
              response.data.forEach(function({
                field,
                message
              }, index) {
                $(`[name="${field}"]`).addClass('is-invalid');
                $(`[name="${field}"]`).parent().addClass('is-invalid');
                $(`.invalid-feedback[for="${field}"]`).html(message);
              });

              // sengaja diberi timeout,
              // karena kalau tidak, saat focus ke field error,
              // akan kembali lagi men-scroll ke tempat scroll sebelumnya
              setTimeout(function() {
                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                  $(`[name="${response.data[0]['field']}"]`).focus();
                } else {
                  $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                };
              }, 650);
            };
            break;
          case 500:
            swal.fire({
              title: response.message.title,
              html: response.message.body,
              icon: 'error',
              confirmButtonColor: '#396113',
            })
            break;
          default:
            break;
        };
      }
    });
  })

  $('#form_akun').on('submit', function(e) {
    e.preventDefault();

    $('#form_akun').find('input, select').removeClass('is-invalid');
    $('input').parent().removeClass('is-invalid');
    $.ajax({
      url: site_url + 'perguruan_tinggi/index/editAkun',
      data: new FormData(this),
      dataType: 'json',
      type: 'POST',
      contentType: false,
      processData: false,
      beforeSend: function() {
        showLoading();
      },
      success: function(data) {
        hideLoading();
        swal.fire({
          title: data.message.title,
          text: data.message.body,
          icon: 'success',
          confirmButtonColor: '#396113',
        }).then(function() {
          window.location.href = site_url + 'perguruan_tinggi/index';
        });
      },
      error: function(jqXHR, textStatus, errorThrown) {
        hideLoading();
        let response = jqXHR.responseJSON;

        switch (jqXHR.status) {
          case 400:
            // Keadaan saat validasi
            if (Array.isArray(response.data)) {
              response.data.forEach(function({
                field,
                message
              }, index) {
                $(`[name="${field}"]`).addClass('is-invalid');
                $(`[name="${field}"]`).parent().addClass('is-invalid');
                $(`.invalid-feedback[for="${field}"]`).html(message);
              });

              // sengaja diberi timeout,
              // karena kalau tidak, saat focus ke field error,
              // akan kembali lagi men-scroll ke tempat scroll sebelumnya
              setTimeout(function() {
                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                  $(`[name="${response.data[0]['field']}"]`).focus();
                } else {
                  $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                };
              }, 650);
            };
            break;
          case 500:
            swal.fire({
              title: response.message.title,
              html: response.message.body,
              icon: 'error',
              confirmButtonColor: '#396113',
            })
            break;
          default:
            break;
        };
      }
    });
  })
</script>