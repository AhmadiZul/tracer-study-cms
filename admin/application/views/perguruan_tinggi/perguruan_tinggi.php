<form id="form_univ">
  <h5 class="text-dark"><span class="badge badge-pill badge-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Data Perguruan Tinggi</h5>
  <div class="form-group row">
    <label for="input_npsn" class="col-md-3 col-form-label">NPSN <b class="text-danger">*</b></label>
    <div class="col-md-9">
      <input type="hidden" name="id_perguruan_tinggi" value="<?= $perguruan_tinggi->id_perguruan_tinggi ?>">
      <input type="text" class="form-control number-only" id="input_npsn" name="input_npsn" value="<?= $perguruan_tinggi->npsn ?>" maxlength="8">
      <div class="invalid-feedback" for="input_npsn"></div>
    </div>
  </div>
  <div class="form-group row">
    <label for="input_nama" class="col-md-3 col-form-label">Nama Perguruan Tinggi <b class="text-danger">*</b></label>
    <div class="col-md-9">
      <input type="text" class="form-control alpha-only" id="input_nama" name="input_nama" value="<?= $perguruan_tinggi->nama_resmi ?>" onpaste="return false" maxlength="25">
      <div class="invalid-feedback" for="input_nama"></div>
    </div>
  </div>
  <div class="form-group row">
    <label for="input_nama_pendek" class="col-md-3 col-form-label">Nama Pendek <b class="text-danger">*</b></label>
    <div class="col-md-9">
      <input type="text" class="form-control alpha-only" id="input_nama_pendek" name="input_nama_pendek" value="<?= $perguruan_tinggi->nama_pendek ?>" onpaste="return false" maxlength="5">
      <div class="invalid-feedback" for="input_nama_pendek"></div>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-3">
      <label for="input_email" class="form-label">Email <b class="text-danger">*</b></label><br>
    </div>
    <div class="col-md-9">
      <input type="text" class="form-control" id="input_email" name="input_email" value="<?= $perguruan_tinggi->email ?>" maxlength="50">
      <div class="invalid-feedback" for="input_email"></div>
      <small class="text-danger">example: info@arkatama.id</small>
    </div>
  </div>
  <div class="form-group row">
    <label for="input_telepon" class="col-md-3 col-form-label">Nomor Telepon <b class="text-danger">*</b></label>
    <div class="col-md-9">
      <input type="text" class="form-control number-only" id="input_telepon" name="input_telepon" value="<?= $perguruan_tinggi->no_telp ?>" maxlength="13">
      <div class="invalid-feedback" for="input_telepon"></div>
    </div>
  </div>
  <div class="form-group row">
    <label for="input_fax" class="col-md-3 col-form-label">No. Fax <b class="text-danger">*</b></label>
    <div class="col-md-9">
      <input type="text" class="form-control number-only" id="input_fax" name="input_fax" value="<?= $perguruan_tinggi->no_fax ?>" maxlength="11">
      <div class="invalid-feedback" for="input_fax"></div>
      <small class="text-danger">example: 0211234567</small>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-3">
      <label for="input_link" class="form-label">Link Website <b class="text-danger">*</b></label><br>
    </div>
    <div class="col-md-9">
      <input type="text" class="form-control" id="input_link" name="input_link" value="<?= $perguruan_tinggi->website ?>" maxlength="100">
      <div class="invalid-feedback" for="input_link"></div>
      <small class="text-danger">example: https://arkatama.id/</small>
    </div>
  </div>
  <div class="form-group row">

    <label for="url_photo" class="col-md-3 col-form-label">Upload Foto <b class="text-danger">*</b></label>
    <?php if (isset($perguruan_tinggi->path_logo)) { ?>
      <div class="col-md-3">
        <input type="hidden" name="oldLogo" value="<?= $perguruan_tinggi->path_logo ?>">
        <div id="ganti-berkas-logo" class="col-md-3">
          <img src="<?= base_url() . $perguruan_tinggi->path_logo ?>" alt="your image" style="width: 200px;" /><br>
        </div>
      </div>
    <?php } ?>
    <div id="new-upload-alumni" class="col-md-4">
      <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
      <input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'alumni')">
      <span class="file-info-alumni text-muted">Tidak ada berkas yang dipilih</span>
      <div id="preview-alumni">

      </div>
      <div class="hint-block text-muted mt-3">
        <small>
          Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
          Ukuran file maksimal: <strong>2 MB</strong>
        </small>
      </div>
      <div class="invalid-feedback" for="url_photo"></div>
    </div>
  </div>
  <hr>
  <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Alamat Perguruan Tinggi</h5>
  <div class="form-group row">
    <label for="input_provinsi" class="col-md-3 col-form-label">Provinsi <b class="text-danger">*</b></label>
    <div class="col-md-9">
      <select id="input_provinsi" name="input_provinsi" class="form-control select2">
        <option value="" selected>Pilih Provinsi</option>
        <?php foreach ($propinsi as $key => $value) { ?>
          <option value="<?= $key; ?>" <?php echo ($perguruan_tinggi->kode_provinsi == $key ? 'selected' : ''); ?>><?php echo $value; ?></option>
        <?php } ?>
      </select>
      <div class="invalid-feedback" for="input_provinsi"></div>
    </div>
  </div>
  <div class="form-group row">
    <label for="input_kab" class="col-md-3 col-form-label">Kabupaten/Kota <b class="text-danger">*</b></label>
    <div class="col-md-9">
      <select id="input_kab" name="input_kab" class="form-control select2">
        <option value="" selected>Pilih Kab/Kota</option>
        <?php if ($kab_kota) { ?>
          <?php foreach ($kab_kota as $key => $value) { ?>
            <option value="<?= $key; ?>" <?php echo ($key == $perguruan_tinggi->kode_kab_kota ? 'selected' : ''); ?>><?php echo $value; ?></option>
          <?php } ?>
        <?php } ?>
      </select>
      <div class="invalid-feedback" for="input_kab"></div>
    </div>
  </div>
  <div class="form-group row">
    <label for="input_kode" class="col-md-3 col-form-label">Kode Pos <b class="text-danger">*</b></label>
    <div class="col-md-9">
      <input type="text" class="form-control number-only" id="input_kode" name="input_kode" value="<?= $perguruan_tinggi->kode_pos ?>" maxlength="5">
      <div class="invalid-feedback" for="input_kode"></div>
    </div>
  </div>
  <div class="form-group row">
    <label for="input_alamat" class="col-md-3 col-form-label">Alamat Perguruan Tinggi <b class="text-danger">*</b></label>
    <div class="col-md-9">
      <textarea class="form-control" name="input_alamat" id="input_alamat" style="height:100%;" maxlength="255"><?= $perguruan_tinggi->alamat ?></textarea>
      <div class="invalid-feedback" for="input_alamat"></div>
    </div>
  </div>
  <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
</form>