<div class="card">
    <div class="top-border"></div>
    <div class="card-body">
        <form id="form_tambah">
            <input type="hidden" class="form-control" name="is_berkas" id="is_berkas" value="1">
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="npsn">NPSN <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <div class="input-group">
                        <input type="text" class="input" name="npsn" id="npsn" maxlength="8">
                        <label for="npsn" class="input-label">Masukan NPSN</label>
                        <div class="invalid-feedback" for="npsn">Wajib diisi</div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="kode">Kode <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <div class="input-group">
                        <input type="text" class="input" name="kode" id="kode" maxlength="8">
                        <label for="kode" class="input-label">Masukan kode</label>
                        <div class="invalid-feedback" for="kode">Wajib diisi</div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="nama_resmi">Nama Resmi <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <div class="input-group">
                        <input type="text" class="input" name="nama_resmi" id="nama_resmi" maxlength="100">
                        <label for="nama_resmi" class="input-label">Masukan nama resmi</label>
                        <div class="invalid-feedback" for="nama_resmi">Wajib diisi</div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="nama_pendek">Nama pendek <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="nama_pendek" id="nama_pendek" maxlength="20" placeholder="Masukkan nama pendek">
                    <div class="invalid-feedback" for="nama_pendek">Wajib diisi</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="email">Email <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="email" id="email" maxlength="40" placeholder="Masukkan Email">
                    <div class="invalid-feedback" for="email">Wajib diisi</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="no_telp">No. Telephone <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="no_telp" id="no_telp" maxlength="15" placeholder="Masukkan No. Telephone">
                    <div class="invalid-feedback" for="no_telp">Wajib diisi</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="no_fax">No. Fax</label>
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="no_fax" id="no_fax" maxlength="40" placeholder="Masukkan No. Fax">
                    <div class="invalid-feedback" for="no_fax">Wajib diisi</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="kota">Kota <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="kota" id="kota" maxlength="30" placeholder="Masukkan Kota">
                    <div class="invalid-feedback" for="kota">Wajib diisi</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="kode_pos">Kode Pos <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="kode_pos" id="kode_pos" maxlength="10" placeholder="Masukkan Kode Pos">
                    <div class="invalid-feedback" for="kode_pos">Wajib diisi</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="alamat">Alamat <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <textarea name="alamat" id="alamat" style="height: 100px;" class="form-control" maxlength="200" placeholder="Masukkan Alamat"></textarea>
                    <div class="invalid-feedback" for="alamat">Wajib diisi</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="website">Link Website <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="website" id="website" maxlength="50" placeholder="Masukkan Link Website">
                    <small class="text-info">Contoh : https://www.pusdiktan.id</small>
                    <div class="invalid-feedback" for="website">Wajib diisi</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="web_tracer">Link Tracer</label>
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="web_tracer" id="web_tracer" maxlength="100" placeholder="Masukkan Link Tracer">
                    <small class="text-info">Contoh : https://www.pusdiktan.id</small>
                    <div class="invalid-feedback" for="web_tracer">Wajib diisi</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="url_photo">Logo <b class="text-danger">*</b></label>
                </div>
                <div id="new-upload-logo-perguruan" class="col-md-10">
                    <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                    <input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo-perguruan')">
                    <span class="file-info-logo-perguruan text-muted">Tidak ada berkas yang dipilih</span>
                    <div id="preview-logo-perguruan">

                    </div>
                    <div class="hint-block text-muted mt-3">
                        <small>
                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                            Ukuran file maksimal: <strong>2 MB</strong>
                        </small>
                    </div>
                    <div class="invalid-feedback" for="url_photo"></div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="username">Username <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="username" id="username" maxlength="30" placeholder="Masukkan Username">
                    <div class="invalid-feedback" for="username">Wajib diisi</div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="password">Password <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <div class="input-group">
                        <input type="password" class="input-password" name="password" id="password">
                        <label for="password" class="input-label">Masukan Password</label>
                        <a class="input-text show-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                        <div class="invalid-feedback" for="npsn">Wajib diisi</div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    <label for="confirm_password">Konfirm Password <b class="text-danger">*</b></label>
                </div>
                <div class="col-md-10">
                    <div class="input-group">
                        <input type="password" class="input-password" name="confirm_password" id="confirm-password">
                        <label for="password" class="input-label">Masukan Konfirm Password</label>
                        <a class="input-text show-confirm-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                        <div class="invalid-feedback" for="confirm_password">Wajib diisi</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url('perguruan_tinggi') ?>" class="btn btn-warning">Kembali</a>
                <button type="submit" class="btn btn-success" id="btn-simpan-perguruan-tinggi">Simpan</button>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view("template/template_scripts") ?>

<script>
    // simpan legalisir
    $('#form_tambah').on('submit', function(e) {
        e.preventDefault();

        $('#form_tambah').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + "perguruan_tinggi/index/simpanPerguruanTinggi",
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    title: data.message.title,
                    text: data.message.body,
                    icon: 'success',
                    confirmButtonColor: '#396113',
                }).then(function() {
                    window.location.href = site_url + 'perguruan_tinggi';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    });
</script>