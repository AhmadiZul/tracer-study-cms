<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
            <h4 class="mg-b-0 tx-spacing-1"><a href="<?php echo base_url('perguruan_tinggi/index/index') ?>" class="btn btn-warning text-white btn-xs"><i class="fas fa-arrow-left"></i> Kembali</a></h4>
                <small>
                    <?php echo $output->output; ?>
                </small>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>