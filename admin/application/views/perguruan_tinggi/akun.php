<form id="form_akun">
    <div class="form-group row">
        <label for="input_username" class="col-md-3 col-form-label">Username <b class="text-danger">*</b></label>
        <div class="col-md-9">
            <input type="text" class="form-control" id="input_username" name="input_username" value="<?= $user['username'] ?>">
            <div class="invalid-feedback" for="input_username"></div>
        </div>
    </div>
    <div class="form-group row">
        <label for="password" class="col-md-3 col-form-label">Password <b class="text-danger">*</b></label>
        <div class="col-md-9">
            <input type="password" class="form-control" id="password" name="password" value="">
            <div class="invalid-feedback" for="password"></div>
        </div>
    </div>
    <div class="form-group row">
        <label for="konfirmasi_password" class="col-md-3 col-form-label">Konfirmasi Password <b class="text-danger">*</b></label>
        <div class="col-md-9">
            <input type="password" class="form-control" id="konfirmasi_password" name="konfirmasi_password" value="">
            <div class="invalid-feedback" for="konfirmasi_password"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="btn-simpan-alumni">Simpan Perubahan</button>
    </div>
</form>