<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-md-3">
                        <img class=" mt-3" width="100%" style="border-radius:20px;" src="<?= base_url() . $perguruan_tinggi->path_logo ?>" alt="your image" /><br>
                    </div>
                    <div class="col-md-7">
                        <p class="fw-bold fs-5 mt-4">
                        <h2 class="text-dark"><?= $perguruan_tinggi->nama_resmi . " (" . $perguruan_tinggi->nama_pendek . ")" ?></h2>
                        </p>
                        <p class="mb-4"><?= $perguruan_tinggi->npsn ?></p>
                        <p class="fs-5 mt-4"><i class="fa fa-phone pr-3"></i><?= $perguruan_tinggi->no_telp ?></p>
                        <p class="fs-5 mt-4"><i class="fa fa-envelope pr-3"></i> <?= $perguruan_tinggi->email ?></p>
                        <p class="fs-5 mt-4"><i class="fa fa fa-globe pr-3"></i> <?= $perguruan_tinggi->website ?></p>
                    </div>
                </div>
            </div>
            <hr>
            <hr>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="daftar-peserta" role="tabpanel" aria-labelledby="detail-tab">
                    <div class="card-header">
                        <p class="text-dark font-weight-bold">
                        <h5 class="text-dark">
                            <span class="badge badge-pill badge-danger text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> Alamat Perguruan Tinggi</h5>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-3">
                                <p class="text-dark">Provinsi</p>
                            </div>
                            <div class="col-md-9">
                                <p class="text-dark">: <?= $propinsiProfil ?></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <p class="text-dark">Kota/Kabupaten</p>
                            </div>
                            <div class="col-md-9">
                                <p class="text-dark">: <?= $kabProfil ?></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <p class="text-dark">Kode Pos</p>
                            </div>
                            <div class="col-md-9">
                                <p class="text-dark">: <?= $perguruan_tinggi->kode_pos ?></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <p class="text-dark">Alamat</p>
                            </div>
                            <div class="col-md-9">
                                <p class="text-dark">: <?= $perguruan_tinggi->alamat ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- container -->
<!-- append theme customizer -->
<?php $this->load->view('template/template_scripts') ?>
</body>

</html>