<div class="row">
    <div class="col-xs-12 col-lg-12">
        <form id="form_edit">
            <div class="card">
                <div class="card-body">
				<input type="hidden" name="id_laporan" id="id_laporan" class="form-control" value="<?= $laporan->id_laporan ?>">
                    <div class="form-group row">
                        <label for="judul" class="col-md-3 col-form-label">Judul <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="judul" name="judul" value="<?= $laporan->judul; ?>" maxlength="255" >
                            <div class="invalid-feedback" for="judul"></div>
                        </div>
                    </div>
					<div class="form-group row">
                        <label for="tahun" class="col-md-3 col-form-label">Tahun <b class="text-danger" >*</b></label>
                        <div class="col-md-9">
						<input type="number" class="form-control" id="tahun" name="tahun" value="<?= $laporan->tahun?>" maxlength="5">
                            <div class="invalid-feedback" for="tahun"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger" >*</b></label>
                        <div class="col-md-9">
                            <textarea  class="form-control" id="deskripsi" name="deskripsi" maxlength="255"><?= $laporan->deskripsi; ?></textarea>
                            <div class="invalid-feedback" for="deskripsi"></div>
                        </div>
                    </div>

					<input type="hidden" name="oldFoto" value="<?= $laporan->url_file ?>">

                    <div class="form-group row">
						<label for="url_file" class="col-md-3 col-form-label">Upload File<b class="text-danger">*</b></label>
						<div id="url_file" class="col-md-9">
							<label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
							<input type="file" accept=".pdf" name="url_file" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'alumni')">
							<span class="file-info-alumni text-muted">Tidak ada berkas yang dipilih</span>
							<div id="preview-alumni">

							</div>
							<div class="hint-block text-muted mt-3">
								<small>
									Jenis file yang diijinkan: <strong>PDF</strong><br>
									Ukuran file maksimal: <strong>2 MB</strong>
								</small>
							</div>
							<div class="invalid-feedback" for="url_file"></div>
						</div>
					</div>
                <div class="modal-footer">
                    <a href="<?php echo base_url('laporan/index/') ?>" class="btn btn-warning">Kembali</a>
                    <button type="submit" class="btn btn-success" id="btn-simpan-alumni">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>

<script>
    ClassicEditor
        .create(document.querySelector('#deskripsi'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>
<script>
	$('#form_edit').on('submit', function(e) {
		e.preventDefault();

		$('#form_edit').find('input, select, textarea').removeClass('is-invalid');
		$('input').parent().removeClass('is-invalid');
		$.ajax({
			url: site_url + 'laporan/Index/edit_laporan',
			data: new FormData(this),
			dataType: 'json',
			type: 'POST',
			contentType: false,
			processData: false,
			beforeSend: function() {
				showLoading();
			},
			success: function(data) {
				hideLoading();
				swal.fire({
					title: data.message.title,
					text: data.message.body,
					icon: 'success',
					confirmButtonColor: '#396113',
				}).then(function() {
					window.location.href = site_url + 'laporan/index';
				});
			},
			error: function(jqXHR, textStatus, errorThrown) {
				hideLoading();
				let response = jqXHR.responseJSON;

				switch (jqXHR.status) {
					case 400:
						// Keadaan saat validasi
						if (Array.isArray(response.data)) {
							response.data.forEach(function({
								field,
								message
							}, index) {
								$(`[name="${field}"]`).addClass('is-invalid');
								$(`[name="${field}"]`).parent().addClass('is-invalid');
								$(`.invalid-feedback[for="${field}"]`).html(message);
							});

							// sengaja diberi timeout,
							// karena kalau tidak, saat focus ke field error,
							// akan kembali lagi men-scroll ke tempat scroll sebelumnya
							setTimeout(function() {
								if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
									$(`[name="${response.data[0]['field']}"]`).focus();
								} else {
									$(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
								};
							}, 650);
						};
						break;
					case 500:
						swal.fire({
							title: response.message.title,
							html: response.message.body,
							icon: 'error',
							confirmButtonColor: '#396113',
						})
						break;
					default:
						break;
				};
			}
		});
	})
</script>

