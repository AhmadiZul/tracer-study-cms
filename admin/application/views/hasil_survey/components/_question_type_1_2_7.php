<div class="col-lg-6">
    <div class="card">
        <div class="card-header align-items-start">
            <span class="badge badge-success badge-square"><?= $no ?></span>&emsp;&emsp;
            <label><?= $question ?></label>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-md">
                <tbody>
                    <tr>
                        <th style="width: 10%;">No</th>
                        <th>Jawaban</th>
                    </tr>
                    <?php if (empty($answer)) { ?>
                        <tr>
                            <td colspan="2">Data kosong</td>
                        </tr>
                    <?php } else { ?>
                        <?php for ($i = 0; $i < count($answer); $i++) { ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td><?= $answer[$i] ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>