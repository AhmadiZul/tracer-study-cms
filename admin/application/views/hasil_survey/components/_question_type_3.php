<div class="col-lg-6">
    <div class="card">
        <div class="card-header align-items-start">
            <span class="badge badge-success badge-square"><?= $no ?></span>&emsp;&emsp;
            <label><?= $question ?></label>
        </div>
        <div class="card-body">
            <div class="col-md-12" id="container_'<?= $id_kuesioner."_$id_status_alumni" ?>'"></div>
        </div>
        <div class="card-footer">
            <?php if ($is_isian) : ?>
                <div class="col">
                    <?php foreach ($opsi as $key1 => $value1) { ?>
                        <table class="table table-bordered table-md">
                            <tbody>
                                <tr>
                                    <th style="width: 10%;">No</th>
                                    <th><?php echo $value1['opsi']?></th>
                                </tr>
                                <?php $noTabel = 1; ?>
                                <?php foreach ($isian as $key2 => $value2) { ?>
                                    <?php if ($key2 == $value1['id_opsi']) : ?>
                                        <tr>
                                            <td><?= $noTabel ?></td>
                                            <td><?= $value2 ?></td>
                                        </tr>
                                        <?php $noTabel++; ?>
                                    <?php endif; ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var answer = <?php echo $answer ?>;
    var buttonChart = {
        buttons: {
            contextButton: {
                menuItems: [
                    'viewFullscreen',
                    'printChart',
                    'separator',
                    'downloadPNG',
                    'downloadJPEG',
                    'downloadPDF',
                    'downloadSVG',
                    'separator',
                    'downloadCSV',
                    'downloadXLS',
                    'viewData'
                ]
            }
        }
    };
    Highcharts.chart("container_'<?= $id_kuesioner."_$id_status_alumni" ?>'", {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: "pie"
        },
        exporting: buttonChart,
        title: {
            text: ""
        },
        tooltip: {
            pointFormat: "{point.name}: <b> {point.y} ({point.percentage:.1f}%)</b>"
        },
        accessibility: {
            point: {
                valueSuffix: "%"
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: "pointer",
                dataLabels: {
                    enabled: true,
                    format: "<b>{point.name}</b>: {point.y} {point.percentage:.1f} %"
                }
            }
        },
        series: [{
            name: "Data Option",
            colorByPoint: true,
            data: answer
        }]
    });
</script>