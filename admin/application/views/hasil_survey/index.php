<script src="<?php echo base_url() . 'public/vendor/highchart/highchart.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/exporting.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/export-data.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/accessibility.js'; ?>"></script>

<div class="card">
    <div class="card-header">
        <h4 class="card-title"><i class="fa fa-chart-pie"></i> <?=$jenis_survey->jenis_survey?> - Hasil Survey</h4>
    </div>
    <div class="card-body">
        <div class="card" style="background-color: #F5F5F5;border: 1px solid #D3D3DA;border-radius: 8px 8px 8px 8px;">
            <div class="card-body">
                <ul class="nav nav-tabs card-header-tabs">
                    <?php foreach ($question as $key => $value) { ?>
                        <?php $is_active = ''; ?>
                        <?php if ($key == 0) : ?>
                            <?php $is_active = 'active'; ?>
                        <?php endif; ?>
                        <li class="nav-item"><a class="nav-link text-dark <?= $is_active ?>" id="<?= $value->name_tab ?>-tab" data-toggle="tab" data-target="#<?= $value->name_tab ?>" type="button" role="tab" aria-controls="<?= $value->name_tab ?>" aria-selected="true"><?= $value->status_alumni . ' (' . $value->jumlah . ')' ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="tab-content">
            <?php foreach ($question as $key => $value) { ?>
                <?php $is_active = ''; ?>
                <?php if ($key == 0) : ?>
                    <?php $is_active = 'active show'; ?>
                <?php endif; ?>
                <div class="tab-pane fade <?= $is_active ?>" id="<?= $value->name_tab ?>" role="tabpanel" aria-labelledby="<?= $value->name_tab ?>-tab">

                    <?php if (!$value->is_kuesioner) { ?>
                        Tidak ada kuesioner
                    <?php } else { ?>
                        <div class="row">
                            <?php foreach ($value->jawaban as $key2 => $value2) { ?>
                                <?php if ($value2->question_type == 1 || $value2->question_type == 2 || $value2->question_type == 7) : ?>
                                    <?php $this->load->view('hasil_survey/components/_question_type_1_2_7', $value2); ?>
                                <?php endif; ?>
                                <?php if ($value2->question_type == 3) : ?>
                                    <?php $this->load->view('hasil_survey/components/_question_type_3', $value2); ?>
                                <?php endif; ?>
                                <?php if ($value2->question_type == 4) : ?>
                                    <?php $this->load->view('hasil_survey/components/_question_type_4', $value2); ?>
                                <?php endif; ?>
                                <?php if ($value2->question_type == 5) : ?>
                                    <?php $this->load->view('hasil_survey/components/_question_type_5', $value2); ?>
                                <?php endif; ?>
                                <?php if ($value2->question_type == 6) : ?>
                                    <?php $this->load->view('hasil_survey/components/_question_type_6', $value2); ?>
                                <?php endif; ?>
                            <?php } ?>
                        </div>
                    <?php } ?>

                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php $this->load->view("template/template_scripts") ?>

<script type="text/javascript">
    let body = $('body');

    $(document).ready(function() {
        body.addClass('sidebar-mini');
        body.removeClass('sidebar-show');

        $(".main-sidebar .sidebar-menu > li").each(function() {
            let me = $(this);

            if (me.find('> .dropdown-menu').length) {
                me.find('> .dropdown-menu').hide();
                me.find('> .dropdown-menu').prepend('<li class="dropdown-title pt-3">' + me.find('> a').text() + '</li>');
            } else {
                me.find('> a').attr('data-toggle', 'tooltip');
                me.find('> a').attr('data-original-title', me.find('> a').text());
                $("[data-toggle='tooltip']").tooltip({
                    placement: 'right'
                });
            }
        });
    });
</script>