<script src="<?php echo base_url() . 'public/vendor/highchart/highchart.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/exporting.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/export-data.js'; ?>"></script>
<script src="<?php echo base_url() . 'public/vendor/highchart/accessibility.js'; ?>"></script>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div id="gJenis_kelamin"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div id="gStatus_alumni_kelamin"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div id="gProvinsi"></div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view("template/template_scripts") ?>
<script type="text/javascript">
    var site_url = "<?php echo base_url() ?>";
    var statusAlumni = <?php echo json_encode($status_alumni) ?>;
    var gJenisKelamin = <?php echo json_encode($g_jenis_kelamin) ?>;
    var gStatusAlumniKelamin = <?php echo json_encode($g_status_alumni_kelamin) ?>;
    var gProvinsi = <?php echo json_encode($g_provinsi) ?>;
    var provinsi = <?php echo json_encode($provinsi) ?>;
    var buttonChart = {
        buttons: {
            contextButton: {
                menuItems: [
                    'viewFullscreen',
                    'printChart',
                    'separator',
                    'downloadPNG',
                    'downloadJPEG',
                    'downloadPDF',
                    'downloadSVG',
                    'separator',
                    'downloadCSV',
                    'downloadXLS',
                    'viewData'
                ]
            }
        }
    };

    var gJenis_kelamin = new Highcharts.chart("gJenis_kelamin", {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: "pie"
        },
        exporting: buttonChart,
        title: {
            text: 'Jenis Kelamin'
        },
        subtitle: {
            text: 'Grafik berdasarkan Jenis Kelamin'
        },
        tooltip: {
            pointFormat: "{point.name}: <b> {point.y} ({point.percentage:.1f}%)</b>"
        },
        accessibility: {
            point: {
                valueSuffix: "%"
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: "pointer",
                dataLabels: {
                    enabled: true,
                    format: "<b>{point.name}</b>: {point.y} {point.percentage:.1f} %"
                },
                showInLegend: true,
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: "Jenis Kelamin",
            colorByPoint: true,
            data: gJenisKelamin
        }]
    });

    var gStatus_alumni_kelamin = new Highcharts.chart("gStatus_alumni_kelamin", {
        chart: {
            type: 'column'
        },
        exporting: buttonChart,
        title: {
            text: 'Status alumni'
        },
        subtitle: {
            text: 'Grafik status alumni berdasarkan jenis kelamin'
        },
        xAxis: {
            categories: statusAlumni,
            max: 4,
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray',
                    textOutline: 'none'
                }
            }
        },
        legend: {
            align: 'left',
            verticalAlign: 'bottom',
            floating: false,
            backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: gStatusAlumniKelamin
    });

    var gProvinsi = new Highcharts.chart("gProvinsi", {
        chart: {
            type: 'column'
        },
        exporting: buttonChart,
        title: {
            text: 'Jenis Kelamin'
        },
        subtitle: {
            text: 'Grafik jenis kelamin berdasarkan provinsi'
        },
        xAxis: {
            categories: provinsi
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray',
                    textOutline: 'none'
                }
            }
        },
        legend: {
            align: 'left',
            verticalAlign: 'bottom',
            floating: false,
            backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        credits: {
            enabled: false
        },
        series: gProvinsi
    });
</script>