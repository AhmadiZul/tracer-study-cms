<main id="main">
    <section id="profile">
        <div class="container">
            <div class="d-flex justify-content-center">
                <img src="<?php echo base_url('public/assets/img/404.png') ?>" class="img-fluid" width="714" height="417.42" alt="">
            </div>
            <h2 class="text-center pt-3 fw-bold">
                <b><span class="text-dark">Oops... Kami Kehilangan Halaman Ini</span>
            </h2>
            <div class="text-center">
                <p >Kami mencari dimana-mana tetapi tidak menemukan apa yang anda cari. </p>
               <p class="pb-3"> Ayo cari tempat yang lebih baik untukmu</p>
            </div>   
        </div>
    </section>
</main>

<?php $this->load->view("template/template_scripts") ?>