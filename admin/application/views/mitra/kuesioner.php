<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <small>
                    <?php echo $output->output; ?>
                </small>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>

<div class="modal fade" id="urutan-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit urutan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="urutan-form">
          <div class="form-group">
            <input type="hidden" id="id_kuesioner" name="id_kuesioner" class="form-control">
            <input type="number" id="urutan" name="urutan" class="form-control">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        <button type="button" onclick="simpanUrutan()" class="btn btn-success">Simpan</button>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    window.addEventListener('gcrud.datagrid.ready', () => {
      $(".gc-header-tools").append(
        ` <div class="l5 mr-1">
            <button id="btn-unduh" class="btn btn-default btn-outline-dark t5">
              <i class="fa fa-plus floatL t3"></i>
              <span class="hidden-xs floatL l5">Tambah Kueisioner</span>
              <div class="clear">
              </div>
            </button>
          </div>`
      );

      $('#btn-unduh').click(function(e) {
        e.preventDefault();
        window.location.href = site_url + 'kuesioner_mitra/index/addKuesioner';
      })
    });
  })

  function aktif(id) {
    var formData = {
      id_kuesioner: id,
      status: '1'
    };
    $.ajax({
      type: 'ajax',
      method: 'POST',
      url: site_url + 'kuesioner_mitra/index/aktif',
      data: formData,
      dataType: 'json',
      encode: true,
      success: function(response) {
        if (response.success) {
          Swal.fire('Proses Berhasil!', 'Berhasil Diaktivasi', 'success').then(function() {
            $('.fa-refresh').trigger('click');
          })
        } else {
          Swal.fire('Proses Gagal!', response.message, 'error');
        }
      },
      error: function(xmlresponse) {
        console.log(xmlresponse);
      }
    });
  }

  function noAktif(id) {
    var formData = {
      id_kuesioner: id,
      status: '0'
    };
    $.ajax({
      type: 'ajax',
      method: 'POST',
      url: site_url + 'kuesioner_mitra/index/aktif',
      data: formData,
      dataType: 'json',
      encode: true,
      success: function(response) {
        if (response.success) {
          Swal.fire('Proses Berhasil!', 'Berhasil Di Non Aktifkan', 'success').then(function() {
            $('.fa-refresh').trigger('click');
          })
        } else {
          Swal.fire('Proses Gagal!', response.message, 'error');
        }
      },
      error: function(xmlresponse) {
        console.log(xmlresponse);
      }
    });
  }

  function urutanModal(id, urutan) {
    $('#id_kuesioner').val(id);
    $('#urutan').val(urutan);
    $('#urutan-modal').modal('show');
  }

  function simpanUrutan() {
    $.ajax({
      type: 'ajax',
      method: 'POST',
      url: site_url + 'kuesioner_mitra/index/simpanUrutan',
      data: new FormData($('#urutan-form')[0]),
      async: false,
      processData: false,
      contentType: false,
      dataType: 'json',
      success: function(response) {
        if (response.success) {
          Swal.fire('Proses Berhasil!', 'Berhasil Simpan', 'success').then(function() {
            $('#urutan-modal').modal('hide');
            $('.fa-refresh').trigger('click');
          })
        } else {
          Swal.fire('Proses Gagal!', response.message, 'error');
        }
      },
      error: function(xmlresponse) {
        console.log(xmlresponse);
      }
    });
  }
</script>
