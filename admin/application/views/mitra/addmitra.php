<div class="card">
    <div class="card-body">
        <form id="form_mitra">
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="nama">Nama <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Masukkan nama" maxlength="100">
                    <div class="invalid-feedback" for="nama"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="jenis">Jenis <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <select name="jenis" id="jenis" class="form-control select2">
                        <option value="">Pilih Jenis</option>
                        <option value="Perusahaan swasta/Industri Swasta">Perusahaan swasta/Industri Swasta</option>
                        <option value="BUMN/Perusahaan Milik Pemerintah">BUMN/Perusahaan Milik Pemerintah</option>
                        <option value="Pemerintah Daerah/Pusat">Pemerintah Daerah/Pusat</option>
                        <option value="Lembaga Pendidikan Negeri">Lembaga Pendidikan Negeri</option>
                        <option value="Lembaga Pendidikan Swasta">Lembaga Pendidikan Swasta</option>
                        <option value="Lainnya">Lainnya</option>
                    </select>
                    <div class="invalid-feedback" for="jenis"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="sektor">Sektor <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="sektor" id="pertanian" value="Pertanian">
                        <label class="custom-control-label" for="pertanian">Pertanian</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" name="sektor" id="nonpertanian" value="Non Pertanian">
                        <label class="custom-control-label" for="nonpertanian">Non Pertanian</label>
                    </div>
                    <div class="invalid-feedback" for="sektor"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="telephone">Telepon <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" class="form-control number-only" name="telephone" id="telephone" placeholder="Masukkan telepon" maxlength="13">
                    <div class="invalid-feedback" for="telephone"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="website">Website <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" name="website" id="website" placeholder="Masukkan website" maxlength="255">
                    <div class="invalid-feedback" for="website"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="alamat">Alamat <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <textarea name="alamat" id="alamat" class="form-control" placeholder="Masukkan alamat" style="height: 100%;"></textarea>
                    <div class="invalid-feedback" for="alamat"></div>
                </div>
            </div>
            <div class="form-group row">
                <label for="url_photo" class="col-md-3 col-form-label">Upload Logo</label>
                <div id="new-upload-logo" class="col-md-9">
                    <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                    <input type="file" name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo')">
                    <span class="file-info-logo text-muted">Tidak ada berkas yang dipilih</span>
                    <div id="preview-logo">

                    </div>
                    <div class="hint-block text-muted mt-3">
                        <small>
                            Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                            Ukuran file maksimal: <strong>2 MB</strong>
                        </small>
                    </div>
                    <div class="invalid-feedback" for="url_photo"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="email">Email <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Masukkan email" maxlength="100">
                    <div class="invalid-feedback" for="email"></div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="password">Password <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <div class="input-group mb-3">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Masukkan password">
                        <a class="input-group-text show-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                        <div class="invalid-feedback" for="password">Wajib diisi</div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label" for="cpassword">Konfirmasi Password <b class="text-danger">*</b></label>
                <div class="col-md-9">
                    <div class="input-group mb-3">
                        <input type="password" id="cpassword" name="cpassword" class="form-control" placeholder="Masukkan konfirmasi password anda">
                        <a class="input-group-text show-confirm-password"><i class="fas fa-eye-slash text-secondary"></i></a>
                        <div class="invalid-feedback" for="cpassword">Wajib diisi</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url(); ?>mitra/dashboard" class="btn btn-theme-dark float-end">Kembali</a>
                <button type="submit" class="btn btn-theme-danger float-end">Simpan</button>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view("template/template_scripts") ?>

<script>
    $('#form_mitra').on('submit', function(e) {
        e.preventDefault();

        $('#form_mitra').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'mitra/dashboard/createMitra',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html:'<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil menambahkan mitra',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'mitra/dashboard';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })

    /**
     * show confirm password
     */
    $('.show-confirm-password').click(function(e) {
        if ('password' == $('[name="cpassword"]').attr('type')) {
            $('[name="cpassword"]').prop('type', 'text');
            $('.show-confirm-password').html('<i class="fas fa-eye text-secondary">');
        } else {
            $('[name="cpassword"]').prop('type', 'password');
            $('.show-confirm-password').html('<i class="fas fa-eye-slash text-secondary">');
        }
    });
</script>