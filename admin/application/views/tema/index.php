<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <small>
                    <?php echo $output->output; ?>
                </small>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>

<script>
    function publish(id_color) {
            var formData = {
                id_color:id_color,
                is_publish: '1'
            };
            $.ajax({
                type: 'ajax',
                method: 'POST',
                url: site_url + 'setting_tema/index/publishAlumni',
                data: formData,
                dataType: 'json',
                encode: true,
                success: function(response) {
                    if (response.success) {
                        Swal.fire('Proses Berhasil!', 'Berhasil Dipublikasikan', 'success').then(function() {
                            $('.fa-refresh').trigger('click');
                        })
                    } else {
                        Swal.fire('Proses Gagal!', response.message, 'error');
                    }
                },
                error: function(xmlresponse) {
                    console.log(xmlresponse);
                }
            });
  }

  function noPublish(id_color) {
            // console.log(id)
            var formData = {
                id_color:id_color,
                is_publish: '0'
            };
            console.log(formData)
            $.ajax({
                type: 'ajax',
                method: 'POST',
                url: site_url + 'setting_tema/index/publishAlumni',
                data: formData,
                dataType: 'json',
                encode: true,
                success: function(response) {
                    if (response.success) {
                        Swal.fire('Proses Berhasil!', 'Berhasil Di Non Publikasikan', 'success').then(function() {
                            $('.fa-refresh').trigger('click');
                        })
                    } else {
                        Swal.fire('Proses Gagal!', response.message, 'error');
                    }
                },
                error: function(xmlresponse) {
                    console.log(xmlresponse);
                }
            });
  }
</script>