
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-md-3">
                        <?php if ($alumni['url_photo'] != null) : ?>
                            <img class=" mt-3" width="100%" style="border-radius:20px;" src="<?=base_url($alumni['url_photo']) ?>" alt="foto-profil">
                        <?php else : ?>
                            <img class=" mt-3" width="100%" style="border-radius:20px;" src="<?php echo base_url() ?>public/assets/img/orang_pusing.png" alt="foto-profil">
                        <?php endif ?>
                    </div>
                    <div class="col-md-7">
                        <p class="fw-bold fs-5 mt-4"><h2 class="text-dark"><?php echo $alumni['nama'] ?></h2></p>
                        <p class="fw-bold fs-5 mt-4"><i class="fa fa-phone"></i> <?php echo empty($alumni['no_hp']) ? "-" : $alumni['no_hp']; ?></p>
                        <p class="fw-bold fs-5 mt-4"><i class="fa fa-envelope"></i> <?php echo $alumni['email'] ?></p>
                        <div class="row">
                            <div class="col-sm">
                                <p class="text-dark font-weight-bold">STATUS ALUMNI</p>
                                <p><?= $alumni['status_alumni'] ?></p>
                            </div>
                            <div class="col-sm">
                                <p class="text-dark font-weight-bold">POSISI PEKERJAAN</p>
                                <?php if ($alumni['id_ref_status_alumni'] == '3' || $alumni['id_ref_status_alumni'] == '1') : ?>
                                    <p><?php echo isset($jobsNow['jabatan']) ? $jobsNow['jabatan'] : '-' ?></p>
                                <?php else : ?>
                                    <p>-</p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <?php if ($alumni['status_data'] == "1") : ?>
                            <p class="fw-bold fs-5 mt-4 text-success">Terverifikasi <i class="fa fa-check-circle"></i></p>
                        <?php else : ?>
                            <p class="fw-bold fs-5 mt-4 text-danger">Belum Terverifikasi <i class="fa fa-times-circle"></i> <button class="btn btn-success" onclick="verif('<?php echo $id_alumni ?>')">Verifikasi</button></p>
                        <?php endif ?>
                    </div>

                </div>
            </div>
            <hr>
            <div class="card-body">
                <ul class="nav" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="link-alumni nav-link text-dark font-weight-bold active" id="detail-tab" data-toggle="tab" data-bs-toggle="tab" href="#daftar-peserta" role="tab" aria-controls="beranda" aria-selected="true">Profil</a>
                    </li>
                    <li class="nav-item">
                        <a class="link-alumni nav-link text-dark font-weight-bold" id="dokumen-tab" data-toggle="tab" data-bs-toggle="tab" href="#riwayat_pekerjaan" role="tab" aria-controls="riwayat_pekerjaan" aria-selected="false">Riwayat Pekerjaan</a>
                    </li>
                    <li class="nav-item">
                        <a class="link-alumni nav-link text-dark font-weight-bold" id="prestasi-tab" data-toggle="tab" data-bs-toggle="tab" href="#prestasi-alumni" role="tab" aria-controls="prestasi-alumni" aria-selected="false">Prestasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="link-alumni nav-link text-dark font-weight-bold" id="skill-tab" data-toggle="tab" data-bs-toggle="tab" href="#skill" role="tab" aria-controls="skill" aria-selected="false">Skill</a>
                    </li>
                    <li class="nav-item">
                        <a class="link-alumni nav-link text-dark font-weight-bold" id="organisasi-tab" data-toggle="tab" data-bs-toggle="tab" href="#organisasi_alumni" role="tab" aria-controls="organisasi_alumni" aria-selected="false">Organisasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="link-alumni nav-link text-dark font-weight-bold" id="pengalaman-tab" data-toggle="tab" data-bs-toggle="tab" href="#pengalaman_alumni" role="tab" aria-controls="pengalaman_alumni" aria-selected="false">Pengalaman</a>
                    </li>
                </ul>
            </div>
            <hr>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="daftar-peserta" role="tabpanel" aria-labelledby="detail-tab">
                    <div class="card">
                        <div class="card-header">
                            <p class="text-dark font-weight-bold"><h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;</span> Data Diri</h5></p>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">NIM/NIRM/NIS</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-3">
                                    <p><?php echo empty($alumni['nim']) ? "-" : $alumni['nim']; ?></p>
                                </div>
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">NIK</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-3">
                                    <p><?php echo empty($alumni['nik']) ? "-" : $alumni['nik']; ?></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">Tempat Lahir</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-3">
                                    <p><?php echo empty($alumni['tempat_lahir']) ? "-" : $alumni['tempat_lahir']; ?></p>
                                </div>
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">Tanggal Lahir</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-3">
                                    <p><?php echo empty($alumni['tgl_lahir']) ? "-" : $alumni['tgl_lahir']; ?></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">Agama</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-3">
                                    <p><?php echo empty($alumni['nama_agama']) ? "-" : $alumni['nama_agama']; ?></p>
                                </div>
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">Jenis Kelamin</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-3">
                                    <p><?php echo empty($alumni['jenis_kelamin']) ? "-" : $alumni['jenis_kelamin']; ?></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">Provinsi</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-3">
                                    <p><?php echo empty($alumni['nama_provinsi']) ? "-" : $alumni['nama_provinsi']; ?></p>
                                </div>
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">Kabupaten/Kota</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-3">
                                    <p><?php echo empty($alumni['nama_kab_kota']) ? "-" : $alumni['nama_kab_kota']; ?></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">Kecamatan</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-3">
                                    <p><?php echo empty($alumni['nama_kecamatan']) ? "-" : $alumni['nama_kecamatan']; ?></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
                                    <p class="text-dark font-weight-bold">Alamat</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-8">
                                    <p><?php echo empty($alumni['alamat']) ? "-" : $alumni['alamat']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h5 class="text-dark"><span class="badge badge-pill badge-danger text-danger">&nbsp;</span> Data Studi</h5>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <p class="text-dark font-weight-bold">Program Studi/Jurusan</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-8">
                                    <p><?php echo empty($alumni['nama_prodi']) ? "-" : $alumni['nama_prodi']; ?></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <p class="text-dark font-weight-bold">Tahun Lulus</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-8">
                                    <p><?php echo empty($alumni['tahun_lulus']) ? "-" : $alumni['tahun_lulus']; ?></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <p class="text-dark font-weight-bold">Ipk Terakhir</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-8">
                                    <p><?php echo empty($alumni['ipk_terakhir']) ? "-" : $alumni['ipk_terakhir']; ?></p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <p class="text-dark font-weight-bold">Judul Tugas Akhir</p>
                                </div>
                                <div class="col-md-1">
                                    <p class="text-dark font-weight-bold">:</p>
                                </div>
                                <div class="col-md-8">
                                    <p><?php echo empty($alumni['judul_skripsi']) ? "-" : $alumni['judul_skripsi']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="riwayat_pekerjaan" role="tabpanel" aria-labelledby="dokumen-tab">
                    <?php $this->load->view('alumni/dashboard/riwayat_pekerjaan'); ?>
                </div>
                <div class="tab-pane fade" id="prestasi-alumni" role="tabpanel" aria-labelledby="prestasi-tab">
                    <?php $this->load->view('alumni/dashboard/prestasi_alumni'); ?>
                </div>
                <div class="tab-pane fade" id="organisasi_alumni" role="tabpanel" aria-labelledby="organisasi-tab">
                    <?php $this->load->view('alumni/dashboard/organisasi'); ?>
                </div>
                <div class="tab-pane fade" id="skill" role="tabpanel" aria-labelledby="skill-tab">
                    <?php $this->load->view('alumni/dashboard/skill_alumni'); ?>
                </div>
                <div class="tab-pane fade" id="pengalaman_alumni" role="tabpanel" aria-labelledby="pengalaman-tab">
                    <?php $this->load->view('alumni/dashboard/pengalaman_alumni'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- container -->
<!-- append theme customizer -->
<?php $this->load->view('template/template_scripts') ?>
</body>

</html>

<script>
    $(function() {
        sosmed = <?php echo $alumni["sosmed"] ?>;
        for (const [key, value] of Object.entries(sosmed)) {
            $("#sosmed").append("<div class='col-sm-4'><p>" + key + "</p><p class='fw-bold'>" + value + "</p></div>");
        }

    });


    function verif(id) {
        console.log(id)
        var formData = {
            id_mhs: id,
            status_data: '1'
        };
        console.log(formData)
        $.ajax({
            type: 'ajax',
            method: 'POST',
            url: site_url + 'alumni/dashboard/verifAlumni',
            data: formData,
            dataType: 'json',
            encode: true,
            success: function(response) {
                if (response.success) {
                    Swal.fire('Proses Berhasil!', response.message, 'success').then(function() {
                        location.reload()
                    })
                } else {
                    Swal.fire('Proses Gagal!', response.message, 'error');
                }
            },
            error: function(xmlresponse) {
                console.log(xmlresponse);
            }
        });
    }
</script>