<?php if (!empty($jobs)) { ?>
    <div class="card ">
        <div class="card-header">
            <h5 class="text-dark">Riwayat Pekerjaan</h5>
        </div>
        <div class="card-body">
            <?php foreach ($jobs as $key => $value) { ?>
                <div class="card border border-secondary">
                    <?php $date = (isset($value['tgl_mulai']) ? DateTime::createFromFormat("Y-m-d", $value['tgl_mulai']) : ''); ?>
                    <div class="card-header bg-secondary">
                        <h4 class="card-title text-white"><?= (isset($value['tgl_mulai']) ? $date->format("Y") : '-') ?></h4>
                    </div>
                    <div class="card-body">
                        <label class="text-dark font-weight-bold">Nama Instansi</label>
                        <p class="card-text"><?= $value['nama_instansi']; ?></p>
                        <label class="tex-dark">Lokasi Instansi</label>
                        <p class="card-text"><?= $value['nama_kab_kota'] . ', ' . $value['nama_provinsi']; ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>

<?php if (!empty($study)) { ?>
    <div class="card ">
        <div class="card-header">
            <h5 class="text-dark">Riwayat Pendidikan</h5>
        </div>
        <div class="card-body">
            <?php foreach ($study as $key => $value) { ?>
                <div class="card border border-secondary">
                    <?php $date = (isset($value['tgl_mulai']) ? DateTime::createFromFormat("Y-m-d", $value['tgl_mulai']) : ''); ?>
                    <div class="card-header bg-secondary">
                        <h4 class="card-title text-white"><?= (isset($value['tgl_mulai']) ? $date->format("Y") : '-') ?></h4>
                    </div>
                    <div class="card-body">
                        <label class="text-dark font-weight-bold">Nama Perguruan Tinggi</label>
                        <p class="card-text"><?= $value['nama_perguruan_tinggi']; ?></p>
                        <label class="text-dark font-weight-bold">Lokasi Instansi</label>
                        <p class="card-text"><?= $value['nama_kab_kota'] . ', ' . $value['nama_provinsi']. ', ' . $value['nama_kecamatan']; ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>