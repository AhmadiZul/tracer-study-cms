<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="top-border"></div>
            <form id="form_alumni">
                <div class="card-body">
                    <input type="hidden" name="id_alumni" id="id_alumni" value="<?php echo $data['id_alumni'] ?>">
                    <input type="hidden" name="is_berkas" id="is_berkas" value="<?php echo ($data['url_photo'] ? '0' : '1') ?>">
                    <input type="hidden" name="id_job_study" id="id_job_study" value="<?php echo (isset($data['id_job_study']) ? $data['id_job_study'] : '') ?>">
                    <div class="form-group row">
                        <label for="input_nama" class="col-md-3 col-form-label">Nama <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input_nama" name="input_nama" value="<?php echo $data['nama'] ?>" maxlength="61">
                            <div class="invalid-feedback" for="input_nama"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_nim" class="col-md-3 col-form-label">NIM/NIRM/NIS <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control number-only" id="input_nim" name="input_nim" value="<?php echo $data['nim'] ?>" maxlength="16">
                            <div class="invalid-feedback" for="input_nim"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_nik" class="col-md-3 col-form-label">NIK<b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control number-only" id="input_nik" name="input_nik" value="<?php echo $data['nik'] ?>" maxlength="16">
                            <div class="invalid-feedback" for="input_nik"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_tempat_lahir" class="col-md-3 col-form-label">Tempat Lahir <b class="text-danger">*</b></label>
                        <div class="col-md-4">
                            <input type="text" class="form-control alpha-only" id="input_tempat_lahir" name="input_tempat_lahir" value="<?php echo $data['tempat_lahir'] ?>" maxlength="30">
                            <div class="invalid-feedback" for="input_tempat_lahir"></div>
                        </div>
                        <label for="input_tanggal_lahir" class="col-md-2 col-form-label">Tanggal Lahir <b class="text-danger">*</b></label>
                        <div class="col-md-3">
                            <input type="text" class="form-control datepicker-lahir number-only" id="input_tanggal_lahir" name="input_tanggal_lahir" value="<?php echo ($data['tgl_lahir'] != '0000-00-00' ? $data['tgl_lahir'] : '') ?>" maxlength="30">
                            <div class="invalid-feedback" for="input_tanggal_lahir"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_agama" class="col-md-3 col-form-label">Agama <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <select id="input_agama" name="input_agama" class="form-control select2">
                                <option value="" selected>Pilih Agama</option>
                                <?php foreach ($agama as $key => $value) { ?>
                                    <option value="<?php echo $key ?>" <?php echo ($key == $data['id_agama'] ? 'selected' : ''); ?>><?php echo $value ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="input_agama"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_jk" class="col-md-3 col-form-label">Jenis Kelamin <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="laki_laki" name="input_jk" value="L" <?php echo ($data['jenis_kelamin'] == 'L' ? 'checked' : '') ?>>
                                <label class="custom-control-label" for="laki_laki">Laki - Laki</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="perempuan" name="input_jk" value="P" <?php echo ($data['jenis_kelamin'] == 'P' ? 'checked' : '') ?>>
                                <label class="custom-control-label" for="perempuan">Perempuan</label>
                            </div>
                            <div class="invalid-feedback" for="input_jk"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_no_hp" class="col-md-3 col-form-label">Nomor HP</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control number-only" id="input_no_hp" name="input_no_hp" value="<?php echo $data['no_hp'] ?>">
                            <div class="invalid-feedback" for="input_no_hp"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_email" class="col-md-3 col-form-label">Email <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input_email" name="input_email" value="<?php echo $data['email'] ?>">
                            <div class="invalid-feedback" for="input_email"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_alamat" class="col-md-3 col-form-label">Alamat</label>
                        <div class="col-md-9">
                            <textarea name="input_alamat" id="input_alamat" class="form-control" style="height: 100%;"><?php echo $data['alamat'] ?></textarea>
                            <div class="invalid-feedback" for="input_alamat"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_provinsi" class="col-md-3 col-form-label">Provinsi <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <select id="input_provinsi" name="input_provinsi" class="form-control select2">
                                <option value="" selected>Pilih Provinsi</option>
                                <?php foreach ($propinsi as $key => $value) { ?>
                                    <option value="<?= $key; ?>" <?php echo ($key == $data['kode_prov'] ? 'selected' : ''); ?>><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="input_provinsi"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_kabupaten" class="col-md-3 col-form-label">Kabupaten <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <select name="input_kabupaten" id="input_kabupaten" class="form-control select2">
                                <option value="" selected>Pilih Kabupaten</option>
                                <?php if ($kab_kota) { ?>
                                    <?php foreach ($kab_kota as $key => $value) { ?>
                                        <option value="<?= $key; ?>" <?php echo ($key == $data['kode_kab_kota'] ? 'selected' : ''); ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="input_kabupaten"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_kecamatan" class="col-md-3 col-form-label">Kecamatan <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <select name="input_kecamatan" id="input_kecamatan" class="form-control select2">
                                <option value="">Pilih Kecamatan</option>
                                <?php if ($kecamatan) { ?>
                                    <?php foreach ($kecamatan as $key => $value) { ?>
                                        <option value="<?= $key; ?>" <?php echo ($key == $data['kode_kecamatan'] ? 'selected' : ''); ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="input_kecamatan"></div>
                        </div>
                    </div>

                    <!-- <div class="form-group row uploadFoto">
                        <label for="url_photo" class="col-md-3 col-form-label">Upload Foto <b class="text-danger">*</label>
                        <div id="ganti-berkas-alumni" class="col-md-9 <?php echo (isset($data['foto']) ? '' : 'd-none') ?>">
                            <img src="<?php echo (isset($data['foto'])  ? $data['foto'] : '') ?>" alt="your image" style="width: 150px;" /><br>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="new_upload_alumni" id="new_upload_alumni" value="1" onclick="uploadFoto('alumni','url_photo')">
                                <label class="custom-control-label" for="new_upload_alumni">Ganti Berkas</label>
                            </div>
                        </div>
                        <div class="col-md-3 <?php echo (isset($data['foto'])  ? '' : 'd-none') ?>"></div>
                        <div id="new-upload-alumni" class="col-md-9 <?php echo (isset($data['foto'])  ? 'd-none' : '') ?>">
                            <label for="input_file" class="btn btn-primary ">Pilih Berkas</label>
                            <input type="file" accept=".png, .jpg, .jpeg " name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'alumni')">
                            <span class="file-info-alumni text-muted">Tidak ada berkas yang dipilih</span>
                            <div id="preview-alumni">

                            </div>
                            <div class="hint-block text-muted mt-3">
                                <small>
                                    Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                    Ukuran file maksimal: <strong>2 MB</strong>
                                </small>
                            </div>
                            <div class="invalid-feedback" for="url_photo"></div>
                        </div>
                    </div> -->

                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="url_photo"> Upload Foto <b class="text-danger">*</b></label>
                        </div>
                        <div id="ganti-berkas-logo-perguruan" class="col-md-9 d-inline">
                            <input type="hidden" name="id" value="<?= $data['id_alumni'] ?>">
                            <img src="<?= base_url() . $data['url_photo'] ?>" alt="your image" style="width: 150px;" /><br>
                            <label class="form-label"><input name="new_upload_logo-perguruan" type="checkbox" onclick="uploadFoto('logo-perguruan','url_photo')" value="1"> Ganti Berkas</label>
                        </div>
                        <div class="col-md-3"></div>
                        <div id="new-upload-logo-perguruan" class="col-md-9 d-none">
                            <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                            <input type="file" accept=".png, .jpg, .jpeg " name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo-perguruan')">
                            <span class="file-info-logo-perguruan text-muted">Tidak ada berkas yang dipilih</span>
                            <div id="preview-logo-perguruan">

                            </div>
                            <div class="hint-block text-muted mt-3">
                                <small>
                                    Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                    Ukuran file maksimal: <strong>2 MB</strong>
                                </small>
                            </div>
                            <div class="invalid-feedback" for="url_photo"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_tahun_lulus" class="col-md-3 col-form-label">Tahun Lulus <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <select id="input_tahun_lulus" name="input_tahun_lulus" class="form-control select2">
                                <option value="" selected>Pilih Tahun Lulus</option>
                                <?php foreach ($tahun_lulus as $key => $value) { ?>
                                    <option value="<?= $value['tahun']; ?>" <?php echo ($value['tahun'] == $data['tahun_lulus'] ? 'selected' : ''); ?>><?php echo $value['tahun']; ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="input_tahun_lulus"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_ipk_terakhir" class="col-md-3 col-form-label">IPK Terakhir</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control ipk" id="input_ipk_terakhir" name="input_ipk_terakhir" value="<?php echo $data['ipk_terakhir'] ?>">
                            <div class="invalid-feedback" for="input_ipk_terakhir"></div>
                        </div>
                    </div>
                    <?php if ($this->session->userdata("tracer_idGroup") == 2) { ?>
                        <div class="form-group row">
                            <label for="input_fakultas" class="col-md-3 col-form-label">Fakultas <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <select id="input_fakultas" name="input_fakultas" class="form-control select2">
                                    <option value="<?= $id_fakultas ?>"> <?= $nama_fakultas ?></option>
                                </select>
                                <div class="invalid-feedback" for="input_fakultas"></div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="form-group row">
                            <label for="input_fakultas" class="col-md-3 col-form-label">Fakultas <b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <select id="input_fakultas" name="input_fakultas" class="form-control select2">
                                    <option value="" selected>Fakultas</option>
                                    <?php foreach ($fakultas as $key => $value) { ?>
                                        <?php if ($data['id_fakultas'] == $value['id_fakultas']) : ?>
                                            <option value="<?= $value['id_fakultas']; ?>" selected><?php echo $value['nama_fakultas']; ?></option>
                                        <?php else : ?>
                                            <option value="<?= $value['id_fakultas']; ?>"><?php echo $value['nama_fakultas']; ?></option>
                                        <?php endif ?>
                                    <?php } ?>
                                </select>
                                <div class="invalid-feedback" for="input_fakultas"></div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group row">
                        <label for="input_prodi" class="col-md-3 col-form-label">Program Studi/Jurusan <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <select id="input_prodi" name="input_prodi" class="form-control select2">
                                <option value="" selected>Pilih Program Studi/Jurusan</option>
                                <?php if ($prodi) { ?>
                                    <?php foreach ($prodi as $key => $value) { ?>
                                        <option value="<?= $key; ?>" <?php echo ($key == $data['id_prodi'] ? 'selected' : '') ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="input_prodi"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_skripsi" class="col-md-3 col-form-label">Judul Tugas Akhir</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input_skripsi" name="input_skripsi" value="<?php echo $data['judul_skripsi'] ?>">
                            <div class="invalid-feedback" for="input_skripsi"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_status_alumni" class="col-md-3 col-form-label">Status Alumni <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <select id="input_status_alumni" name="input_status_alumni" class="form-control select2" onChange="update()">
                                <option value="" selected>Pilih Status Alumni</option>
                                <?php foreach ($status_alumni as $key => $value) { ?>
                                    <option value="<?= $key; ?>" <?php echo ($key == $data['id_ref_status_alumni'] ? 'selected' : '') ?>><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="input_status_alumni"></div>
                            <div class="row pt-3">
                                <div class="col-md">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="chek_riwayat" name="chek_riwayat" value="riwayat_baru">
                                        <label class="form-check-label" for="chek_riwayat">Apakah Riwayat Baru?</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row jobs">
                        <label for="input_tanggal_mulai" class="col-md-3 col-form-label">Tanggal Mulai</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control datepicker" id="input_tanggal_mulai" name="input_tanggal_mulai" value="<?php echo (isset($data['tgl_mulai']) ? $data['tgl_mulai'] : '') ?>">
                            <div class="invalid-feedback" for="input_tanggal_mulai"></div>
                        </div>
                    </div>

                    <div class="form-group row study">
                        <label for="input_tanggal_mulai_study" class="col-md-3 col-form-label">Tanggal Mulai</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control datepicker" id="input_tanggal_mulai_study" name="input_tanggal_mulai_study" value="<?php echo (isset($data['tgl_mulai']) ? $data['tgl_mulai'] : '') ?>">
                            <div class="invalid-feedback" for="input_tanggal_mulai_study"></div>
                        </div>
                    </div>

                    <div class="form-group row jobs">
                        <label for="input_instansi" class="col-md-3 col-form-label">Nama Tempat Kerja</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input_instansi" name="input_instansi" value="<?php echo (isset($data['nama_instansi']) ? $data['nama_instansi'] : '') ?>">
                            <div class="invalid-feedback" for="input_instansi"></div>
                        </div>
                    </div>

                    <div class="form-group row jobs">
                        <label for="input_jabatan" class="col-md-3 col-form-label">Jabatan</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input_jabatan" name="input_jabatan" value="<?php echo (isset($data['jabatan']) ? $data['jabatan'] : '') ?>">
                            <div class="invalid-feedback" for="input_jabatan"></div>
                        </div>
                    </div>

                    <div class="form-group row study">
                        <label for="input_univ_baru" class="col-md-3 col-form-label">Nama Perguruan Tinggi</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input_univ_baru" name="input_univ_baru" value="<?php echo (isset($data['nama_instansi']) ? $data['nama_instansi'] : '') ?>">
                            <div class="invalid-feedback" for="input_univ_baru"></div>
                        </div>
                    </div>

                    <div class="form-group row study">
                        <label for="input_prodi_baru" class="col-md-3 col-form-label">Nama Prodi</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input_prodi_baru" name="input_prodi_baru" value="<?php echo (isset($data['prodi']) ? $data['prodi'] : '') ?>">
                            <div class="invalid-feedback" for="input_prodi_baru"></div>
                        </div>
                    </div>

                    <div class="form-group row jobs">
                        <label for="provinsi_instansi" class="col-md-3 col-form-label">Provinsi</label>
                        <div class="col-md-9">
                            <select id="provinsi_instansi" name="provinsi_instansi" class="form-control select2">
                                <option value="" selected>Pilih Provinsi</option>
                                <?php if (isset($data['provinsi_instansi'])) {
                                    foreach ($propinsi as $key => $value) { ?>
                                        <option value="<?= $key; ?>" <?php echo ($key == $data['provinsi_instansi'] ? 'selected' : '') ?>><?php echo $value; ?></option>
                                    <?php }
                                } else {
                                    foreach ($propinsi as $key => $value) { ?>
                                        <option value="<?= $key; ?>"><?php echo $value; ?></option>
                                <?php }
                                } ?>
                            </select>
                            <div class="invalid-feedback" for="provinsi_instansi"></div>
                        </div>
                    </div>

                    <div class="form-group row jobs">
                        <label for="kabupaten_instansi" class="col-md-3 col-form-label">Kabupaten</label>
                        <div class="col-md-9">
                            <select name="kabupaten_instansi" id="kabupaten_instansi" class="form-control select2">
                                <option value="" selected>Pilih Kabupaten</option>
                                <?php if ($instansi_kab_kota) { ?>
                                    <?php foreach ($instansi_kab_kota as $key => $value) { ?>
                                        <option value="<?= $key; ?>" <?php echo ($key == $data['kab_kota_instansi'] ? 'selected' : ''); ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="kabupaten_instansi"></div>
                        </div>
                    </div>

                    <div class="form-group row jobs">
                        <label for="kecamatan_instansi" class="col-md-3 col-form-label">Kecamatan</label>
                        <div class="col-md-9">
                            <select name="kecamatan_instansi" id="kecamatan_instansi" class="form-control select2">
                                <option value="" value="">Pilih Kecamatan</option>
                                <?php if ($instansi_kecamatan) { ?>
                                    <?php foreach ($instansi_kecamatan as $key => $value) { ?>
                                        <option value="<?= $key; ?>" <?php echo ($key == $data['kecamatan_instansi'] ? 'selected' : ''); ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="kecamatan_instansi"></div>
                        </div>
                    </div>

                    <div class="form-group row study">
                        <label for="provinsi_instansi2" class="col-md-3 col-form-label">Provinsi</label>
                        <div class="col-md-9">
                            <select id="provinsi_instansi2" name="provinsi_instansi2" class="form-control select2">
                                <option value="" selected>Pilih Provinsi</option>
                                <?php foreach ($propinsi as $key => $value) { ?>
                                    <option value="<?= $key; ?>" <?php echo ($key == $data['provinsi_instansi'] ? 'selected' : '') ?>><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="provinsi_instansi2"></div>
                        </div>
                    </div>

                    <div class="form-group row study">
                        <label for="kabupaten_instansi2" class="col-md-3 col-form-label">Kabupaten </label>
                        <div class="col-md-9">
                            <select name="kabupaten_instansi2" id="kabupaten_instansi2" class="form-control select2">
                                <option value="" selected>Pilih Kabupaten</option>
                                <?php if ($instansi_kab_kota) { ?>
                                    <?php foreach ($instansi_kab_kota as $key => $value) { ?>
                                        <option value="<?= $key; ?>" <?php echo ($key == $data['kab_kota_instansi'] ? 'selected' : ''); ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="kabupaten_instansi2"></div>
                        </div>
                    </div>

                    <div class="form-group row study">
                        <label for="kecamatan_instansi2" class="col-md-3 col-form-label">Kecamatan </label>
                        <div class="col-md-9">
                            <select name="kecamatan_instansi2" id="kecamatan_instansi2" class="form-control select2">
                                <option value="">Pilih Kecamatan</option>
                                <?php if ($instansi_kecamatan) { ?>
                                    <?php foreach ($instansi_kecamatan as $key => $value) { ?>
                                        <option value="<?= $key; ?>" <?php echo ($key == $data['kecamatan_instansi'] ? 'selected' : ''); ?>><?php echo $value; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <div class="invalid-feedback" for="kecamatan_instansi2"></div>
                        </div>
                    </div>

                    <div class="form-group row jobs">
                        <label for="alamat_instansi" class="col-md-3 col-form-label">Alamat</label>
                        <div class="col-md-9">
                            <textarea name="alamat_instansi" id="alamat_instansi" class="form-control" style="height: 100px;"><?php echo (isset($data['alamat_instansi']) ? $data['alamat_instansi'] : '') ?></textarea>
                        </div>
                        <div class="invalid-feedback" for="alamat_instansi"></div>
                    </div>

                </div>
                <div class="modal-footer">
                    <a href="<?php echo base_url('alumni/dashboard/') ?>" class="btn btn-theme-dark">Kembali</a>
                    <button type="submit" class="btn btn-theme-danger" id="btn-simpan-alumni">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<?php $this->load->view("template/template_scripts") ?>

<script>
    $(".study").hide();
    $(".jobs").hide();
    $(".wiraswasta").hide();

    function update() {
        var select = $('#input_status_alumni :selected').val();
        switch (select) {
            case '1':
                pilih = 1
                $(".jobs").show();
                $(".study").hide();
                $(".wiraswasta").hide();
                break;
            case '3':
                pilih = 1
                $(".jobs").show();
                $(".study").hide();
                $(".wiraswasta").hide();
                break;
            case '4':
                pilih = 2
                $(".study").show();
                $(".jobs").hide();
                $(".wiraswasta").hide();
                break;
            default:
                $(".study").hide();
                $(".jobs").hide();
                $(".wiraswasta").hide();
                break;

        }

    }

    $(function() {
        var select = $('#input_status_alumni :selected').val();
        switch (select) {
            case '1':
                pilih = 1
                $(".jobs").show();
                $(".study").hide();
                $(".wiraswasta").hide();
                break;
            case '3':
                pilih = 1
                $(".jobs").show();
                $(".study").hide();
                if ($('[name="input_sektor2"]:checked').val() == "Pertanian") {
                    $(".wiraswasta").show();
                } else {
                    $(".wiraswasta").hide();
                }
                break;
            case '4':
                pilih = 2
                $(".study").show();
                $(".jobs").hide();
                $(".wiraswasta").hide();
                break;
            default:
                $(".study").hide();
                $(".jobs").hide();
                $(".wiraswasta").hide();
                break;

        }
    })

    /**
     * select kabupaten/kota
     */
    $('[name="input_provinsi"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'alumni/dashboard/getKabKota',
            type: "GET",
            data: {
                kode_provinsi: val
            },
            dataType: "JSON",
            success: function(data) {
                buatKabKot(data);
                $('[name="input_kabupaten"]').select2({
                    placeholder: '- Pilih Kabupaten/Kota -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatKabKot(data) {
        let optionLoop = '<option value>Pilih Kabupaten / Kota</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        $('[name="input_kecamatan"]').children().remove();
        $('[name="input_kecamatan"]').select2({
            placeholder: '- Pilih Kecamatan -',
        });
        if ($('[name="input_kabupaten"]')) {
            $('[name="input_kabupaten"]').children().remove();
        }
        $('[name="input_kabupaten"]').append(optionLoop);
    }

    /**
     * select kecamatan
     */
    $('[name="input_kabupaten"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'alumni/dashboard/getKecamatan',
            type: "GET",
            data: {
                kode_kab_kota: val
            },
            dataType: "JSON",
            success: function(data) {
                buatKecamatan(data);
                $('[name="input_kecamatan"]').select2({
                    placeholder: '- Pilih Kecamatan -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatKecamatan(data) {
        let optionLoop = '<option value>Pilih Kecamatan</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        if ($('[name="input_kecamatan"]')) {
            $('[name="input_kecamatan"]').children().remove();
        }
        $('[name="input_kecamatan"]').append(optionLoop);
    }

    /**
     * select prodi
     */
    $('[name="input_fakultas"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'alumni/dashboard/getprodi',
            type: "GET",
            data: {
                kode_prodi: val
            },
            dataType: "JSON",
            success: function(data) {
                buatProdi(data);
                $('[name="input_prodi"]').select2({
                    placeholder: '- Pilih Program Studi/Jurusan -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatProdi(data) {
        let optionLoop = '<option value>Pilih Program Studi/Jurusan</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        if ($('[name="input_prodi"]')) {
            $('[name="input_prodi"]').children().remove();
        }
        $('[name="input_prodi"]').append(optionLoop);
    }

    /**
     * select kabupaten/kota study
     */
    $('[name="provinsi_instansi"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'alumni/dashboard/getKabKota',
            type: "GET",
            data: {
                kode_provinsi: val
            },
            dataType: "JSON",
            success: function(data) {
                buatKabKotInstansi(data);
                $('[name="kabupaten_instansi"]').select2({
                    placeholder: '- Pilih Kabupaten/Kota -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatKabKotInstansi(data) {
        let optionLoop = '<option value>Pilih Kabupaten / Kota</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        $('[name="kecamatan_instansi"]').children().remove();
        $('[name="kecamatan_instansi"]').select2({
            placeholder: '- Pilih Kecamatan -',
        });
        if ($('[name="kabupaten_instansi"]')) {
            $('[name="kabupaten_instansi"]').children().remove();
        }
        $('[name="kabupaten_instansi"]').append(optionLoop);
    }

    /**
     * select kecamatan study
     */
    $('[name="kabupaten_instansi"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'alumni/dashboard/getKecamatan',
            type: "GET",
            data: {
                kode_kab_kota: val
            },
            dataType: "JSON",
            success: function(data) {
                buatKecamatanInstansi(data);
                $('[name="kecamatan_instansi"]').select2({
                    placeholder: '- Pilih Kecamatan -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatKecamatanInstansi(data) {
        let optionLoop = '<option value>Pilih Kecamatan</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        if ($('[name="kecamatan_instansi"]')) {
            $('[name="kecamatan_instansi"]').children().remove();
        }
        $('[name="kecamatan_instansi"]').append(optionLoop);
    }

    /**
     * select kabupaten/kota jobs
     */
    $('[name="provinsi_instansi2"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'alumni/dashboard/getKabKota',
            type: "GET",
            data: {
                kode_provinsi: val
            },
            dataType: "JSON",
            success: function(data) {
                buatKabKotInstansi2(data);
                $('[name="kabupaten_instansi2"]').select2({
                    placeholder: '- Pilih Kabupaten/Kota -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatKabKotInstansi2(data) {
        let optionLoop = '<option value>Pilih Kabupaten / Kota</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        $('[name="kecamatan_instansi2"]').children().remove();
        $('[name="kecamatan_instansi2"]').select2({
            placeholder: '- Pilih Kecamatan -',
        });
        if ($('[name="kabupaten_instansi2"]')) {
            $('[name="kabupaten_instansi2"]').children().remove();
        }
        $('[name="kabupaten_instansi2"]').append(optionLoop);
    }

    /**
     * select kecamatan job
     */
    $('[name="kabupaten_instansi2"]').change(function(e) {
        var val = e.target.value;
        $.ajax({
            url: site_url + 'alumni/dashboard/getKecamatan',
            type: "GET",
            data: {
                kode_kab_kota: val
            },
            dataType: "JSON",
            success: function(data) {
                buatKecamatanInstansi2(data);
                $('[name="kecamatan_instansi2"]').select2({
                    placeholder: '- Pilih Kecamatan -',
                });
            },
            error: function(xx) {
                alert(xx)
            }
        });
    });

    function buatKecamatanInstansi2(data) {
        let optionLoop = '<option value>Pilih Kecamatan</option>';
        Object.keys(data).forEach(key => {
            optionLoop += `<option value="${key.trim()}">${data[key].trim()}</option>`
        });
        if ($('[name="kecamatan_instansi2"]')) {
            $('[name="kecamatan_instansi2"]').children().remove();
        }
        $('[name="kecamatan_instansi2"]').append(optionLoop);
    }

    /**
     * select kabupaten/kota jobs
     */
    $('[name="input_sektor"]').change(function(e) {
        var val = e.target.value;

        if ($('[name="input_status_alumni"]').val() == 3 && val == "Pertanian") {
            $(".wiraswasta").show();
        } else {
            $(".wiraswasta").hide();
        }
    });

    /**
     * select input sektor jobs
     */
    $('[name="input_sektor2"]').change(function(e) {
        var val = e.target.value;

        if ($('[name="input_status_alumni"]').val() == 3 && val == "Pertanian") {
            $(".wiraswasta").show();
        } else {
            $(".wiraswasta").hide();
        }
    });

    $('[name="input_tipe_usaha"]').change(function(e) {
        var val = e.target.value;

        if (val == 'Lainnya') {
            $('[name="input_lainnya"]').removeAttr('readonly');
        } else {
            $('[name="input_lainnya"]').val('');
            $('[name="input_lainnya"]').attr('readonly', 'readonly');
        }
    });

    $('#form_alumni').on('submit', function(e) {
        e.preventDefault();

        $('#form_alumni').find('input, select').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'alumni/dashboard/editAlumni',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Alumni',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'alumni/dashboard';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })
</script>