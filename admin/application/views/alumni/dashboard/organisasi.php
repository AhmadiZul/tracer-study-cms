<div class="card">
  <div class="card-header">
    <h5 class="text-dark">Organisasi</h5>
  </div>
  <div class="card-body">
    <?php foreach ($organisasi as $key => $value) { ?>
      <div class="card border border-secondary">
        <div class="card-body">
          <h5 class="card-title text-dark"><?= $value['nama_organisasi']; ?></h5>
          <h6 class="card-subtitle mb-2 text-dark"><?= $value['posisi']; ?></h6>
          <?php
            $start = new \DateTime($value['start_date']);
            $startMonth = $start->format('M');
            $startYear = $start->format('Y'); 
            $end = new \DateTime($value['end_date']);
            $endMonth = $end->format('M');
            $endYear = $end->format('Y');
          ?>
          <h6 class="card-subtitle mb-2 text-dark"><?= $startMonth.' '.$startYear ?> s/d <?= $endMonth.' '.$endYear ?></h6>
          <p class="card-text"><?= $value['job_desc']; ?></p>
          <?php if ($value['url_bukti'] != "") : ?>
            <button type="button" class="btn btn-warning" onclick="preview_organisasi('<?= PATH_FILE_FRONT . $value['url_bukti'] ?>')">
              Preview
            </button>
          <?php else : ?>
            <span>Tidak ada sertifikat</span>
          <?php endif ?>
        </div>
      </div>
    <?php } ?>
  </div>
</div>

<div class="modal fade" id="organisasi-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">PDF FILE</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <embed type="application/pdf" id="view-organisasi" width="100%" height="400">
      </div>
      <div class="modal-footer">
        <button id="btCancel" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
  function preview_organisasi(url) {
    $("#view-organisasi").attr('src', url);
    $("#organisasi-modal").modal('show');
  }
</script>