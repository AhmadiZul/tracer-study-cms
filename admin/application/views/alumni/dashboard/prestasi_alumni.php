<div class="card">
  <div class="card-header">
    <h5 class="text-dark">Prestasi</h5>
  </div>
  <div class="card-body">
    <?php foreach ($prestasi as $key => $value) { ?>
      <div class="card">
        <div class="card-body">
          <h5 class="card-title text-dark"><?= $value['nama_prestasi']; ?></h5>
          <?php if ($value['gelar'] == 'Juara 1') : ?>
            <h6 class="card-subtitle mb-2 text-dark"><img src="<?php echo base_url() ?>public/assets/img/alumni/medal-star-emas.svg" alt="mendali"> <?= $value['gelar']; ?> • <?= $value['tingkat']; ?></h6>
          <?php elseif ($value['gelar'] == 'Juara 2') : ?>
            <h6 class="card-subtitle mb-2 text-dark"><img src="<?php echo base_url() ?>public/assets/img/alumni/medal-star-perak.svg" alt="mendali"> <?= $value['gelar']; ?> • <?= $value['tingkat']; ?></h6>
          <?php else : ?>
            <h6 class="card-subtitle mb-2 text-dark"><img src="<?php echo base_url() ?>public/assets/img/alumni/medal-star-perunggu.svg" alt="mendali"> <?= $value['gelar']; ?> • <?= $value['tingkat']; ?></h6>
          <?php endif ?>
          <p class="card-text"><?= $value['penyelenggara']; ?></p>
          <?php if ($value['url_sertifikat'] != "") : ?>
            <button type="button" class="btn btn-warning" onclick="preview_prestasi('<?=PATH_FILE_FRONT.$value['url_sertifikat'] ?>')">
              Preview
            </button>
          <?php else : ?>
            <span>Tidak ada sertifikat</span>
          <?php endif ?>
        </div>
      </div>
    <?php } ?>
  </div>
</div>


<div class="modal fade" id="prestasi" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="prestasi">PDF FILE</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <embed type="application/pdf" id="view-prestasi" width="100%" height="400">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
  function preview_prestasi(url) {
    $("#view-prestasi").attr('src',url);
    $("#prestasi").modal('show');
  }
</script>