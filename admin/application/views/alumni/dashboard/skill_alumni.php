<div class="card">
  <div class="card-header">
    <h5 class="text-dark">Skill</h5>
  </div>
  <div class="card-body">
    <?php foreach ($skill as $key => $value) { ?>
      <div class="card border border-secondary">
        <div class="card-body">
          <h5 class="card-title text-dark"><?= $value['nama_skill']; ?></h5>
          <h6 class="card-subtitle mb-2 text-dark"><?= $value['bidang']; ?> • <?= $value['level']; ?></h6>
          <?php if ($value['url_sertifikat'] != "") : ?>
            <button type="button" class="btn btn-warning" onclick="preview_skill('<?= PATH_FILE_FRONT . $value['url_sertifikat'] ?>')">
              Preview
            </button>
          <?php else : ?>
            <span>Tidak ada sertifikat</span>
          <?php endif ?>
        </div>
      </div>
    <?php } ?>
  </div>
</div>

<div class="modal fade" id="skill-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">PDF FILE</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <embed type="application/pdf" id="view-skill" width="100%" height="400">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
  function preview_skill(url) {
    $("#view-skill").attr('src', url);
    $("#skill-modal").modal('show');
  }
</script>