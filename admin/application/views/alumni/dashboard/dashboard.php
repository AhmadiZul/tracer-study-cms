<div class="section-header justify-content-between">
	<div class="col-4">
		<h1><i class="far fa-object-ungroup"></i> Dashboard</h1>
	</div>
	<div class="col-2">
		<a href="" role="button" class="btn btn-light float-end" style="border-radius: 16px;"><i class="fas fa-rotate"></i> Refresh</a>
	</div>
</div>

<div class="bg-light-green top-border"></div>
<div class="topi card">
	<div class="card-body">
		<div class="row">
			<div class="col-lg-9 col-xs-12">
				<h2>Selamat Pagi, Rafli</h2>
				<p>Selamat Datang di Aplikasi LinkAN Pusdiktan, Tracer Study Alumni. Dimohon untuk segera melakukan Pembaharuan Profil</p>
			</div>
			<div class="col-lg-3 col-xs-12">
				<img class="img-fluid" src="<?php echo base_url()?>public/assets/img/admin/selamat-pagi.png" alt="">
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-8 col-xs-12">
		<div class="card">
			<div class="card-header">
				<h3>Tahapan Rekrutmen</h3>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-3 col-xs-12">
						<img class="img-fluid" src="<?php echo base_url()?>public/assets/img/admin/rekrutmen.png" alt="">
					</div>
					<div class="col-lg-9 col-xs-12">
						<h2>Segera lakukan pengisian borang tracer study dan lakukan pendaftaran kerja</h2>
						<p>Lamar pekerjaan untuk alumni binaan Pusdiktan dan dapatkan pekerja yang unggul serta kompetitif.</p>
						<a href="" role="button" class="btn btn-success">Lamar Pekerjaan</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-xs-12">
		<div class="card">
			<div class="card-body">
				<div class="col-lg-12 text-center">
					<img class="img-fluid" style="border-radius: 1000px;box-shadow: 0 0 0 16px #E8F0E0;margin-bottom: 24px;margin-top: 24px;" src="<?php echo base_url()?>public/assets/img/alumni/card/foto-1.png" alt="">
					<h2>Rafli Hillan Yufandani</h2>
					<p><i class="fas fa-location-dot"></i> Malang, Jawa Timur</p>
					<label>Status Anda : <span class="badge badge-success">Alumni</span></label>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-8 col-xs-12">
		<div class="card bg-dark-green">
			<div class="card-body">
				<img class="img-fluid" style="margin-bottom: 24;" src="<?php echo base_url()?>public/assets/img/admin/segera.png" alt="">
				<h2 class="text-white">Segera isi tracer study dan lamar pekerjaan sesuai bidangmu</h2>
				<p class="text-white">Pendataan alumni binaan Pusat Pendidikan Pertanian</p>
				<a href="" role="button" class="btn btn-light-success">Lihat Selengkapnya</a>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-xs-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-9">
						<h2>Selamat Pagi, Rafli</h2>
						<p>Selamat Datang di Aplikasi LinkAN Pusdiktan, Tracer Study Alumni. Dimohon untuk segera melakukan Pembaharuan Profil</p>
					</div>
					<div class="col-3">
						<img class="img-fluid" src="<?php echo base_url()?>public/assets/img/admin/selamat-pagi.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>