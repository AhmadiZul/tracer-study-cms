<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <small>
                    <?php echo $output->output; ?>
                </small>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>



<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>

<script>
    $(document).ready(function() {
        window.addEventListener('gcrud.datagrid.ready', () => {
            $(".gc-header-tools").append(
                `<div class="floatL">
            <button id="btn-unduh" class="btn btn-default btn-outline-dark t5">
              <i class="fa fa-plus floatL t3"></i>
              <span class="hidden-xs floatL l5">Tambah Alumni</span>
              <div class="clear">
              </div>
            </button>
          </div>` 
            );

            $('#btn-unduh').click(function(e) {
                e.preventDefault();
                window.location.href = site_url + 'alumni/dashboard/addAlumni';
            })
        });
    })
</script>

<script>
    function update() {
        var select = $('#status :selected').val();
        console.log(select)
    }

    function verifAlumni(id) {
        console.log(id)
        var formData = {
            id_mhs: id,
            status_data: '1'
        };
        console.log(formData)
        $.ajax({
            type: 'ajax',
            method: 'POST',
            url: site_url + 'alumni/dashboard/verifAlumni',
            data: formData,
            dataType: 'json',
            encode: true,
            success: function(response) {
                if (response.success) {
                    Swal.fire('Proses Berhasil!', response.message, 'success').then(function() {
                        $('.fa-refresh').trigger('click');
                    })
                } else {
                    Swal.fire('Proses Gagal!', response.message, 'error');
                }
            },
            error: function(xmlresponse) {
                console.log(xmlresponse);
            }
        });
    }

    /**
     * Cek Import File
     */
    function cekFile(input, name) {
        var limit = 2048576; // 2 MB
        if (input.files && input.files[0]) {
            var filesize = input.files[0].size;
            var filetype = (input.files[0].name).split('.').pop().toLowerCase();
            if (input.name == 'url_photo') {
                var allowed = ['png', 'jpg', 'jpeg'];
                var sizePreview = ['250px', '250px'];
            } else {
                var allowed = ['png', 'jpg', 'jpeg', 'pdf'];
                var sizePreview = ['100%', '800'];
            }
            if (filesize < limit && allowed.includes(filetype)) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);

                if (filetype == 'pdf') {
                    $('#preview-' + name).html('<embed src="' + URL.createObjectURL(input.files[0]) + '" class="mt-4" width="' + sizePreview[0] + '" height="' + sizePreview[1] + '" />')
                } else {
                    $('#preview-' + name).html('<img src="' + URL.createObjectURL(input.files[0]) + '" class="mt-4" alt="your image" style="width: ' + sizePreview[0] + '"/>')
                }

                $(file_info).addClass('text-success').html(input.files[0].name + ' (' + getFileSize(filesize) + ') <i class="fa fa-circle-check text-success"></i>');
            } else {
                $(input).val('');
                $('#preview-' + name).html('');
                if (allowed.includes(filetype) === false)
                    $(file_info).addClass('text-danger').html("<i>Jenis file '" + filetype.toUpperCase() + "' tidak diijinkan. </i>")
                if (filesize > limit)
                    $(file_info).addClass('text-danger').html('<i>Ukuran file tidak boleh lebih dari 2 MB</i>')
            }

        } else {
            $(input).val('');
            $('#preview-' + name).html('');
            $(file_info).addClass('text-muted').html("Tidak ada berkas yang dipilih")
        }
    }

    function importFile(input) {
        var filetype = (input.files[0].name).split('.').pop().toLowerCase();
        var allowed = ['xlsx', 'xls'];

        if (allowed.includes(filetype)) {
            $.ajax({
                url: site_url + 'alumni/dashboard/importExcel',
                data: new FormData($('#import_excel')[0]),
                dataType: 'json',
                type: 'POST',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    showLoading();
                },
                success: function(response) {
                    if (response.success) {
                        Swal.fire('Proses Berhasil!', response.text, 'success').then(function() {
                            $('.fa-refresh').trigger('click');
                            $('#import_file').val('');
                        })
                        hideLoading();
                    } else {
                        $('.alert').addClass('show fade');
                        $('#pesan').html(response.text);
                        /* $(".alert").fadeTo(10000, 500).slideUp(500, function() {
                            $(".alert").slideUp(500);
                        }); */
                        $('#import_file').val('');
                        hideLoading();
                    }
                },
                error: function(xmlresponse) {
                    console.log(xmlresponse);
                    $('#import_file').val('');
                    hideLoading();
                }
            });
        } else {
            Swal.fire('File tidak sesuai!', 'File harus berformat xlsx/xls', 'error');
            $('#import_file').val('');
        }
    }
</script>