<style>
	.btn-light-back {
		background-color: #FFE8DE;
		margin-right: 10px;
	}

	.btn-light-back:hover {
		background-color: #FF844B;
	}

	.bg-lime {
		background-color: #E8F0E0;
	}
</style>
<section id="login" class="vh-100">
	<div class="vh-100">
		<div class="row justify-content-center vh-100">
			<div class="col-xs-12 col-lg-8 align-self-center mx-0">
				<div class="text-center">
					<img class="rounded mb-3" src="<?php echo base_url() ?>public/assets/img/login/logo-login.png">
				</div>
				<div class="bg-light-green top-border"></div>
				<div class="card">
					<div class="card-body">
						<a href="<?php echo base_url() ?>Login" role="button" class="d-inline btn btn-light-danger btn-light-back mr-md-3"><i class="fas fa-chevron-left"></i> Kembali</a>
						<h2 class="d-inline card-title"><?php echo $title ?></h2>
						<hr>
						<p class="card-text">Silahkan isi formulir berikut menggunakan data yang sebenar-benarnya</p>
						<div class="alert alert-primary">
							<i class="fas fa-info-circle"></i> Isikan “Profil Alumni” jika Anda merupakan Lulusan sekolah binaan Pusdiktan.
						</div>
						<div class="badge bg-lime text-success">IDENTITAS DIRI</div>
						<div class="form-group">
							<label>Nama Lengkap <b class="text-success">*</b></label>
							<input type="text" name="nama_lengkap" class="form-control" placeholder="Masukkan nama lengkap anda" required>
						</div>
						<div class="form-group">
							<label>Nomor Induk Mahasiswa/Siswa (NIM/NIS) <b class="text-success">*</b></label>
							<input type="text" name="nim" class="form-control" onkeypress="return onlyNumber(event)" placeholder="Masukkan NIM/NIS anda" required>
						</div>
						<div class="form-group">
							<label>Nomor Induk Kependudukan (NIK) <b class="text-success">*</b></label>
							<input type="text" name="nik" maxlength="16" minlength="16" class="form-control" onkeypress="return onlyNumber(event)" placeholder="Masukkan NIK anda" required>
						</div>
						<div class="form-group">
							<label>Tempat Lahir <b class="text-success">*</b></label>
							<input type="text" name="tempat_lahir" class="form-control" placeholder="Masukkan Tempat Lahir Anda" required>
						</div>
						<div class="form-group">
							<label>Tanggal Lahir <b class="text-success">*</b></label>
							<input type="text" name="tanggal_lahir" class="form-control datepicker" placeholder="Masukkan Tanggal Lahir Anda" required>
						</div>
						<div class="form-group">
							<label>Jenis Kelamin <b class="text-success">*</b></label>
							<div class="custom-control custom-radio">
								<label class="custom-control-label">
									<input type="radio" name="jenis_kelamin" value="Laki-laki" class="custom-control-input" required>
									Laki-laki</label>
							</div>
							<div class="custom-control custom-radio">
								<label class="custom-control-label">
									<input type="radio" name="jenis_kelamin" value="Perempuan" class="custom-control-input" required>
									Perempuan</label>
							</div>
						</div>
						<div class="form-group">
							<label>Provinsi <b class="text-success">*</b></label>
							<select name="kode_provinsi" id="kode_provinsi" class="form-control" required>
								<option value="">- Pilih Provinsi -</option>
							</select>
						</div>
						<div class="form-group">
							<label>Kabupaten/Kota <b class="text-success">*</b></label>
							<select name="kode_kabupaten" id="kode_kabupaten" class="form-control" required>
								<option value="">- Pilih Kabupaten/Kota -</option>
							</select>
						</div>
						<div class="form-group">
							<label>Kecamatan <b class="text-success">*</b></label>
							<select name="kode_kecamatan" id="kode_kecamatan" class="form-control" required>
								<option value="">- Pilih Kecamatan -</option>
							</select>
						</div>
						<div class="form-group">
							<label>Detail Alamat <b class="text-success">*</b></label>
							<textarea name="alamat" id="alamat" class="form-control" required style="height: 100px;"></textarea>
						</div>
						<hr>
						<div class="badge bg-lime text-success">PROFIL ALUMNI</div>
						<div class="form-group">
							<label>Tahun Kelulusan <b class="text-success">*</b></label>
							<select name="tahun_angkatan" id="tahun_angkatan" class="form-control" required>
								<option value="">- Pilih Tahun Kelulusan -</option>
							</select>
						</div>
						<div class="form-group">
							<label>Jenjang Pendidikan <b class="text-success">*</b></label>
							<select name="jenjang_pendidikan" id="jenjang_pendidikan" class="form-control" required>
								<option value="">- Pilih Jenjang Pendidikan -</option>
							</select>
						</div>
						<div class="form-group">
							<label>Prodi/Jurusan <b class="text-success">*</b></label>
							<select name="kode_prodi" id="kode_prodi" class="form-control" required>
								<option value="">- Pilih Prodi/Jurusan -</option>
							</select>
						</div>
						<div class="badge bg-lime text-success">BUAT AKUN</div>
						<div class="form-group">
							<label>Email <b class="text-success">*</b></label>
							<input type="email" name="email" class="form-control" placeholder="Masukkan email anda" required>
						</div>

						<div class="form-group">
							<label>Password <b class="text-success">*</b></label>
							<input type="password" name="password" class="form-control" placeholder="Masukkan password anda" required>
						</div>
						<div class="form-group">
							<label>Konfirmasi Password <b class="text-success">*</b></label>
							<input type="password" name="konfirmasi_password" class="form-control" placeholder="Masukkan konfirmasi password anda" required>
						</div>
						<div class="d-flex justify-content-center">
							<button class="btn btn-success btn-lg">SUBMIT</button>
						</div>
						<div class="d-flex justify-content-center">
							<small class="mt-3">Sudah punya akun? <a href="<?php echo site_url('Login') ?>"><b>Klik Disini</b></a></small>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>