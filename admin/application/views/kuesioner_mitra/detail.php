<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="<?php echo base_url('kuesioner_mitra/index/createKuesioner') ?>" method="post" id="myformkuisioner">

                    <div class="form-group row">
                        <label for="urutan" class="col-md-3 col-form-label">Urutan<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="urutan" name="urutan" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_pertanyaan" class="col-md-3 col-form-label">Pertanyaan<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="input_pertanyaan" name="input_pertanyaan" value=""></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_jenis" class="col-md-3 col-form-label">Jenis Pertanyaan<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <select id="input_jenis" name="input_jenis" class="form-control select2" onChange="update()" required>
                                <option selected>Pilih Jenis Pertanyaan</option>
                                <option value="1">Jawaban Singkat</option>
                                <option value="2">Paragraf</option>
                                <option value="3">Pilihan Ganda</option>
                                <option value="4">Kotak Centang</option>
                                <option value="5">Skala Linier</option>
                                <option value="7">Lokasi</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row singkat">
                        <label for="input_singkat" class="col-md-3 col-form-label">Jawaban<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <select id="input_singkat" name="input_singkat" class="form-control select2" required>
                                <option value="tidak" selected>Teks Biasa (default)</option>
                                <option value="integer">Harus Angka</option>
                                <option value="email">Harus Email Valid</option>
                                <option value="is_date">Harus Tanggal</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row paragraf">
                        <label for="input_paragraf" class="col-md-3 col-form-label">Jawaban<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="input_paragraf" name="input_paragraf" value=""></textarea>
                        </div>
                    </div>

                    <div class="container pilgan" id="con_pilgan">
                        <input type="hidden" name="total" value="1">
                        <button type="button" class="btn btn-success mb-2" id="jawaban_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>
                        <div class="form-group row" id='pilgan0'>
                            <label for="input_pilgan" class="col-md-3 col-form-label">Pilihan Jawaban<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="input_pilgan" name="input_pilgan0" value="" multiple>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-danger" onclick='hapusPilgan(0)'>Hapus</button>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md">
                                        <div class="form-check">
                                            <input type="checkbox" onclick="addpilgan(0)" class="form-check-input" id="add_pilgan0" name="add_pilgan0" value="isian[on]">
                                            <label class="form-check-label" for="add_pilgan">Tambah Isian</label>
                                        </div>
                                    </div>
                                    <div class="col-md">
                                        <select id="isian_pilgan0" class="isian_pilgan0 form-control" name="isian_pilgan0">
                                            <option value="" selected>Teks Biasa (default)</option>
                                            <option value="integer">Harus Angka</option>
                                            <option value="valid_email">Harus Email Valid</option>
                                            <option value="is_date">Harus Tanggal</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row lainnya_kokcen">
                        <label for="lainnya" class="col-md-3 col-form-label">Lainnya<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="lainnya" name="lainnya" value="lainnya[on]">
                                <label class="form-check-label" for="lainnya">Lainnya [on]</label>
                            </div>
                        </div>
                    </div>

                    <div class="container kokcen" id="form_kokcen">
                        <input type="hidden" name="totalU" value="1">
                        <button type="button" class="btn btn-success mb-2" id="kokcen_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>
                        <div class="form-group row" id="kokcen0">
                            <label for="input_kokcen" class="col-md-3 col-form-label">Pilihan Jawaban<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="input_kokcen" name="input_kokcen0" value="" multiple>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-danger" onclick='hapusKokcen(0)'>Hapus</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container linier" id="form_linier">
                        <input type="hidden" name="totalL" value="1">
                        <div class="form-group row">
                            <label for="" class="col-md-3 col-form-label">Keterangan<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <p>Skala likert digunakan untuk mengukur sikap atau pendapat. Aplikasi ini menggunakan skala angka 1 - 5 yang berarti bahwa <b> 5: Sangat Setuju (SS), 4: Setuju (S), 3:Ragu-ragu (RG), 2:Tidak Setuju (TS), 1:Sangat Tidak Setuju (STS) </b></p>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success mb-2" id="linier_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>
                        <div class="form-group row" id="linier0">
                            <label for="input_linier" class="col-md-3 col-form-label">Pilihan Jawaban<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="input_linier" name="input_linier0" value="" multiple>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-danger" onclick='hapusLinier(0)'>Hapus</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="container sub" id="form_sub">
                        <input type="hidden" name="totalS" value="1">
                        <div class="form-group row">
                            <label for="" class="col-md-3 col-form-label">Keterangan<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <p>Jawaban dari Sub Pertanyaan bertipe Isian Singkat Maksimal 15 Sub Pertanyaan dalam 1 Pertanyaan</p>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success mb-2" id="sub_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>
                        <div class="form-group row" id="sub0">
                            <label for="input_sub" class="col-md-3 col-form-label">Pilihan Jawaban<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="input_sub" name="input_sub0" value="" multiple>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-danger" onclick='hapusSub(0)'>Hapus</button>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <select id="isian_sub0" class="isian_sub0 form-control" name="isian_sub0">
                                            <option value="" selected>Teks Biasa (default)</option>
                                            <option value="integer">Harus Angka</option>
                                            <option value="valid_email">Harus Email Valid</option>
                                            <option value="is_date">Harus Tanggal</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url('kuesioner_mitra/index/') ?>" class="btn btn-warning">Kembali</a>
                <button type="submit" class="btn btn-success" id="btn-simpan-kuisioner">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>


<?php $this->load->view("template/template_scripts") ?>



<script>
    $(".singkat").hide(200);
    $(".isian_pilgan0").hide(200);
    $(".paragraf").hide(200);
    $(".pilgan").hide(200);
    $(".lainnya_kokcen").hide(200);
    $(".kokcen").hide(200);
    $(".linier").hide(200);
    $(".sub").hide(200);

    function update() {
        var select = $('#input_jenis :selected').val();
        switch (select) {
            case '1':
                $(".singkat").show(300);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").hide(200);
                break;
            case '2':
                $(".singkat").hide(200);
                $(".paragraf").show(300);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").hide(200);
                break;
            case '3':
                $(".singkat").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").show(300);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").hide(200);
                break;
            case '4':
                $(".singkat").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").show(300);
                $(".kokcen").show(300);
                $(".linier").hide(200);
                $(".sub").hide(200);
                break;
            case '5':
                $(".singkat").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").show(300);
                $(".sub").hide(200);
                break;
            case '6':
                $(".singkat").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").show(300);
                break;
            default:
                $(".singkat").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").hide(300);
                break;

        }

    }

    var total = 1;
    $("#jawaban_plus").click(function() {
        $("#con_pilgan").append("<div class='form-group row' id='pilgan" + total + "'><label for='input_pilgan' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-9'><input type='text' class='form-control' id='input_pilgan' name='input_pilgan" + total + "' value='' multiple></div><div class='col-md-3'><button class='btn btn-danger' onclick='hapusPilgan(" + total + ")'>Hapus</button></div></div><br><div class='row'><div class='col-md'><div class='form-check'><input type='checkbox' onclick='addpilgan(" + total + ")' class='form-check-input' id='add_pilgan" + total + "' name='add_pilgan" + total + "' value='isian[on]'>><label class='form-check-label' for='add_pilgan'>Tambah Isian</label></div></div><div class='col-md'><select id='isian_pilgan" + total + "' class='isian_pilgan" + total + " form-control' name='isian_pilgan" + total + "'><option value='' selected>Teks Biasa (default)</option><option value='integer'>Harus Angka</option><option value='valid_email'>Harus Email Valid</option><option value='is_date'>Harus Tanggal</option></select></div></div></div>");
        var isian_awal = ".isian_pilgan" + total.toString()
        $(isian_awal).hide(200);
        total += 1;
        $('[name=total]').val(total);
    });

    var totalU = 1;
    $("#kokcen_plus").click(function() {
        $("#form_kokcen").append("<div class='form-group row' id='kokcen" + totalU + "'><label for='form_kokcen' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-9'><input type='text' class='form-control' id='input_kokcen' name='input_kokcen" + totalU + "' value='' multiple></div><div class='col-md-3'><button class='btn btn-danger' onclick='hapusKokcen(" + totalU + ")'>Hapus</button></div></div></div>");
        totalU += 1;
        $('[name=totalU]').val(totalU);
    });

    var totalL = 1;
    $("#linier_plus").click(function() {
        $("#form_linier").append("<div class='form-group row' id='linier" + totalL + "'><label for='input_linier' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-9'><input type='text' class='form-control' id='input_linier' name='input_linier" + totalL + "' value='' multiple></div><div class='col-md-3'><button class='btn btn-danger' onclick='hapusLinier(" + totalL + ")'>Hapus</button></div></div></div>");
        totalL += 1;
        $('[name=totalL]').val(totalL);
    });

    var totalS = 1;
    $("#sub_plus").click(function() {
        $("#form_sub").append("<div class='form-group row' id='sub" + totalS + "'><label for='input_sub' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-7'><input type='text' class='form-control' id=input_sub' name='input_sub" + totalS + "' value='' multiple></div><div class='col-md-3'><input type='text' class='form-control' id='inputKodeSub' name='inputKodeSub" + totalS + "' placeholder='kode sub' value='' multiple></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusSub(" + totalS + ")'>Hapus</button></div><br><div class='row'><div class='col-md-6'><select id='isian_sub" + totalS + "' class='isian_sub" + totalS + " form-control' name='isian_sub" + totalS + "'><option value='' selected>Teks Biasa (default)</option><option value='integer'>Harus Angka</option><option value='valid_email'>Harus Email Valid</option><option value='is_date'>Harus Tanggal</option></select></div></div></div></div>");
        totalS += 1;
        $('[name=totalS]').val(totalS);
    });

    $('#btn-simpan-kuisioner').click(function(e) {
        var input_kategori = $("#input_kategori").val();
        var input_kode = $("#input_kode").val();
        var input_pertanyaan = $("#input_pertanyaan").val();
        var input_jenis = $("#input_jenis").val();

        const isFilled = (input_kategori != "" && input_kode != "" && input_pertanyaan != "" && input_jenis != "");
        e.preventDefault();

        if (isFilled) {
            $.ajax({
                type: 'ajax',
                method: 'post',
                url: site_url + 'kuesioner_mitra/index/createKuesioner',
                data: new FormData($('#myformkuisioner')[0]),
                async: false,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        Swal.fire('Proses Berhasil!', response.message, 'success').then(function() {
                            window.location.href = "<?php echo base_url('kuesioner_mitra/index/') ?>";
                        })
                    } else {
                        Swal.fire('Proses Gagal!', response.message, 'error');
                    }
                },
                error: function(xmlresponse) {
                    console.log(xmlresponse);
                }
            })
        } else {
            Swal.fire({
                confirmButtonColor: '#3ab50d',
                icon: 'error',
                title: 'Peringatan',
                text: 'Isian bertanda bintang wajib diisi',
            })
        }
    })

    function hapusPilgan(id) {
        let hapus = 'pilgan' + id.toString()
        console.log(hapus)
        total = total - 1
        $('[name=total]').val(total);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function hapusKokcen(id) {
        let hapus = 'kokcen' + id.toString()
        console.log(hapus)
        totalU = totalU - 1
        $('[name=totalU]').val(totalU);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function hapusLinier(id) {
        let hapus = 'linier' + id.toString()
        console.log(hapus)
        totalL = totalL - 1
        $('[name=totalL]').val(totalL);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function hapusSub(id) {
        let hapus = 'sub' + id.toString()
        console.log(hapus)
        totalS = totalS - 1
        $('[name=totalS]').val(totalS);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function addpilgan(id) {
        var x = document.getElementById("add_pilgan" + id.toString()).checked;
        console.log(x);
        var isian = ".isian_pilgan" + id.toString()
        if (x) {

            $(isian).show(300);
        } else {
            $(isian).hide(200);
        }
    }
</script>