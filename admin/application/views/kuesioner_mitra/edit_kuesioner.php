<div class="row">
    <div class="col-xs-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="<?php echo base_url('kuesioner_mitra/index/updateKuesioner') ?>" method="post" id="myformkuisioner">

                    <div class="form-group row">
                        <label for="urutan" class="col-md-3 col-form-label">Urutan<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="urutan" name="urutan" value="<?= $data['urutan']; ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <input type="hidden" name="id_kuesioner" id="id_kuesioner" value="<?php echo $data['id_kuesioner'] ?>">
                        <label for="input_pertanyaan" class="col-md-3 col-form-label">Pertanyaan<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <input class="form-control" id="input_pertanyaan" name="input_pertanyaan" value="<?= $data['question']; ?>">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="input_jenis" class="col-md-3 col-form-label">Jenis Pertanyaan<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <select id="input_jenis" name="input_jenis" class="form-control select2" onChange="update()" required>
                                <option selected>Pilih Jenis Pertanyaan</option>
                                <option value="1">Jawaban Singkat</option>
                                <option value="2">Paragraf</option>
                                <option value="3">Pilihan Ganda</option>
                                <option value="4">Kotak Centang</option>
                                <option value="5">Skala Linier</option>
                                <option value="7">Lokasi</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row singkat">
                        <label for="input_singkat" class="col-md-3 col-form-label">Jawaban<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <select id="input_singkat" name="input_singkat" class="form-control select2" required>
                                <option value="tidak" selected>Teks Biasa (default)</option>
                                <option value="integer">Harus Angka</option>
                                <option value="email">Harus Email Valid</option>
                                <option value="is_date">Harus Tanggal</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row paragraf">
                        <label for="input_paragraf" class="col-md-3 col-form-label">Jawaban<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="input_paragraf" name="input_paragraf" value="<?= $data['others']; ?>"></textarea>
                        </div>
                    </div>

                    <div class="container pilgan" id="con_pilgan">
                        <input type="hidden" name="total" value="1">
                        <button type="button" class="btn btn-success mb-2" id="jawaban_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>

                    </div>

                    <div class="form-group row lainnya_kokcen">
                        <label for="lainnya" class="col-md-3 col-form-label">Lainnya<small class="text-danger">*</small></label>
                        <div class="col-md-9">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="lainnya" name="lainnya" value="lainnya[on]" <?php echo ($data['others'] == 'lainnya[on]' ? 'checked' : '') ?>>
                                <label class="form-check-label" for="lainnya">Lainnya [on]</label>
                            </div>
                        </div>
                    </div>

                    <div class="container kokcen" id="form_kokcen">
                        <input type="hidden" name="totalU" value="1">
                        <button type="button" class="btn btn-success mb-2" id="kokcen_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>

                    </div>

                    <div class="container linier" id="form_linier">
                        <input type="hidden" name="totalL" value="1">
                        <div class="form-group row">
                            <label for="" class="col-md-3 col-form-label">Keterangan<small class="text-danger">*</small></label>
                            <div class="col-md-9">
                                <p>Skala likert digunakan untuk mengukur sikap atau pendapat. Aplikasi ini menggunakan skala angka 1 - 5 yang berarti bahwa <b> 5: Sangat Setuju (SS), 4: Setuju (S), 3:Ragu-ragu (RG), 2:Tidak Setuju (TS), 1:Sangat Tidak Setuju (STS) </b></p>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success mb-2" id="linier_plus"><i class="fas fa-plus"></i> Tambah Pilihan Jawaban</button>

                    </div>


            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url('kuesioner_mitra/index/') ?>" class="btn btn-warning">Kembali</a>
                <button type="submit" class="btn btn-success" id="btn-simpan-kuisioner">Simpan</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>


<?php $this->load->view("template/template_scripts") ?>



<script>
    $(".singkat").hide(200);
    $(".isian_pilgan0").hide(200);
    $(".paragraf").hide(200);
    $(".pilgan").hide(200);
    $(".linier").hide(200);
    $(".lainnya_kokcen").hide(200);
    var totalS = 0;
    var total = 0;
    var totalL = 0;
    var totalU = 0;
    $(function() {
        var jenis = '<?php echo $data['question_type'] ?>';
        var answer = <?php echo json_encode($data['answer']) ?>;
        $("#input_jenis").val(jenis).change();
        console.log(jenis)
        switch (jenis) {
            case '1':
                var singkat = '<?php echo $data['answer_rules'] ?>';
                $("#input_singkat").val(singkat).change();
                break;
            case '2':
                var paragraf = '<?php echo $data['others'] ?>';
                break;
            case '3':
                $.each(answer, function(index, value) {
                    $("#con_pilgan").append("<div class='form-group row' id='pilgan" + total + "'><label for='input_pilgan' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-10'><input type='text' name='id_pilgan[]' value='" + value["id_opsi"] + "' class='form-control d-none'><input type='text' class='form-control' id='input_pilgan' name='input_pilgan[]' value='" + value["opsi"] + "' multiple></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusPilgan(" + total + ")'>Hapus</button></div></div><br><div class='row'><div class='col-md'><div class='form-check'><input type='checkbox' onclick='addpilgan(" + total + ")' class='form-check-input' id='add_pilgan" + total + "' name='add_pilgan[]' value='isian[on]'><label class='form-check-label' for='add_pilgan'>Tambah Isian</label></div></div><div class='col-md'><select id='isian_pilgan" + total + "' class='isian_pilgan" + total + " form-control' name='isian_pilgan[]'><option value='' selected>Teks Biasa (default)</option><option value='integer'>Harus Angka</option><option value='valid_email'>Harus Email Valid</option><option value='is_date'>Harus Tanggal</option></select></div></div></div>");
                    var isian_awal = ".isian_pilgan" + total.toString()
                    $(isian_awal).hide(200);

                    if (value["isian"] == 'isian[on]') {
                        $(isian_awal).show(300);
                        $(isian_awal).val(value["answer_rules"]).trigger('change');
                        $('#add_pilgan' + total).prop('checked', true);
                    }
                    total += 1;
                    $('[name=total]').val(total);
                });
                break;
            case '4':
                $.each(answer, function(index, value) {
                    $("#form_kokcen").append("<div class='form-group row' id='kokcen" + totalU + "'><label for='form_kokcen' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-10'><input type='text' name='id_kokcen[]' value='" + value["id_opsi"] + "' class='form-control d-none'><input type='text' class='form-control' id='input_kokcen' name='input_kokcen[]' value='" + value["opsi"] + "' multiple></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusKokcen(" + totalU + ")'>Hapus</button></div></div></div>");
                    totalU += 1;
                    $('[name=totalU]').val(totalU);
                });
                break;
            case '5':
                $.each(answer, function(index, value) {
                    $("#form_linier").append("<div class='form-group row' id='linier" + totalL + "'><label for='input_linier' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-10'><input type='text' name='id_likert[]' value='" + value["id_likert"] + "' class='form-control d-none'><input type='text' class='form-control' id='input_linier' name='input_linier[]' value='" + value["pertanyaan"] + "' multiple></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusLinier(" + totalL + ")'>Hapus</button></div></div></div>");
                    totalL += 1;
                    $('[name=totalL]').val(totalL);
                });
                break;
            default:
                $(".singkat").hide(200);
                $(".isian_pilgan0").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").hide(200);
                break;

        }

    });

    function update() {
        var select = $('#input_jenis :selected').val();
        switch (select) {
            case '1':
                $(".singkat").show(300);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").hide(200);
                break;
            case '2':
                $(".singkat").hide(200);
                $(".paragraf").show(300);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").hide(200);
                break;
            case '3':
                $(".singkat").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").show(300);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").hide(200);
                break;
            case '4':
                $(".singkat").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").show(300);
                $(".kokcen").show(300);
                $(".linier").hide(200);
                $(".sub").hide(200);
                break;
            case '5':
                $(".singkat").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").show(300);
                $(".sub").hide(200);
                break;
            case '6':
                $(".singkat").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").show(300);
                break;
            default:
                $(".singkat").hide(200);
                $(".paragraf").hide(200);
                $(".pilgan").hide(200);
                $(".lainnya_kokcen").hide(200);
                $(".kokcen").hide(200);
                $(".linier").hide(200);
                $(".sub").hide(200);
                break;

        }

    }

    $("#jawaban_plus").click(function() {
        $("#con_pilgan").append("<div class='form-group row' id='pilgan" + total + "'><label for='input_pilgan' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-10'><input type='text' name='id_pilgan[]' value='' class='form-control d-none'><input type='text' class='form-control' id='input_pilgan' name='input_pilgan[]' value='' multiple></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusPilgan(" + total + ")'>Hapus</button></div></div><br><div class='row'><div class='col-md'><div class='form-check'><input type='checkbox' onclick='addpilgan(" + total + ")' class='form-check-input' id='add_pilgan" + total + "' name='add_pilgan[]' value='isian[on]'><label class='form-check-label' for='add_pilgan'>Tambah Isian</label></div></div><div class='col-md'><select id='isian_pilgan" + total + "' class='isian_pilgan" + total + " form-control' name='isian_pilgan[]'><option value='' selected>Teks Biasa (default)</option><option value='integer'>Harus Angka</option><option value='valid_email'>Harus Email Valid</option><option value='is_date'>Harus Tanggal</option></select></div></div></div>");
        var isian_awal = ".isian_pilgan" + total.toString()
        $(isian_awal).hide(200);
        total += 1;
        $('[name=total]').val(total);
    });

    $("#kokcen_plus").click(function() {
        $("#form_kokcen").append("<div class='form-group row' id='kokcen" + totalU + "'><label for='form_kokcen' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-10'><input type='text' name='id_kokcen[]' value='' class='form-control d-none'><input type='text' class='form-control' id='input_kokcen' name='input_kokcen[]' value='' multiple></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusKokcen(" + totalU + ")'>Hapus</button></div></div></div>");
        totalU += 1;
        $('[name=totalU]').val(totalU);
    });



    $("#linier_plus").click(function() {
        $("#form_linier").append("<div class='form-group row' id='linier" + totalL + "'><label for='input_linier' class='col-md-3 col-form-label'>Pilihan Jawaban<small class='text-danger'>*</small></label><div class='col-md-9'><div class='row'><div class='col-md-10'><input type='text' name='id_likert[]' value='' class='form-control d-none'><input type='text' class='form-control' id='input_linier' name='input_linier[]' value='' multiple></div><div class='col-md-2'><button class='btn btn-danger' onclick='hapusLinier(" + totalL + ")'>Hapus</button></div></div></div>");
        totalL += 1;
        $('[name=totalL]').val(totalL);
    });


    $('#btn-simpan-kuisioner').click(function(e) {
        var input_kategori = $("#input_kategori").val();
        var input_kode = $("#input_kode").val();
        var input_pertanyaan = $("#input_pertanyaan").val();
        var input_jenis = $("#input_jenis").val();

        const isFilled = (input_kategori != "" && input_kode != "" && input_pertanyaan != "" && input_jenis != "");
        e.preventDefault();

        if (isFilled) {
            $.ajax({
                type: 'ajax',
                method: 'post',
                url: site_url + 'kuesioner_mitra/index/updateKuesioner',
                data: new FormData($('#myformkuisioner')[0]),
                async: false,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(response) {
                    if (response.success) {
                        Swal.fire('Proses Berhasil!', response.message, 'success').then(function() {
                            window.location.href = "<?php echo base_url('kuesioner_mitra/index/') ?>";
                        })
                    } else {
                        Swal.fire('Proses Gagal!', response.message, 'error');
                    }
                },
                error: function(xmlresponse) {
                    console.log(xmlresponse);
                }
            })
        } else {
            Swal.fire({
                confirmButtonColor: '#3ab50d',
                icon: 'error',
                title: 'Peringatan',
                text: 'Isian bertanda bintang wajib diisi',
            })
        }
    })

    function hapusPilgan(id) {
        let hapus = 'pilgan' + id.toString()
        console.log(hapus)
        total = total - 1
        $('[name=total]').val(total);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function hapusKokcen(id) {
        let hapus = 'kokcen' + id.toString()
        console.log(hapus)
        totalU = totalU - 1
        $('[name=totalU]').val(totalU);
        const element = document.getElementById(hapus);
        element.remove();
    }

    function hapusLinier(id) {
        let hapus = 'linier' + id.toString()
        console.log(hapus)
        totalL = totalL - 1
        $('[name=totalL]').val(totalL);
        const element = document.getElementById(hapus);
        element.remove();
    }


    function addpilgan(id) {
        var x = document.getElementById("add_pilgan" + id.toString()).checked;
        console.log(x);
        var isian = ".isian_pilgan" + id.toString()
        if (x) {

            $(isian).show(300);
        } else {
            $(isian).hide(200);
        }
    }
</script>