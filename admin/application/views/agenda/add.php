<div class="row">
	<div class="col-xs-12 col-lg-12">
		<form id="form_agenda">
			<div class="card">
				<div class="top-border"></div>
				<div class="card-body">
					<div class="form-group row">
						<label for="nama" class="col-md-3 col-form-label">Nama <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="input" name="nama" value="" maxlength="100" placeholder="Masukkan Nama" onpaste="return false">
							<div class="invalid-feedback" for="nama"></div>
						</div>
					</div>
					<div class="form-group row">
						<label for="penyelenggara" class="col-md-3 col-form-label">Penyelenggara <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="input" name="penyelenggara" value="" maxlength="100" placeholder="Masukkan Penyelenggara Acara" onpaste="return false">
							<div class="invalid-feedback" for="penyelenggara"></div>
						</div>
					</div>
					<div class="form-group row">
						<label for="lokasi" class="col-md-3 col-form-label">Lokasi <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="input" name="lokasi" value="" maxlength="250" placeholder="Masukkan Lokasi Acara" onpaste="return false">
							<div class="invalid-feedback" for="lokasi"></div>
						</div>
					</div>
					<div class="form-group row">
						<label for="waktu_mulai" class="col-md-3 col-form-label">Tanggal Mulai <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<input type="text" class="form-control tanggal-mulai" id="tanggal-mulai" name="waktu_mulai" value="" onkeydown="return false" placeholder="Masukkan Tanggal Mulai">
							<div class="invalid-feedback" for="waktu_mulai"></div>
						</div>
					</div>
					<div class="form-group row">
						<label for="waktu_selesai" class="col-md-3 col-form-label">Tanggal Selesai <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<input type="text" class="form-control tanggal-selesai" id="tanggal-selesai" name="waktu_selesai" value="" onkeydown="return false" placeholder="Masukkan Tanggal Selesai">
							<div class="invalid-feedback" for="waktu_selesai"></div>
							<input type="hidden" name="cek_tanggal">
							<div class="invalid-feedback" for="cek_tanggal"></div>
						</div>
					</div>
					<div class="form-group row">
						<label for="time" class="col-md-3 col-form-label">Waktu Acara <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-3 mr-4">
									Waktu Mulai
									<input type="time" class="form-control" name="waktu_acara"  />
									<div class="invalid-feedback" for="waktu_acara"></div>
								</div>
								<div class="col-md-3">
									Waktu Berakhir
									<input type="time" class="form-control" name="waktu_berakhir"  />
									<div class="invalid-feedback" for="waktu_berakhir"></div>
								</div>
							</div>
							<input type="hidden" name="cek_waktu">
							<div class="invalid-feedback" for="cek_waktu"></div>
						</div>
					</div>
					<div class="form-group row">
						<label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<textarea class="form-control" id="deskripsi" name="deskripsi" value="" maxlength="60"></textarea>
							<div class="invalid-feedback" for="deskripsi"></div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-3">
							<label for="flyer">Flyer <b class="text-danger">*</b></label>
						</div>
						<div id="new-upload-logo-perguruan" class="col-md-9">
							<label for="input_file" class="btn btn-primary">Pilih Berkas</label>
							<input type="file" accept=".png, .jpg, .jpeg " name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo-perguruan')">
							<span class="file-info-logo-perguruan text-muted">Tidak ada berkas yang dipilih</span>
							<div id="preview-logo-perguruan">

							</div>
							<div class="hint-block text-muted mt-3">
								<small>
									Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
									Ukuran file maksimal: <strong>2 MB</strong>
								</small>
							</div>
							<div class="invalid-feedback" for="url_photo"></div>
							<small class="text-danger">Gambar maksimal berukuran  2048x2048px  </small>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<a href="<?php echo base_url('agenda/index/') ?>" class="btn btn-theme-dark">Kembali</a>
					<button type="submit" class="btn btn-theme-danger" id="btn-simpan-agenda">Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>

<?php $this->load->view("template/template_scripts") ?>

<style>
    .image-berkas {
        width: 150px;
        height: 200px;
        display: flex;
        border-radius: 10px;
        object-fit: scale-down;
        border: 1px solid lightgrey;
    }

</style>

<script>
	ClassicEditor
		.create(document.querySelector('#deskripsi'))
		.then(editor => {
			console.log(editor);
		})
		.catch(error => {
			console.error(error);
		});
</script>

<script>
	$('#form_agenda').on('submit', function(e) {
		e.preventDefault();

		$('#form_agenda').find('input, select, textarea').removeClass('is-invalid');
		$('input').parent().removeClass('is-invalid');
		$.ajax({
			url: site_url + 'agenda/Index/simpanAgenda',
			data: new FormData(this),
			dataType: 'json',
			type: 'POST',
			contentType: false,
			processData: false,
			beforeSend: function() {
				showLoading();
			},
			success: function(data) {
				hideLoading();
				swal.fire({
					html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil menambahkan Agenda',
					title: data.message.title,
					text: data.message.body,
					confirmButtonColor: '#BD1F28',
					confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
					showClass: {
						popup: 'animate__animated animate__fadeInDown'
					},
					hideClass: {
						popup: 'animate__animated animate__fadeOutUp'
					}
				}).then(function() {
					window.location.href = site_url + 'agenda/index';
				});
			},
			error: function(jqXHR, textStatus, errorThrown) {
				hideLoading();
				let response = jqXHR.responseJSON;

				switch (jqXHR.status) {
					case 400:
						// Keadaan saat validasi
						if (Array.isArray(response.data)) {
							response.data.forEach(function({
								field,
								message
							}, index) {
								$(`[name="${field}"]`).addClass('is-invalid');
								$(`[name="${field}"]`).parent().addClass('is-invalid');
								$(`.invalid-feedback[for="${field}"]`).html(message);
							});

							// sengaja diberi timeout,
							// karena kalau tidak, saat focus ke field error,
							// akan kembali lagi men-scroll ke tempat scroll sebelumnya
							setTimeout(function() {
								if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
									$(`[name="${response.data[0]['field']}"]`).focus();
								} else {
									$(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
								};
							}, 650);
						};
						break;
					case 500:
						swal.fire({
							title: response.message.title,
							html: response.message.body,
							icon: 'error',
							confirmButtonColor: '#396113',
						})
						break;
					default:
						break;
				};
			}
		});
	})
	$(".tanggal-mulai").datepicker({
		startDate: new Date(),
		format: 'yyyy-mm-dd',
		language: 'id',
		daysOfWeekHighlighted: "0",
		autoclose: true,
		todayHighlight: true
	});

	const today = new Date();
	const tomorrow = new Date(today);
	tomorrow.setDate(tomorrow.getDate() + 1);

	$(".tanggal-selesai").datepicker({
		startDate: today,
		// datesDisabled: new Date(),
		format: 'yyyy-mm-dd',
		language: 'id',
		daysOfWeekHighlighted: "0",
		autoclose: true,
		// todayHighlight: true

	});
</script>