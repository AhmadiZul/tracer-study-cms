<!-- Map area -->
<div class="card border-0 bg-white-9 rounded-xl p-0 mb-3">
    <div class="card-body">
        <?php echo $output->output; ?>
    </div>
</div>
<br>
</div>
<!-- container -->
<!-- append theme customizer -->
<?php $this->load->view('template/template_scripts') ?>
<?php
if (isset($output->js_files)) {
    foreach ($output->js_files as $file) {
        echo '<script src="' . $file . '"></script>';
    }
}
?>

<script>
    $(document).ready(function() {
        var baseUrl = "<?php echo base_url() ?>"
        window.addEventListener('gcrud.datagrid.ready', () => {
            $(".gc-header-tools").append(
                `<div class="floatL">
            <button id="btn-unduh" class="btn btn-default btn-outline-dark t5">
              <i class="fa fa-plus floatL t3"></i>
              <span class="hidden-xs floatL l5">Tambah Agenda</span>
              <div class="clear">
              </div>
            </button>
          </div>`
            );
            $("#btn-unduh").click(function() {
            window.location.href =  baseUrl + "agenda/index/tambahAgenda";
        });
        });
    });

    
</script>