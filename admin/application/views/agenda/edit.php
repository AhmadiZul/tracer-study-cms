<div class="row">
    <div class="col-xs-12 col-lg-12">
        <form id="form_edit">
            <div class="card">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="nama" class="col-md-3 col-form-label">Nama <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input" name="nama" value="<?= $agenda->nama; ?>" placeholder="Masukkan Nama " onpaste="return false" maxlength="100">
                            <div class="invalid-feedback" for="nama"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="penyelenggara" class="col-md-3 col-form-label">Penyelenggara <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input" name="penyelenggara" value="<?= $agenda->penyelenggara; ?>" placeholder="Masukkan Penyelenggara Acara" onpaste="return false" maxlength="100">
                            <div class="invalid-feedback" for="penyelenggara"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lokasi" class="col-md-3 col-form-label">Lokasi <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="input" name="lokasi" value="<?= $agenda->lokasi; ?>" placeholder="Masukkan Lokasi Acara" onpaste="return false" maxlength="250">
                            <div class="invalid-feedback" for="lokasi"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="waktu_mulai" class="col-md-3 col-form-label">Tanggal mulai <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control tanggal-mulai" id="tanggal-mulai" name="waktu_mulai" value="<?= $agenda->waktu_mulai; ?>" onkeydown="return false">
                            <div class="invalid-feedback" for="waktu_mulai"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="waktu_selesai" class="col-md-3 col-form-label">Tanggal selesai <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control tanggal-selesai" id="tanggal-selesai" name="waktu_selesai" value="<?= $agenda->waktu_selesai; ?>" onkeydown="return false">
                            <div class="invalid-feedback" for="waktu_selesai"></div>
                            <input type="hidden" name="cek_tanggal">
                            <div class="invalid-feedback" for="cek_tanggal"></div>
                        </div>
                    </div>
                    <div class="form-group row">
						<label for="time" class="col-md-3 col-form-label">Waktu Acara <b class="text-danger">*</b></label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-3 mr-4">
									Waktu Mulai
									<input type="time" class="form-control" name="waktu_acara" value = "<?= $agenda->waktu_acara ?>" />
								</div>
								<div class="col-md-3">
									Waktu Berakhir
									<input type="time" class="form-control" name="waktu_berakhir" value = "<?= $agenda->waktu_berakhir ?>" />
								</div>
							</div>
							<input type="hidden" name="cek_waktu">
							<div class="invalid-feedback" for="cek_waktu"></div>
						</div>
					</div>
                    <div class="form-group row">
                        <label for="deskripsi" class="col-md-3 col-form-label">Deskripsi <b class="text-danger">*</b></label>
                        <div class="col-md-9">
                            <textarea class="form-control alpha-only" id="deskripsi" name="deskripsi" maxlength="250"><?= $agenda->deskripsi; ?> </textarea>
                            <div class="invalid-feedback" for="deskripsi"></div>
                        </div>
                        <input type="hidden" name="id" value="<?= $agenda->id ?>">
                        <input type="hidden" name="oldFoto" value="<?= $agenda->flyer ?>">
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label for="url_photo">Flyer <b class="text-danger">*</b></label>
                        </div>
                        <div id="ganti-berkas-logo-perguruan" class="col-md-9 d-inline">
                            <input type="hidden" name="id" value="<?= $agenda->id ?>">
                            <img src="<?= base_url() . $agenda->flyer ?>" alt="your image" style="width: 150px;" /><br>
                            <label class="form-label"><input name="new_upload_logo-perguruan" type="checkbox" onclick="uploadFoto('logo-perguruan','url_photo')" value="1"> Ganti Berkas</label>
                        </div>
                        <div class="col-md-3"></div>
                        <div id="new-upload-logo-perguruan" class="col-md-9 d-none">
                            <label for="input_file" class="btn btn-primary text-white">Pilih Berkas</label>
                            <input type="file" accept=".png, .jpg, .jpeg " name="url_photo" id="input_file" class="form-control custom-file" onchange="cekFile(this, 'logo-perguruan')">
                            <span class="file-info-logo-perguruan text-muted">Tidak ada berkas yang dipilih</span>
                            <div id="preview-logo-perguruan">

                            </div>
                            <div class="hint-block text-muted mt-3">
                                <small>
                                    Jenis file yang diijinkan: <strong>PNG, JPEG, JPG</strong><br>
                                    Ukuran file maksimal: <strong>2 MB</strong>
                                </small>
                            </div>
                            <div class="invalid-feedback" for="flyer"></div>
                            <small class="text-danger">Gambar maksimal berukuran  2048x2048px  </small>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <a href="<?php echo base_url('agenda/index/') ?>" class="btn btn-theme-dark">Kembali</a>
                    <button type="submit" class="btn btn-theme-danger" id="btn-simpan-alumni">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $this->load->view("template/template_scripts") ?>


<style>
    .image-berkas {
        width: 150px;
        height: 200px;
        display: flex;
        border-radius: 10px;
        object-fit: scale-down;
        border: 1px solid lightgrey;
    }

</style>

<script>
    ClassicEditor
        .create(document.querySelector('#deskripsi'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });
</script>

<script>
    $('#form_edit').on('submit', function(e) {
        e.preventDefault();

        $('#form_edit').find('input, select, textarea').removeClass('is-invalid');
        $('input').parent().removeClass('is-invalid');
        $.ajax({
            url: site_url + 'agenda/Index/editAgenda',
            data: new FormData(this),
            dataType: 'json',
            type: 'POST',
            contentType: false,
            processData: false,
            beforeSend: function() {
                showLoading();
            },
            success: function(data) {
                hideLoading();
                swal.fire({
                    html: '<img src="<?php echo base_url() . 'public/assets/img/SweetAlert/berhasil.png' ?>" alt="..." style="padding-top: -220px;"><br>Berhasil mengedit Agenda',
                    title: data.message.title,
                    text: data.message.body,
                    confirmButtonColor: '#BD1F28',
                    confirmButtonText: '<i class="fa fa-thumbs-up"></i> Ok!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    }
                }).then(function() {
                    window.location.href = site_url + 'agenda/index';
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                hideLoading();
                let response = jqXHR.responseJSON;

                switch (jqXHR.status) {
                    case 400:
                        // Keadaan saat validasi
                        if (Array.isArray(response.data)) {
                            response.data.forEach(function({
                                field,
                                message
                            }, index) {
                                $(`[name="${field}"]`).addClass('is-invalid');
                                $(`[name="${field}"]`).parent().addClass('is-invalid');
                                $(`.invalid-feedback[for="${field}"]`).html(message);
                            });

                            // sengaja diberi timeout,
                            // karena kalau tidak, saat focus ke field error,
                            // akan kembali lagi men-scroll ke tempat scroll sebelumnya
                            setTimeout(function() {
                                if ($(`[name="${response.data[0]['field']}"]`).is(':visible')) {
                                    $(`[name="${response.data[0]['field']}"]`).focus();
                                } else {
                                    $(window).scrollTop($(`[name="${response.data[0]['field']}"]`).parent().offset().top);
                                };
                            }, 650);
                        };
                        break;
                    case 500:
                        swal.fire({
                            title: response.message.title,
                            html: response.message.body,
                            icon: 'error',
                            confirmButtonColor: '#396113',
                        })
                        break;
                    default:
                        break;
                };
            }
        });
    })

    $(".tanggal-mulai").datepicker({
		startDate: new Date(),
		format: 'yyyy-mm-dd',
		language: 'id',
		daysOfWeekHighlighted: "0",
		autoclose: true,
		todayHighlight: true
	});

	const today = new Date();
	const tomorrow = new Date(today);
	tomorrow.setDate(tomorrow.getDate() + 1);

	$(".tanggal-selesai").datepicker({
		startDate: today,
		// datesDisabled: new Date(),
		format: 'yyyy-mm-dd',
		language: 'id',
		daysOfWeekHighlighted: "0",
		autoclose: true,
		// todayHighlight: true

	});
</script>