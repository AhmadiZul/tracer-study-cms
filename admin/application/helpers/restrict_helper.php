<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	function is_logged_in(){
		$ci = &get_instance();

		$modul = strtolower($ci->uri->segment(1));

		if(!$ci->session->userdata('bgskin_userId')){
			if($modul != 'login' && $modul != ''){
				redirect(site_url());
			}
		}else{
			$id_user = $ci->session->userdata('bgskin_userId');
			$id_group = $ci->session->userdata('bgskin_idGroup');
			$modul = strtolower($ci->uri->segment(1));
			if($modul != "index" && ($modul != '' || $modul != null)){
				$query = $ci->db->get_where('akses_group_modul', ['id_group'=>$id_group, 'nama_modul'=>$modul])->row_array();
				if(empty($query) || $query['hak_akses'] != "access"){
					$message = "Maaf, Anda tidak memiliki akses ke halaman tersebut.";
					$ci->session->set_flashdata('false', $message);
					redirect("index");
				}
			}
		}
	}

	function is_status_member_active(){
		$ci = &get_instance();
		if(!empty($ci->session->userdata('bgskin_userId')) && $ci->session->userdata('bgskin_idGroup') != 1){
			$id_user = $ci->session->userdata('bgskin_userId');
			$modul = strtolower($ci->uri->segment(1));
			$member = $ci->db->get_where('members', ['id_user'=> $id_user])->row_array();
			if($modul != 'index' && $member['status'] != '1'){
				$message = "Maaf, Silahkan lengkapi biodata anda.";
				$ci->session->set_flashdata('not_active', $message);
				redirect("index");
			}

		}

	}
 ?>