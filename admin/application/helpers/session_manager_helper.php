<?php
if (!function_exists('getLoginSession')) {
    /**
     * getLoginSession
     * function to get all session data
     *  [tracer_userId]
     *  [tracer_username]
     *  [tracer_nama]
     *  [tracer_email]
     *  [tracer_idGroup]
     *  [tracer_namaGroup]
     *  [tracer_isActive]
     *  [tracer_dbUsername]
     *  [tracer_tahun]
     *  [tracer_urlLogo]
     *  [tracer_idPerguruanTinggi]
     * @return array
     */
    function getLoginSession()
    {
        $ci =& get_instance();

        $result = $ci->session->all_userdata();
        return $result;
    }

    if (!function_exists('getSessionRoleAccess')) {
        /**
         * getSessionRoleAccess
         * function to get role access
         * @return void
         */
        function getSessionRoleAccess()
        {
            $result = getLoginSession();

            return $result['tracer_idGroup'];
        }
    }

    if (!function_exists('getSessionID')) {
        function getSessionID()
        {
            $result = getLoginSession();
            return $result['tracer_userId'];
        }
    }

    if (!function_exists('getPTSessionID')) {
        function getPTSessionID()
        {
            $result = getLoginSession();
            return $result['tracer_idPerguruanTinggi'];
        }
    }
}

function getSession($name)
{
    $ci =& get_instance();


    return $ci->session->userdata($name);
}
 function setSession($name, $value)
 {

    $ci =& get_instance();

    $ci->session->set_userdata($name, $value);
 }
?>