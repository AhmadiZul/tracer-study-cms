<?php

function setPronvinsi()
{
    $return = [];
    $ci =& get_instance();
    $get = $ci->db->query("SELECT * FROM ref_provinsi order by nama_provinsi");
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['kode_provinsi']] = $rows['nama_provinsi'];
        }
    }
    return $return;

}
function setKabupaten($kode_prov = null)
{
    $return = [];
    $ci =& get_instance();

    if ($kode_prov) {
        $ci->db->where('kode_provinsi',$kode_prov);
    }
    
    $ci->db->order_by('nama_kab_kota');
    $get = $ci->db->get("ref_kab_kota");
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['kode_kab_kota']] = $rows['nama_kab_kota'];
        }
    }
    return $return;

}
function setKecamatan($kode_kab_kota = null)
{
    $return = [];
    $ci =& get_instance();

    if ($kode_kab_kota) {
        $ci->db->where('kode_kab_kota',$kode_kab_kota);
    }
    $ci->db->order_by('nama_kecamatan');

    $get = $ci->db->get("ref_kecamatan");
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['kode_kecamatan']] = $rows['nama_kecamatan'];
        }
    }
    return $return;

}
?>