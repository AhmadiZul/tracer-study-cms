<?php

function setJenjangPendidikan()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_perguruan_tinggi');
    $ci->db->order_by('npsn');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_perguruan_tinggi']] = $rows['nama_resmi'];
        }
    }
    return $return;

}

function setProdi($id_fakultas)
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_prodi');
    if ($id_fakultas != null) {
        $ci->db->where('id_fakultas', $id_fakultas);
    }
    $ci->db->order_by('nama_prodi');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_prodi']] = $rows['nama_prodi'];
        }
    }
    return $return;

}

function setTahun()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_tahun');
    $ci->db->where('is_active', '1');
    $ci->db->order_by('tahun','DESC');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['tahun']] = $rows['tahun'];
        }
    }
    return $return;

}

function setStatusAlumni()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_status_alumni');
    $ci->db->order_by('id');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id']] = $rows['status_alumni'];
        }
    }
    return $return;

}
function setAgama()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_agama');
    $ci->db->order_by('id_agama');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_agama']] = $rows['nama_agama'];
        }
    }
    return $return;

}

function setAlumni($user_id = null)
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('alumni a');

    if (!empty($user_id)) {
        $ci->db->join('ref_perguruan_tinggi rpt','a.id_perguruan_tinggi=rpt.id_perguruan_tinggi','LEFT');
        $ci->db->where('rpt.id_user',$user_id);
    }

    $ci->db->order_by('nama');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_alumni']] = $rows['nim'].' - '.$rows['nama'];
        }
    }
    return $return;

}

function setMitra()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('mitra m');

    $ci->db->order_by('nama');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_instansi']] = $rows['nama'].' - '.$rows['sektor'];
        }
    }
    return $return;

}

function setPendidikan()
{
    $return = [];
    $ci =& get_instance();
    $ci->db->from('ref_pendidikan rp');

    $ci->db->where('rp.is_active','1');
    $get = $ci->db->get();
    if ($get->num_rows()) {
        foreach ($get->result_array() as $key => $rows) {
            $return[$rows['id_pendidikan']] = $rows['nama_pendidikan'];
        }
    }
    return $return;

}
?>