<?php

// PWMP API

class APIHELPER
{

    public static function role_code($data) {
        $result = null;
        switch(true) {
            case ($data['id'] === 1 || strpos(strtolower($data['nama_role']), 'admin pusat') !== false):
                $result = 'ADM1';
                break;
            case ($data['id'] === 2 || strpos(strtolower($data['nama_role']), 'admin korwil') !== false):
                $result = 'ADM2';
                break;
            case ($data['id'] === 3 || strpos(strtolower($data['nama_role']), 'admin pelaksana') !== false):
                $result = 'ADM3';
                break;
            case ($data['id'] === 4 || strpos(strtolower($data['nama_role']), 'pembimbing') !== false):
                $result = 'PMB';
                break;
            case ($data['id'] === 5 || strpos(strtolower($data['nama_role']), 'mentor') !== false):
                $result = 'MTR';
                break;
            case ($data['id'] === 6 || strpos(strtolower($data['nama_role']), 'peserta pwmp') !== false):
                $result = 'PST';
                break;
            default: // Pendaftar PWMP
                $result = 'PDF';
                break;
        }
        return $result;
    }

}
?>