<?php

$ci = &get_instance();

function randomPassword()
{
  $alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890';
  $pass = array();
  $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
  for ($i = 0; $i < 8; $i++) {
    $n = rand(0, $alphaLength);
    $pass[] = $alphabet[$n];
  }
  return implode($pass); //turn the array into a string
}

function randomPassword_number()
{
  $alphabet = '1234567890';
  $pass = array(); //remember to declare $pass as an array
  $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
  for ($i = 0; $i < 8; $i++) {
    $n = rand(0, $alphaLength);
    $pass[] = $alphabet[$n];
  }
  return implode($pass); //turn the array into a string
}

function getUUID()
{
  $ci = &get_instance();
  $result = $ci->db->query("SELECT UUID()")->row_array()['UUID()'];
  return $result;
}

function isPDF($param)
{
  $file = 'gambar';
  $panjang =  strlen($param);
  if (strpos($param, '.pdf')) {
    $file = 'pdf';
  }
  return $file;
}

function tampil_sebagian($param, $panjang)
{
  //$panjang = strlen($param);
  $tampil = substr($param, 0, $panjang);
  return $tampil;
}

function tgl_indo($param)
{
  $tanggal = date('Y-m-d', strtotime($param));
  $bulan = array(
    1 =>   'Januari',
    'Februari',
    'Maret',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Agustus',
    'September',
    'Oktober',
    'November',
    'Desember'
  );
  $pecahkan = explode('-', $tanggal);

  // variabel pecahkan 0 = tahun
  // variabel pecahkan 1 = bulan
  // variabel pecahkan 2 = tanggal

  return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
}


function active_page($page, $class)
{
  $_this = &get_instance();
  if ($page == $_this->uri->segment(1)) {
    return $class;
  }
}

function active_subpage($pages)
{
  $_this = &get_instance();
  $active = '';

  if ((count($pages) == 1 && $pages[0] == $_this->uri->segment(1)) && $_this->uri->segment(2) == null) {
    $active = 'active';
  } else {
    foreach ($pages as $key => $page) {
      if ($page == $_this->uri->segment($key + 1) && count($pages) > 1) {
        $active = 'active';
      } else {
        $active = '';
      }
    }
  }

  return $active;
}

function getInitial($name)
{
  if (!isset($name)) {
    return "TS";
  }

  $str_arr = explode(' ', $name);
  $count = str_word_count($name);

  if ($count == 1) {
    return strtoupper(substr($name, 0, 2));
  } else {
    return strtoupper(substr($str_arr[0], 0, 1) . substr($str_arr[1], 0, 1));
  }
}

function getDayName($day_of_week)
{
  switch ($day_of_week) {
    case 1:
      return 'Senin';
      break;

    case 2:
      return 'Selasa';
      break;

    case 3:
      return 'Rabu';
      break;

    case 4:
      return 'Kamis';
      break;

    case 5:
      return 'Jumat';
      break;

    case 6:
      return 'Sabtu';
      break;

    case 0:
      return 'Minggu';
      break;

    default:
      return 'Senin';
      break;
  }
}

function getMonthName($month)
{
  switch ($month) {
    case 1:
      return 'Januari';
      break;

    case 2:
      return 'Februari';
      break;

    case 3:
      return 'Maret';
      break;

    case 4:
      return 'April';
      break;

    case 5:
      return 'Mei';
      break;

    case 6:
      return 'Juni';
      break;

    case 7:
      return 'Juli';
      break;

    case 8:
      return 'Agustus';
      break;

    case 9:
      return 'September';
      break;

    case 10:
      return 'Oktober';
      break;

    case 11:
      return 'November';
      break;

    case 12:
      return 'Desember';
      break;

    default:
      # code...
      break;
  }
}

function parseTanggal($date)
{
  date_default_timezone_set('Asia/Jakarta');
  $day_name = getDayName(date('w', strtotime($date)));
  $day = date('d', strtotime($date));
  $month = getMonthName(date('m', strtotime($date)));
  $year = date('Y', strtotime($date));
  return "$day_name, $day $month $year";
}

function cekParameter($arrayKey = null, $params = null)
{
  $return['status'] = true;
  $return['message'] = null;
  if (!empty($arrayKey) && !empty($params)) {
    for ($i = 0; $i < count($arrayKey); $i++) {
      if (!array_key_exists($arrayKey[$i], $params)) {
        $return['status'] = false;
        $return['message'] = 'Parameter request harus lengkap, Silahkan lihat API Documentation';
        break;
      }
    }
  }

  return $return;
}

function showTitleName()
{
  $ci =&get_instance();
  return $ci->session->userdata('tracer_nama');
}

function userValidation($params, $id="")
{
  $return['status'] = 500;
  $return['message'] = '';

  $ci =& get_instance();

  $ci->db->select("*");
  $ci->db->from("user");
  $ci->db->where($params);
  if ($id!="") {
    $ci->db->where("id_user !=",$id);
  }
  $get = $ci->db->get();
  if ($get->num_rows()!=0) {
    $return['status'] = 500;
    $return['message'] = 'Akun sudah digunakan. Silahkan cek kembali data anda';
  } else {
    $return['status'] = 201;
    $return['message'] = 'Akun bisa digunakan untuk mendaftar';
  }
  return $return;
}


