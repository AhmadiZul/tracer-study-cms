<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "berita";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_berita', 'berita');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-list-alt';
        $this->data['title'] = 'Tips Karir';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->data['footer'] = $this->sistem->footer();
        $this->data['header'] = $this->sistem->header();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->unsetAdd();
        $crud->callbackDelete(array($this,'delete_berita'));
        $crud->where(['is_active' => '1']);
        $crud->callbackColumn('url_photo', [$this, 'callPhoto']);
        $crud->unsetEdit();
        $crud->setSubject('Tips Karir');

        

        $crud->setTable('berita_crud');
        
        $crud->columns([
            'judul',
            'deskripsi',
            'url_photo',
        ]);

        $crud->displayAs([
            'judul' => 'Judul',
            'deskripsi' => 'Deskripsi',
            'url_photo' => 'Foto',
        ]);

        $crud->requiredFields([
            'judul',
            'deskripsi',
            'url_photo',
        ]);

        $crud->fieldType('id_group', 'hidden');

        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('tips_karir/Index/pageEdit?key=' . $row->id_berita);
        }, false);
        $output = $crud->render();
        $this->_setOutput('berita/index/index', $output);
    }

    // public function aktivasi($value, $row)
    // {
    //     if($row->is_active == '1')
    //     {
    //         return '<button type="button" onclick="noAktif(' . "'" . $row->deskripsi . "'" . ')" id="btn-edit-task" class="btn btn-warning btn-xs"><i class="fas fa-lock"></i> Tidak Aktif</button>';
    //     } else {
    //         $id = 1;
    //         return '<button type="button" onclick="aktif(' . "'" . $row->deskripsi . "'" . ')" id="btn-edit-task" class="btn btn-success btn-xs"><i class="fas fa-unlock"></i> Aktif</button>';
    //     } 
    // } 
    public function callPhoto($value, $rows)
    {
        $return = '-';
        if ($value!=null || $value!="") {
            if (file_exists($value)) {
                $return = '<img src="'.base_url().$value.'" class="img img-fluid" width="100px" alt="Tips Karir">';
            }
        }
        return $return;
    }

    public function pageTips()
    {
        $this->data['title'] = 'Tambah Tips Karir';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('index/add.php');
    }
    public function pageEdit()
    {
        $this->data['title'] = 'Edit Tips Karir';
        $this->data['is_home'] = false;
        $this->data['berita'] = $this->berita->getBerita(array('id_berita' => $this->input->get('key')));
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('index/edit.php');
    }

    public function tambahTips()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $data = array(
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('deskripsi'),
            'is_active' => '1',
            'last_action_user' => "CREATE",
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );
        $id_berita = $this->db->insert_id();
        // var_dump($data);
        // die;

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_berita);
            $data['url_photo'] = $urlPhoto['file_name'];
        }

        

        $this->berita->add_berita($data);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan Tips Karir',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);

       
    }

    private function _runValidation()
    {
        $errors = FALSE;
        // var_dump($this->input->post());
        // die;
        $this->form_validation->set_rules('judul', 'Judul Berita', 'trim|required|max_length[60]|min_length[3]|callback_whitespace');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }

    function _urlphoto($id_berita)
    {
        $id_berita = str_replace("-", "", $id_berita);

        if (!file_exists('public/uploads/berita/' . $id_berita )) {
            mkdir('public/uploads/berita/' . $id_berita , 0755, true);
        }

        $berita_lama = $this->berita->getBerita(array('id_berita' => $id_berita));
        $oldUrlBukti = '';
        if (!empty($berita_lama)) {
            $oldUrlBukti = $berita_lama->url_photo;
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/berita/' . $id_berita .  '/'; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'foto' . $id_berita;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name']='public/uploads/berita/'.$id_berita.'/'.$file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function delete_berita($id)
    {
        $this->db->where('id_berita', $id->primaryKeyValue);
        $berita = $this->db->get('berita_crud')->row();
        if(empty($berita))
            return false;
    
        unlink($berita->url_photo);
        $this->db->where('id_berita', $id->primaryKeyValue);
        $this->db->delete('berita_crud');
        return true;
    }
    public function edit_tips()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $id_berita = $this->input->post('id_berita');
        $data = array(
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('deskripsi'),
            'last_action_user' => "UPDATE",
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );

        if($_FILES['url_photo']['name']){
            $urlPhoto = $this->_urlphoto( $id_berita);
            $data['url_photo'] = $urlPhoto['file_name'];
        }else{
            $data['url_photo'] = $this->input->post('oldFoto');
        }

        $editBanner = $this->berita->editBerita($data, $id_berita);

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit tips karir',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
}
