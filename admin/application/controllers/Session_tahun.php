<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Session_tahun extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('tracer_userId')!="") {
            redirect(site_url('index'));
        }
    }

    public function tahun()
    {
        $tahun = $this->input->post('tahun');
        $this->session->set_userdata("tracer_tahun", $tahun);

        echo json_encode(array('success' => true, 'message' => 'Session telah diubah'));
    }

    public function mitra()
    {
        $this->data['title'] = 'mitra';
        $this->data['is_full'] = true;
        $this->renderTo('login/mitra');
    }

}
