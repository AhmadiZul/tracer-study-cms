<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "agenda";
    
    /* private $id = '';
 */
    protected $onUpdate = 0;


    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_agenda', 'agenda');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-calendar-check';
        $this->data['title'] = 'Agenda';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->unsetAdd();
        $crud->callbackDelete(array($this, 'hapus_agenda'));
        $crud->unsetEdit();
        $crud->setSubject('agenda');



        $crud->setTable('agenda');
        $crud->callbackColumn('flyer', [$this, 'callPhoto']);

        $crud->columns([
            'nama',
            'penyelenggara',
            'lokasi',
            'waktu_mulai',
            'waktu_selesai',
            'waktu_acara',
            'waktu_berakhir',
            'flyer'
        ]);

        $crud->displayAs([
            'nama' => 'Nama',
            'penyelenggara' => 'Penyelenggara',
            'lokasi' => 'Lokasi',
            'waktu_mulai' => 'Tanggal Mulai',
            'waktu_selesai' => 'Tanggal Selesai',
            'waktu_acara' => 'Waktu Mulai',
            'waktu_berakhir' => 'Waktu Selesai',
            'flyer' => 'flyer',
        ]);

        $crud->fieldType('id_group', 'hidden');

        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('agenda/index/halamanEdit?key=' . $row->id);
        }, false);
        $output = $crud->render();
        $this->_setOutput('agenda/index', $output);
    }

    public function tambahAgenda()
    {
        $this->data['title'] = 'Tambah Agenda';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('add');
    }

    public function halamanEdit()
    {
        $this->data['title'] = 'Edit Agenda';
        $this->data['is_home'] = false;
        $this->data['agenda'] = $this->agenda->getAgenda(array('id' => $this->input->get('key')));
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('edit');
    }

    public function callPhoto($value, $rows)
    {
        $return = '-';
        if ($value != null || $value != "") {
            if (file_exists($value)) {
                $return = '<img src="' . base_url() . $value . '" class="img img-fluid" width="100px" alt="url_photo">';
            }
        }
        return $return;
    }

    function _urlphoto($id)
    {
        $data_agenda = $this->agenda->getAgenda(array('id' => $id));

        if (!file_exists('public/uploads/agenda/' . $id)) {
            mkdir('public/uploads/agenda/' . $id, 0755, true);
            chmod('public/uploads/agenda/', 0755);
        }

        $oldUrlBukti = '';
        if (!empty($data_agenda)) {
            $oldUrlBukti = $data_agenda->flyer;
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/agenda/' . $id; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            // $config['max_width'] = '2048px' ;
            // $config['max_height'] = '2048px' ;                  
            $config['file_name'] = date('YmdHis') . 'LOGO' . $id;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = 'public/uploads/agenda/' . $id . '/' . $file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus dibawah 2 MB, dan ukuran tidak lebih dari ...";
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function simpanAgenda()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $id = getUUID();

        $data = array(
            'id' => $id,
            'nama' => $this->input->post('nama'),
            'penyelenggara' => $this->input->post('penyelenggara'),
            'lokasi' => $this->input->post('lokasi'),
            'waktu_mulai' => $this->input->post('waktu_mulai'),
            'waktu_selesai' => $this->input->post('waktu_selesai'),
            'waktu_acara' => $this->input->post('waktu_acara'),
            'waktu_berakhir' => $this->input->post('waktu_berakhir'),
            'deskripsi' => $this->input->post('deskripsi'),
        );
        /* $id = $this->db->insert_id(); */

        $urlPhoto = $this->_urlphoto($id);
        $data['flyer'] = $urlPhoto['file_name'];

        $this->agenda->add_agenda($data);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan agenda',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan Agenda',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    private function _runValidation()
    {
        $errors = FALSE;
        $this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[3]|max_length[100]');
        $this->form_validation->set_rules('penyelenggara', 'Penyelenggara', 'trim|required|min_length[3]|max_length[100]');
        $this->form_validation->set_rules('lokasi', 'Lokasi', 'trim|required|min_length[3]|max_length[250]');
        $this->form_validation->set_rules('waktu_mulai', 'Tanggal mulai', 'trim|required');
        $this->form_validation->set_rules('waktu_selesai', 'Tanggal selesai', 'trim|required');
        $this->form_validation->set_rules('waktu_acara', 'Waktu mulai', 'trim|required');
        $this->form_validation->set_rules('waktu_berakhir', 'Waktu selesai', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|max_length[300]');

        if ( !empty(($_POST['waktu_mulai'])) && !empty(($_POST['waktu_selesai']))) {
            $tgl_mulai = date_create($_POST['waktu_mulai']);
            $tgl_selesai = date_create($_POST['waktu_selesai']);
            $waktu_acara = $_POST['waktu_acara'];
            $waktu_berakhir = $_POST['waktu_berakhir']; 
            $diff = date_diff($tgl_mulai, $tgl_selesai);
            
            // var_dump($waktu_acara);
            // die;

            if($diff->invert){
                $errors[] = [
                    'field'   => 'cek_tanggal',
                    'message' => 'Tanggal selesai wajib setelah tanggal mulai',
                ];
            }else if($tgl_mulai==$tgl_selesai){
                if($waktu_berakhir <= $waktu_acara){
                    $errors[] = [
                        'field'   => 'cek_waktu',
                        'message' => 'Tidak bisa memilih waktu mulai melebihi waktu selesai',
                    ];
                }
            };
        }

        if(isset($_POST['oldFoto'])){
            if (empty($_FILES['url_photo']['name']) && empty(($_POST['oldFoto']))) {
                $errors[] = [
                    'field'   => 'url_photo',
                    'message' => 'Flyer tidak boleh kosong.',
                ];
            }
        }else{
            if (empty($_FILES['url_photo']['name'])) {
                $errors[] = [
                    'field'   => 'url_photo',
                    'message' => 'Flyer tidak boleh kosong.',
                ];
            }
        }

        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function editAgenda()
    {
        $htmlCodeNumber = 201;
        $response = '';
        /* $input = $this->input->post(); */

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $id = $this->input->post('id');
        $data = array(
            'nama' => $this->input->post('nama'),
            'penyelenggara' => $this->input->post('penyelenggara'),
            'lokasi' => $this->input->post('lokasi'),
            'waktu_mulai' => $this->input->post('waktu_mulai'),
            'waktu_selesai' => $this->input->post('waktu_selesai'),
            'waktu_acara' => $this->input->post('waktu_acara'),
            'waktu_berakhir' => $this->input->post('waktu_berakhir'),
            'deskripsi' => $this->input->post('deskripsi'),
        );

        // $urlLogo = $this->_urlphoto($id);
        // $data['flyer'] = $urlLogo['file_name'];

        if($_FILES['url_photo']['name']){
            $urlPhoto = $this->_urlphoto( $id);
            $data['flyer'] = $urlPhoto['file_name'];
        }else{
            $data['flyer'] = $this->input->post('oldFoto');
        }

        // var_dump($data['flyer']);
        // die;

        $this->agenda->editAgenda($data, array('id' => $id));


        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit Agenda',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function hapus_agenda($id)
    {
        $this->db->where('id', $id->primaryKeyValue);
        $agenda = $this->db->get('agenda')->row();
        if (empty($agenda))
            return false;

        unlink($agenda->flyer);
        $this->db->where('id', $id->primaryKeyValue);
        $this->db->delete('agenda');
        return true;
    }
}
