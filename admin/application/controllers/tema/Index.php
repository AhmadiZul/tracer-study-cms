<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "tema";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        /* $this->load->model('M_vidio', 'vidio'); */
    }

    public function index()
    {
        $this->data['title'] = 'Tema';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Vidio');

        $crud->setTable('vidio_landing');

        /* $crud->callbackColumn('url_vidio', [$this,'callVidio']); */
        /* $crud->callbackColumn('is_publish', [$this,'publikasi']); */

        $crud->columns([
            'judul',
            'paragraf',
            'url_vidio',
            'is_publish',
        ]);

        $crud->displayAs([
            'judul' => 'Nama Judul',
            'paragraf' => 'Keterangan',
            'url_vidio' => 'url youtube',
            'is_publish' => 'Publikasi'
        ]);

        $crud->addFields([
            'judul',
            'paragraf',
            'url_vidio',
        ]);

        $crud->editFields([
            'judul',
            'paragraf',
            'url_vidio',
        ]);

        $crud->requiredFields([
            'judul',
            'paragraf',
        ]);

       /*  $crud->setActionButton('Detail', 'fa fa-book', function ($row) {
            return site_url('vidio/Index/detail/' . $row->id_vidio);
        }, false); */



        $output = $crud->render();
        $this->_setOutput('tema/index/index', $output);
    }

}
