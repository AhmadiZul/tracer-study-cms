<?php

class My404 extends BaseController {

    protected $template = "app_back";

    public function __construct() {
        parent::__construct();
        $this->load->model('M_index', 'index');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index() {
        $this->output->set_status_header('404');
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->renderTo('index/404'); //loading in custom error view
    }

}
