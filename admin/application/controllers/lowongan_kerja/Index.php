<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "lowongan_kerja";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_lowongan_kerja','lowongan');
    }

    public function index()
    {
        $this->data['title'] = 'Lowongan Kerja';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Lowongan Kerja');
        $crud->setTable('lowongan_pekerjaan');

        $crud->setRelation('id_instansi', 'mitra', 'nama');
        $crud->setRelation('id_pendidikan', 'ref_pendidikan', 'nama_pendidikan');
        $crud->setRelation('lokasi_kerja', 'ref_kab_kota', 'nama_kab_kota');

        $crud->where([
            'lowongan_pekerjaan.start_publish >= ?' => $this->session->userdata("tracer_tahun").'-01-01',
            'lowongan_pekerjaan.start_publish <= ?' => $this->session->userdata("tracer_tahun").'-12-31',
            'lowongan_pekerjaan.is_active' => '1'
        ]);

        // Removes only the PDF button on export
        $crud->unsetExportPdf();
        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->unsetPrint();

        $column = ['id_instansi', 'jenis', 'posisi', 'gaji_rate_bawah', 'gaji_rate_atas', 'min_pengalaman', 'lokasi_kerja', 'id_pendidikan', 'persyaratan'];
        $fields = ['id_instansi', 'jenis', 'posisi', 'gaji_rate_bawah', 'gaji_rate_atas', 'min_pengalaman', 'lokasi_kerja', 'id_pendidikan', 'persyaratan'];
        $requireds = ['id_instansi', 'jenis', 'posisi', 'lokasi_kerja', 'id_pendidikan', 'persyaratan'];
        $fieldsDisplay = [
            'id_instansi' => 'Perusahaan',
            'jenis' => 'Jenis', 
            'posisi' => 'posisi', 
            'gaji_rate_bawah' => 'Minimal Gaji', 
            'gaji_rate_atas' => 'Maksimal Gaji', 
            'min_pengalaman' => 'Minimal Pengalaman', 
            'lokasi_kerja' => 'Lokasi Kerja', 
            'id_pendidikan' => 'Minimal Pendidikan', 
            'persyaratan' => 'Persyaratan',
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);

        $crud->fieldType('jenis', 'dropdown_search', [
            'PART' => 'PART TIME',
            'FULL' => 'FULL TIME'
        ]);

        $crud->editFields(['jenis_survey']);

        $crud->callbackDelete(array($this, 'delete'));
        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('lowongan_kerja/index/edit?key=' . $row->id_lowongan_pekerjaan);
        }, false);

        // $crud->setActionButton('Detail', 'fa fa-book', function ($row) {
        //     return site_url('alumni/dashboard/detail/' . $row->id_instansi);
        // }, false);

        $output = $crud->render();
        $this->_setOutput('lowongan_kerja/index', $output);
    }

    public function add()
    {
        $this->data['title'] = 'Tambah Lowongan Kerja';
        $this->data['is_home'] = false;
        $this->data['select_mitra'] = setMitra();
        $this->data['select_kabupaten'] = setKabupaten();
        $this->data['select_pendidikan'] = setPendidikan();
        $this->render('tambah');
    }

    public function edit()
    {
        $data = $this->input->get();
        if ($data['key'] == "" || empty($data['key'])) {
            redirect('lowongan_kerja/index');
        }
        $this->data['title'] = 'Edit Lowongan Kerja';
        $this->data['is_home'] = false;
        $this->data['id_lowongan'] = $id_lowongan = $data['key'];
        $this->data['lowongan'] = $lowongan = $this->lowongan->getLowongan($id_lowongan);
        $this->data['select_mitra'] = setMitra();
        $this->data['select_kabupaten'] = setKabupaten();
        $this->data['select_pendidikan'] = setPendidikan();
        $this->render('edit');
    }

    function _urlphoto($id_lowongan, $id_instansi)
    {
        $lowongan = $this->lowongan->getLowongan($id_lowongan);
        $id_mitra = $id_instansi;

        $id_lowongan = str_replace("-", "", $id_lowongan);
        $id_mitra = str_replace("-", "", $id_mitra);

        if (!file_exists('public/uploads/mitra/')) {
            mkdir('public/uploads/mitra/', 0755, true);
        }
        if (!file_exists('public/uploads/mitra/' .$id_mitra)) {
            mkdir('public/uploads/mitra/'.$id_mitra, 0755,  true);
        }
        if (!file_exists('public/uploads/mitra/' .$id_mitra.'/lowongan_kerja/')) {
            mkdir('public/uploads/mitra/'.$id_mitra.'/lowongan_kerja/', 0755,  true);
        }

        $oldUrlBukti = '';
        if (!empty($lowongan)) {
            $base_url = base_url();
            $oldUrlBukti = str_replace($base_url, '',$lowongan->url_poster);
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/mitra/'.$id_mitra.'/lowongan_kerja/'; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'poster' . $id_lowongan;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = base_url() . 'public/uploads/mitra/'.$id_mitra.'/lowongan_kerja/' . $file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function create()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_lowongan_kerja = getUUID();
        $input = $this->input->post();

        $data = array(
            'id_lowongan_pekerjaan' => $id_lowongan_kerja,
            'id_instansi' => $input['id_instansi'],
            'start_publish' => $input['start_publish'],
            'end_publish' => $input['end_publish'],
            'jenis' => $input['jenis'],
            'posisi' => $input['posisi'],
            'deskripsi' => $input['deskripsi'],
            'gaji_rate_bawah' => $input['gaji_rate_bawah'],
            'gaji_rate_atas' => $input['gaji_rate_atas'],
            'min_pengalaman' => $input['min_pengalaman'],
            'lokasi_kerja' => $input['lokasi_kerja'],
            'id_pendidikan' => $input['id_pendidikan'],
            'persyaratan' => $input['persyaratan'],
            'status' => $input['status'],
            'last_modified_by' => $this->session->userdata('tracer_userId')
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_lowongan_kerja, $input['id_instansi']);
            $data['url_poster'] = $urlPhoto['file_name'];
        }

        $simpan = $this->lowongan->simpan_lowongan($data);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan lowongan pekerjaan',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan lowongan pekerjaan',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function update()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $input = $this->input->post();
        $id_lowongan_kerja = $input['id_lowongan'];

        $data = array(
            'id_instansi' => $input['id_instansi'],
            'start_publish' => $input['start_publish'],
            'end_publish' => $input['end_publish'],
            'jenis' => $input['jenis'],
            'posisi' => $input['posisi'],
            'deskripsi' => $input['deskripsi'],
            'gaji_rate_bawah' => $input['gaji_rate_bawah'],
            'gaji_rate_atas' => $input['gaji_rate_atas'],
            'min_pengalaman' => $input['min_pengalaman'],
            'lokasi_kerja' => $input['lokasi_kerja'],
            'id_pendidikan' => $input['id_pendidikan'],
            'persyaratan' => $input['persyaratan'],
            'status' => $input['status'],
            'last_modified_by' => $this->session->userdata('tracer_userId')
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_lowongan_kerja, $input['id_instansi']);
            $data['url_poster'] = $urlPhoto['file_name'];
        }

        $ubah = $this->lowongan->ubah_lowongan($data, $id_lowongan_kerja);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan lowongan pekerjaan',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan lowongan pekerjaan',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function delete($row)
    {
        return $this->db->update('lowongan_pekerjaan', array('is_active' => '0', 'last_modified_by' => $this->session->userdata('tracer_userId')), array('id_lowongan_pekerjaan' => $row->primaryKeyValue));
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('status', 'Status', 'trim');
        $this->form_validation->set_rules('id_instansi', 'Mitra', 'trim|required');
        $this->form_validation->set_rules('start_publish', 'Tanggal Mulai Publish', 'trim|required|callback_validDate');
        $this->form_validation->set_rules('end_publish', 'Tanggal Selesai Publish', 'trim|required|callback_cantLessDate[start_publish]|callback_validDate');
        $this->form_validation->set_rules('jenis', 'Jenis', 'trim|required');
        $this->form_validation->set_rules('posisi', 'Posisi', 'trim|required|callback_whitespace|callback_notOnlyNumber|max_length[100]');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|callback_whitespace|callback_notOnlyNumber');
        $this->form_validation->set_rules('gaji_rate_bawah', 'Rentang Gaji Atas', 'trim');
        $this->form_validation->set_rules('gaji_rate_atas', 'Rentang Gaji Bawah', 'trim');
        $this->form_validation->set_rules('min_pengalaman', 'Minimal Pengalaman', 'trim');
        $this->form_validation->set_rules('lokasi_kerja', 'Lokasi Kerja', 'trim|required');
        $this->form_validation->set_rules('id_pendidikan', 'Minimal Pendidikan', 'trim|required');
        $this->form_validation->set_rules('persyaratan', 'Persyaratan', 'trim|required|callback_whitespace|callback_notOnlyNumber');
        $this->form_validation->set_rules('url_photo', 'Upload Poster', 'trim');

        if ($this->input->post('jenis') == null) {
            $errors[] = [
                'field'   => 'jenis',
                'message' => 'Jenis belum dipilih ',
            ];
        }

        if ($this->input->post('id_pendidikan') == null) {
            $errors[] = [
                'field'   => 'id_pendidikan',
                'message' => 'Minimal Pendidikan belum dipilih ',
            ];
        }

        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');
        $this->form_validation->set_message('cantLessDate', '{field} tidak boleh kurang dari Tanggal Mulai Publish');
        $this->form_validation->set_message('validDate', '{field} format tanggal belum seusai. Format : yyyy-mm-dd');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }
    public function notOnlyNumber($str)
    {
        if (ctype_digit($str)) {
            return false;
        }
        return true;
    }

    public function cantLessDate($str, $start)
    {
        if ($str < $this->input->post($start)) {
            return false;
        }
        return true;
    }

    function validDate($date)
    {
        $date = DateTime::createFromFormat('Y-m-d', $date);
        $date_errors = DateTime::getLastErrors();
        if ($date_errors['warning_count'] + $date_errors['error_count'] > 0) {
            return false;
        }

        return true;
    }

}
