<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "laporan";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_laporan', 'laporan');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-list-alt';
        $this->data['title'] = 'Laporan';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->unsetAdd();
        $crud->callbackDelete(array($this,'delete_laporan'));
        $crud->where(['is_active' => '1']);
        $crud->unsetEdit();
        $crud->setSubject('Laporan');

        

        $crud->setTable('laporan');
        
        $crud->columns([
            'judul',
            'deskripsi',
            'url_file',
            'tahun',
        ]);

        $crud->displayAs([
            'judul' => 'Judul',
            'deskripsi' => 'Deskripsi',
            'url_file' => 'File',
            'tahun' => 'Tahun'
        ]);

        $crud->requiredFields([
            'judul',
            'deskripsi',
            'url_file',
            'Tahun'
        ]);

        $crud->fieldType('id_group', 'hidden');

        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('laporan/index/pageEdit?key=' . $row->id_laporan);
        }, false);
        $output = $crud->render();
        $this->_setOutput('laporan/index/index', $output);
    }

    // public function aktivasi($value, $row)
    // {
    //     if($row->is_active == '1')
    //     {
    //         return '<button type="button" onclick="noAktif(' . "'" . $row->deskripsi . "'" . ')" id="btn-edit-task" class="btn btn-warning btn-xs"><i class="fas fa-lock"></i> Tidak Aktif</button>';
    //     } else {
    //         $id = 1;
    //         return '<button type="button" onclick="aktif(' . "'" . $row->deskripsi . "'" . ')" id="btn-edit-task" class="btn btn-success btn-xs"><i class="fas fa-unlock"></i> Aktif</button>';
    //     } 
    // } 
    function _urlphoto($id_laporan = NULL)
    {
        // var_dump($id_laporan);
        // die;

        
        if (!file_exists('public/uploads/laporan/' )) {
            mkdir('public/uploads/laporan/' , 0755, true);
        }
        
        // var_dump($data_laporan);
        // die;
        
        $data_laporan = $this->laporan->getLaporan(array('id_laporan' => $id_laporan));
        $oldUrlBukti = '';
        if (!empty($data_laporan)) {
            $oldUrlBukti = $data_laporan->url_file;
        }

        $name_input = 'url_file';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/laporan/'; //path folder file upload
            $config['allowed_types'] = 'pdf'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'file' ;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name']='public/uploads/laporan/'.$file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function pageLaporan()
    {
        $this->data['title'] = 'Tambah Laporan';
        $this->data['is_home'] = false;
        $this->data['back'] = '/Laporan';
        $this->data['url_back'] = 'laporan/index/index';
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('index/add.php');
    }
    public function pageEdit()
    {
        $this->data['title'] = 'Edit Laporan';
        $this->data['is_home'] = false;
        $this->data['back'] = '/Laporan';
        $this->data['url_back'] = 'laporan/index/index';
        $this->data['laporan'] = $this->laporan->getLaporan(array('id_laporan' => $this->input->get('key')));
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('index/edit.php');
    }

    public function tambah_laporan()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();
        
        $dbError[] = $this->db->error();

        $data = array(
            'judul' => $this->input->post('judul'),
            'tahun' => $this->input->post('tahun'),
            'deskripsi' => $this->input->post('deskripsi'),
            'is_active' => '1',
            'last_action_user' => "CREATE",
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );
        if (isset($_FILES['url_file']['name']) && $_FILES['url_file']['name'] != "") {
            $urlPhoto = $this->_urlphoto();
            $data['url_file'] = $urlPhoto['file_name'];
        }

        $this->laporan->add_laporan($data);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan laporan',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan laporan',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);

       
    }

    private function _runValidation()
    {
        $errors = FALSE;
        // var_dump($this->input->post());
        // die;
        $this->form_validation->set_rules('judul', 'Judul Laporan', 'trim|required|max_length[60]|min_length[3]|callback_whitespace');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|max_length[4]|min_length[4]');

        if (empty($_FILES['url_file']['name']) && empty(($_POST['oldFoto']))) {
            $errors[] = [
                'field'   => 'url_file',
                'message' => 'Upload File tidak boleh kosong.',
            ];
        }

        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }

    public function delete_laporan($id)
    {
        $this->db->where('id_laporan', $id->primaryKeyValue);
        $laporan = $this->db->get('laporan')->row();
        if(empty($laporan))
            return false;
        $this->db->where('id_laporan', $id->primaryKeyValue);
        $this->db->delete('laporan');
        return true;
    }
    public function edit_laporan()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $id_laporan = $this->input->post('id_laporan');
        $data = array(
            'judul' => $this->input->post('judul'),
            'deskripsi' => $this->input->post('deskripsi'),
            'tahun' => $this->input->post('tahun'),
            'last_action_user' => "UPDATE",
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );
        if (!empty($_FILES['url_file']['name'])) {
            $urlLogo = $this->_urlphoto($id_laporan);
            $data['url_file'] = $urlLogo['file_name'];
        }

        $editBanner = $this->laporan->editLaporan($data, $id_laporan);

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan laporan',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit laporan',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
}
