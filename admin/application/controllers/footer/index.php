<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "footer";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_footer', 'footer');
    }

    public function index()
    {
        $this->data['title'] = 'Setting Footer';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->callbackDelete(array($this,'delete_footer'));
        $crud->unsetJquery();
        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->callbackColumn('url_photo', [$this, 'callPhoto']);
        $crud->setSubject('Footer');

        $crud->setTable('footer');
        $crud->columns([
            'deskripsi',
            'url_photo'
        ]);

        $crud->displayAs([
            'deskripsi' => 'Deskripsi',
            'url_photo' => 'Foto'
        ]);

        $crud->requiredFields([
            'deskripsi',
            'url_photo'
        ]);

        $crud->fieldType('is_active', 'dropdown', [
            '0' => 'Tidak Aktif',
            '1' => 'Aktif'
        ]);

        $crud->fieldType('id_group', 'hidden');

        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('footer/index/pageEdit?key=' . $row->id_footer);
        }, false);
        $output = $crud->render();
        $this->_setOutput('footer/index/index', $output);
    }

    public function add_footer()
    {
        $this->data['title'] = 'Footer';
        $this->data['is_home'] = false;
        $this->render('index/add_footer');
    }

    public function pageEdit()
    {
        $this->data['title'] = 'Edit Footer';
        $this->data['is_home'] = false;
        $this->data['footer'] = $this->footer->getFooter(array('id_footer' => $this->input->get('key')));
        $this->render('index/edit.php');
    }

    public function tambahFooter()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $data = array(
            'deskripsi' => $this->input->post('deskripsi'),
            'is_active' => '1',
            'last_action_user' => "CREATE",
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );
        $id_footer = $this->db->insert_id();
        // var_dump($data);
        // die;

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_footer);
            $data['url_photo'] = $urlPhoto['file_name'];
        }

        $this->footer->add_footer($data);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan footer',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);

       
    }

    private function _runValidation()
    {
        $errors = FALSE;
        // var_dump($this->input->post());
        // die;
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|max_length[60]|callback_whitespace');

        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }

    function _urlphoto($id_footer)
    {
        $id_footer = str_replace("-", "", $id_footer);

        if (!file_exists('public/uploads/logo/' . $id_footer )) {
            mkdir('public/uploads/logo/' . $id_footer , 0755, true);
        }

        $oldUrlBukti = '';
        if (!empty($id_footer)) {
            $oldUrlBukti = $id_footer->path_logo;
        }
        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/logo/' . $id_footer .  '/'; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'foto' . $id_footer;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] ='public/uploads/logo/'.$id_footer.'/'.$file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function delete_footer($id)
    {
        $this->db->where('id_footer', $id->primaryKeyValue);
        $footer = $this->db->get('footer')->row();
        if(empty($footer))
            return false;
    
        unlink($footer->url_photo);
        $this->db->where('id_footer', $id->primaryKeyValue);
        $this->db->delete('footer');
        return true;
    }
    public function callPhoto($value, $rows)
    {
        $return = '-';
        if ($value!=null || $value!="") {
            if (file_exists($value)) {
                $return = '<img src="'.base_url().$value.'" class="img img-fluid" width="100px" alt="Berita">';
            }
        }
        return $return;
    }
    public function edit_footer()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $id_footer = $this->input->post('id_footer');
        $data = array(
            'deskripsi' => $this->input->post('deskripsi'),
            'last_action_user' => "UPDATE",
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );

        if($_FILES['url_photo']['name']){
            $foto = $_FILES['url_photo']['name'];
            $urlPhoto = $this->_urlphoto( $foto, $id_footer);
            $data['url_photo'] = $urlPhoto['file_name'];
        }else{
            $data['url_photo'] = $this->input->post('oldFoto');
        }

        $footer_edit = $this->footer->editFooter($data, $id_footer);

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Footer',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit Footer',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
}