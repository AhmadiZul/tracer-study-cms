<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "alumni";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_index', 'rekap');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {


        $this->data['title'] = 'Rekap Alumni';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Rekap Data Alumni');
        $crud->setTable('alumni_total_per_prodi');

        if (getSessionRoleAccess() == '2') {
            $columns = [
                'ref_tahun',
                'id_prodi',
                'total_alumni'
            ];
            $fields = $columns;
            $crud->setRelation('id_prodi', 'ref_prodi', 'nama_prodi', ['id_perguruan_tinggi' => getPTSessionID()]);
        } else {
            $columns = [
                'ref_tahun',
                'id_fakultas',
                'id_prodi',
                'total_alumni'
            ];
            $fields = $columns;

            $crud->setRelation('id_fakultas', 'ref_fakultas', 'nama_fakultas');
            $crud->setRelation('id_prodi', 'ref_prodi', 'nama_prodi');

            $crud->setDependentRelation('id_prodi', 'id_fakultas', 'id_fakultas');
        }

        $crud->unsetExportPdf();


        $fieldsDisplay = [
            'ref_tahun'             => 'Tahun Lulus',
            'id_fakultas'   => 'Fakultas',
            'id_prodi'              => 'Prodi',
            'total_alumni'          => 'Total Lulusan',
        ];

        $crud->fieldType('ref_tahun', 'dropdown_search', setTahun());;


        $crud->columns($columns);
        $crud->fields($fields);
        $crud->displayAs($fieldsDisplay);
        $crud->unsetPrint();
        $crud->defaultOrdering('ref_tahun');

        // $crud->callbackBeforeInsert([$this, '_callbackBeforeInsert']);

        $output = $crud->render();
        $this->_setOutput('alumni/index/rekap', $output);
    }

    public function _callbackBeforeInsert($params)
    {
        $parameters['ref_tahun'] = $params->data['ref_tahun'];
        $parameters['id_prodi'] = $params->data['id_prodi'];
        if (getSessionRoleAccess() == '2') {
            $parameters['id_perguruan_tinggi'] = getPTSessionID();
        } else {
            $parameters['id_perguruan_tinggi'] = $params->data['id_perguruan_tinggi'];
        }

        $check = $this->rekap->getRekapPT($parameters);

        if ($check['status'] == 201) {
            return $params;
        } else {
            $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
            return $errorMessage->setMessage($check['message']);
        }
    }
}
