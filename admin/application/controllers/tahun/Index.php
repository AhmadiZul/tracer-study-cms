<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "tahun";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_tahun', 'tahun');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fa fa-calendar-days';
        $this->data['title'] = 'Tahun';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Tahun');
        $crud->setTable('ref_tahun');
        $column = ['tahun', 'is_active'];
        $fields = ['tahun'];
        $fieldsDisplay = [
            'tahun' => 'Tahun',
            'is_active' => 'Aktivasi'
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->displayAs($fieldsDisplay);
        $crud->fieldType('tahun', 'int');

        $crud->callbackColumn('is_active', [$this, 'aktivasi']);

        $crud->callbackBeforeInsert([$this, '_callBeforeInsert']);
        $crud->callbackBeforeUpdate([$this, '_callBeforeUpdate']);
        $crud->callbackDelete(array($this, '_callDelete'));

        $crud->editFields(['tahun']);

        $output = $crud->render();
        $this->_setOutput('tahun/index', $output);
    }
    
    /**
     * _callBeforeInsert
     * mengecek input sebelum di insert
     * @param  mixed $params
     * @return void
     */
    public function _callBeforeInsert($params)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $cek = $this->tahun->checkTahun($params->data);
        if ($cek['status'] == 500) {
            return $errorMessage->setMessage($cek['message']);
        }
        return $params;
    }
    
    /**
     * _callBeforeUpdate
     * mengecek input sebelum diupdate
     * @param  mixed $params
     * @return void
     */
    public function _callBeforeUpdate($params)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $cek = $this->tahun->checkTahunEdit($params->data);
        if ($cek['status'] == 500) {
            return $errorMessage->setMessage($cek['message']);
        }
        return $params;
    }
    
    /**
     * _callDelete
     * mengecek tahun yang akan dihapus (*apakah tahun digunakan di modul lain?)
     * @param  mixed $id
     * @return void
     */
    public function _callDelete($id)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $tahun = $id->primaryKeyValue;

        $cekAlumni = $this->tahun->checkAlumni($tahun);
        $cekSurvey = $this->tahun->checkSurvey($tahun);

        if ($cekAlumni['status'] == 500) {
            return $errorMessage->setMessage($cekAlumni['message']);
        }

        if ($cekSurvey['status'] == 500) {
            return $errorMessage->setMessage($cekSurvey['message']);
        }
        if($this->tahun->deleteTahun($tahun)){
            return true;
        }
        
        return false;
    }    
    /**
     * aktivasi
     * menjadikan kolom is_active sebagai button aktivasi
     * @param  mixed $value
     * @param  mixed $row
     * @return void
     */
    public function aktivasi($value, $row)
    {
        if ($row->is_active == '1') {
            return '<button type="button" onclick="noAktif(' . "'" . $row->tahun . "'" . ')" id="btn-edit-task" class="btn btn-warning btn-xs"><i class="fas fa-lock"></i> Tidak Aktif</button>';
        } else {
            $id = 1;
            return '<button type="button" onclick="aktif(' . "'" . $row->tahun . "'" . ')" id="btn-edit-task" class="btn btn-success btn-xs"><i class="fas fa-unlock"></i> Aktif</button>';
        }
    }
    
    /**
     * aktifTahun
     * fungsi mengaktifkan dan menonaftifkan tahun
     * @return void
     */
    public function aktifTahun()
    {
        $tahun = $this->input->post('id_tahun');
        $data = [
            'is_active' => $this->input->post('is_active')
        ];

        $response = $this->tahun->aktifTahun($tahun, $data);

        echo json_encode($response);
    }
}
