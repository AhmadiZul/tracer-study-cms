<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "faq";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_sistem','sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-database';
        $this->data['title'] = 'Frequently Asked Questions';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Frequently Asked Questions');

        $crud->setTable('faq');

        $crud->columns([
            'pertanyaan',
            'jawaban'
        ]);

        $crud->displayAs([
            'pertanyaan' => 'Pertanyaan',
            'jawaban' => 'Jawaban'
        ]);

        $crud->addFields([
            'pertanyaan',
            'jawaban'
        ]);

        $crud->editFields([
            'pertanyaan',
            'jawaban'
        ]);

        $crud->requiredFields([
            'pertanyaan',
            'jawaban'
        ]);


        $output = $crud->render();
        $this->_setOutput('faq/index', $output);
    }

}
