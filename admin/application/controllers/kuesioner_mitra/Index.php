<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "kuesioner_mitra";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_mitra','mitra');
        
    }

    public function index()
    {
        $this->data['title'] = 'kuesioner Survey Mitra';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('kuesioner Survey Mitra');
        $crud->setTable('kuesioner_mitra_question');

        $crud->where([
            'kuesioner_mitra_question.is_active' => '1'
        ]);

        $crud->defaultOrdering('urutan', 'ASC');

        $column = ['question', 'urutan', 'question_type', 'status'];
        $fields = ['question', 'urutan', 'question_type', 'status'];
        $requireds = ['question', 'urutan', 'question_type', 'status', 'answer_rules', 'others'];
        $fieldsDisplay = [
            'question'=>'Question',
            'urutan'=>'Urutan',
            'question_type'=>'Question Type',
            'status'=>'status',
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);
        $crud->unsetAdd();
        $crud->unsetEdit();

        $crud->callbackColumn('urutan', function ($value, $row) {
            return '<button type="button" onclick="urutanModal(' . "'" . $row->id_kuesioner . "'" . ','. "'" . $value. "'" . ')" class="btn bg-violet text-white">'.$value.'</button>';
            
        });
        $crud->callbackColumn('question_type', function ($value, $row) {
            if($value == "1") {
                return '<span class="badge badge-square bg-green-taffeta text-white">Jawaban Singkat</span>';
            } elseif($value == "2") {
                return '<span class="badge badge-square bg-secondary text-white">Paragraf</span>';
            } elseif($value == "3") {
                return '<span class="badge badge-square bg-red-taffeta text-white">Pilihan Ganda</span>';
            } elseif($value == "4") {
                return '<span class="badge badge-square bg-violet-taffeta text-white">Kotak Centang</span>';
            } elseif($value == "5") {
                return '<span class="badge badge-square bg-yellow-taffeta text-white">Skala Likert</span>';
            } elseif($value == "6") {
                return '<span class="badge badge-square bg-dark text-white">Sub Pertanyaan</span>';
            } else {
                return '<span class="badge badge-square bg-blue-taffeta text-white">Lokasi</span>';
            }
            
        });
        $crud->callbackColumn('status', function ($value, $row) {
            if ($value == "1") {
                return '<button type="button" onclick="noAktif(' . "'" . $row->id_kuesioner . "'" . ')" class="btn bg-green-taffeta text-white"><i class="fas fa-unlock"></i> Aktif</button>';
            }
            return '<button type="button" onclick="aktif(' . "'" . $row->id_kuesioner . "'" . ')" class="btn bg-yellow-taffeta text-white"><i class="fas fa-lock"></i> Tidak Aktif</button>';
            
        });

        $crud->callbackDelete(array($this,'delete_kuesioner'));

        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('kuesioner_mitra/index/ubahKuesioner?key='. $row->id_kuesioner);
        }, false);

        $output = $crud->render();
        $this->_setOutput('mitra/kuesioner', $output);
    }

    public function delete_kuesioner($id)
    {
        return $this->db->update('kuesioner_mitra_question',array('is_active' => '0','status' => '0'),array('id_kuesioner' => $id->primaryKeyValue));
    }

    public function addKuesioner()
    {
        $this->data['title'] = 'Tambah Data Kuesioner';
        $this->data['is_home'] = false;
        $this->render('detail');
    }

    public function ubahKuesioner()
    {
        $this->data['title'] = 'Edit Kuesioner';
        $this->data['is_home'] = false;
        $this->data['id_kuesioner'] = $this->input->get("key");
        if ($this->data['id_kuesioner'] == "") {
            redirect(base_url('kuesioner_mitra/index'));
        }
        $this->data['data'] = $this->getKuesionerById($this->data['id_kuesioner']);
        $this->render('edit_kuesioner');
    }

    public function updateKuesioner()
    {
        $params = $this->input->post();
        
        $update = $this->mitra->updateKuesioner($params);

        echo json_encode($update);
    }

    public function createKuesioner()
    {
        $params = $this->input->post();

        $insert = $this->mitra->createKuesioner($params);

        echo json_encode($insert);
    }

    public function getKuesionerById($id)
    {
        $kuesioner = $this->mitra->getKuesioner($id);
        $kuesioner['answer'] = $this->getAnswerByKuesioner($id, $kuesioner['question_type']);

        return $kuesioner;
    }

    public function getAnswerByKuesioner($id, $type)
    {
        $answer = array();

        switch ($type) {
            case '3':
                $answer = $this->mitra->getAnswerOptionByKuesioner($id, array('is_active' => '1'));
                break;
            case '4':
                $answer = $this->mitra->getAnswerOptionByKuesioner($id, array('is_active' => '1'));
                break;
            case '5':
                $answer = $this->mitra->getAnswerLikertByKuesioner($id, array('is_active' => '1'));
                break;
            
            default:
                $answer = null;
                break;
        }

        return $answer;
    }

    public function aktif()
    {
        $id_kuesioner = $this->input->post('id_kuesioner');
        $data = [
            'status' => $this->input->post('status')
        ];

        $response = $this->mitra->aktif($id_kuesioner, $data);

        echo json_encode($response);

    }

    public function simpanUrutan()
    {
        $id_kuesioner = $this->input->post('id_kuesioner');
        $urutan = $this->input->post('urutan');
        $data = [
            'urutan' => $urutan
        ];

        $response = $this->mitra->simpanUrutan($id_kuesioner, $data);

        echo json_encode($response);

    }

}
