<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "prodi";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_universitas','pt');
    }

    public function index()
    {
        $this->id = $this->input->get("key");
        $this->data['title'] = 'Program Studi ';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Program Studi');
        $crud->setTable('ref_prodi');

        // $crud->where([
        //     'ref_prodi.id_perguruan_tinggi = ?' => $this->id
        // ]);

        $column = ['kode_prodi', 'nama_prodi', 'singkatan'];
        $fields = ['nama_prodi', 'singkatan'];
        $requireds = ['nama_prodi', 'singkatan'];
        $fieldsDisplay = [
            'kode_prodi' => 'Kode Prodi',
            'nama_prodi' => 'Nama Prodi',
            'singkatan' => 'Singkatan',
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);
        $crud->uniqueFields(['kode_prodi']);

        $crud->callbackBeforeInsert(function ($stateParameters) {
            $stateParameters->data['id_prodi'] = getUUID();
            $stateParameters->data['id_perguruan_tinggi'] = $this->id;
            return $stateParameters;
        });

        $output = $crud->render();
        $this->_setOutput('prodi/index/index', $output);
    }


}
