<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "alumni_berprestasi";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_prestasi', 'prestasi');
        $this->load->model('M_referensi', 'referensi');
        $this->load->model('M_alumni', 'alumni');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-user-graduate';
        $this->data['title'] = 'Alumni Berprestasi';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Prestasi Alumni');

        $crud->setTable('alumni_berprestasi');

        $crud->columns([
            'nama_prestasi',
            'kesan',
            'url_foto',
            'is_publish',
        ]);

        $crud->displayAs([
            'nama_prestasi' => 'Prestasi',
            'kesan' => 'Kesan',
            'url_foto' => 'Foto',
            'is_publish' => 'Publikasi',
        ]);

        $crud->callbackColumn('is_publish', [$this, 'publikasi']);
        $crud->callbackColumn('url_foto', [$this, 'callPhoto']);
        $crud->callbackDelete([$this, 'deleteBanner']);
        $crud->editFields([
            'judul',
            'paragraf',
            'url_vidio',
        ]);

        $crud->requiredFields([]);

        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('alumni_berprestasi/index/edit_prestasi?key=' . $row->id);
        }, false);


        $output = $crud->render();
        $this->_setOutput('alumni_berprestasi/index', $output);
    }
    
    /**
     * addAlumniBerprestasi
     * menampilkan form tambah alumni berprestasi
     * @return void
     */
    public function addAlumniBerprestasi()
    {
        $this->data['title'] = 'Tambah Alumni Berprestasi';
        $this->data['is_home'] = false;
        $this->data['fakultas'] = $this->referensi->getFakultas();
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('tambah');
    }
    
    /**
     * callPhoto
     * mengubah kolom foto menjadi preview 
     * @param  mixed $value
     * @param  mixed $rows
     * @return void
     */
    public function callPhoto($value, $rows)
    {
        $return = '-';
        if ($value != null || $value != "") {
            if (file_exists($value)) {
                $return = '<img src="' . base_url() . $value . '" class="img img-fluid" width="100px" alt="Alumni Berprestasi">';
            }
        }
        return $return;
    }
    
    /**
     * _urlLogo
     * fungsi insert file foto
     * @param  mixed $id
     * @param  mixed $id_fakultas
     * @return void
     */
    function _urlLogo($id = 0, $id_fakultas)
    {
        if ($id) {
            $alumni_prestasi = $this->prestasi->getAlumniBerprestasi(array('id' => $id));
        }
        $id_fakultas = str_replace('-', '', $id_fakultas);
        if (!file_exists('public/uploads/alumni_berprestasi/' . $id_fakultas)) {
            mkdir('public/uploads/alumni_berprestasi/' . $id_fakultas, 0755, true);
            chmod('public/uploads/alumni_berprestasi/', 0755);
        }

        $oldUrlBukti = '';
        if (!empty($alumni_prestasi)) {
            $oldUrlBukti = $alumni_prestasi->url_foto;
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/alumni_berprestasi/' . $id_fakultas; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'foto' . $id_fakultas;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = 'public/uploads/alumni_berprestasi/' . $id_fakultas . '/' . $file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File Harus kurang dari 2 mb dan ukuran gambar 354 x 472";
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }
    
    /**
     * simpanAlumniPrestasi
     * fungsi menyimpan data alumni berprestasi ke database
     * @return void
     */
    public function simpanAlumniPrestasi()
    {
        $input = $this->input->post();

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $dbError[] = $this->db->error();

        $id = getUUID();
        $id_fakultas = $input['input_fakultas'];
        $alumni_prestasi = array(
            'id' => $id,
            'nama_alumni' => $input['input_alumni'],
            'id_fakultas' => $id_fakultas,
            'id_prodi' => $input['input_prodi'],
            'kesan' => $input['kesan'],
            'nama_prestasi' => $input['nama_prestasi'],
            'is_publish' => '1',
            'created_by'        => $this->session->userdata('tracer_userId'),
            'modified_by'  => $this->session->userdata('tracer_userId')
        );

        $urlLogo = $this->_urlLogo($id, $id_fakultas);
        $alumni_prestasi['url_foto'] = $urlLogo['file_name'];

        $this->prestasi->simpan_alumni_berprestasi($alumni_prestasi);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan alumni berprestasi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan alumni berprestasi',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    
    /**
     * getProdi
     * fungsi memperoleh data prodi berdasarkan fakultas
     * @return void
     */
    public function getProdi()
    {
        $id_fakultas = $this->input->get('kode_prodi');
        $prodi = setProdi($id_fakultas);
        echo json_encode($prodi);
    }
    
    /**
     * getAlumni
     * 
     * @return void
     */
    
    /**
     * _runValidation
     * validasi sebelum tambah atau edit alumni berprestasi
     * @return void
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('nama_prestasi', 'Nama prestasi', 'trim|required|max_length[50]|min_length[3]');
        $this->form_validation->set_rules('kesan', 'Kesan', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('input_perguruan_tinggi', 'Asal Kampus/Asal Sekolah', 'trim|required');
        $this->form_validation->set_rules('input_fakultas', 'Fakultas', 'trim|required');
        $this->form_validation->set_rules('input_prodi', 'Program Studi/Jurusan', 'trim|required');
        $this->form_validation->set_rules('input_alumni', 'Nama alumni', 'trim|required|min_length[3]|max_length[15]');

        if (empty($_FILES['url_photo']['name']) && empty(($_POST['oldFoto']))) {
            $errors[] = [
                'field'   => 'url_photo',
                'message' => 'Foto tidak boleh kosong.',
            ];
        }

        $this->form_validation->set_message('required', '{field} tidak boleh kosong.');
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('validUrl', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('checkPT', '{field} sudah terdaftar');
        $this->form_validation->set_message('checkUser', '{field} sudah terdaftar');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }
    
    /**
     * publikasi
     * mengubah kolom is_publish menjadi tombol publikasi
     * @param  mixed $value
     * @param  mixed $row
     * @return void
     */
    public function publikasi($value, $row)
    {
        if ($row->is_publish == '1') {
            return '<button type="button" onclick="noPublish(' . "'" . $row->id . "'" . ')" id="btn-edit-task" class="btn btn-warning btn-xs"><i class="fas fa-lock"></i> Stop Publish</button>';
        } else {
            $id = 1;
            return '<button type="button" onclick="publish(' . "'" . $row->id . "'" . ')" id="btn-edit-task" class="btn btn-success btn-xs"><i class="fas fa-unlock"></i> Publish</button>';
        }
    }
    
    /**
     * publishAlumni
     * fungsi mengubah kolom is_publish alumni berprestasi
     * @return void
     */
    public function publishAlumni()
    {
        $id = $this->input->post('id');
        $data = [
            'is_publish' => $this->input->post('is_publish')
        ];

        $response = $this->prestasi->publishAlumni($id, $data);

        echo json_encode($response);
    }
    
    /**
     * deleteBanner
     * fungsi menghapus alumni berprestasi untuk grocery
     * @param  mixed $id
     * @return void
     */
    public function deleteBanner($id)
    {
        $this->db->where('id', $id->primaryKeyValue);
        $alumni = $this->db->get('alumni_berprestasi')->row();
        if (empty($alumni))
            return false;

        unlink($alumni->url_foto);
        $this->db->where('id', $id->primaryKeyValue);
        $this->db->delete('alumni_berprestasi');
        return true;
    }
    
    /**
     * edit_prestasi
     * menampilkan halaman edit alumni berprestasi
     * @return void
     */
    public function edit_prestasi()
    {
        $alumni_prestasi = $this->prestasi->getAlumniBerprestasi(array('id' => $this->input->get('key')));
        $this->data['title'] = 'Edit Alumni Berprestasi';
        $this->data['is_home'] = false;
        $this->data['alumni_prestasi'] = $alumni_prestasi;
        $this->data['fakultas'] = $this->referensi->getFakultas();
        $this->data['prodi'] = $this->referensi->getProdiByFakultas($alumni_prestasi->id_fakultas);
        $this->data['alumni'] = $this->prestasi->getAlumniByProdi($alumni_prestasi->id_prodi);
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('edit');
    }    
    /**
     * editPrestasi
     * fungsi untuk mengedit alumni berprestasi di database
     * @return void
     */
    public function editPrestasi()
    {
        $input = $this->input->post();

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $dbError[] = $this->db->error();

        $id = $input['id'];
        $id_fakultas = $input['input_fakultas'];
        $alumni_prestasi = array(
            'nama_alumni' => $input['input_alumni'],
            'id_fakultas' => $id_fakultas,
            'id_prodi' => $input['input_prodi'],
            'kesan' => $input['kesan'],
            'nama_prestasi' => $input['nama_prestasi'],
            'is_publish' => $input['is_publish'],
            'created_by'        => $this->session->userdata('tracer_userId'),
            'modified_by'  => $this->session->userdata('tracer_userId')
        );

        if ($_FILES['url_photo']['name']) {
            $urlLogo = $this->_urlLogo($id, $id_fakultas);
            $alumni_prestasi['url_foto'] = $urlLogo['file_name'];
        } else {
            $alumni_prestasi['url_foto'] = $input['oldFoto'];
        }

        $this->prestasi->editPrestasi($alumni_prestasi, $id);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan alumni berprestasi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit alumni berprestasi',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
}
