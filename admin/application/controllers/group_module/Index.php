<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "group_module";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_module','modul');
        $this->load->model('M_sistem','sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-layer-group';
        $this->data['title'] = 'Group Module System';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('User Group');

        $crud->setTable('user_group');
        $crud->where(['is_active' => '1']);
        $crud->columns([
            'nama_group',
            'keterangan',
            'is_active',
        ]);

        $crud->displayAs([
            'nama_group' => 'Nama Grup',
            'keterangan' => 'Keterangan',
            'is_active' => 'Aktif'
        ]);

        $crud->addFields([
            'nama_group',
            'is_active',
            'dbusername',
            'keterangan',
        ]);

        $crud->editFields([
            'nama_group',
            'is_active',
            'dbusername',
            'keterangan',
        ]);

        $crud->requiredFields([
            'nama_group',
        ]);

        $crud->fieldType('is_active', 'dropdown', [
            '0' => 'Tidak Aktif',
            '1' => 'Aktif'
        ]);

        $crud->fieldType('id_group', 'hidden');

        $crud->setActionButton('Hak Akses', 'fas fa-users-cog', function ($row) {
            return site_url("group_module/detail?key=$row->id_group");
        }, false);

        $crud->callbackDelete(array($this,'delete_module'));

        $output = $crud->render();
        $this->_setOutput('group_module/index/index', $output);
    }

    public function detail()
    {
        $this->data['title'] = 'Akses group module';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->data['id'] = $this->input->get('key');
        $this->data['scripts'] = ['group_module/js/detail.js'];
        
        $this->render('index/detail');
    }

    public function loadModule()
    {
        $response['status'] = 500;
        $response['data'] = [];

        $data = [];

        $modul = $this->modul->getModule();

        $modul_group = $this->modul->getModuleGroup($this->input->post('id_group'));

        $data['modul_group'] = $modul_group['data'];
        if ($modul['status']==201) {
            $data['module'] = $modul['data'];
            $response['status'] = 201;

        }else{
            $data['module'] = [];
            $response['status'] = 500;
        }
        $response['data'] = $data;
        $this->responseJson($response);

    }

    public function updateModule()
    {
        $response['status'] = 500;
        $response['message'] = '';

        $params = $this->input->post(null, true);

        if (isset($params['module'])) {
            $result = $this->modul->updateModule($params);

            if ($result['status']==201) {
                $response['status'] = 201;
                $response['message'] = $result['message'];
            }else{
                $response['status'] = 500;
                $response['message'] = $result['message'];
            }
        }else{
            $response['status'] = 500;
            $response['message'] = "Modul tidak boleh kosong, pilih minimal 1 modul untuk update.";
        }


        $this->responseJson($response);
    }

    public function delete_module($id)
    {
        $this->db->where('id_group', $id->primaryKeyValue);
        $modul = $this->db->get('user_group')->row();
        if(empty($modul))
            return false;
    
        $this->db->set('is_active', '0');
        $this->db->where('id_group', $id->primaryKeyValue);
        $this->db->update('user_group');
        return true;
    }


}
