<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "company_talk";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('tanggalindo');
        $this->load->model('M_talk', 'talk');
    }

    public function index()
    {
        $this->data['title'] = 'Data Company Talk';
        $this->data['is_home'] = false;

        $referensi = $this->initData();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Company Talk');
        $crud->setTable('company_talks');

        $crud->setRelation('id_mitra', 'mitra', 'nama');
        if (getSessionRoleAccess() == '1') {
            $crud->setRelation('id_perguruan_tinggi_lokasi', 'ref_perguruan_tinggi', 'nama_pendek');
        } else {
            $crud->where(['id_perguruan_tinggi_lokasi' => getPTSessionID()]);
        }

        $crud->columns($referensi['columns']);
        $crud->displayAs($referensi['displays']);


        $crud->callbackColumn('waktu_awal_daftar', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('waktu_akhir_daftar', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('tanggal_pelaksanaan', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->fieldType('is_approved', 'dropdown_search', [
            '0' => 'Belum Diverifikasi',
            '1' => 'Disetujui',
            '2' => 'Tidak Disetujui',
        ]);
        $crud->callbackColumn('approved_by', [$this, 'callbackActionColumn']);
        $crud->callbackColumn('fasilitas', [$this, 'callbackActionFasilitas']);
        /* $crud->callbackColumn('url_formulir_kepesertaan', function ($value, $row) {
            if ($value != '' || $value != null) {
                $ext = explode('.', $value);
                if ($ext[1] == 'pdf') {
                    return '<a href="' . base_url('public/uploads/job-fairs/' . $value) . '" class="btn btn-info btn-sm" target="_blank"><i class="fas fa-download"></i> Unduh</a>';
                } else {
                    return '<img src="' . base_url('public/uploads/job-fairs/' . $value) . '" class="img-fluid">';
                }
            } else {
                return 'formulir tidak tersedia';
            }
        }); */
        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->unsetDelete();
        $crud->unsetExportPdf();
        $crud->unsetPrint();
        $output = $crud->render();
        $this->_setOutput('company_talk/index/index', $output);
    }

    public function callbackActionFasilitas($value, $rows)
    {
        if ($value != '' || $value != null) {
            $result = json_decode($value, true);
            $id = [];
            foreach ($result as $key => $value) {
                $id[$key] = $value['fasilitas'];
            }
            $result = $this->talk->getFasilitas($id,false);
            if ($result['status'] == 201) {
                $fasilitas = '<ol>';
                foreach ($result['data'] as $rows ) {
                    $fasilitas .= '<li>'.$rows->nama.'</li>';
                }
                $fasilitas .= '</ol>';
                return $fasilitas;
            }else{
                return 'Tidak ada permintaan fasilitas';
            }
        }else{
            return '-';
        }
    }

    public function callbackActionColumn($value, $rows)
    {
        $return = '<div class="btn-group">';
        $return .= '<a href="'.site_url('company_talk/index/detail?key='.$rows->id).'" class="btn btn-warning"><i class="fas fa-th-list"></i></a>';
        if ($rows->is_approved == '0') {
            $return .= '<button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Verifikasi pendaftaran company talk" onclick="validasi('.$rows->id.')"><i class="fas fa-check"></i></button>';
        }
        $return .= '</div>';
        return $return;
    }


    public function initData()
    {
        $columns = [];
        $displays = [];

        if (getSessionRoleAccess() == '1') {
            $columns = [
                'approved_by',
                'id_mitra',
                'judul_presentasi',
                'waktu_awal_daftar',
                'waktu_akhir_daftar',
                'tanggal_pelaksanaan',
                'id_perguruan_tinggi_lokasi',
                'di_ruang',
                'kapasitas',
                'fasilitas',
                'is_approved',
            ];

        } else {
            $columns = [
                'approved_by',
                'id_mitra',
                'judul_presentasi',
                'waktu_awal_daftar',
                'waktu_akhir_daftar',
                'tanggal_pelaksanaan',
                'di_ruang',
                'kapasitas',
                'fasilitas',
                'is_approved',
            ];

        }

        $displays = [
            'approved_by'                => 'Kelola',
            'id_mitra'                   => 'Mitra',
            'judul_presentasi'           => 'Judul Presentasi',
            'waktu_awal_daftar'          => 'Awal Pendaftaran',
            'waktu_akhir_daftar'         => 'Akhir Pendaftaran',
            'tanggal_pelaksanaan'        => 'Pelaksanaan',
            'id_perguruan_tinggi_lokasi' => 'Lokasi',
            'di_ruang'                   => 'Ruang',
            'kapasitas'                  => 'Kapasitas',
            'fasilitas'                  => 'Fasilitas Tambahan',
            'is_approved'                => 'Status',
        ];

        $data['columns']    = $columns;
        $data['displays']   = $displays;
        return $data;
    }

    public function detail()
    {
        $this->data['data']     = $data =$this->talk->edit($this->input->get('key'));;
        $this->data['title']    = 'Detail Company Talk'.($data['status'] == 201 ? ' '.$data['data']->judul_presentasi : '');
        $this->data['is_home']  = false;
        $this->data['scripts']  = ['company_talk/js/detail.js'];
        $this->render('detail');
    }

    public function pendaftar()
    {
        $return = [];

        $field = [
            'sSearch',
            'iSortCol_0',
            'sSortDir_0',
            'iDisplayStart',
            'iDisplayLength',
            'id_company_talks',
        ];

        foreach ($field as $v) {
            $$v = $this->input->get_post($v);
        }

        $return = [
            "sEcho"                 => $this->input->post('sEcho'),
            "iTotalRecords"         => 0,
            "iTotalDisplayRecords"  => 0,
            "aaData"                => []
        ];

        $params = [
            'sSearch'   => $sSearch,
            'start'     => $iDisplayStart,
            'limit'     => $iDisplayLength,
            'id_company_talks' => $id_company_talks,
        ];

        $data = $this->talk->pendaftar($params);
        if ($data['total'] > 0) {
            $return['iTotalRecords'] = $data['total'];
            $return['iTotalDisplayRecords'] = $return['iTotalRecords'];

            foreach ($data['rows'] as $k => $row) {

                $row['no']                  = '<p class="text-center">' . ($iDisplayStart + ($k + 1)) . '</p>';
                $row['nama']                = $row['nama'];
                $row['asalKampus']               = $row['nama_resmi'];
                $row['prodi']   = $row['nama_prodi'];


                $return['aaData'][] = $row;
            }
        }
        $this->db->flush_cache();
        echo json_encode($return);
    }

    public function verifyTalk()
    {
        $id = $this->input->get('id');

        $response = '';

        $this->db->trans_begin();
        $dbError = [];

        $ubahPendaftar = $this->talk->verifyTalk(['is_approved' => '1'], ['id' => $id]);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $response = [
                'success' => false,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal verifikasi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $response = [
            'success' => true,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil verifikasi',
            ],
        ];

        End:
        echo json_encode($response);
    }

    public function tolakTalk()
    {
        $id = $this->input->get('id');

        $response = '';

        $this->db->trans_begin();
        $dbError = [];

        $ubahPendaftar = $this->talk->verifyTalk(['is_approved' => '2'], ['id' => $id]);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $response = [
                'success' => false,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal verifikasi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $response = [
            'success' => true,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil verifikasi',
            ],
        ];

        End:
        echo json_encode($response);
    }
}
