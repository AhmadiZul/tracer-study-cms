<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = false;
    public $template = "login_app";
    protected $module = "register";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title'] = 'Registrasi';
        $this->data['is_full'] = true;
        $this->renderTo('auth/register/index');
    }

    public function alumni()
    {
        $this->data['title'] = 'alumni';
        $this->data['is_full'] = true;
        $this->renderTo('login/alumni');
    }

    public function mitra()
    {
        $this->data['title'] = 'mitra';
        $this->data['is_full'] = true;
        $this->renderTo('login/mitra');
    }
}
