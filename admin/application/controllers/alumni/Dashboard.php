<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "alumni";
    protected $onUpdate = 0;
    private $sheet, $spreadsheet;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_referensi', 'referensi');
        $this->load->model('M_alumni', 'alumni');
        $this->load->model('M_sistem', 'sistem');
        $this->load->model('M_user', 'user');
        $this->load->library('phpexcel');
        $this->load->library('form_validation');
        
    }

    public function index()
    {
        $this->data['icon'] = 'fa fa-book';
        $this->data['title'] = 'Alumni';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Alumni');
        $crud->setTable('alumni');

        $crud->setRelation('id_fakultas', 'ref_fakultas', 'nama_fakultas');
        // if ($this->session->userdata("tracer_idGroup") == 2) {
        //     $crud->setRelation('id_perguruan_tinggi', 'ref_perguruan_tinggi', 'nama_resmi', array('id_user' => $this->session->userdata("tracer_userId")));
        // }

        $crud->setRelation('id_prodi', 'ref_prodi', 'nama_prodi');
        $crud->setRelation('id_ref_status_alumni', 'ref_status_alumni', 'status_alumni');
        $crud->setRelation('kode_prov', 'ref_provinsi', 'nama_provinsi');
        $crud->setRelation('kode_kab_kota', 'ref_kab_kota', 'nama_kab_kota');
        $crud->setRelation('kode_kecamatan', 'ref_kecamatan', 'nama_kecamatan');

        $crud->where([
            'alumni.is_active' => '1',
            'tahun_lulus' => $this->session->userdata("tracer_tahun")
        ]);

        $crud->defaultOrdering('alumni.nama', 'asc');

        $crud->unsetSearchColumns(['last_modified_time', 'url_photo', 'status_data']);

        // Removes only the PDF button on export
        $crud->unsetExportPdf();

        $column = ['nama', 'id_fakultas', 'id_prodi', 'nim', 'nik', 'tempat_lahir', 'tgl_lahir', 'jenis_kelamin', 'no_hp', 'email', 'tahun_lulus', 'alamat', 'kode_prov', 'kode_kab_kota', 'kode_kecamatan', 'id_ref_status_alumni', 'last_modified_time', 'url_photo', 'status_data'];
        $fieldsDisplay = [
            'nama' => 'Nama',
            'id_fakultas' => 'Fakultas',
            'id_prodi' => 'Program Studi/Jurusan',
            'nim' => 'NIM/NIRM/NIS',
            'nip' => 'NIP',
            'nik' => 'NIK',
            'tempat_lahir' => 'Tempat Lahir',
            'tgl_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'no_hp' => 'No. Telepon',
            'email' => 'Email',
            'tahun_lulus' => 'Tahun Lulus',
            'kode_prov' => 'Kode Provinsi',
            'kode_kab_kota' => 'Kode Kabupaten/Kota',
            'kode_kecamatan' => 'Kode Kecamatan',
            'id_ref_status_alumni' => 'Status Alumni',
            'last_modified_time' => 'Sektor',
            'url_photo' => 'Foto',
            'status_data' => 'Verifikasi'
        ];

        $crud->fieldType('jenis_kelamin', 'dropdown_search', [
            'L' => 'LAKI-LAKI',
            'P' => 'PEREMPUAN'
        ]);

        $crud->fieldType('tahun_lulus', 'dropdown_search', setTahun());

        $crud->columns($column);
        $crud->displayAs($fieldsDisplay);
        $crud->uniqueFields(['email']);
        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->unsetPrint();

        $user_id = $this->session->userdata("tracer_userId");
        $this->data['user'] = $user = $this->user->getUser($user_id);
        if($this->session->userdata("tracer_idGroup") == 2){
            $crud->where(['id_fakultas' => $user['id_fakultas']]);
            $crud->unsetSearchColumns(['id_fakultas', 'id_prodi']);
        }
        $crud->callbackColumn('status_data', [$this, 'verifikasi_alumni']);
        $crud->callbackColumn('url_photo', [$this, 'callPhoto']);
        $crud->callbackColumn('tgl_lahir', [$this, 'callTgl']);

        $crud->callbackDelete(array($this, 'deleteAlumni'));
        $crud->setActionButton('Detail', 'fa fa-book', function ($row) {
            return site_url('alumni/dashboard/detail/' . $row->id_alumni);
        }, false);
        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('alumni/dashboard/ubahAlumni?key=' . $row->id_alumni);
        }, false);

        $crud->callbackDelete([$this, 'deleteAlumni']);
        $output = $crud->render();
        $this->_setOutput('alumni/index/index', $output);
    }
    
    /**
     * deleteAlumni
     * menghapus data alumni
     * @param  mixed $id
     * @return void
     */
    public function deleteAlumni($id)
    {
        $id_alumni = $id->primaryKeyValue;
        if($this->alumni->hapusAlumni($id_alumni)){
            return true;
        }
        return false;
    }
    
    /**
     * verifikasi_alumni
     * mengubah kolom is_active menjadi tombol aktivasi
     * @param  mixed $value
     * @param  mixed $row
     * @return void
     */
    public function verifikasi_alumni($value, $row)
    {
        $data = $this->alumni->getAlumni($row->id_alumni);
        if ($row->status_data == '1') {
            return '<p class="fs-7 fw-bold">Sudah Terverifikasi</p>';
        } else {
            $id = 1;
            return '<button type="button" onclick="verifAlumni(' . "'" . $data['id_alumni'] . "'" . ')" id="btn-edit-task" class="btn btn-success btn-xs"><i class="fas fa-edit"></i> Verifikasi</button>';
        }
    }

    
    /**
     * verifAlumni
     * fungsi verifikasi alumni oleh admin
     * @return void
     */
    public function verifAlumni()
    {
        $id_alumni = $this->input->post('id_mhs');
        $data = [
            'status_data' => $this->input->post('status_data'),
            'last_modified_user' => $this->session->userdata("tracer_userId")
        ];

        $response = $this->alumni->verifAlumni($id_alumni, $data);

        echo json_encode($response);
    }
    
    /**
     * detail
     * menampilkan halaman detail data alumni
     * @param  mixed $id
     * @return void
     */
    public function detail($id)
    {
        $this->data['title'] = 'Detail Alumni';
        $this->data['back'] = '/Alumni';
        $this->data['url_back'] = 'alumni/dashboard/index';
        $this->data['icon'] = 'fa fa-book';
        $this->data['is_home'] = false;
        $this->data['id_alumni'] = $id;
        $this->data['alumni'] = $alumni = $this->alumni->getAlumni($id);
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();

        if ($this->data['alumni']['jenis_kelamin'] == 'L') {
            $this->data['alumni']['jenis_kelamin'] = "LAKI-LAKI";
        }
        if ($this->data['alumni']['jenis_kelamin'] == 'P') {
            $this->data['alumni']['jenis_kelamin'] = "PEREMPUAN";
        }

        if ($this->data['alumni']['tgl_lahir'] == '0000/00/00' || $this->data['alumni']['tgl_lahir'] == '0000-00-00') {
            $this->data['alumni']['tgl_lahir'] = "-";
        }
        $this->data['jobsNow'] = $this->alumni->getNowJobs($id);
        $this->data['jobs'] = $this->alumni->getJobsAlumni($id);
        $this->data['study'] = $this->alumni->getStudyAlumni($id);
        $this->data['prestasi'] = $this->alumni->getPrestasiAlumni($id);
        $this->data['organisasi'] = $this->alumni->getOrganisasiAlumni($id);
        $this->data['pengalaman'] = $this->alumni->getPengalamanAlumni($id);
        $this->data['skill'] = $this->alumni->getSkillAlumni($id);
        $this->render('dashboard/detail_alumni');
    }
    
    /**
     * addAlumni
     * menampilkan halaman tambah data alumni
     * @return void
     */
    public function addAlumni()
    {
        $this->data['title'] = 'Tambah Alumni';
        $this->data['back'] = '/Alumni';
        $this->data['url_back'] = 'alumni/dashboard/index';
        $this->data['icon'] = 'fa fa-book';
        $this->data['is_home'] = false;
        $this->data['propinsi'] = setPronvinsi();
        $this->data['tahun_lulus'] = $this->referensi->getTahunLulus();
        $this->data['agama'] = setAgama();
        $this->data['fakultas'] = $fakultas = $this->referensi->getFakultas();
        $this->data['status_alumni'] = $this->referensi->getStatusAlumni();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->data['warna_tema'] = $this->sistem->warna_tema();

        $user_id = $this->session->userdata("tracer_userId");
        $this->data['user'] = $user = $this->user->getUser($user_id);

        foreach ($fakultas as $key => $value) {
            if($value['id_fakultas'] == $user['id_fakultas']){
                $this->data['id_fakultas'] = $value['id_fakultas'];
                $this->data['nama_fakultas'] = $value['nama_fakultas'];
            }
        }
        $this->render('dashboard/detail');
    }
    
    /**
     * ubahAlumni
     * menampilkan halaman edit data alumni
     * @return void
     */
    public function ubahAlumni()
    {
        $this->data['icon'] = 'fa fa-book';
        $this->data['title'] = 'Edit Alumni';
        $this->data['is_home'] = false;
        $this->data['id_alumni'] = $this->input->get("key");
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->data['warna_tema'] = $this->sistem->warna_tema();


        if ($this->data['id_alumni'] == "") {
            redirect(base_url('alumni/dashboard/index'));
        }
        $this->data['data'] = $this->alumni->getAlumni($this->data['id_alumni']);
        if ($this->data['data']['url_photo']) {
            $foto = $this->data['data']['url_photo'];
            $this->data['data']['foto'] = $foto;
        }

        if ($this->data['data']['kode_prov']) {
            $this->data['kab_kota'] = setKabupaten($this->data['data']['kode_prov']);
        }

        if ($this->data['data']['kode_kab_kota']) {
            $this->data['kecamatan'] = setKecamatan($this->data['data']['kode_kab_kota']);
        }

        if ($this->data['data']['id_fakultas']) {
            $this->data['prodi'] = setProdi($this->data['data']['id_fakultas']);
        }

        if ($this->data['data']['id_ref_status_alumni'] == '3' || $this->data['data']['id_ref_status_alumni'] == '1') {
            $jobs = $this->alumni->getNowJobs($this->data['id_alumni']);
            $this->data['data']['id_job_study'] = $jobs['id_job'];
            $this->data['data']['nama_instansi'] = $jobs['nama_instansi'];
            $this->data['data']['tgl_mulai'] = $jobs['tgl_mulai'];
            $this->data['data']['jabatan'] = $jobs['jabatan'];
            $this->data['data']['tipe_usaha'] = $jobs['tipe_usaha'];
            $this->data['data']['lainnya'] = $jobs['lainnya'];
            $this->data['data']['alamat_instansi'] = $jobs['alamat'];
            $this->data['data']['provinsi_instansi'] = $jobs['kode_provinsi'];
            $this->data['data']['kab_kota_instansi'] = $jobs['kode_kab_kota'];
            $this->data['data']['kecamatan_instansi'] = $jobs['kode_kecamatan'];

            if ($jobs['kode_provinsi']) {
                $this->data['instansi_kab_kota'] = setKabupaten($jobs['kode_provinsi']);
            }

            if ($jobs['kode_kab_kota']) {
                $this->data['instansi_kecamatan'] = setKecamatan($jobs['kode_kab_kota']);
            }
        } else if ($this->data['data']['id_ref_status_alumni'] == '4') {
            $study = $this->alumni->getNowStudy($this->data['id_alumni']);
            $this->data['data']['id_job_study'] = $study['id_study'];
            $this->data['data']['nama_instansi'] = $study['nama_perguruan_tinggi'];
            $this->data['data']['tgl_mulai'] = $study['tgl_mulai'];
            $this->data['data']['prodi'] = $study['prodi'];
            $this->data['data']['provinsi_instansi'] = $study['kode_provinsi'];
            $this->data['data']['kab_kota_instansi'] = $study['kode_kab_kota'];
            $this->data['data']['kecamatan_instansi'] = $study['kode_kecamatan'];

            if ($study['kode_provinsi']) {
                $this->data['instansi_kab_kota'] = setKabupaten($study['kode_provinsi']);
            }

            if ($study['kode_kab_kota']) {
                $this->data['instansi_kecamatan'] = setKecamatan($study['kode_kab_kota']);
            }
        }

        $this->data['propinsi'] = setPronvinsi();
        $this->data['tahun_lulus'] = $this->referensi->getTahunLulus();
        $this->data['fakultas'] = $fakultas = $this->referensi->getFakultas();
        $this->data['agama'] = setAgama();

        $this->data['status_alumni'] = setStatusAlumni();

        // if ($this->session->userdata("tracer_idGroup") == 2) {
        //     $this->data['perguruan_tinggi'] = $this->referensi->getFakultas($this->session->userdata("tracer_idPerguruanTinggi"));
        // }

        $user_id = $this->session->userdata("tracer_userId");
        $this->data['user'] = $user = $this->user->getUser($user_id);

        foreach ($fakultas as $key => $value) {
            if($value['id_fakultas'] == $user['id_fakultas']){
                $this->data['id_fakultas'] = $value['id_fakultas'];
                $this->data['nama_fakultas'] = $value['nama_fakultas'];
            }
        }
        $this->render('dashboard/edit_alumni');
    }
    
    /**
     * _urlphoto
     * fungsi menambahkan foto ke penyimpanan
     * @param  mixed $id_alumni
     * @return void
     */
    function _urlphoto($id_alumni)
    {
        $idFk = $this->input->post('input_fakultas');
        $alumni = $this->alumni->getAlumni($id_alumni);

        $idFk = str_replace("-", "", $idFk);
        $alumni = str_replace("-", "", $alumni);
        $id_alumni = str_replace("-", "", $id_alumni);

        if (!file_exists('public/uploads/fakultas/' . $idFk)) {
            mkdir('public/uploads/fakultas/' . $idFk, 0755,  true);
        }
        if (!file_exists('public/uploads/fakultas/' . $idFk . '/alumni')) {
            mkdir('public/uploads/fakultas/' . $idFk .  '/alumni/', 0755, true);
        }
        if (!file_exists('public/uploads/fakultas/' . $idFk . '/alumni/' . $id_alumni)) {
            mkdir('public/uploads/fakultas/' . $idFk .  '/alumni/' . $id_alumni, 0755, true);
        }

        $oldUrlBukti = '';
        if (!empty($alumni)) {
            $base_url = base_url();
            $oldUrlBukti = str_replace($base_url, '', $alumni['url_photo']);
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/fakultas/' . $idFk . '/alumni/' . $id_alumni . '/'; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'foto' . $id_alumni;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = 'public/uploads/fakultas/' . $idFk . '/alumni/' . $id_alumni . '/' . $file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }
    
    /**
     * createAlumni
     * menambahkan data alumni ke database
     * @return void
     */
    public function createAlumni()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_alumni = getUUID();

        $data3 = array();

        $email = '';

        $univ = $this->referensi->getListUniv($this->input->post('input_perguruan_tinggi'));


        $data2 = array(
            'username' => $this->input->post('input_nim'),
            'email' => $this->input->post('input_email'),
            'real_name' => $this->input->post('input_nama'),
            'id_group' => 4,
            'password' => hash('sha256', $this->input->post('input_nim')),
        );

        $id_user = $this->alumni->simpan_user($data2);
        $dbError[] = $this->db->error();

        $data = array(
            'id_alumni' => $id_alumni,
            'nim' => $this->input->post('input_nim'),
            'nama' => $this->input->post('input_nama'),
            'nik' => $this->input->post('input_nik'),
            'tempat_lahir' => $this->input->post('input_tempat_lahir'),
            'tgl_lahir' => $this->input->post('input_tanggal_lahir'),
            'jenis_kelamin' => $this->input->post('input_jk'),
            'no_hp' => $this->input->post('input_no_hp'),
            'id_agama' => $this->input->post('input_agama'),
            'email' => $email,
            'alamat' => $this->input->post('input_alamat'),
            'kode_prov' => $this->input->post('input_provinsi'),
            'kode_kab_kota' => $this->input->post('input_kabupaten'),
            'kode_kecamatan' => $this->input->post('input_kecamatan'),
            'tahun_lulus' => $this->input->post('input_tahun_lulus'),
            'ipk_terakhir' => $this->input->post('input_ipk_terakhir'),
            'id_fakultas' => $this->input->post('input_fakultas'),
            'id_prodi' => $this->input->post('input_prodi'),
            'judul_skripsi' => $this->input->post('input_skripsi'),
            'id_ref_status_alumni' => $this->input->post('input_status_alumni'),
            'id_user' => $id_user,
            'last_action_user' => "CREATE",
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_alumni);
            $data['url_photo'] = $urlPhoto['file_name'];
        }

        $simpanAlumni = $this->alumni->simpan_alumni($data);
        $dbError[] = $this->db->error();

        $id_job_study = getUUID();
        if ($this->input->post('input_status_alumni') == '3' || $this->input->post('input_status_alumni') == '1') {
            $tipe_usaha = '';
            $lainnya = '';
            if ($this->input->post('input_status_alumni') == '3') {
                $tipe_usaha = $this->input->post('input_tipe_usaha');
                if ($tipe_usaha == 'Lainnya') {
                    $lainnya = $this->input->post('input_lainnya');
                }
            }
            $data3 = [
                'id_job' => $id_job_study,
                'id_alumni' => $id_alumni,
                'id_ref_status_alumni' => $this->input->post('input_status_alumni'),
                'nama_instansi' => $this->input->post('input_instansi'),
                'jabatan' => $this->input->post('input_jabatan'),
                'tipe_usaha' => $tipe_usaha,
                'lainnya' => $lainnya,
                'kode_provinsi' => ($this->input->post('provinsi_instansi2') ? $this->input->post('provinsi_instansi2') : null),
                'kode_kab_kota' => ($this->input->post('kabupaten_instansi2') ? $this->input->post('kabupaten_instansi2') : null),
                'kode_kecamatan' => ($this->input->post('kecamatan_instansi2') ? $this->input->post('kecamatan_instansi2') : null),
                'tgl_mulai' => $this->input->post('input_tanggal_mulai'),
                'alamat' => $this->input->post('alamat_instansi'),
                'created_by' => $this->session->userdata('tracer_userId'),
                'last_modified_by' => $this->session->userdata('tracer_userId')
            ];

            $simpanJobs = $this->alumni->simpan_alumni_jobs($data3);
            $dbError[] = $this->db->error();
        }
        if ($this->input->post('input_status_alumni') == '4') {
            $data3 = [
                'id_study' => $id_job_study,
                'id_alumni' => $id_alumni,
                'nama_perguruan_tinggi' => $this->input->post('input_univ_baru'),
                'prodi' => $this->input->post('input_prodi_baru'),
                'tgl_mulai' => $this->input->post('input_tanggal_mulai2'),
                'kode_provinsi' => ($this->input->post('provinsi_instansi') ? $this->input->post('provinsi_instansi') : null),
                'kode_kab_kota' => ($this->input->post('kabupaten_instansi') ? $this->input->post('kabupaten_instansi') : null),
                'kode_kecamatan' => ($this->input->post('kecamatan_instansi') ? $this->input->post('kecamatan_instansi') : null),
                'created_by' => $this->session->userdata('tracer_userId'),
                'last_modified_by' => $this->session->userdata('tracer_userId')
            ];

            $simpanStudy = $this->alumni->simpan_alumni_study($data3);
            $dbError[] = $this->db->error();
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan alumni',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan alumni',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    
    /**
     * editAlumni
     * mengedit data alumni
     * @return void
     */
    public function editAlumni()
    {
        $data2 = array();

        $this->onUpdate = 1;

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_alumni = $this->input->post('id_alumni');
        $alumni = $this->alumni->getAlumni($id_alumni);
        $user = array();

        if ($this->input->post('input_nim') && $this->input->post('input_nim') != $alumni['nim']) {
            $user['username'] = $this->input->post('input_nim');
        }

        if ($this->input->post('input_email') && $this->input->post('input_email') != $alumni['email']) {
            $user['email'] = $this->input->post('input_email');
        }

        if ($this->input->post('input_nama') != $alumni['nama']) {
            $user['real_name'] = $this->input->post('input_nama');
        }

        if (!empty($user)) {
            $this->alumni->edit_user($user, array('id_user' => $alumni['id_user']));
            $dbError[] = $this->db->error();
        }

        $data = array(
            'nim' => $this->input->post('input_nim'),
            'nama' => $this->input->post('input_nama'),
            'nik' => $this->input->post('input_nik'),
            'tempat_lahir' => $this->input->post('input_tempat_lahir'),
            'tgl_lahir' => $this->input->post('input_tanggal_lahir'),
            'jenis_kelamin' => $this->input->post('input_jk'),
            'no_hp' => $this->input->post('input_no_hp'),
            'id_agama' => $this->input->post('input_agama'),
            'email' => $this->input->post('input_email'),
            'alamat' => $this->input->post('input_alamat'),
            'kode_prov' => $this->input->post('input_provinsi'),
            'kode_kab_kota' => $this->input->post('input_kabupaten'),
            'kode_kecamatan' => $this->input->post('input_kecamatan'),
            'tahun_lulus' => $this->input->post('input_tahun_lulus'),
            'ipk_terakhir' => $this->input->post('input_ipk_terakhir'),
            'id_fakultas' => $this->input->post('input_fakultas'),
            'id_prodi' => $this->input->post('input_prodi'),
            'judul_skripsi' => $this->input->post('input_skripsi'),
            'id_ref_status_alumni' => $this->input->post('input_status_alumni'),
            'last_action_user' => "UPDATE",
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($this->input->post('id_alumni'));
            $data['url_photo'] = $urlPhoto['file_name'];
        }

        $this->alumni->edit_alumni($data, array('id_alumni' => $id_alumni));
        $dbError[] = $this->db->error();


        if ($this->input->post('input_status_alumni') == '3' || $this->input->post('input_status_alumni') == '1') {
            $tipe_usaha = '';
            $lainnya = '';
            if ($this->input->post('input_status_alumni') == '3') {
                $tipe_usaha = $this->input->post('input_tipe_usaha');
                if ($tipe_usaha == 'Lainnya') {
                    $lainnya = $this->input->post('input_lainnya');
                }
            }
            $data2 = [
                'nama_instansi' => $this->input->post('input_instansi'),
                'jabatan' => $this->input->post('input_jabatan'),
                'tgl_mulai' => $this->input->post('input_tanggal_mulai'),
                'tipe_usaha' => $tipe_usaha,
                'lainnya' => $lainnya,
                'alamat' => $this->input->post('alamat_instansi'),
                'kode_provinsi' => ($this->input->post('provinsi_instansi') ? $this->input->post('provinsi_instansi') : null),
                'kode_kab_kota' => ($this->input->post('kabupaten_instansi') ? $this->input->post('kabupaten_instansi') : null),
                'kode_kecamatan' => ($this->input->post('kecamatan_instansi') ? $this->input->post('kecamatan_instansi') : null),
                'last_modified_by' => $this->session->userdata("tracer_userId")
            ];

            if($this->input->post('chek_riwayat') == 'riwayat_baru'){
                $data2['id_job'] = getUUID();
                $data2['id_alumni'] = $id_alumni;
                $this->alumni->simpan_alumni_jobs($data2);
                $dbError[] = $this->db->error();
            }else if ($alumni['id_ref_status_alumni'] == $this->input->post('input_status_alumni')) {
                $this->alumni->edit_alumni_jobs($data2, array('id_job' => $this->input->post('id_job_study')));
                $dbError[] = $this->db->error();
            } else {
                $data2['id_job'] = getUUID();
                $data2['id_alumni'] = $id_alumni;
                $this->alumni->simpan_alumni_jobs($data2);
                $dbError[] = $this->db->error();
            }
        }
        if ($this->input->post('input_status_alumni') == '4') {
            $data2 = [
                'tgl_mulai' => $this->input->post('input_tanggal_mulai_study'),
                'nama_perguruan_tinggi' => $this->input->post('input_univ_baru'),
                'prodi' => $this->input->post('input_prodi_baru'),
                'tgl_mulai' => date("Y-m-d"),
                'kode_provinsi' => ($this->input->post('provinsi_instansi2') ? $this->input->post('provinsi_instansi2') : null),
                'kode_kab_kota' => ($this->input->post('kabupaten_instansi2') ? $this->input->post('kabupaten_instansi2') : null),
                'kode_kecamatan' => ($this->input->post('kecamatan_instansi2') ? $this->input->post('kecamatan_instansi2') : null),
                'last_modified_by' => $this->session->userdata("tracer_userId")
            ];

            if($this->input->post('chek_riwayat') == 'riwayat_baru'){
                $data2['id_study'] = getUUID();
                $data2['id_alumni'] = $id_alumni;
                $this->alumni->simpan_alumni_study($data2);
                $dbError[] = $this->db->error();
            } else if ($alumni['id_ref_status_alumni'] == $this->input->post('input_status_alumni')) {
                $this->alumni->edit_alumni_study($data2, array('id_study' => $this->input->post('id_job_study')));
                $dbError[] = $this->db->error();
            }else{
                $data2['id_study'] = getUUID();
                $data2['id_alumni'] = $id_alumni;
                $this->alumni->simpan_alumni_study($data2);
                $dbError[] = $this->db->error();
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal mengubah alumni',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengubah alumni',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    
    /**
     * importExcel
     * fitur import data alumni dari file excel
     * @return void
     */
    public function importExcel()
    {
        $return = array();
        if (!isset($_FILES['import_file']['name']) && $_FILES['import_file']['name'] == "" && empty($_FILES['import_file'])) {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";

            goto End;
        }

        $file_import = $_FILES['import_file']['tmp_name'];
        $objPHPExcel = PHPExcel_IOFactory::load($file_import);
        $sheet = $objPHPExcel->getActiveSheet(0);
        $lastRow = $sheet->getHighestRow();

        $email = '';

        if (!$this->checkHeader($sheet)) {
            $return['success'] = false;
            $return['text'] = "Header tidak sesuai";

            goto End;
        }

        $checkData = $this->checkData($sheet, $lastRow);
        if (!$checkData['success']) {
            $return['success'] = false;
            $return['text'] = $checkData['message'];

            goto End;
        }

        $this->db->trans_begin();

        $dbError = [];

        for ($row = 2; $row <= $lastRow; $row++) {
            $id_alumni = getUUID();
            $univ = $this->referensi->getListUniv($sheet->getCell("Q" . $row)->getValue());

            if ($sheet->getCell("I" . $row)->getValue() == '') {
                $email = $sheet->getCell("A" . $row)->getValue() . '@' . str_replace(' ', '', $univ['nama_pendek']) . '.id';
            } else {
                $email = $sheet->getCell("I" . $row)->getValue();
            }

            $alumni = array(
                'id_alumni'             => $id_alumni,
                'nim'                   => $sheet->getCell("A" . $row)->getValue(),
                'nama'                  => $sheet->getCell("B" . $row)->getValue(),
                'nik'                   => $sheet->getCell("C" . $row)->getValue(),
                'tempat_lahir'          => $sheet->getCell("D" . $row)->getValue(),
                'tgl_lahir'             => $sheet->getCell("E" . $row)->getValue(),
                'id_agama'              => $sheet->getCell("F" . $row)->getValue(),
                'jenis_kelamin'         => $sheet->getCell("G" . $row)->getValue(),
                'no_hp'                 => $sheet->getCell("H" . $row)->getValue(),
                'email'                 => $email,
                'alamat'                => $sheet->getCell("J" . $row)->getValue(),
                'kode_prov'             => $sheet->getCell("K" . $row)->getValue(),
                'kode_kab_kota'         => $sheet->getCell("L" . $row)->getValue(),
                'kode_kecamatan'        => $sheet->getCell("M" . $row)->getValue(),
                'url_photo'             => $sheet->getCell("N" . $row)->getValue(),
                'tahun_lulus'           => $sheet->getCell("O" . $row)->getValue(),
                'ipk_terakhir'          => $sheet->getCell("P" . $row)->getValue(),
                'id_perguruan_tinggi'   => $sheet->getCell("Q" . $row)->getValue(),
                'id_prodi'              => $sheet->getCell("R" . $row)->getValue(),
                'judul_skripsi'         => $sheet->getCell("T" . $row)->getValue(),
                'id_ref_status_alumni'  => $sheet->getCell("U" . $row)->getValue(),
                'is_active'             => '1',
                'status_data'           => '1',
                'last_action_user'      => 'CREATE',
                'last_modified_user' => $this->session->userdata('tracer_userId'),
            );

            $users = array(
                'username'  => $sheet->getCell("A" . $row)->getValue(),
                'email'     => $email,
                'real_name' => $sheet->getCell("B" . $row)->getValue(),
                'id_group'  => 4,
                'password'  => hash('sha256', $sheet->getCell("A" . $row)->getValue()),
            );

            $id_user = $this->alumni->simpan_user($users);
            $dbError[] = $this->db->error();

            $alumni['id_user'] = $id_user;

            $simpanAlumni = $this->alumni->simpan_alumni($alumni);
            $dbError[] = $this->db->error();

            if ($sheet->getCell("U" . $row)->getValue() == 1 || $sheet->getCell("U" . $row)->getValue() == 3) {
                $jobStudy = array(
                    'id_job'                => getUUID(),
                    'id_alumni'             => $id_alumni,
                    'id_ref_status_alumni'  => $sheet->getCell("U" . $row)->getValue(),
                    'sektor'                => $sheet->getCell("V" . $row)->getValue(),
                    'kode_provinsi'         => null,
                    'kode_kab_kota'         => null,
                    'kode_kecamatan'        => null,
                    'created_by'            => $this->session->userdata('tracer_userId'),
                    'last_modified_by'      => $this->session->userdata('tracer_userId')
                );

                $simpanJobs = $this->alumni->simpan_alumni_jobs($jobStudy);
                $dbError[] = $this->db->error();
            }

            if ($sheet->getCell("U" . $row)->getValue() == 4) {
                $jobStudy = array(
                    'id_study'          => getUUID(),
                    'id_alumni'         => $id_alumni,
                    'sektor_prodi'      => $sheet->getCell("V" . $row)->getValue(),
                    'kode_provinsi'     => null,
                    'kode_kab_kota'     => null,
                    'kode_kecamatan'    => null,
                    'created_by'        => $this->session->userdata('tracer_userId'),
                    'last_modified_by'  => $this->session->userdata('tracer_userId')
                );

                $simpanStudy = $this->alumni->simpan_alumni_study($jobStudy);
                $dbError[] = $this->db->error();
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $return['success'] = false;
            $return['text'] = 'Gagal menyimpan alumni';
            $return['dbError'] = $dbError;

            goto End;
        };
        $this->db->trans_commit();

        $return['success'] = true;
        $return['text'] = 'Berhasil menyimpan alumni';

        End:
        echo json_encode($return);
    }
    
    /**
     * checkHeader
     * pengecekan header untuk fitur import
     * @param  mixed $sheet
     * @return void
     */
    public function checkHeader($sheet)
    {
        $header = ['NIM/NIRM/NIS', 'Nama', 'NIK', 'Tempat lahir', 'Tanggal lahir', 'Agama', 'Jenis Kelamin', 'Nomor HP', 'Email', 'Alamat', 'Provinsi', 'Kabupaten/Kota', 'Kecamatan', 'Upload Foto', 'Tahun Lulus', 'IPK Terakhir', 'Asal Kampus/SMK', 'Prodi/Jurusan', 'Jalur Masuk', 'Judul Tugas Akhir', 'Status Alumni', 'Sektor'];

        $col = 'A';
        $row = 1;
        $isTrue = true;
        for ($i = 0; $i < count($header); $i++) {
            if ($sheet->getCell(($col++) . $row)->getValue() != $header[$i]) {
                $isTrue = false;
            }
        }
        return $isTrue;
    }
    
    /**
     * checkdata
     * pengecekan data untuk fitur import
     * @param  mixed $sheet
     * @param  mixed $lastRow
     * @return void
     */
    public function checkdata($sheet, $lastRow)
    {
        $return['success'] = true;
        $return['message'] = '';
        $header = ['NIM/NIRM/NIS', 'Nama', 'NIK', 'Tempat lahir', 'Tanggal lahir', 'Agama', 'Jenis Kelamin', 'Nomor HP', 'Email', 'Alamat', 'Provinsi', 'Kabupaten/Kota', 'Kecamatan', 'Upload Foto', 'Tahun Lulus', 'IPK Terakhir', 'Asal Kampus/SMK', 'Prodi/Jurusan', 'Jalur Masuk', 'Judul Tugas Akhir', 'Status Alumni', 'Sektor'];
        $jenis_kelamin = ['L', 'P'];
        $sektorArray = ['Pertanian', 'Non Pertanian'];
        $status_alumni = [1, 3, 4];
        $rowError = array();
        $duplicate = array();

        for ($i = 0; $i < count($header); $i++) {
            $rowError[$i] = '';
            $duplicate[$i] = '';
        }

        for ($row = 2; $row <= $lastRow; $row++) {
            $nim = $sheet->getCell('A' . $row)->getValue();
            $nama = $sheet->getCell('B' . $row)->getValue();
            $nik = $sheet->getCell('C' . $row)->getValue();
            $tempatLahir = $sheet->getCell('D' . $row)->getValue();
            $tanggalLahir = \PHPExcel_Style_NumberFormat::toFormattedString($sheet->getCell('E' . $row)->getValue(), 'YYYY-MM-DD');
            $agama = $sheet->getCell('F' . $row)->getValue();
            $jenisKelamin = $sheet->getCell('G' . $row)->getValue();
            $noHp = $sheet->getCell('H' . $row)->getValue();
            $email = $sheet->getCell('I' . $row)->getValue();
            $alamat = $sheet->getCell('J' . $row)->getValue();
            $provinsi = $sheet->getCell('K' . $row)->getValue();
            $kabupaten = $sheet->getCell('L' . $row)->getValue();
            $kecamatan = $sheet->getCell('M' . $row)->getValue();
            $urlPhoto = $sheet->getCell('N' . $row)->getValue();
            $tahunLulus = $sheet->getCell('O' . $row)->getValue();
            $ipk = $sheet->getCell('P' . $row)->getValue();
            $perguruanTinggi = $sheet->getCell('Q' . $row)->getValue();
            $prodi = $sheet->getCell('R' . $row)->getValue();
            $jalurMasuk = $sheet->getCell('S' . $row)->getValue();
            $judul = $sheet->getCell('T' . $row)->getValue();
            $status = $sheet->getCell('U' . $row)->getValue();
            $sektor = $sheet->getCell('V' . $row)->getValue();

            //NIM/NIRM/NIS
            if (empty($nim) || !$this->whitespace($nim)) {
                $return['success'] = false;

                if ($rowError[0] == '') {
                    $rowError[0] .= $row;
                } else {
                    $rowError[0] .= ', ' . $row;
                }
            }

            //NIM/NIRM/NIS DUPLICATE
            if (!$this->alumni->checkAlumni(array('nim' => $nim)) || !$this->alumni->checkUser(array('username' => $nim)) || !$this->checkDuplicate(['A', $nim, $row], $sheet, $lastRow)) {
                $return['success'] = false;

                if ($duplicate[0] == '') {
                    $duplicate[0] .= $row;
                } else {
                    $duplicate[0] .= ', ' . $row;
                }
            }

            //Nama
            if (empty($nama) || !$this->whitespace($nama) || !$this->validName($nama)) {
                $return['success'] = false;

                if ($rowError[1] == '') {
                    $rowError[1] .= $row;
                } else {
                    $rowError[1] .= ', ' . $row;
                }
            }

            //NIK
            if (!empty($nik) && !$this->validNik($nik)) {
                $return['success'] = false;

                if ($rowError[2] == '') {
                    $rowError[2] .= $row;
                } else {
                    $rowError[2] .= ', ' . $row;
                }
            }

            //NIK DUPLICATE
            if (!empty($nik) && (!$this->alumni->checkAlumni(array('nik' => $nik)) || !$this->checkDuplicate(['C', $nik, $row], $sheet, $lastRow))) {
                $return['success'] = false;

                if ($duplicate[2] == '') {
                    $duplicate[2] .= $row;
                } else {
                    $duplicate[2] .= ', ' . $row;
                }
            }

            //Tempat lahir
            if (!empty($tempatLahir) && !$this->notOnlyNumber($tempatLahir)) {
                $return['success'] = false;

                if ($rowError[3] == '') {
                    $rowError[3] .= $row;
                } else {
                    $rowError[3] .= ', ' . $row;
                }
            }

            //Tanggal lahir
            if (!empty($tanggalLahir) && (!$this->validDate($tanggalLahir) || !$this->checkDate($tanggalLahir))) {
                $return['success'] = false;

                if ($rowError[4] == '') {
                    $rowError[4] .= $row;
                } else {
                    $rowError[4] .= ', ' . $row;
                }
            }

            //Agama
            if (!empty($agama) && !$this->alumni->checkAgama(array('id_agama' => $agama))) {
                $return['success'] = false;

                if ($rowError[5] == '') {
                    $rowError[5] .= $row;
                } else {
                    $rowError[5] .= ', ' . $row;
                }
            }

            //Jenis Kelamin
            if (empty($jenisKelamin) || !in_array($jenisKelamin, $jenis_kelamin)) {
                $return['success'] = false;

                if ($rowError[6] == '') {
                    $rowError[6] .= $row;
                } else {
                    $rowError[6] .= ', ' . $row;
                }
            }

            //Nomor Hp
            if (!empty($noHp) && !$this->validHp($noHp)) {
                $return['success'] = false;

                if ($rowError[7] == '') {
                    $rowError[7] .= $row;
                } else {
                    $rowError[7] .= ', ' . $row;
                }
            }

            //Email
            if (!empty($email) && !$this->validEmail($email)) {
                $return['success'] = false;

                if ($rowError[8] == '') {
                    $rowError[8] .= $row;
                } else {
                    $rowError[8] .= ', ' . $row;
                }
            }

            //EMAIL DUPLICATE
            if (!empty($email) && (!$this->alumni->checkAlumni(array('email' => $email)) || !$this->alumni->checkUser(array('email' => $email)) || !$this->checkDuplicate(['I', $email, $row], $sheet, $lastRow))) {
                $return['success'] = false;

                if ($duplicate[8] == '') {
                    $duplicate[8] .= $row;
                } else {
                    $duplicate[8] .= ', ' . $row;
                }
            }

            //Alamat
            if (!empty($alamat) && !$this->notOnlyNumber($alamat)) {
                $return['success'] = false;

                if ($rowError[9] == '') {
                    $rowError[9] .= $row;
                } else {
                    $rowError[9] .= ', ' . $row;
                }
            }

            //Provinsi
            if (empty($provinsi) || !$this->alumni->checkProvinsi(array('kode_provinsi' => $provinsi))) {
                $return['success'] = false;

                if ($rowError[10] == '') {
                    $rowError[10] .= $row;
                } else {
                    $rowError[10] .= ', ' . $row;
                }
            }

            //Kabupaten/Kota
            if (empty($kabupaten) || !$this->alumni->checkKabupaten(array('kode_provinsi' => $provinsi, 'kode_kab_kota' => $kabupaten))) {
                $return['success'] = false;

                if ($rowError[11] == '') {
                    $rowError[11] .= $row;
                } else {
                    $rowError[11] .= ', ' . $row;
                }
            }

            //Kecamatan
            if (!empty($kecamatan) && !$this->alumni->checkKecamatan(array('kode_kab_kota' => $kabupaten, 'kode_kecamatan' => $kecamatan))) {
                $return['success'] = false;

                if ($rowError[12] == '') {
                    $rowError[12] .= $row;
                } else {
                    $rowError[12] .= ', ' . $row;
                }
            }

            //Upload Foto
            if (!empty($urlPhoto) && !$this->validUrl($urlPhoto)) {
                $return['success'] = false;

                if ($rowError[13] == '') {
                    $rowError[13] .= $row;
                } else {
                    $rowError[13] .= ', ' . $row;
                }
            }

            //Tahun Lulus
            if (empty($tahunLulus) || !$this->alumni->checkTahun(array('tahun' => $tahunLulus))) {
                $return['success'] = false;

                if ($rowError[14] == '') {
                    $rowError[14] .= $row;
                } else {
                    $rowError[14] .= ', ' . $row;
                }
            }

            //IPK Terakhir
            if (!empty($ipk) && !$this->validIpk($ipk)) {
                $return['success'] = false;

                if ($rowError[15] == '') {
                    $rowError[15] .= $row;
                } else {
                    $rowError[15] .= ', ' . $row;
                }
            }

            //Asal Kampus/Sekolah
            if (empty($perguruanTinggi) || !$this->alumni->checkPerguruanTinggi(array('id_perguruan_tinggi' => $perguruanTinggi))) {
                $return['success'] = false;

                if ($rowError[16] == '') {
                    $rowError[16] .= $row;
                } else {
                    $rowError[16] .= ', ' . $row;
                }
            }

            //Program Studi/Jurusan
            if (empty($prodi) || !$this->alumni->checkProdi(array('id_perguruan_tinggi' => $perguruanTinggi, 'id_prodi' => $prodi))) {
                $return['success'] = false;

                if ($rowError[17] == '') {
                    $rowError[17] .= $row;
                } else {
                    $rowError[17] .= ', ' . $row;
                }
            }

            //Judul Tugas Akhir
            if (!empty($judul) && !$this->notOnlyNumber($judul)) {
                $return['success'] = false;

                if ($rowError[19] == '') {
                    $rowError[19] .= $row;
                } else {
                    $rowError[19] .= ', ' . $row;
                }
            }

            //Status Alumni
            if (empty($status) || !$this->alumni->checkStatus(array('id' => $status))) {
                $return['success'] = false;

                if ($rowError[20] == '') {
                    $rowError[20] .= $row;
                } else {
                    $rowError[20] .= ', ' . $row;
                }
            }

            //Sektor
            if (in_array($status, $status_alumni) && (empty($sektor) || !in_array($sektor, $sektorArray))) {
                $return['success'] = false;

                if ($rowError[21] == '') {
                    $rowError[21] .= $row;
                } else {
                    $rowError[21] .= ', ' . $row;
                }
            }
        }

        if (!$return['success']) {
            if (!empty($rowError)) {
                $return['message'] .= '<i class="fa fa-triangle-exclamation"></i> <b>Data tidak sesuai template</b> : <br>';
                $return['message'] .= '<ul>';
                for ($i = 0; $i < count($rowError); $i++) {
                    if (!empty($rowError[$i])) {
                        $return['message'] .= '<li>' . $header[$i] . ' pada baris ke ' . $rowError[$i] . '</li>';
                    }
                }
                $return['message'] .= '</ul>';
            }
            if (!empty($duplicate)) {
                $return['message'] .= '<i class="fa fa-triangle-exclamation"></i> <b>Data ganda</b> : <br>';
                $return['message'] .= '<ul>';
                for ($i = 0; $i < count($duplicate); $i++) {
                    if (!empty($duplicate[$i])) {
                        $return['message'] .= '<li>' . $header[$i] . ' pada baris ke ' . $duplicate[$i] . '</li>';
                    }
                }
                $return['message'] .= '</ul>';
            }
        }

        return $return;
    }

    public function checkDuplicate($data, $sheet, $lastRow)
    {
        for ($row = 2; $row <= $lastRow; $row++) {
            if ($data[2] != $row && $sheet->getCell($data[0] . $row)->getValue() == $data[1]) {
                return false;
            }
        }

        return true;
    }
    
    /**
     * callPhoto
     * menampilkan preview foto pada tabel grocery crud
     * @param  mixed $value
     * @param  mixed $rows
     * @return void
     */
    public function callPhoto($value, $rows)
    {
        $return = '-';
        if ($value != null || $value != "") {
            if (file_exists($value)) {
                $return = '<img src="' . base_url() . $value . '" class="img img-fluid" width="100px" alt="url_photo">';
            }
        }
        return $return;
    }
    public function callTgl($value, $rows)
    {
        $return = $value;
        if ($value == '0000-00-00' || $value == "0000/00/00") {
            $return = '-';
        }
        return $return;
    }
    
    /**
     * getKabKota
     * memperoleh data kab/kota berdasarkan provinsi
     * @return void
     */
    public function getKabKota()
    {
        $kode_provinsi = $this->input->get('kode_provinsi');
        $kabKota = setKabupaten($kode_provinsi);
        echo json_encode($kabKota);
    }
    
    /**
     * getKecamatan
     * memperoleh data kecamatan berdasarkan kab/kota
     * @return void
     */
    public function getKecamatan()
    {
        $kode_kab_kota = $this->input->get('kode_kab_kota');
        $kecamatan = setKecamatan($kode_kab_kota);
        echo json_encode($kecamatan);
    }
    
    /**
     * getProdi
     * memperoleh data prodi berdasarkan fakultas
     * @return void
     */
    public function getProdi()
    {
        $id_fakultas = $this->input->get('kode_prodi');
        $prodi = setProdi($id_fakultas);
        echo json_encode($prodi);
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('input_nama', 'Nama lengkap', 'trim|required|max_length[60]|callback_whitespace');
        $this->form_validation->set_rules('input_nim', 'NIM/NIRM/NIS', 'trim|required|min_length[3]|max_length[20]' . ($this->onUpdate ? '|callback_checkAlumni[nim]|callback_checkUser[username]' : '|is_unique[alumni.nim]|is_unique[user.username]'));
        $this->form_validation->set_rules('input_nik', 'NIK', 'trim|min_length[16]|max_length[16]|required' . ($this->onUpdate ? '|callback_checkAlumni[nik]' : '|is_unique[alumni.nik]'));
        $this->form_validation->set_rules('input_no_hp', 'No HP', 'trim' . ($this->input->post('input_no_hp') ? '|max_length[13]|callback_validHp' : ''));
        $this->form_validation->set_rules('input_email', 'Email', 'trim|valid_email|max_length[50]|required' . ($this->onUpdate ? '|callback_checkUser[email]|callback_checkAlumni[email]' : '|is_unique[alumni.email]|is_unique[user.email]'));
        $this->form_validation->set_rules('input_tempat_lahir', 'Tempat lahir', 'trim|callback_whitespace|required');
        $this->form_validation->set_rules('input_tanggal_lahir', 'Tanggal lahir', 'trim|callback_checkDate|required');
        $this->form_validation->set_rules('input_agama', 'Agama', 'trim|required');
        $this->form_validation->set_rules('input_jk', 'Jenis kelamin', 'trim|required');
        $this->form_validation->set_rules('input_alamat', 'Alamat', 'trim|max_length[255]|callback_whitespace|callback_notOnlyNumber');
        $this->form_validation->set_rules('input_provinsi', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('input_kabupaten', 'Kabupaten/Kota', 'trim|required');
        $this->form_validation->set_rules('input_kecamatan', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('input_tahun_lulus', 'Tahun Lulus', 'trim|required');
        $this->form_validation->set_rules('input_fakultas', 'Fakultas', 'trim|required');
        $this->form_validation->set_rules('input_prodi', 'Program Studi/Jurusan', 'trim|required');
        $this->form_validation->set_rules('input_ipk_terakhir', 'IPK terakhir', 'trim|callback_lessEqual[4]');
        $this->form_validation->set_rules('input_skripsi', 'Judul tugas akhir', 'trim|callback_whitespace|callback_notOnlyNumber');
        $this->form_validation->set_rules('input_status_alumni', 'Status alumni', 'trim|required');

        if ($this->input->post('input_jk') == null) {
            $errors[] = [
                'field'   => 'input_jk',
                'message' => 'Jenis kelamin belum diisi',
            ];
        }
        if ($this->input->post('input_status_alumni') == '1' || $this->input->post('input_status_alumni') == '3') {
            $this->form_validation->set_rules('input_instansi', 'Nama tempat kerja', 'trim|callback_whitespace|callback_notOnlyNumber');
            $this->form_validation->set_rules('input_jabatan', 'Jabatan', 'trim|callback_whitespace|callback_notOnlyNumber');
        }

        if ($this->input->post('input_status_alumni') == '4') {
            $this->form_validation->set_rules('input_univ_baru', 'Nama perguruan tinggi', 'trim|callback_whitespace|callback_notOnlyNumber');
            $this->form_validation->set_rules('input_jabatan', 'Nama prodi', 'trim|callback_whitespace|callback_notOnlyNumber');
        }

        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('lessEqual', '{field} harus kurang atau sama dengan {param}.00');
        $this->form_validation->set_message('checkAlumni', '{field} sudah terdaftar');
        $this->form_validation->set_message('checkUser', '{field} sudah terdaftar');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');
        $this->form_validation->set_message('validHp', '{field} diawali dengan kode negara (08), minimal 11 digit dan maksimal 13 digit');
        $this->form_validation->set_message('alphaOnly', '{field} hanya boleh huruf atau spasi');
        $this->form_validation->set_message('xss_clean', '{field} tidak boleh memasukkan script');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function lessEqual($str)
    {
        if ($str >= 0 && $str <= 4.00) {
            return true;
        }

        return false;
    }

    public function validIpk($str)
    {
        if (preg_match('/^[1-9]\d*(\.\d{0,2})?$/', $str) && (float)$str >= 0 && (float)$str <= 4.00) {
            return true;
        }

        return false;
    }

    public function checkAlumni($str, $col)
    {
        if ($str == "") {
            return true;
        }
        return $this->alumni->checkAlumni(array($col => $str), $this->input->post('id_alumni'));
    }

    public function checkUser($str, $col)
    {
        if ($str == "") {
            return true;
        }
        $id_user = $this->alumni->getAlumni($this->input->post('id_alumni'))['id_user'];
        return $this->alumni->checkUser(array($col => $str), $id_user);
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }
    public function notOnlyNumber($str)
    {
        if (ctype_digit($str)) {
            return false;
        }
        return true;
    }

    public function validHp($str)
    {
        if (preg_match('/^(\+62|62|0)8[1-9][0-9]{6,10}$/', $str)) {
            return true;
        }

        return false;
    }

    public function alphaOnly($str)
    {
        if (!preg_match('/^[a-zA-Z ]*$/', $str)) {
            return false;
        }

        return true;
    }
    public function validName($str)
    {
        if (!preg_match('/^[a-zA-Z -`’]*$/', $str)) {
            return false;
        }

        return true;
    }

    public function numberOnly($str)
    {
        if (!preg_match('/\D/', $str)) {
            return false;
        }

        return true;
    }

    public function checkDate($date)
    {
        $current = date('Y-m-d');
        if ($date > $current) {
            return false;
        }
        return true;
    }

    function validDate($date)
    {
        $date = DateTime::createFromFormat('Y-m-d', $date);
        $date_errors = DateTime::getLastErrors();
        if ($date_errors['warning_count'] + $date_errors['error_count'] > 0) {
            return false;
        }

        return true;
    }

    function validEmail($email)
    {
        if (preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i', $email)) {
            return true;
        }

        return false;
    }

    function validUrl($url)
    {
        if (preg_match('/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $url)) {
            return true;
        }

        return false;
    }

    function validNik($nik)
    {
        if (preg_match('/^\d{10,18}$/', $nik)) {
            return true;
        }

        return false;
    }
}
