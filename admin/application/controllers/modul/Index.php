<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "modul";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_sistem','sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-database';
        $this->data['title'] = 'Module System';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Modul System');

        $crud->setTable('modul_sistem');

        $crud->columns([
            'nama_modul',
            'keterangan',
            'is_active',
        ]);

        $crud->displayAs([
            'nama_modul' => 'Nama Modul',
            'keterangan' => 'Keterangan',
            'is_active' => 'Aktif'
        ]);

        $crud->addFields([
            'nama_modul',
            'keterangan',
        ]);

        $crud->editFields([
            'nama_modul',
            'keterangan',
        ]);

        $crud->requiredFields([
            'nama_modul',
            'keterangan',
        ]);

        $crud->fieldType('is_active', 'dropdown', [
            '0' => 'Tidak Aktif',
            '1' => 'Aktif'
        ]);

        $output = $crud->render();
        $this->_setOutput('modul/index/index', $output);
    }

}
