<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "perguruan_tinggi";
    private $id = "";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_universitas', 'pt');
        $this->load->model('M_sistem', 'sistem');
        $this->load->model('M_referensi', 'refrensi');
        $this->load->model('M_user', 'user');
    }

    public function index()
    {
        $this->data['title'] = 'Perguruan Tinggi';
        $this->data['is_home'] = false;
        $this->data['logo_title'] = $this->sistem->logo_title();
        $this->data['logo_utama'] = $this->sistem->logo_utama();
        $this->data['about_us'] = $this->sistem->about_us();
        $this->data['copyright'] = $this->sistem->copyright();
        $this->data['video_perkenalan'] = $this->sistem->video_perkenalan();
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $this->data['sistem'] = $this->dataSistem();
        $this->data['perguruan_tinggi'] = $perguruan =  $this->refrensi->getUniv();
        $this->data['propinsi'] = $propinsi = setPronvinsi();
        $this->data['kab_kota'] = $kab = setKabupaten($perguruan->kode_provinsi);
        $user_id = $this->session->userdata("tracer_userId");
        $this->data['user'] = $user = $this->user->getUser($user_id);
        if ($this->session->userdata("tracer_idGroup") == 2) {
            $this->settingFakultas($user['id_fakultas']);
        } else {
            $this->settingFakultas();
        }

        foreach ($propinsi as $key => $value) {
            if ($perguruan->kode_provinsi == $key) {
                $this->data['propinsiProfil'] = $value;
            }
        }

        foreach ($kab as $key => $value) {
            if ($perguruan->kode_kab_kota == $key) {
                $this->data['kabProfil'] = $value;
            }
        }
        $this->render('index');
    }
    
    /**
     * settingFakultas
     * menampilkan data daftar fakultas
     * @param  mixed $id_fakultas
     * @return void
     */
    public function settingFakultas($id_fakultas = null)
    {
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Fakultas');
        $crud->setTable('ref_fakultas');

        $column = ['kode_fakultas', 'nama_fakultas', 'singkatan'];
        $fields = ['kode_fakultas', 'nama_fakultas', 'singkatan'];
        $requireds = ['kode_fakultas', 'nama_fakultas', 'singkatan'];
        $fieldsDisplay = [
            'kode_fakultas' => 'Kode Fakultas',
            'nama_fakultas' => 'Nama Fakultas',
            'singkatan' => 'Singkatan',
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);
        $crud->editFields($fields);

        if ($id_fakultas) {
            $crud->where(['id_fakultas' => $id_fakultas]);
            $crud->unsetAdd();
        }

        $crud->callbackDelete(array($this, '_callDelete'));
        $crud->callbackBeforeInsert(array($this, 'addFakultas'));
        $crud->callbackBeforeUpdate(array($this, 'editFakultas'));
        $crud->setActionButton('Daftar Prodi', 'fa fa-book', function ($row) {
            return site_url('perguruan_tinggi/index/daftarprodi/' . $row->id_fakultas);
        }, false);
        $output = $crud->render();
        $this->_setOutput('perguruan_tinggi/fakultas', $output);
    }
    
    /**
     * daftarprodi
     * menampilkan data list prodi per fakultas
     * @param  mixed $id_fakultas
     * @return void
     */
    public function daftarprodi($id_fakultas)
    {
        $this->data['title'] = 'Daftar Program Studi';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $this->data['back'] = ' &nbsp/ Daftar Fakultas';
        $this->data['url_back'] = 'perguruan_tinggi/index';
        $this->id = $id_fakultas;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Prodi');
        $crud->setTable('ref_prodi');
        $crud->setRelation('id_fakultas', 'ref_fakultas', 'nama_fakultas');
        $crud->where(['id_fakultas' => $id_fakultas]);
        $column = ['kode_prodi', 'id_fakultas', 'nama_prodi', 'singkatan'];
        $fields = ['kode_prodi', 'nama_prodi', 'singkatan'];
        $requireds = ['kode_prodi', 'nama_prodi', 'singkatan'];
        $fieldsDisplay = [
            'kode_prodi' => 'Kode Prodi',
            'id_fakultas' => 'Fakultas',
            'nama_prodi' => 'Nama Prodi',
            'singkatan' => 'Singkatan',
        ];

        $crud->callbackBeforeInsert(array($this, 'addProdi'));
        $crud->callbackBeforeUpdate(array($this, 'editProdi'));
        $crud->callbackDelete(array($this, '_callDeleteProdi'));
        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);
        $crud->unsetSearchColumns(['id_fakultas']);
        $crud->fieldType('kode_prodi', 'int');


        $output = $crud->render();
        $this->_setOutput('perguruan_tinggi/prodi', $output);
    }
    
    /**
     * editProdi
     * mengedit data prodi
     * @param  mixed $prodi
     * @return void
     */
    public function editProdi($prodi)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $id_fakultas = $this->id;
        $cek = $this->pt->cekProdiInput($prodi->data, $id_fakultas, true);

        if ($cek['status'] == 500) {
            return $errorMessage->setMessage($cek['message']);
        }        
        return $prodi;
    }
    
    /**
     * addProdi
     * menambahkan data prodi
     * @param  mixed $prodi
     * @return void
     */
    public function addProdi($prodi)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $id_fakultas = $this->id;
        $cek = $this->pt->cekProdiInput($prodi->data, $id_fakultas, false);

        if ($cek['status'] == 500) {
            return $errorMessage->setMessage($cek['message']);
        }

        $id_prodi = getUUID();
        $prodi->data['id_prodi'] = $id_prodi;
        $prodi->data['id_fakultas'] = $id_fakultas;
        return $prodi;
    }
    
    /**
     * editFakultas
     * mengedit data fakultas
     * @param  mixed $fakultas
     * @return void
     */
    public function editFakultas($fakultas)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $cek = $this->pt->cekFakultas($fakultas->data, true);
        if ($cek['status'] == 500) {
            return $errorMessage->setMessage($cek['message']);
        }
        return $fakultas;
    }    
    /**
     * addFakultas
     * menambahkan data fakultas
     * @param  mixed $fakultas
     * @return void
     */
    public function addFakultas($fakultas)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $cek = $this->pt->cekFakultas($fakultas->data, false);
        if ($cek['status'] == 500) {
            return $errorMessage->setMessage($cek['message']);
        }

        $id_fakultas = getUUID();
        $fakultas->data['id_fakultas'] = $id_fakultas;
        return $fakultas;
    }
    
    /**
     * _callDelete
     * menghapus data fakultas
     * @param  mixed $id
     * @return void
     */
    public function _callDelete($id)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $fakultas = $id->primaryKeyValue;

        $cekAlumni = $this->pt->checkAlumni($fakultas);
        $cekProdi = $this->pt->checkProdi($fakultas);

        if ($cekAlumni['status'] == 500) {
            return $errorMessage->setMessage($cekAlumni['message']);
        }

        if ($cekProdi['status'] == 500) {
            return $errorMessage->setMessage($cekProdi['message']);
        }
        if($this->pt->deleteFakultas($fakultas)){
            return true;
        }        
        return false;
    }
    
    /**
     * _callDeleteProdi
     * menghapus data prodi
     * @param  mixed $id
     * @return void
     */
    public function _callDeleteProdi($id)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $prodi = $id->primaryKeyValue;

        $cekAlumni = $this->pt->checkAlumniProdi($prodi);

        if ($cekAlumni['status'] == 500) {
            return $errorMessage->setMessage($cekAlumni['message']);
        }

        if($this->pt->deleteProdi($prodi)){
            return true;
        }        
        return false;
    }

    public function addPerguruanTinggi()
    {
        $this->data['title'] = 'Tambah Perguruan Tinggi';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $this->render('tambah');
    }
    
    /**
     * ubahPerguruanTinggi
     * mengubah data perguruan tinggi
     * @return void
     */
    public function ubahPerguruanTinggi()
    {
        $this->data['title'] = 'Edit Perguruan Tinggi';
        $this->data['is_home'] = false;
        $this->data['data'] = $this->pt->getPerguruanTinggi(array('id_perguruan_tinggi' => $this->input->get('key')));
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $this->render('edit');
    }

    public function callPhoto($value, $rows)
    {
        $return = '-';
        if ($value != null || $value != "") {
            if (file_exists($value)) {
                $return = '<img src="' . base_url() . $value . '" class="img img-fluid" width="100px" alt="Logo universitas">';
            }
        }
        return $return;
    }

    public function detail()
    {
        $this->id = $this->input->get("key");
        $this->data['title'] = 'Program Studi Dari Perguruan Tinggi';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Program Studi');
        $crud->setTable('ref_prodi');

        $crud->where([
            'ref_prodi.id_perguruan_tinggi = ?' => $this->id
        ]);

        $column = ['kode_prodi', 'nama_prodi', 'singkatan'];
        $fields = ['nama_prodi', 'singkatan'];
        $requireds = ['nama_prodi', 'singkatan'];
        $fieldsDisplay = [
            'kode_prodi' => 'Kode Prodi',
            'nama_prodi' => 'Nama Prodi',
            'singkatan' => 'Singkatan',
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);
        $crud->uniqueFields(['kode_prodi']);

        $crud->callbackBeforeInsert(function ($stateParameters) {
            $stateParameters->data['id_prodi'] = getUUID();
            $stateParameters->data['id_perguruan_tinggi'] = $this->id;
            return $stateParameters;
        });

        $output = $crud->render();
        $this->_setOutput('perguruan_tinggi/detail', $output);
    }
    
    /**
     * _urlLogo
     * menambahkan file foto ke penyimpanan
     * @param  mixed $id_perguruan_tinggi
     * @param  mixed $oldLogo
     * @return void
     */
    function _urlLogo($id_perguruan_tinggi, $oldLogo)
    {

        $id_perguruan_tinggi = str_replace("-", "", $id_perguruan_tinggi);
        if (!file_exists('public/uploads/perguruan_tinggi/' . $id_perguruan_tinggi)) {
            mkdir('public/uploads/perguruan_tinggi/' . $id_perguruan_tinggi, 0755, true);
            chmod('public/uploads/perguruan_tinggi/', 0755);
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/perguruan_tinggi/' . $id_perguruan_tinggi; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'LOGO' . $id_perguruan_tinggi;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = 'public/uploads/perguruan_tinggi/' . $id_perguruan_tinggi . '/' . $file['file_name'];
                if (file_exists($oldLogo)) {
                    unlink($oldLogo);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang";
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function simpanPerguruanTinggi()
    {
        $input = $this->input->post();

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $user = array(
            'username' => $input['username'],
            'email' => $input['email'],
            'real_name' => $input['nama_pendek'],
            'id_group' => 2,
            'password' => hash('sha256', $input['password'])
        );

        $id_user = $this->pt->simpan_user($user);
        $dbError[] = $this->db->error();

        $id_perguruan_tinggi = getUUID();

        $perguruan_tinggi = array(
            'id_perguruan_tinggi' => $id_perguruan_tinggi,
            'npsn' => $input['npsn'],
            'kode' => $input['kode'],
            'nama_resmi' => $input['nama_resmi'],
            'nama_pendek' => $input['nama_pendek'],
            'alamat' => $input['alamat'],
            'kota' => $input['kota'],
            'no_telp' => $input['no_telp'],
            'kode_pos' => $input['kode_pos'],
            'no_fax' => $input['no_fax'],
            'email' => $input['email'],
            'website' => $input['website'],
            'web_tracer' => ($input['web_tracer'] ? $input['web_tracer'] : null),
            'id_user' => $id_user,
            'last_modified_user' => $this->session->userdata('tracer_userId'),
        );

        $urlLogo = $this->_urlLogo($id_perguruan_tinggi, $input['oldFoto']);
        $perguruan_tinggi['path_logo'] = $urlLogo['file_name'];

        $this->pt->simpan_perguruan_tinggi($perguruan_tinggi);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan perguruan tinggi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan perguruan tinggi',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function editPerguruanTinggi()
    {
        $input = $this->input->post();
        $this->onUpdate = 1;

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $perguruan_tinggi = array(
            'npsn' => $input['input_npsn'],
            'nama_resmi' => $input['input_nama'],
            'nama_pendek' => $input['input_nama_pendek'],
            'kode_provinsi' => $input['input_provinsi'],
            'kode_kab_kota' => $input['input_kab'],
            'alamat' => $input['input_alamat'],
            'no_telp' => $input['input_telepon'],
            'kode_pos' => $input['input_kode'],
            'no_fax' => $input['input_fax'],
            'email' => $input['input_email'],
            'website' => $input['input_link'],
            'last_modified_user' => $this->session->userdata('tracer_userId'),
        );

        if ($_FILES['url_photo']['name']) {
            $urlLogo = $this->_urlLogo($input['id_perguruan_tinggi'], $input['oldLogo']);
            $perguruan_tinggi['path_logo'] = $urlLogo['file_name'];
        } else {
            $perguruan_tinggi['path_logo'] = $input['oldLogo'];
        }

        $this->pt->edit_perguruan_tinggi($perguruan_tinggi, array('id_perguruan_tinggi' => $input['id_perguruan_tinggi']));
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan perguruan tinggi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan perguruan tinggi',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function editAkun()
    {
        $input = $this->input->post();
        $this->onUpdate = 1;

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_validationPassword();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $user = array(
            'username' => $input['input_username'],
            'password'  => hash('sha256', $input['password']),
        );

        $id_user = $this->session->userdata('tracer_userId');
        $this->pt->edit_user($user, array('id_user' => $id_user));
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan perguruan tinggi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan data user',
            ],
            $this->session->sess_destroy()
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('input_npsn', 'NPSN', 'trim|required|exact_length[8]');
        $this->form_validation->set_rules('input_nama', 'Nama Perguruan Tinggi', 'trim|required|max_length[25]|min_length[3]');
        $this->form_validation->set_rules('input_nama_pendek', 'Nama Pendek', 'trim|required|max_length[5]|min_length[3]');
        $this->form_validation->set_rules('input_alamat', 'Alamat', 'trim|required|max_length[255]|min_length[3]');
        $this->form_validation->set_rules('input_telepon', 'No. Telephone', 'trim|required|max_length[13]|callback_validHp');
        $this->form_validation->set_rules('input_kode', 'Kode pos', 'trim|required|exact_length[5]');
        $this->form_validation->set_rules('input_email', 'Email', 'trim|required|valid_email|max_length[40]');
        $this->form_validation->set_rules('input_fax', 'Nomor Fax', 'trim|required|callback_validFax|max_length[10]');
        $this->form_validation->set_rules('input_link', 'Link website', 'trim|required|max_length[50]|min_length[3]|callback_validUrl');
        $this->form_validation->set_rules('input_provinsi', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('input_kab', 'Kabupaten/Kota', 'trim|required');
        if (empty($_FILES['url_photo']['name']) && empty(($_POST['oldLogo']))) {
            $errors[] = [
                'field'   => 'url_photo',
                'message' => 'Foto tidak boleh kosong.',
            ];
        }

        $this->form_validation->set_message('required', '{field} tidak boleh kosong.');
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('exact_length', '{field} wajib {param} digit');
        $this->form_validation->set_message('validUrl', '{field} wajib menggunakan http/https dan domain');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('validFax', '{field} diawali dengan 021');
        $this->form_validation->set_message('validHp', '{field} diawali dengan(08), minimal 10 digit dan maksimal 13 digit');
        $this->form_validation->set_message('checkPT', '{field} sudah terdaftar');
        $this->form_validation->set_message('checkUser', '{field} sudah terdaftar');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    private function _validationPassword()
    {
        $errors = FALSE;
        $this->form_validation->set_rules('input_username', 'Username', 'trim|required|callback_whitespace|max_length[60]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('konfirmasi_password', 'Konfirmasi password', 'trim|required|matches[password]');

        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function validUrl($url)
    {
        $pattern_url = '/^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/';

        if ($url == '' || empty($url) || $url == null) {
            return true;
        }

        if (!preg_match($pattern_url, $url)) {
            return false;
        }

        return true;
    }

    public function valid_email($email)
    {
        $pattern_email = '/^\\S+@\\S+\\.\\S+$/';

        if ($email == '' || empty($email) || $email == null) {
            return true;
        }

        if (!preg_match($pattern_email, $email)) {
            return false;
        }

        return true;
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }

    public function validFax($str)
    {
        if (preg_match('/^(021)[1-9][0-9]{6}$/', $str)) {
            return true;
        }

        return false;
    }

    public function validHp($str)
    {
        if (preg_match('/^(\+62|62|0)8[1-9][0-9]{6,10}$/', $str)) {
            return true;
        }

        return false;
    }

    public function checkPT($str, $col)
    {
        return $this->pt->checkPT(array($col => $str), $this->input->post('id_perguruan_tinggi'));
    }

    // public function checkUser($str, $col)
    // {
    //     $id_user = $this->pt->getPerguruanTinggi(array('id_perguruan_tinggi' => $this->input->post('id_perguruan_tinggi')))->id_user;
    //     return $this->pt->checkUser(array($col => $str), $id_user);
    // }

    public function dataSistem()
    {
        $sistem = $this->sistem->getSistem(array('id' => $this->input->get('key')));

        $sistem = $this->sistem->official_account();

        $sistem->instagram = "";
        $sistem->twitter = "";
        $sistem->youtube = "";

        if ($sistem->deskripsi != "" || !empty($sistem->deskripsi)) {
            $deskripsi = json_decode($sistem->deskripsi);

            $sistem->instagram = ($deskripsi->instagram != "" ? $deskripsi->instagram : "");
            $sistem->twitter = ($deskripsi->twitter != "" ? $deskripsi->twitter : "");
            $sistem->youtube = ($deskripsi->youtube != "" ? $deskripsi->youtube : "");
        }

        return $sistem;
    }
}
