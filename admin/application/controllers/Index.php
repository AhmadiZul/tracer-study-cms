<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_index', 'index');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {
        $this->data['title'] = 'mitra';
        $this->data['is_home'] = true;
        $this->data['provinsi'] = $this->getProvinsi();
        $this->data['kabupaten'] = $this->getKabupaten(11);
        $this->data['selectPolbangtan'] = $this->index->getPolbangtanAll();
        $this->data['selectStatusAlumni'] = $this->index->getStatusALumniAll();
        $this->data['status_alumni'] = $this->getStatusAlumni();
        $this->data['select_provinsi'] = setPronvinsi();
        $this->data['total'] = $this->index->getCountAlumniBy(null);
        $this->data['g_jenis_kelamin'] = $this->getGrafikByJenisKelamin();
        $this->data['g_status_alumni_kelamin'] = $this->getGrafikByStatusAlumniJenisKelamin();
        $this->data['g_provinsi'] = $this->getGrafikByProvinsi();
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('index/index');
    }

    public function getProvinsi()
    {
        $provinsi = setPronvinsi();
        $return = array();

        foreach ($provinsi as $key => $value) {
            $return[] = $value;
        }

        return $return;
    }

    public function getKabupaten($kode_provinsi)
    {
        $kabupaten = setKabupaten($kode_provinsi);
        $return = array();

        foreach ($kabupaten as $key => $value) {
            $return[] = $value;
        }

        return $return;
    }

    public function getStatusAlumni()
    {
        $status = $this->index->getStatusALumniAll();
        $return = array();

        foreach ($status as $key => $value) {
            $return[] = $value->status_alumni;
        }

        return $return;
    }

    public function getGrafikByJenisKelamin()
    {
        $totalL = $this->index->getCountAlumniBy(array('a.jenis_kelamin' => 'L'));
        $totalP = $this->index->getCountAlumniBy(array('a.jenis_kelamin' => 'P'));
        $grafik[0] = array(
            'name' => "LAKI-LAKI",
            'y' => $totalL,
            'color' => 'rgb(85, 161, 255)'
        );
        $grafik[1] = array(
            'name' => "PEREMPUAN",
            'y' => $totalP,
            'color' => 'rgb(234, 63, 95)'
        );
        return $grafik;
    }

    public function getGrafikByStatusAlumniJenisKelamin()
    {
        $grafik = array();
        $totalL = $this->index->countGenderByStatus(array('a.jenis_kelamin' => 'L'));
        $totalP = $this->index->countGenderByStatus(array('a.jenis_kelamin' => 'P'));

        $grafik[0] = array(
            'name' => 'LAKI-LAKI',
            'data' => $totalL,
            'color' => 'rgb(85, 161, 255)'
        );
        $grafik[1] = array(
            'name' => 'PEREMPUAN',
            'data' => $totalP,
            'color' => 'rgb(234, 63, 95)'
        );
        return $grafik;
    }

    public function getGrafikByProvinsi()
    {
        $grafik = array();
            $whereL = array('a.jenis_kelamin' => 'L');
            $whereP = array('a.jenis_kelamin' => 'P');

            $totalL = $this->index->countGenderByProvinsi($whereL);
            $totalP = $this->index->countGenderByProvinsi($whereP);

        $grafik[0] = array(
            'name' => 'LAKI-LAKI',
            'data' => $totalL,
            'color' => 'rgb(85, 161, 255)'
        );
        $grafik[1] = array(
            'name' => 'PEREMPUAN',
            'data' => $totalP,
            'color' => 'rgb(234, 63, 95)'
        );
        return $grafik;
    }

    public function logout()
    {
        $id_user = $this->session->userdata('tracer_userId');
        $activity = "LOG OUT";
        $page_url = base_url("?logout=true");

        $this->m_activity_log->insert($id_user, $activity, $page_url);

        $this->unSetUserData();
        $this->load->database("default", FALSE, TRUE); //CHANGE DB TO DEFAULT sialogin
        if ($this->session->flashdata('changepassword')) {
            $this->session->set_flashdata('true', 'Ubah password berhasil, silahkan login kembali.');
            redirect("login?logout=true");
        }
        redirect("?logout=true");
    }
}
