<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "sistem_setting";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_sistem', 'sistem');
        $this->load->model('M_tema', 'tema');
    }

    public function index()
    {
        $this->data['title'] = 'Sistem Setting';
        $this->data['icon'] = 'fa fa-image';
        $this->data['logo_title'] = $this->sistem->logo_title();
        $this->data['logo_utama'] = $this->sistem->logo_utama();
        $this->data['about_us'] = $this->sistem->about_us();
        $this->data['copyright'] = $this->sistem->copyright();
        $this->data['video_perkenalan'] = $this->sistem->video_perkenalan();
        $this->data['banner'] = $this->sistem->banner();
        $this->data['background_1'] = $this->sistem->background_1();
        $this->data['background_2'] = $this->sistem->background_2();
        $this->data['background_3'] = $this->sistem->background_3();
        $this->data['tema'] = $this->sistem->tema();
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['rektor'] = $this->sistem->rektor();
        $this->data['footer'] = $this->sistem->footer();
        $this->data['header'] = $this->sistem->header();
        $this->data['sistem'] = $this->dataSistem();
        $this->data['is_home'] = false;
        /* setSession('tema', 'dark'); */
        $this->UbahTema();
        
        $this->render('index');
    }

    public function UbahTema()
    {
        $tema = $this->sistem->Tema();
        if($tema->deskripsi == 'dark')
        {
            setSession('tema', 'dark');
        
        }elseif($tema->deskripsi == 'light')
        {
            setSession('tema', 'light');
        } else {
            setSession('tema', '');
        }
        return $tema;
    }

    public function publishAlumni()
    {
        $key = $this->input->post('key');
        $data = [
            'is_publish' => $this->input->post('is_publish')
        ];

        $response = $this->tema->publishTema($key, $data);

        echo json_encode($response);

    }


    public function HalamanEdit()
    {
        $this->data['title'] = 'Edit Sistem';
        $this->data['is_home'] = false;
        $this->data['sistem'] = $this->dataSistem();
        $this->render('edit');
    }

    public function dataSistem()
    {
        $sistem = $this->sistem->getSistem(array('id' => $this->input->get('key')));

        $sistem = $this->sistem->official_account();

        $sistem->instagram = "";
        $sistem->twitter = "";
        $sistem->youtube = "";

        if ($sistem->deskripsi != "" || !empty($sistem->deskripsi)) {
            $deskripsi = json_decode($sistem->deskripsi);

            $sistem->instagram = ($deskripsi->instagram != "" ? $deskripsi->instagram : "");
            $sistem->twitter = ($deskripsi->twitter != "" ? $deskripsi->twitter : "");
            $sistem->youtube = ($deskripsi->youtube != "" ? $deskripsi->youtube : "");
        }

        return $sistem;
    }

    /* function _urlLogo($id){
        $sistem = $this->sistem->logo_title(array('id' => $id));

        if(!file_exists('public/uploads/setting_sistem/'.$id)){
            mkdir('public/uploads/setting_sistem/'.$id, 0755, true);
            chmod('public/uploads/setting_sistem/', 0755);
        }
        
        $oldUrlBukti = '';
        if (!empty($sistem)) {
            $oldUrlBukti = $sistem->url_file;
        }

        $name_input = 'url_photo';
        $return['success']=true;
        if(isset($_FILES[$name_input]['name'])&&$_FILES[$name_input]['name']!=""){
            $config['upload_path']='public/uploads/setting_sistem/'.$id; //path folder file upload
            $config['allowed_types']='jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis').'LOGO'.$id;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)){
                $file = $this->upload->data();
                $return['success']=true;
                $return['file_name']='public/uploads/setting_sistem/'.$id.'/'.$file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            }else{
                $return['success']=false;
                $return['text']="Gagal Upload, File harus 2 MB atau kurang";
            }
        }else{
            $return['success']=false;
            $return['text']="File Tidak Ada";
        }
        return $return;
    } */
    function _urlLogo($id, $oldLogo)
    {

        $id = str_replace("-", "", $id);
        if (!file_exists('public/uploads/setting_sistem/' . $id)) {
            mkdir('public/uploads/setting_sistem/' . $id, 0755, true);
            chmod('public/uploads/setting_sistem/', 0755);
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/setting_sistem/' . $id; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'LOGO' . $id;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = 'public/uploads/setting_sistem/' . $id . '/' . $file['file_name'];
                if (file_exists($oldLogo)) {
                    unlink($oldLogo);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang";
            } 
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }
    function _urlfile($id){
        $sistem = $this->sistem->logo_title(array('id' => $id));

        if(!file_exists('public/uploads/setting_sistem/'.$id)){
            mkdir('public/uploads/setting_sistem/'.$id, 0755, true);
            chmod('public/uploads/setting_sistem/', 0755);
        }
        
        $oldUrlBukti = '';
        if (!empty($sistem)) {
            $oldUrlBukti = $sistem->url_file;
        }

        $name_input = 'url';
        $return['success']=true;
        if(isset($_FILES[$name_input]['name'])&&$_FILES[$name_input]['name']!=""){
            $config['upload_path']='public/uploads/setting_sistem/'.$id; //path folder file upload
            $config['allowed_types']='jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis').'LOGO'.$id;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)){
                $file = $this->upload->data();
                $return['success']=true;
                $return['file_name']='public/uploads/setting_sistem/'.$id.'/'.$file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            }else{
                $return['success']=false;
                $return['text']="Gagal Upload, File harus 2 MB atau kurang";
            }
        }else{
            $return['success']=false;
            $return['text']="File Tidak Ada";
        }
        return $return;
    }
    function _banner1($id){
        $sistem = $this->sistem->logo_title(array('id' => $id));

        if(!file_exists('public/uploads/setting_sistem/'.$id)){
            mkdir('public/uploads/setting_sistem/'.$id, 0755, true);
            chmod('public/uploads/setting_sistem/', 0755);
        }
        
        $oldUrlBukti = '';
        if (!empty($sistem)) {
            $oldUrlBukti = $sistem->url_file;
        }

        $name_input = 'banner1';
        $return['success']=true;
        if(isset($_FILES[$name_input]['name'])&&$_FILES[$name_input]['name']!=""){
            $config['upload_path']='public/uploads/setting_sistem/'.$id; //path folder file upload
            $config['allowed_types']='jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis').'LOGO'.$id;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)){
                $file = $this->upload->data();
                $return['success']=true;
                $return['file_name']='public/uploads/setting_sistem/'.$id.'/'.$file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            }else{
                $return['success']=false;
                $return['text']="Gagal Upload, File harus 2 MB atau kurang";
            }
        }else{
            $return['success']=false;
            $return['text']="File Tidak Ada";
        }
        return $return;
    }

    public function editSistem()
    {
        $htmlCodeNumber = 201;
        $response = '';
		/* $input = $this->input->post(); */

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $key = $this->input->post('key');
        $data = array(
            'deskripsi' => $this->input->post('deskripsi'),
			'url_link' => $this->input->post('url_link'),
			'title' => $this->input->post('title'),
			'sub_title' => $this->input->post('sub_title'),
        );


        if ($_FILES['url_photo']['name']) {
            $urlLogo = $this->_urlLogo($this->input->post('id'), $this->input->post('oldLogo'));
            $data['url_file'] = $urlLogo['file_name'];
        } else {
            $data['url_file'] = $this->input->post('oldLogo');
        }

		$this->sistem->editsistem($data,array('key' => $key));
        

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit Sistem Setting',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    public function editBackgroud()
    {
        $htmlCodeNumber = 201;
        $response = '';
		/* $input = $this->input->post(); */

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $key = $this->input->post('key');


        if ($_FILES['url_photo']['name']) {
            $urlLogo = $this->_urlLogo($this->input->post('id'), $this->input->post('oldLogo'));
            $data['url_file'] = $urlLogo['file_name'];
        } else {
            $data['url_file'] = $this->input->post('oldLogo');
        }

		$this->sistem->editsistem($data,array('key' => $key));
        

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit Sistem Setting',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    public function editTema()
    {
        $htmlCodeNumber = 201;
        $response = '';
		/* $input = $this->input->post(); */

        $errors = $this->_validationhex();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $key = $this->input->post('key');
        $data = array(
            'deskripsi' => $this->input->post('deskripsi'),
        );

		$this->sistem->editsistem($data,array('key' => $key));
        

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit Sistem Setting',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    public function editAbout_Us()
    {
        $htmlCodeNumber = 201;
        $response = '';
		/* $input = $this->input->post(); */

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $key = $this->input->post('key');
        $data = array(
            'deskripsi' => $this->input->post('deskripsi'),
			'url_link' => $this->input->post('url_link'),
			'title' => $this->input->post('title'),
			'sub_title' => $this->input->post('sub_title'),
            'warna_1' => $this->input->post('warna_1'),
            'warna_2' => $this->input->post('warna_2')
        );

		$this->sistem->editsistem($data,array('key' => $key));
        

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();
        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => '<strong>Berhasil</strong>',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    public function editLogo()
    {
        $htmlCodeNumber = 201;
        $response = '';
		/* $input = $this->input->post(); */

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $key = $this->input->post('key');
        $data = array(
            'deskripsi' => $this->input->post('deskripsi'),
			'url_link' => $this->input->post('url_link'),
			'title' => $this->input->post('title'),
			'sub_title' => $this->input->post('sub_title'),
        );

        $urlLogo = $this->_urlfile($this->input->post('id'));
        $data['url_file'] = $urlLogo['file_name'];

		$this->sistem->editsistem($data,array('key' => $key));
        

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit Sistem Setting',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    
    public function updateVideo()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $key = $this->input->post('key');

        $data = array(
            'deskripsi' => $this->input->post('deskripsi'),
			'url_link' => $this->input->post('url_link'),
			'title' => $this->input->post('title'), 
			'sub_title' => $this->input->post('sub_title'), 
        );

        if ($_FILES['url_photo']['name']) {
            $urlLogo = $this->_urlLogo($this->input->post('id'), $this->input->post('oldLogo'));
            $data['url_file'] = $urlLogo['file_name'];
        } else {
            $data['url_file'] = $this->input->post('oldLogo');
        }

		$this->sistem->editsistem($data,array('key' => $key));
        

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit Sistem Setting',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function update_sosmed()
    {
        $htmlCodeNumber = 201;
        $response = '';
		/* $input = $this->input->post(); */

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $sosmed = array(
            'instagram' => $this->input->post('instagram'),
            'twitter' => $this->input->post('twitter'),
            'youtube' => $this->input->post('youtube'),
        );

        $key = $this->input->post('key');
        $data = array(
            'deskripsi' => json_encode($sosmed),
        );

        $this->sistem->editsistem($data,array('key' =>$key));

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit Sistem Setting',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    public function update_banner()
    {
        $htmlCodeNumber = 201;
        $response = '';
		/* $input = $this->input->post(); */

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];
        $urlLogo = $this->_urlfile($this->input->post('id'));

        $sosmed = array(
            'banner1' => $urlLogo['file_name'],
            'banner2' => $urlLogo['file_name'],
            'banner3' => $urlLogo['file_name'],
        );

        $key = $this->input->post('key');
        $data = array(
            'url_file' => json_encode($sosmed),
            'title' => $this->input->post('title'),
            'sub_title' => $this->input->post('sub_title'),
        );

        $this->sistem->editsistem($data,array('key' =>$key));

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan Berita',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit Sistem Setting',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    private function _runValidation()
    {
        $errors = FALSE;
        /* $pattern_url = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'; */
        /* $this->form_validation->set_rules('url_link', 'Url Link', 'trim|required'); */
        /* $this->form_validation->set_rules('url_file', 'Url File', 'trim|required'); */
        $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('sub_title', 'Sub Title', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('url_link', 'Url Link', 'trim|required|callback_checkUrlLink|min_length[3]');
        $this->form_validation->set_rules('instagram', 'Instagram', 'trim|required|callback_checkUrlLink');
        $this->form_validation->set_rules('twitter', 'Twitter', 'trim|required|callback_checkUrlLink');
        $this->form_validation->set_rules('youtube', 'Youtube', 'trim|required|callback_checkUrlLink');

        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('alpha', '{field} tidak boleh menggunakan spasi');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('checkUrlLink', '{field} tidak sesuai contoh');
        $this->form_validation->set_message('alpha_numeric_spaces', '{field} tidak boleh hanya spasi');
        
        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    private function _validationhex()
    {
        $errors = FALSE ;
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|callback_checkhex');
       
        $this->form_validation->set_message('checkhex', '{field} tidak sesuai contoh');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function checkUrlLink($str)
	{
			if (preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', trim($str)))
			{
					return TRUE;
			}
					return FALSE;
			
	}

    public function checkhex($str)
    {
        if (preg_match('/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/', trim($str)))
        {
            return TRUE;
        }
            return FALSE;
    }
   

}
