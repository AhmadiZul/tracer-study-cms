<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "universitas_profil";

    private $id = "";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_profil_universitas');
        $this->load->library('form_validation');
        /* $this->load->library('form_validation');
        $this->load->model('m_profil'); */
    }

    public function index()
    {
        $this->data['title'] = 'Data Perguruan Tinggi';
        $this->data['is_home'] = false;
        $this->data['alumni'] = $this->m_profil_universitas->getAdmin($this->session->userdata("tracer_userId"));
        $this->renderTo('universitas_profil/index');

    }

    function _urlphoto()
    {
        $alumni = $this->getPt();

        $idPT = str_replace("-", "", $alumni->id_perguruan_tinggi);
        $id_alumni = str_replace("-", "", $alumni->id_perguruan_tinggi);

        if (!file_exists('admin/public/uploads/perguruan_tinggi/' . $idPT)) {
            mkdir('admin/public/uploads/perguruan_tinggi/' . $idPT, 0755,  true);
        }
        if (!file_exists('admin/public/uploads/perguruan_tinggi/' . $idPT . '/alumni')) {
            mkdir('admin/public/uploads/perguruan_tinggi/' . $idPT .  '/alumni/', 0755, true);
        }
        if (!file_exists('admin/public/uploads/perguruan_tinggi/' . $idPT . '/alumni/' . $id_alumni)) {
            mkdir('admin/public/uploads/perguruan_tinggi/' . $idPT .  '/alumni/' . $id_alumni, 0755, true);
        }
        
        $oldPhoto = $alumni->url_photo;
        $base_url = base_url();
        $oldPhoto = str_replace($base_url, '',$oldPhoto);

        $return['success'] = true;
        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $config['upload_path'] = 'admin/public/uploads/perguruan_tinggi/' . $idPT .  '/alumni/' . $id_alumni; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'photoalumni' . $id_alumni;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload('url_photo')) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = base_url().'admin/public/uploads/perguruan_tinggi/' . $idPT .  '/alumni/' . $id_alumni.'/' . $file['file_name'];
                if (file_exists($oldPhoto)) {
                    unlink($oldPhoto);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang";
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    function _urlbukti($tab,$id = null){
        $alumni = $this->getAlumni();

        $idPT = str_replace("-", "", $alumni->id_perguruan_tinggi);
        $id_alumni = str_replace("-", "", $alumni->id_alumni);

        if (!file_exists('admin/public/uploads/perguruan_tinggi/' . $idPT)) {
            mkdir('admin/public/uploads/perguruan_tinggi/' . $idPT, 0755,  true);
        }
        if (!file_exists('admin/public/uploads/perguruan_tinggi/' . $idPT . '/alumni')) {
            mkdir('admin/public/uploads/perguruan_tinggi/' . $idPT .  '/alumni/', 0755, true);
        }
        if (!file_exists('admin/public/uploads/perguruan_tinggi/' . $idPT . '/alumni/' . $id_alumni)) {
            mkdir('admin/public/uploads/perguruan_tinggi/' . $idPT .  '/alumni/' . $id_alumni, 0755, true);
        }
        if (!file_exists('admin/public/uploads/perguruan_tinggi/' . $idPT . '/alumni/' . $id_alumni.'/'.$tab)) {
            mkdir('admin/public/uploads/perguruan_tinggi/' . $idPT .  '/alumni/' . $id_alumni.'/'.$tab, 0755, true);
        }

        $oldUrlBukti = '';
        $name_input = 'url_bukti';
        if ($id != null) {
            if ($tab == 'pengalaman') {
                $oldUrlBukti = $this->m_profil->get_pengalaman($this->session->userdata("t_alumniId"),$id)->url_bukti;
            }
            if ($tab == 'organisasi') {
                $oldUrlBukti = $this->m_profil->get_organisasi($this->session->userdata("t_alumniId"),$id)->url_bukti;
            }
            if ($tab == 'prestasi') {
                $name_input = 'url_sertifikat';
                $oldUrlBukti = $this->m_profil->get_prestasi($this->session->userdata("t_alumniId"),$id)->url_sertifikat;
            }
            if ($tab == 'skill') {
                $name_input = 'url_sertifikat';
                $oldUrlBukti = $this->m_profil->get_skill($this->session->userdata("t_alumniId"),$id)->url_sertifikat;
            }
            $base_url = base_url();
            $oldUrlBukti = str_replace($base_url, '',$oldUrlBukti);
        }
        if ($tab == 'skill' || $tab == 'prestasi') {
            $name_input = 'url_sertifikat';
        }
        $return['success']=true;
        if(isset($_FILES[$name_input]['name'])&&$_FILES[$name_input]['name']!=""){
            $config['upload_path']='admin/public/uploads/perguruan_tinggi/' . $idPT .  '/alumni/' . $id_alumni.'/'.$tab; //path folder file upload
            $config['allowed_types']='jpg|png|jpeg|pdf'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis').$tab.$id_alumni;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)){
                $file = $this->upload->data();
                $return['success']=true;
                $return['file_name']= base_url().'admin/public/uploads/perguruan_tinggi/' . $idPT .  '/alumni/' . $id_alumni.'/'.$tab.'/'.$file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            }else{
                $return['success']=false;
                $return['text']="Gagal Upload, File harus 2 MB atau kurang";
            }
        }else{
            $return['success']=false;
            $return['text']="File Tidak Ada";
        }
        return $return;
    }

    public function update_profil()
    {
        $dataI = $this->input->post();
        $alumni = $this->getPt();

        $this->form_validation->set_rules('nama_resmi', 'Nama Resmi', 'required');
        $this->form_validation->set_rules('nama_pendek','Nama Pendek', 'required');
        $this->form_validation->set_rules('alamat','Alamat','required');
        $this->form_validation->set_rules('kota','Kota','required');
        $this->form_validation->set_rules('no_telp','Nomor Telepone','required');
        $this->form_validation->set_rules('kode_pos','Kode Pos','required');
        $this->form_validation->set_rules('no_fax','Nomor Fakultas','required');
        $this->form_validation->set_rules('email','Email','required');

        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('less_than_equal_to', '{field} harus kurang atau sama dengan {param}.00');
        $this->form_validation->set_message('checkDate', '{field} tidak boleh lebih dari tanggal hari ini');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');

        if ($this->form_validation->run() == TRUE) {
            $this->db->trans_begin();
            /* $this->m_profil->update_user(array('real_name' => trim($dataI['nama']),'email' => trim($dataI['email'])), $this->session->userdata("t_userId")); */
            $data = array(
                'nama_resmi'            => trim($dataI['nama_resmi']),
                'nama_pendek'           => trim($dataI['nama_pendek']),
                'alamat'                => trim($dataI['alamat']),
                'kota'                  => trim($dataI['kota']),
                'no_telp'               => trim($dataI['no_telp']),
                'kode_pos'              => trim($dataI['kode_pos']),
                'no_fax'                => trim($dataI['no_fax']),
                'email'                 => trim($dataI['email']),
            );

            if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
                $urlphoto = $this->_urlphoto();
                if ($urlphoto['success']) {
                    $data['path_logo'] = $urlphoto['file_name'];
                    $this->session->set_userdata("tracer_path_logo", $urlphoto['file_name']);
                    $this->session->set_userdata("tracer_nama_resmi", trim($dataI['nama_resmi']));
                } else {
                    $return["success"] = FALSE;
                    $return["pesan"] = $urlphoto['text'];
                    echo json_encode($return);
                    exit();
                }
            }

            $this->m_profil_universitas->update_pt($data, trim($dataI['id_perguruan_tinggi']));

            $text = '';

            $kondisi = false;

            /* if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
            }

            $this->db->trans_complete();
            if ($text == '') {
                $return = array('success' => $kondisi, "kode" => trim($dataI['id_perguruan_tinggi']));
            } else {
                $return = array('success' => $kondisi, "kode" => trim($dataI['id_perguruan_tinggi']), 'text' => $text);
            }
            echo json_encode($return); */
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }

    public function simpan_pengalaman()
    {
        $data = $this->input->post();
        $alumni = $this->getAlumni();

        $this->form_validation->set_rules('jenis_pengalaman','Jenis pengalaman','trim|required');
        $this->form_validation->set_rules('nama_instansi','Nama instansi','trim|required');
        $this->form_validation->set_rules('posisi','Posisi','trim|required');
        $this->form_validation->set_rules('alamat_instansi','Alamat','trim|required');
        $this->form_validation->set_rules('start_date','Tanggal mulai','trim|required');
        $this->form_validation->set_rules('end_date','Tanggal selesai','trim|required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','trim|required');

        $this->form_validation->set_message('required', '{field} belum diisi');

        if ($this->form_validation->run() == TRUE) {
            
            $this->db->trans_begin();

            $dataP = array(
                'id_alumni' => $alumni->id_alumni,
                'jenis_pengalaman' => $data['jenis_pengalaman'],
                'nama_instansi' => $data['nama_instansi'],
                'posisi' => $data['posisi'],
                'alamat' => $data['alamat_instansi'],
                'is_current_position' => (isset($data['is_current_position']) ? $data['is_current_position'] : '0'),
                'start_date' => $data['start_date'],
                'end_date' => $data['end_date'],
                'deskripsi' => $data['deskripsi'],
                'created_by' => $this->session->userdata("t_userId"),
                'last_modified_by' => $this->session->userdata("t_userId"),
            );

            if (isset($_FILES['url_bukti']['name']) && $_FILES['url_bukti']['name'] != "") {
                $url_bukti = $this->_urlbukti('pengalaman');
                if ($url_bukti['success']) {
                    $dataP['url_bukti'] = $url_bukti['file_name'];
                } else {
                    $return["success"] = FALSE;
                    $return["pesan"] = $url_bukti['text'];
                    echo json_encode($return);
                    exit();
                }
            }

            $this->m_profil->simpan_pengalaman($dataP);

            $text = '';

            $kondisi = false;

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
                $pengalaman = $this->m_profil->get_pengalaman($this->session->userdata("t_alumniId"));
                $return = array('success' => $kondisi, "kode" => $data['id_pengalaman'], 'pengalaman' => $pengalaman);
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
                $return = array('success' => $kondisi, "kode" => $data['id_pengalaman'], 'text' => $text);
            }
            echo json_encode($return);
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }

    public function update_pengalaman()
    {
        $data = $this->input->post();

        $this->form_validation->set_rules('id_pengalaman','Id','trim|required');
        $this->form_validation->set_rules('jenis_pengalaman','Jenis pengalaman','trim|required');
        $this->form_validation->set_rules('nama_instansi','Nama instansi','trim|required');
        $this->form_validation->set_rules('posisi','Posisi','trim|required');
        $this->form_validation->set_rules('alamat_instansi','Alamat','trim|required');
        $this->form_validation->set_rules('start_date','Tanggal mulai','trim|required');
        $this->form_validation->set_rules('end_date','Tanggal selesai','trim|required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','trim|required');

        $this->form_validation->set_message('required', '{field} belum diisi');

        if ($this->form_validation->run() == TRUE) {
            
            $this->db->trans_begin();

            $dataP = array(
                'jenis_pengalaman' => $data['jenis_pengalaman'],
                'nama_instansi' => $data['nama_instansi'],
                'posisi' => $data['posisi'],
                'alamat' => $data['alamat_instansi'],
                'is_current_position' => (isset($data['is_current_position']) ? $data['is_current_position'] : '0'),
                'start_date' => $data['start_date'],
                'end_date' => $data['end_date'],
                'deskripsi' => $data['deskripsi'],
                'last_modified_by' => $this->session->userdata("t_userId"),
            );

            if (isset($_FILES['url_bukti']['name']) && $_FILES['url_bukti']['name'] != "") {
                $url_bukti = $this->_urlbukti('pengalaman',$data['id_pengalaman']);
                if ($url_bukti['success']) {
                    $dataP['url_bukti'] = $url_bukti['file_name'];
                } else {
                    $return["success"] = FALSE;
                    $return["pesan"] = $url_bukti['text'];
                    echo json_encode($return);
                    exit();
                }
            }

            $this->m_profil->update_pengalaman($dataP, $data['id_pengalaman']);

            $text = '';

            $kondisi = false;

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
                $pengalaman = $this->m_profil->get_pengalaman($this->session->userdata("t_alumniId"));
                // echo '<pre>';
                // print_r($pengalaman);
                // exit();
                $return = array('success' => $kondisi, "kode" => $data['id_pengalaman'], 'pengalaman' => $pengalaman);
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
                $return = array('success' => $kondisi, "kode" => $data['id_pengalaman'], 'text' => $text);
            }

            $this->db->trans_complete();
            echo json_encode($return);
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }

    public function delete_pengalaman()
    {
        $id_pengalaman = $this->input->post('id_pengalaman');
        $data = $this->m_profil->get_pengalaman($this->session->userdata("t_alumniId"),$id_pengalaman);

        $oldBukti = $data->url_bukti;
        $base_url = base_url();
        $oldBukti = str_replace($base_url, '',$oldBukti);
        if (isset($oldBukti)) {
            if (file_exists($oldBukti)) {
                unlink($oldBukti);
            }
        }

        $this->db->trans_begin();

        $this->m_profil->delete_pengalaman($id_pengalaman);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $pengalaman = $this->m_profil->get_pengalaman($this->session->userdata("t_alumniId"));

            $return["success"] = TRUE;
            $return["pesan"] = "Data Berhasil Dihapus";
            $return["pengalaman"] = $pengalaman;
        } else {
            $this->db->trans_rollback();

            $return["success"] = FALSE;
            $return["pesan"] = "Data Gagal Dihapus";
        }

        $this->db->trans_complete();
            
        echo json_encode($return);
    }

    public function simpan_organisasi()
    {
        $data = $this->input->post();
        $alumni = $this->getAlumni();

        $this->form_validation->set_rules('nama_organisasi','Nama organisasi','trim|required');
        $this->form_validation->set_rules('posisi','Posisi','trim|required');
        $this->form_validation->set_rules('job_desc','Job desc','trim|required');
        $this->form_validation->set_rules('start_date','Tanggal mulai','trim|required');
        $this->form_validation->set_rules('end_date','Tanggal selesai','trim|required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','trim|required');

        $this->form_validation->set_message('required', '{field} belum diisi');

        if ($this->form_validation->run() == TRUE) {
            
            $this->db->trans_begin();

            $dataO = array(
                'id_alumni' => $alumni->id_alumni,
                'nama_organisasi' => $data['nama_organisasi'],
                'posisi' => $data['posisi'],
                'job_desc' => $data['job_desc'],
                'is_current_position' => (isset($data['is_current_position']) ? $data['is_current_position'] : '0'),
                'start_date' => $data['start_date'],
                'end_date' => $data['end_date'],
                'deskripsi' => $data['deskripsi'],
                'created_by' => $this->session->userdata("t_userId"),
                'last_modified_by' => $this->session->userdata("t_userId"),
            );

            if (isset($_FILES['url_bukti']['name']) && $_FILES['url_bukti']['name'] != "") {
                $url_bukti = $this->_urlbukti('organisasi');
                if ($url_bukti['success']) {
                    $dataO['url_bukti'] = $url_bukti['file_name'];
                } else {
                    $return["success"] = FALSE;
                    $return["pesan"] = $url_bukti['text'];
                    echo json_encode($return);
                    exit();
                }
            }

            $this->m_profil->simpan_organisasi($dataO);

            $text = '';

            $kondisi = false;

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
                $organisasi = $this->m_profil->get_organisasi($this->session->userdata("t_alumniId"));
                $return = array('success' => $kondisi, "kode" => $data['id_organisasi'], 'organisasi' => $organisasi);
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
                $return = array('success' => $kondisi, "kode" => $data['id_organisasi'], 'text' => $text);
            }
            echo json_encode($return);
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }

    public function update_organisasi()
    {
        $data = $this->input->post();

        $this->form_validation->set_rules('id_organisasi','Id','trim|required');
        $this->form_validation->set_rules('nama_organisasi','Nama organisasi','trim|required');
        $this->form_validation->set_rules('posisi','Posisi','trim|required');
        $this->form_validation->set_rules('job_desc','Job desc','trim|required');
        $this->form_validation->set_rules('start_date','Tanggal mulai','trim|required');
        $this->form_validation->set_rules('end_date','Tanggal selesai','trim|required');
        $this->form_validation->set_rules('deskripsi','Deskripsi','trim|required');

        $this->form_validation->set_message('required', '{field} belum diisi');

        if ($this->form_validation->run() == TRUE) {
            
            $this->db->trans_begin();

            $dataO = array(
                'nama_organisasi' => $data['nama_organisasi'],
                'posisi' => $data['posisi'],
                'job_desc' => $data['job_desc'],
                'is_current_position' => (isset($data['is_current_position']) ? $data['is_current_position'] : '0'),
                'start_date' => $data['start_date'],
                'end_date' => $data['end_date'],
                'deskripsi' => $data['deskripsi'],
                'last_modified_by' => $this->session->userdata("t_userId"),
            );

            if (isset($_FILES['url_bukti']['name']) && $_FILES['url_bukti']['name'] != "") {
                $url_bukti = $this->_urlbukti('organisasi',$data['id_organisasi']);
                if ($url_bukti['success']) {
                    $dataO['url_bukti'] = $url_bukti['file_name'];
                } else {
                    $return["success"] = FALSE;
                    $return["pesan"] = $url_bukti['text'];
                    echo json_encode($return);
                    exit();
                }
            }

            $this->m_profil->update_organisasi($dataO, $data['id_organisasi']);

            $text = '';

            $kondisi = false;

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
                $organisasi = $this->m_profil->get_organisasi($this->session->userdata("t_alumniId"));
                $return = array('success' => $kondisi, "kode" => $data['id_organisasi'], 'organisasi' => $organisasi);
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
                $return = array('success' => $kondisi, "kode" => $data['id_organisasi'], 'text' => $text);
            }

            $this->db->trans_complete();
            echo json_encode($return);
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }

    public function delete_organisasi()
    {
        $id_organisasi = $this->input->post('id_organisasi');
        $data = $this->m_profil->get_organisasi($this->session->userdata("t_alumniId"),$id_organisasi);

        $oldBukti = $data->url_bukti;
        $base_url = base_url();
        $oldBukti = str_replace($base_url, '',$oldBukti);
        if (isset($oldBukti)) {
            if (file_exists($oldBukti)) {
                unlink($oldBukti);
            }
        }

        $this->db->trans_begin();

        $this->m_profil->delete_organisasi($id_organisasi);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $organisasi = $this->m_profil->get_organisasi($this->session->userdata("t_alumniId"));

            $return["success"] = TRUE;
            $return["pesan"] = "Data Berhasil Dihapus";
            $return["organisasi"] = $organisasi;
        } else {
            $this->db->trans_rollback();

            $return["success"] = FALSE;
            $return["pesan"] = "Data Gagal Dihapus";
        }

        $this->db->trans_complete();
            
        echo json_encode($return);
    }

    public function simpan_prestasi()
    {
        $data = $this->input->post();
        $alumni = $this->getAlumni();

        $this->form_validation->set_rules('nama_prestasi','Nama prestasi','trim|required');
        $this->form_validation->set_rules('penyelenggara','Nama penyelenggara','trim|required');
        $this->form_validation->set_rules('tahun','Tahun','trim|required');
        $this->form_validation->set_rules('tingkat','Tingkat','trim|required');
        $this->form_validation->set_rules('gelar','Gelar','trim|required');

        $this->form_validation->set_message('required', '{field} belum diisi');

        if ($this->form_validation->run() == TRUE) {
            
            $this->db->trans_begin();

            $dataP = array(
                'id_alumni' => $alumni->id_alumni,
                'nama_prestasi' => $data['nama_prestasi'],
                'penyelenggara' => $data['penyelenggara'],
                'tahun' => $data['tahun'],
                'tingkat' => $data['tingkat'],
                'gelar' => $data['gelar'],
                'created_by' => $this->session->userdata("t_userId"),
                'last_modified_by' => $this->session->userdata("t_userId"),
            );

            if (isset($_FILES['url_sertifikat']['name']) && $_FILES['url_sertifikat']['name'] != "") {
                $url_bukti = $this->_urlbukti('prestasi');
                if ($url_bukti['success']) {
                    $dataP['url_sertifikat'] = $url_bukti['file_name'];
                } else {
                    $return["success"] = FALSE;
                    $return["pesan"] = $url_bukti['text'];
                    echo json_encode($return);
                    exit();
                }
            }

            $this->m_profil->simpan_prestasi($dataP);

            $text = '';

            $kondisi = false;

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
                $prestasi = $this->m_profil->get_prestasi($this->session->userdata("t_alumniId"));
                $return = array('success' => $kondisi, "kode" => $data['id_prestasi'], 'prestasi' => $prestasi);
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
                $return = array('success' => $kondisi, "kode" => $data['id_prestasi'], 'text' => $text);
            }
            echo json_encode($return);
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }

    public function update_prestasi()
    {
        $data = $this->input->post();

        $this->form_validation->set_rules('id_prestasi','Id','trim|required');
        $this->form_validation->set_rules('nama_prestasi','Nama prestasi','trim|required');
        $this->form_validation->set_rules('penyelenggara','Penyelenggara','trim|required');
        $this->form_validation->set_rules('tahun','Tahun','trim|required');
        $this->form_validation->set_rules('tingkat','Tingkat','trim|required');
        $this->form_validation->set_rules('gelar','Gelar','trim|required');

        $this->form_validation->set_message('required', '{field} belum diisi');

        if ($this->form_validation->run() == TRUE) {
            
            $this->db->trans_begin();

            $dataP = array(
                'nama_prestasi' => $data['nama_prestasi'],
                'penyelenggara' => $data['penyelenggara'],
                'tahun' => $data['tahun'],
                'tingkat' => $data['tingkat'],
                'gelar' => $data['gelar'],
                'last_modified_by' => $this->session->userdata("t_userId"),
            );

            if (isset($_FILES['url_sertifikat']['name']) && $_FILES['url_sertifikat']['name'] != "") {
                $url_bukti = $this->_urlbukti('prestasi',$data['id_prestasi']);
                if ($url_bukti['success']) {
                    $dataP['url_sertifikat'] = $url_bukti['file_name'];
                } else {
                    $return["success"] = FALSE;
                    $return["pesan"] = $url_bukti['text'];
                    echo json_encode($return);
                    exit();
                }
            }

            $this->m_profil->update_prestasi($dataP, $data['id_prestasi']);

            $text = '';

            $kondisi = false;

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
                $prestasi = $this->m_profil->get_prestasi($this->session->userdata("t_alumniId"));
                $return = array('success' => $kondisi, "kode" => $data['id_prestasi'], 'prestasi' => $prestasi);
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
                $return = array('success' => $kondisi, "kode" => $data['id_prestasi'], 'text' => $text);
            }

            $this->db->trans_complete();
            echo json_encode($return);
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }

    public function delete_prestasi()
    {
        $id_prestasi = $this->input->post('id_prestasi');
        $data = $this->m_profil->get_prestasi($this->session->userdata("t_alumniId"),$id_prestasi);

        $oldBukti = $data->url_sertifikat;
        $base_url = base_url();
        $oldBukti = str_replace($base_url, '',$oldBukti);
        if (isset($oldBukti)) {
            if (file_exists($oldBukti)) {
                unlink($oldBukti);
            }
        }

        $this->db->trans_begin();

        $this->m_profil->delete_prestasi($id_prestasi);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $prestasi = $this->m_profil->get_prestasi($this->session->userdata("t_alumniId"));

            $return["success"] = TRUE;
            $return["pesan"] = "Data Berhasil Dihapus";
            $return["prestasi"] = $prestasi;
        } else {
            $this->db->trans_rollback();

            $return["success"] = FALSE;
            $return["pesan"] = "Data Gagal Dihapus";
        }

        $this->db->trans_complete();
            
        echo json_encode($return);
    }

    public function simpan_skill()
    {
        $data = $this->input->post();
        $alumni = $this->getAlumni();

        $this->form_validation->set_rules('nama_skill','Nama skill','trim|required');
        $this->form_validation->set_rules('bidang','Bidang','trim|required');
        $this->form_validation->set_rules('level','Level','trim|required');

        $this->form_validation->set_message('required', '{field} belum diisi');

        if ($this->form_validation->run() == TRUE) {
            
            $this->db->trans_begin();

            $dataS = array(
                'id_alumni' => $alumni->id_alumni,
                'nama_skill' => $data['nama_skill'],
                'bidang' => $data['bidang'],
                'level' => $data['level'],
                'created_by' => $this->session->userdata("t_userId"),
                'last_modified_by' => $this->session->userdata("t_userId"),
            );

            if (isset($_FILES['url_sertifikat']['name']) && $_FILES['url_sertifikat']['name'] != "") {
                $url_bukti = $this->_urlbukti('skill');
                if ($url_bukti['success']) {
                    $dataS['url_sertifikat'] = $url_bukti['file_name'];
                } else {
                    $return["success"] = FALSE;
                    $return["pesan"] = $url_bukti['text'];
                    echo json_encode($return);
                    exit();
                }
            }

            $this->m_profil->simpan_skill($dataS);

            $text = '';

            $kondisi = false;

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
                $skill = $this->m_profil->get_skill($this->session->userdata("t_alumniId"));
                $return = array('success' => $kondisi, "kode" => $data['id_skill'], 'skill' => $skill);
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
                $return = array('success' => $kondisi, "kode" => $data['id_skill'], 'text' => $text);
            }
            echo json_encode($return);
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }

    public function update_skill()
    {
        $data = $this->input->post();

        $this->form_validation->set_rules('id_skill','Id','trim|required');
        $this->form_validation->set_rules('nama_skill','Nama skill','trim|required');
        $this->form_validation->set_rules('bidang','Bidang','trim|required');
        $this->form_validation->set_rules('level','Level','trim|required');

        $this->form_validation->set_message('required', '{field} belum diisi');

        if ($this->form_validation->run() == TRUE) {
            
            $this->db->trans_begin();

            $dataS = array(
                'nama_skill' => $data['nama_skill'],
                'bidang' => $data['bidang'],
                'level' => $data['level'],
                'last_modified_by' => $this->session->userdata("t_userId"),
            );

            if (isset($_FILES['url_sertifikat']['name']) && $_FILES['url_sertifikat']['name'] != "") {
                $url_bukti = $this->_urlbukti('skill',$data['id_skill']);
                if ($url_bukti['success']) {
                    $dataS['url_sertifikat'] = $url_bukti['file_name'];
                } else {
                    $return["success"] = FALSE;
                    $return["pesan"] = $url_bukti['text'];
                    echo json_encode($return);
                    exit();
                }
            }

            $this->m_profil->update_skill($dataS, $data['id_skill']);

            $text = '';

            $kondisi = false;

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
                $skill = $this->m_profil->get_skill($this->session->userdata("t_alumniId"));
                $return = array('success' => $kondisi, "kode" => $data['id_skill'], 'skill' => $skill);
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
                $return = array('success' => $kondisi, "kode" => $data['id_skill'], 'text' => $text);
            }

            $this->db->trans_complete();
            echo json_encode($return);
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }

    public function delete_skill()
    {
        $id_skill = $this->input->post('id_skill');
        $data = $this->m_profil->get_skill($this->session->userdata("t_alumniId"),$id_skill);

        $oldBukti = $data->url_sertifikat;
        $base_url = base_url();
        $oldBukti = str_replace($base_url, '',$oldBukti);
        if (isset($oldBukti)) {
            if (file_exists($oldBukti)) {
                unlink($oldBukti);
            }
        }

        $this->db->trans_begin();

        $this->m_profil->delete_skill($id_skill);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $skill = $this->m_profil->get_skill($this->session->userdata("t_alumniId"));

            $return["success"] = TRUE;
            $return["pesan"] = "Data Berhasil Dihapus";
            $return["skill"] = $skill;
        } else {
            $this->db->trans_rollback();

            $return["success"] = FALSE;
            $return["pesan"] = "Data Gagal Dihapus";
        }

        $this->db->trans_complete();
            
        echo json_encode($return);
    }

    public function update_sosmed()
    {
        $data = $this->input->post();
        $pattern_url = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';

        if (!preg_match($pattern_url, $data['facebook'])) {
            $return = array('success' => false, "kode" => $this->session->userdata("t_alumniId"), 'text' => "Url facebook tidak sesuai");

            goto End;
        }

        if (!preg_match($pattern_url, $data['twitter'])) {
            $return = array('success' => false, "kode" => $this->session->userdata("t_alumniId"), 'text' => "Url twitter tidak sesuai");

            goto End;
        }

        if (!preg_match($pattern_url, $data['linkedin'])) {
            $return = array('success' => false, "kode" => $this->session->userdata("t_alumniId"), 'text' => "Url linkedin tidak sesuai");

            goto End;
        }

        $this->db->trans_begin();

        $dataSosmed = array(
            'facebook' => $data['facebook'],
            'twitter' => $data['twitter'],
            'linkedin' => $data['linkedin'],
        );

        $this->m_profil->update_sosmed(array('sosmed' => json_encode($dataSosmed)), $this->session->userdata("t_alumniId"));

            $text = '';

            $kondisi = false;

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            $kondisi = true;
            $return = array('success' => $kondisi, "kode" => $this->session->userdata("t_alumniId"));

            goto End;
        }

        $this->db->trans_rollback();
        $kondisi = false;
        $return = array('success' => $kondisi, "kode" => $this->session->userdata("t_alumniId"), 'text' => $text);

        goto End;

        End:
        echo json_encode($return);
    }

    public function update_akun()
    {
        $dataI = $this->input->post();

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password_lama', 'Password Lama', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('konfirmasi_password', 'Konfirmasi password', 'trim|required|matches[password]');

        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');

        if ($this->form_validation->run() == TRUE) {

            $this->db->trans_begin();

            $cekUsername = $this->m_profil->cek_user(array('username' => trim($dataI['username'])), $this->session->userdata("t_userId"));
            $cekPasswordLama = $this->m_profil->cek_password(hash('sha256', $dataI['password_lama']), $this->session->userdata("t_userId"));

            if ($cekUsername != 0) {
                $return['success'] = false;
                $return['text']   = 'Username telah terdaftar. Username harus berbeda dengan lainnya.';
                echo json_encode($return);
                return;
            }

            if ($cekPasswordLama != 1) {
                $return['success'] = false;
                $return['text']   = 'Password lama tidak sesuai.';
                echo json_encode($return);
                return;
            }

            $this->m_profil->update_user(array('username' => trim($dataI['username']), 'password' => hash('sha256', $dataI['password'])), $this->session->userdata("t_userId"));

            $text = '';

            $kondisi = false;

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                $kondisi = true;
            } else {
                $this->db->trans_rollback();
                $kondisi = false;
            }

            $this->db->trans_complete();
            if ($text == '') {
                $return = array('success' => $kondisi, "kode" => trim($dataI['username']));
            } else {
                $return = array('success' => $kondisi, "kode" => trim($dataI['username']), 'text' => $text);
            }
            echo json_encode($return);
        } else {
            $return = array('success' => false, "text" => validation_errors());
            echo json_encode($return);
        }
    }
}
