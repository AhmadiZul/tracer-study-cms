<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "visi_misi";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title'] = 'Visi dan Misi';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Visi Misi');

        $crud->setTable('visi_misi');

        $crud->columns([
            'visi',
            'misi',

        ]);

        $crud->displayAs([
            'visi' => 'Visi Universitas',
            'misi' => 'Misi Universitas',

        ]);

        $crud->addFields([
            'visi',
            'misi',
        ]);

        $crud->editFields([
            'visi',
            'misi',
        ]);

        $crud->requiredFields([
            'visi',
            'misi',
        ]);


        $output = $crud->render();
        $this->_setOutput('visi_misi/index/index', $output);
    }


    

    

}
