<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "career_fair";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('tanggalindo');
        $this->load->model('M_career', 'career');
    }

    public function index()
    {
        $this->data['title'] = 'Data Career Fair';
        $this->data['is_home'] = false;

        $referensi = $this->initData();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Career Fair');
        $crud->setTable('career_fair');

        if (getSessionRoleAccess() == '1') {
            $crud->setRelation('id_perguruan_tinggi_pelaksana', 'ref_perguruan_tinggi', 'nama_pendek');
        } else {
            $crud->where(['id_perguruan_tinggi_pelaksana' => getPTSessionID()]);
        }

        $crud->columns($referensi['columns']);
        $crud->fields($referensi['fields']);
        $crud->requiredFields($referensi['requireds']);
        $crud->displayAs($referensi['displays']);
        $crud->callbackBeforeInsert([$this, 'callbackBeforeInsert']);
        $crud->callbackBeforeUpdate([$this, 'callbackBeforeUpdate']);
        $crud->callbackBeforeDelete([$this, 'callbackBeforeDelete']);

        $crud->setActionButton('Detail', 'fa fa-th-list', function ($row) {
            return site_url('career_fair/index/detail?key=' . $row->id);
        }, false);

        $crud->callbackColumn('waktu_awal_pendaftaran', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('tanggal_akhir_pendaftaran', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('waktu_awal_pelaksanaan', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('waktu_akhir_pelaksanaan', function ($value, $row) {
            return $this->tanggalindo->konversi_tgl_jam($value);
        });
        $crud->callbackColumn('url_formulir_kepesertaan', function ($value, $row) {
            if ($value != '' || $value != null) {
                $ext = explode('.', $value);
                if ($ext[1] == 'pdf') {
                    return '<a href="' . base_url('public/uploads/job-fairs/' . $value) . '" class="btn btn-info btn-sm" target="_blank"><i class="fas fa-download"></i> Unduh</a>';
                } else {
                    return '<img src="' . base_url('public/uploads/job-fairs/' . $value) . '" class="img-fluid">';
                }
            } else {
                return 'formulir tidak tersedia';
            }
        });
        $crud->unsetExportPdf();
        $crud->unsetPrint();

        $uploadValidations = [
            'maxUploadSize' => '20M', // 20 Mega Bytes
            'minUploadSize' => '1K', // 1 Kilo Byte
            'allowedFileTypes' => [
                'pdf', 'jpeg', 'jpg', 'png'
            ]
        ];
        $crud->setFieldUpload('url_formulir_kepesertaan', 'public/uploads/job-fairs', base_url() . 'public/uploads/job-fairs', $uploadValidations);

        $output = $crud->render();
        $this->_setOutput('career_fair/index/index', $output);
    }

    public function callbackBeforeDelete($params)
    {
        $current = $this->career->edit($params->primaryKeyValue);
        if ($current['data']->url_formulir_kepesertaan != '' || $current['data']->url_formulir_kepesertaan != null) {
            if (file_exists('./public/uploads/job-fairs/' . $current['data']->url_formulir_kepesertaan)) {
                unlink('./public/uploads/job-fairs/' . $current['data']->url_formulir_kepesertaan);
            }
        }
        return $params;
    }

    public function callbackBeforeInsert($params)
    {

        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $data = $params->data;

        $tanggal_mulai = strtotime($data['waktu_awal_pendaftaran']);
        $tanggal_selesai = strtotime($data['tanggal_akhir_pendaftaran']);
        $tanggal_pelaksanaan = strtotime($data['waktu_awal_pelaksanaan']);
        $tanggal_pelaksanaan_akhir = strtotime($data['waktu_akhir_pelaksanaan']);

        if ($tanggal_mulai > $tanggal_selesai) {
            return $errorMessage->setMessage("Tanggal mulai pendaftaran tidak boleh kurang tanggal selesai pendaftaran");
        } else if ($tanggal_mulai > $tanggal_pelaksanaan) {
            return $errorMessage->setMessage("Tanggal mulai pelaksanaan tidak boleh kurang tanggal mulai pendaftaran");
        } else if ($tanggal_pelaksanaan > $tanggal_pelaksanaan_akhir) {
            return $errorMessage->setMessage("Tanggal mulai pelaksanaan tidak boleh kurang tanggal akhir pelaksanaan");
        } else {
            if (getSessionRoleAccess() == '1') {
                $params->data['id_perguruan_tinggi_pelaksana'] = $params->data['id_perguruan_tinggi_pelaksana'];
            } else {
                $params->data['id_perguruan_tinggi_pelaksana'] = getPTSessionID();
            }
            $params->data['created_by'] = getSessionID();
            $params->data['modified_by'] = getSessionID();
            return $params;
        }
    }
    public function callbackBeforeUpdate($params)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $data = $params->data;

        $tanggal_mulai = strtotime($data['waktu_awal_pendaftaran']);
        $tanggal_selesai = strtotime($data['tanggal_akhir_pendaftaran']);
        $tanggal_pelaksanaan = strtotime($data['waktu_awal_pelaksanaan']);
        $tanggal_pelaksanaan_akhir = strtotime($data['waktu_akhir_pelaksanaan']);

        if ($tanggal_mulai > $tanggal_selesai) {
            return $errorMessage->setMessage("Tanggal mulai pendaftaran tidak boleh kurang tanggal selesai pendaftaran");
        } else if ($tanggal_mulai > $tanggal_pelaksanaan) {
            return $errorMessage->setMessage("Tanggal mulai pelaksanaan tidak boleh kurang tanggal mulai pendaftaran");
        } else if ($tanggal_pelaksanaan > $tanggal_pelaksanaan_akhir) {
            return $errorMessage->setMessage("Tanggal mulai pelaksanaan tidak boleh kurang tanggal akhir pelaksanaan");
        } else {

            $current = $this->career->edit($params->primaryKeyValue);
            if ($data['url_formulir_kepesertaan'] != '' || $data['url_formulir_kepesertaan'] != null) {
                if ($current['data']->url_formulir_kepesertaan != $data['url_formulir_kepesertaan']) {
                    if ($current['data']->url_formulir_kepesertaan != '' || $current['data']->url_formulir_kepesertaan != null) {
                        if (file_exists('./public/uploads/job-fairs/' . $current['data']->url_formulir_kepesertaan)) {
                            unlink('./public/uploads/job-fairs/' . $current['data']->url_formulir_kepesertaan);
                        }
                    }
                }
            } else {
                $params->data['url_formulir_kepesertaan'] = $current['data']->url_formulir_kepesertaan;
            }

            if (getSessionRoleAccess() == '1') {
                $params->data['id_perguruan_tinggi_pelaksana'] = $params->data['id_perguruan_tinggi_pelaksana'];
            } else {
                $params->data['id_perguruan_tinggi_pelaksana'] = getPTSessionID();
            }
            $params->data['created_by'] = getSessionID();
            $params->data['modified_by'] = getSessionID();
            return $params;
        }
    }

    public function initData()
    {
        $columns = [];
        $fields = [];
        $requireds = [];
        $displays = [];

        if (getSessionRoleAccess() == '1') {
            $columns = [
                'nama_job_fair',
                'tempat',
                'id_perguruan_tinggi_pelaksana',
                'waktu_awal_pendaftaran',
                'tanggal_akhir_pendaftaran',
                'waktu_awal_pelaksanaan',
                'waktu_akhir_pelaksanaan',
                'url_formulir_kepesertaan',
            ];

            $fields = [
                'nama_job_fair',
                'tempat',
                'id_perguruan_tinggi_pelaksana',
                'waktu_awal_pendaftaran',
                'tanggal_akhir_pendaftaran',
                'waktu_awal_pelaksanaan',
                'waktu_akhir_pelaksanaan',
                'url_formulir_kepesertaan',
            ];
            $requireds = [
                'nama_job_fair',
                'tempat',
                'id_perguruan_tinggi_pelaksana',
                'waktu_awal_pendaftaran',
                'tanggal_akhir_pendaftaran',
                'waktu_awal_pelaksanaan',
                'waktu_akhir_pelaksanaan',
            ];
        } else {
            $columns = [
                'nama_job_fair',
                'tempat',
                'waktu_awal_pendaftaran',
                'tanggal_akhir_pendaftaran',
                'waktu_awal_pelaksanaan',
                'waktu_akhir_pelaksanaan',
                'url_formulir_kepesertaan',
            ];

            $fields = [
                'nama_job_fair',
                'tempat',
                'waktu_awal_pendaftaran',
                'tanggal_akhir_pendaftaran',
                'waktu_awal_pelaksanaan',
                'waktu_akhir_pelaksanaan',
                'url_formulir_kepesertaan',
            ];
            $requireds = [
                'nama_job_fair',
                'tempat',
                'waktu_awal_pendaftaran',
                'tanggal_akhir_pendaftaran',
                'waktu_awal_pelaksanaan',
                'waktu_akhir_pelaksanaan',
            ];
        }

        $displays = [
            'nama_job_fair' => 'Nama Job Fair',
            'tempat' => 'Tempat Pelaksanaan',
            'id_perguruan_tinggi_pelaksana' => 'Perguruan Tinggi Pelaksana',
            'waktu_awal_pendaftaran' => 'Waktu Pendaftaran',
            'tanggal_akhir_pendaftaran' => 'Batas Pendaftaran',
            'waktu_awal_pelaksanaan' => 'Waktu Pelaksanaan',
            'waktu_akhir_pelaksanaan' => 'Batas Pelaksanaan',
            'url_formulir_kepesertaan' => 'Formulir Kepesertaan',
        ];

        $data['columns']    = $columns;
        $data['fields']     = $fields;
        $data['requireds']  = $requireds;
        $data['displays']   = $displays;
        return $data;
    }

    public function detail()
    {
        $this->data['data']     = $data = $this->career->edit($this->input->get('key'));;
        $this->data['title']    = 'Detail Career Fair' . ($data['status'] == 201 ? ' ' . $data['data']->nama_job_fair : '');
        $this->data['is_home']  = false;
        $this->data['scripts']  = ['career_fair/js/detail.js'];
        $this->render('detail');
    }

    public function mitra()
    {
        $return = [];

        $field = [
            'sSearch',
            'iSortCol_0',
            'sSortDir_0',
            'iDisplayStart',
            'iDisplayLength',
            'id_career',
        ];

        foreach ($field as $v) {
            $$v = $this->input->get_post($v);
        }

        $return = [
            "sEcho"                 => $this->input->post('sEcho'),
            "iTotalRecords"         => 0,
            "iTotalDisplayRecords"  => 0,
            "aaData"                => []
        ];

        $params = [
            'sSearch'   => $sSearch,
            'start'     => $iDisplayStart,
            'limit'     => $iDisplayLength,
            'id_career' => $id_career,
        ];

        $data = $this->career->mitra($params);
        if ($data['total'] > 0) {
            $return['iTotalRecords'] = $data['total'];
            $return['iTotalDisplayRecords'] = $return['iTotalRecords'];

            foreach ($data['rows'] as $k => $row) {

                $row['no']                  = '<p class="text-center">' . ($iDisplayStart + ($k + 1)) . '</p>';
                $row['nama']                = $row['nama'];
                $row['jenis']               = $row['jenis'] . ' / ' . $row['sektor'];
                $row['waktu_pendaftaran']   = $row['waktu_pendaftaran'];
                $row['paket_kerjasama']     = $row['paket'];
                $row['status']              = ($row['is_approved'] == '1' ? 'Diterima' : ($row['is_approved'] == '2' ? 'Tidak Diterima' : 'Belum Diverifikasi'));
                $row['formulir']            = ($row['url_formulir'] != null ? '<a href="' . base_url($row['url_formulir']) . '" class="btn btn-info btn-sm" target="_blank"><i class="fa fa-download"></i> Unduh</a>' : '');

                $kelola = '';
                if ($row['is_approved'] == 0) {
                    $kelola = '<button type="button" class="btn btn-primary btn-sm" id="btn-validasi" onclick=validasi(' . $row['id'] . ')><i class="fa fa-check"></i></button>';
                }

                if ($row['is_approved'] == 1) {
                    $kelola = '<span class="badge badge-success">Sudah diverifikasi</span>';
                }

                if ($row['is_approved'] == 2) {
                    $kelola = '<span class="badge badge-danger">Ditolak</span>';
                }
                $row['kelola'] = $kelola;


                $return['aaData'][] = $row;
            }
        }
        $this->db->flush_cache();
        echo json_encode($return);
    }

    public function verifyCareer()
    {
        $id = $this->input->get('id');

        $response = '';

        $this->db->trans_begin();
        $dbError = [];

        $ubahPendaftar = $this->career->verifyCareer(['is_approved' => '1'], ['id' => $id]);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $response = [
                'success' => false,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal verifikasi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $response = [
            'success' => true,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil verifikasi',
            ],
        ];

        End:
        echo json_encode($response);
    }

    public function tolakCareer()
    {
        $id = $this->input->get('id');

        $response = '';

        $this->db->trans_begin();
        $dbError = [];

        $ubahPendaftar = $this->career->verifyCareer(['is_approved' => '2'], ['id' => $id]);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $response = [
                'success' => false,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal verifikasi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $response = [
            'success' => true,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil verifikasi',
            ],
        ];

        End:
        echo json_encode($response);
    }
}
