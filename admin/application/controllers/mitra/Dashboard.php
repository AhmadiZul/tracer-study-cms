<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "mitra";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_mitra', 'mitra');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-handshake';
        $this->data['title'] = 'Mitra';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Mitra');
        $crud->setTable('mitra');

        $crud->where([
            'is_active' => '1'
        ]);

        $column = ['nama', 'jenis', 'sektor', 'alamat', 'email', 'telephone', 'status_data'];
        $fieldsDisplay = [
            'nama' => 'Nama Mitra',
            'jenis' => 'Jenis Instansi',
            'alamat' => 'Alamat',
            'email' => 'Email',
            'id_prodi' => "Prodi",
            'id_perguruan_tinggi' => 'Perguruan Tinggi',
            'telephone' => 'No. Telepon',
            'created_by' => 'Password',
            'last_modified_by' => 'Ulang Password',
            'status_data' => 'Verifikasi'
        ];

        $crud->columns($column);
        $crud->displayAs($fieldsDisplay);
        $crud->unsetAdd();
        $crud->unsetEdit();
        // Removes only the PDF button on export
        $crud->unsetExportPdf();
        $crud->unsetPrint();

        $crud->callbackColumn('url_logo', [$this, 'callPhoto']);
        $crud->callbackColumn('status_data', [$this, 'verifikasi_mitra']);

        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('mitra/dashboard/ubahMitra?key=' . $row->id_instansi);
        }, false);

        $crud->callbackDelete(array($this, 'deleteMitra'));

        $output = $crud->render();
        $this->_setOutput('mitra/dashboard/index', $output);
    }

    public function deleteMitra($id)
    {
        return $this->db->update('mitra', array('is_active' => '0', 'last_modified_by' => $this->session->userdata("tracer_userId")), array('id_instansi' => $id->primaryKeyValue));
    }

    function _urlphoto($id_instansi)
    {
        $mitra = $this->mitra->getMitra($id_instansi);

        $id_instansi = str_replace("-", "", $id_instansi);

        if (!file_exists('public/uploads/mitra/')) {
            mkdir('public/uploads/mitra/', 0755, true);
        }
        if (!file_exists('public/uploads/mitra/' .$id_instansi)) {
            mkdir('public/uploads/mitra/'.$id_instansi, 0755,  true);
        }

        $oldUrlBukti = '';
        if (!empty($mitra)) {
          
            $oldUrlBukti = $mitra['url_logo'];
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/mitra/'.$id_instansi; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'Logo' . $id_instansi;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] ='public/uploads/mitra/'.$id_instansi.'/'. $file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function addMitra()
    {
        $this->data['title'] = 'Tambah Mitra';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('addmitra');
    }

    public function ubahMitra()
    {
        $id = $this->input->get('key');

        if (!$id) {
            redirect('mitra/dashboard');
        }

        $this->data['title'] = 'Edit Mitra';
        $this->data['is_home'] = false;
        $this->data['id_instansi'] = $id;
        $this->data['mitra'] = $this->mitra->getMitra($id);
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->render('editmitra');
    }

    public function createMitra()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_instansi = getUUID();

        $user = array(
            'username' => $this->input->post('email'),
            'email' => $this->input->post('email'),
            'real_name' => $this->input->post('nama'),
            'id_group' => 3,
            'password' => hash('sha256', $this->input->post('password')),
        );

        $id_user = $this->mitra->simpan_user($user);
        $dbError[] = $this->db->error();

        $mitra = array(
            'id_instansi' => $id_instansi,
            'nama' => $this->input->post('nama'),
            'jenis' => $this->input->post('jenis'),
            'sektor' => $this->input->post('sektor'),
            'telephone' => $this->input->post('telephone'),
            'url_web' => $this->input->post('website'),
            'alamat' => $this->input->post('alamat'),
            'email' => $this->input->post('email'),
            'id_user' => $id_user,
            'status_data' => '1',
            'is_active' => '1',
            'created_by' => $this->session->userdata("tracer_userId"),
            'last_modified_by' => $this->session->userdata("tracer_userId")
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_instansi);
            $mitra['url_logo'] = $urlPhoto['file_name'];
        }

        $simpanMitra = $this->mitra->simpan_mitra($mitra);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan mitra',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan mitra',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function editMitra()
    {
        $this->onUpdate = 1;

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_instansi = $this->input->post('id_instansi');
        $mitra = $this->mitra->getMitra($id_instansi);
        $user = array();

        if ($this->input->post('email') != $mitra['email']) {
            $user['email'] = $this->input->post('email');
            $user['username'] = $this->input->post('email');
        }

        if ($this->input->post('nama') != $mitra['nama']) {
            $user['real_name'] = $this->input->post('nama');
        }

        if ($this->input->post('password')) {
            $user['password'] = hash('sha256', $this->input->post('password'));
        }

        if (!empty($user)) {
            $this->mitra->edit_user($user, array('id_user' => $mitra['id_user']));
            $dbError[] = $this->db->error();
        }

        $data = array(
            'nama' => $this->input->post('nama'),
            'jenis' => $this->input->post('jenis'),
            'sektor' => $this->input->post('sektor'),
            'telephone' => $this->input->post('telephone'),
            'url_web' => $this->input->post('website'),
            'alamat' => $this->input->post('alamat'),
            'email' => $this->input->post('email'),
            'last_modified_by' => $this->session->userdata("tracer_userId")
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_instansi);
            $data['url_logo'] = $urlPhoto['file_name'];
        }

        $this->mitra->edit_mitra($data, array('id_instansi' => $id_instansi));
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal mengubah mitra',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengubah mitra',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('nama', 'Nama instansi', 'trim|required|max_length[100]|callback_whitespace|callback_notOnlyNumber');
        $this->form_validation->set_rules('jenis', 'Jenis', 'trim|required');
        $this->form_validation->set_rules('sektor', 'Sektor', 'trim|required');
        $this->form_validation->set_rules('telephone', 'Telepon', 'trim|required|max_length[13]|callback_validHp');
        $this->form_validation->set_rules('website', 'Website', 'trim|required|callback_validUrl|max_length[255]');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|callback_whitespace|callback_notOnlyNumber|max_length[255]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[100]' . ($this->onUpdate ? '|callback_checkMitra[email]|callback_checkUser[email]' : '|is_unique[user.email]|is_unique[mitra.email]'));
        $this->form_validation->set_rules('password', 'Password', 'trim' . ($this->onUpdate ? '' : '|required'));
        $this->form_validation->set_rules('cpassword', 'Konfirmasi password', 'trim' . ($this->onUpdate ? '|matches[password]' : '|required|matches[password]'));

        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('checkMitra', '{field} mitra sudah terdaftar');
        $this->form_validation->set_message('checkUser', '{field} user sudah terdaftar');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');
        $this->form_validation->set_message('validHp', '{field} diawali dengan kode negara (08), minimal 11 digit dan maksimal 13 digit');
        $this->form_validation->set_message('validUrl', '{field} tidak sesuai');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function checkMitra($str, $col)
    {
        if ($str == "") {
            return false;
        }
        return $this->mitra->checkMitra(array($col => $str), $this->input->post('id_instansi'));
    }

    public function checkUser($str, $col)
    {
        if ($str == "") {
            return true;
        }
        $id_user = $this->mitra->getMitra($this->input->post('id_instansi'))['id_user'];
        return $this->mitra->checkUser(array($col => $str), $id_user);
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }
    public function notOnlyNumber($str)
    {
        if (ctype_digit($str)) {
            return false;
        }
        return true;
    }

    public function validHp($str)
    {
        if (preg_match('/^(\+62|62|0)8[1-9][0-9]{6,10}$/', $str)) {
            return true;
        }

        return false;
    }

    function validUrl($url)
    {
        if (preg_match('/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}' . '((:[0-9]{1,5})?\\/.*)?$/i', $url)) {
            return true;
        }

        return false;
    }

    public function verifikasi_mitra($value, $row)
    {
        $data = $this->mitra->getMitra($row->id_instansi);
        if ($row->status_data == '1') {
            return '<p class="fs-7 fw-bold">Sudah Terverifikasi</p>';
        } else {
            $id = 1;
            return '<button type="button" onclick="verifMitra(' . "'" . $data['id_instansi'] . "'" . ')" id="btn-edit-task" class="btn btn-success btn-xs"><i class="fas fa-edit"></i> Verifikasi</button>';
        }
    }

    public function verifMitra()
    {
        $id_instansi = $this->input->post('id_instansi');
        $data = [
            'status_data' => $this->input->post('status_data')
        ];

        $response = $this->mitra->verifMitra($id_instansi, $data);

        echo json_encode($response);
    }

    public function callPhoto($value, $rows)
    {
        $return = '-';
        if ($value != null || $value != "") {
            if (file_exists('./public/uploads/mitra/' . $value)) {
                $return = '<img src="' . base_url('./public/uploads/mitra/' . $value) . '" class="img img-fluid" width="100px" alt="Logo mitra">';
            }
        }
        return $return;
    }
}
