<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "setting_tema";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_tema', 'tema');
    }

    public function index()
    {
        $this->data['title'] = 'Setting Tema';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Tema');

        $crud->setTable('setting_tema');

        /* $crud->callbackColumn('url_vidio', [$this,'callVidio']); */
        $crud->callbackColumn('is_publish', [$this,'publikasi']);

        $crud->columns([
            'nama_tema',
            'is_publish',
        ]);

        $crud->displayAs([
            'nama_tema' => 'Tema',
            'is_publish' => 'Publikasi'
        ]);

        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->unsetDelete();



        $output = $crud->render();
        $this->_setOutput('tema/index', $output);
    }


    public function publikasi($value, $row)
    {
        if($row->is_publish == '1')
        {
            return '<button type="button" onclick="noPublish(' . "'" . $row->id_color . "'" . ')" id_color="btn-edit-task" class="btn btn-warning btn-xs"><i class="fas fa-lock"></i> Stop Publish</button>';
        } else {
            $id_color = 1;
            return '<button type="button" onclick="publish(' . "'" . $row->id_color . "'" . ')" id_color="btn-edit-task" class="btn btn-success btn-xs"><i class="fas fa-unlock"></i> Publish</button>';
        } 
    }

    public function publishAlumni()
    {
        $id_vidio = $this->input->post('id_color');
        $data = [
            'is_publish' => $this->input->post('is_publish')
        ];

        $response = $this->tema->publishTema($id_vidio, $data);

        echo json_encode($response);

    }


    public function deleteAlumni($id)
    {
        return $this->db->update('alumni', array('is_active' => '0', 'last_action_user' => "UPDATE"), array('id_alumni' => $id->primaryKeyValue));
    }

    public function detail($id)
    {
        $this->data['title'] = 'Detail Vidio Landing';
        $this->data['is_home'] = false;
        $this->data['id_vidio'] = $id;
        $this->data['vidio'] = $vidio = $this->vidio->getVidio($id);

        $this->render('detail');
    }

}
