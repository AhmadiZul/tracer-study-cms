<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "user_akses";
    protected $onUpdate = 0;
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_user', 'user');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fa-solid fa-user';
        $this->data['title'] = 'User Akses';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();

        $crud->setTable('user');
        $crud->setRelation('id_group', 'user_group', 'nama_group');
        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('user_akses/index/editUser?key=' . $row->id_user);
        }, false);
        $crud->where(['is_active' => '1']);
        $column = ['username', 'password', 'real_name', 'email', 'id_group'];
        $requireds = ['username', 'password', 'real_name', 'email', 'id_group'];

        $fieldsDisplay = [
            'username' => 'Username',
            'password' => 'Password',
            'last_modified_user' => 'Konfirmasi Password',
            'real_name' => "Nama",
            'email' => 'Email',
            'id_group' => 'Role User',
        ];

        $crud->columns($column);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);

        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->unsetExportPdf();
        $crud->unsetPrint();
        $crud->callbackDelete(array($this, 'delete_module'));

        $output = $crud->render();
        $this->_setOutput('user_akses/index/index', $output);
    }


    public function addUser()
    {
        $this->data['title'] = 'Tambah User';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->data['role'] = $this->user->getRole();
        $this->data['fakultas'] = $this->user->getFakultas();
        $this->render('index/add_user.php');
    }

    public function tambahUser()
    {
        // var_dump($this->input->post());
        // die();
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $data = array(
            'username' => $this->input->post('username'),
            'real_name' => $this->input->post('nama'),
            'email' => $this->input->post('email'),
            'id_group' => $this->input->post('role'),
            'password' => hash('sha256', $this->input->post('password')),
            'is_active' => '1',
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );

        if($this->input->post('role') == 2){
            $data['id_fakultas'] = $this->input->post('input_fakultas');
        }

        $this->user->tambahUser($data);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal Menambahkan User',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menambahkan user',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    private function _runValidation()
    {
        $errors = FALSE;

        $id = $this->user->getUser('id_user');

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[3]|max_length[25]');
        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[user.username.' . $id . ']|min_length[3]|max_length[25]');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|is_unique[user.email.' . $id . ']|min_length[5]|max_length[50]|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim' . ($this->onUpdate ? '' : '|required|min_length[3]|max_length[25]'));
        $this->form_validation->set_rules('password2', 'Password', 'trim');
        $this->form_validation->set_rules('cpassword', 'Konfirmasi password', 'trim' . ($this->onUpdate ? '|matches[password]' : '|required|matches[password]'));
        $this->form_validation->set_rules('cpassword2', 'Password', 'trim');
        $this->form_validation->set_rules('role', 'Role', 'trim|required');

        if($_POST['role'] == 2){
            $this->form_validation->set_rules('input_fakultas', 'Fakultas', 'trim|required');
        }
        
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} harus menggunakan @ dan domain');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    private function _runValidation_edit()
    {
        $errors = FALSE;

        $id = $this->user->getUser('id_user');

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[3]|max_length[25]');
        $this->form_validation->set_rules('username', 'Username', 'required|trim|min_length[3]|max_length[25]');
        $this->form_validation->set_rules('password', 'Password', 'min_length[3]|max_length[25]');
        $this->form_validation->set_rules('cpassword', 'Konfirmasi Password', 'min_length[3]|max_length[25]|matches[password]');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|min_length[5]|max_length[50]|valid_email');
        $this->form_validation->set_rules('role', 'Role', 'trim|required');

        if($_POST['role'] == 2){
            $this->form_validation->set_rules('input_fakultas', 'Fakultas', 'trim|required');
        }

        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('valid_email', '{field} harus menggunakan @ dan domain');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }


    public function editUser()
    {
        $id = $this->input->get('key');
        $this->data['title'] = 'Edit User';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $this->data['user'] = $user = $this->user->getUser($id);
        $this->data['role'] = $this->user->getRole($user['id_group']);
        $this->data['fakultas'] = $this->user->getFakultas();
        $this->render('index/edit_user.php');
    }



    public function editSimpan()
    {

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation_edit();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id = $this->input->post('id_user');
        $user = array();


        if (!empty($this->input->post('email'))) {
            $user['email'] = $this->input->post('email');
        }

        if (!empty($this->input->post('username'))) {
            $user['username'] = $this->input->post('username');
        }

        if (!empty($this->input->post('nama'))) {
            $user['real_name'] = $this->input->post('nama');
        }

        if ($this->input->post('password')) {
            $user['password'] = hash('sha256', $this->input->post('password'));
        }
        if ($this->input->post('role')) {
            $user['id_group'] = $this->input->post('role');
        }

        if($this->input->post('role') == 2){
            $data['id_fakultas'] = $this->input->post('input_fakultas');
        }

        // var_dump($id);
        // die();
        $result = $this->user->editUser($user, $id);
        $dbError[] = $this->db->error();



        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal mengubah user',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengubah user',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function delete_module($id)
    {
        $this->db->where('id_user', $id->primaryKeyValue);
        $user = $this->db->get('user')->row();
        if (empty($user))
            return false;

        $deleteAlumni = $this->user->deleteUserAlumni($id->primaryKeyValue);
        $deleteLog = $this->user->deleteUserLog($id->primaryKeyValue);
        $deleteUser = $this->user->deleteUser($id->primaryKeyValue);

        if($deleteUser){
            return true;
        }
        return false;
    }

    public function valid_email($email)
    {
        $pattern_email = '/^\\S+@\\S+\\.\\S+$/';

        if ($email == '' || empty($email) || $email == null) {
            return true;
        }

        if (!preg_match($pattern_email, $email)) {
            return false;
        }

        return true;
    }
}
