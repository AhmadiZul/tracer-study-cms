<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "hasil_survey_mitra";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_hasil_survey_mitra','hasil_survey');
    }

    public function index()
    {
        $id_survey = $this->input->get('id_survey');

        if ($id_survey == "") {
            redirect(base_url('jadwal_survey/index'));
        }
        $this->data['title'] = 'Hasil Survey';
        $this->data['is_home'] = false;
        $this->data['question'] = $this->getAnswer($id_survey);
        $this->render('index');
    }

    public function getAnswer($id_survey)
    {
        $question = $this->hasil_survey->get_question();
        $no = 1;

        foreach ($question as $key => $value) {
            $value->no = $no;
            if ($value->question_type == 1 || $value->question_type == 2) {
                $answer = $this->getAnswer1And2($value->id_kuesioner, $id_survey);
                $value->answer = (!empty($answer['answer'])? $answer['answer'] : null);
            }

            if ($value->question_type == 3) {
                $answer = $this->getAnswer3($value->id_kuesioner, $id_survey);
                $value->answer = json_encode($answer['grafik']);
                $value->is_isian = 0;

                if (!empty($answer['isian'])) {
                    $value->opsi = $answer['opsi'];
                    $value->isian = $answer['isian'];
                    $value->is_isian = 1;
                }
                
            }

            if ($value->question_type == 4) {
                $lainnya = 0;

                if ($value->others == "lainnya[on]") {
                    $lainnya = 1;
                }
                $answer = $this->getAnswer4($value->id_kuesioner, $id_survey, $lainnya);
                $value->answer = json_encode($answer['grafik']);
                $value->is_lainnya = 0;

                if (!empty($answer['lainnya'])) {
                    $value->lainnya = $answer['lainnya'];
                    $value->is_lainnya = 1;
                }
            }

            if ($value->question_type == 5) {
                $answer = $this->getAnswer5($value->id_kuesioner, $id_survey);

                $value->pertanyaan = $answer['pertanyaan'];
                $value->answer = $answer['grafik'];
            }

            if ($value->question_type == 6) {
                $answer = $this->getAnswer6($value->id_kuesioner, $id_survey);

                $value->answer = $answer;
            }

            if ($value->question_type == 7) {
                $answer = $this->getAnswer7($value->id_kuesioner, $id_survey);
                $value->answer = (!empty($answer['answer'])? $answer['answer'] : null);
            }
            $no++;
        }

        // echo'<pre>';
        // print_r($question);
        // echo'</pre>';
        // die;
        return $question;
    }

    public function getAnswer1And2($id_kuesioner, $id_survey)
    {
        $isian = $this->hasil_survey->get_jawaban_isian($id_kuesioner, $id_survey, false);
        $return = array();

        foreach ($isian as $key => $value) {
            $return['answer'][] = $value->text;
        }

        return $return;
    }

    public function getAnswer3($id_kuesioner, $id_survey)
    {
        $option = $this->hasil_survey->get_option_by_question($id_kuesioner);
        $return = array();

        foreach ($option as $key => $value) {
            $return['grafik'][] = array(
                'name' => $value->opsi,
                'y' => $this->hasil_survey->get_jawaban_option($id_kuesioner, $id_survey, $value->id_opsi, true)
            );

            if ($value->isian == "isian[on]") {
                $return['opsi'][] = array(
                    'id_opsi' => $value->id_opsi,
                    'opsi' => $value->opsi,
                );
                $isian = $this->hasil_survey->get_jawaban_option($id_kuesioner, $id_survey, $value->id_opsi, false);

                foreach ($isian as $key => $value2) {
                    $text = json_decode($value2->text);
                    $return['isian'][$value->id_opsi] = $text->{$value->id_opsi};
                }
            }
        }

        return $return;
    }

    public function getAnswer4($id_kuesioner, $id_survey, $lainnya)
    {
        $option = $this->hasil_survey->get_option_by_question($id_kuesioner);
        $return = array();

        foreach ($option as $key => $value) {
            $return['grafik'][] = array(
                'name' => $value->opsi,
                'y' => $this->hasil_survey->get_jawaban_option($id_kuesioner, $id_survey, $value->id_opsi, true)
            );
        }

        if ($lainnya) {
            $return['grafik'][] = array(
                'name' => 'Lainnya',
                'y' => $this->hasil_survey->get_jawaban_option($id_kuesioner, $id_survey, 'lainnya', true)
            );

            $textLainnya = $this->hasil_survey->get_jawaban_option($id_kuesioner, $id_survey, 'lainnya', false);

            foreach ($textLainnya as $key => $value2) {
                $text = json_decode($value2->text);

                for ($i=0; $i < count($text); $i++) { 
                    if (is_object($text[$i])) {
                        $return['lainnya'][] = $text[$i]->{'lainnya[on]'};
                    }
                }
            }
        }

        return $return;
    }

    public function getAnswer5($id_kuesioner, $id_survey)
    {
        $likert = $this->hasil_survey->get_likert_by_question($id_kuesioner);
        $answer = $this->hasil_survey->get_jawaban_likert($id_kuesioner, $id_survey);
        $nilai_likert = array('1. Sangat Tidak Setuju (STS)', '2. Tidak Setuju (TS)', '3. Ragu-ragu (RG)', '4. Setuju (S)', '5. Sangat Setuju (SS)');
        $nilai_likert2 = array('1. Tidak sama sekali', '2. Kurang', '3. Cukup Besar', '4. Besar', '5. Sangat Besar');
        $return = array();
        $temp = array();

        foreach ($likert as $key => $value) {
            $temp[$value->id_likert]['skala'] = array();

            for ($i=0; $i < 5; $i++) { 
                $temp[$value->id_likert]['skala'][$i] = 0;
            }
        }

        foreach ($answer as $key => $value) {
            $text = json_decode($value->text);

            foreach ($likert as $key2 => $value2) {
                $temp[$value2->id_likert]['skala'][($text->{$value2->id_likert})-1]++;
            }
            
        }

        $huruf = 'A';
        foreach ($likert as $key => $value) {
            $text_linkert = $nilai_likert;
            $tempAnswer = array();

            for ($i=0; $i < 5; $i++) {
                $tempAnswer[] = array(
                    'name' => $text_linkert[$i],
                    'y' =>  $temp[$value->id_likert]['skala'][$i]
                );
            }

            $return['pertanyaan'][] = array(
                'id_likert' => $value->id_likert,
                'pertanyaan' => $huruf.'. '.$value->pertanyaan
            );
            $return['grafik'][$value->id_likert] = json_encode($tempAnswer);

            $huruf++;
        }

        return $return;
    }

    public function getAnswer6($id_kuesioner, $id_survey)
    {
        $sub = $this->hasil_survey->get_sub_by_question($id_kuesioner);
        $answer = $this->hasil_survey->get_jawaban_sub($id_kuesioner, $id_survey);
        $return = array();
        // $temp = array();

        foreach ($sub as $key => $value) {
            $return[$key]['id_sub'] = $value->id_sub;
            $return[$key]['pertanyaan'] = $value->pertanyaan;
        }

        foreach ($answer as $key => $value) {
            $text = json_decode($value->text);

            foreach ($sub as $key2 => $value2) {
                $return[$key2]['answer'][] = $text->{$value2->id_sub};
            }
            
        }

        return $return;
    }

    public function getAnswer7($id_kuesioner, $id_survey)
    {
        $isian = $this->hasil_survey->get_jawaban_isian($id_kuesioner, $id_survey, false);
        $return = array();

        foreach ($isian as $key => $value) {
            $text = json_decode($value->text);

            $provinsi = '';
            $kab_kota = '';
            if ($text->kode_prov != '') {
                $provinsi = $this->hasil_survey->get_provinsi_by_id($text->kode_prov)->nama_provinsi;
            }
            if ($text->kode_kab != '') {
                $kab_kota = $this->hasil_survey->get_kab_kota_by_id($text->kode_kab)->nama_kab_kota;
            }
            $return['answer'][] = $provinsi . ', ' . $kab_kota;
        }

        return $return;
    }

    function getRandomWord($len = 5) {
        $word = array_merge(range('a', 'z'), range('A', 'Z'));
        shuffle($word);
        return substr(implode($word), 0, $len);
    }
}
