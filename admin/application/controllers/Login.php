<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Login extends BaseController
{

    public $loginBehavior = false;
    public $template = "login_app";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {
        if ($this->session->userdata('tracer_userId')!="") {
            // redirect(site_url('index'));
            $this->data["errorMessage"] = "Gagal login, silahkan periksa kembali informasi akun login Anda.";
        }
        $this->data['title'] = 'Login';
        $this->data['is_full'] = true;
        $this->data["tahun"] = $tahun2= setTahun();
        $this->data["tahun_now"] = $tahun = date('Y');
        $this->data['captcha'] = $this->create_captcha();
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $this->sistem->logo_utama();
        $this->data['logo_title'] = $this->sistem->logo_title();
        
        $template = $this->load->view("template/" . $this->template, $this->data, true);
        $content = $this->load->view('login/index', $this->data, true);
        exit(str_replace("{CONTENT}", $content, $template));
    }

    public function alumni()
    {
        $this->data['title'] = 'alumni';
        $this->data['is_full'] = true;
        $this->renderTo('login/alumni');
    }

    public function mitra()
    {
        $this->data['title'] = 'mitra';
        $this->data['is_full'] = true;
        $this->renderTo('login/mitra');
    }

    public function fetchCaptha()
    {
        $response['data'] = $this->create_captcha();
        echo json_encode($response);
    }

}
