<?php

use GroceryCrud\Core\Error\ErrorMessage;

defined('BASEPATH') or exit('No direct script access allowed');

class Index extends BaseController {


	public function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->data['title'] = 'Tips Karir';
        $this->data['is_home'] = false;
		$crud = new Grid();
		$crud->setSkin('bootstrap-v4');
		$crud->setSubject('Tips Karir');
		$crud->setTable('tips_karir');
		$crud->setRelation('id_user','user','real_name',['id_group'=>['1','2','3']]);
		$crud->defaultOrdering('create_time', 'desc');
		$crud->columns(['judul','info_tips','image_tips','publish_time','id_user']);
		$crud->unsetJquery();
		$crud->unsetExportPdf();
		$crud->unsetPrint();
		$crud->displayAs([
		'judul'=> 'Judul',
		'info_tips' => 'Konten',
		'image_tips' => 'Thumbnail',
		'publish_time' => 'Tanggal Publikasi',
		'id_user' => 'Penulis',
		]);
		$crud->fields(['judul','info_tips','image_tips']);
		// $crud->fields(['judul','info_tips','image_tips','is_dummy','is_full_view']);
		$crud->fieldType('is_dummy', 'dropdown_search', [
			'0' => 'Dummy',
			'1' => 'Real',
		]);
		$crud->fieldType('is_full_view', 'dropdown_search', [
			'0' => 'Default',
			'1' => 'Fullview',
		]);
		$uploadValidations = [
			'maxUploadSize' => '20M', // 20 Mega Bytes
			'minUploadSize' => '1K', // 1 Kilo Byte
			'allowedFileTypes' => [
				'gif', 'jpeg', 'jpg', 'png', 'tiff'
			]
		];
		$crud->setFieldUpload('image_tips', 'public/uploads/berita', base_url() . 'public/uploads/berita', $uploadValidations);
		$crud->requiredFields(['judul','info_tips','image_tips']);
		$crud->setTexteditor(['info_tips', 'full_text']);
		$crud->callbackColumn('image_tips', function ($value, $row) {
			return "<img src='".base_url('/public/uploads/berita/'.$value)."' class='img img-responsive rounded' height='100px'>";
		});

		$crud->callbackBeforeDelete([$this, '_beforeDelete']);
		$crud->callbackBeforeUpdate([$this, '_beforeUpdate']);

		// Validation
		$crud->callbackBeforeInsert(function($value){
			$slug = preg_replace('/[^A-Za-z0-9\-\*\& ]/', '', trim($value->data['judul']));
			$value->data['id_user'] = $this->session->userdata("tracer_userId");
			$value->data['is_active'] = '1';
			$value->data['slug'] = str_replace(' ','-',$slug);
			$value->data['create_time'] = date("Y-m-d H:i:s", time());
			$value->data['publish_time'] = date("Y-m-d H:i:s", time());

			return $value;
		});

		$output = $crud->render();
		$this->_setOutput('berita/index',$output);
	}

	public function _beforeUpdate($stateParameters)
	{
		$get = $this->db->query("SELECT * FROM tips_karir where id=?", array($stateParameters->primaryKeyValue));
		$stateParameters->data['slug'] = str_replace(' ','-',$stateParameters->data['judul']);
		return $stateParameters;
	}

	public function _beforeDelete($stateParameters)
	{
		$get = $this->db->query("SELECT * FROM tips_karir where id=?", array($stateParameters->primaryKeyValue));
		if ($get->num_rows()!=0) {
			$row = $get->row_array();
			if ($row['image_tips']!=null) {
				if (file_exists('./public/uploads/berita/'.$row['image_tips'])) {
					unlink('./public/uploads/berita/'.$row['image_tips']);
				}
			}
		} else {
			return $stateParameters;
		}
		return $stateParameters;
	}
}
