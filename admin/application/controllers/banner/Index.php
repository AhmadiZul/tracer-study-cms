<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "banner";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_referensi', 'referensi');
        $this->load->model('M_banner','banner');
    }

    public function index()
    {
        $this->data['title'] = 'Setting Banner';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->setSubject('Banner');

        $crud->setTable('banner');
        $crud->columns([
            'nama_banner',
            'keterangan',
            'url_banner',
            'is_publish',
        ]);

        $crud->displayAs([
            'keterangan' => 'Keterangan',
            'nama_banner' => 'Nama Banner',
            'url_banner' => 'Banner',
            'is_publish' => 'Publikasi',
        ]);

        $crud->callbackColumn('is_publish', [$this,'publikasi']);
        $crud->callbackColumn('url_banner', [$this, 'callBanner']);
        $crud->callbackDelete([$this, 'deleteBanner']);
        $crud->editFields([
            'nama_group',
            'is_publish',
            'dbusername',
            'keterangan',
        ]);

        $crud->fieldType('is_publish', 'dropdown', [
            '0' => 'Tidak Dipublish',
            '1' => 'Publish'
        ]);

        $crud->fieldType('id_group', 'hidden');

        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('banner/index/edit_banner?key=' . $row->id_banner);
        }, false);

        $output = $crud->render();
        $this->_setOutput('banner/index/index', $output);
    }

    // menampilkan form add banner
    public function add_banner()
    {
        $this->data['title'] = 'Tambah Banner';
        $this->data['is_home'] = false;
        $this->data['perguruan_tinggi'] = $this->referensi->getUniv();
        $this->render('index/add_banner');
    }

    public function addBanner()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $id_Pt = $this->input->post('input_perguruan_tinggi');
        $data = array(
            'nama_banner' => $this->input->post('input_banner'),
            'keterangan' => $this->input->post('input_keterangan'),
            'is_publish' => '1',
            'id_perguruan_tinggi' => $id_Pt,
            'last_action_user' => "CREATE",
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );

        $realBanner = $_FILES['url_photo']['name'];
        if (isset($realBanner) && $realBanner != "") {
            $urlPhoto = $this->_urlphoto($id_Pt, $realBanner);
            $data['url_banner'] = $urlPhoto['file_name'];
        }
        $simpanAlumni = $this->banner->tambah_banner($data);

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan banner',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan banner',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('input_banner', 'Nama banner', 'trim|required|max_length[60]|callback_whitespace');
        $this->form_validation->set_rules('input_keterangan', 'Keterangan', 'trim|required|max_length[60]|callback_whitespace');
        $this->form_validation->set_rules('input_perguruan_tinggi', 'Asal Kampus/Asal Sekolah', 'trim|required');

        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');

        if (empty($_FILES['url_photo']['name']) && empty(($_POST['oldFoto'])))
        {
            $errors[] = [
                'field'   => 'url_photo',
                'message' => 'Foto tidak boleh kosong.',
            ];
        }
        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }

    function _urlphoto($id_Pt, $realBanner, $idBanner = 0)
    {
        $id_Pt = str_replace('-', '', $id_Pt);
        if (!file_exists('public/uploads/banner/' . $id_Pt )) {
            mkdir('public/uploads/banner/' . $id_Pt , 0755, true);
        }

        if ($idBanner) {
            $bannerDb = $this->banner->getBanner(array('id_banner' => $idBanner));
        }
        $oldUrlBukti = '';
        if (!empty($bannerDb)) {
            $oldUrlBukti = $bannerDb->url_banner;
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/banner/' . $id_Pt .  '/'; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = 'banner' . $id_Pt . $realBanner;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = 'public/uploads/banner/' . $id_Pt . '/' . $file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function publikasi($value, $row)
    {
        if($row->is_publish == '1')
        {
            return '<button type="button" onclick="noPublish(' . "'" . $row->id_banner . "'" . ')" id="btn-edit-task" class="btn btn-warning btn-xs"><i class="fas fa-lock"></i> Stop Publish</button>';
        } else {
            $id = 1;
            return '<button type="button" onclick="publish(' . "'" . $row->id_banner . "'" . ')" id="btn-edit-task" class="btn btn-success btn-xs"><i class="fas fa-unlock"></i> Publish</button>';
        } 
    }

    public function publishBanner()
    {
        $id_banner = $this->input->post('id_banner');
        $data = [
            'is_publish' => $this->input->post('is_publish')
        ];

        $response = $this->banner->publishBanner($id_banner, $data);

        echo json_encode($response);

    }

    public function callBanner($value, $rows)
    {
        $return = '-';
        if ($value!=null || $value!="") {
            if (file_exists($value)) {
                $return = '<img src="'.base_url().$value.'" class="img img-fluid" width="100px" alt="Alumni Berprestasi">';
            }
        }
        return $return;
    }

    public function deleteBanner($id)
    {
        $this->db->where('id_banner', $id->primaryKeyValue);
        $banner = $this->db->get('banner')->row();
        if(empty($banner))
            return false;
    
        unlink($banner->url_banner);
        $this->db->where('id_banner', $id->primaryKeyValue);
        $this->db->delete('banner');
        return true;
    }

    // show form edit banner
    public function edit_banner()
    {
        $this->data['title'] = 'Edit Banner';
        $this->data['is_home'] = false;
        $this->data['perguruan_tinggi'] = $this->referensi->getUniv();
        $this->data['banner'] = $this->banner->getBanner(array('id_banner' => $this->input->get('key')));
        $this->render('index/edit_banner');
    }

    public function editBanner()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $data = array();

        $dbError[] = $this->db->error();

        $id_Pt = $this->input->post('input_perguruan_tinggi');
        $idBanner = $this->input->post('id_banner');
        $data = array(
            'nama_banner' => $this->input->post('input_banner'),
            'keterangan' => $this->input->post('input_keterangan'),
            'is_publish' => $this->input->post('is_publish'),
            'id_perguruan_tinggi' => $id_Pt,
            'last_action_user' => "UPDATE",
            'last_modified_user' => $this->session->userdata("tracer_userId")
        );

        if($_FILES['url_photo']['name']){
            $realBanner = $_FILES['url_photo']['name'];
            $urlPhoto = $this->_urlphoto($id_Pt, $realBanner, $idBanner);
            $data['url_banner'] = $urlPhoto['file_name'];
        }else{
            $data['url_banner'] = $this->input->post('oldFoto');
        }

        $editBanner = $this->banner->editBanner($data, $idBanner);

        $dbError[] = $this->db->error(); 

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan banner',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengedit banner',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
}
