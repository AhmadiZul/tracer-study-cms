<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "jadwal_survey";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_survey', 'survey');
        $this->load->model('M_referensi', 'referensi');
        $this->load->model('M_sistem', 'sistem');
        $this->load->model('M_user', 'user');
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-calendar-days';
        $this->data['title'] = 'Jadwal Survey';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->setSubject('Jadwal Survey');
        $crud->setTable('ref_jadwal_survey');
        $crud->setRelation('id_ref_jenis_survey', 'ref_jenis_survey', 'jenis_survey');
        $crud->setRelation('untuk_tahun_lulus', 'ref_tahun', 'tahun');
        $crud->setRelation('untuk_id_fakultas', 'ref_fakultas', 'nama_fakultas');
        $crud->setRelation('untuk_id_prodi', 'ref_prodi', 'nama_prodi');


        $crud->where([
            'ref_jadwal_survey.is_active' => '1',
            'untuk_tahun_lulus' => $this->session->userdata('tracer_tahun')
        ]);

        $column = ['id_ref_jenis_survey', 'untuk_tahun_lulus', 'untuk_id_fakultas', 'untuk_id_prodi', 'start_date', 'end_date', 'waktu_acara', 'waktu_berakhir'];
        $fields = ['id_ref_jenis_survey', 'untuk_id_prodi', 'untuk_tahun_lulus', 'start_date', 'end_date'];
        $requireds = ['id_ref_jenis_survey', 'untuk_tahun_lulus', 'untuk_id_prodi', 'start_date', 'end_date'];
        $fieldsDisplay = [
            'id_ref_jenis_survey' => 'Jenis Survey',
            'untuk_tahun_lulus' => 'Tahun Lulus',
            'untuk_id_fakultas' => 'Fakultas',
            'untuk_id_prodi' => 'Prodi',
            'start_date' => "Tanggal Mulai",
            'end_date' => 'Tanggal Selesai',
            'waktu_acara' => 'Waktu Survey',
            'Waktu_berakhir' => 'Survey Berakhir'
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);

        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return base_url('jadwal_survey/index/edit_survey?id_survey=' . $row->id_survey);
        }, false);
        $crud->setActionButton('Hasil', 'fa fa-search', function ($row) {
            return base_url('hasil_survey?id_survey=' . $row->id_survey);
        }, false);

        $crud->editFields(['id_ref_jenis_survey', 'untuk_id_prodi', 'untuk_tahun_lulus', 'start_date', 'end_date']);

        $crud->callbackBeforeInsert([$this, '_callBeforeInsert']);
        $crud->callbackBeforeUpdate([$this, '_callBeforeUpdate']);

        $user_id = $this->session->userdata("tracer_userId");
        $this->data['user'] = $user = $this->user->getUser($user_id);
        if ($this->session->userdata("tracer_idGroup") == 2) {
            $crud->where(['untuk_id_fakultas' => $user['id_fakultas']]);
            $crud->unsetSearchColumns(['untuk_id_fakultas', 'untuk_id_prodi']);
        }
        $output = $crud->render();
        $this->_setOutput('jadwal_survey/index', $output);
    }

    public function _callBeforeInsert($params)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $cek = $this->survey->cekData($params->data, 0);
        if ($cek['status'] == 500) {
            return $errorMessage->setMessage($cek['message']);
        } else {
            $params->data['id_survey'] = getUUID();
            $params->data['inserted_by'] = $this->session->tracer_userId;
        }
        return $params;
    }

    public function _callBeforeUpdate($params)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $cek = $this->survey->cekData($params->data, 1, $params->primaryKeyValue);
        if ($cek['status'] == 500) {
            return $errorMessage->setMessage($cek['message']);
        } else {
            $params->data['id_survey'] = getUUID();
            $params->data['inserted_by'] = $this->session->tracer_userId;
        }
        return $params;
    }

    public function delete_jadwal($id)
    {
        return $this->db->update('ref_jadwal_survey', array('is_active' => '0'), array('id_survey' => $id->primaryKeyValue));
    }
    
    /**
     * add_survey
     * menampilkan halaman tambah jadwal survey
     * @return void
     */
    public function add_survey()
    {
        $this->data['title'] = 'Tambah Jadwal Survey';
        $this->data['back'] = ' &nbsp/ Jadwal Survey';
        $this->data['url_back'] = 'jadwal_survey/index';
        $this->data['is_home'] = false;
        $this->data['fakultas'] = $this->referensi->getFakultas();
        $this->data['jenis_survey'] = $this->referensi->getJenisSurvey();
        $this->data['tahun_lulus'] = $this->referensi->getTahunLulus();
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $this->data['fakultas'] = $fakultas = $this->referensi->getFakultas();

        $user_id = $this->session->userdata("tracer_userId");
        $this->data['user'] = $user = $this->user->getUser($user_id);
        $this->data['prodi'] = $this->referensi->getProdiByFakultas($user['id_fakultas']);

        foreach ($fakultas as $key => $value) {
            if ($value['id_fakultas'] == $user['id_fakultas']) {
                $this->data['id_fakultas'] = $value['id_fakultas'];
                $this->data['nama_fakultas'] = $value['nama_fakultas'];
            }
        }
        $this->render('tambah');
    }
    
    /**
     * createSurvey
     * menambahkan jadwal survey
     * @return void
     */
    public function createSurvey()
    {
        $input = $this->input->post();

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $dbError[] = $this->db->error();

        $id = getUUID();
        $jadwal_survey = array(
            'id_survey' => $id,
            'id_ref_jenis_survey' => $input['jenis_survey'],
            'untuk_id_fakultas' => $input['input_fakultas'],
            'untuk_id_prodi' => $input['input_prodi'],
            'untuk_tahun_lulus' => $input['tahun_lulus'],
            'start_date' => $input['tanggal_mulai'],
            'end_date' => $input['tanggal_selesai'],
            'waktu_acara' => $input['waktu_acara'],
            'waktu_berakhir' => $input['waktu_berakhir'],
            'is_active' => '1',
            'inserted_by'        => $this->session->userdata('tracer_userId'),
            'last_modified_by'  => $this->session->userdata('tracer_userId')
        );

        $this->survey->createJadwalSurvey($jadwal_survey);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan alumni berprestasi',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan Jadwal Survey',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }
    
    /**
     * edit_survey
     * menampilkan halaman form edit survey
     * @return void
     */
    public function edit_survey()
    {
        $this->data['title'] = 'Edit Jadwal Survey';
        $this->data['back'] = ' &nbsp/ Jadwal Survey';
        $this->data['url_back'] = 'jadwal_survey/index';
        $this->data['is_home'] = false;
        $this->data['fakultas'] = $fakultas = $this->referensi->getFakultas();
        $this->data['jenis_survey'] = $this->referensi->getJenisSurvey();
        $this->data['tahun_lulus'] = $this->referensi->getTahunLulus();
        $this->data['jadwal_survey'] = $jadwal_survey = $this->survey->getJadwalSurvey($this->input->get('id_survey'));
        $this->data['prodi'] = $this->referensi->getProdiByFakultas($jadwal_survey->untuk_id_fakultas);
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();

        $user_id = $this->session->userdata("tracer_userId");
        $this->data['user'] = $user = $this->user->getUser($user_id);

        foreach ($fakultas as $key => $value) {
            if ($value['id_fakultas'] == $user['id_fakultas']) {
                $this->data['id_fakultas'] = $value['id_fakultas'];
                $this->data['nama_fakultas'] = $value['nama_fakultas'];
            }
        }
        $this->render('edit');
    }
    
    /**
     * updateSurvey
     * mengedit jadwal survey
     * @return void
     */
    public function updateSurvey()
    { {
            $input = $this->input->post();

            $htmlCodeNumber = 201;
            $response = '';

            $errors = $this->_runValidation();

            if ($errors) {
                $htmlCodeNumber = 400;
                $response = [
                    'code' => $htmlCodeNumber,
                    'message' => [
                        'title' => 'ERROR',
                        'body' => 'Validasi Gagal',
                    ],
                    'data' => $errors,
                ];
                goto End;
            };

            $this->db->trans_begin();
            $dbError = [];

            $dbError[] = $this->db->error();

            $id = $input['id_jadwal'];
            $jadwal_survey = array(
                'id_ref_jenis_survey' => $input['jenis_survey'],
                'untuk_id_fakultas' => $input['input_fakultas'],
                'untuk_id_prodi' => $input['input_prodi'],
                'untuk_tahun_lulus' => $input['tahun_lulus'],
                'start_date' => $input['tanggal_mulai'],
                'end_date' => $input['tanggal_selesai'],
                'waktu_acara' => $input['waktu_acara'],
                'waktu_berakhir' => $input['waktu_berakhir'],
                'is_active' => '1',
                'inserted_by'        => $this->session->userdata('tracer_userId'),
                'last_modified_by'  => $this->session->userdata('tracer_userId')
            );

            $this->survey->updateJadwalSurvey($id, $jadwal_survey);
            $dbError[] = $this->db->error();

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();

                $htmlCodeNumber = 500;
                $response = [
                    'code' => $htmlCodeNumber,
                    'message' => [
                        'title' => 'Gagal',
                        'body'  => 'Gagal menyimpan alumni berprestasi',
                    ],
                    'data' => $dbError,
                ];

                goto End;
            };
            $this->db->trans_commit();

            $htmlCodeNumber = 201;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Success',
                    'body'  => 'Berhasil Mengupdate Jadwal Survey',
                ],
            ];

            End:
            return $this->responseJSON($response, $htmlCodeNumber);
        }
    }    
    /**
     * _runValidation
     * validasi untuk tambah atau edit jadwal survey
     * @return void
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('jenis_survey', 'Jenis Survey', 'trim|required');
        $this->form_validation->set_rules('tahun_lulus', 'Tahun Kelulusan', 'trim|required');
        $this->form_validation->set_rules('input_fakultas', 'Fakultas', 'trim|required');
        $this->form_validation->set_rules('input_prodi', 'Program Studi/Jurusan', 'trim|required');
        $this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'trim|required');
        $this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'trim|required');
        $this->form_validation->set_rules('waktu_acara', 'Waktu mulai', 'trim|required');
        $this->form_validation->set_rules('waktu_berakhir', 'Waktu selesai', 'trim|required');

        $this->form_validation->set_message('required', '{field} wajib dipilih');
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('validUrl', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('checkPT', '{field} sudah terdaftar');
        $this->form_validation->set_message('checkUser', '{field} sudah terdaftar');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');

        // validasi waktu pelaksanaan tidak boleh terbalik
        if (!empty(($_POST['tanggal_mulai'])) && !empty(($_POST['tanggal_selesai']))) {
            $tgl_mulai = date_create($_POST['tanggal_mulai']);
            $tgl_selesai = date_create($_POST['tanggal_selesai']);
            $waktu_acara = $_POST['waktu_acara'];
            $waktu_berakhir = $_POST['waktu_berakhir'];
            $diff = date_diff($tgl_mulai, $tgl_selesai);

            if ($diff->invert) {
                $errors[] = [
                    'field'   => 'cek_tanggal',
                    'message' => 'Tanggal selesai wajib setelah tanggal mulai',
                ];
            } else if ($tgl_mulai == $tgl_selesai) {
                if ($waktu_berakhir <= $waktu_acara) {
                    $errors[] = [
                        'field'   => 'cek_waktu',
                        'message' => 'Tidak bisa memilih waktu mulai melebihi waktu selesai',
                    ];
                }
            };
        }

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }
}
