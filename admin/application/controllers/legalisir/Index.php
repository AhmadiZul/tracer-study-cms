<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "legalisir";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_legalisir','legalisir');
        
    }

    public function index()
    {
        $this->data['title'] = 'Legalisir Dokumen';
        $this->data['is_home'] = false;

        $id_user = null;
        if ($this->session->userdata("tracer_idGroup") == 2) {
            $id_user = $this->session->userdata("tracer_userId");
        }
        
        $this->data['alumni'] = setAlumni($id_user);
        $this->render("index");
    }

    public function table() {
        $return = array();

        $field = array(
            'sSearch',
            'iSortCol_0',
            'sSortDir_0',
            'iDisplayStart',
            'iDisplayLength'
        );

        foreach ($field as $v) {
            $$v = $this->input->get_post($v);
        }

        $return = array(
            "sEcho" => $this->input->post('sEcho'),
            "iTotalRecords" => 0,
            "iTotalDisplayRecords" => 0,
            "aaData" => array()
        );

        $params = array(
            'sSearch' => $sSearch,
            'start' => $iDisplayStart,
            'limit' => $iDisplayLength
        );

        $data = $this->legalisir->dataTable($params);
        if ($data['total'] > 0) {
            $return['iTotalRecords'] = $data['total'];
            $return['iTotalDisplayRecords'] = $return['iTotalRecords'];
            foreach ($data['rows'] as $k => $row) {
                $row['nomor'] = $iDisplayStart + ( $k + 1 );

                if ($row['jenis'] =='Ijazah') {
                    $row['_jenis'] = '<span class="badge badge-square bg-light-violet text-violet">'.$row["jenis"].'</span>';
                } else {
                    $row['_jenis'] = '<span class="badge badge-square bg-light-red text-light-red">'.$row["jenis"].'</span>';
                }

                if ($row['status'] =='Diajukan') {
                    $row['_status'] = '<span class="badge badge-square bg-light-info text-light-info">'.$row["status"].'</span>';
                } elseif ($row['status'] =='Diproses') {
                    $row['_status'] = '<span class="badge badge-square bg-light-warning text-light-warning">'.$row["status"].'</span>';
                } else {
                    $row['_status'] = '<span class="badge badge-square bg-light-success text-success">'.$row["status"].'</span>';
                }

                $btn_edit = '<button id="edit-button" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal-legalisir" data-action="edit" data-id="'.$row['id_legalisir'].'" data-jenis="'.$row['jenis'].'" data-alumni="'.$row['id_alumni'].'" data-jumlah="'.$row['jumlah'].'" data-status="'.$row['status'].'" data-berkas="'.$row['berkas'].'" title="Edit Data"><i class="fa fa-pencil"></i></button>';

                $row['action'] = '<div class="btn-group">'
                         .$btn_edit
                         . '<button class="btn btn-danger btn-xs" data-container="body" data-toggle="modal" data-target="#modal-hapus"  data-id="'.$row['id_legalisir'].'" data-berkas="'.$row['berkas'].'" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></button>'
                         . '</div>';
                $return['aaData'][] = $row;
            }
        }

        $this->db->flush_cache();
        echo json_encode($return);
    }

    public function getAlumni()
    {
        $id_user = null;
        if ($this->session->userdata("tracer_idGroup") == 2) {
            $id_user = $this->session->userdata("tracer_userId");
        }
        $alumni = setAlumni($id_user);
        echo json_encode($alumni);
    }

    function _urlberkas($jenis){
        $idPT = $this->legalisir->getIdPT($this->input->post('id_alumni'))->id_perguruan_tinggi;
        $legalisir = $this->legalisir->getLegalisir(array('id_legalisir' => $this->input->post('id_legalisir')));

        if(!file_exists('public/uploads/perguruan_tinggi/'.$idPT.'/alumni/legalisir/'.$this->input->post('id_alumni'))){
            mkdir('public/uploads/perguruan_tinggi/'.$idPT.'/alumni/legalisir/'.$this->input->post('id_alumni'), 0755, true);
            chmod('public/uploads/perguruan_tinggi/'.$idPT.'/alumni/legalisir/', 0755);
        }
        
        $oldUrlBukti = '';
        if (!empty($legalisir)) {
            $oldUrlBukti = $legalisir->berkas;
        }

        $name_input = 'url_berkas';
        $return['success']=true;
        if(isset($_FILES[$name_input]['name'])&&$_FILES[$name_input]['name']!=""){
            $config['upload_path']='public/uploads/perguruan_tinggi/'.$idPT.'/alumni/legalisir/'.$this->input->post('id_alumni'); //path folder file upload
            $config['allowed_types']='jpg|png|jpeg|pdf'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis').$jenis.$this->input->post('id_alumni');
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)){
                $file = $this->upload->data();
                $return['success']=true;
                $return['file_name']='public/uploads/perguruan_tinggi/'.$idPT.'/alumni/legalisir/'.$this->input->post('id_alumni').'/'.$file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            }else{
                $return['success']=false;
                $return['text']="Gagal Upload, File harus 2 MB atau kurang";
            }
        }else{
            $return['success']=false;
            $return['text']="File Tidak Ada";
        }
        return $return;
    }

    public function simpanLegalisir()
    {
        $data = $this->input->post();

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_legalisir = getUUID();

        $legalisir = array(
            'id_legalisir' => $id_legalisir,
            'id_alumni' => $data['id_alumni'],
            'jenis' => $data['jenis'],
            'jumlah' => $data['jumlah']
        );

        $urlBerkas = $this->_urlBerkas($data['jenis']);
        $legalisir['berkas'] = $urlBerkas['file_name'];

        $this->legalisir->simpan_legalisir($legalisir);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan legalisir',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan legalisir',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function ubahLegalisir()
    {
        $data = $this->input->post();
        $this->onUpdate = 1;

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $legalisir = array(
            'jenis' => $data['jenis'],
            'jumlah' => $data['jumlah'],
            'status' => $data['status']
        );

        if ($this->input->post('is_berkas')) {
            $urlBerkas = $this->_urlBerkas($data['jenis']);
            $legalisir['berkas'] = $urlBerkas['file_name'];
        } else {
            $oldLegalisir = $this->legalisir->getLegalisir(array('id_legalisir' => $this->input->post('id_legalisir')));

            //cek butuh rename atau tidak
            if ($oldLegalisir->jenis != $data['jenis']) {
                $jenisBaru = $data['jenis'];
                $jenisLama = $oldLegalisir->jenis;

                if ($data['jenis'] == "Transkrip Nilai") {
                    $jenisBaru = 'Transkrip_Nilai';
                }

                if ($oldLegalisir->jenis == "Transkrip Nilai") {
                    $jenisLama = 'Transkrip_Nilai';
                }
                
                $legalisir['berkas'] = str_replace($jenisLama,$jenisBaru,$oldLegalisir->berkas);

                if (!rename($oldLegalisir->berkas,$legalisir['berkas'])) {
                    $htmlCodeNumber = 500;
                    $response = [
                        'code' => $htmlCodeNumber,
                        'message' => [
                            'title' => 'Gagal',
                            'body'  => 'Gagal rename',
                        ],
                        'data' => $dbError,
                    ];

                    goto End;
                }
            }
        }

        $this->legalisir->ubah_legalisir($legalisir, array('id_legalisir' => $data['id_legalisir']));
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal mengubah legalisir',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengubah legalisir',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function hapusLegalisir()
    {
        $data = $this->input->post();

        $this->db->trans_begin();
        $dbError = [];

        $this->legalisir->hapus_legalisir($data['id_hapus']);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal mengubah legalisir',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        if (file_exists($data['url_berkas'])) {
            unlink($data['url_berkas']);
        }

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengubah legalisir',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('id_legalisir', 'Id', ($this->onUpdate ? 'required' : ''));
        $this->form_validation->set_rules('id_alumni', 'Alumni', 'required');
		$this->form_validation->set_rules('jenis', 'Jenis dokumen', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required|max_length[11]');
		$this->form_validation->set_rules('status', 'Status dokumen', ($this->onUpdate ? 'required' : ''));

        if (empty($_FILES['url_berkas']['name']) && $this->input->post('is_berkas'))
        {
            $errors[] = [
                'field'   => 'url_berkas',
                'message' => 'Berkas tidak boleh kosong.',
            ];
        }

        $this->form_validation->set_message('required', '{field} tidak boleh kosong.');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter.');

		if ($this->form_validation->run() == FALSE) {
			foreach ($this->input->post() as $field => $value) {
				if (form_error($field)) {
					$errors[] = [
						'field'   => $field,
						'message' => trim(form_error($field, ' ', ' ')),
					];
				};
			};
		};

		return $errors;
    }

}
