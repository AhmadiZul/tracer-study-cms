<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "jadwal_mitra";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_survey_mitra','survey_mitra');  
    }

    public function index()
    {
        $this->data['title'] = 'Jadwal Survey Mitra';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Jadwal Survey Mitra');
        $crud->setTable('ref_jadwal_survey_mitra');

        $crud->where([
            'is_active' => '1'
        ]);

        $column = ['nama','start_date','end_date','inserted_by'];
        $fields = ['nama','start_date','end_date'];
        $requireds = ['nama','start_date','end_date'];
        $fieldsDisplay = [
            'nama'=>'Nama',
            'start_date'=>'Tanggal Mulai',
            'end_date'=>'Tanggal Selesai',
            'inserted_by'=>'Hasil Survey'
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);

        $crud->callbackColumn('inserted_by', [$this,'hasil_jadwal']);
        $crud->callbackDelete(array($this,'delete_jadwal'));

        $crud->editFields(['nama','start_date','end_date']);

        $crud->callbackBeforeInsert([$this, '_callBeforeInsert']);

        $output = $crud->render();
        $this->_setOutput('jadwal_mitra/index', $output);
    }

    public function hasil_jadwal($value, $row)
    {
        return '<a href="'.base_url().'hasil_survey_mitra?id_survey='.$row->id_survey.'" type="button" id="btn-edit-task" class="btn btn-primary btn-xs"><i class="fa fa-search"></i> Hasil</a>';
    }

    public function _callBeforeInsert($params)
    {
        $params->data['id_survey'] = getUUID();
        $params->data['inserted_by'] = $this->session->tracer_userId;
        return $params;
    }

    public function delete_jadwal($id)
    {
        return $this->db->update('ref_jadwal_survey_mitra',array('is_active' => '0'),array('id_survey' => $id->primaryKeyValue));
    }

}
