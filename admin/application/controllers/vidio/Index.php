<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    public $template = "app_back";
    protected $module = "vidio";
    private $id = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_vidio', 'vidio');
    }

    public function index()
    {
        $this->data['title'] = 'vidio';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Vidio');

        $crud->setTable('vidio_landing');

        /* $crud->callbackColumn('url_vidio', [$this,'callVidio']); */
        $crud->callbackColumn('is_publish', [$this,'publikasi']);

        $crud->columns([
            'judul',
            'paragraf',
            'url_vidio',
            'is_publish',
        ]);

        $crud->displayAs([
            'judul' => 'Nama Judul',
            'paragraf' => 'Keterangan',
            'url_vidio' => 'url youtube',
            'is_publish' => 'Publikasi'
        ]);

        $crud->addFields([
            'judul',
            'paragraf',
            'url_vidio',
        ]);

        $crud->editFields([
            'judul',
            'paragraf',
            'url_vidio',
        ]);

        $crud->requiredFields([
            'judul',
            'paragraf',
        ]);

        $crud->setActionButton('Detail', 'fa fa-book', function ($row) {
            return site_url('vidio/Index/detail/' . $row->id_vidio);
        }, false);



        $output = $crud->render();
        $this->_setOutput('vidio/index', $output);
    }


    public function publikasi($value, $row)
    {
        if($row->is_publish == '1')
        {
            return '<button type="button" onclick="noPublish(' . "'" . $row->id_vidio . "'" . ')" id_vidio="btn-edit-task" class="btn btn-warning btn-xs"><i class="fas fa-lock"></i> Stop Publish</button>';
        } else {
            $id_vidio = 1;
            return '<button type="button" onclick="publish(' . "'" . $row->id_vidio . "'" . ')" id_vidio="btn-edit-task" class="btn btn-success btn-xs"><i class="fas fa-unlock"></i> Publish</button>';
        } 
    }

    public function publishAlumni()
    {
        $id_vidio = $this->input->post('id_vidio');
        $data = [
            'is_publish' => $this->input->post('is_publish')
        ];

        $response = $this->vidio->publishVidio($id_vidio, $data);

        echo json_encode($response);

    }


    public function deleteAlumni($id)
    {
        return $this->db->update('alumni', array('is_active' => '0', 'last_action_user' => "UPDATE"), array('id_alumni' => $id->primaryKeyValue));
    }

    public function detail($id)
    {
        $this->data['title'] = 'Detail Vidio Landing';
        $this->data['is_home'] = false;
        $this->data['id_vidio'] = $id;
        $this->data['vidio'] = $vidio = $this->vidio->getVidio($id);

        $this->render('detail');
    }

}
