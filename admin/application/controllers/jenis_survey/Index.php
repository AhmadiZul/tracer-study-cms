<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "jenis_survey";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_survey','survey');
        $this->load->model('M_sistem','sistem');
        
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-scroll';
        $this->data['title'] = 'Jenis Survey';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title= $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->unsetAdd();
        $crud->unsetDelete();
        $crud->setSubject('Jenis Survey');
        $crud->setTable('ref_jenis_survey');
        $crud->setRule('jenis_survey', 'lengthBetween', ['3','200']);
        $crud->setRule('deskripsi', 'lengthBetween', ['3','255']);

        $column = ['jenis_survey', 'deskripsi', 'is_active'];
        $fields = ['jenis_survey', 'deskripsi'];
        $requireds = ['jenis_survey', 'deskripsi'];
        $fieldsDisplay = [
            'jenis_survey'=>'Jenis Survey',
            'deskripsi' => 'Deskripsi',
            'is_active'=>'Aktivasi'
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);

        $crud->editFields(['jenis_survey' , 'deskripsi']);

        $crud->callbackColumn('is_active', [$this,'aktivasi']);	

        $crud->callbackBeforeInsert([$this, '_callBeforeInsert']);

        $output = $crud->render();
        $this->_setOutput('survey/jenis', $output);
    }

    public function _callBeforeInsert($params)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();
        $cek = $this->survey->checkJenis($params->data);
        if ($cek['status']==500) {
            return $errorMessage->setMessage($cek['message']);
        }
        return $params;
    }

    public function aktivasi($value, $row)
    {
        if($row->is_active == '1')
        {
            return '<button type="button" onclick="noAktif(' . "'" . $row->id . "'" . ')" id="btn-edit-task" class="btn btn-warning btn-xs"><i class="fas fa-lock"></i> Tidak Aktif</button>';
        } else {
            $id = 1;
            return '<button type="button" onclick="aktif(' . "'" . $row->id . "'" . ')" id="btn-edit-task" class="btn btn-success btn-xs"><i class="fas fa-unlock"></i> Aktif</button>';
        } 
    }   

    public function aktifJenis()
    {
        $id_jenis = $this->input->post('id_jenis');
        $data = [
            'is_active' => $this->input->post('is_active')
        ];

        $response = $this->survey->aktifJenis($id_jenis, $data);

        echo json_encode($response);

    }

}
