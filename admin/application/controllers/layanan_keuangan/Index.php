<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "layanan_keuangan";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_layanan_keuangan', 'layanan_keuangan');
    }

    public function index()
    {
        $this->data['title'] = 'Layanan Keuangan';
        $this->data['is_home'] = false;
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Layanan Keuangan');
        $crud->setTable('layanan_keuangan');

        $crud->where([
            'is_active' => '1'
        ]);

        $column = ['url_logo', 'nama_bank', 'url_web','visitor'];
        $fieldsDisplay = [
            'url_logo' => 'Logo',
            'nama_bank' => 'Bank',
            'url_web' => 'Website',
            'visitor' => 'Visitor',
        ];

        $crud->columns($column);
        $crud->displayAs($fieldsDisplay);
        $crud->unsetAdd();
        $crud->unsetEdit();

        $crud->callbackColumn('url_logo', [$this, 'callPhoto']);

        $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
            return site_url('layanan_keuangan/index/ubahLayanan?key=' . $row->id_layanan);
        }, false);

        $crud->callbackDelete(array($this, 'deleteLayanan'));

        $output = $crud->render();
        $this->_setOutput('layanan_keuangan/index', $output);
    }

    public function deleteLayanan($id)
    {
        return $this->db->update('layanan_keuangan', array('is_active' => '0', 'last_modified_by' => $this->session->userdata("tracer_userId")), array('id_layanan' => $id->id_layanan));
    }

    function _urlphoto($id_layanan)
    {
        $layanan = $this->layanan_keuangan->getLayanan($id_layanan);

        $id_layanan = str_replace("-", "", $id_layanan);

        if (!file_exists('public/uploads/layanan_keuangan/')) {
            mkdir('public/uploads/layanan_keuangan/', 0755, true);
        }
        if (!file_exists('public/uploads/layanan_keuangan/' .$id_layanan)) {
            mkdir('public/uploads/layanan_keuangan/'.$id_layanan, 0755,  true);
        }

        $oldUrlBukti = '';
        if (!empty($layanan)) {
            $base_url = base_url();
            $oldUrlBukti = str_replace($base_url, '',$layanan->url_logo);
        }

        $name_input = 'url_photo';
        $return['success'] = true;
        if (isset($_FILES[$name_input]['name']) && $_FILES[$name_input]['name'] != "") {
            $config['upload_path'] = 'public/uploads/layanan_keuangan/'.$id_layanan; //path folder file upload
            $config['allowed_types'] = 'jpg|png|jpeg'; //type file yang boleh di upload
            $config['max_size'] = '2100';
            $config['file_name'] = date('YmdHis') . 'Logo' . $id_layanan;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload($name_input)) {
                $file = $this->upload->data();
                $return['success'] = true;
                $return['file_name'] = base_url() . 'public/uploads/layanan_keuangan/'.$id_layanan.'/'. $file['file_name'];
                if (file_exists($oldUrlBukti)) {
                    unlink($oldUrlBukti);
                }
            } else {
                $return['success'] = false;
                $return['text'] = "Gagal Upload, File harus 2 MB atau kurang " . $this->upload->display_errors();
            }
        } else {
            $return['success'] = false;
            $return['text'] = "File Tidak Ada";
        }
        return $return;
    }

    public function addLayanan()
    {
        $this->data['title'] = 'Tambah Layanan Keuangan';
        $this->data['is_home'] = false;
        $this->render('addlayanan');
    }

    public function ubahLayanan()
    {
        $id = $this->input->get('key');

        if (!$id) {
            redirect('layanan_keuangan/index');
        }

        $this->data['title'] = 'Edit Layanan Keuangan';
        $this->data['is_home'] = false;
        $this->data['id_layanan'] = $id;
        $this->data['layanan'] = $this->layanan_keuangan->getLayanan($id);
        $this->render('editlayanan');
    }

    public function createLayanan()
    {
        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_layanan = getUUID();


        $layanan = array(
            'id_layanan' => $id_layanan,
            'nama_bank' => $this->input->post('nama_bank'),
            'url_web' => $this->input->post('url_web'),
            'is_active' => '1',
            'last_modified_by' => $this->session->userdata("tracer_userId")
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_layanan);
            $layanan['url_logo'] = $urlPhoto['file_name'];
        }

        $simpanLayanan = $this->layanan_keuangan->simpan_layanan($layanan);
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal menyimpan layanan keuangan',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil menyimpan layanan keuangan',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    public function editLayanan()
    {
        $this->onUpdate = 1;

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            goto End;
        };

        $this->db->trans_begin();
        $dbError = [];

        $id_layanan = $this->input->post('id_layanan');

        $data = array(
            'nama_bank' => $this->input->post('nama_bank'),
            'url_web' => $this->input->post('url_web'),
            'last_modified_by' => $this->session->userdata("tracer_userId")
        );

        if (isset($_FILES['url_photo']['name']) && $_FILES['url_photo']['name'] != "") {
            $urlPhoto = $this->_urlphoto($id_layanan);
            $data['url_logo'] = $urlPhoto['file_name'];
        }

        $this->layanan_keuangan->edit_layanan($data, array('id_layanan' => $id_layanan));
        $dbError[] = $this->db->error();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $htmlCodeNumber = 500;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'Gagal',
                    'body'  => 'Gagal mengubah layanan keuangan',
                ],
                'data' => $dbError,
            ];

            goto End;
        };
        $this->db->trans_commit();

        $htmlCodeNumber = 201;
        $response = [
            'code' => $htmlCodeNumber,
            'message' => [
                'title' => 'Success',
                'body'  => 'Berhasil mengubah layanan keuangan',
            ],
        ];

        End:
        return $this->responseJSON($response, $htmlCodeNumber);
    }

    /**
     * untuk menjalankan form validasi
     *
     * @return false|array
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('nama_bank', 'Nama Bank', 'trim|required|max_length[100]|callback_whitespace|callback_notOnlyNumber');
        $this->form_validation->set_rules('url_web', 'Website', 'trim|required|max_length[255]');

        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('required', '{field} belum diisi');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('checkMitra', '{field} mitra sudah terdaftar');
        $this->form_validation->set_message('checkUser', '{field} user sudah terdaftar');
        $this->form_validation->set_message('whitespace', '{field} belum diisi');
        $this->form_validation->set_message('notOnlyNumber', '{field} tidak boleh hanya angka');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }

    public function checkMitra($str, $col)
    {
        if ($str == "") {
            return false;
        }
        return $this->mitra->checkMitra(array($col => $str), $this->input->post('id_instansi'));
    }

    public function checkUser($str, $col)
    {
        if ($str == "") {
            return true;
        }
        $id_user = $this->mitra->getMitra($this->input->post('id_instansi'))['id_user'];
        return $this->mitra->checkUser(array($col => $str), $id_user);
    }

    public function whitespace($str)
    {
        if (ctype_space($str)) {
            return false;
        } else {
            return true;
        }
    }
    public function notOnlyNumber($str)
    {
        if (ctype_digit($str)) {
            return false;
        }
        return true;
    }

    public function callPhoto($value, $rows)
    {
        $return = '-';
        $url_logo = $value;
        if ($url_logo != null || $url_logo != '') {
            $return = '<img src="' . $url_logo . '" class="img img-fluid" width="100px" alt="Logo mitra">';
        }
        return $return;
    }
}
