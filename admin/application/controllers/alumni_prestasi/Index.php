<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "alumni";
    protected $onUpdate = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['title'] = 'Alumni';
        $this->data['is_home'] = false;

        $crud = new Grid();

        $crud->setTable('alumni_berprestasi');

        // Please notice that the APPPATH is used only in Codeigniter Framework
        include(APPPATH . 'models/grocery/M_alumni_berprestasi.php');
        // If we are not using namespaces, we need to make sure that we are starting with "\" as
        // this way we can guarantee that it will not fail in case we will use namespaces at the future
        $model = new \M_alumni_berprestasi();
        $crud->setModel($model);

        $crud->setSubject('Alumni Berprestasi');

        $column = ['id','nama_prestasi','kesan'];
        $fieldsDisplay = [
            'id' => 'NIK Alumni',
            'nama_prestasi' => 'Nama Prestasi',
            'kesan' => 'Kesan',
        ];

        $crud->columns($column);
        $crud->displayAs($fieldsDisplay);

        $output = $crud->render();
        $this->_setOutput('alumni_berprestasi/index', $output);
    }
}
