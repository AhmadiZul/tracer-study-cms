<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Index extends BaseController
{

    public $loginBehavior = true;
    protected $template = "app_back";
    protected $module = "kuesioner_survey";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_survey', 'survey');
        $this->load->model('M_module', 'modul');
        $this->load->model('M_sistem', 'sistem');
    }

    public function index()
    {
        $this->data['icon'] = 'fas fa-scroll';
        $this->data['title'] = 'Kuesioner Survey';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $crud = new Grid();
        $crud->setSkin('bootstrap-v4');
        $crud->unsetJquery();
        $crud->setSubject('Kuesioner Survey');
        $crud->setTable('kuesioner_alumni_question');



        $crud->where([
            'kuesioner_alumni_question.is_active' => '1'
        ]);

        $column = ['question', 'urutan', 'question_type', 'question_code'];
        $fields = ['question', 'urutan', 'question_type'];
        $requireds = ['question', 'urutan', 'question_type', 'answer_rules', 'others'];
        $fieldsDisplay = [
            'question' => 'Question',
            'urutan' => 'Urutan',
            'question_code' => 'Question Code',
            'question_type' => 'Question Type',
        ];

        $crud->columns($column);
        $crud->fields($fields);
        $crud->requiredFields($requireds);
        $crud->displayAs($fieldsDisplay);
        $crud->unsetAdd();
        $crud->unsetEdit();
        $crud->unsetDeleteMultiple();

        $crud->callbackColumn('urutan', function ($value, $row) {
            return '<button type="button" onclick="urutanModal(' . "'" . $row->id_kuesioner . "'" .  ',' . "'" . $value . "'" . ')" class="btn bg-violet text-white">' . $value . '</button>';
        });

        $crud->callbackColumn('question_code', function ($value, $row) {
            return '<span class="badge badge-square bg-success text-white">' . $row->question_code . '</span>';
        });
        $crud->callbackColumn('question_type', function ($value, $row) {
            if ($value == "1") {
                return '<span class="badge badge-square bg-green-taffeta text-white">Jawaban Singkat</span>';
            } elseif ($value == "2") {
                return '<span class="badge badge-square bg-secondary text-white">Paragraf</span>';
            } elseif ($value == "3") {
                return '<span class="badge badge-square bg-red-taffeta text-white">Pilihan Ganda</span>';
            } elseif ($value == "4") {
                return '<span class="badge badge-square bg-violet-taffeta text-white">Kotak Centang</span>';
            } elseif ($value == "5") {
                return '<span class="badge badge-square bg-yellow-taffeta text-white">Skala Likert</span>';
            } elseif ($value == "7") {
                return '<span class="badge badge-square bg-blue-taffeta text-white">Lokasi</span>';
            } elseif ($value == '8') {
                return '<span class="badge badge-square bg-warning text-white">Tanpa Jawaban</span>';
            } elseif ($value == '9') {
                return '<span class="badge badge-square bg-success text-white">Dropdown</span>';
            }
        });
        $crud->callbackColumn('status', function ($value, $row) {
            if ($value == "1") {
                return '<button type="button" onclick="noAktif(' . "'" . $row->id_kuesioner . "'" . ')" class="btn bg-green-taffeta text-white"><i class="fas fa-unlock"></i> Aktif</button>';
            }
            return '<button type="button" onclick="aktif(' . "'" . $row->id_kuesioner . "'" . ')" class="btn bg-yellow-taffeta text-white"><i class="fas fa-lock"></i> Tidak Aktif</button>';
        });

        $crud->callbackDelete(array($this, 'deleteKuesioner'));
        // $crud->setActionButton('Ubah', 'fa fa-pencil', function ($row) {
        //     return site_url('kuesioner_survey/index/ubahKuesioner?key='. $row->id_kuesioner);
        // }, false);

        $crud->setActionButton('Setting Status', 'fas fa-users-cog gc-button-icon', function ($row) {
            if ($row->question_code == '<span class="badge badge-square bg-success text-white">f8</span>') {
                return "javascript:pivotAlert()";
            } else {
                return site_url('kuesioner_survey/index/settingStatus?key=' . $row->id_kuesioner);
            }
        }, false);

        $output = $crud->render();
        $this->_setOutput('survey/kuesioner', $output);
    }
    
    /**
     * deleteKuesioner
     * menghapus pertanyaan kuesioner secara permanen
     * @param  mixed $id
     * @return void
     */
    public function deleteKuesioner($id)
    {
        $kuesioner = $this->survey->getKuesioner($id->primaryKeyValue);
        $delete_offered_answer = $this->survey->getOfferedAnswerByIdKuesioner($kuesioner['id_kuesioner']);
        $kuesionerWithStatus = $this->survey->getKuesionerStatus($id->primaryKeyValue);
        if ($kuesionerWithStatus) {
            $this->survey->deleteKuesionerWithStatus($id->primaryKeyValue);
        }
        switch ($kuesioner['question_type']) {
            case '1':
                $this->survey->hapusSingkat($kuesioner['id_kuesioner']);
                break;
            case '2':
                $this->survey->hapusSingkat($kuesioner['id_kuesioner']);
                break;
            case '3':
                $this->survey->hapusPilgan($kuesioner['id_kuesioner']);
                $this->survey->hapusSingkat($kuesioner['id_kuesioner']);
                break;
            case '4':
                $this->survey->hapusPilgan($kuesioner['id_kuesioner']);
                $this->survey->hapusSingkat($kuesioner['id_kuesioner']);
                break;
            case '5':
                $this->survey->hapusLikert($kuesioner['id_kuesioner']);
                $this->survey->hapusSingkat($kuesioner['id_kuesioner']);
                break;
            case '7':
                $this->survey->hapusSingkat($kuesioner['id_kuesioner']);
                break;
            case '8':
                $this->survey->hapusSingkat($kuesioner['id_kuesioner']);
                break;
            case '9':
                $this->survey->hapusPilgan($kuesioner['id_kuesioner']);
                $this->survey->hapusSingkat($kuesioner['id_kuesioner']);
                break;
        }
    }
    
    /**
     * addKuesioner
     * menampilkan form tambah kuesioner
     * @return void
     */
    public function addKuesioner()
    {
        $this->data['title'] = 'Tambah Kuesioner';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $this->data['parent'] = $this->survey->getParentKuesioner();
        $this->render('add_kuesioner');
    }

    public function ubahKuesioner()
    {
        $this->data['title'] = 'Edit Kuesioner';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $this->data['id_kuesioner'] = $this->input->get("key");
        if ($this->data['id_kuesioner'] == "") {
            redirect(base_url('kuesioner_survey/index'));
        }
        $this->data['data'] = $this->getKuesionerById($this->data['id_kuesioner']);
        // $this->data['kategori'] = $this->survey->getKategoriQuestion();
        $this->render('edit_kuesioner');
    }
    
    /**
     * settingStatus
     * menampilkan halaman setting status alumni dengan kuesioner
     * @return void
     */
    public function settingStatus()
    {
        $this->data['title'] = 'Setting Status Alumni';
        $this->data['is_home'] = false;
        $this->data['warna_tema'] = $this->sistem->warna_tema();
        $this->data['logo_utama'] = $logo_utama = $this->sistem->logo_utama();
        $this->data['logo_title'] = $logo_title = $this->sistem->logo_title();
        $this->data['id'] = $this->input->get('key');
        $this->data['scripts'] = ['kuesioner_survey/js/setting_status.js'];
        $this->render('setting_status');
    }
    
    /**
     * loadStatus
     * menampilkan status dari tiap kuesioner
     * @return void
     */
    public function loadStatus()
    {
        $response['status'] = 500;
        $response['data'] = [];

        $data = [];

        $modul = $this->survey->getStatus();

        $modul_group = $this->survey->getStatusQuestion($this->input->post('id_kuesioner'));

        $data['modul_group'] = $modul_group['data'];
        if ($modul['status'] == 201) {
            $data['module'] = $modul['data'];
            $response['status'] = 201;
        } else {
            $data['module'] = [];
            $response['status'] = 500;
        }
        $response['data'] = $data;
        $this->responseJson($response);
    }
    
    /**
     * updateStatus
     * mengupdate status dari tiap kuesioner
     * @return void
     */
    public function updateStatus()
    {
        $response['status'] = 500;
        $response['message'] = '';

        $params = $this->input->post(null, true);

        if (isset($params['kuesioner'])) {
            $result = $this->survey->updateStatus($params);

            if ($result['status'] == 201) {
                $response['status'] = 201;
                $response['message'] = $result['message'];
            } else {
                $response['status'] = 500;
                $response['message'] = $result['message'];
            }
        } else {
            $result = $this->survey->deleteStatus($params);
            $response = $result;
        }

        $this->responseJson($response);
    }
    
    /**
     * getKuesionerById
     * memperoleh kuesioner berdasarkan id
     * @param  mixed $id
     * @return void
     */
    public function getKuesionerById($id)
    {
        $kuesioner = $this->survey->getKuesioner($id);
        $kuesioner['answer'] = $this->getAnswerByKuesioner($id, $kuesioner['question_type']);

        return $kuesioner;
    }
    
    /**
     * getAnswerByKuesioner
     * mendapatkan jawaban berdasarkan kuesioner
     * @param  mixed $id
     * @param  mixed $type
     * @return void
     */
    public function getAnswerByKuesioner($id, $type)
    {
        $answer = array();

        switch ($type) {
            case '3':
                $answer = $this->survey->getAnswerOptionByKuesioner($id, array('is_active' => '1'));
                break;
            case '4':
                $answer = $this->survey->getAnswerOptionByKuesioner($id, array('is_active' => '1'));
                break;
            case '5':
                $answer = $this->survey->getAnswerLikertByKuesioner($id, array('is_active' => '1'));
                break;
            case '6':
                $answer = $this->survey->getAnswerSubByKuesioner($id, array('is_active' => '1'));
                break;

            default:
                $answer = null;
                break;
        }

        return $answer;
    }

    public function updateKuesioner()
    {
        $params = $this->input->post();

        $update = $this->survey->updateKuesioner($params);

        echo json_encode($update);
    }
    
    /**
     * createKuesioner
     * menambahkan data kuesioner
     * @return void
     */
    public function createKuesioner()
    {
        $params = $this->input->post();

        $this->_runValidation();

        $htmlCodeNumber = 201;
        $response = '';

        $errors = $this->_runValidation();

        if ($errors) {
            $htmlCodeNumber = 400;
            $response = [
                'code' => $htmlCodeNumber,
                'message' => [
                    'title' => 'ERROR',
                    'body' => 'Validasi Gagal',
                ],
                'data' => $errors,
            ];
            return $this->responseJSON($response, $htmlCodeNumber);
        };

        $insert = $this->survey->createKuesioner($params);

        echo json_encode($insert);
    }
    
    /**
     * _callbackBeforeDelete
     * menghapus kuesioner secara permanen
     * @param  mixed $params
     * @return void
     */
    public function _callbackBeforeDelete($params)
    {
        $errorMessage = new \GroceryCrud\Core\Error\ErrorMessage();

        $cek = $this->survey->hapusKue($params->primaryKeyValue);

        if ($cek['success'] == false) {
            return $errorMessage->setMessage($cek['message']);
        } else {
            $params->primaryKeyValue = '1';
            return $params;
        }
    }

    public function aktif()
    {
        $id_kuesioner = $this->input->post('id_kuesioner');
        $data = [
            'status' => $this->input->post('status')
        ];

        $response = $this->survey->aktif($id_kuesioner, $data);

        echo json_encode($response);
    }
    
    /**
     * simpanUrutan
     * mengedit urutan kuesioner
     * @return void
     */
    public function simpanUrutan()
    {
        $id_kuesioner = $this->input->post('id_kuesioner');
        $urutan = $this->input->post('urutan');
        $cekDigit = $this->checkDigitUrutan($urutan);

        $data = [
            'urutan' => $urutan
        ];

        if ($cekDigit['success']) {
            $response = $this->survey->simpanUrutan($id_kuesioner,  $data);
        } else {
            $response = $cekDigit;
        }

        echo json_encode($response);
    }
    
    /**
     * checkDigitUrutan
     * validasi digit urutan kuesioner
     * @param  mixed $urutan
     * @return void
     */
    public function checkDigitUrutan($urutan)
    {
        if (strlen($urutan) >= 10) {
            $return['success'] = false;
            $return['message'] = 'Urutan tidak boleh lebih dari 10 digit';
        } else {
            $return['success'] = true;
        }

        return $return;
    }

    
    /**
     * _runValidation
     * validasi untuk menambahkan kuesioner
     * @return void
     */
    private function _runValidation()
    {
        $errors = FALSE;

        $this->form_validation->set_rules('input_kode', 'Kode Pertanyaan', 'trim|required|min_length[4]|max_length[10]');
        $this->form_validation->set_rules('urutan', 'Urutan', 'trim|required');
        $this->form_validation->set_rules('input_pertanyaan', 'Pertanyaan', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('input_jenis', 'Jenis Pertanyaan', 'trim|required');

        // validasi is parent question
        if(isset($_POST['add_parent'])){
            $this->form_validation->set_rules("isian_parent", 'Parent question', 'trim|required');
        }
        // validasi per jenis pertanyaan
        $input_jenis = $_POST['input_jenis'];
        switch ($input_jenis) {
            case '3':
                for ($i=0; $i < (int)$_POST['total']; $i++) { 
                    $this->form_validation->set_rules("input_pilgan$i", 'Input pilihan', 'trim|required');
                    $this->form_validation->set_rules("inputKodePilgan$i", 'Input kode', 'trim|required');
                }
                break;
            case '4':
                for ($i=0; $i < (int)$_POST['totalU']; $i++) { 
                    $this->form_validation->set_rules("input_kokcen$i", 'Pilihan jawaban', 'trim|required');
                    $this->form_validation->set_rules("inputKodeKokcen$i", 'Input kode', 'trim|required');
                }
                break;
            case '5':
                for ($i=0; $i < (int)$_POST['totalL']; $i++) { 
                    $this->form_validation->set_rules("input_linier$i", 'Pilihan jawaban', 'trim|required');
                    $this->form_validation->set_rules("inputKodeLinier$i", 'Input kode', 'trim|required');
                }
                break;
                case '7':
                    $this->form_validation->set_rules('lokasi', 'Lokasi', 'trim|required');
                    break;
                case '9':
                    for ($i=0; $i < (int)$_POST['totalD']; $i++) { 
                        $this->form_validation->set_rules("input_dropdown$i", 'Pilihan jawaban', 'trim|required');
                        $this->form_validation->set_rules("inputKodeDropdown$i", 'Kode jawaban', 'trim|required');
                    }
                    break;
                default:
                # code...
                break;
        }

        $this->form_validation->set_message('required', '{field} wajib diisi');
        $this->form_validation->set_message('is_unique', '{field} sudah terdaftar');
        $this->form_validation->set_message('matches', '{field} tidak sesuai');
        $this->form_validation->set_message('validUrl', '{field} tidak sesuai');
        $this->form_validation->set_message('valid_email', '{field} tidak sesuai');
        $this->form_validation->set_message('checkPT', '{field} sudah terdaftar');
        $this->form_validation->set_message('checkUser', '{field} sudah terdaftar');
        $this->form_validation->set_message('max_length', '{field} tidak boleh lebih dari {param} karakter');
        $this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter');

        if ($this->form_validation->run() == FALSE) {
            foreach ($this->input->post() as $field => $value) {
                if (form_error($field)) {
                    $errors[] = [
                        'field'   => $field,
                        'message' => trim(form_error($field, ' ', ' ')),
                    ];
                };
            };
        };

        return $errors;
    }
}
