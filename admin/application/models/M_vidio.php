<?php

class M_vidio extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getVidio($id_vidio = null)
    {
        $this->db->select('a.*');
        $this->db->from('vidio_landing a');

        if ($id_vidio) {
            $this->db->where('id_vidio', $id_vidio);
            return $this->db->get()->row_array();
        }

        return $this->db->get()->result_array();
    }

    public function publishVidio($id_vidio, $data)
    {
        $this->db->where('id_vidio', $id_vidio);
        $response = $this->db->update('vidio_landing', $data);

        if($response) {
            $return['success'] = true;
            $return['message'] = 'Berhasil Dipublish';
        }
        else {
            $return['success'] = false;
            $return['message'] = 'Tidak Berhasil Dipublish';
        }
        return $return;
    }
}