<?php

class M_footer extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function add_footer($data)
    {
        $this->db->insert('footer', $data);
        return $this->db->affected_rows();
    }

    public function getFooter($where)
    {
        $this->db->from('footer');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function editFooter($data, $id_footer)
    {
        $this->db->set($data);
        $this->db->where('id_footer', $id_footer);
        $this->db->update('footer');
        return $this->db->affected_rows();
    }
}