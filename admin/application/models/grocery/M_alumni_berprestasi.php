<?php

use GroceryCrud\Core\Model;
use GroceryCrud\Core\Model\ModelFieldType;

class M_alumni_berprestasi extends Model {

    protected $ci;
    protected $db;

    function __construct() {
        include(APPPATH . 'config/database.php');
        $dbConfig = [
            'adapter' => [
                'driver'   => 'Pdo_Mysql',
                'host'     => $db['default']['hostname'],
                'database' => $db['default']['database'],
                'username' => $db['default']['username'],
                'password' => $db['default']['password'],
                'charset'  => 'utf8'
            ]
        ];
        $this->setDatabaseConnection($dbConfig);

        $this->ci = & get_instance();
        $this->db = $this->ci->db;
    }

    public function getFieldTypes($tableName)
    {
        $fieldTypes = parent::getFieldTypes($tableName);

        $nik = new ModelFieldType();
        $nik->dataType = 'varchar';

        $fieldTypes['nik'] = $nik;

        return $fieldTypes;
    }

    protected function _getQueryModelObject($withLimit = true) {
        $order_by = $this->orderBy;
        $sorting = $this->sorting;

        // All the custom stuff here
        $this->db->select('alumni_berprestasi.id_berprestasi, alumni.nik as nama, alumni_berprestasi.nama_prestasi, alumni_berprestasi.kesan', false);
        $this->db->join('alumni','alumni_berprestasi.id_alumni=alumni.id_alumni','left');

        if ($order_by !== null) {
            $this->db->order_by($order_by. " " . $sorting);
        }

        if (!empty($this->_filters)) {
            foreach ($this->_filters as $filter_name => $filter_value) {
                if ($filter_name === 'nama_prestasi') {
                    if (is_array($filter_value)) {
                        foreach ($filter_value as $value) {
                            $this->db->like($filter_name, $value);    
                        }
                    } else {
                        $this->db->like($filter_name, $filter_value);
                    }                    
                }
            }
        }

        if (!empty($this->_filters_or)) {
            foreach ($this->_filters_or as $filter_name => $filter_value) {
                $this->db->or_like($filter_name, $filter_value);
            }
        }

        if ($withLimit) {
            $this->db->limit($this->limit, ($this->limit * ($this->page - 1)));
        }

        return $this->db->get($this->tableName);
    }

    public function getList() {
        return $this->_getQueryModelObject()->result_array();
    }

    public function getTotalItems()
    {
        if (!empty($this->_filters)) {
            return $this->_getQueryModelObject(false)->num_rows();
        }

        // If we don't have any filtering it is faster to have the default total items
        // In case this is more complicated you can add your own code here
        return parent::getTotalItems();
    }
}