<?php


use GroceryCrud\Core\Model;
use GroceryCrud\Core\Model\ModelFieldType;

class M_subcategories extends Model
{

    protected $ci;
    protected $db;
    public $_enableCountRelationFilterOnInit = true;

    function __construct($databaseConfig)
    {
        $this->setDatabaseConnection($databaseConfig);

        $this->ci = &get_instance();
        $this->db = $this->ci->db;
    }

    protected function _getQueryModelObject()
    {
        $order_by = $this->orderBy;
        $sorting  = $this->sorting;

        $this->db->select('
                x1.id,
                x1.question_category AS question_sub_category,
                x2.question_category AS category,
                x1.is_deleted,
                x1.is_active,
                COUNT(DISTINCT x3.id) AS question_count
		');
        $this->db->join('question_categories x2', 'x1.parent_id = x2.id', 'LEFT');
        $this->db->join('questions x3', 'x1.id = x3.question_category_id AND x3.question_enabled = "true"', 'LEFT');
        $this->db->where('x1.parent_id <> ', 0);
        $this->db->group_by(range(1, 4));

        if ($order_by !== null) {
            $this->db->order_by("x1.question_category " . $sorting);
        }

        if (!empty($this->_filters)) {

        }

        if (!empty($this->_filters_or)) {
            foreach ($this->_filters_or as $filter_name => $filter_value) {
                $this->db->or_like($filter_name, $filter_value);
            }
        }

        $this->db->limit($this->limit, ($this->limit * ($this->page - 1)));
        return $this->db->get($this->tableName . ' x1');
    }

    public function getFieldTypes($tableName)
    {
        $fieldTypes = parent::getFieldTypes($tableName);

        $varchar           = new ModelFieldType();
        $varchar->dataType = 'varchar';

        $fieldTypes['category']              = $varchar;
        $fieldTypes['question_count']        = $varchar;
        $fieldTypes['question_sub_category'] = $varchar;

        return $fieldTypes;
    }

    public function getList()
    {
        return $this->_getQueryModelObject()->result_array();
    }

    public function getTotalItems()
    {
        return $this->_getQueryModelObject()->num_rows();
    }

}