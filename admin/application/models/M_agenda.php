<?php

class M_agenda extends CI_Model
{

    private $table = 'agenda';

    function __construct()
    {
        parent::__construct();
    }


    public function add_agenda($data)
    {
        $this->db->insert('agenda', $data);
        return $this->db->affected_rows();
    }

    public function getAgenda($where)
    {
        $this->db->from('agenda');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    /* public function editAgenda($data, $id)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('agenda');
        return $this->db->affected_rows();
    } */

    public function editAgenda($data,$where)
    {
        $this->db->update('agenda',$data,$where);
        return $this->db->affected_rows();
    }
}