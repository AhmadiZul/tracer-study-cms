<?php

class M_lowongan_kerja extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getLowongan($id_lowongan)
    {
        $this->db->where('id_lowongan_pekerjaan',$id_lowongan);

        return $this->db->get('lowongan_pekerjaan')->row();
    }

    public function getMitra($id_instansi)
    {
        $this->db->where('id_instansi',$id_instansi);
        return $this->db->get('mitra')->row();
    }

    public function simpan_lowongan($data)
    {
        $this->db->insert('lowongan_pekerjaan', $data);
        return $this->db->affected_rows();
    }

    public function ubah_lowongan($data, $id)
    {
        $this->db->update('lowongan_pekerjaan', $data, ['id_lowongan_pekerjaan' => $id]);
        return $this->db->affected_rows();
    }

}
