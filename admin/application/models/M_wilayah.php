<?php

class M_wilayah extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->helper('function_helper');
  }

  function getSelectKabupaten($kode_provinsi = null){
    $data = $this->getKotaByProv2($kode_provinsi);
    $html = '<option value="">Pilih Kabupaten</option>';
    if ($data != null) {
      foreach ($data as $rows) {
        $html .= '<option value="'.$rows['kode_kab_kota'].'">'.ucwords(strtolower($rows['nama_kab_kota'])).'</option>';
      }
    }
    $return = $html;
    return $return;
  }

  function getSelectKecamatan($kode_kabupaten = null){
    $data = $this->getKecamatanByKota2($kode_kabupaten);
    $html = '<option value="">Pilih Kecamatan</option>';
    if ($data != null) {
      foreach ($data as $rows) {
        $html .= '<option value="'.$rows['kode_kecamatan'].'">'.ucwords(strtolower($rows['nama_kecamatan'])).'</option>';
      }
    }
    $return = $html;
    return $return;
  }

  function getKotaByProv2($id)
  {

    $get = $this->db->query("SELECT * FROM ref_kab_kota where kode_provinsi=? order by nama_kab_kota ", array($id));

    if ($get->num_rows() != 0) {
      return $get->result_array();
    } else {
      return null;
    }
  }

  function getKecamatanByKota2($id)
  {

    $get = $this->db->query("SELECT * FROM ref_kecamatan where kode_kab_kota=? order by nama_kecamatan", array($id));

    if ($get->num_rows() != 0) {
      return $get->result_array();
    } else {
      return null;
    }
  }

  function getProvinsi()
  {

    $prov = $this->db->query("SELECT * FROM ref_provinsi order by nama_provinsi asc");

    if ($prov->num_rows() != 0) {
      return $prov->result_array();
    } else {
      return null;
    }
  }

  public function getListProvinsi()
  {
    $this->db->order_by('nama_propinsi', 'ASC');

    $results = $this->db->get('ref_provinsi')->result_array();
    $return = array();

    foreach ($results as $key => $value) {
      $return[$value['kode_propinsi']] = $value["nama_propinsi"];
    }

    return $return;
  }

  public function getListKabupaten()
  {
    $this->db->order_by('nama_kab_kota', 'ASC');
    $results = $this->db->get('ref_kab_kota')->result_array();
    $return = array();

    foreach ($results as $key => $value) {
      $return[$value['kode_kab_kota']] = $value["nama_kab_kota"];
    }

    return $return;
  }

  public function getListKecamatan()
  {
    $results = $this->db->query('SELECT kode_kecamatan, nama_kecamatan FROM ref_kab_kota join ref_kecamatan on ref_kab_kota.kode_kab_kota = ref_kecamatan.kode_kab_kota ORDER BY nama_kecamatan ASC')->result_array();

    $return = array();

    foreach ($results as $key => $value) {
      $return[$value['kode_kecamatan']] = $value["nama_kecamatan"];
    }

    return $return;
  }

}
