<?php

class M_module extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getModule()
    {
        $return['status'] = 500;
        $return['data'] = [];

        $get = $this->db->query("SELECT id_modul, nama_modul FROM modul_sistem where is_active='1'");
        if ($get->num_rows()!=0) {
            $return['status'] = 201;
            $return['data'] = $get->result_array();
        }
        return $return;
    }

    public function getModuleGroup($group)
    {
        $return['status'] = 500;
        $return['data'] = [];

        $get = $this->db->query("SELECT nama_modul FROM akses_group_modul where id_group=?",[$group]);
        if ($get->num_rows() != 0) {
            $data = [];
            foreach ($get->result_array() as $key => $value) {
                $data[] = $value['nama_modul'];
            }
            $return['status'] = 201;
            $return['data'] = $data;
        }
        return $return;
    }

    public function updateModule($params)
    {
        $return['status'] = 500;
        $return['message'] = '';

        $this->db->trans_begin();
        $this->db->where("id_group", $params['id_group']);
        $this->db->delete("akses_group_modul");

        $data = [];

        for ($i=0; $i < count($params['module']); $i++) {
            $data[$i]['id_group'] = $params['id_group'];
            $data[$i]['nama_modul'] = $params['module'][$i];
            $data[$i]['hak_akses'] = 'access';
            $this->db->insert('akses_group_modul', $data[$i]);
        }

        if ($this->db->trans_status()===false) {
            $this->db->trans_rollback();
            $return['status'] = 500;
            $return['message'] = 'Perubahan akses modul sistem gagal';
        }else{
            $this->db->trans_commit();
            $return['status'] = 201;
            $return['message'] = 'Perubahan akses modul sistem berhasil';
        }
        $this->db->trans_complete();
        return $return;
    }
}
