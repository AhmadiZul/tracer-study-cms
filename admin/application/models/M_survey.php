<?php

class M_survey extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function createJadwalSurvey($data)
    {
        return $this->db->insert('ref_jadwal_survey', $data);
    }

    public function updateJadwalSurvey($id_jadwal, $data)
    {
        $this->db->where('id_survey', $id_jadwal);
        return $this->db->update('ref_jadwal_survey', $data);
    }

    public function getJadwalSurvey($id_survey)
    {
        $this->db->where('id_survey', $id_survey);
        return $this->db->get('ref_jadwal_survey')->row();
    }
    public function aktifJenis($id_jenis, $data)
    {
        $this->db->where('id', $id_jenis);
        $response = $this->db->update('ref_jenis_survey', $data);

        if ($response) {
            $return['success'] = true;
            $return['message'] = 'Data berhasil diubah';
        } else {
            $return['success'] = false;
            $return['message'] = 'Data Tidak berhasil diubah';
        }
        return $return;
    }

    public function cekData($data, $onUpdate, $id = null)
    {
        $isCheck = $this->checkJadwal(array('untuk_id_prodi' => $data['untuk_id_prodi'], 'untuk_tahun_lulus' => $data['untuk_tahun_lulus'], 'id_ref_jenis_survey' => $data['id_ref_jenis_survey']));

        if ($onUpdate) {
            $isCheck = $this->checkJadwal(array('untuk_id_prodi' => $data['untuk_id_prodi'], 'untuk_tahun_lulus' => $data['untuk_tahun_lulus'], 'id_ref_jenis_survey' => $data['id_ref_jenis_survey'], 'id_survey !=' => $id));
        }

        if ($isCheck) {
            return [
                "status"  => 201,
                "message" => "Berhasil Ditambahkan"
            ];
        }

        return [
            "status"  => 500,
            "message" => "Prodi dan Angkatan sudah digunakan pada Jenis survey ini."
        ];
    }

    public function checkJenis($data)
    {
        $jenis = $data["jenis_survey"];

        $this->db->where('jenis_survey', $jenis);

        $get = $this->db->get("ref_jenis_survey");

        if ($get->num_rows() > 0) {
            return [
                "status"  => 500,
                "message" => "Jenis Survey sudah Ada."
            ];
        } else {
            return [
                "status"  => 201,
                "message" => "Berhasil Ditambahkan"
            ];
        }
    }

    public function checkJadwal($where)
    {
        $this->db->where($where);

        $get = $this->db->get('ref_jadwal_survey');

        if ($get->num_rows() > 0) {
            return false;
        }

        return true;
    }

    public function getKategoriQuestion()
    {
        $kategori = $this->db->query("SELECT * FROM kuesioner_alumni_question_category order by id_category asc");

        if ($kategori->num_rows() != 0) {
            return $kategori->result_array();
        } else {
            return null;
        }
    }
    
    /**
     * createKuesioner
     * menambahkan kuesioner ke database
     * @param  mixed $data
     * @return void
     */
    public function createKuesioner($data)
    {
        $this->db->trans_begin();

        $checkUrutan =  $this->checkKuesioner(array('urutan' => $data['urutan']));
        $checkKode =  $this->checkKuesioner(array('question_code' => $data['input_kode']));

        if ($checkUrutan > 0) {
            $return['success'] = false;
            $return['message'] = 'Urutan tidak boleh sama';

            goto End;
        }

        if ($checkKode > 0) {
            $return['success'] = false;
            $return['message'] = 'Kode pertanyaan tidak boleh sama';

            goto End;
        }

        // cek kode untuk option kuesioner
        $input = [];
        switch ($data['input_jenis']) {
            case '3':
                if ($data['total'] != '0') {
                    for ($i = 0; $i < $data['total']; $i++) {
                        array_push($input, $data['inputKodePilgan' . (string)$i]);
                    }
                    if (count(array_unique($input)) != $data['total']) {
                        $return['success'] = false;
                        $return['message'] = 'Kode pilihan jawaban tidak boleh sama';
                        goto End;
                    }
                }
                break;
            case '4':
                if ($data['totalU'] != '0') {
                    for ($i = 0; $i < $data['totalU']; $i++) {
                        array_push($input, $data['inputKodeKokcen' . (string)$i]);
                    }
                    if (count(array_unique($input)) != $data['totalU']) {
                        $return['success'] = false;
                        $return['message'] = 'Kode pilihan jawaban tidak boleh sama';
                        goto End;
                    }
                }
                break;
            case '5':
                if (!empty($data['totalL']) != '0') {
                    for ($i = 0; $i < $data['totalL']; $i++) {
                        array_push($input, $data['inputKodeLinier' . (string)$i]);
                    }
                    if (count(array_unique($input)) != $data['totalL']) {
                        $return['success'] = false;
                        $return['message'] = 'Kode pilihan jawaban tidak boleh sama';
                        goto End;
                    }
                }
                break;
            case '9':
                if (!empty($data['totalD']) != '0') {
                    for ($i = 0; $i < $data['totalD']; $i++) {
                        array_push($input, $data['inputKodeDropdown' . (string)$i]);
                    }
                    if (count(array_unique($input)) != $data['totalD']) {
                        $return['success'] = false;
                        $return['message'] = 'Kode pilihan jawaban tidak boleh sama';
                        goto End;
                    }
                }
                break;
        }

        $pertanyaan = [
            'id_kuesioner' => getUUID(),
            'question_code' => $data['input_kode'],
            'question' => $data['input_pertanyaan'],
            'question_type' => $data['input_jenis'],
            'urutan' => $data['urutan']
        ];

        if (!empty($data['add_parent'])) {
            $pertanyaan['parent_question_code'] = $data['isian_parent'];
        }

        if (!empty($data['lainnya'])) {
            $pertanyaan['others'] = $data['lainnya'];
        }

        switch ($data['input_jenis']) {
            case "1":
                $pertanyaan['answer_rules'] = $data['input_singkat'];
                $this->db->insert("kuesioner_alumni_question", $pertanyaan);
                break;
            case "2":
                $this->db->insert("kuesioner_alumni_question", $pertanyaan);
                break;
            case "3":
                $this->db->insert("kuesioner_alumni_question", $pertanyaan);
                if (empty($data['input_pilgan0'])) {
                    break;
                }
                for ($x = 0; $x < (int)$data['total']; $x++) {
                    $option = [
                        'id_opsi' => getUUID(),
                        'id_kuesioner' => $pertanyaan['id_kuesioner'],
                        'answer_code' => $data['inputKodePilgan' . (string)$x],
                        'answer_rules' => $data['isian_pilgan' . (string)$x],
                        'opsi' => $data['input_pilgan' . (string)$x],
                        'urutan' =>  $x + 1
                    ];
                    if (!empty($data['add_pilgan' . (string)$x])) {
                        $option['isian'] = $data['add_pilgan' . (string)$x];
                    }
                    $this->db->insert("kuesioner_alumni_question_option", $option);
                }
                break;
            case "4":
                $this->db->insert("kuesioner_alumni_question", $pertanyaan);
                if (empty($data['input_kokcen0'])) {
                    break;
                }
                for ($x = 0; $x < (int)$data['totalU']; $x++) {
                    $option = [
                        'id_opsi' => getUUID(),
                        'id_kuesioner' => $pertanyaan['id_kuesioner'],
                        'answer_code' => $data['inputKodeKokcen' . (string)$x],
                        'opsi' => $data['input_kokcen' . (string)$x],
                        'urutan' =>  $x + 1
                    ];
                    if (!empty($data['add_kokcen' . (string)$x])) {
                        $option['isian'] = $data['add_kokcen' . (string)$x];
                        $option['answer_rules'] = $data['isian_kokcen' . (string)$x];
                    }
                    $this->db->insert("kuesioner_alumni_question_option", $option);
                }
                break;
            case "5":
                $this->db->insert("kuesioner_alumni_question", $pertanyaan);
                if (empty($data['input_linier0'])) {
                    break;
                }
                for ($x = 0; $x < (int)$data['totalL']; $x++) {
                    $option = [
                        'id_likert' => getUUID(),
                        'id_kuesioner' => $pertanyaan['id_kuesioner'],
                        'likert_code' => $data['inputKodeLinier' . (string)$x],
                        'pertanyaan' => $data['input_linier' . (string)$x],
                        'urutan' =>  $x + 1
                    ];
                    if (!empty($data['is_horizontal'])) {
                        $option['is_horizontal'] = '1';
                    }
                    $this->db->insert("kuesioner_alumni_question_likert", $option);
                }
                break;
            case "7":
                $pertanyaan['others'] = $data['lokasi'];
                $this->db->insert("kuesioner_alumni_question", $pertanyaan);
                break;
            case "8":
                $this->db->insert("kuesioner_alumni_question", $pertanyaan);
                break;
            case "9":
                $this->db->insert("kuesioner_alumni_question", $pertanyaan);
                if (empty($data['input_dropdown0'])) {
                    break;
                }
                for ($x = 0; $x < (int)$data['totalD']; $x++) {
                    $option = [
                        'id_opsi' => getUUID(),
                        'id_kuesioner' => $pertanyaan['id_kuesioner'],
                        'answer_code' => $data['inputKodeDropdown' . (string)$x],
                        'opsi' => $data['input_dropdown' . (string)$x],
                        'urutan' =>  $x + 1
                    ];
                    $this->db->insert("kuesioner_alumni_question_option", $option);
                }
                break;
            default:
                $return['success'] = false;
                $return['message'] = 'Jenis pertanyaan tidak ada';

                goto End;
        }


        if ($this->db->trans_status() === FALSE) {
            $return['success'] = false;
            $return['message'] = 'Data gagal ditambahkan';
            $this->db->trans_rollback();

            goto End;
        } else {
            $return['success'] = true;
            $return['message'] = 'Data berhasil ditambahkan';

            $this->db->trans_commit();

            goto End;
        }

        End:
        $this->db->trans_complete();
        return $return;
    }

    public function updateKuesioner($data)
    {
        $this->db->trans_begin();

        $checkUrutan =  $this->checkKuesioner(array('urutan' => $data['urutan']), $data['id_kuesioner']);
        $checkKode =  $this->checkKuesioner(array('question_code' => $data['input_kode']), $data['id_kuesioner']);

        if ($checkUrutan > 0) {
            $return['success'] = false;
            $return['message'] = 'Urutan tidak boleh sama';

            goto End;
        }

        if ($checkKode > 0) {
            $return['success'] = false;
            $return['message'] = 'Kode pertanyaan tidak boleh sama';

            goto End;
        }

        $pertanyaan = [
            'question_code' => $data['input_kode'],
            'question' => $data['input_pertanyaan'],
            'question_type' => $data['input_jenis']
        ];

        if (!empty($data['lainnya'])) {
            $pertanyaan['others'] = $data['lainnya'];
        }

        switch ($data['input_jenis']) {
            case "1":
                $pertanyaan['answer_rules'] = $data['input_singkat'];
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_alumni_question', $pertanyaan);
                break;
            case "2":
                $pertanyaan['others'] = $data['input_paragraf'];
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_alumni_question', $pertanyaan);
                break;
            case "3":
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_alumni_question', $pertanyaan);

                //update semua option non active
                $this->updateAnswerOption(array('id_kuesioner' => $data['id_kuesioner']), array('is_active' => '0'));


                //simpan update option
                for ($x = 0; $x < count($data['id_pilgan']); $x++) {
                    if (!empty($data['id_pilgan'][$x])) {
                        if (empty($data['input_pilgan'][$x])) {
                            continue;
                        }

                        //check kode
                        $checkKode =  $this->checkOption(array('answer_code' => $data['inputKodePilgan'][$x], 'id_kuesioner' => $data['id_kuesioner']), $data['id_pilgan'][$x]);

                        if ($checkKode > 0) {
                            $return['success'] = false;
                            $return['message'] = 'Kode ' . $data['inputKodePilgan'][$x] . ' sudah ada';

                            goto End;
                        }

                        //update option
                        $option = [
                            'id_kuesioner' => $data['id_kuesioner'],
                            'answer_code' => $data['inputKodePilgan'][$x],
                            'answer_rules' => $data['isian_pilgan'][$x],
                            'opsi' => $data['input_pilgan'][$x],
                            'is_active' => '1'
                        ];
                        if (!empty($data['add_pilgan'][$x])) {
                            $option['isian'] = $data['add_pilgan'][$x];
                        }
                        $this->db->where('id_opsi', $data['id_pilgan'][$x]);
                        $this->db->update("kuesioner_alumni_question_option", $option);
                    } else {
                        //check kode
                        $checkKode =  $this->checkOption(array('answer_code' => $data['inputKodePilgan'][$x]));

                        if ($checkKode > 0) {
                            $return['success'] = false;
                            $return['message'] = 'Kode ' . $data['inputKodePilgan'][$x] . ' sudah ada';

                            goto End;
                        }

                        if (empty($data['input_pilgan'][$x])) {
                            continue;
                        }

                        //create option baru
                        $option = [
                            'id_opsi' => getUUID(),
                            'id_kuesioner' => $data['id_kuesioner'],
                            'answer_code' => $data['inputKodePilgan'][$x],
                            'answer_rules' => $data['isian_pilgan'][$x],
                            'opsi' => $data['input_pilgan'][$x],
                            'is_active' => '1'
                        ];
                        if (!empty($data['add_pilgan'][$x])) {
                            $option['isian'] = $data['add_pilgan'][$x];
                        }
                        $this->db->insert("kuesioner_alumni_question_option", $option);
                    }
                }

                // hapus option non active
                $opsiIsNonActive = $this->getAnswerOptionByKuesioner($data['id_kuesioner'], array('is_active' => '0'));

                foreach ($opsiIsNonActive as $key => $value) {
                    $checkOfferedAnswer = $this->getOfferedAnswerByOption($value['id_opsi']);

                    if ($checkOfferedAnswer == 0) {
                        $this->hapusOption($value['id_opsi'], $data['input_jenis']);
                    }
                }
                break;
            case "4":
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_alumni_question', $pertanyaan);

                //update semua kokcen non active
                $this->updateAnswerOption(array('id_kuesioner' => $data['id_kuesioner']), array('is_active' => '0'));

                for ($x = 0; $x < count($data['id_kokcen']); $x++) {
                    if ($data['id_kokcen'][$x] != "") {
                        //check kode
                        $checkKode =  $this->checkOption(array('answer_code' => $data['inputKodeKokcen'][$x], 'id_kuesioner' => $data['id_kuesioner']), $data['id_kokcen'][$x]);

                        if ($checkKode > 0) {
                            $return['success'] = false;
                            $return['message'] = 'Kode ' . $data['inputKodeKokcen'][$x] . ' sudah ada';

                            goto End;
                        }

                        //update kokcen
                        $option = [
                            'id_kuesioner' => $data['id_kuesioner'],
                            'answer_code' => $data['inputKodeKokcen'][$x],
                            'opsi' => $data['input_kokcen'][$x],
                            'is_active' => '1'
                        ];

                        $this->db->where('id_opsi', $data['id_kokcen'][$x]);
                        $this->db->update("kuesioner_alumni_question_option", $option);
                    } else {
                        //check kode
                        $checkKode =  $this->checkOption(array('answer_code' => $data['inputKodeKokcen'][$x]));

                        if ($checkKode > 0) {
                            $return['success'] = false;
                            $return['message'] = 'Kode ' . $data['inputKodeKokcen'][$x] . ' sudah ada';

                            goto End;
                        }

                        if (empty($data['input_kokcen'][$x])) {
                            continue;
                        }

                        //create kokcen baru
                        $option = [
                            'id_opsi' => getUUID(),
                            'id_kuesioner' => $data['id_kuesioner'],
                            'answer_code' => $data['inputKodeKokcen'][$x],
                            'opsi' => $data['input_kokcen'][$x],
                            'is_active' => '1'
                        ];
                        $this->db->insert("kuesioner_alumni_question_option", $option);
                    }
                }

                // hapus kokcen non active
                $opsiIsNonActive = $this->getAnswerOptionByKuesioner($data['id_kuesioner'], array('is_active' => '0'));

                foreach ($opsiIsNonActive as $key => $value) {
                    $checkOfferedAnswer = $this->getOfferedAnswerByOption($value['id_opsi']);

                    if ($checkOfferedAnswer == 0) {
                        $this->hapusOption($value['id_opsi'], $data['input_jenis']);
                    }
                }
                break;
            case "5":
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_alumni_question', $pertanyaan);

                //update semua likert non active
                $this->updateAnswerLikert(array('id_kuesioner' => $data['id_kuesioner']), array('is_active' => '0'));

                for ($x = 0; $x < count($data['id_likert']); $x++) {
                    if ($data['id_likert'][$x] != "") {
                        //check kode
                        $checkKode =  $this->checkLikert(array('likert_code' => $data['inputKodeLinier'][$x], 'id_kuesioner' => $data['id_kuesioner']), $data['id_likert'][$x]);

                        if ($checkKode > 0) {
                            $return['success'] = false;
                            $return['message'] = 'Kode ' . $data['inputKodeLinier'][$x] . ' sudah ada';

                            goto End;
                        }

                        //update likert
                        $option = [
                            'id_kuesioner' => $data['id_kuesioner'],
                            'likert_code' => $data['inputKodeLinier'][$x],
                            'pertanyaan' => $data['input_linier'][$x],
                            'is_active' => '1'
                        ];

                        $this->db->where('id_likert', $data['id_likert'][$x]);
                        $this->db->update("kuesioner_alumni_question_likert", $option);
                    } else {
                        //check kode
                        $checkKode =  $this->checkLikert(array('likert_code' => $data['inputKodeLinier'][$x]));

                        if ($checkKode > 0) {
                            $return['success'] = false;
                            $return['message'] = 'Kode ' . $data['inputKodeLinier'][$x] . ' sudah ada';

                            goto End;
                        }

                        if (empty($data['input_linier'][$x])) {
                            continue;
                        }

                        //create likert baru
                        $option = [
                            'id_likert' => getUUID(),
                            'id_kuesioner' => $data['id_kuesioner'],
                            'likert_code' => $data['inputKodeLinier'][$x],
                            'pertanyaan' => $data['input_linier'][$x],
                            'is_active' => '1'
                        ];
                        $this->db->insert("kuesioner_alumni_question_likert", $option);
                    }
                }

                // hapus likert non active
                $likertIsNonActive = $this->getAnswerLikertByKuesioner($data['id_kuesioner'], array('is_active' => '0'));

                foreach ($likertIsNonActive as $key => $value) {
                    $checkOfferedAnswer = $this->getOfferedAnswerByOption($value['id_likert']);

                    if ($checkOfferedAnswer == 0) {
                        $this->hapusOption($value['id_likert'], $data['input_jenis']);
                    }
                }
                break;
            case "6":
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_alumni_question', $pertanyaan);

                //update semua sub non active
                $this->updateAnswerSub(array('id_kuesioner' => $data['id_kuesioner']), array('is_active' => '0'));

                for ($x = 0; $x < count($data['id_sub']); $x++) {
                    if (!empty($data['id_sub'][$x])) {
                        //check kode
                        $checkKode =  $this->checkSub(array('sub_code' => $data['inputKodeSub'][$x]), $data['id_sub'][$x]);

                        if ($checkKode > 0) {
                            $return['success'] = false;
                            $return['message'] = 'Kode ' . $data['inputKodeSub'][$x] . ' sudah ada';

                            goto End;
                        }

                        //update sub
                        $option = [
                            'id_kuesioner' => $data['id_kuesioner'],
                            'sub_code' => $data['inputKodeSub'][$x],
                            'answer_rules' => $data['isian_sub'][$x],
                            'pertanyaan' => $data['input_sub'][$x],
                            'is_active' => '1'
                        ];

                        $this->db->where('id_sub', $data['id_sub'][$x]);
                        $this->db->update("kuesioner_alumni_question_sub", $option);
                    } else {
                        //check kode
                        $checkKode =  $this->checkSub(array('sub_code' => $data['inputKodeSub'][$x]));

                        if ($checkKode > 0) {
                            $return['success'] = false;
                            $return['message'] = 'Kode ' . $data['inputKodeSub'][$x] . ' sudah ada';

                            goto End;
                        }

                        if (empty($data['input_sub'][$x])) {
                            continue;
                        }

                        //create sub baru
                        $option = [
                            'id_sub' => getUUID(),
                            'id_kuesioner' => $data['id_kuesioner'],
                            'sub_code' => $data['inputKodeSub'][$x],
                            'answer_rules' => $data['isian_sub'][$x],
                            'pertanyaan' => $data['input_sub'][$x],
                            'is_active' => '1'
                        ];
                        $this->db->insert("kuesioner_alumni_question_sub", $option);
                    }
                }

                // hapus sub non active
                $subIsNonActive = $this->getAnswerSubByKuesioner($data['id_kuesioner'], array('is_active' => '0'));

                foreach ($subIsNonActive as $key => $value) {
                    $checkOfferedAnswer = $this->getOfferedAnswerByOption($value['id_sub']);

                    if ($checkOfferedAnswer == 0) {
                        $this->hapusOption($value['id_sub'], $data['input_jenis']);
                    }
                }
                break;
            default:
                echo "Belum Ada";
        }


        if ($this->db->trans_status() === FALSE) {
            $return['success'] = false;
            $return['message'] = 'Data gagal diupdate';
            $this->db->trans_rollback();

            goto End;
        } else {
            $return['success'] = true;
            $return['message'] = 'Data berhasil diupdate';

            $this->db->trans_commit();

            goto End;
        }

        End:
        // $this->db->trans_complete();
        return $return;
    }

    public function hapusOption($id, $type)
    {
        switch ($type) {
            case "3":
                $this->db->where('id_opsi', $id);
                $cek = $this->db->delete('kuesioner_alumni_question_option');
                break;
            case "4":
                $this->db->where('id_opsi', $id);
                $cek = $this->db->delete('kuesioner_alumni_question_option');
                break;
            case "5":
                $this->db->where('id_likert', $id);
                $cek = $this->db->delete('kuesioner_alumni_question_likert');
                break;
            case "6":
                $this->db->where('id_sub', $id);
                $cek = $this->db->delete('kuesioner_alumni_question_sub');
                break;
            default:
                return "Jenis tidak ada";
        }
    }

    public function hapusSub($id)
    {
        $data = $this->getKuesioner($id);
        $cek = false;
        switch ($data['question_type']) {
            case "3":
                $this->db->where('id_kuesioner', $id);
                $cek = $this->db->delete('kuesioner_alumni_question_option');
                break;
            case "4":
                $this->db->where('id_kuesioner', $id);
                $cek = $this->db->delete('kuesioner_alumni_question_option');
                break;
            case "5":
                $this->db->where('id_kuesioner', $id);
                $cek = $this->db->delete('kuesioner_alumni_question_likert');
                break;
            case "6":
                $this->db->where('id_kuesioner', $id);
                $cek = $this->db->delete('kuesioner_alumni_question_sub');
                break;
            default:
                echo "Tidak Ada Hapus";
        }

        if ($cek) {
            $return['success'] = true;
            $return['message'] = 'Data berhasil dihapus';
        } else {
            $return['success'] = false;
            $return['message'] = 'Data gagal dihapus';
        }

        return $return;
    }

    public function hapusKue($id)
    {
        $data = [
            'is_active' => '0',
        ];
        $this->db->where('id_kuesioner', $id);
        $response = $this->db->update('kuesioner_alumni_question', $data);

        if ($response) {
            $return['success'] = true;
            $return['message'] = 'Data berhasil dihapus';
        } else {
            $return['success'] = false;
            $return['message'] = 'Data gagal dihapus';
        }
        return $return;
    }

    public function getKuesioner($id)
    {
        $this->db->select('*');
        $this->db->from('kuesioner_alumni_question');
        $this->db->where('id_kuesioner', $id);
        $get = $this->db->get()->row_array();
        return $get;
    }

    public function getAllKuesioner()
    {
        $this->db->select('*');
        $this->db->from('kuesioner_alumni_question');
        $get = $this->db->get()->result_array();
        return $get;
    }

    public function updateAnswerOption($where, $data)
    {
        $this->db->where($where);
        $this->db->update('kuesioner_alumni_question_option', $data);
        return $this->db->affected_rows();
    }

    public function updateAnswerLikert($where, $data)
    {
        $this->db->where($where);
        $this->db->update('kuesioner_alumni_question_likert', $data);
        return $this->db->affected_rows();
    }

    public function updateAnswerSub($where, $data)
    {
        $this->db->where($where);
        $this->db->update('kuesioner_alumni_question_sub', $data);
        return $this->db->affected_rows();
    }

    public function getOfferedAnswerByOption($id)
    {
        $this->db->from('kuesioner_alumni_offered_answer');
        $this->db->like('text', $id);
        return $this->db->get()->num_rows();
    }

    public function getOfferedAnswerByIdKuesioner($id_kuesioner)
    {
        $offered_answer = $this->db->get_where('kuesioner_alumni_offered_answer', array('id_kuesioner' => $id_kuesioner));
        if (!empty($offered_answer)) {
            $this->db->delete('kuesioner_alumni_offered_answer', array('id_kuesioner' => $id_kuesioner));
        }
        return true;
    }

    public function getAnswerOptionByKuesioner($id, $where)
    {
        $this->db->from('kuesioner_alumni_question_option kaqo');
        $this->db->where('kaqo.id_kuesioner', $id);
        $this->db->where($where);
        return $this->db->get()->result_array();
    }

    public function getAnswerLikertByKuesioner($id, $where)
    {
        $this->db->from('kuesioner_alumni_question_likert kaql');
        $this->db->where('kaql.id_kuesioner', $id);
        $this->db->where($where);
        return $this->db->get()->result_array();
    }

    public function getAnswerSubByKuesioner($id, $where)
    {
        $this->db->from('kuesioner_alumni_question_sub kaqs');
        $this->db->where('kaqs.id_kuesioner', $id);
        $this->db->where($where);
        return $this->db->get()->result_array();
    }

    public function getSubKuesioner($id, $sub)
    {
        if ($sub == '3') {
            $this->db->select('*');
            $this->db->from('kuesioner_alumni_question_option');
            $this->db->where('id_kuesioner', $id);
        } elseif ($sub == '4') {
            $this->db->select('*');
            $this->db->from('kuesioner_alumni_question_option');
            $this->db->where('id_kuesioner', $id);
        } elseif ($sub == '5') {
            $this->db->select('*');
            $this->db->from('kuesioner_alumni_question_likert');
            $this->db->where('id_kuesioner', $id);
        } else {
            $this->db->select('*');
            $this->db->from('kuesioner_alumni_question_sub');
            $this->db->where('id_kuesioner', $id);
        }
        $get = $this->db->get()->result_array();
        return $get;
    }
    
    /**
     * checkKuesioner
     * validasi lanjutan untuk kuesioner
     * @param  mixed $where
     * @param  mixed $id_kuesioner
     * @return void
     */
    public function checkKuesioner($where, $id_kuesioner = null)
    {
        $this->db->from('kuesioner_alumni_question');
        $this->db->where($where);

        if ($id_kuesioner) {
            $this->db->where('id_kuesioner !=', $id_kuesioner);
        }

        return $this->db->get()->num_rows();
    }

    public function checkOption($where, $id_opsi = null)
    {
        $this->db->from('kuesioner_alumni_question_option');
        $this->db->where($where);

        if ($id_opsi) {
            $this->db->where('id_opsi !=', $id_opsi);
        }

        return $this->db->get()->num_rows();
    }

    public function checkLikert($where, $id_likert = null)
    {
        $this->db->from('kuesioner_alumni_question_likert');
        $this->db->where($where);

        if ($id_likert) {
            $this->db->where('id_likert !=', $id_likert);
        }

        return $this->db->get()->num_rows();
    }

    public function checkSub($where, $id_sub = null)
    {
        $this->db->from('kuesioner_alumni_question_sub');
        $this->db->where($where);

        if ($id_sub) {
            $this->db->where('id_sub !=', $id_sub);
        }

        return $this->db->get()->num_rows();
    }

    public function aktif($id_kuesioner, $data)
    {
        $this->db->where('id_kuesioner', $id_kuesioner);
        $response = $this->db->update('kuesioner_alumni_question', $data);

        if ($response) {
            $return['success'] = true;
            $return['message'] = 'Berhasil Diaktivasi';
        } else {
            $return['success'] = false;
            $return['message'] = 'Tidak Berhasil Diaktivasi';
        }
        return $return;
    }

    public function simpanUrutan($id_kuesioner, $data)
    {
        $checkUrutan =  $this->checkKuesioner(array('urutan' => $data['urutan']), $id_kuesioner);

        if ($checkUrutan == 0) {
            $this->db->where('id_kuesioner', $id_kuesioner);
            $response = $this->db->update('kuesioner_alumni_question', $data);

            if ($response) {
                $return['success'] = true;
                $return['message'] = 'Berhasil Diaktivasi';
            } else {
                $return['success'] = false;
                $return['message'] = 'Tidak Berhasil Diaktivasi';
            }
        } else {
            $return['success'] = false;
            $return['message'] = 'Urutan tidak boleh sama';
        }

        return $return;
    }

    public function getParentKuesioner()
    {
        $this->db->select('id_kuesioner, question_code, question');
        $this->db->from('kuesioner_alumni_question');
        $this->db->where('parent_question_code', null);
        $this->db->not_like('question_code', 'f8');
        $get = $this->db->get()->result_array();
        return $get;
    }

    public function hapusSingkat($id)
    {
        $this->db->where('id_kuesioner', $id);
        $response = $this->db->delete('kuesioner_alumni_question');

        if ($response) {
            $return['success'] = true;
            $return['message'] = 'Data berhasil dihapus';
        } else {
            $return['success'] = false;
            $return['message'] = 'Data gagal dihapus';
        }
        return $return;
    }

    public function hapusPilgan($id)
    {
        $this->db->where('id_kuesioner', $id);
        $delete = $this->db->delete('kuesioner_alumni_question_option');
        return $delete;
    }

    public function hapusLikert($id)
    {
        $this->db->where('id_kuesioner', $id);
        $delete = $this->db->delete('kuesioner_alumni_question_likert');
        return $delete;
    }

    public function getStatus()
    {
        $return['status'] = 500;
        $return['data'] = [];

        $get = $this->db->query("SELECT id, status_alumni FROM ref_status_alumni where is_active='1'");
        if ($get->num_rows() != 0) {
            $return['status'] = 201;
            $return['data'] = $get->result_array();
        }
        return $return;
    }

    public function getStatusQuestion($group)
    {
        $return['status'] = 500;
        $return['data'] = [];

        $get = $this->db->query("SELECT id_status_alumni FROM kuesioner_alumni_question_relation where id_kuesioner=?", [$group]);
        if ($get->num_rows() != 0) {
            $data = [];
            foreach ($get->result_array() as $key => $value) {
                $data[] = $value['id_status_alumni'];
            }
            $return['status'] = 201;
            $return['data'] = $data;
        }
        return $return;
    }

    public function updateStatus($params)
    {
        $return['status'] = 500;
        $return['message'] = '';

        $this->db->trans_begin();
        $this->db->where("id_kuesioner", $params['id_kuesioner']);
        $this->db->delete("kuesioner_alumni_question_relation");

        $data = [];

        for ($i = 0; $i < count($params['kuesioner']); $i++) {
            $data[$i]['id_kuesioner'] = $params['id_kuesioner'];
            $data[$i]['id_status_alumni'] = $params['kuesioner'][$i];

            $this->db->insert('kuesioner_alumni_question_relation', $data[$i]);
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $return['status'] = 500;
            $return['message'] = 'Perubahan status alumni gagal';
        } else {
            $this->db->trans_commit();
            $return['status'] = 201;
            $return['message'] = 'Perubahan status alumni pada pertanyaan berhasil';
        }
        $this->db->trans_complete();
        return $return;
    }

    public function deleteStatus($params)
    {
        $this->db->trans_begin();
        $this->db->where("id_kuesioner", $params['id_kuesioner']);
        $this->db->delete("kuesioner_alumni_question_relation");
        $this->db->trans_commit();
        $return['status'] = 201;
        $return['message'] = 'Perubahan status alumni pada pertanyaan berhasil';
        $this->db->trans_complete();
        return $return;
    }

    public function getRelasiQuestion($id_kuesioner)
    {
    }

    public function getKuesionerStatus($id_kuesioner)
    {
        $this->db->where('id_kuesioner', $id_kuesioner);
        $query = $this->db->get('kuesioner_alumni_question_relation');
        return $query->result();
    }

    public function deleteKuesionerWithStatus($id_kuesioner)
    {
        $this->db->trans_begin();
        $this->db->where("id_kuesioner", $id_kuesioner);
        $this->db->delete("kuesioner_alumni_question_relation");
        $this->db->trans_commit();
        $this->db->trans_complete();
        return true;
    }
}
