<?php

class M_survey_mitra extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function cekData($data)
    {
        $isCheck = $this->checkJadwal(array('nama' => $data['nama'], 'start_date' => $data['start_date'], 'end_date' => $data['end_date']));

        if ($isCheck) {
            return [
                "status"  => 201,
                "message" => "Berhasil Ditambahkan"
            ];
        }

        return [
            "status"  => 500,
            "message" => "Prodi dan Angkatan sudah digunakan pada Jenis survey ini."
        ];

    }

    public function checkJadwal($where)
    {
        $this->db->where($where);

        $get = $this->db->get('ref_jadwal_survey_mitra');

        if ($get->num_rows() > 0) {
            return false;
        }

        return true;
    }

}
