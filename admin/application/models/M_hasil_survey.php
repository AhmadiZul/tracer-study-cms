<?php

class M_hasil_survey extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getJenisSurveyById($id)
    {
        $this->db->join('ref_jenis_survey rjs', 'rjds.id_ref_jenis_survey=rjs.id');
        $this->db->where('id_survey', $id);
        return $this->db->get('ref_jadwal_survey rjds')->row();
    }

    public function get_status()
    {
        $this->db->select('id, status_alumni');
        $this->db->from('ref_status_alumni');
        $this->db->where('is_active', 1);
        return $this->db->get()->result();
    }

    public function count_alumni_by_status_alumni($id_survey, $id_status_alumni)
    {
        $this->db->from('kuesioner_alumni_answer kaa');
        $this->db->where('id_survey', $id_survey);
        $this->db->where('id_status_alumni', $id_status_alumni);
        return $this->db->count_all_results();
    }
    public function get_question_category()
    {
        $this->db->select('kaqc.id_category, kaqc.name');
        $this->db->from('kuesioner_alumni_question_category kaqc');
        $this->db->where('is_active', 1);
        return $this->db->get()->result();
    }

    public function count_alumni_by_question_category($id_survey, $id_question_category)
    {
        $this->db->from('kuesioner_alumni_answer kaa');
        $this->db->where('id_survey', $id_survey);
        $this->db->where('id_question_category', $id_question_category);
        return $this->db->count_all_results();
    }

    public function get_question_by_status($id_status)
    {
        $this->db->select('kaq.id_kuesioner, kaq.question_code, kaq.question, kaq.question_type, kaq.urutan, kaq.others, kaqr.id_status_alumni');
        $this->db->from('kuesioner_alumni_question_relation kaqr');
        $this->db->join('kuesioner_alumni_question kaq', 'kaq.id_kuesioner = kaqr.id_kuesioner');
        $this->db->where('kaqr.id_status_alumni', $id_status);
        return $this->db->get()->result();
        // echo "<pre>";
        // var_dump($this->db->last_query());
        // echo "</pre>";
        // die();
    }
    public function get_question_by_category($id_question_category)
    {
        $this->db->select('kaq.id_kuesioner, kaq.question_code, kaq.question, kaq.question_type, kaq.urutan, kaq.others');
        $this->db->from('kuesioner_alumni_question kaq');
        $this->db->where('kaq.is_active', "1");
        $this->db->where('kaqr.status', 1);
        $this->db->where('kaq.id_category', $id_question_category);
        $this->db->order_by('kaq.urutan');
        return $this->db->get()->result();
    }

    public function get_option_by_question($id_kuesioner)
    {
        $this->db->select('kaqo.id_opsi, kaqo.opsi, kaqo.isian');
        $this->db->from('kuesioner_alumni_question_option kaqo');
        $this->db->where('kaqo.is_active', '1');
        $this->db->where('kaqo.id_kuesioner', $id_kuesioner);
        return $this->db->get()->result();
    }

    public function get_likert_by_question($id_kuesioner)
    {
        $this->db->select('kaql.id_likert, kaql.pertanyaan');
        $this->db->from('kuesioner_alumni_question_likert kaql');
        $this->db->where('kaql.is_active', '1');
        $this->db->where('kaql.id_kuesioner', $id_kuesioner);
        return $this->db->get()->result();
    }

    public function get_sub_by_question($id_kuesioner)
    {
        $this->db->select('kaqs.id_sub, kaqs.pertanyaan');
        $this->db->from('kuesioner_alumni_question_sub kaqs');
        $this->db->where('kaqs.is_active', '1');
        $this->db->where('kaqs.id_kuesioner', $id_kuesioner);
        return $this->db->get()->result();
    }

    public function get_jawaban_isian($id_kuesioner, $id_survey)
    {
        $this->db->distinct();
        $this->db->select('kaoa.text');
        $this->db->from('kuesioner_alumni_offered_answer kaoa');
        $this->db->join('kuesioner_alumni_answer kaa', 'kaoa.id_answer=kaa.id_answer', 'LEFT');
        $this->db->where('kaoa.id_kuesioner', $id_kuesioner);
        $this->db->where('kaa.id_survey', $id_survey);
        return $this->db->get()->result();
    }

    public function get_jawaban_option($id_kuesioner, $id_survey, $id_opsi, $is_count)
    {
        $this->db->select('kaoa.text');
        $this->db->from('kuesioner_alumni_offered_answer kaoa');
        $this->db->join('kuesioner_alumni_answer kaa', 'kaoa.id_answer=kaa.id_answer', 'LEFT');
        $this->db->where('kaoa.id_kuesioner', $id_kuesioner);
        $this->db->where('kaa.id_survey', $id_survey);
        $this->db->like('kaoa.text', $id_opsi);

        if ($is_count) {
            return $this->db->count_all_results();
        }
        return $this->db->get()->result();
    }

    public function get_jawaban_likert($id_kuesioner, $id_survey)
    {
        $this->db->select('kaoa.text');
        $this->db->from('kuesioner_alumni_offered_answer kaoa');
        $this->db->join('kuesioner_alumni_answer kaa', 'kaoa.id_answer=kaa.id_answer', 'LEFT');
        $this->db->where('kaoa.id_kuesioner', $id_kuesioner);
        $this->db->where('kaa.id_survey', $id_survey);
        return $this->db->get()->result();
    }

    public function get_jawaban_sub($id_kuesioner, $id_survey)
    {
        $this->db->select('kaoa.text');
        $this->db->from('kuesioner_alumni_offered_answer kaoa');
        $this->db->join('kuesioner_alumni_answer kaa', 'kaoa.id_answer=kaa.id_answer', 'LEFT');
        $this->db->where('kaoa.id_kuesioner', $id_kuesioner);
        $this->db->where('kaa.id_survey', $id_survey);
        return $this->db->get()->result();
    }

    public function get_provinsi_by_id($id)
    {
        $this->db->select('rp.nama_provinsi');
        $this->db->from('ref_provinsi rp');
        $this->db->where('rp.kode_provinsi', $id);
        return $this->db->get()->row();
    }

    public function get_kab_kota_by_id($id)
    {
        $this->db->distinct();
        $this->db->select('rkk.nama_kab_kota');
        $this->db->from('ref_kab_kota rkk');
        $this->db->where('rkk.kode_kab_kota', $id);
        return $this->db->get()->row();
    }

    public function get()
    {
        # code...
    }
}
