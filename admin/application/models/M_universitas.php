<?php

class M_universitas extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getPerguruanTinggi($where)
    {
        $this->db->from('ref_perguruan_tinggi');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function checkPT($where, $id)
    {
        $this->db->from('ref_perguruan_tinggi');
        $this->db->where($where);
        $this->db->where('id_perguruan_tinggi !=', $id);

        if ($this->db->get()->num_rows() > 0) {
            return false;
        }
        return true;
    }

    public function checkUser($where, $id)
    {
        $this->db->from('user');
        $this->db->where($where);
        $this->db->where('id_user !=', $id);

        if ($this->db->get()->num_rows() > 0) {
            return false;
        }
        return true;
    }

    public function simpan_user($data){
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

    public function simpan_perguruan_tinggi($data)
    {
        $this->db->insert('ref_perguruan_tinggi', $data);
        return $this->db->insert_id();
    }

    public function edit_user($data,$where)
    {
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update('user');
        return $this->db->affected_rows();
    }

    public function edit_perguruan_tinggi($data,$where)
    {
        $this->db->update('ref_perguruan_tinggi',$data,$where);
        return $this->db->affected_rows();
    }


    public function delete($id)
    {
        # code...
        $return['status'] = 500;
        $return['message'] = '';

        $get = $this->edit($id);
        if ($get['status']==500) {
            $return['status'] = 500;
            $return['message'] = 'Data perguruan tinggi tidak ditemukan';
        } else {
            $this->db->trans_begin();
            $this->db->where("id_user", $get['data']['id_user']);
            $this->db->delete("user");
            $this->db->where("id_perguruan_tinggi", $id);
            $this->db->delete("ref_perguruan_tinggi");

            if ($this->db->trans_status()===true) {
                $this->db->trans_commit();
                $return['status'] = 201;
                $return['message'] = 'Data universitas berhasil dihapus';
                return $return;
                exit();
            } else {
                $this->db->trans_commit();
                $return['status'] = 500;
                $return['message'] = 'Data universitas gagal dihapus';
                return $return;
                exit();
            }
            $this->db->trans_complete();
        }
        return $return;

    }

    public function edit($id)
    {
        $return['status'] = 500;
        $return['data'] = [];
        $get = $this->db->query("SELECT * FROM ref_perguruan_tinggi where id_perguruan_tinggi=?",[$id]);
        if ($get->num_rows()!=0) {
            $return['status'] = 201;
            $return['data'] = $get->row_array();
        }else{
            $return['status'] = 500;
            $return['data'] = [];
        }
        return $return;
    }

    public function cekKodeProdi($data, $id_fakultas) 
    {
        $kode = $data["kode_prodi"];

        $this->db->where('kode_prodi',$kode);
        $this->db->where('id_fakultas',$id_fakultas);

        $get = $this->db->get("ref_prodi");
        
        if ($get->num_rows() > 0) {
            return [
                "status"  => 500,
                "message" => "Kode Prodi Sudah Digunakan"
            ];
        } else {
            return [
                "status"  => 201,
                "message" => "Berhasil Ditambahkan",
            ];
        }
    }
    
    /**
     * cekProdiInput
     * validasi untuk tambah atau edit prodi
     * @param  mixed $data
     * @param  mixed $id_fakultas
     * @param  mixed $isUpdate
     * @return void
     */
    public function cekProdiInput($data, $id_fakultas, $isUpdate) 
    {
        $kode = $data["kode_prodi"];
        $nama_prodi = $data["nama_prodi"];
        $singkatan = $data["singkatan"];

        $message = [];
        $cekSukses = 1;
        
        $this->db->where('kode_prodi',$kode);
        $this->db->where('id_fakultas',$id_fakultas);
        $kodeQuery = $this->db->get("ref_prodi");
        
        $this->db->where('nama_prodi', $nama_prodi);
        $this->db->where('id_fakultas',$id_fakultas);
        $prodiQuery = $this->db->get('ref_prodi');
        
        $this->db->where('singkatan', $singkatan);
        $this->db->where('id_fakultas',$id_fakultas);
        $singkatanQuery = $this->db->get('ref_prodi');
        
        if ((strlen($kode) < 3) || (strlen($kode) > 5)) {
            array_push($message, "Kode prodi di antara 3 sampai 5 digit<br>");
            $cekSukses = 0;
        }
        if ((strlen($singkatan) < 3) || (strlen($singkatan) > 5)) {
            array_push($message, "Singkatan prodi di antara 3 sampai 5 karakter<br>");
            $cekSukses = 0;
        }
        if ((strlen($nama_prodi) < 2) || (strlen($nama_prodi) > 25)) {
            array_push($message, "Nama prodi di antara 2 sampai 25 karakter<br>");
            $cekSukses = 0;
        }
        if(!$isUpdate){
            if ($kodeQuery->num_rows() > 0) {
                array_push($message, "Kode Prodi Sudah Digunakan<br>");
                $cekSukses = 0;
            }
            if ($prodiQuery->num_rows() > 0) {
                array_push($message, "Kode Prodi Sudah Digunakan<br>");
                $cekSukses = 0;
            } 
            if ($singkatanQuery->num_rows() > 0) {
                array_push($message, "Prodi Sudah Digunakan<br>");
                $cekSukses = 0;
            }
        }

        if($cekSukses){
            return [
                    "status"  => 201,
                    "message" => "Lolos Pengecekan",
                ];
        }else{
            return [
                "status" => 500,
                "message" => $message,
            ];
        }
    }    
    /**
     * cekFakultas
     * validasi untuk edit atau tambah data fakultas
     * @param  mixed $data
     * @param  mixed $isUpdate
     * @return void
     */
    public function cekFakultas($data, $isUpdate) 
    {
        $kode = $data["kode_fakultas"];
        $nama_fakultas = $data["nama_fakultas"];
        $singkatan = $data["singkatan"];

        $message = [];
        $cekSukses = 1;


        $this->db->where('kode_fakultas',$kode);
        $kodeQuery = $this->db->get("ref_fakultas");

        $this->db->where('nama_fakultas', $nama_fakultas);
        $fakultasQuery = $this->db->get('ref_fakultas');

        $this->db->where('singkatan', $singkatan);
        $singkatanQuery = $this->db->get('ref_fakultas');

        if (strlen($kode) > 2) {
            array_push($message, "Kode wajib dibawah 2 digit<br>");
            $cekSukses = 0;
        }
        if ((strlen($singkatan) < 2) || (strlen($singkatan) > 5)) {
            array_push($message, "Singkatan fakultas di antara 2 sampai 5 karakter<br>");
            $cekSukses = 0;
        }
        if ((strlen($nama_fakultas) < 2) || (strlen($nama_fakultas) > 25)) {
            array_push($message, "Nama fakultas di antara 2 sampai 25 karakter<br>");
            $cekSukses = 0;
        }
        if(!$isUpdate){
            if ($kodeQuery->num_rows() > 0) {
                array_push($message, "Kode Fakultas Sudah Digunakan<br>");
                $cekSukses = 0;
            }
            if ($fakultasQuery->num_rows() > 0) {
                array_push($message, "Kode Fakultas Sudah Digunakan<br>");
                $cekSukses = 0;
            } 
            if ($singkatanQuery->num_rows() > 0) {
                array_push($message, "Singkatan Sudah Digunakan<br>");
                $cekSukses = 0;
            }
        }

        if($cekSukses){
            return [
                    "status"  => 201,
                    "message" => "Lolos Pengecekan",
                ];
        }else{
            return [
                "status" => 500,
                "message" => $message,
            ];
        }
    }
    
    /**
     * checkAlumniProdi
     * mengecek apakah prodi memiliki alumni
     * @param  mixed $prodi
     * @return void
     */
    public function checkAlumniProdi($prodi)
    {
        $this->db->where('id_prodi', $prodi);
        $alumni = $this->db->get('alumni')->result();

        if ($alumni) {
            return [
                "status"  => 500,
                "message" => "Prodi telah digunakan untuk data alumni!"
            ];
        }
        return true;
    }
    
    /**
     * checkAlumni
     * mengecek apakah fakultas memiliki alumni
     * @param  mixed $fakultas
     * @return void
     */
    public function checkAlumni($fakultas)
    {
        $this->db->where('id_fakultas', $fakultas);
        $alumni = $this->db->get('alumni')->result();

        if ($alumni) {
            return [
                "status"  => 500,
                "message" => "Fakultas telah digunakan untuk data alumni!"
            ];
        }
        return true;
    }
    
    /**
     * checkProdi
     * mengecek apakah fakultas memiliki prodi
     * @param  mixed $fakultas
     * @return void
     */
    public function checkProdi($fakultas)
    {
        $this->db->where('id_fakultas', $fakultas);
        $alumni = $this->db->get('ref_prodi')->result();

        if ($alumni) {
            return [
                "status"  => 500,
                "message" => "Fakultas telah digunakan untuk data prodi!"
            ];
        }
        return true;
    }
    
    /**
     * deleteFakultas
     * menghapus data fakultas dari database
     * @param  mixed $fakultas
     * @return void
     */
    public function deleteFakultas($fakultas)
    {
        $this->db->trans_begin();
        $this->db->where('id_fakultas', $fakultas);
        $this->db->delete('ref_fakultas');

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $return['status'] = 500;
            $return['message'] = 'Data Fakultas tidak ditemukan';
            $this->db->trans_rollback();
        } else {
            $return['status'] = 201;
            $return['message'] = 'Data Fakultas berhasil dihapus';
            $this->db->trans_commit();
        }

        return $return;
    }
    
    /**
     * deleteProdi
     * menghapus data prodi dari database
     * @param  mixed $prodi
     * @return void
     */
    public function deleteProdi($prodi)
    {
        $this->db->trans_begin();
        $this->db->where('id_prodi', $prodi);
        $this->db->delete('ref_prodi');

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $return['status'] = 500;
            $return['message'] = 'Data Prodi tidak ditemukan';
            $this->db->trans_rollback();
        } else {
            $return['status'] = 201;
            $return['message'] = 'Data Prodi berhasil dihapus';
            $this->db->trans_commit();
        }

        return $return;
    }
}
