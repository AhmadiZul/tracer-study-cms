<?php

class M_referensi extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->helper('function_helper');
  }

  public function getProdi($params = null)
  {
    $prodi = $this->db->query("SELECT * FROM ref_prodi order by nama_prodi asc");

    if ($prodi->num_rows() != 0) {
      return $prodi->result_array();
    } else {
      return null;
    }
  }

  public function getStatusAlumni()
  {
    $status = $this->db->query("SELECT * FROM ref_status_alumni order by id asc");

    if ($status->num_rows() != 0) {
      return $status->result_array();
    } else {
      return null;
    }
  }

  public function getTahunLulus()
  {
    $this->db->from('ref_tahun');
        $this->db->order_by('tahun');
        $this->db->where('is_active','1');
        return $this->db->get()->result_array();
  }

  // $tahun_lulus = $this->db->query("SELECT * FROM ref_tahun order by tahun asc");
  //   // $tahun_lulus = $ci->db->query("SELECT * FROM ref_propinsi where status='1' order by nama_prop asc");

  //   if ($tahun_lulus->num_rows() != 0) {
  //     return $tahun_lulus->result_array();
  //   } else {
  //     return null;
  //   }

  public function getUniv()
  {
    $this->db->from('ref_perguruan_tinggi');
    $fakultas = $this->db->get();
    return $fakultas->row();
  }
  public function getFakultas()
  {
    $this->db->from('ref_fakultas rf');
    $this->db->order_by('nama_fakultas', 'ASC');
    $fakultas = $this->db->get();

    if ($fakultas->num_rows() != 0) {
      return $fakultas->result_array();
    } else {
      return null;
    }
  }

  public function getListProdi($params = null)
  {
    if ($params != null) {
      $this->db->select('nama_prodi');
      $this->db->from('ref_prodi');
      $this->db->where('id_prodi', $params);
      $get = $this->db->get()->row_array();
      return $get;
    } else {
      $this->db->order_by('nama_prodi', 'ASC');

      $results = $this->db->get('ref_prodi')->result_array();
      $return = array();

      foreach ($results as $key => $value) {
        $return[$value['kode_prodi']] = $value["nama_prodi"];
      }

      return $return;
    }
  }

  function getSelectProdi($id_fakultas = null)
  {
    $data = $this->getProdiByFakultas($id_fakultas);
    $html = '<option value="">Pilih Prodi</option>';
    if ($data != null) {
      foreach ($data as $rows) {
        $html .= '<option value="' . $rows['id_prodi'] . '">' . ucwords(strtolower($rows['nama_prodi'])) . '</option>';
      }
    }
    $return = $html;
    return $return;
  }

  function getProdiByFakultas($id)
  {
    $get = $this->db->query("SELECT * FROM ref_prodi where id_fakultas=? order by nama_prodi ", array($id));

    if ($get->num_rows() != 0) {
      return $get->result_array();
    } else {
      return null;
    }
  }

  public function getListUniv($params = null)
  {
    $this->db->select('nama_resmi, nama_pendek');
    $this->db->from('ref_perguruan_tinggi');
    $this->db->where('id_perguruan_tinggi', $params);
    $get = $this->db->get()->row_array();
    return $get;
  }


  public function getListStatusAlumni()
  {
    $this->db->order_by('status_alumni', 'ASC');

    $results = $this->db->get('ref_status_alumni')->result_array();
    $return = array();

    foreach ($results as $key => $value) {
      $return[$value['id']] = $value["status_alumni"];
    }

    return $return;
  }

  public function getJenisSurvey()
  {
    $this->db->select('id, jenis_survey');
    $this->db->from('ref_jenis_survey');
    $get = $this->db->get()->result_array();
    return $get;
  }
}
