<?php

class M_mitra extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function checkMitra($where, $id = null)
    {
        $this->db->from('mitra');
        $this->db->where($where);

        if ($id) {
            $this->db->where('id_instansi !=', $id);
        }

        if ($this->db->get()->num_rows() > 0) {
            return false;
        }

        return true;
    }

    public function checkUser($where, $id = null)
    {
        $this->db->from('user');
        $this->db->where($where);

        if ($id) {
            $this->db->where('id_user !=', $id);
        }

        if ($this->db->get()->num_rows() > 0) {
            return false;
        }

        return true;
    }

    public function getMitra($id_mitra)
    {
        $this->db->select('*');
        $this->db->from('mitra');
        $this->db->where('id_instansi', $id_mitra);
        $get = $this->db->get()->row_array();
        return $get;
    }

    public function simpan_mitra($data)
    {
        $this->db->insert('mitra', $data);
        return $this->db->affected_rows();
    }

    public function simpan_user($data)
    {
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

    public function edit_user($data, $where)
    {
        $this->db->update('user', $data, $where);
        return $this->db->affected_rows();
    }

    public function edit_mitra($data, $where)
    {
        $this->db->update('mitra', $data, $where);
        return $this->db->affected_rows();
    }

    public function verifMitra($id_instansi, $data)
    {
        $this->db->where('id_instansi', $id_instansi);
        $response = $this->db->update('mitra', $data);

        if ($response) {
            $return['success'] = true;
            $return['message'] = 'Data berhasil diubah';
        } else {
            $return['success'] = false;
            $return['message'] = 'Data Tidak berhasil diubah';
        }
        return $return;
    }

    public function getListProdi()
    {
        $this->db->order_by('nama_prodi', 'ASC');

        $results = $this->db->get('ref_prodi')->result_array();
        $return = array();

        foreach ($results as $key => $value) {
            $return[$value['id_prodi']] = $value["nama_prodi"];
        }

        return $return;
    }

    public function getListUniv()
    {
        $this->db->order_by('nama_resmi', 'ASC');

        $results = $this->db->get('ref_perguruan_tinggi')->result_array();
        $return = array();

        foreach ($results as $key => $value) {
            $return[$value['id_perguruan_tinggi']] = $value["nama_resmi"];
        }

        return $return;
    }

    public function createKuesioner($data)
    {
        $this->db->trans_begin();

        $checkUrutan =  $this->checkKuesioner(array('urutan' => $data['urutan']));

        if ($checkUrutan > 0) {
            $return['success'] = false;
            $return['message'] = 'Urutan tidak boleh sama';

            goto End;
        }

        $pertanyaan = [
            'id_kuesioner' => getUUID(),
            'question' => $data['input_pertanyaan'],
            'question_type' => $data['input_jenis'],
            'urutan' => $data['urutan']
        ];

        if (!empty($data['lainnya'])) {
            $pertanyaan['others'] = $data['lainnya'];
        }

        switch ($data['input_jenis']) {
            case "1":
                $pertanyaan['answer_rules'] = $data['input_singkat'];
                $this->db->insert("kuesioner_mitra_question", $pertanyaan);
                break;
            case "2":
                $pertanyaan['others'] = $data['input_paragraf'];
                $this->db->insert("kuesioner_mitra_question", $pertanyaan);
                break;
            case "3":
                $this->db->insert("kuesioner_mitra_question", $pertanyaan);
                for ($x = 0; $x < (int)$data['total']; $x++) {
                    $option = [
                        'id_opsi' => getUUID(),
                        'id_kuesioner' => $pertanyaan['id_kuesioner'],
                        'answer_rules' => $data['isian_pilgan' . (string)$x],
                        'opsi' => $data['input_pilgan' . (string)$x]
                    ];
                    if (!empty($data['add_pilgan' . (string)$x])) {
                        $option['isian'] = $data['add_pilgan' . (string)$x];
                    }
                    $this->db->insert("kuesioner_mitra_question_option", $option);
                }
                break;
            case "4":
                $this->db->insert("kuesioner_mitra_question", $pertanyaan);
                for ($x = 0; $x < (int)$data['totalU']; $x++) {
                    $option = [
                        'id_opsi' => getUUID(),
                        'id_kuesioner' => $pertanyaan['id_kuesioner'],
                        'opsi' => $data['input_kokcen' . (string)$x]
                    ];
                    $this->db->insert("kuesioner_mitra_question_option", $option);
                }
                break;
            case "5":
                $this->db->insert("kuesioner_mitra_question", $pertanyaan);
                for ($x = 0; $x < (int)$data['totalL']; $x++) {
                    $option = [
                        'id_likert' => getUUID(),
                        'id_kuesioner' => $pertanyaan['id_kuesioner'],
                        'pertanyaan' => $data['input_linier' . (string)$x]
                    ];
                    $this->db->insert("kuesioner_mitra_question_likert", $option);
                }
                break;
            case "6":

                break;
            default:
                $return['success'] = false;
                $return['message'] = 'Jenis pertanyaan tidak ada';

                goto End;
        }


        if ($this->db->trans_status() === FALSE) {
            $return['success'] = false;
            $return['message'] = 'Data gagal ditambahkan';
            $this->db->trans_rollback();

            goto End;
        } else {
            $return['success'] = true;
            $return['message'] = 'Data berhasil ditambahkan';

            $this->db->trans_commit();

            goto End;
        }

        End:
        $this->db->trans_complete();
        return $return;
    }

    public function hapusKue($id)
    {
        $data = [
            'is_active' => '0'
        ];
        $this->db->where('id_kuesioner', $id);
        $response = $this->db->update('kuesioner_mitra_question', $data);

        if ($response) {
            $return['success'] = true;
            $return['message'] = 'Data berhasil dihapus';
        } else {
            $return['success'] = false;
            $return['message'] = 'Data gagal dihapus';
        }
        return $return;
    }

    public function getKueSub($id)
    {
        $this->db->select('k.*, l.pertanyaan, o.opsi');
        $this->db->from('kuesioner_mitra_question k');
        $this->db->join('kuesioner_mitra_question_likert l', 'k.id_kuesioner = l.id_kuesioner', 'left');
        $this->db->join('kuesioner_mitra_question_option o', 'k.id_kuesioner = o.id_kuesioner', 'left');
        $this->db->where('k.id_kuesioner', $id);
        $get = $this->db->get()->result_array();
        return $get;
    }

    public function updateKuesioner($data)
    {
        $this->db->trans_begin();

        $checkUrutan =  $this->checkKuesioner(array('urutan' => $data['urutan']), $data['id_kuesioner']);

        if ($checkUrutan > 0) {
            $return['success'] = false;
            $return['message'] = 'Urutan tidak boleh sama';

            goto End;
        }

        $pertanyaan = [
            'question' => $data['input_pertanyaan'],
            'question_type' => $data['input_jenis'],
            'urutan' => $data['urutan']
        ];

        if (!empty($data['lainnya'])) {
            $pertanyaan['others'] = $data['lainnya'];
        }

        switch ($data['input_jenis']) {
            case "1":
                $pertanyaan['answer_rules'] = $data['input_singkat'];
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_mitra_question', $pertanyaan);
                break;
            case "2":
                $pertanyaan['others'] = $data['input_paragraf'];
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_mitra_question', $pertanyaan);
                break;
            case "3":
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_mitra_question', $pertanyaan);

                //update semua option non active
                $this->updateAnswerOption(array('id_kuesioner' => $data['id_kuesioner']), array('is_active' => '0'));


                //simpan update option
                for ($x = 0; $x < count($data['id_pilgan']); $x++) {
                    if (!empty($data['id_pilgan'][$x])) {
                        //update option
                        $option = [
                            'id_kuesioner' => $data['id_kuesioner'],
                            'answer_rules' => $data['isian_pilgan'][$x],
                            'opsi' => $data['input_pilgan'][$x],
                            'is_active' => '1'
                        ];
                        if (!empty($data['add_pilgan'][$x])) {
                            $option['isian'] = $data['add_pilgan'][$x];
                        }
                        $this->db->where('id_opsi', $data['id_pilgan'][$x]);
                        $this->db->update("kuesioner_mitra_question_option", $option);
                    } else {
                        if (empty($data['input_pilgan'][$x])) {
                            continue;
                        }

                        //create option baru
                        $option = [
                            'id_opsi' => getUUID(),
                            'id_kuesioner' => $data['id_kuesioner'],
                            'answer_rules' => $data['isian_pilgan'][$x],
                            'opsi' => $data['input_pilgan'][$x],
                            'is_active' => '1'
                        ];
                        if (!empty($data['add_pilgan'][$x])) {
                            $option['isian'] = $data['add_pilgan'][$x];
                        }
                        $this->db->insert("kuesioner_mitra_question_option", $option);
                    }
                }

                // hapus option non active
                $opsiIsNonActive = $this->getAnswerOptionByKuesioner($data['id_kuesioner'], array('is_active' => '0'));

                foreach ($opsiIsNonActive as $key => $value) {
                    $checkOfferedAnswer = $this->getOfferedAnswerByOption($value['id_opsi']);

                    if ($checkOfferedAnswer == 0) {
                        $this->hapusSub($value['id_opsi'], $data['input_jenis']);
                    }
                }
                break;
            case "4":
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_mitra_question', $pertanyaan);

                //update semua kokcen non active
                $this->updateAnswerOption(array('id_kuesioner' => $data['id_kuesioner']), array('is_active' => '0'));


                //simpan update kokcen
                for ($x = 0; $x < count($data['id_kokcen']); $x++) {
                    if (!empty($data['id_kokcen'][$x])) {
                        //update kokcen
                        $option = [
                            'id_kuesioner' => $data['id_kuesioner'],
                            'opsi' => $data['input_kokcen'][$x],
                            'is_active' => '1'
                        ];

                        $this->db->where('id_opsi', $data['id_kokcen'][$x]);
                        $this->db->update("kuesioner_mitra_question_option", $option);
                    } else {
                        if (empty($data['input_kokcen'][$x])) {
                            continue;
                        }

                        //create kokcen baru
                        $option = [
                            'id_opsi' => getUUID(),
                            'id_kuesioner' => $data['id_kuesioner'],
                            'opsi' => $data['input_kokcen'][$x],
                            'is_active' => '1'
                        ];
                        $this->db->insert("kuesioner_mitra_question_option", $option);
                    }
                }

                // hapus kokcen non active
                $opsiIsNonActive = $this->getAnswerOptionByKuesioner($data['id_kuesioner'], array('is_active' => '0'));

                foreach ($opsiIsNonActive as $key => $value) {
                    $checkOfferedAnswer = $this->getOfferedAnswerByOption($value['id_opsi']);

                    if ($checkOfferedAnswer == 0) {
                        $this->hapusSub($value['id_opsi'], $data['input_jenis']);
                    }
                }
                break;
            case "5":
                $this->db->where('id_kuesioner', $data['id_kuesioner']);
                $this->db->update('kuesioner_mitra_question', $pertanyaan);

                //update semua likert non active
                $this->updateAnswerLikert(array('id_kuesioner' => $data['id_kuesioner']), array('is_active' => '0'));


                //simpan update likert
                for ($x = 0; $x < count($data['id_likert']); $x++) {
                    if (!empty($data['id_likert'][$x])) {
                        //update likert
                        $option = [
                            'id_kuesioner' => $data['id_kuesioner'],
                            'pertanyaan' => $data['input_linier'][$x],
                            'is_active' => '1'
                        ];

                        $this->db->where('id_likert', $data['id_likert'][$x]);
                        $this->db->update("kuesioner_mitra_question_likert", $option);
                    } else {
                        if (empty($data['input_linier'][$x])) {
                            continue;
                        }

                        //create likert baru
                        $option = [
                            'id_likert' => getUUID(),
                            'id_kuesioner' => $data['id_kuesioner'],
                            'pertanyaan' => $data['input_linier'][$x],
                            'is_active' => '1'
                        ];
                        $this->db->insert("kuesioner_mitra_question_likert", $option);
                    }
                }
                // hapus likert non active
                $opsiIsNonActive = $this->getAnswerLikertByKuesioner($data['id_kuesioner'], array('is_active' => '0'));

                foreach ($opsiIsNonActive as $key => $value) {
                    $checkOfferedAnswer = $this->getOfferedAnswerByOption($value['id_likert']);

                    if ($checkOfferedAnswer == 0) {
                        $this->hapusSub($value['id_likert'], $data['input_jenis']);
                    }
                }
                break;

            default:
                echo "Belum Ada";
        }


        if ($this->db->trans_status() === FALSE) {
            $return['success'] = false;
            $return['message'] = 'Data gagal diubah';
            $this->db->trans_rollback();

            goto End;
        } else {
            $return['success'] = true;
            $return['message'] = 'Data berhasil diubah';

            $this->db->trans_commit();

            goto End;
        }
        $this->db->trans_complete();

        End:
        return $return;
    }

    public function hapusSub($id, $jenis)
    {
        $cek = false;
        switch ($jenis) {
            case "3":
                $this->db->where('id_opsi', $id);
                $cek = $this->db->delete('kuesioner_mitra_question_option');
                break;
            case "4":
                $this->db->where('id_opsi', $id);
                $cek = $this->db->delete('kuesioner_mitra_question_option');
                break;
            case "5":
                $this->db->where('id_likert', $id);
                $cek = $this->db->delete('kuesioner_mitra_question_likert');
                break;

            default:
                $cek = "Tidak Ada Hapus";
        }

        if ($cek) {
            $return['success'] = true;
            $return['message'] = 'Data berhasil dihapus';
        } else {
            $return['success'] = false;
            $return['message'] = 'Data gagal dihapus';
        }

        return $return;
    }

    public function getKuesioner($id)
    {
        $this->db->select('*');
        $this->db->from('kuesioner_mitra_question');
        $this->db->where('id_kuesioner', $id);
        $get = $this->db->get()->row_array();
        return $get;
    }

    public function updateAnswerOption($where, $data)
    {
        $this->db->where($where);
        $this->db->update('kuesioner_mitra_question_option', $data);
        return $this->db->affected_rows();
    }

    public function updateAnswerLikert($where, $data)
    {
        $this->db->where($where);
        $this->db->update('kuesioner_mitra_question_likert', $data);
        return $this->db->affected_rows();
    }

    public function getOfferedAnswerByOption($id)
    {
        $this->db->from('kuesioner_mitra_offered_answer');
        $this->db->like('text', $id);
        return $this->db->get()->num_rows();
    }

    public function getAnswerOptionByKuesioner($id, $where)
    {
        $this->db->from('kuesioner_mitra_question_option kaqo');
        $this->db->where('kaqo.id_kuesioner', $id);
        $this->db->where($where);
        return $this->db->get()->result_array();
    }

    public function getAnswerLikertByKuesioner($id, $where)
    {
        $this->db->from('kuesioner_mitra_question_likert kaql');
        $this->db->where('kaql.id_kuesioner', $id);
        $this->db->where($where);
        return $this->db->get()->result_array();
    }

    public function checkKuesioner($where, $id_kuesioner = null)
    {
        $this->db->from('kuesioner_mitra_question');
        $this->db->where($where);

        if ($id_kuesioner) {
            $this->db->where('id_kuesioner !=', $id_kuesioner);
        }

        return $this->db->get()->num_rows();
    }

    public function checkOption($where, $id_opsi = null)
    {
        $this->db->from('kuesioner_mitra_question_option');
        $this->db->where($where);

        if ($id_opsi) {
            $this->db->where('id_opsi !=', $id_opsi);
        }

        return $this->db->get()->num_rows();
    }

    public function checkLikert($where, $id_likert = null)
    {
        $this->db->from('kuesioner_mitra_question_likert');
        $this->db->where($where);

        if ($id_likert) {
            $this->db->where('id_likert !=', $id_likert);
        }

        return $this->db->get()->num_rows();
    }

    public function aktif($id_kuesioner, $data)
    {
        $this->db->where('id_kuesioner', $id_kuesioner);
        $response = $this->db->update('kuesioner_mitra_question', $data);

        if ($response) {
            $return['success'] = true;
            $return['message'] = 'Berhasil Diaktivasi';
        } else {
            $return['success'] = false;
            $return['message'] = 'Tidak Berhasil Diaktivasi';
        }
        return $return;
    }

    public function simpanUrutan($id_kuesioner, $data)
    {
        $checkUrutan =  $this->checkKuesioner(array('urutan' => $data['urutan']), $id_kuesioner);

        if ($checkUrutan == 0) {
            $this->db->where('id_kuesioner', $id_kuesioner);
            $response = $this->db->update('kuesioner_mitra_question', $data);

            if ($response) {
                $return['success'] = true;
                $return['message'] = 'Berhasil Diaktivasi';
            } else {
                $return['success'] = false;
                $return['message'] = 'Tidak Berhasil Diaktivasi';
            }
        } else {
            $return['success'] = false;
            $return['message'] = 'Urutan tidak boleh sama';
        }

        return $return;
    }
}
