<?php

class M_talk extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function edit($id)
    {
        $return['status'] = 0;
        $return['data']   = [];

        $get = $this->db
        ->select('cf.*, pt.nama_resmi, pt.nama_pendek')
        ->join('ref_perguruan_tinggi pt', 'cf.id_perguruan_tinggi_lokasi=pt.id_perguruan_tinggi', 'left')
        ->where('cf.id', $id)
        ->get('company_talks cf');
        if ($get->num_rows() != 0) {
            $return['status'] = 201;
            $return['data']   = $get->row_object();
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }

        return $return;
    }

    public function pendaftar($params)
    {
        $return = array('total' => 0, 'rows' => array());

        $this->db->start_cache();
        $this->db->select('ct.id, ct.id_company_talks, ct.id_alumni, a.nama, rp.nama_prodi');

        if (isset($data['sSearch']) || $params['sSearch'] != '') {
            $search = $this->db->escape_str($params['sSearch']);
            $this->db->where("(a.nama LIKE '%{$search}%')");
        }
        if (!empty($data['id_company_talks']) || $params['id_company_talks'] != '') {
            $id_company_talks = $this->db->escape_str($params['id_company_talks']);
            $this->db->where("ct.id_company_talks", $id_company_talks);
        }
        $this->db->join('alumni a', 'ct.id_alumni = a.id_alumni', 'left');
        $this->db->join('ref_perguruan_tinggi rpt', 'a.id_perguruan_tinggi = rpt.id_perguruan_tinggi', 'left');
        $this->db->join('ref_prodi rp', 'a.id_prodi = rp.id_prodi', 'left');

        $this->db->stop_cache();
        $rs = $this->db->count_all_results('company_talks_pendaftar ct');
        $return['total'] = $rs;
        if ($return['total'] > 0) {
            $this->db->limit($params['limit'], $params['start']);
            $this->db->order_by('ct.waktu_daftar', 'desc');
            $rs = $this->db->get('company_talks_pendaftar ct');
            if ($rs->num_rows())
                $return['rows'] = $rs->result_array();
        }
        $this->db->flush_cache();
        return $return;
    }
    public function getFasilitas($params, $single = false)
    {
        $return['status'] = 0;
        $return['data']   = [];

        $this->db->select('*');
        $this->db->from('company_talks_fasilitas');
        if ($single) {
            $this->db->where('id', $params);
        }else{
            $this->db->where_in('id', $params);
        }

        $get = $this->db->get();
        if ($get->num_rows() != 0) {
            $data = ($single ? $get->row_object():$get->result_object());
            $return['status'] = 201;
            $return['data']   = $data;
        } else {
            $return['status'] = 500;
            $return['data']   = [];
        }
        return $return;
    }

    public function verifyTalk($data,$where)
    {
        $this->db->update('company_talks',$data,$where);
        return $this->db->affected_rows();
    }
}
