<?php

class M_berita extends CI_Model
{

    private $table = 'berita_crud';

    function __construct()
    {
        parent::__construct();
    }


    public function add_berita($data)
    {
        $this->db->insert('berita_crud', $data);
        return $this->db->affected_rows();
    }

    public function getBerita($where)
    {
        $this->db->from('berita_crud');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function editBerita($data, $id_berita)
    {
        $this->db->set($data);
        $this->db->where('id_berita', $id_berita);
        $this->db->update('berita_crud');
        return $this->db->affected_rows();
    }
}