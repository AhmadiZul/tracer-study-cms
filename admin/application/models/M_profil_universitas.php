<?php

class M_profil_universitas extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getCountAlumniBy($where = null, $count = true)
    {
        if ($where) {
            $this->db->where($where);
        }

        if ($count) {
            return $this->db->count_all_results('alumni a');
        }

        $this->db->select('a.id_alumni, a.id_ref_status_alumni');
        return $this->db->get('alumni a')->result();
    }

    public function getAdmin($where)
    {
        $this->db->from('user');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function getAlumniById($id_alumni)
    {
        $this->db->select('pt.id_perguruan_tinggi, pt.nama_resmi, pt.nama_pendek, pt.alamat, pt.kota, pt.no_telp, pt.kode_pos, pt.no_fax, pt.email');
        $this->db->from('ref_perguruan_tinggi pt');
        $this->db->where('pt.id_perguruan_tinggi',$id_alumni);
        return $this->db->get()->row_array();
    }

    public function get_alumni_by_id_user($id)
    {
        $this->db->select('u.username, pt.id_perguruan_tinggi, pt.nama_resmi, pt.nama_pendek, pt.alamat, pt.kota, pt.no_telp, pt.kode_pos, pt.no_fax, pt.email, pt.path_logo');
        $this->db->from('ref_perguruan_tinggi pt');
        $this->db->join('user u','pt.id_user=u.id_user','LEFT');
        $this->db->where('pt.id_user',$id);
        return $this->db->get()->row();
    }

    public function cek_alumni($where,$id_alumni)
    {
        $this->db->select('*');
        $this->db->from('alumni');
        $this->db->where($where);
        $this->db->where('id_alumni != ',$id_alumni);
        return $this->db->count_all_results();
    }

    public function cek_password($password,$id_user)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('password',$password);
        $this->db->where('id_user',$id_user);
        return $this->db->count_all_results();
    }

    public function cek_user($where,$id_user)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($where);
        $this->db->where('id_user != ',$id_user);
        return $this->db->count_all_results();
    }

    public function simpan_alumnijobs($data) {
        $this->db->insert('alumni_jobs', $data);
        return $this->db->affected_rows();
    }

    public function simpan_alumnistudy($data) {
        $this->db->insert('alumni_study', $data);
        return $this->db->affected_rows();
    }

    public function simpan_pengalaman($data)
    {
        $this->db->insert('alumni_pengalaman', $data);
        return $this->db->affected_rows();
    }

    public function simpan_organisasi($data)
    {
        $this->db->insert('alumni_organisasi', $data);
        return $this->db->affected_rows();
    }

    public function simpan_prestasi($data)
    {
        $this->db->insert('alumni_prestasi', $data);
        return $this->db->affected_rows();
    }

    public function simpan_skill($data)
    {
        $this->db->insert('alumni_skill', $data);
        return $this->db->affected_rows();
    }

    public function update_user($data,$id){
        $this->db->update('user',$data,array('id_user'=>$id));
        return $this->db->affected_rows();
    }

    public function update_pt($data,$id) {
        $this->db->update('ref_perguruan_tinggi',$data,array('id_perguruan_tinggi'=>$id));
        return $this->db->affected_rows();
    }

    public function update_alumnijobs($data,$id) {
        $this->db->update('alumni_jobs',$data,array('id_job'=>$id));
        return $this->db->affected_rows();
    }

    public function update_alumnistudy($data,$id) {
        $this->db->update('alumni_study',$data,array('id_study'=>$id));
        return $this->db->affected_rows();
    }

    public function update_pengalaman($data,$id) {
        $this->db->update('alumni_pengalaman',$data,array('id'=>$id));
        return $this->db->affected_rows();
    }

    public function update_organisasi($data,$id) {
        $this->db->update('alumni_organisasi',$data,array('id'=>$id));
        return $this->db->affected_rows();
    }

    public function update_prestasi($data,$id) {
        $this->db->update('alumni_prestasi',$data,array('id'=>$id));
        return $this->db->affected_rows();
    }

    public function update_skill($data,$id) {
        $this->db->update('alumni_skill',$data,array('id'=>$id));
        return $this->db->affected_rows();
    }

    public function update_sosmed($data,$id) {
        $this->db->update('alumni',$data,array('id_alumni'=>$id));
        return $this->db->affected_rows();
    }

    public function delete_pengalaman($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('alumni_pengalaman');
        return $this->db->affected_rows();
    }

    public function delete_organisasi($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('alumni_organisasi');
        return $this->db->affected_rows();
    }

    public function delete_prestasi($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('alumni_prestasi');
        return $this->db->affected_rows();
    }

    public function delete_skill($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('alumni_skill');
        return $this->db->affected_rows();
    }
}
