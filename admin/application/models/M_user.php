<?php

class M_user extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function tambahUser($data)
    {
        $this->db->insert('user', $data);
        return $this->db->affected_rows();
    }

    public function getUser($id_user)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('id_user', $id_user);
        $get = $this->db->get()->row_array();
        return $get;
    }

    public function getFakultas()
    {
        $get = $this->db->get('ref_fakultas')->result();
        return $get;
    }

    public function editUser($data, $id_user)
    {
        $this->db->where(['id_user'=>$id_user]);
        $this->db->update('user', $data);
        return $this->db->affected_rows();
    }

    // public function getRole()
    // {
    //     $this->db->select('u.id_group , ug.id_group, ug. nama_group');
    //     $this->db->from('user as u');
    //     $this->db->join('user_group ug', 'u.id_group=ug.id_group' , 'LEFT');
    //     $this->db->where('u.is_active', 1);

    //     return $this->db->get()->result();
        
    // }

    public function getRole()
  {
    $this->db->from('user_group ug');
    $this->db->order_by('nama_group', 'ASC');
    return $this->db->get()->result_array();
  }

    public function edit_mitra($data, $where)
    {
        $this->db->update('user', $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteUserAlumni($id_user)
    {
        $this->db->trans_begin();

        $this->db->where('id_user', $id_user);
        $alumni = $this->db->get('alumni')->row();
        if($alumni){
            $this->db->where('id_user', $id_user);
            $this->db->delete('alumni');
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $return['status'] = 500;
            $return['message'] = 'Data user tidak ditemukan';
            $this->db->trans_rollback();
        }
        else
        {
            $return['status'] = 201;
            $return['message'] = 'Data user berhasil dihapus';
            $this->db->trans_commit();
        }

        return $return;
    }

    public function deleteUserLog($id_user)
    {
        $this->db->trans_begin();
        
        $this->db->where('id_user', $id_user);
        $log = $this->db->get('activity_log')->result();
        if($log){
            $this->db->where('id_user', $id_user);
            $this->db->delete('activity_log');
        }
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            $return['status'] = 500;
            $return['message'] = 'Data user tidak ditemukan';
            $this->db->trans_rollback();
        }
        else
        {
            $return['status'] = 201;
            $return['message'] = 'Data user berhasil dihapus';
            $this->db->trans_commit();
        }

        return $return;
    }

    public function deleteUser($id_user)
    {
        $this->db->trans_begin();

        $this->db->where('id_user', $id_user);
        $user = $this->db->get('user')->row();
        if($user){
            $this->db->where('id_user', $id_user);
            $this->db->delete('user');
        }
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            $return['status'] = 500;
            $return['message'] = 'Data user tidak ditemukan';
            $this->db->trans_rollback();
        }
        else
        {
            $return['status'] = 201;
            $return['message'] = 'Data user berhasil dihapus';
            $this->db->trans_commit();
        }

        return $return;
    }
}