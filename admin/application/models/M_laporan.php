<?php

class M_laporan extends CI_Model
{

    private $table = 'laporan';

    function __construct()
    {
        parent::__construct();
    }


    public function add_laporan($data)
    {
        $this->db->insert('laporan', $data);
        return $this->db->affected_rows();
    }

    public function getLaporan($where)
    {
        $this->db->from('laporan');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function editLaporan($data, $id_laporan)
    {
        $this->db->set($data);
        $this->db->where('id_laporan', $id_laporan);
        $this->db->update('laporan');
        return $this->db->affected_rows();
    }
}