<?php

class M_tema extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getTema($key = null)
    {
        $this->db->select('s.*');
        $this->db->from('sistem_setting s');

        if ($key) {
            $this->db->where('key', $key);
            return $this->db->get()->row_array();
        }

        return $this->db->get()->result_array();
    }

    public function publishTema($key, $data)
    {
        $this->db->where('key', $key);
        $response = $this->db->update('setting_tema', $data);

        if($response) {
            $return['success'] = true;
            $return['message'] = 'Berhasil Dipublish';
        }
        else {
            $return['success'] = false;
            $return['message'] = 'Tidak Berhasil Dipublish';
        }
        return $return;
    }
}