<?php

class M_alumni extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function simpan_alumni($data)
    {
        $this->db->insert('alumni', $data);
        return $this->db->affected_rows();
    }

    public function simpan_user($data)
    {
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

    public function simpan_alumni_jobs($data)
    {
        $this->db->insert('alumni_jobs', $data);
        return $this->db->affected_rows();
    }

    public function simpan_alumni_study($data)
    {
        $this->db->insert('alumni_study', $data);
        return $this->db->affected_rows();
    }

    public function edit_user($data,$where)
    {
        $this->db->update('user',$data,$where);
        return $this->db->affected_rows();
    }

    public function edit_alumni($data,$where)
    {
        $this->db->update('alumni',$data,$where);
        return $this->db->affected_rows();
    }

    public function edit_alumni_jobs($data,$where)
    {
        $this->db->update('alumni_jobs',$data,$where);
        return $this->db->affected_rows();
    }

    public function edit_alumni_study($data,$where)
    {
        $this->db->update('alumni_study',$data,$where);
        return $this->db->affected_rows();
    }

    public function updateData($id_mhs, $data, $data2)
    {

        $this->db->trans_begin();
        

        $this->db->where('id_alumni', $id_mhs);
        $this->db->update('alumni', $data);

        $this->db->error();

        if(!empty($data2)) {
            if($data['id_ref_status_alumni'] == '3' || $data['id_ref_status_alumni'] == '1')
            {
                $this->db->where('id_alumni', $id_mhs);
                $this->db->update('alumni_jobs', $data2);

                $this->db->error();
            }
            if($data['id_ref_status_alumni'] == '4') {
                $this->db->where('id_alumni', $id_mhs);
                $this->db->update('alumni_study', $data2);

                $this->db->error();
            }
        }
        
 
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $return['success'] = false;
            $return['message'] = 'Data gagal diubah';
            $this->db->trans_rollback();
        }
        else
        {
            $return['success'] = true;
            $return['message'] = 'Data berhasil diubah';
            
            $this->db->trans_commit();
        }
        
        return $return;
    }

    public function createUser($params)
    {
        $return['status'] = 500;
        $return['message'] = '';
        $return['data'] = '';

        $check['email']= $params['email'];
        $checkUser = userValidation($check);
        if ($checkUser['status']==500) {
            $return['status'] = 500;
            $return['message'] = $checkUser['message'];
            return $return;
        }

        $data['username'] = $params['email'];
        $data['email'] = $params['email'];
        $data['real_name'] = $params['nama'];
        $data['id_group'] = 4;
        if($params['id_ref_status_alumni'] == 3 || $params['id_ref_status_alumni'] == 1)
        {
            $data['password'] = hash('sha256',$params['nip']);
        } else {
            $data['password'] = hash('sha256',$params['nim']);
        }
        

        $this->db->insert("user", $data);
        if ($this->db->affected_rows()) {
            $return['status'] = 201;
            $return['message'] = 'Data user berhasil dibuat';
            $return['data'] = $this->db->insert_id();
            return $return;
            exit();
        }else{
            $return['status'] = 500;
            $return['message'] = 'Data user gagal dibuat';
            return $return;
            exit();
        }
    }

    public function updateUser($params)
    {
        $current = $this->getAlumni($params->primaryKeyValue);
        $check['email']= $params->data['email'];
        $check['real_name'] = $params->data['nama'];
        if($current['email'] == $check['email']){
            $checkUser = userValidation($check);
            if($checkUser['status']==500)
            {
                $return['status'] = 201;
                $return['message'] = 'Data user berhasil dibuat';
                return $return;
                
            } else {
                $data['real_name'] = $params->data['nama'];
                $this->db->where('id_user', $current['id_user']);
                $this->db->update('user', $data);
                $return['status'] = 201;
                $return['message'] = 'Data user berhasil dibuat';
                return $return;
            }
      
        }
        else {
            $checkUser = userValidation($check);
            if ($checkUser['status']==500) {
                $return['status'] = 500;
                $return['message'] = $checkUser['message'];
                return $return;
            }
            else {
                $data['email'] = $params->data['email'];
                if($params->data['nama'] != $current['real_name'])
                {
                    $data['real_name'] = $params->data['nama'];
                }
                $this->db->where('id_user', $current['id_user']);
                $this->db->update('user', $data);
                $return['status'] = 201;
                $return['message'] = 'Data user berhasil dibuat';
                return $return;
            }
        }     
    }

    public function updateStatus($params, $id_alumni)
    {
        $data = [
            'kode_provinsi' => $params['kode_prov'],
            'kode_kab_kota' => $params['kode_kab_kota'],
            'kode_kecamatan' => $params['kode_kecamatan']
        ];

        if($params['id_ref_status_alumni'] == 3 || $params['id_ref_status_alumni'] == 1)
        {
            $this->db->where('id_alumni', $id_alumni);
            $response = $this->db->update('alumni_jobs', $data);
        }
        else {
            $this->db->where('id_alumni', $id_alumni);
            $response = $this->db->update('alumni_study', $data);
        }
        
        return $response;
    }

    public function hapusAlumni($params)
    {

        $current = $this->getAlumni($params);

        $this->db->trans_begin();

        if($current['id_ref_status_alumni'] == 3 || $current['id_ref_status_alumni'] == 1)
        {
            $this->db->where('id_alumni', $current['id_alumni']);
            $this->db->delete('alumni_jobs');
        }
        else {
            $this->db->where('id_alumni', $current['id_alumni']);
            $this->db->delete('alumni_study');
        }

        // hapus activity log
        $this->db->where('id_user', $current['id_user']);
        $activity = $this->db->get('activity_log')->result();
        if($activity){
            $this->db->where('id_user', $current['id_user']);
            $this->db->delete('activity_log');
        }

        // hapus alumni organisasi
        $this->db->where('id_alumni', $current['id_alumni']);
        $organisasi = $this->db->get('alumni_organisasi')->result();
        if($organisasi){
            $this->db->where('id_alumni', $current['id_alumni']);
            $this->db->delete('alumni_organisasi');
        }

        // hapus alumni pengalaman
        $this->db->where('id_alumni', $current['id_alumni']);
        $pengalaman = $this->db->get('alumni_pengalaman')->result();
        if($pengalaman){
            $this->db->where('id_alumni', $current['id_alumni']);
            $this->db->delete('alumni_pengalaman');
        }

        // hapus alumni prestasi
        $this->db->where('id_alumni', $current['id_alumni']);
        $prestasi = $this->db->get('alumni_prestasi')->result();
        if($prestasi){
            $this->db->where('id_alumni', $current['id_alumni']);
            $this->db->delete('alumni_prestasi');
        }

        // hapus alumni skill
        $this->db->where('id_alumni', $current['id_alumni']);
        $skill = $this->db->get('alumni_skill')->result();
        if($skill){
            $this->db->where('id_alumni', $current['id_alumni']);
            $this->db->delete('alumni_skill');
        }

        // hapus kuesioner alumni answer
        $this->db->where('id_alumni', $current['id_alumni']);
        $kuesioner = $this->db->get('kuesioner_alumni_answer')->result();
        if($kuesioner){
        foreach ($kuesioner as $key => $value) {
            $this->db->where('id_answer', $value->id_answer);
            $this->db->delete('kuesioner_alumni_offered_answer');
        }
            $this->db->where('id_alumni', $params);
            $this->db->delete('kuesioner_alumni_answer');
        }

        $this->db->where('id_alumni', $params);
        $this->db->delete('alumni');

        $this->db->where('id_user', $current['id_user']);
        $this->db->delete('user');
        
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $return['status'] = 500;
            $return['message'] = 'Data user tidak ditemukan';
            $this->db->trans_rollback();
        }
        else
        {
            $return['status'] = 201;
            $return['message'] = 'Data user berhasil dihapus';
            $this->db->trans_commit();
        }
        
        return $return;
    }

    public function deleteUser($params)
    {
        $current = $this->getAlumni($params);

        if($current == null) {
            $return['status'] = 500;
            $return['message'] = 'Data user tidak ditemukan';
        }
        else {
            $this->db->where('id_user', $current['id_user']);
            $this->db->delete('user');
            $return['status'] = 201;
            $return['message'] = 'Data user berhasil dihapus';
        }
        return $return;
        
    }

    public function deleteStudy($params)
    {
        $current = $this->getAlumni($params->primaryKeyValue);

        if($current == null) {
            $return['status'] = 500;
            $return['message'] = 'Data alumni study tidak ditemukan';
        }
        else {
            $this->db->where('id_alumni', $current['id_mhs']);
            $this->db->delete('alumni_study');
            $return['status'] = 201;
            $return['message'] = 'Data alumni study berhasil dihapus';
        }
        return $return;
        
    }

    public function verifAlumni($id_mhs, $data)
    {
        $this->db->where('id_alumni', $id_mhs);
        $response = $this->db->update('alumni', $data);

        if($response) {
            $return['success'] = true;
            $return['message'] = 'Data berhasil diubah';
        }
        else {
            $return['success'] = false;
            $return['message'] = 'Data Tidak berhasil diubah';
        }
        return $return;
    }

    public function getAlumni($id_alumni = null)
    {
        $this->db->select('a.*, p.nama_prodi, fk.nama_fakultas, ra.nama_agama, rp.nama_provinsi, rkb.nama_kab_kota, rk.nama_kecamatan, rsa.status_alumni');
        $this->db->from('alumni a');
        $this->db->join('ref_agama ra','a.id_agama=ra.id_agama','LEFT');
        $this->db->join('ref_prodi p', 'a.id_prodi = p.id_prodi', 'left');
        $this->db->join('ref_fakultas fk', 'a.id_fakultas = fk.id_fakultas', 'left');
        $this->db->join('ref_status_alumni rsa','a.id_ref_status_alumni=rsa.id','LEFT');
        $this->db->join('ref_provinsi rp','a.kode_prov=rp.kode_provinsi','LEFT');
        $this->db->join('ref_kab_kota rkb','a.kode_kab_kota=rkb.kode_kab_kota','LEFT');
        $this->db->join('ref_kecamatan rk','a.kode_kecamatan=rk.kode_kecamatan','LEFT');

        if ($id_alumni) {
            $this->db->where('id_alumni', $id_alumni);
            return $this->db->get()->row_array();
        }

        return $this->db->get()->result_array();
    }

    public function getAlumniByProdi($id_prodi)
    {
        $this->db->select('id_alumni, nama');
        $this->db->where('id_prodi', $id_prodi);
        $query = $this->db->get('alumni');
        return $query;
    }
    public function getJobsAlumni($id_alumni)
    {
        $this->db->select('aj.*, rp.nama_provinsi, rkk.nama_kab_kota');
        $this->db->from('alumni_jobs aj');
        $this->db->join('ref_provinsi rp','aj.kode_provinsi=rp.kode_provinsi','LEFT');
        $this->db->join('ref_kab_kota rkk','aj.kode_kab_kota=rkk.kode_kab_kota','LEFT');
        $this->db->where('id_alumni', $id_alumni);
        $this->db->order_by('tgl_mulai','DESC');
        $get = $this->db->get()->result_array();
        return $get;
    }

    public function getStudyAlumni($id_alumni)
    {
        $this->db->select('as.*, rp.nama_provinsi, rkk.nama_kab_kota');
        $this->db->from('alumni_study as');
        $this->db->join('ref_provinsi rp','as.kode_provinsi=rp.kode_provinsi','LEFT');
        $this->db->join('ref_kab_kota rkk','as.kode_kab_kota=rkk.kode_kab_kota','LEFT');
        $this->db->where('id_alumni', $id_alumni);
        $this->db->order_by('tgl_mulai','DESC');
        $get = $this->db->get()->result_array();
        return $get;
    }

    public function getNowJobs($id_alumni)
    {
        $this->db->select('*');
        $this->db->from('alumni_jobs');
        $this->db->where('id_alumni', $id_alumni);
        $this->db->order_by('tgl_mulai', 'DESC');
        $get = $this->db->get()->row_array();
        return $get;
    }

    public function getNowStudy($id_alumni)
    {
        $this->db->select('*');
        $this->db->from('alumni_study');
        $this->db->where('id_alumni', $id_alumni);
        $this->db->order_by('tgl_mulai', 'DESC');
        $get = $this->db->get()->row_array();
        return $get;
    }

    public function getPrestasiAlumni($id_alumni)
    {
        $this->db->select('*');
        $this->db->from('alumni_prestasi');
        $this->db->where('id_alumni', $id_alumni);
        $this->db->order_by('tahun','DESC');
        $get = $this->db->get()->result_array();
        return $get;
    }

    public function getOrganisasiAlumni($id_alumni)
    {
        $this->db->select('*');
        $this->db->from('alumni_organisasi');
        $this->db->where('id_alumni', $id_alumni);
        $this->db->order_by('start_date','DESC');
        $get = $this->db->get()->result_array();
        return $get;
    }

    public function getPengalamanAlumni($id_alumni)
    {
        $this->db->select('*');
        $this->db->from('alumni_pengalaman');
        $this->db->where('id_alumni', $id_alumni);
        $this->db->order_by('start_date','DESC');
        $get = $this->db->get()->result_array();
        return $get;
    }

    public function getSkillAlumni($id_alumni)
    {
        $this->db->select('*');
        $this->db->from('alumni_skill');
        $this->db->where('id_alumni', $id_alumni);
        $this->db->order_by('created_time','DESC');
        $get = $this->db->get()->result_array();
        return $get;
    }

    public function getListProdi()
    {
        $this->db->order_by('nama_prodi', 'ASC');

        $results = $this->db->get('ref_prodi')->result_array();
        $return = array();

        foreach ($results as $key => $value) {
        $return[$value['kode_prodi']] = $value["nama_prodi"];
        }

        return $return;
    }

    public function getSubAlumni($id)
    {
        $this->db->select('a.*, p.nama_prodi, pt.nama_resmi');
        $this->db->from('alumni a');
        $this->db->where('a.id_alumni', $id);
        $this->db->join('ref_prodi p', 'a.id_prodi = p.id_prodi', 'left');
        $this->db->join('ref_perguruan_tinggi pt', 'a.id_perguruan_tinggi = pt.id_perguruan_tinggi', 'left');
        $get = $this->db->get()->row_array();
        return $get;
    }

    public function checkAlumni($where, $id_alumni = null)
    {
        $this->db->from('alumni');
        $this->db->where($where);

        if ($id_alumni) {
            $this->db->where('id_alumni !=', $id_alumni);
        }
        
        if ($this->db->get()->num_rows() > 0) {
            return false;
        }

        return true;
    }

    public function checkUser($where, $id_user = null)
    {
        $this->db->from('user');
        $this->db->where($where);

        if ($id_user) {
            $this->db->where('id_user !=', $id_user);
        }
        
        if ($this->db->get()->num_rows() > 0) {
            return false;
        }

        return true;
    }

    public function checkAgama($where)
    {
        $this->db->from('ref_agama');
        $this->db->where($where);
        
        if ($this->db->get()->num_rows() == 1) {
            return true;
        }

        return false;
    }

    public function checkProvinsi($where)
    {
        $this->db->from('ref_provinsi');
        $this->db->where($where);
        
        if ($this->db->get()->num_rows() == 1) {
            return true;
        }

        return false;
    }

    public function checkKabupaten($where)
    {
        $this->db->from('ref_kab_kota');
        $this->db->where($where);
        
        if ($this->db->get()->num_rows() == 1) {
            return true;
        }

        return false;
    }

    public function checkKecamatan($where)
    {
        $this->db->from('ref_kecamatan');
        $this->db->where($where);
        
        if ($this->db->get()->num_rows() == 1) {
            return true;
        }

        return false;
    }

    public function checkTahun($where)
    {
        $this->db->from('ref_tahun');
        $this->db->where($where);
        
        if ($this->db->get()->num_rows() == 1) {
            return true;
        }

        return false;
    }

    public function checkPerguruanTinggi($where)
    {
        $this->db->from('ref_perguruan_tinggi');
        $this->db->where($where);
        
        if ($this->db->get()->num_rows() == 1) {
            return true;
        }

        return false;
    }

    public function checkProdi($where)
    {
        $this->db->from('ref_prodi');
        $this->db->where($where);
        
        if ($this->db->get()->num_rows() == 1) {
            return true;
        }

        return false;
    }

    public function checkStatus($where)
    {
        $this->db->from('ref_status_alumni');
        $this->db->where($where);
        
        if ($this->db->get()->num_rows() == 1) {
            return true;
        }

        return false;
    }
}
