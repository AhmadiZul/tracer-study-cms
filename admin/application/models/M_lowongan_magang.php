<?php

class M_lowongan_magang extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getLowongan($id_lowongan)
    {
        $this->db->where('id_lowongan_magang',$id_lowongan);

        return $this->db->get('lowongan_magang')->row();
    }

    public function getMitra($id_instansi)
    {
        $this->db->where('id_instansi',$id_instansi);
        return $this->db->get('mitra')->row();
    }

    public function simpan_lowongan($data)
    {
        $this->db->insert('lowongan_magang', $data);
        return $this->db->affected_rows();
    }

    public function ubah_lowongan($data, $id)
    {
        $this->db->update('lowongan_magang', $data, ['id_lowongan_magang' => $id]);
        return $this->db->affected_rows();
    }

}
