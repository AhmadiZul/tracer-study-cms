<?php

class M_legalisir extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function dataTable($params) {

        $return = array('total' => 0, 'rows' => array());



        $this->db->start_cache();
        if (isset($params['sSearch']) && $params['sSearch'] != '') {
            $search = $this->db->escape_str($params['sSearch']);
            $this->db->where("(a.nama LIKE '%{$search}%' OR a.nim LIKE '%{$search}%')");
        }

        if ($this->session->userdata("tracer_idGroup") == 2) {
            $this->db->where('rpt.id_user',$this->session->userdata("tracer_userId"));
        }

        $this->db->join('alumni a', 'l.id_alumni = a.id_alumni', 'LEFT');
        $this->db->join('ref_perguruan_tinggi rpt', 'a.id_perguruan_tinggi = rpt.id_perguruan_tinggi', 'LEFT');
        $this->db->join('ref_prodi rp', 'a.id_prodi = rp.id_prodi', 'LEFT');
        $this->db->stop_cache();

        $rs = $this->db->count_all_results('legalisir l');
        $return['total'] = $rs;
        if ($return['total'] > 0) {
            $this->db->select('l.id_legalisir, l.id_alumni, a.nim, a.nama, rp.nama_prodi, l.jenis, l.jumlah, l.status, l.berkas');
            $this->db->limit($params['limit'], $params['start']);
            $this->db->order_by('l.id_legalisir', 'ASC');
            $rs = $this->db->get('legalisir l');
            if ($rs->num_rows())
                $return['rows'] = $rs->result_array();
        }

        $this->db->flush_cache();

        return $return;
    }

    public function getLegalisir($where)
    {
        $this->db->from('legalisir');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function getIdPT($id_alumni)
    {
        $this->db->from('alumni');
        $this->db->where('id_alumni',$id_alumni);
        return $this->db->get()->row();
    }

    public function simpan_legalisir($data)
    {
        $this->db->insert('legalisir', $data);
        return $this->db->insert_id();
    }

    public function ubah_legalisir($data,$where)
    {
        $this->db->update('legalisir',$data,$where);
        return $this->db->affected_rows();
    }

    public function hapus_legalisir($id)
    {
        $this->db->where('id_legalisir', $id);
        $this->db->delete('legalisir');
        return $this->db->affected_rows();
    }

}