<?php

class M_layanan_keuangan extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function checkMitra($where, $id = null)
    {
        $this->db->from('mitra');
        $this->db->where($where);

        if ($id) {
            $this->db->where('id_instansi !=', $id);
        }

        if ($this->db->get()->num_rows() > 0) {
            return false;
        }

        return true;
    }

    public function checkUser($where, $id = null)
    {
        $this->db->from('user');
        $this->db->where($where);

        if ($id) {
            $this->db->where('id_user !=', $id);
        }

        if ($this->db->get()->num_rows() > 0) {
            return false;
        }

        return true;
    }

    public function getLayanan($id_layanan)
    {
        $this->db->select('*');
        $this->db->from('layanan_keuangan');
        $this->db->where('id_layanan', $id_layanan);
        $get = $this->db->get()->row();
        return $get;
    }

    public function simpan_layanan($data)
    {
        $this->db->insert('layanan_keuangan', $data);
        return $this->db->affected_rows();
    }

    public function edit_layanan($data, $where)
    {
        $this->db->update('layanan_keuangan', $data, $where);
        return $this->db->affected_rows();
    }

    public function verifMitra($id_instansi, $data)
    {
        $this->db->where('id_instansi', $id_instansi);
        $response = $this->db->update('mitra', $data);

        if ($response) {
            $return['success'] = true;
            $return['message'] = 'Data berhasil diubah';
        } else {
            $return['success'] = false;
            $return['message'] = 'Data Tidak berhasil diubah';
        }
        return $return;
    }

    public function getListProdi()
    {
        $this->db->order_by('nama_prodi', 'ASC');

        $results = $this->db->get('ref_prodi')->result_array();
        $return = array();

        foreach ($results as $key => $value) {
            $return[$value['id_prodi']] = $value["nama_prodi"];
        }

        return $return;
    }

    public function getListUniv()
    {
        $this->db->order_by('nama_resmi', 'ASC');

        $results = $this->db->get('ref_perguruan_tinggi')->result_array();
        $return = array();

        foreach ($results as $key => $value) {
            $return[$value['id_perguruan_tinggi']] = $value["nama_resmi"];
        }

        return $return;
    }
}
