<?php

class M_prestasi extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * getAlumniBerprestasi
     * memperoleh data alumni berprestasi
     * @param  mixed $where
     * @return void
     */
    public function getAlumniBerprestasi($where)
    {
        $this->db->from('alumni_berprestasi');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }
    
    /**
     * simpan_alumni_berprestasi
     * menambahkan alumni berprestasi ke database
     * @param  mixed $data
     * @return void
     */
    public function simpan_alumni_berprestasi($data)
    {
        $this->db->insert('alumni_berprestasi', $data);
        return $this->db->affected_rows();
    }
    
    /**
     * publishAlumni
     * mengubah status kolom is_publish database
     * @param  mixed $id
     * @param  mixed $data
     * @return void
     */
    public function publishAlumni($id, $data)
    {
        $this->db->where('id', $id);
        $response = $this->db->update('alumni_berprestasi', $data);

        if($response) {
            $return['success'] = true;
            $return['message'] = 'Berhasil Dipublish';
        }
        else {
            $return['success'] = false;
            $return['message'] = 'Tidak Berhasil Dipublish';
        }
        return $return;
    }
    
    /**
     * getAlumniByProdi
     * memperoleh data alumni berdasarkan prodi
     * @param  mixed $id
     * @return void
     */
    function getAlumniByProdi($id)
    {
  
      $get = $this->db->query("SELECT * FROM alumni where id_prodi=? order by nama ", array($id));
  
      if ($get->num_rows() != 0) {
        return $get->result_array();
      } else {
        return null;
      }
    }
    
    /**
     * editPrestasi
     * mengedit data alumni berprestasi pada database
     * @param  mixed $data
     * @param  mixed $id
     * @return void
     */
    public function editPrestasi($data, $id)
    {
        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update('alumni_berprestasi');
        return $this->db->affected_rows();
    }
}