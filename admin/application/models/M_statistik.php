<?php

class M_statistik extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function statistik($params)
    {
        $return['status'] = 201;
        $return['data']   = [];

        $alumni['total'] = $this->countTotalAlumni($params);
        $alumni['alumni_submit'] = $this->allAlumni($params, 'filled');
        $alumni['persentase'] = ($alumni['total'] != 0 ? round(($alumni['alumni_submit'] / $alumni['total']) * 100, 2) : 0);

        $bekerja = ['1', '3', '4'];
        /* $bekerja = [
            '6050e5f0-fb1e-11eb-817d-0a002700000b',
            '605e2ef0-fb1e-11eb-817d-0a002700000b',
            '605e6cf9-fb1e-11eb-817d-0a002700000b'
        ]; */

        $submited['bekerja'] = $this->alumniStatusWith($params, $bekerja, 'in');
        $submited['iddle'] = $this->alumniStatusWith($params, $bekerja, 'not_in');
        $submited['persentase'] = ($submited['bekerja'] != 0 && $alumni['alumni_submit'] != 0 ? round(($submited['bekerja'] / $alumni['alumni_submit']) * 100, 2) : 0);

        $data['alumni'] = $alumni;
        $data['bekerja'] = $submited;

        $return['data'] = $data;

        return $return;
    }

    public function allAlumni($params, $status = null)
    {
        $this->db->select('*');
        $this->db->from('alumni');
        if ($params['ref_tahun'] != 'all') {
            $this->db->where('tahun_lulus', $params['ref_tahun']);
        }
        if ($status != null) {
            $this->db->where('id_ref_status_alumni is not null');
        }

        if(!empty($params['fakultas']) || $params['fakultas'] != ''){
            $this->db->where('id_fakultas', $params['fakultas']);
        }
        if (!empty($params['prodi']) || $params['prodi'] != '') {
            $this->db->where('id_prodi', $params['prodi']);
        }
        $this->db->where('is_active', '1');

        $get = $this->db->get();

        if ($get->num_rows() != 0) {
            return $get->num_rows();
        } else {
            return 0;
        }
    }

    public function alumniSubmited($params)
    {
        $this->db->select('ka.id_answer');
        $this->db->from('kuesioner_alumni_answer ka');
        $this->db->join('alumni a', 'a.id_alumni = ka.id_alumni', 'left');
        if ($params['ref_tahun'] != 'all') {
            $this->db->where('a.tahun_lulus', $params['ref_tahun']);
        }
        if (!empty($params['asal_alumni'])) {
            $this->db->where('a.id_perguruan_tinggi', $params['asal_alumni']);
        }
        $this->db->where('a.is_active', '1');

        $get = $this->db->get();

        if ($get->num_rows() != 0) {
            return $get->num_rows();
        } else {
            return 0;
        }
    }

    public function alumniSubmitWith($ref, $params, $condition = 'in')
    {
        $this->db->select('ka.id_answer');
        $this->db->from('kuesioner_alumni_answer ka');
        $this->db->join('alumni a', 'a.id_alumni = ka.id_alumni', 'left');
        if ($ref['ref_tahun'] != 'all') {
            $this->db->where('a.tahun_lulus', $ref['ref_tahun']);
        }
        if (!empty($ref['asal_alumni'])) {
            $this->db->where('a.id_perguruan_tinggi', $ref['asal_alumni']);
        }
        if (!empty($params)) {
            if ($condition == 'in') {
                $this->db->where_in('ka.id_question_category', $params);
            } else {
                $this->db->where_not_in('ka.id_question_category', $params);
            }
        }
        $this->db->where('a.is_active', '1');

        $get = $this->db->get();

        if ($get->num_rows() != 0) {
            return $get->num_rows();
        } else {
            return 0;
        }
    }
    public function alumniStatusWith($tahun, $params, $condition = 'in')
    {
        $this->db->select('*');
        $this->db->from('alumni');
        if ($tahun['ref_tahun'] != 'all') {
            $this->db->where('tahun_lulus', $tahun['ref_tahun']);
        }
        if ($tahun['prodi'] != '' || (!empty($tahun['prodi']))) {
            $this->db->where('id_prodi', $tahun['prodi']);
        }
        if ($tahun['fakultas'] != '' || (!empty($tahun['fakultas']))) {
            $this->db->where('id_fakultas', $tahun['fakultas']);
        }
        if (!empty($params)) {
            if ($condition == 'in') {
                $this->db->where_in('id_ref_status_alumni', $params);
            } else {
                $this->db->where_not_in('id_ref_status_alumni', $params);
            }
        }
        $this->db->where('is_active', '1');

        $get = $this->db->get();

        if ($get->num_rows() != 0) {
            return $get->num_rows();
        } else {
            return 0;
        }
    }

    public function countTotalAlumni($params)
    {
        $this->db->select('SUM(total_alumni) as total');
        $this->db->from('alumni_total_per_prodi');
        if ($params['ref_tahun'] != 'all') {
            $this->db->where('ref_tahun', $params['ref_tahun']);
        }
        if (!empty($params['fakultas']) || $params['fakultas'] != '') {
            $this->db->where('id_fakultas', $params['fakultas']);
        }
        if (!empty($params['prodi']) || $params['prodi'] != '') {
            $this->db->where('id_prodi', $params['prodi']);
        }

        $get = $this->db->get();

        if ($get->num_rows() != 0) {
            $rows = $get->row_array();
            if ($rows['total'] != null) {
                return $rows['total'];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function countLaki($params)
    {
        $this->db->select('nama');
        $this->db->from('alumni');
        if ($params['ref_tahun'] != 'all') {
            $this->db->where('tahun_lulus', $params['ref_tahun']);
        }
        if (!empty($params['fakultas']) || $params['fakultas'] != '') {
            $this->db->where('id_fakultas', $params['fakultas']);
        }
        if (!empty($params['prodi']) || $params['prodi'] != '') {
            $this->db->where('id_prodi', $params['prodi']);
        }
        $this->db->where('jenis_kelamin', 'L');
        $get = $this->db->get();
        if ($get->num_rows() != 0) {
            return $get->num_rows();
        } else {
            return 0;
        }
    }

    public function countPerempuan($params)
    {
        $this->db->select('nama');
        $this->db->from('alumni');
        if ($params['ref_tahun'] != 'all') {
            $this->db->where('tahun_lulus', $params['ref_tahun']);
        }
        if (!empty($params['fakultas']) || $params['fakultas'] != '') {
            $this->db->where('id_fakultas', $params['fakultas']);
        }
        if (!empty($params['prodi']) || $params['prodi'] != '') {
            $this->db->where('id_prodi', $params['prodi']);
        }
        $this->db->where('jenis_kelamin', 'P');
        $get = $this->db->get();

        if ($get->num_rows() != 0) {
            return $get->num_rows();
        } else {
            return 0;
        }
    }

    public function countLakiStatus($params)
    {
        if ($params['tahun'] != 'all') {
            $this->db->where('tahun_lulus', $params['tahun']);
        }
        if (!empty($params['fakultas']) || $params['fakultas'] != '') {
            $this->db->where('id_fakultas', $params['fakultas']);
        }
        if (!empty($params['prodi']) || $params['prodi'] != '') {
            $this->db->where('id_prodi', $params['prodi']);
        }
        $this->db->where('jenis_kelamin', 'L');

        $this->db->select('id_ref_status_alumni');
        $get_status = $this->db->get('alumni')->result();

        $status = [];
        $bekerja = 0;
        $belumMungkin = 0;
        $wiraswasta = 0;
        $lanjutPendidikan = 0;
        $cariKerja = 0;
        foreach ($get_status as $key => $value) {
            switch ($value->id_ref_status_alumni) {
                case 1:
                    $bekerja++;
                    break;
                case 2:
                    $belumMungkin++;
                    break;
                case 3:
                    $wiraswasta++;
                    break;
                case 4:
                    $lanjutPendidikan++;
                    break;
                case 5:
                    $cariKerja++;
                    break;
            }
        }

        $status[] = array(
            'bekerja' => $bekerja,
            'belumMungkin' => $belumMungkin,
            'wiraswasta' => $wiraswasta,
            'lanjutPendidikan' => $lanjutPendidikan,
            'cariKerja' => $cariKerja,
        );
        return $status;
    }

    public function countPerempuanStatus($params)
    {
        if ($params['tahun'] != 'all') {
            $this->db->where('tahun_lulus', $params['tahun']);
        }
        if (!empty($params['fakultas']) || $params['fakultas'] != '') {
            $this->db->where('id_fakultas', $params['fakultas']);
        }
        if (!empty($params['prodi']) || $params['prodi'] != '') {
            $this->db->where('id_prodi', $params['prodi']);
        }
        $this->db->where('jenis_kelamin', 'P');
        $this->db->select('id_ref_status_alumni');
        $get_status = $this->db->get('alumni')->result();

        $status = [];
        $bekerja = 0;
        $belumMungkin = 0;
        $wiraswasta = 0;
        $lanjutPendidikan = 0;
        $cariKerja = 0;
        foreach ($get_status as $key => $value) {
            switch ($value->id_ref_status_alumni) {
                case 1:
                    $bekerja++;
                    break;
                case 2:
                    $belumMungkin++;
                    break;
                case 3:
                    $wiraswasta++;
                    break;
                case 4:
                    $lanjutPendidikan++;
                    break;
                case 5:
                    $cariKerja++;
                    break;
            }
        }

        $status[] = array(
            'bekerja' => $bekerja,
            'belumMungkin' => $belumMungkin,
            'wiraswasta' => $wiraswasta,
            'lanjutPendidikan' => $lanjutPendidikan,
            'cariKerja' => $cariKerja,
        );
        return $status;
    }
}
