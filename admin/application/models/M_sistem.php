<?php

class M_sistem extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * logo_utama
     *
     * @return void
     */
    public function logo_utama()
    {
        $this->db->select('id, key, url_file');
        $this->db->where('key', 'logo_utama');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * logo_title
     *
     * @return void
     */
    public function logo_title()
    {
        $this->db->select('id, key, url_file');
        $this->db->where('key', 'logo_title');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * background_1
     *
     * @return void
     */
    public function background_1()
    {
        $this->db->select('id, key, url_file');
        $this->db->where('key', 'background_1');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * background_2
     *
     * @return void
     */
    public function background_2()
    {
        $this->db->select('id, key, url_file');
        $this->db->where('key', 'background_2');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * background_3
     *
     * @return void
     */
    public function background_3()
    {
        $this->db->select('id, key, url_file');
        $this->db->where('key', 'background_3');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * banner
     *
     * @return void
     */
    public function banner()
    {
        $this->db->select('id, key, title, sub_title, deskripsi, warna_1, warna_2');
        $this->db->where('key', 'banner');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * about_us
     *
     * @return void
     */
    public function about_us()
    {
        $this->db->select('key, deskripsi');
        $this->db->where('key', 'about_us');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * copyright
     *
     * @return void
     */
    public function copyright()
    {
        $this->db->select('key, sub_title');
        $this->db->where('key', 'copyright');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * footer
     *
     * @return void
     */
    public function footer()
    {
        $this->db->select('key, sub_title, deskripsi, warna_1, warna_2');
        $this->db->where('key', 'footer');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }
    
    /**
     * header
     *
     * @return void
     */
    public function header()
    {
        $this->db->select('key, warna_1, warna_2');
        $this->db->where('key', 'header');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * video_perkenalan
     *
     * @return void
     */
    public function video_perkenalan()
    {
        $this->db->select('id, key, url_link, title, deskripsi, url_file');
        $this->db->where('key', 'video_perkenalan');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }
    
    /**
     * rektor
     *
     * @return void
     */
    public function rektor()
    {
        $this->db->select('id, key, title, deskripsi, url_file');
        $this->db->where('key', 'rektor');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * official_account
     *
     * @return void
     */
    public function official_account()
    {
        $this->db->select('key, deskripsi');
        $this->db->where('key', 'official_account');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * tema
     *
     * @return void
     */
    public function tema()
    {
        $this->db->select('key, deskripsi');
        $this->db->where('key', 'tema');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }    
    /**
     * warna_tema
     *
     * @return void
     */
    public function warna_tema()
    {
        $this->db->select('key, deskripsi');
        $this->db->where('key', 'warna_tema');
        $query=$this->db->get('sistem_setting');

        return $query->row();
    }


    public function add_agenda($data)
    {
        $this->db->insert('agenda', $data);
        return $this->db->affected_rows();
    }

    public function getSistem($where)
    {
        $this->db->from('sistem_setting');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function editsistem($data,$where)
    {
        $this->db->update('sistem_setting',$data,$where);
        return $this->db->affected_rows();
    }

    public function update_sosmed($data, $where)
    {
        $this->db->update('sistem_setting',$data, $where);
        return $this->db->affected_rows();
    }
}