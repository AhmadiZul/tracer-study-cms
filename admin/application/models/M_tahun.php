<?php

class M_tahun extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    /**
     * aktifTahun
     * fungsi update status is_active tahun
     * @param  mixed $id_tahun
     * @param  mixed $data
     * @return void
     */
    public function aktifTahun($id_tahun, $data)
    {
        $this->db->where('tahun', $id_tahun);
        $response = $this->db->update('ref_tahun', $data);

        if ($response) {
            $return['success'] = true;
            $return['message'] = 'Berhasil Diaktivasi';
        } else {
            $return['success'] = false;
            $return['message'] = 'Tidak Berhasil Diaktivasi';
        }
        return $return;
    }
    
    /**
     * checkTahun
     * validasi input tahun ketika tambah data
     * @param  mixed $data
     * @return void
     */
    public function checkTahun($data)
    {
        $tahun = $data["tahun"];

        if (empty($tahun)) {
            return [
                "status"  => 500,
                "message" => "Tahun Wajib Diisi"
            ];
        }

        $str_tahun = (string)$tahun;
        if (strlen($str_tahun) != 4) {
            return [
                "status"  => 500,
                "message" => "Tahun harus diisi dengan 4 digit"
            ];
        }

        $this->db->where('tahun', $tahun);

        $get = $this->db->get("ref_tahun");

        if ($get->num_rows() > 0) {
            return [
                "status"  => 500,
                "message" => "Tahun sudah Ada."
            ];
        } else {
            return [
                "status"  => 201,
                "message" => "Berhasil Ditambahkan"
            ];
        }
    }
    
    /**
     * checkTahunEdit
     * validasi input tahun ketika edit data
     * @param  mixed $data
     * @return void
     */
    public function checkTahunEdit($data)
    {
        $tahun = $data["tahun"];

        if (empty($tahun)) {
            return [
                "status"  => 500,
                "message" => "Tahun Wajib Diisi"
            ];
        }

        $str_tahun = (string)$tahun;
        if (strlen($str_tahun) != 4) {
            return [
                "status"  => 500,
                "message" => "Tahun harus diisi dengan 4 digit"
            ];
        }

        $this->db->where('tahun', $tahun);

        $get = $this->db->get("ref_tahun");

        if ($get->num_rows() > 0) {
            return [
                "status"  => 500,
                "message" => "Tahun sudah Ada."
            ];
        } else {
            return [
                "status"  => 201,
                "message" => "Berhasil Diupdate"
            ];
        }
    }
    
    /**
     * deleteTahun
     * fungsi hapus data di database
     * @param  mixed $tahun
     * @return void
     */
    public function deleteTahun($tahun)
    {
        $this->db->trans_begin();
        $this->db->where('tahun', $tahun);
        $this->db->delete('ref_tahun');

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $return['status'] = 500;
            $return['message'] = 'Data Tahun tidak ditemukan';
            $this->db->trans_rollback();
        } else {
            $return['status'] = 201;
            $return['message'] = 'Data Tahun berhasil dihapus';
            $this->db->trans_commit();
        }

        return $return;
    }
    
    /**
     * checkAlumni
     * fungsi mengecek apakah tahun digunakan di data alumni
     * @param  mixed $tahun
     * @return void
     */
    public function checkAlumni($tahun)
    {
        $this->db->where('tahun_lulus', $tahun);
        $alumni = $this->db->get('alumni')->result();

        if ($alumni) {
            return [
                "status"  => 500,
                "message" => "Tahun telah digunakan untuk data alumni!"
            ];
        }
        return true;
    }
    
    /**
     * checkSurvey
     * fungsi apakah tahun digunakan di data survey
     * @param  mixed $tahun
     * @return void
     */
    public function checkSurvey($tahun)
    {
        $this->db->where('untuk_tahun_lulus', $tahun);
        $survey = $this->db->get('ref_jadwal_survey')->result();

        if ($survey) {
            return [
                "status"  => 500,
                "message" => "Tahun telah digunakan untuk data survey!"
            ];
        }
        return true;
    }
}
