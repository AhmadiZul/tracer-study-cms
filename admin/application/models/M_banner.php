<?php

class M_banner extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function tambah_banner($data)
    {
        $this->db->insert('banner', $data);
        return $this->db->affected_rows();
    }

    public function getBanner($where)
    {
        $this->db->from('banner');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }

    public function editBanner($data, $id_banner)
    {
        $this->db->set($data);
        $this->db->where('id_banner', $id_banner);
        $this->db->update('banner');
        return $this->db->affected_rows();
    }

    public function publishBanner($id_banner, $data)
    {
        $this->db->where('id_banner', $id_banner);
        $response = $this->db->update('banner', $data);

        if($response) {
            $return['success'] = true;
            $return['message'] = 'Berhasil Dipublish';
        }
        else {
            $return['success'] = false;
            $return['message'] = 'Tidak Berhasil Dipublish';
        }
        return $return;
    }
}