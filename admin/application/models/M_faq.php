<?php 

class M_faq extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    public function getFaq($where)
    {
        $this->db->from('faq');
        
        if ($where) {
            $this->db->where($where);
            return $this->db->get()->row();
        }

        return $this->db->get()->result();
    }
}
?>