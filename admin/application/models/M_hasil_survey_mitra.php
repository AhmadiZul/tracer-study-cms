<?php

class M_hasil_survey_mitra extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function get_question()
    {
        $this->db->select('kmq.id_kuesioner, kmq.question, kmq.question_type, kmq.urutan, kmq.others');
        $this->db->from('kuesioner_mitra_question kmq');
        $this->db->where('kmq.is_active',"1");
        $this->db->where('kmq.status',"1");
        $this->db->order_by('kmq.urutan');
        return $this->db->get()->result();
    }

    public function get_option_by_question($id_kuesioner)
    {
        $this->db->select('kmqo.id_opsi, kmqo.opsi, kmqo.isian');
        $this->db->from('kuesioner_mitra_question_option kmqo');
        $this->db->where('kmqo.is_active','1');
        $this->db->where('kmqo.id_kuesioner',$id_kuesioner);
        return $this->db->get()->result();
    }

    public function get_likert_by_question($id_kuesioner)
    {
        $this->db->select('kmql.id_likert, kmql.pertanyaan');
        $this->db->from('kuesioner_mitra_question_likert kmql');
        $this->db->where('kmql.is_active','1');
        $this->db->where('kmql.id_kuesioner',$id_kuesioner);
        return $this->db->get()->result();
    }

    public function get_sub_by_question($id_kuesioner)
    {
        $this->db->select('kmqs.id_sub, kmqs.pertanyaan');
        $this->db->from('kuesioner_mitra_question_sub kmqs');
        $this->db->where('kmqs.is_active','1');
        $this->db->where('kmqs.id_kuesioner',$id_kuesioner);
        return $this->db->get()->result();
    }

    public function get_jawaban_isian($id_kuesioner, $id_survey)
    {
        $this->db->distinct();
        $this->db->select('kmoa.text');
        $this->db->from('kuesioner_mitra_offered_answer kmoa');
        $this->db->join('kuesioner_mitra_answer kma','kmoa.id_answer=kma.id_answer','LEFT');
        $this->db->where('kmoa.id_kuesioner',$id_kuesioner);
        $this->db->where('kma.id_survey',$id_survey);
        return $this->db->get()->result();
    }

    public function get_jawaban_option($id_kuesioner, $id_survey, $id_opsi, $is_count)
    {
        $this->db->select('kmoa.text');
        $this->db->from('kuesioner_mitra_offered_answer kmoa');
        $this->db->join('kuesioner_mitra_answer kma','kmoa.id_answer=kma.id_answer','LEFT');
        $this->db->where('kmoa.id_kuesioner',$id_kuesioner);
        $this->db->where('kma.id_survey',$id_survey);
        $this->db->like('kmoa.text',$id_opsi);

        if ($is_count) {
            return $this->db->count_all_results();
        }
        return $this->db->get()->result();
    }

    public function get_jawaban_likert($id_kuesioner, $id_survey)
    {
        $this->db->select('kmoa.text');
        $this->db->from('kuesioner_mitra_offered_answer kmoa');
        $this->db->join('kuesioner_mitra_answer kma','kmoa.id_answer=kma.id_answer','LEFT');
        $this->db->where('kmoa.id_kuesioner',$id_kuesioner);
        $this->db->where('kma.id_survey',$id_survey);
        return $this->db->get()->result();
    }

    public function get_jawaban_sub($id_kuesioner, $id_survey)
    {
        $this->db->select('kmoa.text');
        $this->db->from('kuesioner_mitra_offered_answer kmoa');
        $this->db->join('kuesioner_mitra_answer kma','kmoa.id_answer=kma.id_answer','LEFT');
        $this->db->where('kmoa.id_kuesioner',$id_kuesioner);
        $this->db->where('kma.id_survey',$id_survey);
        return $this->db->get()->result();
    }

    public function get_provinsi_by_id($id)
    {
        $this->db->select('rp.nama_provinsi');
        $this->db->from('ref_provinsi rp');
        $this->db->where('rp.kode_provinsi',$id);
        return $this->db->get()->row();
    }

    public function get_kab_kota_by_id($id)
    {
        $this->db->distinct();
        $this->db->select('rkk.nama_kab_kota');
        $this->db->from('ref_kab_kota rkk');
        $this->db->where('rkk.kode_kab_kota',$id);
        return $this->db->get()->row();
    }
}
