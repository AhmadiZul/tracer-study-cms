<?php

class M_index extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function getCountAlumniBy($where, $count = true)
    {
        // if ($this->session->userdata("tracer_idGroup") == 2) {
        //     $this->db->join('ref_perguruan_tinggi rpt','a.id_perguruan_tinggi=rpt.id_perguruan_tinggi','LEFT');
        //     $this->db->where('rpt.id_user',$this->session->userdata("tracer_userId"));
        // }

        $this->db->where('a.tahun_lulus',$this->session->userdata('tracer_tahun'));

        if ($where) {
            $this->db->where($where);
        }

        if ($count) {
            return $this->db->count_all_results('alumni a');
        }

        $this->db->select('a.id_alumni');
        return $this->db->get('alumni a')->result();
    }
    public function getCountAlumniByStatus($where, $count = true)
    {
        // if ($this->session->userdata("tracer_idGroup") == 2) {
        //     $this->db->join('ref_perguruan_tinggi rpt','a.id_perguruan_tinggi=rpt.id_perguruan_tinggi','LEFT');
        //     $this->db->where('rpt.id_user',$this->session->userdata("tracer_userId"));
        // }
        

        $this->db->where('a.tahun_lulus',$this->session->userdata('tracer_tahun'));

        if ($where) {
            $this->db->where($where);
        }

        if ($count) {
            return $this->db->count_all_results('alumni a');
        }

        $this->db->select('a.id_alumni');
        return $this->db->get('alumni a')->result();
    }

    public function getStatusALumniAll()
    {
        return $this->db->get('ref_status_alumni')->result();
    }

    public function getAlumniJobs($id_alumni)
    {
        $this->db->select('tgl_mulai, sektor');
        $this->db->where('id_alumni',$id_alumni);
        $this->db->order_by('tgl_mulai','DESC');
        return $this->db->get('alumni_jobs')->row();
    }

    public function getAlumniStudy($id_alumni)
    {
        $this->db->select('tgl_mulai, sektor_prodi as sektor');
        $this->db->where('id_alumni',$id_alumni);
        $this->db->order_by('tgl_mulai','DESC');
        return $this->db->get('alumni_study')->row();
    }

    public function getPolbangtanAll()
    {
        return $this->db->get('ref_perguruan_tinggi')->result();
    }

    public function getRekapPT($params)
    {
        $return['status']  = 0;
        $return['message'] = '';

        $this->db->select('*');
        $this->db->from('alumni_total_per_prodi');
        if (!empty($params)) {
            $this->db->where($params);
        }

        $get = $this->db->get();

        if ($get->num_rows() != 0) {
            $return['status']  = 500;
            $return['message'] = 'Data sudah ada. Silahkan cek kembali data anda';
        } else {
            $return['status']  = 201;
            $return['message'] = 'Data bisa dientri';
        }
        return $return;
    }

    public function countGenderByStatus($where)
    {
        $this->db->where('a.tahun_lulus',$this->session->userdata('tracer_tahun'));
        if ($where) {
            $this->db->where($where);
        }

        $this->db->select('id_ref_status_alumni');
        $get_status = $this->db->get('alumni a')->result();

        $status = [];
        $bekerja = 0;
        $belumMungkin = 0;
        $wiraswasta = 0;
        $lanjutPendidikan = 0;
        $cariKerja = 0;
        foreach ($get_status as $key => $value) {
            switch ($value->id_ref_status_alumni) {
                case 1:
                    $bekerja++;
                    break;
                case 2:
                    $belumMungkin++;
                    break;
                case 3:
                    $wiraswasta++;
                    break;
                case 4:
                    $lanjutPendidikan++;
                    break;
                case 5:
                    $cariKerja++;
                    break;
            }
        }

        $status[0] = $bekerja;
        $status[1] = $belumMungkin;
        $status[2] = $wiraswasta;
        $status[3] = $lanjutPendidikan;
        $status[4] = $cariKerja;

        return $status;
    }

    public function countGenderByProvinsi($where)
    {
        $this->db->where('a.tahun_lulus',$this->session->userdata('tracer_tahun'));
        if ($where) {
            $this->db->where($where);
        }

        $this->db->select('kode_prov');
        $get_status = $this->db->get('alumni a')->result();

        $status = [];
        $aceh = 0;
        $sumut = 0;
        $sumbar = 0;
        $riau = 0;
        $jambi = 0;
        $sumsel = 0;
        $bengkulu = 0;
        $lampung = 0;
        $bangka = 0;
        $kepri = 0;
        $lampung = 0;
        $jakarta = 0;
        $jabar = 0;
        $jateng = 0;
        $jogja = 0;
        $jatim = 0;
        $banten = 0;
        $bali = 0;
        $ntb = 0;
        $ntt = 0;
        $kalbar = 0;
        $kalteng = 0;
        $kalsel = 0;
        $kaltim = 0;
        $kalut = 0;
        $sulut = 0;
        $sulteng = 0;
        $sulsel = 0;
        $sultenggara = 0;
        $gorontalo = 0;
        $sulbar = 0;
        $maluku = 0;
        $malukuUtara = 0;
        $papuaBarat = 0;
        $papua = 0;

        foreach ($get_status as $key => $value) {
            switch ($value->kode_prov) {
                case '11':
                    $aceh++;
                    break;
                case '12':
                    $sumut++;
                    break;
                case '13':
                    $sumbar++;
                    break;
                case '14':
                    $riau++;
                    break;
                case '15':
                    $jambi++;
                    break;
                case '16':
                    $sumsel++;
                    break;
                case '17':
                    $bengkulu++;
                    break;
                case '18':
                    $lampung++;
                    break;
                case '19':
                    $bangka++;
                    break;
                case '21':
                    $kepri++;
                    break;
                case '31':
                    $jakarta++;
                    break;
                case '32':
                    $jabar++;
                    break;
                case '33':
                    $jateng++;
                    break;
                case '34':
                    $jogja++;
                    break;
                case '35':
                    $jatim++;
                    break;
                case '36':
                    $banten++;
                    break;
                case '51':
                    $bali++;
                    break;
                case '52':
                    $ntb++;
                    break;
                case '53':
                    $ntt++;
                    break;
                case '61':
                    $kalbar++;
                    break;
                case '62':
                    $kalteng++;
                    break;
                case '63':
                    $kalsel++;
                    break;
                case '64':
                    $kaltim++;
                    break;
                case '65':
                    $kalut++;
                    break;
                case '71':
                    $sulut++;
                    break;
                case '72':
                    $sulteng++;
                    break;
                case '73':
                    $sulsel++;
                    break;
                case '74':
                    $sultenggara++;
                    break;
                case '75':
                    $gorontalo++;
                    break;
                case '76':
                    $sulbar++;
                    break;
                case '81':
                    $maluku++;
                    break;
                case '82':
                    $malukuUtara++;
                    break;
                case '91':
                    $papuaBarat++;
                    break;
                case '94':
                    $papua++;
                    break;
            }
        }

        $status[0] = $aceh;
        $status[1] = $sumut;
        $status[2] = $sumbar;
        $status[3] = $riau;
        $status[4] = $jambi;
        $status[5] = $sumsel;
        $status[6] = $bengkulu;
        $status[7] = $lampung;
        $status[8] = $bangka;
        $status[9] = $kepri;
        $status[10] = $jakarta;
        $status[11] = $jabar;
        $status[12] = $jateng;
        $status[13] = $jogja;
        $status[14] = $jatim;
        $status[15] = $banten;
        $status[16] = $bali;
        $status[17] = $ntb;
        $status[18] = $ntt;
        $status[19] = $kalbar;
        $status[20] = $kalteng;
        $status[21] = $kalsel;
        $status[22] = $kaltim;
        $status[23] = $kalut;
        $status[24] = $sulut;
        $status[25] = $sulteng;
        $status[26] = $sulsel;
        $status[27] = $sultenggara;
        $status[28] = $gorontalo;
        $status[29] = $sulbar;
        $status[30] = $maluku;
        $status[31] = $malukuUtara;
        $status[32] = $papuaBarat ;
        $status[33] = $papua;

        return $status;
    } 
}
