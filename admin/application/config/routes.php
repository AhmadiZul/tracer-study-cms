<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = "login";
$route['(profile|forgot_password|aktifkan_akun|forgot_password_reset|logout)'] = 'index/$1';
$route['404_override'] = 'my404';
$route['translate_uri_dashes'] = FALSE;
$route['api/(:any)'] = "api/$1";
$route['api'] = "api/index";

$route['pwmp/(:any)'] = "pwmp/Index/$1";

// auth
$route['login'] = "auth/login";
// api
$route['api/wilayahadministrasi/(:any)'] = "api/WilayahAdministrasi/$1";

$route['group_module'] = 'group_module/index';
$route['group_module/detail?(:any)'] = 'group_module/index/detail/$1';

$route['modul'] = 'modul/index';

$route['perguruan_tinggi'] = 'perguruan_tinggi/index';
$route['universitas_profil'] = 'universitas_profil/index';

$route['hasil_survey'] = 'hasil_survey/index';
$route['hasil_survey_mitra'] = 'hasil_survey_mitra/index';